<?
	$pg_name = basename($_SERVER['SCRIPT_FILENAME']);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome to our Website</title>
<link href="styles/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</head>

<body>
	<div class="navigation">
		<ul>
			<li class="contactus"><a href="contactus.php" <? if($pg_name=='contactus.php') echo 'class="active"'; ?>>&nbsp;</a></li>
			<li class="registration"><a href="registration.php" <? if($pg_name=='registration.php') echo 'class="active"'; ?>>&nbsp;</a></li>
			<li class="news"><a href="news.php" <? if($pg_name=='news.php') echo 'class="active"'; ?>>&nbsp;</a></li>
			<li class="event"><a href="event.php" <? if($pg_name=='event.php') echo 'class="active"'; ?>>&nbsp;</a></li>
			<li class="sponsor"><a href="sponsor.php" <? if($pg_name=='sponsor.php') echo 'class="active"'; ?>>&nbsp;</a></li>
			<li class="attendee"><a href="attendee.php" <? if($pg_name=='attendee.php') echo 'class="active"'; ?>>&nbsp;</a></li>
			<li class="home"><a href="index.php" <? if($pg_name=='index.php') echo 'class="active"'; ?>>&nbsp;</a></li>
		</ul>
	<div class="clear"></div>
	</div>
	<div class="header">
		<div class="header-left">
			<div class="logo"><a href="#"><img src="images/logo.gif" alt="International Foodservice Sustainability Symposium"  /></a></div>
			
		</div>
		<div class="header-right">
		<!-- banner start -->
		<div class="bnrTop"><span class="lft"></span></div>
		<div class="bnrCont">
			<img src="images/banner-in.jpg" alt="" />
		</div>
		<!-- banner end -->
		</div>
	<div class="clear"></div>
	</div>
	<div class="content-area">
