<?php

	class TransactionStatus {
		const ACTIVE		= 1;
		const WAITING_CONF	= 2;
		const BOOKED		= 3;
		const TAKING_LESSON     = 4;
		const CHANGE_TO_MONEY   = 5;
	}
	class TransactionType {
		const WITHDRAW		= 0;
		const CREDIT		= 1;
		const DEBIT		= 2;
		const DEBIT_REFUND	= 3;
                const CREDIT_REFUND	= 4;
	}
	
?>