<?php
/*******************************************************************************/
			#This page is to add/edit game details
			
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$game_id=$_REQUEST['game_id'];
$game_name=$_REQUEST['game_name'];
$game_description=$_REQUEST['game_description'];
$action=$_REQUEST['action'];

if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	
	/* Holding Data If Error Starts */

	$FabricArr['game_name']			 = htmlspecialchars(trim($game_name));
	$FabricArr['game_descrption']	 = htmlspecialchars(trim($game_description));
	$FabricArr['is_active']				 = $is_active;
	
	/* Holding Data If Error Ends */

	/* Error Checking Starts */
	
	$err_msgs="";
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_game","game_name",$game_name,"Game","game_id",$game_id);
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{		
		if(empty($game_id))
		{
			
			 $table_name = TABLEPREFIX."_game ";

			$fields_values = array( 
									'game_name'         => $game_name,	
									'game_description'  => $game_description,								
									'is_active'			=> $is_active,
									'date_added'		=> date("Y-m-d H:i:s")									
									);		
			//print_r($fields_values);
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			
			$game_id=mysql_insert_id();	

			/* Insert Into Events Ends */
		}
		else if(!empty($game_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_game";

			$fields_values = array( 
									'game_name'         => $game_name,	
									'game_description' => $game_description,								
									'is_active'		    => $is_active,
									'date_edited'	    => date("Y-m-d H:i:s")		
									);		
			
			$where="game_id='$game_id'";										

			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */
		}
				
		echo "<script>window.location.href='game_manager.php?messg=".$msgreport."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($game_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_game WHERE game_id=".$game_id;
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
    
	$FabricArr['game_name']             = show_to_control($RsCatSql["game_name"]);
	$FabricArr['game_description']      = show_to_control($RsCatSql["game_description"]);
	$FabricArr['is_active']			    = $RsCatSql["is_active"];
	
	/* Get Record For Display Ends */

	$SubmitButton="Update Game ";	
	$flag=1;
}
else
{
	$SubmitButton=" Add Game ";
}


/* Assign Smarty Variables Starts */

$smarty->assign('CmsArr',$CmsArr);
$smarty->assign('game_id',$game_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("game_update.tpl");
?>