<?php
/*******************************************************************************/
			#This page is to add/edit ladder details
			#last Updated : August 23 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$game_id =$_REQUEST['game_id'];
$map_id =$_REQUEST['map_id'];
$map_title=$_REQUEST['map_title'];
$action=$_REQUEST['action'];
if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	/* Holding Data If Error Starts */

	$FabricArr['map_title']		 = htmlspecialchars(trim($map_title));
	$FabricArr['is_active']			 = $is_active;
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
	
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_game_map","map_title",$map_title,"Map title","map_id",$map_id,"game_id",$game_id);
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{
			
		if(empty($map_id))
		{
			
			$table_name = TABLEPREFIX."_game_map ";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'map_title'					    => $map_title,
									'is_active'  						=> $is_active,
									'date_added' 						=> date("Y-m-d H:i:s")
									);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			$map_id=mysql_insert_id();	

			/* Insert Into Events Ends */
			
		}
		else if(!empty($map_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_game_map";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'map_title'					    => $map_title,
									'is_active'  						=> $is_active,
									'date_edited' 						=> date("Y-m-d H:i:s")
									);

			$where="map_id='$map_id'";										
			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */	
		}		
		echo "<script>window.location.href='map_manager.php?messg=".$msgreport."&game_id=".$game_id."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($map_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_game_map WHERE map_id='".$map_id."' AND game_id='".$game_id."'";
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	$FabricArr['map_title'] 				= show_to_control($RsCatSql["map_title"]);
	$FabricArr['date_edited'] 				= $RsCatSql["date_edited"];
	$FabricArr['date_added'] 				= $RsCatSql["date_added"];
	$FabricArr['is_active']					= $RsCatSql["is_active"];

	/* Get Record For Display Ends */

	$SubmitButton="Update Map";	
	$flag=1;
}
else
{
	$SubmitButton=" Add Map ";
}


/* Assign Smarty Variables Starts */

$smarty->assign('game_id',$game_id);
$smarty->assign('map_id',$map_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("map_update.tpl");
?>