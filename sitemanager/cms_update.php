<?php
/*******************************************************************************/
	#This page is to add/edit CMS content
	#last Updated : June 30 , 2010
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$cms_id=$_REQUEST['cms_id'];
$cms_title=$_REQUEST['cms_title'];
$cms_description=$_REQUEST['cms_description'];
$action=$_REQUEST['action'];

$cms_not_metatag = array(4,5,6,7,16);
$cms_has_photo = array(4,5);
$cms_has_short_description = array(6);
$cms_not_description = array(1,6);
$image_object->SetImagePath("../uploaded/cms_images");

if($action=="trans")
{

	/* Holding Data If Error Starts */
    
	$CMSArr['cms_title']=htmlspecialchars(trim($cms_title));
	$CMSArr['cms_description']=$cms_description;
	$CMSArr['metatag_title']=htmlspecialchars(trim($metatag_title));
	$CMSArr['metatag_title']=htmlspecialchars(trim($metatag_title));
	$CMSArr['metatag_title']=htmlspecialchars(trim($metatag_title));
	$CMSArr['metatag_title']=htmlspecialchars(trim($metatag_title));
	
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";	
	
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_cms","cms_title",$cms_title,"CMS Title","cms_id",$cms_id);
	if(in_array($cms_id,$cms_has_short_description))
	{
	    $err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($cms_short_description),"Short Description","EMP",$type="");
	}
	if(!in_array($cms_id,$cms_not_metatag))
	{
		$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($metatag_title),"Metatag Title","EMP",$type="");
		$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($metatag_keywords),"Metatag Keywords","EMP",$type="");
		$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($metatag_description),"Metatag Description","EMP",$type="");
	}
	/* Image(thumbnail) check starts */
    if(in_array($cms_id,$cms_has_photo))
	{
	
		$http_image_name=$_FILES['img1']['name'];
		$http_image_size=$_FILES['img1']['size'];
		$http_image_type=$_FILES['img1']['type'];
		$http_image_temp=$_FILES['img1']['tmp_name'];
		$PhotoT1=$_POST['PhotoT1'];
		$imghidden1=$_POST['imghidden1'];
		
		$err_msgs .=$image_object->CheckImage($PhotoT1,$http_image_name,$http_image_temp,$http_image_type,$http_image_size,$checktype=1,$Photolebel="Upload Image","Y");
	}	
	/* Image(thumbnail) check ends */
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
	{	
	    /* Image Upload Starts */

		$http_image_name=$_FILES['img1']['name'];
		$http_image_temp=$_FILES['img1']['tmp_name'];
		$Picc1=$image_object->UploadImage($PhotoT1,$imghidden1,$http_image_name,$http_image_temp,120,$resize_type=1,89);
						
		/* Image Upload Ends */
		
		if(empty($cms_id))
		{		
			/* Insert Into CMS table Starts */

			$table_name = TABLEPREFIX."_cms";

			$fields_values = array( 
									'cms_title'  		  	=> $cms_title,
									'cms_short_description' => addslashes($cms_short_description),
									'cms_description'   	=> $cms_description,
									'cms_image'			    => $Picc1,
									'metatag_title'   	  	=> $metatag_title,
									'metatag_keywords'  	=> $metatag_keywords,
									'metatag_description'   => $metatag_description,
									'date_added' 		  	=> date("Y-m-d H:i:s")
							    	);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);

			$cms_id=mysql_insert_id();	

			/* Insert Into CMS table Ends */
			
		}
		elseif(!empty($cms_id))
		{
			/* Update Cms Starts */

			$table_name = TABLEPREFIX."_cms ";

			$fields_values = array( 
									'cms_title'  		  	=> $cms_title,
									'cms_short_description' => addslashes($cms_short_description),
									'cms_description'   	=> $cms_description,
									'cms_image'			    => $Picc1,
									'metatag_title'   	  	=> $metatag_title,
									'metatag_keywords'  	=> $metatag_keywords,
									'metatag_description'   => $metatag_description,
									'date_edited' 		  	=> date("Y-m-d H:i:s")
								   );	

			$where=" cms_id='$cms_id' ";										

			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Cms Ends */	

		}		
	 echo "<script>window.location.href='cms_manager.php?messg=".$msgreport."&IsPreserved=Y'</script>";

		exit; 
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($_REQUEST[cms_id]))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_cms WHERE cms_id=".$cms_id;
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
    
	$CMSArr['cms_title'] 	  			= $RsCatSql["cms_title"];
	$CMSArr['cms_image'] 			    = $RsCatSql["cms_image"];
	$CMSArr['metatag_title'] 	  		= $RsCatSql["metatag_title"];
	$CMSArr['metatag_keywords'] 	  	= $RsCatSql["metatag_keywords"];
	$CMSArr['metatag_description'] 	  	= $RsCatSql["metatag_description"];
	$CMSArr['cms_short_description'] 	= stripslashes($RsCatSql["cms_short_description"]);
	$CMSArr['cms_description'] 	  		= $RsCatSql["cms_description"];
	
	/* Get Record For Display Ends */

	$SubmitButton="Update CMS";	
	$flag=1;
}
else
{
	$SubmitButton="Add CMS";
}
$DisplayImageControlStr=$image_object->DisplayImageControl("Upload Image","img1",$RsCatSql['cms_image'],"imghidden1","PhotoT1",$instuctiontype=1,$mandatory="Y","Normal",90);

$smarty->assign('cms_id',$cms_id);
$smarty->assign('cms_has_short_description',$cms_has_short_description);
$smarty->assign('cms_not_description',$cms_not_description);
$smarty->assign('cms_not_metatag',$cms_not_metatag);
$smarty->assign('cms_has_photo',$cms_has_photo);
$smarty->assign('CMSArr',$CMSArr);
$smarty->assign('DisplayImageControlStr',$DisplayImageControlStr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("cms_update.tpl");
?>