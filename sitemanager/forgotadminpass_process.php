<?php
include('general_include.php');

$action=$_GET[action];

if($action=="forgotPass")
{   
	$Username=$_GET[Username];

	$SqlUserInfo="SELECT * FROM ".TABLEPREFIX."_admin WHERE  admin_username =".sql_value($Username);

	$RsUserInfo=$AdminManagerObjAjax->GetRecords("Row",$SqlUserInfo);

	if(!empty($Username))
	{
		if((count($RsUserInfo)>0) && ($RsUserInfo[admin_username]==$Username))
		{	
			/*** User Email ***/
			$toemail=$RsUserInfo['admin_email'];
			/*******************/
			
			/*** User Password ***/
			$userpassword=$RsUserInfo['admin_password'];
			/*******************/

			$Subject="Global Esports: Admin Password";

			$TemplateFile="email_template/forgotadminpass.html";
			$TemplateFileHandler=fopen($TemplateFile,"r");
			$TemplateMessage=fread($TemplateFileHandler,filesize($TemplateFile));

			fclose($TemplateFileHandler);

			$TemplateMessage=str_replace("[SITEURL]",$site_url,$TemplateMessage);
			$TemplateMessage=str_replace("[USERNAME]",$Username,$TemplateMessage);
			$TemplateMessage=str_replace("[PASSWORD]",$userpassword,$TemplateMessage);
			$TemplateMessage=str_replace("[SITENAME]",$site_name,$TemplateMessage);

			/*** PHPMailer Code ***/

			$mail = new PHPMailer;

			/*** Use SMTP*********/

			//$mail->IsSMTP(); 

			//$mail->Host = "mail.citytechcorp.com";

			/*** Use SMTP*********/

			$mail->ClearAllRecipients();
			$mail->FromName = "Global Esports";
			$mail->From    	= "Global Esports";
			$mail->Subject 	= $Subject;
			$mail->Body    	= stripslashes($TemplateMessage);
			$mail->IsHTML(true);
			$mail->AddAddress($toemail,"Global Esports");

			if(!$mail->Send())
			{
				$msgerr=1;

				echo '2-Error in sending mail.';
			}
			else
			{
				echo '2-<span>Your password has been sent to your Email Address.</span>';
			} 
			
			exit;
		}
		else
		{
			echo '2-Invalid <strong>Username</strong>.';
		}
	}
	else
	{
		echo '2-Please enter the <strong>Username</strong>.';
	}
}
?>