<?php
/*******************************************************************************/
	#This page lists all the race we have in table
	#last Updated : August 25 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$page_name="coach_ime_manager.php";
$user_id = $_REQUEST['user_id'];
$action=$_REQUEST['action'];
$id=$_REQUEST['id'];

$IsPreserved=$_REQUEST['IsPreserved'];

if($IsPreserved=="Y")
{
/* preserve select starts */
$PreserveVariableArr=$preserve_variable_object->PreserveVariableSelect();
foreach($PreserveVariableArr as $a=>$b){$$a=$b;}
/* preserve select ends */
}

/* Activate Operation Starts */
if($action=="activate")
{
	$ChangeStatusObjAjax->ChangeStatus($record_id,7);	
}
/* Activate Operation Ends */


/* Delete Operation Starts */

if($action=="del")
{	
	if(!empty($delete_id))
	{
	
		/* Delete Starts */
		$SqlDelete="DELETE FROM ".TABLEPREFIX."_timings WHERE timing_id='$timing_id'";
		$ResultDelete=$adodbcon->Execute($SqlDelete);		
				
		/* Delete Ends */

		if($adodbcon->ErrorNo()) 
			$messg = 5;
		else 
			$messg = 6;
	}
	else
	{
		$messg = 7;
	}		
}

/* Delete Operation Ends */


/* IsProcess Starts */

$action_arr=array("list_order","ranking","activate","list_paginate","list_search","del","default");

if(in_array($action,$action_arr))
{
	$smarty->assign('IsProcess',"Y");
}

/* IsProcess Ends */

	/* listing Operation Starts */

	/* Order section starts */	

	$SortingSequenceArr=array(1 => "race_title");	
	$ReturnSortingArr=$SortingObjAjax->Sorting("timing_id",$SortingSequenceArr,$do_order,$OrderByID,$OrderType);	
	$OrderBySql=$ReturnSortingArr['OrderBySql'];
	$OrderLink=$ReturnSortingArr['OrderLink'];
	$OrderType=$ReturnSortingArr['OrderType'];			

	/* Order section ends */

	$SqlSelectCat="SELECT * FROM ".TABLEPREFIX."_timings Where user_id=".$user_id.$searchSql.$OrderBySql;

	/* pagination starts */	

	$pagination_arr=$PaginationObjAjax->PaginationAjax($SqlSelectCat,$page_name."?action=".$action."&".$SearchLink."&".$OrderLink,"ManagerGeneral");	

	/* pagination ends */ 

	/* preserve update starts */

	$PreserveLink="from=$from&".$SearchLink."&".$OrderLink;

	$preserve_variable_object->PreserveVariableUpdate($PreserveLink);

	/* preserve update ends */		


	#Fetch all project gallery and store them in an array
	$SelectCmsArr=$adodbcon->GetAll($pagination_arr[0]);	

	$NumSelectCms=count($SelectCmsArr);

	for($i=0;$i<$NumSelectCms;$i++)
	{				
		$SelectCmsArr[$i]['time_slot_id']				= $SelectCmsArr[$i]['time_slot_id'];
		$SelectCmsArr[$i]['time_from']                  = $SelectCmsArr[$i]['time_from'];
		$SelectCmsArr[$i]['time_to']                    = $SelectCmsArr[$i]['time_to'];
        
		
		$SelectCmsArr[$i]['time_slot'] = $AdminManagerObjAjax->GetRecords("One","SELECT time_slot FROM ".TABLEPREFIX."_time_slot
		                                 WHERE time_slot_id=".$SelectCmsArr[$i]['time_slot_id']);	
		$SelectCmsArr[$i]['time_delete']                = addslashes(show_to_control($SelectCmsArr[$i]['time_slot']));								 
	}

/* listing Operation Ends */

$MessgReportText=displayMessage($messg);

$UserName = $AdminManagerObjAjax->GetRecords("One","SELECT name FROM ".TABLEPREFIX."_user WHERE user_id=".$user_id);	
/* Assign Smarty Variables Starts */

$smarty->assign("UserName",$UserName);
$smarty->assign("user_id",$user_id);
$smarty->assign("MessgReportText",$MessgReportText);
$smarty->assign('page_name',$page_name);
$smarty->assign('from',$from);
$smarty->assign('PreserveLink',$PreserveLink);
$smarty->assign('SearchLink',$SearchLink);
$smarty->assign('dosearch',$dosearch);
$smarty->assign("Search_race_name",$Search_race_name);
$smarty->assign('OrderLink',$OrderLink);
$smarty->assign('do_order',$do_order);
$smarty->assign('OrderType',$OrderType);
$smarty->assign('OrderByID',$OrderByID);
$smarty->assign('ReturnSortingArr',$ReturnSortingArr);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('NumSelectCms',$NumSelectCms);
$smarty->assign('SelectCmsArr',$SelectCmsArr);

/* Assign Smarty Variables Ends */

$smarty->display("coach_time_manager.tpl");
?>