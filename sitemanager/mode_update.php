<?php
/*******************************************************************************/
			#This page is to add/edit mode details
			#last Updated : August 24 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$game_id =$_REQUEST['game_id'];
$mode_id =$_REQUEST['mode_id'];
$mode_title=$_REQUEST['mode_title'];
$action=$_REQUEST['action'];
if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	/* Holding Data If Error Starts */

	$FabricArr['mode_title']		 = htmlspecialchars(trim($mode_title));
	$FabricArr['is_active']			 = $is_active;
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
	
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_game_mode","mode_title",$mode_title,"Mode title","mode_id",$mode_id,"game_id",$game_id);
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{
			
		if(empty($mode_id))
		{
			
			$table_name = TABLEPREFIX."_game_mode ";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'mode_title'					    => $mode_title,
									'is_active'  						=> $is_active,
									'date_added' 						=> date("Y-m-d H:i:s")
									);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			$class_id=mysql_insert_id();	

			/* Insert Into Events Ends */
			
		}
		else if(!empty($mode_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_game_mode";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'mode_title'					    => $mode_title,
									'is_active'  						=> $is_active,
									'date_edited' 						=> date("Y-m-d H:i:s")
									);

			$where="mode_id='$mode_id'";										
			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */	
		}		
		echo "<script>window.location.href='mode_manager.php?messg=".$msgreport."&game_id=".$game_id."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($mode_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_game_mode WHERE mode_id='".$mode_id."' AND game_id='".$game_id."'";
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	$FabricArr['mode_title'] 				= show_to_control($RsCatSql["mode_title"]);
	$FabricArr['date_edited'] 				= $RsCatSql["date_edited"];
	$FabricArr['date_added'] 				= $RsCatSql["date_added"];
	$FabricArr['is_active']					= $RsCatSql["is_active"];

	/* Get Record For Display Ends */

	$SubmitButton="Update mode";	
	$flag=1;
}
else
{
	$SubmitButton=" Add mode ";
}


/* Assign Smarty Variables Starts */

$smarty->assign('game_id',$game_id);
$smarty->assign('mode_id',$mode_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("mode_update.tpl");
?>