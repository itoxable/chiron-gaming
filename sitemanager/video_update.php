<?php
/*******************************************************************************/
	#This page is to add/edit video details
	#last Updated : July 09 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$u_id =$_REQUEST['u_id'];
$utype = $_REQUEST['utype'];
$video_id		= $_REQUEST['video_id'];
$video_title	= $_REQUEST['video_title'];
$is_video_url	= $_REQUEST['is_video_url'];
$action			= $_REQUEST['action'];

if($is_video_url =="N")
{
	$man_file="Y";
}
else
{
	$man_file="N";
}

$image_object->SetImagePath("../uploaded/video_images");
$video_object->SetVideoPath("uploaded/videos");

if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	
	/* Holding Data If Error Starts */

	$FabricArr['video_title']	= htmlspecialchars(trim($video_title));
	$FabricArr['is_video_url']	= htmlspecialchars(trim($is_video_url));
	$FabricArr['video_url']		= htmlspecialchars(trim($video_url));
	$FabricArr['description']   = htmlspecialchars(trim($description));
	$FabricArr['time_length']   = htmlspecialchars(trim($time_length));
	$FabricArr['game_id']		= htmlspecialchars(trim($game_id));
	$FabricArr['ladder_id']      = $ladder_id;
	
	$FabricArr['versus_id']        = $submited_versus;
	$FabricArr['class_id']         		= $submited_class;
	$FabricArr['team_id']      	   		= $submited_team;
	$FabricArr['type_id']          		= $submited_type;
	$FabricArr['champion_id']      = $submited_champion;
	$FabricArr['map_id']      	   	= $submited_map;
	$FabricArr['mode_id']      	   		= $submited_mode;
	
	/*echo "<pre>";
	print_r($FabricArr);
	exit;*/
	
	if($FabricArr['is_video_url']=='N')
	{
		$url_link = 'N';
	}
	else
	{
		$url_link = 'Y';
	}
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
	$err_msgs .= $AdminManagerObjAjax->Validate(strip_tags($game_id),"Game","EMP",$type="");
	//$err_msgs .= $AdminManagerObjAjax->Validate(strip_tags($ladder_id),"Ladder","EMP",$type="");	
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_video","video_title",$video_title,"Video Title","video_id",$video_id,"user_id",$u_id);
	$err_msgs .= $AdminManagerObjAjax->Validate(strip_tags($is_video_url),"Video Type","EMP",$type="");
	
	if($is_video_url == 'Y')
	{
		$err_msgs .= $AdminManagerObjAjax->Validate(strip_tags($video_url),"Youtube Url","EMP",$type="UTUBE");
	}
	
	$http_video_name=$_FILES['video_doc']['name'];
	$http_video_size=$_FILES['video_doc']['size'];
	$http_video_type=$_FILES['video_doc']['type'];
	$http_video_temp=$_FILES['video_doc']['tmp_name'];	
	
	$err_msgs .=$video_object->CheckVideo($VideoRadio,$http_video_name,$http_video_temp,$http_video_type,$http_video_size,"Upload File",$mandatory=$man_file,$bytesize=10);
	
	/* Image(thumbnail) check starts */

	$http_image_name=$_FILES['img1']['name'];
	$http_image_size=$_FILES['img1']['size'];
	$http_image_type=$_FILES['img1']['type'];
	$http_image_temp=$_FILES['img1']['tmp_name'];
	$PhotoT1=$_POST['PhotoT1'];
	$imghidden1=$_POST['imghidden1'];
	
	$err_msgs .=$image_object->CheckImage($PhotoT1,$http_image_name,$http_image_temp,$http_image_type,$http_image_size,$checktype=1,$Photolebel="Upload Image",$man_file);
	/* Image(thumbnail) check ends */
	
	
	
	if(empty($err_msgs))// If Empty Error Starts 
	{	
	
	
	
		if(count($submited_versus)>0)  $versus_id=implode(",",$submited_versus);
		if(count($submited_class)>0)  $class_id=implode(",",$submited_class);
		if(count($submited_team)>0)  $team_id=implode(",",$submited_team);
		if(count($submited_type)>0)  $type_id=implode(",",$submited_type);
		if(count($submited_champion)>0)  $champion_id=implode(",",$submited_champion);
		if(count($submited_map)>0)  $map_id=implode(",",$submited_map);
		if(count($submited_mode)>0)  $mode_id=implode(",",$submited_mode);
		
		
		$http_video_name=$_FILES['video_doc']['name'];
		$http_video_temp=$_FILES['video_doc']['tmp_name'];																		
		$Vid1=$video_object->UploadVideo($VideoRadio,$videohidden1,$http_video_name,$http_video_temp);
		
		/* Gallery Image Upload Starts */

		$http_image_name=$_FILES['img1']['name'];
		$http_image_temp=$_FILES['img1']['tmp_name'];
		//$Picc1=$image_object->UploadImage($PhotoT1,$imghidden1,$http_image_name,$http_image_temp,120,$resize_type=1,89);
		$Picc1 = $image_object->UploadImage($PhotoT1,$imghidden1,$http_image_name,$http_image_temp,57,$resize_type=2,'','Y',226,$resize_type=5,'169');
		$uploadfile = "../uploaded/video_images/".$Picc1;
		$mediumimagename = "thumbs/mid_" . $Picc1;
		$smallimagename = "thumbs/small_" . $Picc1;
		$image_object->create_gdthumbnail($uploadfile,$smallimagename,120,1,90);
		$image_object->create_gdthumbnail($uploadfile,$mediumimagename,190,1,142);				
		/* Gallery Image Upload Ends */
	
		if(empty($video_id))
		{
			
			/* Insert Into Video Starts */

			$table_name = TABLEPREFIX."_video ";

			$fields_values = array( 
									'video_title'  			=> $video_title,
									'is_video_url'  		=> $is_video_url,
									'description'           => $description,
									'time_length'  			=> $time_length ,
									'video_url'				=> $video_url,
									'video_file'			=> $Vid1,	
									'video_image'			=> $Picc1,
									'game_id'				=> $game_id,	
									'ladder_id'				=> $ladder_id,
									'versus_id'     		=> $versus_id,
									'class_id'      		=> $class_id,
									'team_id'      			=> $team_id,
									'type_id'      			=> $type_id,
									'champion_id'      		=> $champion_id,
									'map_id'   				=> $map_id,
									'mode_id' 				=> $mode_id,
									'user_id'               => $u_id,
									'date_added'			=> date("Y-m-d H:i:s")
							    	);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);

			$video_id=mysql_insert_id();	
            // echo mysql_error();
			/* Insert Into Video Ends */
		}
		elseif(!empty($video_id))
		{
			/* Update Video Starts */

			$table_name = TABLEPREFIX."_video ";
            $Sql1= "UPDATE ".TABLEPREFIX."_video set ladder_id='',versus_id='',class_id='', team_id='', type_id='',champion_id='',map_id='',
			mode_id='' where video_id='$video_id'";  
		    $AdminManagerObjAjax->Execute($Sql1);
			
			$fields_values = array( 
									'video_title'  			=> $video_title,
									'is_video_url'  		=> $is_video_url,
									'description'           => $description,
									'time_length'  			=> $time_length ,
									'video_url'				=> $video_url,
									'video_file'			=> $Vid1,	
									'video_image'			=> $Picc1,
									'game_id'				=> $game_id,	
									'ladder_id'				=> $ladder_id,
									'versus_id'     		=> $versus_id,
									'class_id'      		=> $class_id,
									'team_id'      			=> $team_id,
									'type_id'      			=> $type_id,
									'champion_id'      		=> $champion_id,
									'map_id'   				=> $map_id,
									'mode_id' 				=> $mode_id,
									'user_id'               => $u_id,
									'date_edited'			=> date("Y-m-d H:i:s")
								   );	

			$where=" video_id='$video_id' ";										
               
			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Video Ends */	

		}	
			
		echo "<script>window.location.href='video_manager.php?u_id=".$u_id."&messg=".$msgreport."&IsPreserved=Y'</script>";
		exit;
		 
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}
$submited_ladder=array();
$submited_versus=array();
$submited_team=array();
$submited_type=array();
$submited_champion=array();
$submited_map=array();
$submited_mode=array();
$submited_class=array();

if(!empty($video_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_video WHERE video_id=".$video_id;
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
    
	$FabricArr['video_title'] 			= $RsCatSql["video_title"];
	$FabricArr['video_url'] 			= $RsCatSql["video_url"];
	$FabricArr['video_file']			= $RsCatSql["video_file"];
	$FabricArr['is_video_url']			= $RsCatSql["is_video_url"];
	$FabricArr['video_image'] 			= $RsCatSql["video_image"];
	$FabricArr['game_id']				= $RsCatSql["game_id"];
	$FabricArr['ladder_id'] 			= $RsCatSql["ladder_id"];
	$FabricArr['description'] 			= show_to_control($RsCatSql["description"]);
	$FabricArr['time_length'] 			= show_to_control($RsCatSql["time_length"]);
	
	
	$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game WHERE game_id=".$FabricArr['game_id'];
	$RsgameSql = $AdminManagerObjAjax->GetRecords("Row",$SelectgameSql);
	
	$FabricArr['is_ladder']					= $RsgameSql["is_ladder"];
	$FabricArr['is_versus']					= $RsgameSql["is_versus"];
	$FabricArr['is_team']					= $RsgameSql["is_team"];
	$FabricArr['is_type']					= $RsgameSql["is_type"];
	$FabricArr['is_champion']				= $RsgameSql["is_champion"];
	$FabricArr['is_map']					= $RsgameSql["is_map"];
	$FabricArr['is_mode']					= $RsgameSql["is_mode"];
	$FabricArr['is_class']					= $RsgameSql["is_class"];

	
	$SelectVersusSql="SELECT * FROM ".TABLEPREFIX."_game_versus WHERE  game_id=".$FabricArr['game_id']." AND versus_id IN (".$RsCatSql["versus_id"].")";
	$RsVersusSql = $AdminManagerObjAjax->GetRecords("All",$SelectVersusSql);	
	

	for($i=0;$i<count($RsVersusSql);$i++)
	{		
			$submited_versus[] = $RsVersusSql[$i]['versus_id'];
	}
	
	$SelectteamSql="SELECT * FROM ".TABLEPREFIX."_game_team WHERE  game_id=".$FabricArr['game_id']." AND team_id IN (".$RsCatSql["team_id"].")";
	$RsteamSql = $AdminManagerObjAjax->GetRecords("All",$SelectteamSql);	
	

	for($i=0;$i<count($RsteamSql);$i++)
	{		
			$submited_team[] = $RsteamSql[$i]['team_id'];
	}
	
	$SelecttypeSql="SELECT * FROM ".TABLEPREFIX."_game_type WHERE  game_id=".$FabricArr['game_id']." AND type_id IN (".$RsCatSql["type_id"].")";
	$RstypeSql = $AdminManagerObjAjax->GetRecords("All",$SelecttypeSql);	
	

	for($i=0;$i<count($RstypeSql);$i++)
	{		
			$submited_type[] = $RstypeSql[$i]['type_id'];
	}
	
	$SelectchampionSql="SELECT * FROM ".TABLEPREFIX."_game_champion WHERE game_id=".$FabricArr['game_id']." AND champion_id IN (".$RsCatSql["champion_id"].")";
	$RschampionSql = $AdminManagerObjAjax->GetRecords("All",$SelectchampionSql);	
	

	for($i=0;$i<count($RschampionSql);$i++)
	{		
			$submited_champion[] = $RschampionSql[$i]['champion_id'];
	}
	
	$SelectmapSql="SELECT * FROM ".TABLEPREFIX."_game_map WHERE game_id=".$FabricArr['game_id']." AND map_id IN (".$RsCatSql["map_id"].")";
	$RsmapSql = $AdminManagerObjAjax->GetRecords("All",$SelectmapSql);	
	

	for($i=0;$i<count($RsmapSql);$i++)
	{		
			$submited_map[] = $RsmapSql[$i]['map_id'];
	}
	
	$SelectmodeSql="SELECT * FROM ".TABLEPREFIX."_game_mode WHERE game_id=".$FabricArr['game_id']." AND mode_id IN (".$RsCatSql["mode_id"].")";
	$RsmodeSql = $AdminManagerObjAjax->GetRecords("All",$SelectmodeSql);	
	

	for($i=0;$i<count($RsmodeSql);$i++)
	{		
			$submited_mode[] = $RsmodeSql[$i]['mode_id'];
	}
	
	$SelectclassSql="SELECT * FROM ".TABLEPREFIX."_game_class WHERE game_id=".$FabricArr['game_id']." AND class_id IN (".$RsCatSql["class_id"].")";
	$RsclassSql = $AdminManagerObjAjax->GetRecords("All",$SelectclassSql);	
	

	for($i=0;$i<count($RsclassSql);$i++)
	{		
			$submited_class[] = $RsclassSql[$i]['class_id'];
	}
	
	$SelectmapSql="SELECT * FROM ".TABLEPREFIX."_game_map WHERE game_id=".$FabricArr['game_id']." AND map_id IN (".$RsCatSql["map_id"].")";
	$RsmapSql = $AdminManagerObjAjax->GetRecords("All",$SelectmapSql);	
	

	for($i=0;$i<count($RsmapSql);$i++)
	{		
			$submited_map[] = $RsmapSql[$i]['map_id'];
	}
	
	$SelectmodeSql="SELECT * FROM ".TABLEPREFIX."_game_mode WHERE game_id=".$FabricArr['game_id']." AND mode_id IN (".$RsCatSql["mode_id"].")";
	$RsmodeSql = $AdminManagerObjAjax->GetRecords("All",$SelectmodeSql);	
	

	for($i=0;$i<count($RsmodeSql);$i++)
	{		
			$submited_mode[] = $RsmodeSql[$i]['mode_id'];
	}
	
	
	if($FabricArr['is_video_url'] =="N")
	{
		$man_file="Y";
		$url_link="N";
	}
	else
	{
		$man_file="N";
		$url_link="Y";
	}
	
	/* Get Record For Display Ends */

	$SubmitButton="Update Video";	
	$flag=1;
}
else
{
	$SubmitButton="Add Video";
}

$DisplayVideoControlStr=$video_object->DisplayVideoControl("Upload File","video_doc",$FabricArr['video_file'],"videohidden1","VideoRadio",$mandatory=$man_file,$display_video="Y",$videosize=10);

$DisplayImageControlStr=$image_object->DisplayImageControl("Upload Image","img1",$RsCatSql['video_image'],"imghidden1","PhotoT1",$instuctiontype=1,$mandatory=$man_file,"Normal",90);

$gameSql = "SELECT game_id,game_name FROM ".TABLEPREFIX."_game where is_active='Y' ORDER BY game_name";
$GameArr = $AdminManagerObjAjax->HtmlOptionArrayCreate($gameSql);

	$LadderSql = "SELECT ladder_id,ladder_name  FROM ".TABLEPREFIX."_game_ladder  ORDER BY date_added DESC ";  
	$LadderArr = $AdminManagerObjAjax->HtmlOptionArrayCreate($LadderSql);
	//print_r($LadderArr);	 
		 
	$VersusSql = "SELECT *  FROM ".TABLEPREFIX."_game_versus  ORDER BY date_added DESC ";   
	$VersusArr = $AdminManagerObjAjax->GetRecords("All",$VersusSql);	
	
	$TeamSql = "SELECT *  FROM ".TABLEPREFIX."_game_team   ORDER BY date_added DESC ";   
	$TeamArr = $AdminManagerObjAjax->GetRecords("All",$TeamSql);
	
	$TypeSql = "SELECT *  FROM ".TABLEPREFIX."_game_type ORDER BY date_added DESC ";   
	$TypeArr = $AdminManagerObjAjax->GetRecords("All",$TypeSql);
	
	$ChampionSql = "SELECT *  FROM ".TABLEPREFIX."_game_champion ORDER BY date_added DESC ";   
	$ChampionArr = $AdminManagerObjAjax->GetRecords("All",$ChampionSql);
	
	$ClassSql = "SELECT *  FROM ".TABLEPREFIX."_game_class ORDER BY date_added DESC ";   
	$ClassArr = $AdminManagerObjAjax->GetRecords("All",$ClassSql);
	
	$ModeSql = "SELECT *  FROM ".TABLEPREFIX."_game_mode ORDER BY date_added DESC ";   
	$ModeArr = $AdminManagerObjAjax->GetRecords("All",$ModeSql);
	
	$MapSql = "SELECT *  FROM ".TABLEPREFIX."_game_map ORDER BY date_added DESC ";   
	$MapArr = $AdminManagerObjAjax->GetRecords("All",$MapSql);
		

/* Assign Smarty Variables Starts */
$smarty->assign('action',$action);
$smarty->assign('u_id',$u_id);
$smarty->assign('utype',$utype);
$smarty->assign('video_id',$video_id);
$smarty->assign('GameArr',$GameArr);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('url_link',$url_link); 
$smarty->assign('DisplayVideoControlStr',$DisplayVideoControlStr);
$smarty->assign('DisplayImageControlStr',$DisplayImageControlStr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('LadderArr',$LadderArr);
$smarty->assign('VersusArr',$VersusArr);
$smarty->assign('TeamArr',$TeamArr);
$smarty->assign('TypeArr',$TypeArr);
$smarty->assign('ChampionArr',$ChampionArr);
$smarty->assign('ClassArr',$ClassArr);
$smarty->assign('ModeArr',$ModeArr);
$smarty->assign('MapArr',$MapArr);
$smarty->assign('submited_versus',$submited_versus);
$smarty->assign('submited_ladder',$submited_ladder);
$smarty->assign('submited_team',$submited_team);
$smarty->assign('submited_type',$submited_type);
$smarty->assign('submited_champion',$submited_champion);
$smarty->assign('submited_class',$submited_class);
$smarty->assign('submited_mode',$submited_mode);
$smarty->assign('submited_map',$submited_map);
$smarty->register_modifier("inarray","in_array");

$smarty->assign('html',$html);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("video_update.tpl");
?>