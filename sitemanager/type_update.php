<?php
/*******************************************************************************/
			#This page is to add/edit ladder details
			#last Updated : August 23 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$game_id =$_REQUEST['game_id'];
$type_id =$_REQUEST['type_id'];
$type_title=$_REQUEST['type_title'];
$type_full_title=$_REQUEST['type_full_title'];
$action=$_REQUEST['action'];
if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	/* Holding Data If Error Starts */

	$FabricArr['type_title']		 = htmlspecialchars(trim($type_title));
	$FabricArr['type_full_title']		 = htmlspecialchars(trim($type_full_title));
	$FabricArr['is_active']			 = $is_active;
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
	
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_game_type","type_title",$type_title,"Type title","type_id",$type_id,"game_id",$game_id);
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{
			
		if(empty($type_id))
		{
			
			$table_name = TABLEPREFIX."_game_type ";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'type_title'					    => $type_title,
									'type_full_title'					=> $type_full_title,
									'is_active'  						=> $is_active,
									'date_added' 						=> date("Y-m-d H:i:s")
									);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			$type_id=mysql_insert_id();	

			/* Insert Into Events Ends */
			
		}
		else if(!empty($type_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_game_type";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'type_title'					    => $type_title,
									'type_full_title'					=> $type_full_title,
									'is_active'  						=> $is_active,
									'date_edited' 						=> date("Y-m-d H:i:s")
									);

			$where="type_id='$type_id'";										
			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */	
		}		
		echo "<script>window.location.href='type_manager.php?messg=".$msgreport."&game_id=".$game_id."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($type_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_game_type WHERE type_id='".$type_id."' AND game_id='".$game_id."'";
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	$FabricArr['type_title'] 				= show_to_control($RsCatSql["type_title"]);
	$FabricArr['type_full_title'] 				= show_to_control($RsCatSql["type_full_title"]);
	$FabricArr['date_edited'] 				= $RsCatSql["date_edited"];
	$FabricArr['date_added'] 				= $RsCatSql["date_added"];
	$FabricArr['is_active']					= $RsCatSql["is_active"];

	/* Get Record For Display Ends */

	$SubmitButton="Update Type";	
	$flag=1;
}
else
{
	$SubmitButton=" Add Type ";
}


/* Assign Smarty Variables Starts */

$smarty->assign('game_id',$game_id);
$smarty->assign('type_id',$type_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("type_update.tpl");
?>