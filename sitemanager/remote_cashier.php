<?php
	include("general_include.php");
	include "checklogin.php";
	
	$sqlQuery = "SELECT ut.nk_transaction_id as id, u.username username, CONCAT(u.username, ' (', u.user_id,')') as user, ut.tx_date as date, ut.qty_points as points, ut.method, ut.method_detail,ut.Status,u.user_id FROM ".TABLEPREFIX."_user_transaction ut, ".TABLEPREFIX."_transaction_status ts, ".TABLEPREFIX."_user u WHERE ut.transaction_type = 0 AND ut.status = ts.nk_transaction_status_id AND u.user_id = ut.nk_user_id ORDER BY tx_date DESC, nk_transaction_id";

	$transactionList = $adodbcon->GetAll($sqlQuery);				
	$noTransactions = count($transactionList);
	
	$rows = isset($_GET['rows'])?$_GET['rows']:10;
	$cols = isset($_GET['cols'])?$_GET['cols']:7;
	$page = isset($_GET['page'])?$_GET['page']:1;
	$sort = isset($_GET['sort'])?$_GET['sort']:-1;

?>
<table>
<tbody>
	<?php 
		$size = (($rows*($page-1))+$rows);
		if($page>=0 && ($rows*($page-1))<$noTransactions){
			for ($i=($rows*($page-1)); $i<$size; $i++){
				if($transactionList[$i] == null)
                    break;
	?>
			<tr>
				<?php
				echo "<td>". $transactionList[$i]['id'] ."</td>";
				echo "<td>". $transactionList[$i]['user'] ."</td>";
				echo "<td>". $transactionList[$i]['date'] ."</td>";
				echo "<td>". $transactionList[$i]['points'] ."</td>";
				echo "<td>". $transactionList[$i]['method'] ."</td>";
				echo "<td>". $transactionList[$i]['method_detail'] ."</td>";
				if($transactionList[$i]['Status']==2){
					echo "<td><a style='color: black; font-weight: bold' href='?method=".$transactionList[$i]['method']."&quant=".$transactionList[$i]['points']."&userId=$user_id&action=markaspayed&txid=".$transactionList[$i]['id']."' class='button' title='withdraw pending' \">Pay</a></td>";

				}else{
					echo "<td>Payed <a style='color: black; font-weight: bold' href='?method=".$transactionList[$i]['method']."&quant=".$transactionList[$i]['points']."&userId=$user_id&action=markasunpayed&txid=".$transactionList[$i]['id']."' class='button' title='withdraw pending' \">Unpay</a></td>";

				}
				
	
				?>
			</tr>
	<?php }} ?>
</tbody>
</table>