{if $IsProcess neq "Y"}
{include file="top.tpl"}
{literal}
<script type="text/javascript">
var j = jQuery.noConflict();
j(document).ready(function(){
focus_blur('Search_by_name','Search by Name');
focus_blur('Search_by_email','Search by Email');
focus_blur('Search_from_date','Search From');
focus_blur('Search_to_date','Search To');
});

function SetFeatured(url)
{
  j('#TransMsgDisplay').html('<img src="images/indicator.gif" align="center">');	
  j.ajax({
  type: "POST",
  url: url,
  dataType: 'text',
  success: function(data){
	//j('#records_listing').html(data);
	//j(document).html(data);
	document.location.reload();
	//document.write(data);
	if(j('#TransMsgDisplay')) 
	{										
  	  j('#TransMsgDisplay').html('');
	}				           

	}

   });

}
</script>
{/literal}

<h2>Training Partners <div id="TransMsgDisplay" align="right" style="height:3px" ></div></h2>
<form method="POST" name="form_search" id="form_search" action="{$page_name}" onsubmit="return false;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="17%" align="right" valign="middle">	
				<input size="30" type="text" name="Search_by_name" id="Search_by_name" value="{$Search_by_name}" 
				class="textbox search-box" style="width:100px;"/>	
				<input type="hidden" name="dosearch" value="GO">
				</td>
				<td width="17%" align="right" valign="middle" >	
				<input size="30" type="text" name="Search_by_email" id="Search_by_email" value="{$Search_by_email}" class="textbox search-box" style="width:100px;"/>	
				<input type="hidden" name="dosearch" value="GO">
				</td>
				
				<td width="20%" align="left" valign="middle">
				<input type="text" name="Search_from_date" id="Search_from_date" class="datebox" readonly="" value="{$Search_from_date}" style="width:80px;"/>
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_from_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				<input type="hidden" name="dosearch" value="GO">
				</td>

				<td width="20%" align="left" valign="middle">
				<input type="text" name="Search_to_date" id="Search_to_date" class="datebox" readonly="" value="{$Search_to_date}" style="width:80px;"/>
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_to_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				<input type="hidden" name="dosearch" value="GO">
				</td>

				<td valign="middle">
				<input  name="submitdosearch" value="GO" type="button" class="go" onClick="ManagerGeneralForm('{$page_name}?action=list_search','form_search')" />
				<input name="" type="button"  class="reset" value="RESET" onClick="window.location.href='{$page_name}';"/></td>
			</tr>
		
		</table>
	</form>
{/if}	

<div id="records_listing">

{if $MessgReportText neq ""}<div align="left" class="successful2" style="color:#FF0000; margin:-25px 0 0 0;">{$MessgReportText}</div>{/if}	
		
<div class="newsName">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td colspan="6" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#9f9f9f">
				  <tr  bgcolor="#9f9f9f">
				  	<td  align="left" width="20%" valign="middle" class="padleft">
						<a href="#"  onClick="ManagerGeneral('{$page_name}?action=list_order&do_order=GO&OrderByID=1&OrderType={$OrderType}&{$SearchLink}&from={$from}')" >								
							<span class="whitetext"><strong>Name</strong></span>									
							{$ReturnSortingArr.DisplaySortingImage[1]}				
						</a>	
					</td>
					<td  align="left" width="20%" valign="middle" class="padleft">
						<a href="#"  onClick="ManagerGeneral('{$page_name}?action=list_order&do_order=GO&OrderByID=2&OrderType={$OrderType}&{$SearchLink}&from={$from}')" >								
							<span class="whitetext"><strong>Email</strong></span>									
							{$ReturnSortingArr.DisplaySortingImage[2]}				
						</a>	
					</td>
					<td  align="left" width="12%" valign="middle" class="padleft">
						<a href="#"  onClick="ManagerGeneral('{$page_name}?action=list_order&do_order=GO&OrderByID=3&OrderType={$OrderType}&{$SearchLink}&from={$from}')" >								
							<span class="whitetext"><strong>Date Added</strong></span>									
							{$ReturnSortingArr.DisplaySortingImage[3]}				
						</a>	
					</td>	
					<td width="8%" class="whitetext" align="center" valign="middle"><strong>Game</strong></td>
					<td width="8%" class="whitetext" align="center" valign="middle"><strong>Video</strong></td>
					<td width="6%" class="whitetext" align="center" valign="middle"><strong>Active</strong></td>
					<!--td width="10%" class="whitetext" align="center" valign="middle"><strong>Actions</strong></td-->
				  </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#878787">
				{if $NumSelectCms > 0}
			{section name=RowCms loop=$SelectCmsArr}
				  <tr {if ($templatelite.section.RowCms.index % 2) eq 0} bgcolor="#ffffff" {else} bgcolor="#ffffff" {/if}>
				  
				  	<td  align="left" width="20%" height="39" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].username}
					</td>
			        <td  align="left" width="20%" height="39" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].email}
					</td>
					<td  align="left" width="12%" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].date_registered}
					</td>
					<td width="8%" align="center" valign="middle">
					<a href="training_partner_gamemanager.php?u_id={$SelectCmsArr[RowCms].user_id}">
					<img src="images/add-tab-icon.gif" width="20" height="19" border="0" alt="Manage Game" /></a></td>
					<td width="8%" align="center" valign="middle">
					<a href="video_manager.php?u_id={$SelectCmsArr[RowCms].user_id}&utype=tp">
					<img src="images/add-tab-icon.gif" width="20" height="19" border="0" alt="Manage Video" /></a></td>


					<td width="6%" align="center" valign="middle"><a href="#" onClick="ManagerGeneral('{$page_name}?action=activate&{$PreserveLink}&record_id={$SelectCmsArr[RowCms].user_id}')">
					<img src="images/{$SelectCmsArr[RowCms].active_img}" width="15" height="14" border="0" alt="{$SelectCmsArr[RowCms].active_alt}" /></a></td>
					<!--td width="10%" align="center" valign="middle">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
						  <td width="60" height="39" align="center" valign="middle" class="rightBorder" >
						  <a href="user_update.php?u_id={$SelectCmsArr[RowCms].user_id}">
						  <img src="images/edit_dark.gif" alt="" width="15" height="14" border="0" />
						  </a>
						  </td>
						  
						  <td width="50%" align="center" valign="middle">
						  <a href="#" onClick="ConfirmDelete('{$page_name}?action=del&{$PreserveLink}','{$SelectCmsArr[RowCms].user_id}','{$SelectCmsArr[RowCms].name_delete}','Coach:: ')">
						  <img src="images/delete_icon.gif" border="0" alt="" />
						  </a>
						  </td>
						  </tr>
					</table>					
					</td-->
				 </tr>
				  {/section}
			{else}
				<tr bgcolor="#f4f4f5"><td class="plaintxt" colspan="5" align="center">No Record Found</td></tr>
			{/if}       
				  
				 
			  </table>
			</td>
		  </tr>
		 
  </table>
</div>
<br />
<div>	{if $pagination_arr[3] > 0}
   <table>
	  <tr>
        <td height="30" valign="middle" align="center">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td align="left" class="plaintxt">					
					{if $pagination_arr[2] neq ""}					
						{$pagination_arr[2]}
					{/if}					
					</td>
					<td align="right" class="plaintxt">					   	
					{if $pagination_arr[1] neq ""}					
						{$pagination_arr[1]}
					{/if}					
					</td>
				</tr>
			</table>
		</td>
      </tr>
	  </table>
	{/if} </div><br /><br /><br /><br /><br />
</div>

{if $IsProcess neq "Y"}  	
{include file="bottom.tpl"}
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>
{/if}
