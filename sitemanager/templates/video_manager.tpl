{if $IsProcess neq "Y"}
{include file="top.tpl"}

<h2>Video of {$UserName}<div id="TransMsgDisplay" align="right" style="height:3px" ></div></h2>


{if $utype eq 'coach'}
<div align="right" class="addNews_secand"><a href="coach_manager.php" class="normal-bttn">- Coach</a></div>
{/if}
{if $utype eq 'tp'}
<div align="right" class="addNews_secand"><a href="training_partner_manager.php" class="normal-bttn">- Training Partner</a></div>
{/if}
{if $utype eq 'user'}
<div align="right" class="addNews_secand"><a href="user_manager.php" class="normal-bttn">- User</a></div>
{/if}
<div align="right" class="addNews_secand"><a href="video_update.php?u_id={$u_id}&utype={$utype}" class="normal-bttn">+ Add Video</a></div>
{/if}	

<div id="records_listing">

{if $MessgReportText neq ""}<div align="left" class="successful2" style="color:#FF0000; margin:-25px 0 0 0;">{$MessgReportText}</div>{/if}	
		
<div class="newsName">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td colspan="6" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#9f9f9f">
				  <tr  bgcolor="#9f9f9f">
				  	<td  align="left" width="20%" valign="middle" class="padleft"><strong>
							<span class="whitetext"><strong>Video Title</strong></span>									
						   </strong></span></td>
					 	<td  align="left" width="8%" valign="middle" class="padleft"><strong>
							<span class="whitetext"><strong>Likes</strong></span>									
						   </strong></span></td>
						<td  align="left" width="8%" valign="middle" class="padleft"><strong>
							<span class="whitetext"><strong>Dislikes</strong></span>									
						   </strong></span></td>   
					<td width="14%" align="center" valign="middle" >
						  <span class="whitetext"><strong>Game</strong></span>
					</td>
					<td width="10%" align="center" valign="middle" >
						  <span class="whitetext"><strong>Type</strong></span>
					</td>
					<td width="10%" class="whitetext" align="center" valign="middle"><strong>Featured</strong></td>
					<td width="10%" class="whitetext" align="center" valign="middle"><strong>Actions</strong></td>
				  </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#878787">
				{if $NumSelectCms > 0}
			{section name=RowCms loop=$SelectCmsArr}
				  <tr {if ($templatelite.section.RowCms.index % 2) eq 0} bgcolor="#ffffff" {else} bgcolor="#ffffff" {/if}>
				  
				  	<td  align="left" width="20%" height="39" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].video_title}
					</td>
					<td  align="left" width="8%" height="39" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].numlike}
					</td>
					<td  align="left" width="8%" height="39" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].numdlike}
					</td>
			        <td width="14%" class="simpletxt" align="center" valign="middle">
					{$SelectCmsArr[RowCms].game_name}</td>
					<td width="10%" class="simpletxt" align="center" valign="middle">
					{if $SelectCmsArr[RowCms].is_video_url == 'Y'} Video {else} Url {/if}</td>
					<td width="10%" align="center" valign="middle"><a href="#" onClick="ManagerGeneral('{$page_name}?action=featured&{$PreserveLink}&record_id={$SelectCmsArr[RowCms].video_id}&u_id={$u_id}&utype={$utype}')">
					<img src="images/{$SelectCmsArr[RowCms].active_img}" width="15" height="14" border="0" alt="{$SelectCmsArr[RowCms].active_alt}" /></a></td>
					<td width="10%" align="center" valign="middle">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
						  <td width="60" height="39" align="center" valign="middle" class="rightBorder" >
						  <a href="video_update.php?video_id={$SelectCmsArr[RowCms].video_id}&u_id={$u_id}&utype={$utype}">
						  <img src="images/edit_dark.gif" alt="" width="15" height="14" border="0" />
						  </a>
						  </td>
						  <td width="50%" align="center" valign="middle">
						  <a href="#" onClick="ConfirmDelete('{$page_name}?action=del&{$PreserveLink}&u_id={$u_id}&utype={$utype}','{$SelectCmsArr[RowCms].video_id}','{$SelectCmsArr[RowCms].video_title_delete}','Video:: ')">
						  <img src="images/delete_icon.gif" border="0" alt="" />
						  </a>
						  </td>
						  </tr>
					</table>					
					</td>
				 </tr>
				  {/section}
			{else}
				<tr bgcolor="#f4f4f5"><td class="plaintxt" colspan="5" align="center">No Record Found</td></tr>
			{/if}       
				  
				 
			  </table>
			</td>
		  </tr>
		 
  </table>
</div>
<br />
<div>	{if $pagination_arr[3] > 0}
   <table>
	  <tr>
        <td height="30" valign="middle" align="center">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td align="left" class="plaintxt">					
					{if $pagination_arr[2] neq ""}					
						{$pagination_arr[2]}
					{/if}					
					</td>
					<td align="right" class="plaintxt">					   	
					{if $pagination_arr[1] neq ""}					
						{$pagination_arr[1]}
					{/if}					
					</td>
				</tr>
			</table>
		</td>
      </tr>
	  </table>
	{/if} </div><br /><br /><br /><br /><br />
</div>

{if $IsProcess neq "Y"}  	
{include file="bottom.tpl"}
{/if}
