{literal}
<script type="text/javascript">
function focusit(id,text){
val=document.getElementById(id).value;
if(val==text)
	document.getElementById(id).value='';
}
function blurit(id,text){
val=document.getElementById(id).value;
if(val=='')
	document.getElementById(id).value=text;
}
</script>	
{/literal}
{include file="top.tpl"}
<h2>{$SubmitButton}</h2>
<div class="tabarea">
<div class="tabcontent">
<p align="right" class="fields">Fields marked with <span class="red">*</span> are required</p>
<form action="ads_update.php?action=trans" method="post" name="frm_admin" id="frm_admin" enctype="multipart/form-data">
		<table width="100%" border="0" cellspacing="4" cellpadding="0">
		  {if $err_msgs neq ""}     
			<tr>
				<td class="simpleText" colspan="2"><ul>{$err_msgs}</ul></td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
			 </tr>
		  {/if}
		 <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		 </tr>
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Ad Position<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
				<select name="ads_position" id="ads_position" class="selectbox" style="height:35px;">
					<option value="">Select Position</option>
					<option value="1" {if $FabricArr.ads_position eq 1} selected="selected" {/if}>Right Panel</option>
					<option value="2" {if $FabricArr.ads_position eq 2} selected="selected" {/if}>Left Panel</option>
				</select>
			</label></td>
		 </tr>
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Ad Type<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
				<select name="ads_type" id="ads_type" class="selectbox" style="height:35px;">
					<option value="">Select Type</option>
					<option value="1" {if $FabricArr.ads_type eq 1} selected="selected" {/if}>Google Ads</option>
					<option value="2" {if $FabricArr.ads_type eq 2} selected="selected" {/if}>Banner</option>
				</select>
			</label></td>
		 </tr>
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Ad Code<span class="red" id="code">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <textarea rows="3" cols="23"  name="ads_code" id="ads_code" class="bigger" style="height:110px;">{$FabricArr.ads_code}</textarea>
			</label></td>
		 </tr>	
 		 <tr>
		  	<td width="30%" align="left" valign="middle" class="plaintxt"></td>
			<td width="70%" align="left" valign="middle" class="plaintxt"><label>Maximum width should be 300pixel</label></td>			
		 </tr>
		 <tr>
		  	<td colspan="2"></td>
		 </tr>
 		 <tr>
			<td align="left" valign="top" class="plaintxt">{$DisplayImageControlStr}</td>
			<td align="left" valign="top" class="plaintxt"></td>
			<td align="left" valign="top" class="plaintxt"></td>
		 </tr>
		 <tr>
		  	<td colspan="2"></td>
		 </tr>
 	   	 <tr>
		  	<td width="30%" align="left" valign="middle" class="plaintxt"></td>
			<td width="70%" align="left" valign="middle" class="plaintxt"><label>Banner Size should be 300x300</label></td>			
		 </tr>
		 <tr>
		  	<td colspan="2"></td>
		 </tr>
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Set Banner Link:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <input type="text"  name="ads_link" id="ads_link" value="{$FabricArr.ads_link}" class="textbox-a search-box" style="width:250px;" />
			</label></td>
		 </tr>	
 	   	 <tr>
		  	<td width="30%" align="left" valign="middle" class="plaintxt"></td>
			<td width="70%" align="left" valign="middle" class="plaintxt"><label>Link format should be like this http://www.chirongaming.com</label></td>			
		 </tr>
		 <tr>
		  	<td colspan="2"></td>
		 </tr>
		 <tr>
			<td height="28" align="left" class="plaintxt">Set Validity:<span class="red">*</span></td>
			<td height="28" align="left" class="maintext">
			<table>
			<tr>
			<td><input class="datebox" style="width:80px;" name="ads_start_date" id="ads_start_date" readonly type="text" size="10" value="{$FabricArr.ads_start_date}" onfocus="focusit('ads_start_date','Start Date');" onblur="blurit('ads_start_date','Start Date');" /><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frm_admin.ads_start_date,'');return false;" HIDEFOCUS><img src="images/icon_calender.gif" alt="" border="0" /></a></td>
			<td><input class="datebox" style="width:80px;" name="ads_end_date" id="ads_end_date" readonly type="text" size="10" value="{$FabricArr.ads_end_date}" onfocus="focusit('ads_end_date','End Date');" onblur="blurit('ads_end_date','End Date');" /><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fEndPop(document.frm_admin.ads_end_date,document.frm_admin.ads_end_date);return false;" HIDEFOCUS><img src="images/icon_calender.gif" alt="" border="0" /></a>
		  </td>
		  	</tr>
			</table>
			</td>
		 </tr>
		 <tr>
			<td height="28" align="left" class="plaintxt">Active:</td>
			<td height="28" align="left" class="maintext">
				<input name="is_active" id="is_active" type="checkbox" class="maintext" value="Y" {if $FabricArr.is_active eq 'Y'}checked{/if}>
			</td>
		 </tr>
		 <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		 </tr>
		 <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">
			 <input name="ads_id" id="ads_id" type="hidden" value="{$FabricArr.ads_id}"/>
			 <input name="imageField" type="submit" class="addsadmin" value="{$SubmitButton}" {if $image_id eq ''} style="width:150px;"{else}style="width:150px;"{/if}/>
&nbsp;&nbsp;&nbsp;<input name="" type="button" class="cancel" value="Cancel" onclick="window.location.href='ads_manager.php?IsPreserved=Y'"/></td>
		 </tr>
		 <tr>
		 	<td>&nbsp;</td>
		 	<td>&nbsp;</td>
		 </tr>
		</table>
</form>

	</div>
 </div>
    <p>&nbsp;</p>
{include file="bottom.tpl"}
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>