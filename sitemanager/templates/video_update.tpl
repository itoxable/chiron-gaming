{include file="top.tpl"}
{literal}
<script language="javascript" type="text/javascript">
j(document).ready(function(){
	getPageId('{/literal}{$FabricArr.game_id}{literal}','{/literal}{$FabricArr.ladder_id}{literal}');
});

function getPageId(type,id)
{	
	
	j.ajax({
			type:'POST',
			url:'get_game_ladder.php',
			data: 'type='+type+'&id='+id,
			dataType:'json',
			success:function(jData){
			
				if(jData.flag == 1)
				{
					j('#ladder_id_td').html(jData.html);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				j(".contactus-block").html("Some champion error occoured. Please report to system administrator.</div>");
			}
		});
}

function checking()
{
var opt=jQuery("#is_video_url").val();
	if(opt=='N')
	{
		jQuery("#url_star").html('');
		jQuery("#check_doc").html('*');
		jQuery("#check_img").html('*');
		jQuery("#doc_up").html('');
		jQuery("#img_up").html('');
	}
	else
	{
		jQuery("#url_star").html('*');
		jQuery("#check_doc").html('');
		jQuery("#check_img").html('');
		jQuery("#doc_up").html('');
		jQuery("#img_up").html('');
	}
}
function getChangeGame(id)
{	
	//alert(id);
	j('#ladder').hide();
	j('#versus').hide();
	j('#type').hide();
	j('#champion').hide();
	j('#team').hide();
	j('#map').hide();
	j('#mode').hide();
	j('#class').hide();
	j('#ladder_gap').hide();
	j('#versus_gap').hide();
	j('#type_gap').hide();
	j('#champion_gap').hide();
	j('#team_gap').hide();
	j('#map_gap').hide();
	j('#mode_gap').hide();
	j('#class_gap').hide();
	j('#champion_gap').hide();
	j.ajax({
			url:'game_details.php?is_vedio=1',
			data: 'id='+id,
			type:'post',		
			dataType:'json',	
		   success:function(resp){
		   //alert(resp.map);
		   if(resp.ladder=='Y')
		   {
		   		j('#ladder').show();
				j('#ladder_gap').show();
			}			
			 if(resp.versus=='Y')
			 {
		   		/*j('#versus').css('display','block');
				j('#versus').html(resp.versus_str);*/
				j('#versus').show();
				j('#versus_gap').show();
			}	
			 if(resp.type=='Y')
		   	{
				j('#type').show();				
				j('#type_gap').show();
			}	
			 if(resp.champion=='Y')
		   	{
				j('#champion').show();
				j('#champion_gap').show();
			}	
			 if(resp.team=='Y')
		   	{
				j('#team').show();
				j('#team_gap').show();
			}
			 if(resp.map=='Y')
		   	{
				j('#map').show();
				j('#map_gap').show();
			}
			 if(resp.mode=='Y')
		   	{
				j('#mode').show();
				j('#mode_gap').show();
			}
			 if(resp.class=='Y')
		   	{
				j('#class').show();
				j('#class_gap').show();
			}
				}
		});
}
function keyCheck(eventObj, obj)
{
    var keyCode
 
    // Check For Browser Type
    if (document.all){
        keyCode=eventObj.keyCode
    }
    else{
        keyCode=eventObj.which
    }
 
    var str=obj.value
 
    if(keyCode==46){
        if (str.indexOf(".")>0){
            return false
        }
    }
 
    if((keyCode<46 || keyCode >57)   &&   (keyCode >31)){ // Allow only integers and decimal points
        return false
    }
    
    return true
}
</script>
{/literal}
<h2>{$SubmitButton}</h2>
<div class="tabarea">
<div class="tabcontent">
<p align="right" class="fields">Fields marked with <span class="red">*</span> are required</p>
<form action="video_update.php?action=trans" method="post" name="frm_admin" id="frm_admin" enctype="multipart/form-data">
		<table width="100%" border="0" cellspacing="4" cellpadding="0">
		  {if $err_msgs neq ""}     
			<tr>
				<td class="simpleText" colspan="2"><ul>{$err_msgs}</ul></td>
			</tr>
			<tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		  {/if}
		   <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Game<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			 <select name="game_id" id="game_id" class="selectbox" onchange="getChangeGame(this.value)">
				<option value="">Select</option>
				
				{html_options options=$GameArr selected=$FabricArr.game_id}
			</select>
			</label></td>
		 </tr>
		 <!--<tr>
			<td height="28" align="left" class="plaintxt">Ladder<span class="red">*</span>:</td>
			<td height="28" align="left" class="maintext" id="ladder_id_td">
				<select name="ladder_id" id="ladder_id">
					<option value="">Select</option>
				</select>
			</td>
		 </tr>-->
		
		  {if $FabricArr.is_ladder eq 'Y'}
		   <tr id="ladder_gap">
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		  {/if}
		 <tr id="ladder" {if $FabricArr.is_ladder neq 'Y'} style='display:none' {/if}>
			<td height="28" align="left" class="plaintxt">Ladder:</td>
			<td height="28" align="left" class="maintext" >
				<select name="ladder_id" id="ladder_id">
					<option value="">Select</option>
					{html_options options=$LadderArr selected=$FabricArr.ladder_id}
				</select>
			</td>
		 </tr>
		  <tr id="versus_gap" {if $FabricArr.is_versus neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>

		
		  <tr id="versus"   {if $FabricArr.is_versus neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Versus:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $VersusArr|@count neq 0}
		 	{section name=RowVersus loop=$VersusArr}
			<input type="checkbox" name="submited_versus[]"  value="{$VersusArr[RowVersus].versus_id}" 
{if $VersusArr[RowVersus].versus_id|inarray:$submited_versus}checked{/if} /> 
			{$VersusArr[RowVersus].versus_title}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		  <tr id="team_gap" {if $FabricArr.is_team neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		
		  <tr id="team" {if $FabricArr.is_team neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Team:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $TeamArr|@count neq 0}
		 	{section name=RowTeam loop=$TeamArr}
			<input type="checkbox" name="submited_team[]"  value="{$TeamArr[RowTeam].team_id }" 
{if $TeamArr[RowTeam].team_id|inarray:$submited_team}checked{/if} /> 
			{$TeamArr[RowTeam].team_title}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		
		  <tr id="type_gap" {if $FabricArr.is_type neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		 
		 <tr id="type" {if $FabricArr.is_type neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Type:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $TypeArr|@count neq 0}
		 	{section name=RowType loop=$TypeArr}
			<input type="checkbox" name="submited_type[]"  value="{$TypeArr[RowType].type_id }" 
{if $TypeArr[RowType].type_id|inarray:$submited_type}checked{/if} /> 
			{$TypeArr[RowType].type_title}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		
		  <tr id="champion_gap" {if $FabricArr.is_champion neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
	
		 <tr id="champion" {if $FabricArr.is_champion neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Champion:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $ChampionArr|@count neq 0}
		 	{section name=RowChampion loop=$ChampionArr}
			<input type="checkbox" name="submited_champion[]"  value="{$ChampionArr[RowChampion].champion_id}" 
{if $ChampionArr[RowChampion].champion_id|inarray:$submited_champion}checked{/if} /> 
			{$ChampionArr[RowChampion].champion_title}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		
		 <tr id="class_gap" {if $FabricArr.is_class neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
	
		 <tr id="class" {if $FabricArr.is_class neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Class:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $ClassArr|@count neq 0}
		 	{section name=RowClass loop=$ClassArr}
			<input type="checkbox" name="submited_class[]"  value="{$ClassArr[RowClass].class_id}" 
{if $ClassArr[RowClass].class_id|inarray:$submited_class}checked{/if} /> 
			{$ClassArr[RowClass].class_title}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		
		 <tr id="map_gap" {if $FabricArr.is_map neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
	
		 <tr id="map" {if $FabricArr.is_map neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Map:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $MapArr|@count neq 0}
		 	{section name=RowMap loop=$MapArr}
			<input type="checkbox" name="submited_map[]"  value="{$MapArr[RowMap].map_id}" 
{if $MapArr[RowMap].map_id|inarray:$submited_map}checked{/if} /> 
			{$MapArr[RowMap].map_title}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		
		 <tr id="mode_gap" {if $FabricArr.is_mode neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
	
		 <tr id="mode" {if $FabricArr.is_mode neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Mode:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $ModeArr|@count neq 0}
		 	{section name=RowMode loop=$ModeArr}
			<input type="checkbox" name="submited_mode[]"  value="{$ModeArr[RowMode].mode_id}" 
{if $ModeArr[RowMode].mode_id|inarray:$submited_mode}checked{/if} /> 
			{$ModeArr[RowMode].mode_title}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		
		   <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		 
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Video Title<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <input type="text" name="video_title" id="video_title" class="textbox" value="{$FabricArr.video_title}"/>
			</label></td>
		 </tr>
		 

		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Video Type<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			<select name="is_video_url" id="is_video_url" onchange="checking();">
			<option value='Y' {if $FabricArr.is_video_url eq 'Y'}selected{/if}> URL</option>
			<option value='N' {if $FabricArr.is_video_url eq 'N'}selected{/if}>Upload Video</option>
			</select>
			
			</label></td>
		 </tr>

		 <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		 
		  <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Youtube Url{if $url_link eq 'Y' || $video_id eq '' && $action neq 'trans'}<span class="red" id="url_star">*</span>{else}<span class="red" id="url_star"></span>{/if}:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <input type="text" name="video_url" id="video_url" class="textbox" value="{$FabricArr.video_url}"/>
			</label></td>
		 </tr>

		 <tr><td valign="top" align="left" class="plaintxt">
			</td><td valign="top" align="left" class="plaintxt">
			<div class="msgbox">
			[i.e https://www.youtube.com/watch?v=Di7G4nbpyDg].
			</div>
			</td>
		 </tr>
		
		<tr>
			<td align="left" valign="top" class="plaintxt">{$DisplayVideoControlStr}</td>
			<td align="left" valign="top" class="plaintxt"></td>
			<td align="left" valign="top" class="plaintxt">
			</td>
		 </tr>

		  </tr>
		
			<tr>
			<td align="left" valign="top" class="plaintxt">{$DisplayImageControlStr}<span class="red" id="img_star"></span></td>
			<td align="left" valign="top" class="plaintxt"></td>
			<td align="left" valign="top" class="plaintxt">
			</td>
		 </tr>
		  <tr>
			<td width="20%" align="left" valign="top" class="plaintxt">Time Length (min):</td>
			<td width="80%" align="left" valign="middle" class="plaintxt"><label>
			<input type="text" name="time_length" id="time_length" class="textbox" value="{$FabricArr.time_length}" onkeypress="return keyCheck(event, this);"/>
			</label></td>
	    </tr>
        <tr><td valign="top" align="left" class="plaintxt">
			</td><td valign="top" align="left" class="plaintxt">
			<div class="msgbox">
			[Format will be   mm.ss].
			</div>
			</td>
		 </tr>
		  <tr>
			<td align="left" valign="top" class="plaintxt">Description:</td>
			<td align="left" valign="top"><label>
			  <textarea rows="3" cols="23"  name="description" id="description" class="bigger" style="height:110px;">{$FabricArr.description}</textarea>
			</label></td>
	    </tr>
		
		  
		
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">
			 <input name="video_id" id="video_id" type="hidden"  value="{$video_id}"/>
			 <input name="u_id" id="u_id" type="hidden" value="{$u_id}"/>
			 <input name="utype" id="utype" type="hidden" value="{$utype}"/>
			 <input name="imageField" type="submit" class="addsadmin" value="{$SubmitButton}" {if $video_id eq ''} style="width:120px;"{else}style="width:145px;"{/if}/>
&nbsp;&nbsp;&nbsp;<input name="" type="button" class="cancel" value="Cancel" onclick="window.location.href='video_manager.php?u_id={$u_id}&utype={$utype}&IsPreserved=Y'"/></td>
		  </tr>
		  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  </tr>
		</table>
</form>

	</div>
 </div>
    <p>&nbsp;</p>
{include file="bottom.tpl"}