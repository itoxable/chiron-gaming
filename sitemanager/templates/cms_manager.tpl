{if $IsProcess neq "Y"}
	{include file="top.tpl"}
{literal}
<script type="text/javascript">
var j = jQuery.noConflict();
j(document).ready(function(){
		
focus_blur('Search_cms_title','Search by CMS Title');

});

</script>
{/literal}
<h2>CMS <div id="TransMsgDisplay" align="right" style="height:3px" ></div></h2>
	
	<form method="POST" name="form_search" id="form_search" action="{$page_name}" onsubmit="return false;">
		<table width="80%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="56%" align="right" valign="middle">	
				
				<input size="30" type="text" name="Search_cms_title" id="Search_cms_title" value="{$Search_cms_title}" class="textbox" onfocus="this.value=''"/>	
				<input type="hidden" name="dosearch" value="GO">
				</td>
				
				<td align="left" valign="middle"><input  name="submitdosearch" value="GO" type="button" class="go" onClick="ManagerGeneralForm('{$page_name}?action=list_search','form_search')" />
				<input name="" type="button"  class="reset" value="RESET" onClick="window.location.href='{$page_name}';"/></td>
			</tr>
		
		</table>
	</form>
	<!--<div align="right" class="addNews_secand"><a href="cms_update.php">Add CMS</a></div>-->		
{/if}	
<div id="records_listing">
{if $MessgReportText neq ""}<div align="left" class="successful" style="color:#FF0000; margin:25px 0 0 0;">{$MessgReportText}</div>
{/if}
<br /><br />
<div class="newsName">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td colspan="6" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#9f9f9f">
				  <tr  bgcolor="#9f9f9f">
					<td  align="left" width="60%" valign="middle" class="padleft">
						<a href="#"  onClick="ManagerGeneral('{$page_name}?action=list_order&do_order=GO&OrderByID=1&OrderType={$OrderType}&{$SearchLink}&from={$from}')" >								
							<span class="whitetext"><strong>CMS Title</strong></span>									
							{$ReturnSortingArr.DisplaySortingImage[1]}				
						</a>	
					</td>
					<!--<td width="15%" align="center" valign="middle" >
						<a href="#"  onClick="ManagerGeneral('{$page_name}?action=list_order&do_order=GO&OrderByID=2&OrderType={$OrderType}&{$SearchLink}&from={$from}')" >
							<span class="whitetext"><strong>Date Added</strong></span>
							{$ReturnSortingArr.DisplaySortingImage[2]}			
						</a>	
					</td>-->
					<td width="15%" align="center" valign="middle" class="padleft">
						<a href="#"  onClick="ManagerGeneral('{$page_name}?action=list_order&do_order=GO&OrderByID=3&OrderType={$OrderType}&{$SearchLink}&from={$from}')" >
							<span class="whitetext"><strong>Date Edited</strong></span>
							{$ReturnSortingArr.DisplaySortingImage[3]}			
						</a>	
					</td>
							
					<td width="10%" class="whitetext" align="center" valign="middle"><strong>Actions</strong></td>
				  </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#878787">
				{if $NumSelectCms > 0}
			{section name=RowCms loop=$SelectCmsArr}
				  <tr {if ($templatelite.section.RowCms.index % 2) eq 0} bgcolor="#ffffff" {else} bgcolor="#ffffff" {/if}>
					<td  align="left" width="60%" valign="middle" class="padleft">
					<span class="simpletxt">{$SelectCmsArr[RowCms].cms_title}</span>
					</td>
					<!--<td width="15%" class="simpletxt" align="center" valign="middle">{$SelectCmsArr[RowCms].date_added}</td>-->
					<td width="15%" class="simpletxt" align="center" valign="middle">{$SelectCmsArr[RowCms].date_edited}</td>
					
					<td width="10%" align="center" valign="middle">
					  <a href="cms_update.php?cms_id={$SelectCmsArr[RowCms].cms_id}">
					  <img src="images/edit_dark.gif" alt="" width="15" height="14" border="0" />
					  </a>						
					</td>
				 </tr>
				  {/section}
			{else}
				<tr bgcolor="#ffffff"><td class="plaintxt" colspan="5" align="center">No Record Found</td></tr>
			{/if}       
				  
				 
			  </table>
			</td>
		  </tr>
		 
  </table>
</div>
<br />
<div>	{if $pagination_arr[3] > 0}
   <table>
	  <tr>
        <td height="30" valign="middle" align="center">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td align="left" class="plaintxt">					
					{if $pagination_arr[2] neq ""}					
						{$pagination_arr[2]}
					{/if}					
					</td>
					<td align="right" class="plaintxt">					   	
					{if $pagination_arr[1] neq ""}					
						{$pagination_arr[1]}
					{/if}					
					</td>
				</tr>
			</table>
		</td>
      </tr>
	  </table>
	{/if} </div><br /><br /><br /><br /><br />
</div>

{if $IsProcess neq "Y"}  	
{include file="bottom.tpl"}
{/if}
