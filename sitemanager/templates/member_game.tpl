{include file="top.tpl"}
{literal}
<script type="text/javascript" src="../js/jquery-1.4.4.min.js"></script>

<link rel="stylesheet" href="../style/autosuggest.css" type="text/css" />
<script type="text/javascript" src="../js/jquery.ausu-autosuggest.min.js"></script>
<script language="javascript" type="text/javascript">
 j().ready(function() {  
		
		 $.fn.autosugguest({  
				   className: 'ausu-suggest',
				  methodType: 'POST',
					minChars: 2,
					  rtnIDs: true,
					dataFile: 'autocomplete.php?action=populateCity'
			});  
   });  
function getPageId(type,id,rid,sid)
{	
	
	j.ajax({
			type:'POST',
			url:'get_game_ladder.php',
			data: 'type='+type+'&id='+id+'&rid='+rid+'&sid='+sid,
			dataType:'json',
			success:function(jData){
			
				if(jData.flag == 1)
				{
					j('#ladder_id_td').html(jData.html);
					j('#race_id_td').html(jData.html1);
					j('#server_id_td').html(jData.html2);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				j(".contactus-block").html("Some server error occoured. Please report to system administrator.</div>");
			}
		});
}

function getChangeGame(id)
{	
	//alert(id);
	j('#ladder').hide();
	j('#race').hide();
	j('#region').hide();
	j('#server').hide();
	j('#rating').hide();
	j('#ladder_gap').hide();
	j('#race_gap').hide();
	j('#region_gap').hide();
	j('#server_gap').hide();
	j('#rating_gap').hide();
	j.ajax({
			url:'game_details.php',
			data: 'id='+id,
			type:'post',		
			dataType:'json',	
		   success:function(resp){
		   //alert(resp.server);
		   if(resp.ladder=='Y')
		   {
		   		j('#ladder').show();
				j('#ladder_gap').show();
			}			
			 if(resp.race=='Y')
			 {
		   		/*j('#race').css('display','block');
				j('#race').html(resp.race_str);*/
				j('#race').show();
				j('#race_gap').show();
			}	
			 if(resp.region=='Y')
		   	{
				j('#region').show();				
				j('#region_gap').show();
			}	
			 if(resp.server=='Y')
		   	{
				j('#server').show();
				j('#server_gap').show();
			}	
			 if(resp.rating=='Y')
		   	{
				j('#rating').show();
				j('#rating_gap').show();
			}
				}
		});
}
function showLocal()
{
     if(document.getElementById('avail_L').checked == true)
	  {  
		j('#avail_city').show();
		j('#avail_country').show();	
	 }
	if(document.getElementById('avail_L').checked == false)
	 {  	
	   j('#avail_city').hide();
	   j('#avail_country').hide();	
	 }		
}

function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

</script>
{/literal}

<h2>Add Game</h2>
<div class="tabarea">
<div class="tabcontent">
<p align="right" class="fields">Fields marked with <span class="red">*</span> are required</p>
<form action="member_game.php?action=trans" method="post" name="frm_admin" id="frm_admin" >
<input type="hidden" name="u_id" value="{$u_id}" />
<input type="hidden" name="user_type" value="{$user_type}">
		<table width="100%" border="0" cellspacing="4" cellpadding="0">
		  {if $err_msgs neq ""}     
			<tr>
				<td class="simpleText" colspan="2"><ul>{$err_msgs}</ul></td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
			 </tr>
		  {/if}
		  {if $user_type eq '1'}
		      <tr>
				<td width="30%" align="left" valign="middle" class="plaintxt">Paypal Email ID<span class="red">*</span>:</td>
				<td width="70%" valign="middle" align="left"><label>
				<input type="text" name="paypal_email_id" id="paypal_email_id"  class="textbox">
				</label></td>
			 </tr>
		  {/if}	
		  {if ($is_coach eq '1' && $is_partner eq '0') || ($is_coach eq '0' && $is_partner eq '1')}
		    <tr>
				<td width="20%" align="left" valign="top" class="plaintxt">Availability<span class="red">*</span>:</td>
				<td width="80%" align="left" valign="middle" class="plaintxt">
				Local&nbsp;&nbsp;
				<input name="availability_type1" type="checkbox" value="L" id="avail_L" {if $FabricArr.availability_type eq 'L'}checked=checked{/if} onclick="showLocal()"/>
				Online&nbsp;&nbsp;<input name="availability_type2" type="checkbox" value="O" {if $FabricArr.availability_type eq 'O'}checked=checked{/if}/>
				</td>
			  </tr>
			  <tr>
			 
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
			 </tr>
			 <tr id="avail_city"  {if $FabricArr.availability_type =='L'} {else} style='display:none' {/if}>
				<td width="20%" align="left" valign="top" class="plaintxt">City (Local meet-up)<span class="red">*</span>:</td>
				<td width="80%" align="left" valign="middle"><label><div class="ausu-suggest">
				<input type="text" name="acity_name" id="acity_name" class="textbox" value="{$FabricArr.acity_name}" autocomplete="off"  />
				<input name="availability_city" id="availability_city" type="hidden"  value="{$FabricArr.availability_city}" autocomplete="off" />
				</div>
				</label></td>
			 </tr>
			  
		   {/if}	 
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Game<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			 <select name="game_id" id="game_id" class="selectbox" onchange="getChangeGame(this.value)">
				<option value="">Select</option>				
				{html_options options=$GameArr selected=$FabricArr.game_id}
			</select>
			</label></td>
		 </tr>
		 {if $user_type eq '1'}
		 <tr>
			<td width="20%" valign="top" align="left" class="plaintxt">Rate<span class="red">*</span> ($):</td>
			<td width="80%" valign="middle" align="left"><label>
			<input type="text" onkeypress="return floatnumbersonly(this,event)" value="{$FabricArr.rate}" class="textbox" id="rate" name="rate">
			</label></td>
		 </tr>
		{/if} 
		 {if $user_type eq '3'}
		 <tr>
			<td width="20%" valign="top" align="left" class="plaintxt">Peak Hours :</td>
			<td width="80%" valign="middle" align="left"><label>
			<input type="text" value="{$FabricArr.peak_hours}" class="textbox" id="peak_hours" name="peak_hours">
			</label></td>
		 </tr>
		 {/if} 
		 {if $FabricArr.is_ladder eq 'Y'}
		   <tr id="ladder_gap">
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		  {/if}
		 <tr id="ladder" {if $FabricArr.is_ladder neq 'Y'} style='display:none' {/if}>
			<td height="28" align="left" class="plaintxt">Ladder:</td>
			<td height="28" align="left" class="maintext" id="ladder_id_td">
				<select name="ladder_id" id="ladder_id">
					<option value="">Select</option>
					{html_options options=$LadderArr selected=$FabricArr.ladder_id}
				</select>
			</td>
		 </tr>
		<!-- <tr id="ladder" {if $FabricArr.is_ladder neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Ladder<span class="red">*</span>:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $LadderArr|@count neq 0}
		 	{section name=RowLadder loop=$LadderArr}
			<input type="checkbox" name="submited_ladder[]"  value="{$LadderArr[RowLadder].ladder_id}" 
{if $LadderArr[RowLadder].ladder_id|inarray:$submited_ladder}checked{/if} /> 
			{$LadderArr[RowLadder].ladder_name}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>-->
		
		  <tr id="race_gap" {if $FabricArr.is_race neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>

		<!-- <tr>
			<td height="28" align="left" class="plaintxt">Race<span class="red">*</span>:</td>
			<td height="28" align="left" class="maintext" id="race_id_td">
				<select name="race_id" id="race_id">
					<option value="">Select</option>
				</select>
			</td>
		 </tr>-->
		  <tr id="race"   {if $FabricArr.is_race neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Race:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $RaceArr|@count neq 0}
		 	{section name=RowRace loop=$RaceArr}
			<input type="checkbox" name="submited_race[]"  value="{$RaceArr[RowRace].race_id}" 
{if $RaceArr[RowRace].race_id|inarray:$submited_race}checked{/if} /> 
			{$RaceArr[RowRace].race_title}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		  <tr id="rating_gap" {if $FabricArr.is_rating neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		 
		 <!--<tr>
			<td height="28" align="left" class="plaintxt">Server<span class="red">*</span>:</td>
			<td height="28" align="left" class="maintext" id="server_id_td">
				<select name="server_id" id="server_id">
					<option value="">Select</option>
				</select>
			</td>
		 </tr>-->
		  <tr id="rating" {if $FabricArr.is_rating neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Rating:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $RatingArr|@count neq 0}
		 	{section name=RowRating loop=$RatingArr}
			<input type="checkbox" name="submited_rating[]"  value="{$RatingArr[RowRating].rating_id }" 
{if $RatingArr[RowRating].rating_id|inarray:$submited_rating}checked{/if} /> 
			{$RatingArr[RowRating].rating_title}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		
		  <tr id="region_gap" {if $FabricArr.is_region neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		 
		 <tr id="region" {if $FabricArr.is_region neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Region:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $RegionArr|@count neq 0}
		 	{section name=RowRegion loop=$RegionArr}
			<input type="checkbox" name="submited_region[]"  value="{$RegionArr[RowRegion].region_id }" 
{if $RegionArr[RowRegion].region_id|inarray:$submited_region}checked{/if} /> 
			{$RegionArr[RowRegion].region_title}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		
		  <tr id="server_gap" {if $FabricArr.is_server neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
	
		 <tr id="server" {if $FabricArr.is_server neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Server:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $ServerArr|@count neq 0}
		 	{section name=RowServer loop=$ServerArr}
			<input type="checkbox" name="submited_server[]"  value="{$ServerArr[RowServer].server_id}" 
{if $ServerArr[RowServer].server_id|inarray:$submited_server}checked{/if} /> 
			{$ServerArr[RowServer].server_name}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		   <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		 <!--tr>
			<td width="20%" align="left" valign="top" class="plaintxt">Experience:</td>
			<td width="80%" align="left" valign="middle"><label>
			<input type="text" name="experience" id="experience" class="textbox" value="{$FabricArr.experience}"/>
			</label></td>
	     </tr-->

          <tr>
			<td align="left" valign="top" class="plaintxt">Experience:</td>
			<td align="left" valign="top"><label>
			  <textarea rows="3" cols="23"  name="experience" id="experience" class="bigger" style="height:110px;">{$FabricArr.experience}</textarea>
			</label></td>
		  </tr>
		  <tr>
		  	<td colspan="2"></td>
          </tr>
		  {if $user_type eq '1'}
		  <tr>
			<td width="20%" valign="top" align="left" class="plaintxt">Lesson Plan:</td>
			<td width="80%" valign="middle" align="left"><label>
			<textarea rows="3" cols="23"  name="lesson_plan" id="lesson_plan" class="bigger" style="height:110px;">{$FabricArr.lesson_plan}</textarea>
			</label></td>
		  </tr>
		 {/if} 
	 	 {if $user_type eq '3'}
		  <tr>
			<td width="20%" valign="top" align="left" class="plaintxt">Need Improvement In:</td>
			<td width="80%" valign="middle" align="left"><label>
			<textarea rows="3" cols="23"  name="need_improvement" id="need_improvement" class="bigger" style="height:110px;">{$FabricArr.need_improvement}</textarea>
			</label></td>
		  </tr>
		 {/if}
		 {if ($is_coach eq '1' && $is_partner eq '0') || ($is_coach eq '0' && $is_partner eq '1')}
		  <tr>
			<td align="left" valign="top" class="plaintxt">About User:</td>
			<td align="left" valign="top" class="plaintxt"><label>
			<textarea rows="3" cols="23"  name="description" id="description"  onKeyDown="limitText(this.form.description,this.form.countdown,300);"
			onKeyUp="limitText(this.form.description,this.form.countdown,300);" class="bigger" style="height:110px;">{$FabricArr.description}</textarea>
			</label>
			</td>
		
			 </tr>
			 <tr>
					<td align="left" valign="top" class="plaintxt">&nbsp;</td>
					<td align="left" valign="top" class="plaintxt"><label> 
					Characters left <input readonly type="text" name="countdown" size="3" value="300" class="tiny">
					</label></td>
			</tr>	
		 {/if}		   
		  <tr>
			<td height="28" align="left" class="plaintxt">Active:</td>
			<td height="28" align="left" class="maintext">
				<input name="is_active" id="is_active" type="checkbox" class="maintext" value="Y" {if $FabricArr.is_active =='Y'}checked{/if}>
			</td>
		  </tr>
		 
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">
			 <input name="user_game_id" id="user_game_id" type="hidden" value="{$user_game_id}"/>
			 <input name="u_id" id="u_id" type="hidden" value="{$u_id}"/>
			 <input name="imageField" type="submit" class="addsadmin" value="Submit" style="width:120px;"/></td>
		  </tr>
		  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  </tr>
		</table>
</form>

	</div>
 </div>
    <p>&nbsp;</p>
{include file="bottom.tpl"}
