{include file="top.tpl"}
{literal}
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script language="javascript" type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "exact",
		elements:"news_description",
		theme : "advanced",
		plugins : "table,save,advhr,advimage,advlink,insertdatetime,searchreplace,contextmenu,paste,directionality",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,zoom,separator,forecolor,backcolor",
		theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
		theme_advanced_buttons3_add_before : "tablecontrols,separator",
		theme_advanced_buttons3_add : "advhr",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		content_css : "example_word.css",
	    plugi2n_insertdate_dateFormat : "%Y-%m-%d",
	    plugi2n_insertdate_timeFormat : "%H:%M:%S",
		external_image_list_url : "image_list.js.php",
		file_browser_callback : "tinyBrowser",
		paste_use_dialog : false,
		theme_advanced_resizing : true,
		theme_advanced_resize_horizontal : false,
		theme_advanced_link_targets : "_something=My somthing;_something2=My somthing2;_something3=My somthing3;",
		paste_auto_cleanup_on_paste : true,
		paste_convert_headers_to_strong : false,
		paste_strip_class_attributes : "all",
		paste_remove_spans : false,
		paste_remove_styles : false
	});
</script>
{/literal}
<h2>{$SubmitButton}</h2>
<div class="tabarea">
<div class="tabcontent">
<p align="right" class="fields">Fields marked with <span class="red">*</span> are required</p>
<form action="news_update.php?action=trans" method="post" name="frm_admin" id="frm_admin" enctype="multipart/form-data">
		<table width="100%" border="0" cellspacing="4" cellpadding="0">
		  {if $err_msgs neq ""}     
			<tr>
				<td class="simpleText" colspan="2"><ul>{$err_msgs}</ul></td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
			 </tr>
		  {/if}
		 
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">News Title<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <input type="text" name="news_title" id="news_title" class="textbox" value="{$FabricArr.news_title}"/>
			</label></td>
		 </tr>	
		 
		 <!--tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">News Image :</td>
			<td align="left" valign="top">
			{if $SubmitButton eq " Add News "}
			<img src="../uploaded/news_images/Image_Upload.png" height="42" width="57" />
			{elseif $SubmitButton eq "Update News " and $FabricArr.photo eq ""}
			<img src="../uploaded/news_images/Image_Upload.png" height="42" width="57" />
			{else}
			<img src="../uploaded/news_images/{$FabricArr.photo}" height="42" width="57" />
			{/if}
			</td>	
			<td></td>	
		 </tr-->
		 <tr>
			{$DisplayImageControlStr}
        </tr>
		 
		 <!--tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">&nbsp;</td>
			<td align="left" valign="top">
			<input type="file" name="photo" id="photo"/>
			</td>			
		 </tr-->	
		 
		<tr>
			<td align="left" valign="top" class="plaintxt">Summary:</td>
			<td align="left" valign="top"><label>
			  <textarea rows="3" cols="23"  name="news_content" id="news_content" class="bigger" style="height:110px;">{$FabricArr.news_content}</textarea>
			</label></td>
		  </tr>
		  <tr>
		  	<td colspan="2"></td>
		  </tr>
		 <tr>
			<td align="left" valign="top" class="plaintxt">News Description:</td>
			<td>&nbsp;</td>
		 </tr>
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" height="8">&nbsp;</td>
		 </tr>
		 <tr>
			<td align="left" valign="top" colspan="2" class="simpleText">
			<textarea name="news_description" id="news_description" >{$FabricArr.news_description}</textarea>
			</td>
		 </tr>
		 <tr>
		  	<td colspan="2"></td>
		  </tr>
		 <tr>
			<td height="28" align="left" class="plaintxt">Active:</td>
			<td height="28" align="left" class="maintext">
				<input name="is_active" id="is_active" type="checkbox" class="maintext" value="Y" 
				{if $FabricArr.is_active =='Y'}checked{/if}>				
			</td>
		  </tr>
		 
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">
			 <input name="news_id" id="news_id" type="hidden" value="{$news_id}"/>
			 <input name="imageField" type="submit" class="addsadmin" value="{$SubmitButton}" />
&nbsp;&nbsp;&nbsp;<input name="" type="button" class="cancel" value="Cancel" 
onclick="window.location.href='news_manager.php?IsPreserved=Y'"/></td>
		  </tr>
		  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  </tr>
		</table>
</form>

	</div>
 </div>
    <p>&nbsp;</p>
{include file="bottom.tpl"}
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>