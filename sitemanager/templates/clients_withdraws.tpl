{if $IsProcess neq "Y"}
{include file="top.tpl"}  

<h2> Withdraws <div id="TransMsgDisplay" align="right" style="height:3px" ></div></h2>
{/if}
<script type="text/javascript" src="js/script.js"></script>
<div id="records_listing" style="color: black;">	
				
	<table id="withdrawsTable">
		<thead>
			<tr>
				<th>Id</th>
				<th>User</th>
				<th>Date</th>
				<th>Points</th>
				<th>Method</th>
				<th>Detail</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	
	<script type="text/javascript">
		loadTable();
	</script>
</div>

{if $IsProcess neq "Y"}  	
{include file="bottom.tpl"}
{/if}
