{if $IsProcess neq "Y"}
{include file="top.tpl"}
{literal}  
<script type="text/javascript">
var j = jQuery.noConflict();
j(document).ready(function(){
		
focus_blur('Search_lesson_title','Search by title');
focus_blur('Search_from_date','From');
focus_blur('Search_to_date','To');
focus_blur('Search_from_date_in','From');
focus_blur('Search_to_date_in','To');
});

function GetTotalIncome(){
	if(document.getElementById('tot_incm').checked==true){
			document.getElementById('show_tot').style.display='block';
	}
	else{
			document.getElementById('show_tot').style.display='none';
	}
}

function GetTotalPrice(){
	if(document.getElementById('tot_price').checked==true){
			document.getElementById('price_tot').style.display='block';
	}
	else{
			document.getElementById('price_tot').style.display='none';
	}
}

function ChangeLessonStatus(lesson_status_id,lesson_id){

	/*j.post("change_status.php", {lesson_status_id:lesson_status_id, lesson_id:lesson_id}, 
		function(data){
		alert(data);
		if(data.msg){
		    j('#msgbox').fadeIn(0);
		    j('#msgbox').html(data.msg);
		    j('#msgbox').fadeOut(5000);
			location.reload();
		}
		if(data.option_list){
			j('#lesson_status_id').html(data.option_list);
			location.reload();
		}
	});*/
	var params ={

		'module': 'chng_status',
		'lesson_status_id': lesson_status_id,
		'lesson_id': lesson_id,
	};

	
	j.ajax({
		type: "POST",
		url:  'change_status.php',
		data: params,
		dataType : 'json',
		success: function(data){
		//alert(data.lesson_status_id);
			if(data.msg != ''){
				j('#msgbox').fadeIn(0);
				j('#msgbox').html(data.msg);
				j('#msgbox').fadeOut(5000);
			}
			if(data.op != ''){
				//alert(data.op);
				j('#lesson_status_id_'+data.lesson_id).html(data.op);
				//location.reload();
			}
        }
      });
}

function Search_Income(formID){


	var frmID='#'+formID;
	
	var params ={
	
		'module': 'contact'
	};

	var paramsObj = j(frmID).serializeArray();

	j.each(paramsObj, function(i, field){

		params[field.name] = field.value;

	});
	
	j.ajax({
		type: "POST",
		url:  'income_search.php',
		data: params,
		dataType : 'json',
		success: function(data){
		//alert(data.IncomeSql);
			if(data.total_in != ''){
				//alert(data.total_in);
				document.getElementById('lable_total').style.display="block";
				j('#total_income').html(data.total_in);
			}
        }
      });


}
</script>
{/literal}
<h2> Lesson <div id="TransMsgDisplay" align="right" style="height:3px" ></div></h2>

	<div style="padding:10px; color:#330000; font-weight:bold; font-size:16px; font-style:italic;">General Search</div>

	<form method="POST" name="form_search" id="form_search" action="{$page_name}" onsubmit="return false;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td width="29%" align="left" valign="middle" style="padding-left:5px;">	
				<input size="30" type="text" name="Search_lesson_title" id="Search_lesson_title" value="{$Search_lesson_title}" class="textbox-a search-box"/>	
		      </td>
				<td colspan="2">&nbsp;</td>
		  </tr>
			 <tr>
		  	  <td width="50%" align="left" valign="middle" style="padding-left:5px;">
				<div style="float:left;">
					<input type="text" name="Search_from_date" id="Search_from_date" class="datebox" value="{$Search_from_date}" style="width:80px;" readonly=""/><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_from_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				</div>
				<div style="float:left; margin-left:10px;">
					<input type="text" name="Search_to_date" id="Search_to_date" class="datebox" value="{$Search_to_date}" style="width:80px;" readonly=""/><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_to_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				</div>
			  </td>
				<td colspan="2" style=" color:#333333;"><table width="50%%" border="0" cellspacing="0" cellpadding="0">
       <tr>
             <th scope="col"><select name="Search_lesson_status" id="Search_lesson_status" class="selectbox">
					<option value="">--Select Status--</option>
					{section name=Rows loop=$SelectStatusArr}
						<option value="{$SelectStatusArr[Rows].lesson_status_id}" {if $SelectStatusArr[Rows].lesson_status_id eq $Search_lesson_status} selected {/if}>{$SelectStatusArr[Rows].lesson_status_title}</option>
					{/section}
				</select></th>
             <th scope="col"><select name="Search_student_id" id="Search_student_id" class="selectbox">
					<option value="">--Select Student--</option>
					{section name=Rows loop=$SelectStudentArr}
						<option value="{$SelectStudentArr[Rows].student_id}" {if $SelectStudentArr[Rows].student_id eq $Search_student_id} selected {/if}>{$SelectStudentArr[Rows].student_name}</option>
					{/section}
				</select></th>
             <th scope="col"><select name="Search_coach_id" id="Search_coach_id" class="selectbox">
					<option value="">--Select Coach--</option>
					{section name=Rows loop=$SelectCoachArr}
						<option value="{$SelectCoachArr[Rows].coach_id}" {if $SelectCoachArr[Rows].coach_id eq $Search_coach_id} selected {/if}>{$SelectCoachArr[Rows].coach_name}</option>
					{/section}
				</select></th>
        </tr>
</table>
</td>
		  </tr>
	  </table>
		 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:#006600 1px solid;">
			  <tr>
				<td width="29%" align="left" valign="middle" style="color:#000000; padding-left:5px;">
				<!--<select name="Search_student_id" id="Search_student_id" class="selectbox">
					<option value="">--Select Student--</option>
					{section name=Rows loop=$SelectStudentArr}
						<option value="{$SelectStudentArr[Rows].student_id}" {if $SelectStudentArr[Rows].student_id eq $Search_student_id} selected {/if}>{$SelectStudentArr[Rows].student_name}</option>
					{/section}
				</select>-->
			  </td>
				<td width="27%" align="left" valign="middle" style="color:#000000;">
				<!--<select name="Search_coach_id" id="Search_coach_id" class="selectbox">
					<option value="">--Select Coach--</option>
					{section name=Rows loop=$SelectCoachArr}
						<option value="{$SelectCoachArr[Rows].coach_id}" {if $SelectCoachArr[Rows].coach_id eq $Search_coach_id} selected {/if}>{$SelectCoachArr[Rows].coach_name}</option>
					{/section}
				</select>-->
			  </td>
			  <td width="44%" colspan="2" align="left" valign="middle" style="color:#000000;">
				<!--<select name="Search_lesson_status" id="Search_lesson_status" class="selectbox">
					<option value="">--Select Status--</option>
					{section name=Rows loop=$SelectStatusArr}
						<option value="{$SelectStatusArr[Rows].lesson_status_id}" {if $SelectStatusArr[Rows].lesson_status_id eq $Search_lesson_status} selected {/if}>{$SelectStatusArr[Rows].lesson_status_title}</option>
					{/section}
				</select>-->
			  </td>
			</tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr>
			<!--<td width="22%" align="left" valign="middle">
				<div style="float:left;">
					<input type="text" name="Search_from_date" id="Search_from_date" class="datebox" value="{$Search_from_date}" style="width:80px;"/>
					<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_from_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				</div>
				<div style="float:right;">
					<input type="text" name="Search_to_date" id="Search_to_date" class="datebox" value="{$Search_to_date}" style="width:80px;"/>
					<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_to_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				</div>
			  </td>-->
				<!--<td colspan="2" align="left" valign="middle" style="color:#000000; padding-left:5px;">
				Select Status:&nbsp;
				<select name="Search_lesson_status" id="Search_lesson_status" class="selectbox">
					<option value="">--Select--</option>
					{section name=Rows loop=$SelectStatusArr}
						<option value="{$SelectStatusArr[Rows].lesson_status_id}" {if $SelectStatusArr[Rows].lesson_status_id eq $Search_lesson_status} selected {/if}>{$SelectStatusArr[Rows].lesson_status_title}</option>
					{/section}
				</select>
			  </td>-->
			</tr>
			<tr>	
				<td valign="middle">
				<input type="hidden" name="dosearch" value="GO">
				<input  name="submitdosearch" value="GO" type="button" class="go" style="margin-bottom:10px;" onClick="ManagerGeneralForm('{$page_name}?action=list_search','form_search')" />
				<input name="" type="button"  class="reset" value="RESET" onClick="window.location.href='{$page_name}';"/></td>
				
				
			</tr>
	  </table>
</form>
	
	<div style="padding:10px; color:#330000; font-weight:bold; font-size:16px; font-style:italic;">Income Search</div>
	
<form method="POST" name="form_search_in" id="form_search_in" action="{$page_name}" onsubmit="return false;">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="24%" style="color:#000000; padding-top:10px;">Check it to see total income: </td>
      <td width="76%" align="left" valign="bottom"><input type="checkbox" name="tot_incm" id="tot_incm" onclick="GetTotalIncome();" style="float:left;" />
      &nbsp;<span style="color:#000000; display:none; width:300px; float:left; padding-left:50px;" id="show_tot">Total Income: &nbsp;<b>$</b>{$total}&nbsp;&nbsp;Total Price: &nbsp;<b>$</b>{$total_price}</span> </td>
    </tr>
    <!--<tr>
		<td width="24%" style="color:#000000; padding-top:10px;">Check it to see total price: </td>
	  	<td width="50%"><input type="checkbox" name="tot_price" id="tot_price" onclick="GetTotalPrice();" style="float:left;" />&nbsp;<span style="color:#000000; display:none; width:300px; float:left; padding-left:50px;" id="price_tot">Total Price: &nbsp;<b>$</b>{$total_price}</span>
		</td>
		</tr>-->
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:#006600 1px solid;">
    <tr>
      <td width="35%" align="left" valign="middle"><div style="float:left;">
          <input type="text" name="Search_from_date_in" id="Search_from_date_in" class="datebox" value="{$Search_from_date_in}" style="width:100px;" readonly=""/>
        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search_in.Search_from_date_in,document.form_search_in.Search_from_date_in);return false;" hidefocus><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17" /></a> </div>
          <div style="float:right;">
            <input type="text" name="Search_to_date_in" id="Search_to_date_in" class="datebox" value="{$Search_to_date_in}" style="width:80px;" readonly=""/>
        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search_in.Search_from_date_in,document.form_search_in.Search_to_date_in);return false;" hidefocus><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17" /></a> </div></td>
      <td style="color:#000000; padding-left:35px;" width="65%"><span id="lable_total" style="display:none; float:left;">Total Income:&nbsp;&nbsp;<b>$</b></span><span id="total_income" style="float:left;"></span></td>
    </tr>
    <tr>
      <td valign="middle" colspan="2"><input type="hidden" name="in_search" value="DO" />
          <input  name="submitdosearch2" value="GO" type="button" class="go" style="margin-bottom:10px;" onclick="Search_Income('form_search_in');" />
          <input name="Input" type="button"  class="reset" value="RESET" onclick="window.location.href='{$page_name}';"/></td>
    </tr>
  </table>
</form>
	<!--<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="24%" style="color:#000000; padding-top:10px;">Check it to see total income: </td>
	  	<td width="9%"><input type="checkbox" name="tot_incm" id="tot_incm" onclick="GetTotalIncome();" /></td>
		<td width="67%" style="color:#000000;"><div id="show_tot" style="display:none;">Total Income: &nbsp;<b>$</b>{$total}</div></td>
	</tr>
			<tr><td colspan="3">&nbsp;</td></tr><tr><td colspan="3">&nbsp;</td></tr>

	</table>-->
<!--<div align="right" class="addNews_secand"><a href="language_update.php">+ Add Language</a></div>-->
<table width="100%">   <tr><td colspan="2">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>  </table>
{/if}
<div id="records_listing">	
{if $MessgReportText neq ""}<div align="left" class="successful2" style="color:#FF0000; margin:-25px 0 0 0;">{$MessgReportText}</div>{/if}
<div id="msgbox" style="color:#009900;"></div>		
<div class="newsName">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td colspan="5" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#9f9f9f">
				  <tr  bgcolor="#9f9f9f">
					<td  align="center" width="10%" valign="middle" class="padleft">
						<a href="#"  onClick="ManagerGeneral('{$page_name}?action=list_order&do_order=GO&OrderByID=1&OrderType={$OrderType}&{$SearchLink}&from={$from}')" >								
							<span class="whitetext"><strong>Title</strong></span>									
							{$ReturnSortingArr.DisplaySortingImage[1]}				
						</a>	
					</td>
					<td  align="center" width="12%" valign="middle" class="padleft">
							<span class="whitetext"><strong>Coach</strong></span>		
					</td>
					<td  align="center" width="12%" valign="middle" class="padleft">
							<span class="whitetext"><strong>Student</strong></span>									
					</td>
					<td width="6%" align="center" valign="middle" >
					<a href="#"  onClick="ManagerGeneral('{$page_name}?action=list_order&do_order=GO&OrderByID=4&OrderType={$OrderType}&{$SearchLink}&from={$from}')" >								
							<span class="whitetext"><strong>Price</strong></span>
					</a>
					</td>
					<td width="12%" align="center" valign="middle" >
							<span class="whitetext"><strong>Commission</strong></span>
					</td>
					<td width="12%" align="center" valign="middle" >
					<a href="#"  onClick="ManagerGeneral('{$page_name}?action=list_order&do_order=GO&OrderByID=3&OrderType={$OrderType}&{$SearchLink}&from={$from}')" >								
							<span class="whitetext"><strong>Paid Date</strong></span>
					</a>
					</td>
					<td width="12%" align="center" valign="middle" >
					<a href="#"  onClick="ManagerGeneral('{$page_name}?action=list_order&do_order=GO&OrderByID=2&OrderType={$OrderType}&{$SearchLink}&from={$from}')" >								
							<span class="whitetext"><strong>Date Added</strong></span>
					</a>
					</td>
					<td width="8%" align="center" class="whitetext" valign="middle"><strong>Status</strong></td>	
					<td width="4%" align="center" class="whitetext" valign="middle"><strong>View</strong></td>					
					<td width="6%" align="center" class="whitetext" valign="middle"><strong>Actions</strong></td>
				  </tr>
				</table>
			</td>
		</tr>
		<tr>
		  <td colspan="5" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#878787">
				{if $NumSelectCms > 0}
			{section name=RowCms loop=$SelectCmsArr}
			
				  <tr {if ($templatelite.section.RowCms.index % 2) eq 0} bgcolor="#ffffff" {else} bgcolor="#ffffff" {/if}>
					<td  align="left" width="10%" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].lesson_title}
					</td>
					<td  align="left" width="12%" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].coach_name}
					</td>
					<td  align="left" width="12%" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].student_name}
					</td>
					<td width="6%" class="simpletxt" align="center" valign="middle">${$SelectCmsArr[RowCms].lesson_price}</td>
					<td width="12%" class="simpletxt" align="center" valign="middle">${$SelectCmsArr[RowCms].admin_income}<br />({$SelectCmsArr[RowCms].lesson_commision_admin} %)</td>
					<td width="12%" class="simpletxt" align="center" valign="middle">{$SelectCmsArr[RowCms].lesson_payment_date}</td>
					<td width="12%" class="simpletxt" align="center" valign="middle">
					{$SelectCmsArr[RowCms].date_added}
					</td>
					<td width="8%" height="28" align="center" valign="middle">
						{$SelectCmsArr[RowCms].status_name}
					</td>
					<td width="4%" height="28" align="center" valign="middle">
					{if $SelectCmsArr[RowCms].lesson_status_id eq '4' || $SelectCmsArr[RowCms].lesson_status_id eq '5' }
						<a href="lesson_update.php?lesson_id={$SelectCmsArr[RowCms].lesson_id}">
						  <img src="images/view.gif" alt="" width="15" height="14" border="0" />
					  	</a>
					{else}
						<a href="lesson_update.php?lesson_id={$SelectCmsArr[RowCms].lesson_id}">
						  <img src="images/edit.gif" alt="" width="15" height="14" border="0" />
					  	</a>
					{/if}
					</td>
					<td width="6%" align="center" valign="middle">
					{if $SelectCmsArr[RowCms].lesson_status_id neq '2'}
					<a href="#" onClick="ConfirmDelete('{$page_name}?action=del&{$PreserveLink}','{$SelectCmsArr[RowCms].lesson_id}','{$SelectCmsArr[RowCms].lesson_title}','Lesson:: ')">
						  <img src="images/delete_icon.gif" border="0" alt="" />
					  </a>					  {else}N/A{/if}
					</td>
				 </tr>
				  {/section}
			{else}
				<tr bgcolor="#f4f4f5"><td class="plaintxt" colspan="5" align="center">No Record Found</td></tr>
			{/if}       
				  
				 
			  </table>
			</td>
		 <!-- <tr>
		  <td colspan="2">&nbsp;</td>
		  </tr>
  		  <tr>
		  <td colspan="2" style="color:#000000;font-family: 'Trebuchet MS','Myriad Pro',Arial,Verdana,sans-serif;font-size: 14px; border:1px solid #006600;">Total Income:&nbsp;{$total}</td>
		  </tr>-->

	  </tr>
		 
  </table>
</div>
<br />
<div>	{if $pagination_arr[3] > 0}
   <table>
	  <tr>
        <td height="30" valign="middle" align="center">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td align="left" class="plaintxt">					
					{if $pagination_arr[2] neq ""}					
						{$pagination_arr[2]}
					{/if}					
					</td>
					<td align="right" class="plaintxt">					   	
					{if $pagination_arr[1] neq ""}					
						{$pagination_arr[1]}
					{/if}					
					</td>
				</tr>
			</table>
		</td>
      </tr>
    </table>
	{/if} </div>
</div>

{if $IsProcess neq "Y"}  	
{include file="bottom.tpl"}
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>
{/if}
