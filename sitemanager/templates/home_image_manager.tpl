{if $IsProcess neq "Y"}
{include file="top.tpl"}
{literal}
<script type="text/javascript">
var j = jQuery.noConflict();
j(document).ready(function(){
		
focus_blur('Search_image_title','Search by Image Title');
focus_blur('Search_event_id','Search by Event');

});

</script>	
{/literal}
<h2>Home Image<div id="TransMsgDisplay" align="right" style="height:3px" ></div></h2>
	
<form method="POST" name="form_search" id="form_search" action="{$page_name}" onsubmit="return false;">
	<table width="80%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="60%" align="right" valign="middle" colspan="3">	
			<input size="30" type="text" name="Search_image_title" id="Search_image_title" value="{$Search_image_title}" class="textbox search-box"/>				
			<input type="hidden" name="dosearch" value="GO">
			</td>
			<!--<td width="40%" align="left" valign="top" colspan="2">
			
			</td>	
			
		</tr>
		<tr>
			<td width="5%" align="right" valign="middle" class="maintext">Date(From) :</td>				
			<td width="15%" align="center">
				<input name="Search_date_from" id="Search_date_from" readonly type="text" size="10" value="{$Search_date_from}" /><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.form_search.Search_date_from,'');return false;" HIDEFOCUS><img src="images/icon_calender.gif" alt="" border="0" /></a>
				<input type="hidden" name="dosearch" value="GO">			  
			</td>	
			<td width="5%" align="right" class="maintext">Date(To) :</td>				
			<td width="23%" align="center">
				<input name="Search_date_to" id="Search_date_to" readonly type="text" size="10" value="{$Search_date_to}" /><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_date_from,document.form_search.Search_date_to);return false;" HIDEFOCUS><img src="images/icon_calender.gif" alt="" border="0" /></a>	
			</td>-->
			<td valign="middle" align="right">
			<input  name="submitdosearch" value="GO" type="button" class="go" onClick="ManagerGeneralForm('{$page_name}?action=list_search','form_search')" />			
			<input name="" type="button"  class="reset" value="RESET" onClick="window.location.href='{$page_name}';"/></td>
			</tr>	
	</table>
</form>
<div align="right" class="addNews_secand">&nbsp;<a href="home_image_update.php">+ Add Image</a></div>
{/if}
<div id="records_listing">	
{if $MessgReportText neq ""}<div align="left" class="successful2" style="color:#FF0000; margin:-25px 0 0 0;">{$MessgReportText}</div>{/if}
				
<div class="newsName">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td colspan="6" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#9f9f9f">
				 <tr  bgcolor="#9f9f9f">
				  	<td width="10%" align="center" class="whitetext" valign="middle">Image</td>	
					<td  align="left" width="25%" valign="middle" class="whitetext">
						<a href="#"  onClick="ManagerGeneral('{$page_name}?action=list_order&do_order=GO&OrderByID=1&OrderType={$OrderType}&{$SearchLink}&from={$from}')" >								
							Image Title								
							{$ReturnSortingArr.DisplaySortingImage[1]}				
						</a>	
					</td>
					<td width="12%" align="center" class="whitetext" valign="middle"><strong>Date Added</strong></td>							
					<td width="10%" align="center" class="whitetext" valign="middle"><strong>Active</strong></td>	
					<td width="10%" align="center" class="whitetext" valign="middle">Actions</td>
				  </tr>
				</table>
			</td>
		</tr>
		<tr>
		
		
			<td colspan="6" align="left" valign="top">
					<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#878787">
				{if $NumSelectCms > 0}
		{section name=RowCms loop=$SelectCmsArr}
				  <tr {if ($templatelite.section.RowCms.index % 2) eq 0} bgcolor="#ffffff" {else} bgcolor="#ffffff" {/if}>
				  
					<td  align="center" width="10%" valign="middle" class="simpletxt">
					<img src="../uploaded/home_images/thumbs/{$SelectCmsArr[RowCms].image_file}" style="padding:5px;" height="22" />
					</td>
					<td  align="left" width="25%" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].image_alt}
					</td>
					
					<td  align="left" width="12%" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].date_added}
					</td>				
					<td width="10%" height="28" align="center" valign="middle"><a href="#" onClick="ManagerGeneral('{$page_name}?action=activate&{$PreserveLink}&record_id={$SelectCmsArr[RowCms].image_id}')">
					<img src="images/{$SelectCmsArr[RowCms].active_img}" width="15" height="14" border="0" alt="{$SelectCmsArr[RowCms].active_alt}" /></a></td>
					
					
					
					<td width="10%" align="center" valign="middle">	
					  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="42">
						  <tr>
						 <td width="50%" height="27" align="center" valign="middle" class="rightBorder" >
						  <a href="home_image_update.php?image_id={$SelectCmsArr[RowCms].image_id}">
						   <img src="images/edit_dark.gif" alt="" width="15" height="14" border="0" />
						  </a>
						  </td>
						  
						  <td width="50%" align="center" valign="middle">
						  <a href="#" onClick="ConfirmDelete('{$page_name}?action=del&{$PreserveLink}','{$SelectCmsArr[RowCms].image_id}','{$SelectCmsArr[RowCms].image_alt_delete}','Image:: ')">
						  <img src="images/delete_icon.gif" border="0" alt="" />
						  </a>
						  </td>
						  </tr>
					</table>					
					</td>
				 </tr>
				  {/section}
			{else}
				<tr bgcolor="#f4f4f5"><td class="plaintxt" colspan="5" align="center">No Record Found</td></tr>
			{/if}       
				  
				 
			  </table>
			</td>
		  </tr>
		 
  </table>
</div>
<br />
<div>	{if $pagination_arr[3] > 0}
   <table>
	  <tr>
        <td height="30" valign="middle" align="center">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td align="left" class="plaintxt">					
					{if $pagination_arr[2] neq ""}					
						{$pagination_arr[2]}
					{/if}					
					</td>
					<td align="right" class="plaintxt">					   	
					{if $pagination_arr[1] neq ""}					
						{$pagination_arr[1]}
					{/if}					
					</td>
				</tr>
			</table>
		</td>
      </tr>
	  </table>
	{/if} </div>
</div>

{if $IsProcess neq "Y"}  	
{include file="bottom.tpl"}
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>
{/if}