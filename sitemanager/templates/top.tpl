<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome to Admin Panel : Game</title>
<link href="style/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/jquery-1.js"></script>
<script type="text/javascript" src="js/ddaccordion.js"></script>
<script type="text/javascript" src="js/menu_collapse.js"></script>
<script language="javascript1.2" src="js/jquery-impromptu.2.4.min.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">
	var j = jQuery.noConflict();
</script>
<link rel="stylesheet" href="../style/ingrid.css" type="text/css" media="screen" />
<script type="text/javascript" src="../js/ingrid.js"></script>
<script language="javascript1.2" src="../includejs/general_functions.js" type="text/javascript"></script>
<script language="javascript1.2" src="../includeajax/AjaxAdminGeneral.js" type="text/javascript"></script>
<script language="javascript1.2" src="../includeajax/AjaxManager.js" type="text/javascript"></script>
<script language="javascript1.2" src="js/general_functions.js" type="text/javascript"></script>
<script language="javascript1.2" src="../includejs/functions_admin.js" type="text/javascript"></script>
<script language="javascript1.2" src="js/onepage.js" type="text/javascript"></script>
</head>

<body>
    <div class="header">
			<div class="logoinner">
				<a href="home.php">
					<img src="images/logo.gif" alt="IFSS" title="IFSS" border="0" />
				</a>
			</div>
	</div>
	<div class="middle_body">
		<div class="innerMid">
			<div class="leftPart">
		
				<div class="glossymenu">
					<a class="menuitem submenuheader" href="#">
					<span class="accordprefix"></span>Admin General<span class="accordsuffix">
					<img src="images/plus.gif" class="statusicon" alt=""/></span></a>
						<div class="submenu">
							<ul>
								<li><a href="home.php">Admin Home</a></li>
								<li><a href="admin_manager.php">Admin Details</a></li>
								<li><a href="default_admin_comission.php">Commission Manager</a></li>
							</ul>
						</div>								
						
						<a class="menuitem" href="cms_manager.php">CMS Manager</a>
						<a class="menuitem" href="home_image_manager.php">Banner Manager</a>
						<a class="menuitem" href="language_manager.php">Language Manager</a>
						<a class="menuitem" href="game_manager.php">Game Manager</a>
						<a class="menuitem" href="news_manager.php">News Manager</a>
						<a class="menuitem" href="user_manager.php">User Manager</a>
						<a class="menuitem" href="coach_manager.php">Coach Manager</a>
						<a class="menuitem" href="training_partner_manager.php">Training Partner Manager</a>								
						<!--a class="menuitem submenuheader" href="#">
						<!--span class="accordprefix"></span>User Manager<span class="accordsuffix">
						<img src="images/plus.gif" class="statusicon" alt=""/></span></a>
						<div class="submenu">
							<ul>
								<li><a href="coach_manager.php">Coach</a></li>
								<li><a href="training_partner_manager.php">Training Partner</a></li>
								<li><a href="student_manager.php">Student</a></li>
							</ul>
						</div-->			
						<a class="menuitem" href="lesson_manager.php">Lesson Manager</a>
						<a class="menuitem" href="coach_review_manager.php">Coach Review and Rating Manager</a>
						<a class="menuitem" href="training_partner_review_manager.php">Training Partner Review Rating</a>
						<a class="menuitem" href="video_review_manager.php">Video Review Manager</a>
						<a class="menuitem" href="ads_manager.php">Ads Manager</a>
						<!--<a class="menuitem submenuheader" href="#">
						<span class="accordprefix"></span>Review and rating Manager<span class="accordsuffix">
						<img src="images/plus.gif" class="statusicon" alt=""/></span></a>
						<div class="submenu">
							<ul>
								<li><a href="coach_review_manager.php">Coach</a></li>
								<li><a href="video_review_manager.php">Video</a></li>
							</ul>
						</div>-->		
						<a class="menuitem" href="clients_withdraws.php" >Client Withdraws</a>
                                                <a class="menuitem" href="settings.php">Settings</a>
                                                <a class="menuitem" href="/freichat/server/admin.php">Chat settings</a>
						<a class="menuitem" href="../forum" target="_blank">Go to Forum</a>
						<a class="menuitem" href="logout.php">Log Out</a>
				</div>
</div>
<div class="rightPart">