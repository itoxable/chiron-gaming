{include file="top.tpl"}
{literal}
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
<script language="javascript" type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "exact",
		elements:"cms_description",
		theme : "advanced",
		plugins : "table,save,advhr,advimage,advlink,insertdatetime,searchreplace,contextmenu,paste,directionality",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,zoom,separator,forecolor,backcolor",
		theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
		theme_advanced_buttons3_add_before : "tablecontrols,separator",
		theme_advanced_buttons3_add : "advhr",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		content_css : "example_word.css",
	    plugi2n_insertdate_dateFormat : "%Y-%m-%d",
	    plugi2n_insertdate_timeFormat : "%H:%M:%S",
		external_image_list_url : "image_list.js.php",
		file_browser_callback : "tinyBrowser",
		paste_use_dialog : false,
		theme_advanced_resizing : true,
		theme_advanced_resize_horizontal : false,
		theme_advanced_link_targets : "_something=My somthing;_something2=My somthing2;_something3=My somthing3;",
		paste_auto_cleanup_on_paste : true,
		paste_convert_headers_to_strong : false,
		paste_strip_class_attributes : "all",
		paste_remove_spans : false,
		paste_remove_styles : false
	});
</script>
{/literal}
<h2>{$SubmitButton}</h2>
<div class="tabarea">
<div class="tabcontent">
<p align="right" class="fields">Fields marked with <span class="red">*</span> are required</p>
<form action="cms_update.php?action=trans" method="post" name="frm_admin" id="frm_admin" enctype="multipart/form-data">

		<table width="100%" border="0" cellspacing="4" cellpadding="0">
		  {if $err_msgs neq ""}
			<tr>
				<td class="simpleText" colspan="2"><ul>{$err_msgs}</ul></td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
		  	</tr>
		  {/if}
		  <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">CMS Title<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <input type="text" name="cms_title" id="cms_title" class="textbox" value="{$CMSArr.cms_title}"/>
			</label></td>
		  </tr>
		  {if in_array($cms_id,$cms_has_short_description)}
		  <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Short Description<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <textarea rows="8" cols="43"  name="cms_short_description" id="cms_short_description" >{$CMSArr.cms_short_description}</textarea>
			</label></td>
		  </tr>

		  {/if}
		 {if !in_array($cms_id,$cms_not_description)}
		 <tr>
			<td align="left" valign="top" class="plaintxt">CMS Description:</td>
			<td>&nbsp;</td>
		 </tr>
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" height="8">&nbsp;</td>
		 </tr>
		 <tr>
			<td align="left" valign="top" colspan="2" class="simpleText">
			<textarea name="cms_description" id="cms_description" >{$CMSArr.cms_description}</textarea>
			</td>
		 </tr>
		 {/if}
		 {if in_array($cms_id,$cms_has_photo)}
         <tr>
			<td align="left" valign="top" class="plaintxt">{$DisplayImageControlStr}<span class="red" id="img_star"></span></td>
			<td align="left" valign="top" class="plaintxt"></td>
			<td align="left" valign="top" class="plaintxt">
			</td>
		 </tr>
		 {/if}
		 <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		 </tr>
		 {if !in_array($cms_id,$cms_not_metatag)}
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Metatag Title<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <input type="text" name="metatag_title" id="metatag_title" class="textbox" value="{$CMSArr.metatag_title}"/>
			</label></td>
		  </tr>

		  <tr>
			<td align="left" valign="middle" class="plaintxt">Metatag Keywords<span class="red">*</span>:</td>
			<td align="left" valign="middle"><label>
			  <textarea rows="3" cols="23"  name="metatag_keywords" id="metatag_keywords" class="bigger">{$CMSArr.metatag_keywords}</textarea>
			</label></td>
		  </tr>

		  <tr>
			<td align="left" valign="middle" class="plaintxt">Metatag Description<span class="red">*</span>:</td>
			<td align="left" valign="middle"><label>
			  <textarea rows="3" cols="23"  name="metatag_description" id="metatag_description" class="bigger">{$CMSArr.metatag_description}</textarea>
			</label></td>
		  </tr>

		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		  {/if}
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">
			 <input name="cms_id" id="cms_id" type="hidden"  value="{$cms_id}"/>

			 <input name="imageField" type="submit" class="addsadmin" value="{$SubmitButton}" style="width:96px;"/>
&nbsp;&nbsp;&nbsp;<input name="" type="button"  class="cancel" value="Cancel" onclick="window.location.href='cms_manager.php?IsPreserved=Y'"/></td>
		  </tr>
		  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  </tr>
		</table>
</form>

	</div>
 </div>
    <p>&nbsp;</p>
{include file="bottom.tpl"}