{include file="top.tpl"}
{literal}
<script type="text/javascript" src="../js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="../js/jQuery.functions.pack.js"></script>
<script type="text/javascript" src="../js/jquery.dateLists.min.js"></script>
<link rel="stylesheet" type="text/css" href="../style/jquery.dateLists.css" />
<link rel="stylesheet" href="../style/autosuggest.css" type="text/css" />
<script type="text/javascript" src="../js/jquery.ausu-autosuggest.min.js"></script>

    <script type="text/javascript">
        $().ready(function() {  
        $('#date_of_birth').dateDropDowns({dateFormat:'yy-mm-dd'});
		
		 $.fn.autosugguest({  
				   className: 'ausu-suggest',
				  methodType: 'POST',
					minChars: 2,
					  rtnIDs: true,
					dataFile: 'autocomplete.php?action=populateCity'
			});  
   });  
    </script>
<script language="javascript">
function showLocal()
{
     if(document.getElementById('avail_L').checked == true)
	  {  
		$('#avail_city').show();
	 }
	if(document.getElementById('avail_L').checked == false)
	 {  	
	   $('#avail_city').hide();
	 }		
}
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}


</script>
{/literal}
<h2>{$SubmitButton}</h2>
<div class="tabarea">
<div class="tabcontent">
<form action="user_update.php?user_type_id={$user_type_id}&action=trans" method="post" name="frm_admin" id="frm_admin" enctype="multipart/form-data">
 <input type="hidden" name="is_coach" value="{$is_coach}" />
 <input type="hidden" name="is_partner" value="{$is_partner}" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">

	{if $err_msgs neq ""}     
		<tr>
			<td class="simpleText" colspan="2"><ul>{$err_msgs}</ul></td>
		</tr>
		<tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		</tr>
	 {/if}
	 {if $user_id neq ''}
	 <tr>
	   <td width="20%" align="left" valign="top" class="plaintxt">Member Type:</td>
	   <td width="20%" align="left" valign="top" class="plaintxt">{if $is_coach eq '0'}<input type="radio" name="user_type" value="1" />Coach{/if}&nbsp;&nbsp;
	   {if $is_partner eq '0'}<input type="radio" name="user_type" value="3" />Training Partner{/if}</td>
	 </tr> 
	  <tr>
	 
		<td align="left" valign="top">&nbsp;</td>
		<td align="left" valign="top">&nbsp;</td>
	 </tr>
	 {/if}
	 {if $u_id neq ''}
	 <tr>
		<td width="20%" align="left" valign="top" class="plaintxt">Registration Number:</td>
		<td width="80%" align="left" valign="top"><label class="textbox" style="border:0px;">{$FabricArr.registration_number}</label></td>
	 </tr>
	 {/if}
	  <tr>
		<td width="20%" align="left" valign="top" class="plaintxt">Email Address<span class="red">*</span>:</td>
		<td width="80%" align="left" valign="middle"><label>
		<input type="text" name="email" id="email" class="textbox" value="{$FabricArr.email}" {if $u_id neq ''} 
		readonly="" {/if}/>
		</label></td>
	 </tr>
	 <tr>
		<td width="20%" align="left" valign="top" class="plaintxt">Password<span class="red">*</span>:</td>
		<td width="80%" align="left" valign="middle"><label>
		<input type="text" name="password" id="password" class="textbox" value="{$FabricArr.password}"/>
		</label></td>
	 </tr>
     <tr>
		<td width="20%" align="left" valign="top" class="plaintxt">UserID<span class="red">*</span>:</td>
		<td width="80%" align="left" valign="middle"><label>
		<input type="text" name="name" id="name" class="textbox" value="{$FabricArr.name}"/>
		</label></td>
	 </tr>
	  {if $is_coach eq 1}
     <tr>
		<td width="20%" align="left" valign="top" class="plaintxt">Paypal Email ID<span class="red">*</span>:</td>
		<td width="80%" align="left" valign="middle"><label>
		<input type="text" name="paypal_email_id" id="paypal_email_id" class="textbox" value="{$FabricArr.paypal_email_id}"/>
		</label></td>
	 </tr>
	  {/if}
     
	 <tr>
			{$DisplayImageControlStr}
     </tr>
	 <tr>
	 
		<td align="left" valign="top">&nbsp;</td>
		<td align="left" valign="top">&nbsp;</td>
	 </tr>

	  <tr>
		<td width="20%" align="left" valign="top" class="plaintxt">City<span class="red">*</span>:</td>
		<td width="80%" align="left" valign="middle"><label><div class="ausu-suggest">
		<input type="text" name="citynm" id="citynm" class="textbox" value="{$FabricArr.city_name}" autocomplete="off" onblur="CreateCountry()"/>
		<input name="city" id="city" type="hidden"  value="{$FabricArr.city}" autocomplete="off" />
		</div>
		</label></td>
	 </tr>

	<!-- <tr>
	  <td align="left" valign="top" class="plaintxt">Language :</td>
	  <td align="left" valign="middle"><label>
	   <select  class="selectbox" id="s2" multiple="multiple" name="language[]">
			{section name=lang loop=$langArr}
             <option value="{$langArr[lang].language_id}" {if in_array($langArr[lang].language_id,$language_ids)} selected="selected" {/if}>{$langArr[lang].language_name}</option>
            {/section}
       </select>
	   </label></td>
	 </tr> -->
	  <tr>
			<td align="left" valign="top" class="plaintxt">Language :</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $langArr|@count neq 0}
		 	{section name=lang loop=$langArr}
			<input type="checkbox" name="language[]"  value="{$langArr[lang].language_id}" {if $language_ids neq ''} {if $langArr[lang].language_id|inarray:$language_ids}checked{/if} {/if} /> 
			{$langArr[lang].language_name}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>	
	 <tr><td colspan="2">&nbsp;</td></tr> 
	
	
<!-- 	 <tr> -->
<!-- 		<td width="20%" align="left" valign="top" class="plaintxt">Skype Id:</td> -->
<!-- 		<td width="80%" align="left" valign="middle"><label> -->
<!-- 		<input type="text" name="skype_id" id="skype_id" class="textbox" value="{$FabricArr.skype_id}"/> -->
<!-- 		</label></td> -->
<!-- 	 </tr> -->
	  {if $is_coach eq '1' || $is_partner eq '1'}
	   <tr>
		<td width="20%" align="left" valign="top" class="plaintxt">Availability<span class="red">*</span>:</td>
		<td width="80%" align="left" valign="middle" class="plaintxt"><label>
		Local&nbsp;&nbsp;
		<input name="availability_type1" type="checkbox" value="L" id="avail_L" {if $FabricArr.availability_type eq 'L'}checked=checked{/if} onclick="showLocal()"/>
		Online&nbsp;&nbsp;<input name="availability_type2" type="checkbox" value="O" id="avail_O" {if $FabricArr.availability_type eq 'O'}checked=checked{/if}/>
		</label></td>
	  </tr>
	  <tr>
	 
		<td align="left" valign="top">&nbsp;</td>
		<td align="left" valign="top">&nbsp;</td>
	 </tr>
	 <tr id="avail_city"  {if $FabricArr.availability_type =='L'} {else} style='display:none' {/if}>
		<td width="20%" align="left" valign="top" class="plaintxt">City (Local meet-up)<span class="red">*</span>:</td>
		<td width="80%" align="left" valign="middle"><label><div class="ausu-suggest">
		<input type="text" name="acity_name" id="acity_name" class="textbox" value="{$FabricArr.acity_name}" onblur="CreateACountry()" autocomplete="off" />
		<input name="availability_city" id="availability_city" type="hidden"  value="{$FabricArr.availability_city}" autocomplete="off" />
		</div>
		</label></td>
	 </tr>
	 <tr>
			<td align="left" valign="top" class="plaintxt">About User:</td>
			<td align="left" valign="top" class="plaintxt"><label>
			<textarea rows="3" cols="23"  name="description" id="description"  onKeyDown="limitText(this.form.description,this.form.countdown,300);"
			onKeyUp="limitText(this.form.description,this.form.countdown,300);" class="bigger" style="height:110px;">{$FabricArr.description}</textarea>
			</label>
			</td>
		
     </tr>
	 <tr>
			<td align="left" valign="top" class="plaintxt">&nbsp;</td>
			<td align="left" valign="top" class="plaintxt"><label> 
			Characters left <input readonly type="text" name="countdown" size="3" value="{$FabricArr.char_left}" class="tiny">
			</label></td>
	</tr>		
	      		
	 {/if}
	  <tr>
			<td height="28" align="left" class="plaintxt">Active:</td>
			<td height="28" align="left" class="maintext">
				<input name="is_active" id="is_active" type="checkbox" class="maintext" value="Y" {if $FabricArr.is_active =='Y'}checked{/if}>
			</td>
	</tr>
	 <tr>
	 
		<td align="left" valign="top">&nbsp;</td>
		<td align="left" valign="top">&nbsp;</td>
	 </tr>
	 
	 <tr>
	 	<td align="left" valign="top">&nbsp;</td>
		<td align="left" valign="top">
		<!--<input name="user_id" id="u_id" type="hidden" value="{$u_id}"/>-->
		<input name="u_id" id="u_id" type="hidden" value="{$user_id}"/>
		<input name="imageField" type="submit" class="addsadmin" value="{$SubmitButton}" {if $user_id eq ''} style="width:160px;"{else}style="width:185px;"{/if}/>
		<input name="" type="button"  class="cancel" value="Cancel" 
		onclick="window.location.href='{$link}?IsPreserved=Y'"/></td>
	 </tr>
	 <tr>
	  	<td>&nbsp;</td>
	  	<td>&nbsp;</td>
	 </tr>
	</table>
</form>

	</div>
 </div>
    <p>&nbsp;</p>
	<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>
{include file="bottom.tpl"}
