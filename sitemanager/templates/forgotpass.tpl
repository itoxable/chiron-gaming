<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome to Admin Panel : IFSS</title>
<link href="style/style.css" rel="stylesheet" type="text/css" />

{literal}
<script type="text/javascript" src="js/jquery-1.js"></script>
<script type="text/javascript" src="js/ddaccordion.js"></script>
<script type="text/javascript" src="js/menu_collapse.js"></script>
<script language="javascript" type="text/javascript">
	var j = jQuery.noConflict();
</script>
<script language="javascript1.2" src="../includeajax/AjaxAdminGeneral.js"></script>
<script language="javascript1.2" src="../includejs/general_functions.js"></script>
<script>

function set_focus()
{
    j('#Username').focus();
}

function cancel()
{ 
	 window.location.href='index.php';
}
</script>
{/literal}
</head>
<body onload="set_focus()">
<div class="indexinner">
   <div class="logo" align="center"><a href="#"><img src="images/logo.gif" alt="" border="0" /></a></div>
	<div class="clear"></div>
    <div class="loginouter">
       <div class="indexLogin">
	   	<span id="myResponse" style="width:300px;"></span><br/>
			<form name="frmLogin" id="frmLogin" method="post" action="" onsubmit="return false;">
				<fieldset>
					<label>Username :</label>
					<input name="Username" id="Username" value="" type="text"/>
					<label class="fpass">&nbsp;</label>
					<input name="" type="button" value="OK"  class="ok" onclick="javascript:submit_form1()" />
					<input name="" type="button"  value="CANCEL" class="cancel" onclick="cancel(); " />
					<div class="spacer"></div>
				</fieldset>
			</form>
		</div>
	</div>
	<div class="footerouter">
	&copy;2011 All Rights Reserved.
	</div>
</div>
	
</body>
</html>
