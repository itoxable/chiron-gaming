{if $IsProcess neq "Y"}
{include file="top.tpl"}
{literal}
<script type="text/javascript">
var j = jQuery.noConflict();
j(document).ready(function(){
		

focus_blur('Search_from_date','Search From');
focus_blur('Search_to_date','Search To');
});

</script>
{/literal}
{/if}
{literal}
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
						   		   
	//When you click on a link with class of poplight and the href starts with a # 
	$('a.poplight[href^=#]').click(function() {
		var popID = $(this).attr('rel'); //Get Popup Name
		var popURL = $(this).attr('href'); //Get Popup href to define size
				
		//Pull Query & Variables from href URL
		var query= popURL.split('?');
		var dim= query[1].split('&');
		var popWidth = dim[0].split('=')[1]; //Gets the first query string value

		//Fade in the Popup and add close button
		$('#' + popID).fadeIn().css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close"><img src="images/close_button.png" class="btn_close" title="Close Window" alt="Close" /></a>');
		
		//Define margin for center alignment (vertical + horizontal) - we add 80 to the height/width to accomodate for the padding + border width defined in the css
		var popMargTop = ($('#' + popID).height() + 80) / 2;
		var popMargLeft = ($('#' + popID).width() + 80) / 2;
		
		//Apply Margin to Popup
		$('#' + popID).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		//Fade in Background
		$('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
		$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer 
		
		return false;
	});
	
	
	//Close Popups and Fade Layer
	$('a.close, #fade').live('click', function() { //When clicking on the close or fade layer...
	  	$('#fade , .popup_block').fadeOut(function() {
			$('#fade, a.close').remove();  
	}); //fade them both out
		
		return false;
	});

	
});

</script>
{/literal}
{if $IsProcess neq "Y"}
<h2>Coach Reviews and Ratings <div id="TransMsgDisplay" align="right" style="height:3px" ></div></h2>
<form method="POST" name="form_search" id="form_search" action="{$page_name}" onsubmit="return false;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				
				<td width="17%" align="right" valign="middle" >	
				<select name="u_id" class="selectbox">
				<option value="">Select</option>
				{section name=coachRow loop=$Coach}
				<option value="{$Coach[coachRow].user_id}" {if $u_id eq $Coach[coachRow].user_id} selected="selected" {/if}>{$Coach[coachRow].name}</option>
				{/section}
				</select>
				<input type="hidden" name="dosearch" value="GO">
				</td>
				
				<td width="20%" align="left" valign="middle">
				<input type="text" name="Search_from_date" id="Search_from_date" class="datebox" readonly="" value="{$Search_from_date}" style="width:80px;"/>
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_from_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				<input type="hidden" name="dosearch" value="GO">
				</td>

				<td width="20%" align="left" valign="middle">
				<input type="text" name="Search_to_date" id="Search_to_date" class="datebox" readonly="" value="{$Search_to_date}" style="width:80px;"/>
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_to_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				<input type="hidden" name="dosearch" value="GO">
				</td>

				<td valign="middle">
				<input  name="submitdosearch" value="GO" type="button" class="go" onClick="ManagerGeneralForm('{$page_name}?action=list_search','form_search')" />
				<input name="" type="button"  class="reset" value="RESET" onClick="window.location.href='{$page_name}';"/></td>
			</tr>
		
		</table>
	</form>
{/if}	

<div id="records_listing">

{if $MessgReportText neq ""}<div align="left" class="successful2" style="color:#FF0000; margin:-25px 0 0 0;">{$MessgReportText}</div>{/if}	
		
<div class="newsName">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td colspan="6" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#9f9f9f">
				  <tr  bgcolor="#9f9f9f">
				  	<td  align="left" width="25%" valign="middle" class="padleft">
							<span class="whitetext"><strong>Review to</strong></span>									
										
						
					</td>
					<td  align="left" width="25%" valign="middle" class="padleft">
							<span class="whitetext"><strong>Review By</strong></span>									
					</td>
					<td  align="left" width="18%" valign="middle" class="padleft">
						<a href="#"  onClick="ManagerGeneral('{$page_name}?action=list_order&do_order=GO&OrderByID=3&OrderType={$OrderType}&{$SearchLink}&from={$from}')" >								
							<span class="whitetext"><strong>Review Date</strong></span>									
							{$ReturnSortingArr.DisplaySortingImage[1]}				
						</a>	
					</td>	
					<td width="10%" class="whitetext" align="center" valign="middle"><strong>View</strong></td>
					<td width="10%" class="whitetext" align="center" valign="middle"><strong>Active</strong></td>
					<td width="10%" class="whitetext" align="center" valign="middle"><strong>Actions</strong></td>
				  </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#878787">
				{if $NumSelectCms > 0}
			{section name=RowCms loop=$SelectCmsArr}
				  <tr {if ($templatelite.section.RowCms.index % 2) eq 0} bgcolor="#ffffff" {else} bgcolor="#ffffff" {/if}>
				  
				  	<td  align="left" width="25%" height="39" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].review_to}
					</td>
			        <td  align="left" width="25%" height="39" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].review_by}
					</td>
					<td  align="left" width="18%" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].date_added}
					</td>
					<td width="10%" align="center" valign="middle">
					<a href="#?w=400" rel="popup{$SelectCmsArr[RowCms].user_review_id}" class="poplight">
					<img src="images/view.gif" width="20" height="19" border="0" alt="View Review" /></a></td>
					<td width="10%" align="center" valign="middle"><a href="javascript:;" onClick="ManagerGeneral('{$page_name}?action=activate&{$PreserveLink}&record_id={$SelectCmsArr[RowCms].user_review_id}')">
					<img src="images/{$SelectCmsArr[RowCms].active_img}" width="15" height="14" border="0" alt="{$SelectCmsArr[RowCms].active_alt}" /></a></td>
					
					 <td width="10%" class="simpletxt" align="center" valign="middle">
						  <a href="#" onClick="ConfirmDelete('{$page_name}?action=del&{$PreserveLink}','{$SelectCmsArr[RowCms].user_review_id}','{$SelectCmsArr[RowCms].name_delete}','Coach Review by:: ')">
						  <img src="images/delete_icon.gif" border="0" alt="" />
						  </a>
				     </td>
					
				 </tr>
				 <tr>
				 <td colspan="6">
				 <div id="popup{$SelectCmsArr[RowCms].user_review_id}" class="popup_block">
					<h4 style="color:#5B8F2A;">View Review Details</h4>
					<p>
					<div style="font-size:12px; color:#000000;">
					  <table style="font-size:14px; color:#000000;">
					    <tr><td colspan="2">&nbsp;</td></tr>
					    <tr>
						 <td valign="top"><b>Review:</b> </td><td>{$SelectCmsArr[RowCms].review_comment|nl2br}</td>
						</tr>
						<tr><td><b>Overall:</b> </td><!--<td>{$SelectCmsArr[RowCms].overall_comment}</td>--><td><b>Rating:</b> </td><td>{$SelectCmsArr[RowCms].rating}</td></tr>
						{section name=review loop=$Individual[RowCms]}
						<tr><td><b>{$Individual[RowCms][review].rating_category} :</b> </td><!--<td>{$Individual[RowCms][review].comment}</td>--><td><b>Rating:</b> </td><td>{$Individual[RowCms][review].rating}</td></tr>
						{/section}
					  </table>
					</div>
					</p>
				</div>
				</td>
				</tr>
				  {/section}
			{else}
				<tr bgcolor="#f4f4f5"><td class="plaintxt" colspan="5" align="center">No Record Found</td></tr>
			{/if}       
				  
				 
			  </table>
			</td>
		  </tr>
		 
  </table>
</div>
<br />
<div>	{if $pagination_arr[3] > 0}
   <table>
	  <tr>
        <td height="30" valign="middle" align="center">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td align="left" class="plaintxt">					
					{if $pagination_arr[2] neq ""}					
						{$pagination_arr[2]}
					{/if}					
					</td>
					<td align="right" class="plaintxt">					   	
					{if $pagination_arr[1] neq ""}					
						{$pagination_arr[1]}
					{/if}					
					</td>
				</tr>
			</table>
		</td>
      </tr>
	  </table>
	{/if} </div><br /><br /><br /><br /><br />
</div>

{if $IsProcess neq "Y"}  	
{include file="bottom.tpl"}
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>
{/if}
