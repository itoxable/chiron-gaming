{literal}
<script type="text/javascript">
function checkPaid(status){
	if(status==2){
		document.getElementById('show_row').style.display="block";
	}else{
		document.getElementById('show_row').style.display="none";
	}
}

function date_focus(){
	
	if(document.getElementById('paid_date').value=='Date'){
		document.getElementById('paid_date').value='';
	}

}

function transc_focus(){
	
	if(document.getElementById('transaction_no').value=='Transaction No.'){
		document.getElementById('transaction_no').value='';
	}

}

function date_blur(){
	
	if(document.getElementById('paid_date').value==''){
		document.getElementById('paid_date').value='Date';
	}

}

function transc_blur(){
	
	if(document.getElementById('transaction_no').value==''){
		document.getElementById('transaction_no').value='Transaction No.';
	}

}


</script>
{/literal}
{include file="top.tpl"}
<h2>{$SubmitButton}</h2>
<div class="tabarea">
<div class="tabcontent">
<p align="right" class="fields">Fields marked with <span class="red">*</span> are required</p>
<form action="lesson_update.php?action=trans" method="post" name="frm_admin" id="frm_admin" enctype="multipart/form-data">
		<table width="100%" border="0" cellspacing="4" cellpadding="0">
		  {if $err_msgs neq ""}     
			<tr>
				<td class="simpleText" colspan="2"><ul>{$err_msgs}</ul></td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
			 </tr>
		  {/if}
		 
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Title <span style="color:#FF0000;">*</span>:</td>
			<td width="70%" align="left" valign="middle">
			{if $FabricArr.lesson_status_id neq '4' && $FabricArr.lesson_status_id neq '5'}
			<label>
			<input type="text" name="lesson_title" id="lesson_title" value="{$FabricArr.lesson_title}" class="textbox" style=" margin-left:7px;" />
			</label>
			{else}
			<label style="color:#000000; padding-left:5px;">{$FabricArr.lesson_title}</label>
			{/if}
			</td>
		 </tr>	
		 <tr>
			<td width="30%" align="left" valign="top" class="plaintxt">Description : </td>
			<td width="70%" align="left" valign="middle">
			{if $FabricArr.lesson_status_id neq '4' && $FabricArr.lesson_status_id neq '5'}
			<label>
			<textarea name="lesson_description" id="lesson_description"  class="bigger" style="height:110px; margin-left:7px;" />{$FabricArr.lesson_description}</textarea></label>
			{else}
			<label style="color:#000000; padding-left:5px;">{$FabricArr.lesson_description}</label>
			{/if}
			</td>
		 </tr>	
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Price <span style="color:#FF0000;">*</span>:</td>
			<td width="70%" align="left" valign="middle">
			{if $FabricArr.lesson_status_id neq '4' && $FabricArr.lesson_status_id neq '5'}
			<label style="color:#000000;">
			<b style="float:left; padding-top:0px;">$</b>&nbsp;<input type="text" name="lesson_price" id="lesson_price" value="{$FabricArr.lesson_price}" class="textbox" style="width:100px;"/></label>
			{else}
			<label style="color:#000000; padding-left:5px;"><b>$</b>&nbsp;{$FabricArr.lesson_price}</label>
			{/if}
			</td>
		 </tr>
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Admin Commission <span style="color:#FF0000;">*</span>:</td>
			<td width="70%" align="left" valign="middle">
			{if $FabricArr.lesson_status_id neq '4' && $FabricArr.lesson_status_id neq '5'}
			<label style="color:#000000;">
			<input type="text" name="lesson_commision_admin" id="lesson_commision_admin" value="{$FabricArr.lesson_commision_admin}" class="textbox" style="width:50px; margin-left:7px;"/><b style="float:left; padding-top:0px;">&nbsp;%</b></label>
			{else}
			<label style="color:#000000; padding-left:5px;">{$FabricArr.lesson_commision_admin}&nbsp;<b>%</b></label>
			{/if}
			</td>
		 </tr>
		 <tr>
			<td height="28" align="left" class="plaintxt">Status :</td>
			<td height="28" align="left" class="maintext">
			{if $FabricArr.lesson_status_id neq '4' && $FabricArr.lesson_status_id neq '5'}
				<select name="lesson_status_id" id="lesson_status_id" class="selectbox" style="width:250px; margin-left:7px;" onchange="checkPaid(this.value)">
					{section name=Row loop=$SelectStatusArr}
					<option value="{$SelectStatusArr[Row].lesson_status_id}" {if $SelectStatusArr[Row].lesson_status_id eq $FabricArr.lesson_status_id} selected {/if}>{$SelectStatusArr[Row].lesson_status_title}</option>
					{/section}
				</select>
			{else}
				<label style="font-family: 'Trebuchet MS','Myriad Pro',Arial,Verdana,sans-serif;font-size: 13px; color:#000000; margin-left:7px;">{$FabricArr.status_name}</label>
			{/if}		
				
			</td>
		  </tr>
  		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">&nbsp; </td>
			<td width="70%" align="left" valign="middle">
			<div style="display:none;" id="show_row">
			<div style="float:left; padding-left:7px;"><input type="text" name="paid_date" id="paid_date" value="{$FabricArr.lesson_payment_date}" class="datebox" style="width:100px;" onfocus="date_focus();" onblur="date_blur();" readonly=""/><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.frm_admin.paid_date,document.frm_admin.paid_date);return false;" hidefocus><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17" /></a></div>
			<div style="float:left; color:#000000; padding-left:20px;"><input type="text" name="transaction_no" id="transaction_no" value="{$FabricArr.lesson_payment_transaction_no}" style="width:200px;" class="datebox" onfocus="transc_focus();" onblur="transc_blur();"/></div>
			</div></td>
		 </tr>
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Coach : </td>
			<td width="70%" align="left" valign="middle"><label style="font-family: 'Trebuchet MS','Myriad Pro',Arial,Verdana,sans-serif;font-size: 13px; color:#000000; margin-left:7px;">
			{$FabricArr.coach_name}&nbsp;(&nbsp;Paypal Email ID:&nbsp;{$FabricArr.paypal_email_id}&nbsp;)</label></td>
		 </tr>
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Student : </td>
			<td width="70%" align="left" valign="middle"><label style="font-family: 'Trebuchet MS','Myriad Pro',Arial,Verdana,sans-serif;font-size: 13px; color:#000000; margin-left:7px;">{$FabricArr.student_name}</label></td>
		 </tr>	 
 		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Payment Date : </td>
			<td width="70%" align="left" valign="middle"><label style="font-family: 'Trebuchet MS','Myriad Pro',Arial,Verdana,sans-serif;font-size: 13px; color:#000000; margin-left:7px;">{if $FabricArr.lesson_payment_date neq '0000-00-00'}{$FabricArr.lesson_payment_date}{else}N/A{/if}</label></td>
		 </tr>	 
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Closed Date : </td>
			<td width="70%" align="left" valign="middle"><label style="font-family: 'Trebuchet MS','Myriad Pro',Arial,Verdana,sans-serif;font-size: 13px; color:#000000; margin-left:7px;">
				{if $FabricArr.status_name neq 'Closed'}
					{if $FabricArr.lesson_closed_date neq '0000-00-00'}
						{$FabricArr.lesson_closed_date}
					{else}
						N/A
					{/if}
				{else}
					 N/A
				{/if}
			</label>
			</td>
		 </tr>	 
		 <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">
			{if $FabricArr.lesson_status_id neq '4' && $FabricArr.lesson_status_id neq '5'}
			 <input name="lesson_id" id="lesson_id" type="hidden" value="{$lesson_id}"/>
			 <input name="imageField" type="submit" class="addsadmin" value="Update" />
&nbsp;&nbsp;&nbsp;<input name="" type="button" class="cancel" value="Cancel" 
onclick="window.location.href='lesson_manager.php?IsPreserved=Y'"/>{else}<input name="" type="button" class="cancel" value="Back" 
onclick="window.location.href='lesson_manager.php?IsPreserved=Y'"/>{/if}</td>
		  </tr>
		  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  </tr>
		</table>
</form>

	</div>
 </div>
    <p>&nbsp;</p>
{include file="bottom.tpl"}
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>