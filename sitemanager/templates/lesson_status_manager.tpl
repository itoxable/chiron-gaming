{if $IsProcess neq "Y"}
{include file="top.tpl"}
{literal}  
<script type="text/javascript">
var j = jQuery.noConflict();
j(document).ready(function(){
		
focus_blur('Search_language','Search by lesson status title');

});

</script>
{/literal}
<h2> Lesson Status <div id="TransMsgDisplay" align="right" style="height:3px" ></div></h2>
	
<form method="POST" name="form_search" id="form_search" action="{$page_name}" onsubmit="return false;">
	<table width="80%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="56%" align="right" valign="middle">	
			<input size="30" type="text" name="Search_language" id="Search_language" value="{$Search_language}" class="textbox search-box"/>	
			<input type="hidden" name="dosearch" value="GO">
			</td>
			
			<td valign="middle">
			<input  name="submitdosearch" value="" type="button" class="go" onClick="ManagerGeneralForm('{$page_name}?action=list_search','form_search')" />
			<input name="" type="button"  class="reset" value="" onClick="window.location.href='{$page_name}';"/></td>
		</tr>
	
	</table>
</form>
<div align="right" class="addNews_secand"><a href="lesson_status_update.php">+ Add Lesson Status</a></div>
{/if}
<div id="records_listing">	
{if $MessgReportText neq ""}<div align="left" class="successful2" style="color:#FF0000; margin:-25px 0 0 0;">{$MessgReportText}</div>{/if}
				
<div class="newsName">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td colspan="5" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#9f9f9f">
				  <tr  bgcolor="#9f9f9f">
					<td  align="left" width="45%" valign="middle" class="padleft">
							<span class="whitetext"><strong>Title</strong></span>									
					</td>
					<td width="15%" align="center" valign="middle" >
						<a href="#"  onClick="ManagerGeneral('{$page_name}?action=list_order&do_order=GO&OrderByID=2&OrderType={$OrderType}&{$SearchLink}&from={$from}')" >
							<span class="whitetext"><strong>Date Added</strong></span>
							{$ReturnSortingArr.DisplaySortingImage[1]}			
						</a>	
					</td>
					<td width="15%" align="center" valign="middle" >
							<span class="whitetext"><strong>Date Edited</strong></span>
					</td>
					<td width="10%" align="center" class="whitetext" valign="middle"><strong>Active</strong></td>	
											
					<td width="20%" align="center" class="whitetext" valign="middle"><strong>Actions</strong></td>
				  </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="5" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#878787">
				{if $NumSelectCms > 0}
			{section name=RowCms loop=$SelectCmsArr}
			
				  <tr {if ($templatelite.section.RowCms.index % 2) eq 0} bgcolor="#ffffff" {else} bgcolor="#ffffff" {/if}>
					<td  align="left" width="45%" valign="middle" class="simpletxt">
					{$SelectCmsArr[RowCms].lesson_status_title}
					</td>
					<td width="15%" class="simpletxt" align="center" valign="middle">{$SelectCmsArr[RowCms].date_added}</td>
					<td width="15%" class="simpletxt" align="center" valign="middle">{$SelectCmsArr[RowCms].date_edited}</td>
					<td width="10%" height="28" align="center" valign="middle"><a href="#" onClick="ManagerGeneral('{$page_name}?action=activate&{$PreserveLink}&record_id={$SelectCmsArr[RowCms].lesson_status_id}')">
					<img src="images/{$SelectCmsArr[RowCms].active_img}" width="15" height="14" border="0" alt="{$SelectCmsArr[RowCms].active_alt}" /></a></td>
					
					<td width="20%" align="center" valign="middle">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
						  <td width="50%" height="32" align="center" valign="middle" class="rightBorder" >
						  <a href="lesson_status_update.php?lesson_status_id={$SelectCmsArr[RowCms].lesson_status_id}">
						  <img src="images/edit_dark.gif" alt="" width="15" height="14" border="0" />
						  </a>
						  </td>
						  
						  <td width="50%" align="center" valign="middle">
						  <a href="#" onClick="ConfirmDelete('{$page_name}?action=del&{$PreserveLink}','{$SelectCmsArr[RowCms].lesson_status_id}','{$SelectCmsArr[RowCms].lesson_status_title}','Status:: ')">
						  <img src="images/delete_icon.gif" border="0" alt="" />
						  </a>
						  </td>
						  </tr>
					</table>					
					</td>
				 </tr>
				  {/section}
			{else}
				<tr bgcolor="#f4f4f5"><td class="plaintxt" colspan="5" align="center">No Record Found</td></tr>
			{/if}       
				  
				 
			  </table>
			</td>
		  </tr>
		 
  </table>
</div>
<br />
<div>	{if $pagination_arr[3] > 0}
   <table>
	  <tr>
        <td height="30" valign="middle" align="center">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td align="left" class="plaintxt">					
					{if $pagination_arr[2] neq ""}					
						{$pagination_arr[2]}
					{/if}					
					</td>
					<td align="right" class="plaintxt">					   	
					{if $pagination_arr[1] neq ""}					
						{$pagination_arr[1]}
					{/if}					
					</td>
				</tr>
			</table>
		</td>
      </tr>
	  </table>
	{/if} </div>
</div>

{if $IsProcess neq "Y"}  	
{include file="bottom.tpl"}
{/if}
