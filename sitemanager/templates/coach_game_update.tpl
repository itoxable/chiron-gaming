{include file="top.tpl"}
{literal}
<script language="javascript" type="text/javascript">
/*j(document).ready(function(){
	getPageId('{/literal}{$FabricArr.game_id}{literal}','{/literal}{$FabricArr.ladder_id}{literal}','{/literal}{$FabricArr.race_id}{literal}','{/literal}{$FabricArr.server_id}{literal}');
});
*/
function getPageId(type,id,rid,sid)
{	
	
	j.ajax({
			type:'POST',
			url:'get_game_ladder.php',
			data: 'type='+type+'&id='+id+'&rid='+rid+'&sid='+sid,
			dataType:'json',
			success:function(jData){
			
				if(jData.flag == 1)
				{
					j('#ladder_id_td').html(jData.html);
					j('#race_id_td').html(jData.html1);
					j('#server_id_td').html(jData.html2);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				j(".contactus-block").html("Some server error occoured. Please report to system administrator.</div>");
			}
		});
}

function getChangeGame(id)
{	
	//alert(id);
	j('#ladder').hide();
	j('#race').hide();
	j('#region').hide();
	j('#server').hide();
	j('#rating').hide();
	j('#ladder_gap').hide();
	j('#race_gap').hide();
	j('#region_gap').hide();
	j('#server_gap').hide();
	j('#rating_gap').hide();
	j.ajax({
			url:'game_details.php',
			data: 'id='+id,
			type:'post',		
			dataType:'json',	
		   success:function(resp){
		   //alert(resp.server);
		   if(resp.ladder=='Y')
		   {
		   		j('#ladder').show();
				j('#ladder_gap').show();
			}			
			 if(resp.race=='Y')
			 {
		   		/*j('#race').css('display','block');
				j('#race').html(resp.race_str);*/
				j('#race').show();
				j('#race_gap').show();
			}	
			 if(resp.region=='Y')
		   	{
				j('#region').show();				
				j('#region_gap').show();
			}	
			 if(resp.server=='Y')
		   	{
				j('#server').show();
				j('#server_gap').show();
			}	
			 if(resp.rating=='Y')
		   	{
				j('#rating').show();
				j('#rating_gap').show();
			}
				}
		});
}
</script>
{/literal}

<h2>{$SubmitButton}</h2>
<div class="tabarea">
<div class="tabcontent">
<p align="right" class="fields">Fields marked with <span class="red">*</span> are required</p>
<form action="coach_game_update.php?action=trans" method="post" name="frm_admin" id="frm_admin" >
<input type="hidden" name="u_id" value="{$u_id}" />
		<table width="100%" border="0" cellspacing="4" cellpadding="0">
		  {if $err_msgs neq ""}     
			<tr>
				<td class="simpleText" colspan="2"><ul>{$err_msgs}</ul></td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
			 </tr>
		  {/if}
		 
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Game<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			 <select name="game_id" id="game_id" class="selectbox" onchange="getChangeGame(this.value)">
				<option value="">Select</option>				
				{html_options options=$GameArr selected=$FabricArr.game_id}
			</select>
			</label></td>
		 </tr>
		 {if $FabricArr.is_ladder eq 'Y'}
		   <tr id="ladder_gap">
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		  {/if}
		 <tr id="ladder" {if $FabricArr.is_ladder neq 'Y'} style='display:none' {/if}>
			<td height="28" align="left" class="plaintxt">Ladder:</td>
			<td height="28" align="left" class="maintext" id="ladder_id_td">
				<select name="ladder_id" id="ladder_id">
					<option value="">Select</option>
					{html_options options=$LadderArr selected=$FabricArr.ladder_id}
				</select>
			</td>
		 </tr>
		<!-- <tr id="ladder" {if $FabricArr.is_ladder neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Ladder<span class="red">*</span>:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $LadderArr|@count neq 0}
		 	{section name=RowLadder loop=$LadderArr}
			<input type="checkbox" name="submited_ladder[]"  value="{$LadderArr[RowLadder].ladder_id}" 
{if $LadderArr[RowLadder].ladder_id|inarray:$submited_ladder}checked{/if} /> 
			{$LadderArr[RowLadder].ladder_name}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>-->
		
		  <tr id="race_gap" {if $FabricArr.is_race neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>

		<!-- <tr>
			<td height="28" align="left" class="plaintxt">Race<span class="red">*</span>:</td>
			<td height="28" align="left" class="maintext" id="race_id_td">
				<select name="race_id" id="race_id">
					<option value="">Select</option>
				</select>
			</td>
		 </tr>-->
		  <tr id="race"   {if $FabricArr.is_race neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Race:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $RaceArr|@count neq 0}
		 	{section name=RowRace loop=$RaceArr}
			<input type="checkbox" name="submited_race[]"  value="{$RaceArr[RowRace].race_id}" 
{if $RaceArr[RowRace].race_id|inarray:$submited_race}checked{/if} /> 
			{$RaceArr[RowRace].race_title}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		  <tr id="rating_gap" {if $FabricArr.is_rating neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		 
		 <!--<tr>
			<td height="28" align="left" class="plaintxt">Server<span class="red">*</span>:</td>
			<td height="28" align="left" class="maintext" id="server_id_td">
				<select name="server_id" id="server_id">
					<option value="">Select</option>
				</select>
			</td>
		 </tr>-->
		  <tr id="rating" {if $FabricArr.is_rating neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Rating:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $RatingArr|@count neq 0}
		 	{section name=RowRating loop=$RatingArr}
			<input type="checkbox" name="submited_rating[]"  value="{$RatingArr[RowRating].rating_id }" 
{if $RatingArr[RowRating].rating_id|inarray:$submited_rating}checked{/if} /> 
			{$RatingArr[RowRating].rating_title}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		
		  <tr id="region_gap" {if $FabricArr.is_region neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		 
		 <tr id="region" {if $FabricArr.is_region neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Region:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $RegionArr|@count neq 0}
		 	{section name=RowRegion loop=$RegionArr}
			<input type="checkbox" name="submited_region[]"  value="{$RegionArr[RowRegion].region_id }" 
{if $RegionArr[RowRegion].region_id|inarray:$submited_region}checked{/if} /> 
			{$RegionArr[RowRegion].region_title}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		
		  <tr id="server_gap" {if $FabricArr.is_server neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
	
		 <tr id="server" {if $FabricArr.is_server neq 'Y'} style='display:none' {/if}>
			<td align="left" valign="top" class="plaintxt">Server:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">
			{if $ServerArr|@count neq 0}
		 	{section name=RowServer loop=$ServerArr}
			<input type="checkbox" name="submited_server[]"  value="{$ServerArr[RowServer].server_id}" 
{if $ServerArr[RowServer].server_id|inarray:$submited_server}checked{/if} /> 
			{$ServerArr[RowServer].server_name}		
			<br />
			{/section}
			{/if}
			</td>	
		</tr>
		   <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		 <!--tr>
			<td width="20%" align="left" valign="top" class="plaintxt">Experience:</td>
			<td width="80%" align="left" valign="middle"><label>
			<input type="text" name="experience" id="experience" class="textbox" value="{$FabricArr.experience}"/>
			</label></td>
	     </tr-->
      <tr>
		<td width="20%" valign="top" align="left" class="plaintxt">Rate<span class="red">*</span> ($):</td>
		<td width="80%" valign="middle" align="left"><label>
		<input type="text" onkeypress="return floatnumbersonly(this,event)" value="{$FabricArr.rate}" class="textbox" id="rate" name="rate">
		</label></td>
	 </tr>
	 	   
	 <tr>
			<td height="28" align="left" class="plaintxt">Active:</td>
			<td height="28" align="left" class="maintext">
				<input name="is_active" id="is_active" type="checkbox" class="maintext" value="Y" {if $FabricArr.is_active =='Y'}checked{/if}>
			</td>
	</tr>
      <tr>
			<td align="left" valign="top" class="plaintxt">Experience:</td>
			<td align="left" valign="top"><label>
			  <textarea rows="3" cols="23"  name="experience" id="experience" class="bigger" style="height:110px;">{$FabricArr.experience}</textarea>
			</label></td>
		  </tr>
		  <tr>
		  	<td colspan="2"></td>
      </tr>
	  <tr>
			<td align="left" valign="top" class="plaintxt">Lesson Plan:</td>
			<td align="left" valign="top"><label>
			  <textarea rows="3" cols="23"  name="lesson_plan" id="lesson_plan" class="bigger" style="height:110px;">{$FabricArr.lesson_plan}</textarea>
			</label></td>
		  </tr>
		  <tr>
		  	<td colspan="2"></td>
      </tr>
		 
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">
			 <input name="user_game_id" id="user_game_id" type="hidden" value="{$user_game_id}"/>
			 <input name="u_id" id="u_id" type="hidden" value="{$u_id}"/>
			 <input name="imageField" type="submit" class="addsadmin" value="{$SubmitButton}" {if $user_game_id eq ''} style="width:145px;"{else}style="width:145px;"{/if}/>
&nbsp;&nbsp;&nbsp;<input name="" type="button" class="cancel" value="Cancel" onclick="window.location.href='coach_game_manager.php?u_id={$u_id}&IsPreserved=Y'"/></td>
		  </tr>
		  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  </tr>
		</table>
</form>

	</div>
 </div>
    <p>&nbsp;</p>
{include file="bottom.tpl"}
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>