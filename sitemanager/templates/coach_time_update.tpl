{include file="top.tpl"}
{literal}
<script language="javascript" type="text/javascript" src="js/datetimepicker.js"></script>
<script src="js/jquery.clockpick.1.2.7.js" ></script>
<link rel="stylesheet" href="style/jquery.clockpick.1.2.7.css" type="text/css">
<script>
j(document).ready(function(){
j("#clockpick1").clockpick({ valuefield: 'time_from',starthour: 0,endhour:23,minutedivisions:1 }); 
j("#clockpick2").clockpick({ valuefield: 'time_to',starthour: 0,endhour:23,minutedivisions:1 }); 
});
</script>

{/literal}

<h2>{$SubmitButton}</h2>
<div class="tabarea">
<div class="tabcontent">
<p align="right" class="fields">Fields marked with <span class="red">*</span> are required</p>
<form action="coach_time_update.php?user_id={$user_id}&action=trans" method="post" name="frm_admin" id="frm_admin" >
		<table width="100%" border="0" cellspacing="4" cellpadding="0">
		  {if $err_msgs neq ""}     
			<tr>
				<td class="simpleText" colspan="2"><ul>{$err_msgs}</ul></td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
			 </tr>
		  {/if}
		 
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Time Slot<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			 <select name="time_slot_id" id="time_slot_id" class="selectbox" onChange="getTimeval(this.value)">
				<option value="">Select</option>
				{html_options options=$TimeArr selected=$FabricArr.time_slot_id}
			</select>
			</label></td>
		 </tr>
		<tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Time From<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <input type="text" name="time_from" id="time_from" readonly="" 
			  value="{$FabricArr.time_from}" class="datebox"/>
					<a href="#"><img src="images/clock.gif" id="clockpick1" align="left" class="PopcalTrigger" /> </a>
					<p class="clear"></p>
			</label></td>
		 </tr>
		 
		  <tr>
		  	<td colspan="2"></td>
		  </tr>
		  <tr>
			<td align="left" valign="middle" class="plaintxt">Time To<span class="red">*</span>:</td>
			<td align="left" valign="middle"><label>
			 <input type="text" name="time_to" id="time_to" readonly="" value="{$FabricArr.time_to}" class="datebox"/>
					<a href="#"><img src="images/clock.gif" id="clockpick2" align="left" class="PopcalTrigger" /> </a>
					<p class="clear"></p>
			</label></td>
		  </tr>
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		  </tr>
		  <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">
			 <input name="timing_id" id="timing_id" type="hidden" value="{$timing_id}"/>
			 <input name="user_id" id="user_id" type="hidden" value="{$user_id}"/>
			 <input name="imageField" type="submit" class="addsadmin" value="{$SubmitButton}" {if $timing_id eq ''} style="width:120px;"{else}style="width:145px;"{/if}/>
&nbsp;&nbsp;&nbsp;<input name="" type="button" class="cancel" value="Cancel" onclick="window.location.href='coach_time_manager.php?user_id={$user_id}&IsPreserved=Y'"/></td>
		  </tr>
		  <tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  </tr>
		</table>
</form>

	</div>
 </div>
    <p>&nbsp;</p>
{include file="bottom.tpl"}
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>