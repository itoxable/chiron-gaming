
{include file="top.tpl"}
    
<script src="js/game_manager.js" type="text/javascript"></script>
<script src="js/jquery-1.8.0.min.js" type="text/javascript"></script>

<div style="border: 1px solid black" id="games_wrap"></div>

<div id="overlay"></div>

{literal}  
<script>
    GameManagement.start('#games_wrap');
</script>
{/literal}  
{include file="bottom.tpl"}
