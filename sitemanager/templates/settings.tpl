{if $IsProcess neq "Y"}
{include file="top.tpl"}  

<h2> Settings </h2>
{/if}
<form method="POST">
    <table cellspacing="10" style="color: #000">
        <tr>
            <td>
                <span>PayPal Id</span>
            </td>
            <td>
                <input type="text" name="paypalid" id="paypalid" value="{$paypalid}" style="padding: 2px 5px" />
            </td>
        </tr>
        <tr>
            <td>
                <span>PayPal URL</span>
            </td>
            <td>
                <input type="text" name="paypalurl" id="paypalurl" value="{$paypalurl}" style="padding: 2px 5px"/>
            </td>
        </tr>
        <tr>
            <td>
                <span>Facebook URL</span>
            </td>
            <td>
                <input type="text" name="facebook" id="facebook" value="{$facebook}" style="padding: 2px 5px"/>
            </td>
        </tr>
        <tr>
            <td>
                <span>Twitter Id</span>
            </td>
            <td>
                <input type="text" name="twitter" id="twitter" value="{$twitter}" style="padding: 2px 5px"/>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <input type="submit" name="action" id="action" value="submit" style="padding: 2px 5px"/>
            </td>
        </tr>
    </table>
</form>

{if $IsProcess neq "Y"}  	
{include file="bottom.tpl"}
{/if}
