<?php
/*******************************************************************************/
			#This page is to add/edit ladder details
			#last Updated : August 23 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$game_id =$_REQUEST['game_id'];
$team_id =$_REQUEST['team_id'];
$team_title=$_REQUEST['team_title'];
$action=$_REQUEST['action'];
if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	/* Holding Data If Error Starts */

	$FabricArr['team_title']		 = htmlspecialchars(trim($team_title));
	$FabricArr['is_active']			 = $is_active;
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
	
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_game_team","team_title",$team_title,"Team title","team_id",$team_id,"game_id",$game_id);
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{
			
		if(empty($team_id))
		{
			
			$table_name = TABLEPREFIX."_game_team ";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'team_title'					    => $team_title,
									'is_active'  						=> $is_active,
									'date_added' 						=> date("Y-m-d H:i:s")
									);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			$server_id=mysql_insert_id();	

			/* Insert Into Events Ends */
			
		}
		else if(!empty($team_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_game_team";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'team_title'					    => $team_title,
									'is_active'  						=> $is_active,
									'date_edited' 						=> date("Y-m-d H:i:s")
									);

			$where="team_id='$team_id'";										
			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */	
		}		
		echo "<script>window.location.href='team_manager.php?messg=".$msgreport."&game_id=".$game_id."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($team_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_game_team WHERE team_id='".$team_id."' AND game_id='".$game_id."'";
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	$FabricArr['team_title'] 				= show_to_control($RsCatSql["team_title"]);
	$FabricArr['date_edited'] 				= $RsCatSql["date_edited"];
	$FabricArr['date_added'] 				= $RsCatSql["date_added"];
	$FabricArr['is_active']					= $RsCatSql["is_active"];

	/* Get Record For Display Ends */

	$SubmitButton="Update Team";	
	$flag=1;
}
else
{
	$SubmitButton=" Add Team ";
}


/* Assign Smarty Variables Starts */

$smarty->assign('game_id',$game_id);
$smarty->assign('team_id',$team_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("team_update.tpl");
?>