<?php
/*******************************************************************************/
	#This page lists all the language we have in table
	#last Updated : Aug 23 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$page_name="lesson_manager.php";

$action=$_REQUEST['action'];

$IsPreserved=$_REQUEST['IsPreserved'];

if($IsPreserved=="Y")
{
/* preserve select starts */
$PreserveVariableArr=$preserve_variable_object->PreserveVariableSelect();
foreach($PreserveVariableArr as $a=>$b){$$a=$b;}
/* preserve select ends */
}

/* Activate Operation Starts */
/*if($action=="activate")
{
	$ChangeStatusObjAjax->ChangeStatus($record_id,1);	
}*/
/* Activate Operation Ends */


/* Delete Operation Starts */

if($action=="del")
{	
	if(!empty($delete_id))
	{
		
		/*   Set order After delete Starts  */
		$RankingObjAjax->RankingDelete($delete_id);			
		/*   Set order After delete Ends  */
		
		/* Delete Events Starts */
		$SqlDelete="DELETE FROM ".TABLEPREFIX."_lesson WHERE lesson_id='$delete_id'";
		$ResultDelete=$adodbcon->Execute($SqlDelete);		
		/* Delete Events Ends */

		if($adodbcon->ErrorNo()) 
			$messg = 5;
		else 
			$messg = 6;
	}	
	else
	{
		$messg = 7;
	}		
}

/* Delete Operation Ends */


/* IsProcess Starts */

$action_arr=array("list_order","ranking","activate","list_paginate","list_search","del");

if(in_array($action,$action_arr))
{
	$smarty->assign('IsProcess',"Y");
}

/* IsProcess Ends */

	/* listing Operation Starts */

	/* search section starts */

	if($dosearch=="GO")
	{ 
		/*echo '<pre>';
		print_r($_REQUEST);
		exit;*/
		$Search_lesson_title = $Search_lesson_title=='Search by title'?'':$Search_lesson_title;
		$Search_student_id = $Search_student_id;
		$Search_lesson_status = $Search_lesson_status;
		$Search_from_date = $Search_from_date=='From'?'':$Search_from_date." 00:00:00";
		$Search_to_date = $Search_to_date=='To'?'':$Search_to_date." 23:59:59";
		
		if(!empty($Search_lesson_title))
		{
			 $searchSql =  $searchSql." AND lesson_title like '%".trim(mysql_quote($Search_lesson_title,"N"))."%'"; 
		}
		
		if(!empty($Search_student_id))
		{
			 $searchSql =  $searchSql." AND student_id=".$Search_student_id; 
		}
		
		if(!empty($Search_coach_id))
		{
			 $searchSql =  $searchSql." AND coach_id=".$Search_coach_id; 
		}
		
		if(!empty($Search_lesson_status))
		{
			 $searchSql =  $searchSql." AND lesson_status_id=".$Search_lesson_status; 
		}
		
		if(!empty($Search_from_date))
		{
			 $searchSql =  $searchSql." AND date_added >= '".mysql_quote($Search_from_date,"N")."'"; 
		}
		
		if(!empty($Search_to_date))
		{
			 $searchSql =  $searchSql." AND date_added <= '".mysql_quote($Search_to_date,"N")."'"; 
		}
		
		$SearchLink="dosearch=GO&Search_language=$Search_language";
	}	

	/* search section ends */

	/* Order section starts */	

	$SortingSequenceArr=array(1 => "lesson_title",2 => "date_added",3 => "lesson_payment_date",4 => "lesson_price",);	
	$ReturnSortingArr=$SortingObjAjax->Sorting("date_edited desc",$SortingSequenceArr,$do_order,$OrderByID,$OrderType);	
	$OrderBySql=$ReturnSortingArr['OrderBySql'];
	$OrderLink=$ReturnSortingArr['OrderLink'];
	$OrderType=$ReturnSortingArr['OrderType'];			

	/* Order section ends */

	$SqlSelectCat="SELECT * FROM ".TABLEPREFIX."_lesson Where 1=1 ".$searchSql.$OrderBySql;
	/* pagination starts */	

	$pagination_arr=$PaginationObjAjax->PaginationAjax($SqlSelectCat,$page_name."?action=".$action."&".$SearchLink."&".$OrderLink,"ManagerGeneral");	

	/* pagination ends */ 

	/* preserve update starts */

	$PreserveLink="from=$from&".$SearchLink."&".$OrderLink;

	$preserve_variable_object->PreserveVariableUpdate($PreserveLink);

	/* preserve update ends */		


	#Fetch all events and store them in an array
	$SelectCmsArr=$adodbcon->GetAll($pagination_arr[0]);	

	$NumSelectCms=count($SelectCmsArr);
	
	$total=0; $total_price=0;

	for($i=0;$i<$NumSelectCms;$i++)
	{				
		$SelectCmsArr[$i]['lesson_id'] 		= $SelectCmsArr[$i]['lesson_id'];
		$SelectCmsArr[$i]['lesson_title'] 	= addslashes(show_to_control($SelectCmsArr[$i]['lesson_title']));
		$SelectCmsArr[$i]['coach_id'] 		= $SelectCmsArr[$i]['coach_id'];
		
		$Sql_Coach="SELECT name FROM ".TABLEPREFIX."_user WHERE user_id=".$SelectCmsArr[$i]['coach_id'];
		$Coach_Name = $AdminManagerObjAjax->GetRecords("Row",$Sql_Coach);
		
		$SelectCmsArr[$i]['coach_name'] 	= $Coach_Name['name'];
		
		$Sql_CoachPaypal="SELECT paypal_email_id FROM ".TABLEPREFIX."_user WHERE user_id=".$SelectCmsArr[$i]['coach_id'];
		$Coach_Paypal = $AdminManagerObjAjax->GetRecords("Row",$Sql_CoachPaypal);
		
		$SelectCmsArr[$i]['paypal_email_id'] 	= $Coach_Paypal['paypal_email_id'];
		
		$SelectCmsArr[$i]['student_id'] 	= $SelectCmsArr[$i]['student_id'];
		
		$Sql_Student="SELECT name FROM ".TABLEPREFIX."_user WHERE user_id=".$SelectCmsArr[$i]['student_id'];
		$Student_Name = $AdminManagerObjAjax->GetRecords("Row",$Sql_Student);
		
		$SelectCmsArr[$i]['student_name'] 	= $Student_Name['name'];
		
		$SelectCmsArr[$i]['lesson_price'] 	= number_format($SelectCmsArr[$i]['lesson_price'], 2, '.', '');
		$SelectCmsArr[$i]['lesson_status_id'] 	= $SelectCmsArr[$i]['lesson_status_id'];
		
		$Sql_Status="SELECT lesson_status_title FROM ".TABLEPREFIX."_lesson_status WHERE lesson_status_id=".$SelectCmsArr[$i]['lesson_status_id'];
		$Status_Name = $AdminManagerObjAjax->GetRecords("Row",$Sql_Status);
		
		$SelectCmsArr[$i]['status_name'] 	= $Status_Name['lesson_status_title'];
		
		$SelectCmsArr[$i]['lesson_commision_admin'] = $SelectCmsArr[$i]['lesson_commision_admin'];
		$SelectCmsArr[$i]['admin_income'] = number_format(($SelectCmsArr[$i]['lesson_price']*($SelectCmsArr[$i]['lesson_commision_admin']/100)), 2, '.', '');
		$SelectCmsArr[$i]['date_added']	= date_format_admin($SelectCmsArr[$i]['date_added']);
		$SelectCmsArr[$i]['lesson_payment_date']  = date_format_admin($SelectCmsArr[$i]['lesson_payment_date']);
		$SelectCmsArr[$i]['lesson_closed_date']	 = date_format_admin($SelectCmsArr[$i]['lesson_closed_date']);
		
		$SelectCmsArr[$i]['lesson_price_deducted']=$SelectCmsArr[$i]['lesson_price']*($SelectCmsArr[$i]['lesson_commision_admin']/100);
		if($SelectCmsArr[$i]['lesson_status_id']==4){		
		
			$total=number_format(($total + $SelectCmsArr[$i]['lesson_price_deducted']), 2, '.', '');
		}
		
		$total_price=$total_price+$SelectCmsArr[$i]['lesson_price'];
	}
/* listing Operation Ends */

/* Student Record */
$StudentRecSql="SELECT us.name,les.student_id FROM ".TABLEPREFIX."_lesson AS les,".TABLEPREFIX."_user AS us WHERE les.student_id=us.user_id GROUP BY us.name";
$StudentRecArr=$AdminManagerObjAjax->GetRecords("All",$StudentRecSql);
for($i=0;$i<count($StudentRecArr);$i++)
{
$SelectStudentArr[$i]['student_id'] = $StudentRecArr[$i]['student_id'];

$SelectStudentArr[$i]['student_name'] 	= $StudentRecArr[$i]['name'];
}	
/* Student Record */

/* Coach Record */
$CoachRecSql="SELECT us.name,les.coach_id FROM ".TABLEPREFIX."_lesson AS les,".TABLEPREFIX."_user AS us WHERE les.coach_id=us.user_id GROUP BY us.name";
$CoachRecArr=$AdminManagerObjAjax->GetRecords("All",$CoachRecSql);
for($i=0;$i<count($CoachRecArr);$i++)
{
$SelectCoachArr[$i]['coach_id'] = $CoachRecArr[$i]['coach_id'];
$SelectCoachArr[$i]['coach_name'] = $CoachRecArr[$i]['name'];
}	
/* Coach Record */

/* Status Record */
$StatusRecSql="SELECT lesson_status_id,lesson_status_title FROM ".TABLEPREFIX."_lesson_status ";
$StatusRecArr=$AdminManagerObjAjax->GetRecords("All",$StatusRecSql);
for($i=0;$i<count($StatusRecArr);$i++)
{
$SelectStatusArr[$i]['lesson_status_id'] = $StatusRecArr[$i]['lesson_status_id'];
$SelectStatusArr[$i]['lesson_status_title']	= $StatusRecArr[$i]['lesson_status_title'];
}	
/* Status Record */

/*echo '<pre>';
print_r($SelectStatusArr);*/
$MessgReportText=displayMessage($messg);

/* Assign Smarty Variables Starts */

$smarty->assign("brcrumb", $breadcrumbArr);
$smarty->assign("MessgReportText",$MessgReportText);
$smarty->assign('page_name',$page_name);
$smarty->assign('from',$from);
$smarty->assign('PreserveLink',$PreserveLink);
$smarty->assign('SearchLink',$SearchLink);
$smarty->assign('dosearch',$dosearch);
$smarty->assign('OrderLink',$OrderLink);
$smarty->assign('do_order',$do_order);
$smarty->assign('OrderType',$OrderType);
$smarty->assign('OrderByID',$OrderByID);
$smarty->assign('ReturnSortingArr',$ReturnSortingArr);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('NumSelectCms',$NumSelectCms);
$smarty->assign('SelectCmsArr',$SelectCmsArr);
$smarty->assign('SelectCoachArr',$SelectCoachArr);
$smarty->assign('SelectStudentArr',$SelectStudentArr);
$smarty->assign('SelectStatusArr',$SelectStatusArr);
$smarty->assign("Search_lesson_title",$Search_lesson_title);
$smarty->assign("Search_student_id",$Search_student_id);
$smarty->assign("Search_lesson_status",$Search_lesson_status);
$smarty->assign("Search_from_date",$Search_from_date);
$smarty->assign("Search_to_date",$Search_to_date);
$smarty->assign('total',$total);
$smarty->assign('total_in',$total_in);
$smarty->assign('total_price',$total_price);
/* Assign Smarty Variables Ends */

$smarty->display("lesson_manager.tpl");
?>