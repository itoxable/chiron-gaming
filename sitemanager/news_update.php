<?php
/*******************************************************************************/
			#This page is to add/edit game details
			
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$news_id=$_REQUEST['news_id'];
$news_title=$_REQUEST['news_title'];
$action=$_REQUEST['action'];
$photo=$_FILES['photo']['name'];
$image_object->SetImagePath("../uploaded/news_images");

if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	
	/* Holding Data If Error Starts */

	$FabricArr['news_title']			 = htmlspecialchars(trim($news_title));
	$FabricArr['news_content']	         = htmlspecialchars(trim($news_content));
	$FabricArr['news_description']       = $news_description;
	$FabricArr['is_active']				 = $is_active;
	$FabricArr['photo']				     = $photo;
	/* Holding Data If Error Ends */

	/* Error Checking Starts */
	
	$err_msgs="";
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_news","news_title",$news_title,"News Title","news_id",$news_id);
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{		
		
		//move_uploaded_file($_FILES["photo"]["tmp_name"],"../uploaded/news_images/". $_FILES["photo"]["name"]);
		$http_image_name = $_FILES['img1']['name'];
		$http_image_temp = $_FILES['img1']['tmp_name'];
		//$Picc1 = $image_object->UploadImage($PhotoT1,$imghidden1,$http_image_name,$http_image_temp,140,$resize_type=1,99);
		$Picc1 = $image_object->UploadImage($PhotoT1,$imghidden1,$http_image_name,$http_image_temp,54,$resize_type=5,'54','Y',198,$resize_type=5,172);
		
		if(empty($news_id))
		{
	         
			 $table_name = TABLEPREFIX."_news ";

			$fields_values = array( 
									'news_title'        => $news_title,	
									'news_content'      => $news_content,
									'news_description'  => $news_description,								
									'is_active'			=> $is_active,
									'date_added'		=> date("Y-m-d H:i:s"),
									//'photo'		        => $photo
									'photo'		        => $Picc1									
									);		
			//print_r($fields_values);
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			
			$news_id=mysql_insert_id();	

			/* Insert Into Events Ends */
		}
		else if(!empty($news_id))
		{
			/* Update Events Starts */
			
			
			$table_name = TABLEPREFIX."_news";

			//if($_FILES['photo']['name']!="")
			$fields_values = array( 
									'news_title'        => $news_title,	
									'news_content'      => $news_content,	
									'news_description'  => $news_description,							
									'is_active'		    => $is_active,
									'date_added'	    => date("Y-m-d H:i:s"),
									//'photo'		        => $photo
									'photo'		        => $Picc1		
									);	
/*			else
			$fields_values = array( 
									'news_title'         => $news_title,	
									'news_content'  => $news_content,								
									'is_active'		    => $is_active,
									'date_added'	    => date("Y-m-d H:i:s"),
									);	
*/			
			$where="news_id='$news_id'";										

			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */
		}
				
		echo "<script>window.location.href='news_manager.php?messg=".$msgreport."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($news_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_news WHERE news_id=".$news_id;
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
    
	$FabricArr['news_title']            = show_to_control($RsCatSql["news_title"]);
	$FabricArr['news_content']          = show_to_control($RsCatSql["news_content"]);
	$FabricArr['news_description'] 	  	= $RsCatSql["news_description"];
	$FabricArr['photo']					= $RsCatSql["photo"];
	$FabricArr['is_active']			    = $RsCatSql["is_active"];
	
	/* Get Record For Display Ends */

	$SubmitButton="Update News ";	
	$flag=1;
}
else
{
	$SubmitButton=" Add News ";
}

$DisplayImageControlStr=$image_object->DisplayImageControl("Upload Photo","img1",$RsCatSql['photo'],"imghidden1","PhotoT1",$instuctiontype=1,$mandatory="N","Normal",90);

/* Assign Smarty Variables Starts */

$smarty->assign('CmsArr',$CmsArr);
$smarty->assign('news_id',$news_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('DisplayImageControlStr',$DisplayImageControlStr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("news_update.tpl");
?>