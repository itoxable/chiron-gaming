<?php
/*******************************************************************************/
			#This page is to add/edit language details
			
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$default_comision_id=$_REQUEST['default_comision_id'];
$default_comision_value=$_REQUEST['default_comision_value'];
$action=$_REQUEST['action'];

if($action=="trans")
{

	$FabricArr['default_comision_value'] = $default_comision_value;
	
	$err_msgs="";
	$err_msgs .=$AdminManagerObjAjax->Validate($default_comision_value," Default Commission Value","EMP",$type="");
		
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{		

			$table_name = TABLEPREFIX."_default_comision";

			$fields_values = array( 
									'default_comision_value' => $default_comision_value	
									);		
			
			$where="default_comision_id='$default_comision_id'";										

			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */
				//exit; 
		
		echo "<script>window.location.href='default_admin_comission.php?messg=".$msgreport."&IsPreserved=Y'</script>";
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_default_comision";
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
    
	$FabricArr['default_comision_value'] = show_to_control($RsCatSql["default_comision_value"]);
	$FabricArr['default_comision_id']  = $RsCatSql["default_comision_id"];

/* Assign Smarty Variables Starts */

$smarty->assign('CmsArr',$CmsArr);
$smarty->assign('default_comision_id',$default_comision_id);
$smarty->assign('FabricArr',$FabricArr);

/* Assign Smarty Variables Ends */

$smarty->display("default_admin_comission.tpl");
?>