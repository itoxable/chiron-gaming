<?php

include('general_include.php');

include("checklogin.php");

//class Setting{
//	const PAYPAL_ID = 1;
//	const PAYPAL_URL = 2;
//	const FACEBOOK_URL = 3;
//	const TWITTER_ID = 4;
//}

$action=$_REQUEST['action'];

if($action == "submit"){	

    $paypalid = trim($_POST['paypalid']);
    $paypalurl = trim($_POST['paypalurl']);
    $facebook = trim($_REQUEST['facebook']);
    $twitter = trim($_REQUEST['twitter']);
        
    $SqlTruncate="TRUNCATE TABLE ".TABLEPREFIX."_settings";
    $ResultUpdate=$adodbcon->Execute($SqlTruncate);
            
    $SqlInsert = "INSERT INTO ".TABLEPREFIX."_settings (`id`, `name`, `description`, `value`) values 
        (".PAYPAL_ID.",'PAYPAL_ID','PAYPAL_ID','$paypalid'),
        (".PAYPAL_URL.",'PAYPAL_URL','PAYPAL_URL','$paypalurl'),
        (".FACEBOOK_URL.",'FACEBOOK_URL','FACEBOOK_URL','$facebook'),
        (".TWITTER_ID.",'TWITTER_ID','TWITTER_ID','$twitter')";

    $ResultUpdate=$adodbcon->Execute($SqlInsert);		
    
	if($adodbcon->ErrorNo()) 
		$messg = 5;
	else 
		$messg = 6;
			
}  else {
    $paypalid = "";
    $paypalurl = "";
    $facebook = "";
    $twitter = "";
    
    $SqlSettings = "SELECT * FROM ".TABLEPREFIX."_settings";
    $settingsArr = $AdminManagerObjAjax->GetRecords("All",$SqlSettings);
    foreach($settingsArr as $setting){
	if($setting['id'] == PAYPAL_ID){
            $paypalid = $setting['value'];
        }else if($setting['id'] == PAYPAL_URL){
            $paypalurl = $setting['value'];
        }else if($setting['id'] == FACEBOOK_URL){
            $facebook = $setting['value'];
        }else if($setting['id'] == TWITTER_ID){
            $twitter = $setting['value'];
        }
    }
    
    $ok = "";
}

$smarty->assign('paypalid',$paypalid);
$smarty->assign('paypalurl',$paypalurl);
$smarty->assign('facebook',$facebook);
$smarty->assign('twitter',$twitter);

$smarty->display("settings.tpl");


?>
