<?php
/*******************************************************************************/
			#This page is to add/edit language details
			
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$lesson_status_id=$_REQUEST['lesson_status_id'];
$lesson_status_title=$_REQUEST['lesson_status_title'];
$action=$_REQUEST['action'];

if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	
	/* Holding Data If Error Starts */

	$FabricArr['lesson_status_title']	 = htmlspecialchars(trim($language_name));
	$FabricArr['is_active']				 = $is_active;
	
	/* Holding Data If Error Ends */

	/* Error Checking Starts */
	
	$err_msgs="";
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_lesson_status","lesson_status_title",$lesson_status_title,"Title","lesson_status_id",$lesson_status_id);
		
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{		
		if(empty($lesson_status_id))
		{
			
			 $table_name = TABLEPREFIX."_lesson_status ";

			$fields_values = array( 
									'lesson_status_title'   => $lesson_status_title,	
									'is_active'			    => $is_active,
									'date_added'			=> date("Y-m-d H:i:s")									
									);		
			//print_r($fields_values);
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			
			$language_id=mysql_insert_id();	

			/* Insert Into Events Ends */
		}
		else if(!empty($lesson_status_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_lesson_status";

			$fields_values = array( 
									'lesson_status_title'   => $lesson_status_title,	
									'is_active'			    => $is_active,
									'date_edited'			=> date("Y-m-d H:i:s")		
									);		
			
			$where="lesson_status_id='$lesson_status_id'";										

			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */
		}
				
		echo "<script>window.location.href='lesson_status_manager.php?messg=".$msgreport."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($lesson_status_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_lesson_status WHERE lesson_status_id=".$lesson_status_id;
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
    
	$FabricArr['lesson_status_id']       	= $RsCatSql["lesson_status_id"];
	$FabricArr['lesson_status_title']       = show_to_control($RsCatSql["lesson_status_title"]);
	$FabricArr['is_active']			        = $RsCatSql["is_active"];
	
	/* Get Record For Display Ends */

	$SubmitButton="Update Status ";	
	$flag=1;
}
else
{
	$SubmitButton=" Add Status ";
}


/* Assign Smarty Variables Starts */

$smarty->assign('CmsArr',$CmsArr);
$smarty->assign('lesson_status_id',$lesson_status_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("lesson_status_update.tpl");
?>