<?php
/*******************************************************************************/
			#This page is to add/edit race details
			#last Updated : August 25 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$u_id =$_REQUEST['u_id'];
$user_game_id =$_REQUEST['user_game_id'];
$action=$_REQUEST['action'];
if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	/* Holding Data If Error Starts */

    $FabricArr['game_id']	     = $game_id;
	//$FabricArr['ladder_id']      = $submited_ladder;
	$FabricArr['ladder_id']      = $ladder_id;
	$FabricArr['race_id']        = $submited_race;
	$FabricArr['server_id']      = $submited_server;
	$FabricArr['rating_id']      = $submited_rating;
	$FabricArr['region_id']      = $submited_region;
	$FabricArr['peak_hours']	 = $peak_hours;
	$FabricArr['experience']     = htmlspecialchars(trim($experience));
	$FabricArr['need_improvement'] = htmlspecialchars(trim($need_improvement));
	$FabricArr['is_active']		 = $is_active;
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
	
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_user_game","game_id",$game_id,"Game","user_game_id",$user_game_id,"user_id",$u_id,"user_type_id",3);
	/*$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($ladder_id),"Ladder","EMP",$type="");
	$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($race_id),"Race","EMP",$type="");
	$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($server_id),"Server","EMP",$type="");*/
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{
		if(count($submited_ladder)>0)  $ladder_id=implode(",",$submited_ladder);
		if(count($submited_race)>0)  $race_id=implode(",",$submited_race);
		if(count($submited_server)>0)  $server_id=implode(",",$submited_server);
		if(count($submited_region)>0)  $region_id=implode(",",$submited_region);
		if(count($submited_rating)>0)  $rating_id=implode(",",$submited_rating);
			
		if(empty($user_game_id))
		{
			
			$table_name = TABLEPREFIX."_user_game ";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'ladder_id'                         => $ladder_id,
									'race_id'                           => $race_id,
									'server_id'                         => $server_id,
									'region_id'                         => $region_id,
									'rating_id'                         => $rating_id,
									'peak_hours'                        => $peak_hours,	
									'user_id'                           => $u_id,
									'user_type_id'                      => 3,
									'experience'					    => $experience,
									'need_improvement'                  => $need_improvement,                          
									'is_active'  						=> $is_active,
									'date_added' 						=> date("Y-m-d H:i:s")
									);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			$user_game_id=mysql_insert_id();
			
			/*print_r($fields_values);	
			exit();*/

			/* Insert Into Events Ends */
			
		}
		else if(!empty($user_game_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_user_game";
			$Sql1= "UPDATE ".TABLEPREFIX."_user_game set ladder_id='',server_id='',race_id='', region_id='', rating_id='' where user_game_id='$user_game_id'";  
		    $AdminManagerObjAjax->Execute($Sql1);

			$fields_values = array( 
									'game_id'							=> $game_id,
									'ladder_id'                         => $ladder_id,
									'race_id'                           => $race_id,
									'server_id'                         => $server_id,
									'region_id'                         => $region_id,
									'rating_id'                         => $rating_id,
									'peak_hours'                        => $peak_hours,
									'user_id'                           => $u_id,
									'user_type_id'                      => 3,
									'experience'					    => $experience,
									'need_improvement'                  => $need_improvement,                          
									'is_active'  						=> $is_active,
									'date_edited' 						=> date("Y-m-d H:i:s")
									);

			$where="user_game_id='$user_game_id'";										
			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);
			
			/*print_r($fields_values);	
			
			exit();*/

			/* Update Events Ends */	
		}		
		echo "<script>window.location.href='training_partner_gamemanager.php?messg=".$msgreport."&u_id=".$u_id."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}
$submited_ladder=array();
$submited_race=array();
$submited_rating=array();
$submited_region=array();
$submited_server=array();
if(!empty($user_game_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_user_game WHERE user_game_id='".$user_game_id."' AND user_id='".$u_id."'";
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	$FabricArr['game_id']                   = $RsCatSql["game_id"];
	$FabricArr['ladder_id']                 = $RsCatSql["ladder_id"];
	/*$FabricArr['server_id']                 = $RsCatSql["server_id"];
	$FabricArr['race_id']                   = $RsCatSql["race_id"];*/
	
	$FabricArr['experience'] 				= show_to_control($RsCatSql["experience"]);
	$FabricArr['need_improvement'] 		    = show_to_control($RsCatSql["need_improvement"]);
	$FabricArr['date_edited'] 				= $RsCatSql["date_edited"];
	$FabricArr['date_added'] 				= $RsCatSql["date_added"];
	$FabricArr['is_active']					= $RsCatSql["is_active"];
	$FabricArr['peak_hours']				= $RsCatSql['peak_hours'];
	/*$FabricArr['race_id']					= $RsCatSql["race_id"];
	$FabricArr['server_id']					= $RsCatSql["server_id"];
	$FabricArr['region_id']					= $RsCatSql["region_id"];
	$FabricArr['rating_id']					= $RsCatSql["rating_id"];*/
	
	$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game WHERE game_id=".$FabricArr['game_id'];
	$RsgameSql = $AdminManagerObjAjax->GetRecords("Row",$SelectgameSql);
	
	$FabricArr['is_ladder']					= $RsgameSql["is_ladder"];
	$FabricArr['is_race']					= $RsgameSql["is_race"];
	$FabricArr['is_server']					= $RsgameSql["is_server"];
	$FabricArr['is_region']					= $RsgameSql["is_region"];
	$FabricArr['is_rating']					= $RsgameSql["is_rating"];

	/*$SelectladderSql="SELECT * FROM ".TABLEPREFIX."_game_ladder WHERE game_id=".$user_game_id." AND ladder_id =".$RsCatSql["ladder_id"]."";
	$RsladderSql = $AdminManagerObjAjax->GetRecords("All",$SelectladderSql);	
	

	for($i=0;$i<count($RsladderSql);$i++)
	{		
			$submited_ladder[] = $RsladderSql[$i]['ladder_id'];
	}
	*/
	$SelectRaceSql="SELECT * FROM ".TABLEPREFIX."_game_race WHERE  game_id=".$FabricArr['game_id']." AND race_id IN (".$RsCatSql["race_id"].")";
	$RsRaceSql = $AdminManagerObjAjax->GetRecords("All",$SelectRaceSql);	
	

	for($i=0;$i<count($RsRaceSql);$i++)
	{		
			$submited_race[] = $RsRaceSql[$i]['race_id'];
	}
	
	$SelectratingSql="SELECT * FROM ".TABLEPREFIX."_game_rating WHERE  game_id=".$FabricArr['game_id']." AND rating_id IN (".$RsCatSql["rating_id"].")";
	$RsratingSql = $AdminManagerObjAjax->GetRecords("All",$SelectratingSql);	
	

	for($i=0;$i<count($RsratingSql);$i++)
	{		
			$submited_rating[] = $RsratingSql[$i]['rating_id'];
	}
	
	$SelectregionSql="SELECT * FROM ".TABLEPREFIX."_game_region WHERE  game_id=".$FabricArr['game_id']." AND region_id IN (".$RsCatSql["region_id"].")";
	$RsregionSql = $AdminManagerObjAjax->GetRecords("All",$SelectregionSql);	
	

	for($i=0;$i<count($RsregionSql);$i++)
	{		
			$submited_region[] = $RsregionSql[$i]['region_id'];
	}
	
	$SelectserverSql="SELECT * FROM ".TABLEPREFIX."_game_server WHERE game_id=".$FabricArr['game_id']." AND server_id IN (".$RsCatSql["server_id"].")";
	$RsserverSql = $AdminManagerObjAjax->GetRecords("All",$SelectserverSql);	
	

	for($i=0;$i<count($RsserverSql);$i++)
	{		
			$submited_server[] = $RsserverSql[$i]['server_id'];
	}
	/* Get Record For Display Ends */
	
	
	$SubmitButton="Update Game";	
	$flag=1;
}
else
{
	$SubmitButton=" Add Game";
}
			
		$gameSql = "SELECT game_id,game_name FROM ".TABLEPREFIX."_game where is_active='Y' ORDER BY game_name";
		$GameArr = $AdminManagerObjAjax->HtmlOptionArrayCreate($gameSql);
			
	
		$LadderSql = "SELECT ladder_id,ladder_name  FROM ".TABLEPREFIX."_game_ladder  ORDER BY date_added DESC ";  
		 $LadderArr = $AdminManagerObjAjax->HtmlOptionArrayCreate($LadderSql);
		//$LadderArr = $AdminManagerObjAjax->GetRecords("All",$LadderSql);
	
	
	
	$RaceSql = "SELECT *  FROM ".TABLEPREFIX."_game_race  ORDER BY date_added DESC ";   
	$RaceArr = $AdminManagerObjAjax->GetRecords("All",$RaceSql);
	
	
	
	$RatingSql = "SELECT *  FROM ".TABLEPREFIX."_game_rating   ORDER BY date_added DESC ";   
	$RatingArr = $AdminManagerObjAjax->GetRecords("All",$RatingSql);
	
		$RegionSql = "SELECT *  FROM ".TABLEPREFIX."_game_region ORDER BY date_added DESC ";   
		$RegionArr = $AdminManagerObjAjax->GetRecords("All",$RegionSql);
	
		$ServerSql = "SELECT *  FROM ".TABLEPREFIX."_game_server ORDER BY date_added DESC ";   
		$ServerArr = $AdminManagerObjAjax->GetRecords("All",$ServerSql);
	
//print_r($submited_race);
//print_r($submited_ladder);
//print_r($submited_server);	

/* Assign Smarty Variables Starts */

$smarty->assign('u_id',$u_id);
$smarty->assign('GameArr',$GameArr);
$smarty->assign('user_game_id',$user_game_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);
$smarty->assign('LadderArr',$LadderArr);
$smarty->assign('RaceArr',$RaceArr);
$smarty->assign('RatingArr',$RatingArr);
$smarty->assign('RegionArr',$RegionArr);
$smarty->assign('ServerArr',$ServerArr);
$smarty->assign('submited_race',$submited_race);
$smarty->assign('submited_ladder',$submited_ladder);
$smarty->assign('submited_rating',$submited_rating);
$smarty->assign('submited_region',$submited_region);
$smarty->assign('submited_server',$submited_server);
$smarty->register_modifier("inarray","in_array");

/* Assign Smarty Variables Ends */

$smarty->display("training_partner_game_update.tpl");
?>