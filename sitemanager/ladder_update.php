<?php
/*******************************************************************************/
			#This page is to add/edit ladder details
			#last Updated : August 25 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$game_id =$_REQUEST['game_id'];
$ladder_id =$_REQUEST['ladder_id'];
$ladder_name=$_REQUEST['ladder_name'];
$ladder_description=$_REQUEST['ladder_description'];
$action=$_REQUEST['action'];
if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	/* Holding Data If Error Starts */

	$FabricArr['ladder_name']		 = htmlspecialchars(trim($ladder_name));
	$FabricArr['ladder_descrption']	 = htmlspecialchars(trim($ladder_description));
	$FabricArr['is_active']			 = $is_active;
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
	
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_game_ladder","ladder_name",$ladder_name,"Ladder name","ladder_id",$ladder_id,"game_id",$game_id);
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{
			
		if(empty($ladder_id))
		{
		 	$ladder_order = $AdminManagerObjAjax->GetRecords("One","select ifnull(max(ladder_order)+1,1) from ".TABLEPREFIX."_game_ladder WHERE game_id='".$game_id."'");
			
			$table_name = TABLEPREFIX."_game_ladder ";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'ladder_order'					    => $ladder_order,
									'ladder_name'					    => $ladder_name,
									'ladder_description'  				=> $ladder_description,
									'is_active'  						=> $is_active,
									'date_added' 						=> date("Y-m-d H:i:s")
									);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			$ladder_id=mysql_insert_id();	

			/* Insert Into Events Ends */
			
		}
		else if(!empty($ladder_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_game_ladder";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'ladder_name'					    => $ladder_name,
									'ladder_description'  				=> $ladder_description,
									'is_active'  						=> $is_active,
									'date_edited' 						=> date("Y-m-d H:i:s")
									);

			$where="ladder_id='$ladder_id'";										
			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */	
		}		
		echo "<script>window.location.href='ladder_manager.php?messg=".$msgreport."&game_id=".$game_id."&IsPreserved=Y'</script>";
		exit; 
	
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($ladder_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_game_ladder WHERE ladder_id='".$ladder_id."' AND game_id='".$game_id."'";
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	$FabricArr['ladder_name'] 				= show_to_control($RsCatSql["ladder_name"]);
	$FabricArr['ladder_description'] 		= show_to_control($RsCatSql["ladder_description"]);
	$FabricArr['date_edited'] 				= $RsCatSql["date_edited"];
	$FabricArr['date_added'] 				= $RsCatSql["date_added"];
	$FabricArr['is_active']					= $RsCatSql["is_active"];

	/* Get Record For Display Ends */

	$SubmitButton="Update Ladder";	
	$flag=1;
}
else
{
	$SubmitButton=" Add Ladder ";
}


/* Assign Smarty Variables Starts */

$smarty->assign('game_id',$game_id);
$smarty->assign('ladder_id',$ladder_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("ladder_update.tpl");
?>