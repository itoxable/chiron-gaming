<?php
/*******************************************************************************/
	#This page lists all the ladder we have in table
	#last Updated : August 25 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$page_name="video_review_manager.php";
$action=$_REQUEST['action'];



$IsPreserved=$_REQUEST['IsPreserved'];

if($IsPreserved=="Y")
{
/* preserve select starts */
$PreserveVariableArr=$preserve_variable_object->PreserveVariableSelect();
foreach($PreserveVariableArr as $a=>$b){$$a=$b;}
/* preserve select ends */
}

/* Activate Operation Starts */
if($action=="activate")
{
  $Reviewdtls = "SELECT * FROM ".TABLEPREFIX."_video_review WHERE video_review_id='$record_id'";
  $dtlsArr = $AdminManagerObjAjax->GetRecords("Row",$Reviewdtls);
  $rating = $dtlsArr['rating'];
  $video_id = $dtlsArr['video_id'];
  $is_active = $dtlsArr['is_active'];
  $totreview = "SELECT count(*),sum(rating) as totrate FROM ".TABLEPREFIX."_video_review WHERE video_id='".$video_id."' AND is_active='Y'";
  $totArr = $AdminManagerObjAjax->GetRecords("Row",$totreview);
  $tot_count = $totArr[0];
  $tot_rate = $totArr['totrate'];
  if($is_active == 'N')
  {
    $rate = $tot_rate+$rating;
	$totreviewer = $tot_count+1;
  }	
  if($is_active == 'Y')
  {
    $rate = $tot_rate-$rating;
	$totreviewer = $tot_count-1;
  }	
	$avg = round($rate/$totreviewer);
	$UpdateSql = "UPDATE ".TABLEPREFIX."_video SET overall_rating='$avg' WHERE video_id='$video_id'";
	$adodbcon->Execute($UpdateSql);
    $ChangeStatusObjAjax->ChangeStatus($record_id,11);	
}
/* Activate Operation Ends */


/* Delete Operation Starts */

if($action=="del")
{	
	if(!empty($delete_id))
	{
	     $Reviewdtls = "SELECT * FROM ".TABLEPREFIX."_video_review WHERE video_review_id='$delete_id'";
		  $dtlsArr = $AdminManagerObjAjax->GetRecords("Row",$Reviewdtls);
		  $rating = $dtlsArr['rating'];
		  $review_to_id = $dtlsArr['video_id'];
		  $is_active = $dtlsArr['is_active'];
		  $totreview = "SELECT count(*),sum(rating) as totrate FROM ".TABLEPREFIX."_video_review WHERE video_id='".$review_to_id."' AND is_active='Y'";
		  $totArr = $AdminManagerObjAjax->GetRecords("Row",$totreview);
		  $tot_count = $totArr[0];
		  $tot_rate = $totArr['totrate'];
		  if($is_active == 'Y')
		  {
			$rate = $tot_rate-$rating;
			$totreviewer = $tot_count-1;
		  }	
		  $avg = round($rate/$totreviewer);
		  $UpdateSql = "UPDATE ".TABLEPREFIX."_video SET overall_rating='$avg' WHERE video_id='$review_to_id'";
		  $adodbcon->Execute($UpdateSql);
		/* Delete review Starts */
		$SqlDelete="DELETE FROM ".TABLEPREFIX."_video_review WHERE video_review_id='$delete_id'";
		$ResultDelete=$adodbcon->Execute($SqlDelete);
		/* Delete review Ends */

		if($adodbcon->ErrorNo()) 
			$messg = 5;
		else 
			$messg = 6;
	}
	else
	{
		$messg = 7;
	}		
}

/* Delete Operation Ends */




/* IsProcess Starts */

$action_arr=array("list_order","ranking","activate","list_paginate","list_search","del","default");

if(in_array($action,$action_arr))
{
	$smarty->assign('IsProcess',"Y");
}

/* IsProcess Ends */

	/* listing Operation Starts */

	/* search section starts */

	if($dosearch=="GO")
	{ 
		
		$Search_from_date = $Search_from_date=='Search From'?'':$Search_from_date." 00:00:00";
		$Search_to_date = $Search_to_date=='Search To'?'':$Search_to_date." 23:59:59";
		
		if(!empty($u_id))
		{
			 $searchSql =  $searchSql." AND v.user_id='$u_id'"; 
		}

		if(!empty($Search_from_date))
		{
			 $searchSql =  $searchSql." AND vr.date_added >= '".mysql_quote($Search_from_date,"N")."'"; 
		}
		if(!empty($Search_to_date))
		{
			 $searchSql =  $searchSql." AND vr.date_added <= '".mysql_quote($Search_to_date,"N")."'"; 
		}
		$SearchLink="dosearch=GO&u_id=$u_id&Search_from_date=$Search_from_date&Search_to_date=$Search_to_date";
	}	

	/* search section ends */

	/* Order section starts */	

	$SortingSequenceArr=array(1 => "vr.date_added");
	$ReturnSortingArr=$SortingObjAjax->Sorting("vr.date_added DESC",$SortingSequenceArr,$do_order,$OrderByID,$OrderType);	
	$OrderBySql=$ReturnSortingArr['OrderBySql'];
	$OrderLink=$ReturnSortingArr['OrderLink'];
	$OrderType=$ReturnSortingArr['OrderType'];			

	/* Order section ends */

  $SqlSelectCat="SELECT vr.*,v.user_id,v.video_title FROM ".TABLEPREFIX."_video_review vr,".TABLEPREFIX."_video v WHERE vr.video_id=v.video_id".$searchSql.$OrderBySql;
	//echo $SqlSelectCat;
	/* pagination starts */	

	$pagination_arr=$PaginationObjAjax->PaginationAjax($SqlSelectCat,$page_name."?action=".$action."&".$SearchLink."&".$OrderLink,"ManagerGeneral");	

	/* pagination ends */ 

	/* preserve update starts */

	$PreserveLink="from=$from&".$SearchLink."&".$OrderLink;

	$preserve_variable_object->PreserveVariableUpdate($PreserveLink);

	/* preserve update ends */		
    


	#Fetch all project gallery and store them in an array
	$SelectCmsArr=$adodbcon->GetAll($pagination_arr[0]);	

	$NumSelectCms=count($SelectCmsArr);

	for($i=0;$i<$NumSelectCms;$i++)
	{				
		//$SelectVideo = "SELECT video_title FROM ".TABLEPREFIX."_video WHERE video_id='".$SelectCmsArr[$i]['video_id']."'";
		//$vdoArr = $AdminManagerObjAjax->GetRecords("Row",$SelectVideo);
		//echo $SelectCmsArr[$i]['video_review_id'];
		$SelectReviewby = "SELECT name FROM ".TABLEPREFIX."_user WHERE user_id='".$SelectCmsArr[$i]['reviewed_by']."'";
		$UserArr = $AdminManagerObjAjax->GetRecords("Row",$SelectReviewby);
		
		$SelectPostby = "SELECT name FROM ".TABLEPREFIX."_user WHERE user_id='".$SelectCmsArr[$i]['user_id']."'";
		$UploaderArr = $AdminManagerObjAjax->GetRecords("Row",$SelectPostby);
		
		$SelectCmsArr[$i]['review_to'] 		                = $SelectCmsArr[$i]['video_title'];
		$SelectCmsArr[$i]['review_by'] 		                = $UserArr['name'];
		$SelectCmsArr[$i]['upload_by'] 		                = $UploaderArr['name'];
		$SelectCmsArr[$i]['name_delete']                    = $UserArr['name'];
		$SelectCmsArr[$i]['date_added'] 			        = date_format_admin($SelectCmsArr[$i]['date_added']);
		$SelectCmsArr[$i]['active_img'] 				    = $SelectCmsArr[$i]['is_active']=="Y" ? "true.gif" : "false.gif";
		$SelectCmsArr[$i]['active_alt'] 				    = $SelectCmsArr[$i]['is_active']=="Y" ? "Active" : "Inactive";
	}

/* listing Operation Ends */

  $SelectCoach = "SELECT a.user_id,a.name FROM ".TABLEPREFIX."_user a order by name";
  $Coach = $AdminManagerObjAjax->GetRecords("All",$SelectCoach);

  $MessgReportText=displayMessage($messg);

#Getting project title for this gallery

/* Assign Smarty Variables Starts */

$smarty->assign("MessgReportText",$MessgReportText);
$smarty->assign('page_name',$page_name);
$smarty->assign('from',$from);
$smarty->assign('Coach',$Coach);
$smarty->assign('PreserveLink',$PreserveLink);
$smarty->assign('SearchLink',$SearchLink);
$smarty->assign('dosearch',$dosearch);
$smarty->assign('user_id',$u_id);
$smarty->assign("Search_from_date",$Search_from_date);
$smarty->assign("Search_to_date",$Search_to_date);
$smarty->assign('OrderLink',$OrderLink);
$smarty->assign('do_order',$do_order);
$smarty->assign('OrderType',$OrderType);
$smarty->assign('OrderByID',$OrderByID);
$smarty->assign('ReturnSortingArr',$ReturnSortingArr);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('NumSelectCms',$NumSelectCms);
$smarty->assign('SelectCmsArr',$SelectCmsArr);


/* Assign Smarty Variables Ends */

$smarty->display("video_review_manager.tpl");
?>