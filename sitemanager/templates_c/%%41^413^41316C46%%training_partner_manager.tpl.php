<?php /* Smarty version 2.6.16, created on 2013-02-24 11:37:49
         compiled from training_partner_manager.tpl */ ?>
<?php if ($this->_tpl_vars['IsProcess'] != 'Y'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "top.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script type="text/javascript">
var j = jQuery.noConflict();
j(document).ready(function(){
focus_blur(\'Search_by_name\',\'Search by Name\');
focus_blur(\'Search_by_email\',\'Search by Email\');
focus_blur(\'Search_from_date\',\'Search From\');
focus_blur(\'Search_to_date\',\'Search To\');
});

function SetFeatured(url)
{
  j(\'#TransMsgDisplay\').html(\'<img src="images/indicator.gif" align="center">\');	
  j.ajax({
  type: "POST",
  url: url,
  dataType: \'text\',
  success: function(data){
	//j(\'#records_listing\').html(data);
	//j(document).html(data);
	document.location.reload();
	//document.write(data);
	if(j(\'#TransMsgDisplay\')) 
	{										
  	  j(\'#TransMsgDisplay\').html(\'\');
	}				           

	}

   });

}
</script>
'; ?>


<h2>Training Partners <div id="TransMsgDisplay" align="right" style="height:3px" ></div></h2>
<form method="POST" name="form_search" id="form_search" action="<?php echo $this->_tpl_vars['page_name']; ?>
" onsubmit="return false;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="17%" align="right" valign="middle">	
				<input size="30" type="text" name="Search_by_name" id="Search_by_name" value="<?php echo $this->_tpl_vars['Search_by_name']; ?>
" 
				class="textbox search-box" style="width:100px;"/>	
				<input type="hidden" name="dosearch" value="GO">
				</td>
				<td width="17%" align="right" valign="middle" >	
				<input size="30" type="text" name="Search_by_email" id="Search_by_email" value="<?php echo $this->_tpl_vars['Search_by_email']; ?>
" class="textbox search-box" style="width:100px;"/>	
				<input type="hidden" name="dosearch" value="GO">
				</td>
				
				<td width="20%" align="left" valign="middle">
				<input type="text" name="Search_from_date" id="Search_from_date" class="datebox" readonly="" value="<?php echo $this->_tpl_vars['Search_from_date']; ?>
" style="width:80px;"/>
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_from_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				<input type="hidden" name="dosearch" value="GO">
				</td>

				<td width="20%" align="left" valign="middle">
				<input type="text" name="Search_to_date" id="Search_to_date" class="datebox" readonly="" value="<?php echo $this->_tpl_vars['Search_to_date']; ?>
" style="width:80px;"/>
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_to_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				<input type="hidden" name="dosearch" value="GO">
				</td>

				<td valign="middle">
				<input  name="submitdosearch" value="GO" type="button" class="go" onClick="ManagerGeneralForm('<?php echo $this->_tpl_vars['page_name']; ?>
?action=list_search','form_search')" />
				<input name="" type="button"  class="reset" value="RESET" onClick="window.location.href='<?php echo $this->_tpl_vars['page_name']; ?>
';"/></td>
			</tr>
		
		</table>
	</form>
<?php endif; ?>	

<div id="records_listing">

<?php if ($this->_tpl_vars['MessgReportText'] != ""): ?><div align="left" class="successful2" style="color:#FF0000; margin:-25px 0 0 0;"><?php echo $this->_tpl_vars['MessgReportText']; ?>
</div><?php endif; ?>	
		
<div class="newsName">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td colspan="6" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#9f9f9f">
				  <tr  bgcolor="#9f9f9f">
				  	<td  align="left" width="20%" valign="middle" class="padleft">
						<a href="#"  onClick="ManagerGeneral('<?php echo $this->_tpl_vars['page_name']; ?>
?action=list_order&do_order=GO&OrderByID=1&OrderType=<?php echo $this->_tpl_vars['OrderType']; ?>
&<?php echo $this->_tpl_vars['SearchLink']; ?>
&from=<?php echo $this->_tpl_vars['from']; ?>
')" >								
							<span class="whitetext"><strong>Name</strong></span>									
							<?php echo $this->_tpl_vars['ReturnSortingArr']['DisplaySortingImage'][1]; ?>
				
						</a>	
					</td>
					<td  align="left" width="20%" valign="middle" class="padleft">
						<a href="#"  onClick="ManagerGeneral('<?php echo $this->_tpl_vars['page_name']; ?>
?action=list_order&do_order=GO&OrderByID=2&OrderType=<?php echo $this->_tpl_vars['OrderType']; ?>
&<?php echo $this->_tpl_vars['SearchLink']; ?>
&from=<?php echo $this->_tpl_vars['from']; ?>
')" >								
							<span class="whitetext"><strong>Email</strong></span>									
							<?php echo $this->_tpl_vars['ReturnSortingArr']['DisplaySortingImage'][2]; ?>
				
						</a>	
					</td>
					<td  align="left" width="12%" valign="middle" class="padleft">
						<a href="#"  onClick="ManagerGeneral('<?php echo $this->_tpl_vars['page_name']; ?>
?action=list_order&do_order=GO&OrderByID=3&OrderType=<?php echo $this->_tpl_vars['OrderType']; ?>
&<?php echo $this->_tpl_vars['SearchLink']; ?>
&from=<?php echo $this->_tpl_vars['from']; ?>
')" >								
							<span class="whitetext"><strong>Date Added</strong></span>									
							<?php echo $this->_tpl_vars['ReturnSortingArr']['DisplaySortingImage'][3]; ?>
				
						</a>	
					</td>	
					<td width="8%" class="whitetext" align="center" valign="middle"><strong>Game</strong></td>
					<td width="8%" class="whitetext" align="center" valign="middle"><strong>Video</strong></td>
					<td width="6%" class="whitetext" align="center" valign="middle"><strong>Active</strong></td>
					<!--td width="10%" class="whitetext" align="center" valign="middle"><strong>Actions</strong></td-->
				  </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#878787">
				<?php if ($this->_tpl_vars['NumSelectCms'] > 0): ?>
			<?php unset($this->_sections['RowCms']);
$this->_sections['RowCms']['name'] = 'RowCms';
$this->_sections['RowCms']['loop'] = is_array($_loop=$this->_tpl_vars['SelectCmsArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['RowCms']['show'] = true;
$this->_sections['RowCms']['max'] = $this->_sections['RowCms']['loop'];
$this->_sections['RowCms']['step'] = 1;
$this->_sections['RowCms']['start'] = $this->_sections['RowCms']['step'] > 0 ? 0 : $this->_sections['RowCms']['loop']-1;
if ($this->_sections['RowCms']['show']) {
    $this->_sections['RowCms']['total'] = $this->_sections['RowCms']['loop'];
    if ($this->_sections['RowCms']['total'] == 0)
        $this->_sections['RowCms']['show'] = false;
} else
    $this->_sections['RowCms']['total'] = 0;
if ($this->_sections['RowCms']['show']):

            for ($this->_sections['RowCms']['index'] = $this->_sections['RowCms']['start'], $this->_sections['RowCms']['iteration'] = 1;
                 $this->_sections['RowCms']['iteration'] <= $this->_sections['RowCms']['total'];
                 $this->_sections['RowCms']['index'] += $this->_sections['RowCms']['step'], $this->_sections['RowCms']['iteration']++):
$this->_sections['RowCms']['rownum'] = $this->_sections['RowCms']['iteration'];
$this->_sections['RowCms']['index_prev'] = $this->_sections['RowCms']['index'] - $this->_sections['RowCms']['step'];
$this->_sections['RowCms']['index_next'] = $this->_sections['RowCms']['index'] + $this->_sections['RowCms']['step'];
$this->_sections['RowCms']['first']      = ($this->_sections['RowCms']['iteration'] == 1);
$this->_sections['RowCms']['last']       = ($this->_sections['RowCms']['iteration'] == $this->_sections['RowCms']['total']);
?>
				  <tr <?php if (( $this->_tpl_vars['templatelite']['section']['RowCms']['index'] % 2 ) == 0): ?> bgcolor="#ffffff" <?php else: ?> bgcolor="#ffffff" <?php endif; ?>>
				  
				  	<td  align="left" width="20%" height="39" valign="middle" class="simpletxt">
					<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['username']; ?>

					</td>
			        <td  align="left" width="20%" height="39" valign="middle" class="simpletxt">
					<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['email']; ?>

					</td>
					<td  align="left" width="12%" valign="middle" class="simpletxt">
					<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['date_registered']; ?>

					</td>
					<td width="8%" align="center" valign="middle">
					<a href="training_partner_gamemanager.php?u_id=<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['user_id']; ?>
">
					<img src="images/add-tab-icon.gif" width="20" height="19" border="0" alt="Manage Game" /></a></td>
					<td width="8%" align="center" valign="middle">
					<a href="video_manager.php?u_id=<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['user_id']; ?>
&utype=tp">
					<img src="images/add-tab-icon.gif" width="20" height="19" border="0" alt="Manage Video" /></a></td>


					<td width="6%" align="center" valign="middle"><a href="#" onClick="ManagerGeneral('<?php echo $this->_tpl_vars['page_name']; ?>
?action=activate&<?php echo $this->_tpl_vars['PreserveLink']; ?>
&record_id=<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['user_id']; ?>
')">
					<img src="images/<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['active_img']; ?>
" width="15" height="14" border="0" alt="<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['active_alt']; ?>
" /></a></td>
					<!--td width="10%" align="center" valign="middle">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
						  <td width="60" height="39" align="center" valign="middle" class="rightBorder" >
						  <a href="user_update.php?u_id=<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['user_id']; ?>
">
						  <img src="images/edit_dark.gif" alt="" width="15" height="14" border="0" />
						  </a>
						  </td>
						  
						  <td width="50%" align="center" valign="middle">
						  <a href="#" onClick="ConfirmDelete('<?php echo $this->_tpl_vars['page_name']; ?>
?action=del&<?php echo $this->_tpl_vars['PreserveLink']; ?>
','<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['user_id']; ?>
','<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['name_delete']; ?>
','Coach:: ')">
						  <img src="images/delete_icon.gif" border="0" alt="" />
						  </a>
						  </td>
						  </tr>
					</table>					
					</td-->
				 </tr>
				  <?php endfor; endif; ?>
			<?php else: ?>
				<tr bgcolor="#f4f4f5"><td class="plaintxt" colspan="5" align="center">No Record Found</td></tr>
			<?php endif; ?>       
				  
				 
			  </table>
			</td>
		  </tr>
		 
  </table>
</div>
<br />
<div>	<?php if ($this->_tpl_vars['pagination_arr'][3] > 0): ?>
   <table>
	  <tr>
        <td height="30" valign="middle" align="center">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td align="left" class="plaintxt">					
					<?php if ($this->_tpl_vars['pagination_arr'][2] != ""): ?>					
						<?php echo $this->_tpl_vars['pagination_arr'][2]; ?>

					<?php endif; ?>					
					</td>
					<td align="right" class="plaintxt">					   	
					<?php if ($this->_tpl_vars['pagination_arr'][1] != ""): ?>					
						<?php echo $this->_tpl_vars['pagination_arr'][1]; ?>

					<?php endif; ?>					
					</td>
				</tr>
			</table>
		</td>
      </tr>
	  </table>
	<?php endif; ?> </div><br /><br /><br /><br /><br />
</div>

<?php if ($this->_tpl_vars['IsProcess'] != 'Y'): ?>  	
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "bottom.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>
<?php endif; ?>