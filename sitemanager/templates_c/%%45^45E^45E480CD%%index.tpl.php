<?php /* Smarty version 2.6.16, created on 2013-01-01 19:47:02
         compiled from index.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome to Admin Panel : Noobskool</title>
<link href="style/style.css" rel="stylesheet" type="text/css" />

<?php echo '
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/jQuery.functions.pack.js"></script>
<script type="text/javascript" src="js/ddaccordion.js"></script>
<script type="text/javascript" src="js/menu_collapse.js"></script>
<script language="javascript" type="text/javascript">
	var j = jQuery.noConflict();
</script>
<script language="javascript1.2" src="../includeajax/AjaxAdminGeneral.js"></script>
<script language="javascript1.2" src="../includejs/general_functions.js"></script>
<script>
function set_focus()
{
    j(\'#Username\').focus();
}
/* Check submitbyenter starts */
/* Note: Use In Control Attribute as onKeyPress="return submitbyenter(\'frmLogin\',this, event)" */
function submitbyenter(formid,myfield, e, dec)
{
	var key;
	var keychar;
	if (window.event)
	key = window.event.keyCode;
	else if (e)
	key = e.which;
	else
	return true;		
	if (key==13)
	{		
		GetLogin(\'frmLogin\');		
	}	
}
/* Check submitbyenter ends */

function submit_frm()
{
	GetLogin(\'frmLogin\');	
}
</script>
'; ?>

</head>
<body onload="set_focus()">
<div class="indexinner">
   <div class="logo" align="center"><a href="#"><img src="images/logo.gif" alt="" border="0" /></a></div>
   	 
   <div class="clear"></div>	 
		
		<div class="login_outer">
		   <div class="indexLogin">
		  		<div class="lerror">
					<span id="myusername"></span><br />
					<span id="mypassword"></span><br />
					<span id="TransMsgDisplay"></span><br />
				 </div>
   				 <div class="clear"></div>	
				<form name="frmLogin" id="frmLogin" method="post" action="" onsubmit="return false;">
					<fieldset>
					    <label>Username:</label>
						<input type="text" name="Username" id="Username" value="" />
					    <label>Password:</label>
						<input type="password" name="Password" id="Password" value="" />
						<div class="clear"></div>
						
					<label class="big">Forgot Password? Please <a href="forgot_password.php">click here</a></label>
					<input  type="text" value="LOGIN"  class="submit" onclick=" javascript : submit_frm();"/>
					</fieldset>
				</form>
			</div>
		</div>
	<div class="clear"></div>
	<div class="footerouter">
	&copy;2011 All Rights Reserved.
	</div>
	
</div>
</body>
</html>