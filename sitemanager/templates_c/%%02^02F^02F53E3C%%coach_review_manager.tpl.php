<?php /* Smarty version 2.6.16, created on 2013-02-24 11:27:55
         compiled from coach_review_manager.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'nl2br', 'coach_review_manager.tpl', 172, false),)), $this); ?>
<?php if ($this->_tpl_vars['IsProcess'] != 'Y'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "top.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
<script type="text/javascript">
var j = jQuery.noConflict();
j(document).ready(function(){
		

focus_blur(\'Search_from_date\',\'Search From\');
focus_blur(\'Search_to_date\',\'Search To\');
});

</script>
'; ?>

<?php endif; ?>
<?php echo '
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
						   		   
	//When you click on a link with class of poplight and the href starts with a # 
	$(\'a.poplight[href^=#]\').click(function() {
		var popID = $(this).attr(\'rel\'); //Get Popup Name
		var popURL = $(this).attr(\'href\'); //Get Popup href to define size
				
		//Pull Query & Variables from href URL
		var query= popURL.split(\'?\');
		var dim= query[1].split(\'&\');
		var popWidth = dim[0].split(\'=\')[1]; //Gets the first query string value

		//Fade in the Popup and add close button
		$(\'#\' + popID).fadeIn().css({ \'width\': Number( popWidth ) }).prepend(\'<a href="#" class="close"><img src="images/close_button.png" class="btn_close" title="Close Window" alt="Close" /></a>\');
		
		//Define margin for center alignment (vertical + horizontal) - we add 80 to the height/width to accomodate for the padding + border width defined in the css
		var popMargTop = ($(\'#\' + popID).height() + 80) / 2;
		var popMargLeft = ($(\'#\' + popID).width() + 80) / 2;
		
		//Apply Margin to Popup
		$(\'#\' + popID).css({ 
			\'margin-top\' : -popMargTop,
			\'margin-left\' : -popMargLeft
		});
		
		//Fade in Background
		$(\'body\').append(\'<div id="fade"></div>\'); //Add the fade layer to bottom of the body tag.
		$(\'#fade\').css({\'filter\' : \'alpha(opacity=80)\'}).fadeIn(); //Fade in the fade layer 
		
		return false;
	});
	
	
	//Close Popups and Fade Layer
	$(\'a.close, #fade\').live(\'click\', function() { //When clicking on the close or fade layer...
	  	$(\'#fade , .popup_block\').fadeOut(function() {
			$(\'#fade, a.close\').remove();  
	}); //fade them both out
		
		return false;
	});

	
});

</script>
'; ?>

<?php if ($this->_tpl_vars['IsProcess'] != 'Y'): ?>
<h2>Coach Reviews and Ratings <div id="TransMsgDisplay" align="right" style="height:3px" ></div></h2>
<form method="POST" name="form_search" id="form_search" action="<?php echo $this->_tpl_vars['page_name']; ?>
" onsubmit="return false;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				
				<td width="17%" align="right" valign="middle" >	
				<select name="u_id" class="selectbox">
				<option value="">Select</option>
				<?php unset($this->_sections['coachRow']);
$this->_sections['coachRow']['name'] = 'coachRow';
$this->_sections['coachRow']['loop'] = is_array($_loop=$this->_tpl_vars['Coach']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['coachRow']['show'] = true;
$this->_sections['coachRow']['max'] = $this->_sections['coachRow']['loop'];
$this->_sections['coachRow']['step'] = 1;
$this->_sections['coachRow']['start'] = $this->_sections['coachRow']['step'] > 0 ? 0 : $this->_sections['coachRow']['loop']-1;
if ($this->_sections['coachRow']['show']) {
    $this->_sections['coachRow']['total'] = $this->_sections['coachRow']['loop'];
    if ($this->_sections['coachRow']['total'] == 0)
        $this->_sections['coachRow']['show'] = false;
} else
    $this->_sections['coachRow']['total'] = 0;
if ($this->_sections['coachRow']['show']):

            for ($this->_sections['coachRow']['index'] = $this->_sections['coachRow']['start'], $this->_sections['coachRow']['iteration'] = 1;
                 $this->_sections['coachRow']['iteration'] <= $this->_sections['coachRow']['total'];
                 $this->_sections['coachRow']['index'] += $this->_sections['coachRow']['step'], $this->_sections['coachRow']['iteration']++):
$this->_sections['coachRow']['rownum'] = $this->_sections['coachRow']['iteration'];
$this->_sections['coachRow']['index_prev'] = $this->_sections['coachRow']['index'] - $this->_sections['coachRow']['step'];
$this->_sections['coachRow']['index_next'] = $this->_sections['coachRow']['index'] + $this->_sections['coachRow']['step'];
$this->_sections['coachRow']['first']      = ($this->_sections['coachRow']['iteration'] == 1);
$this->_sections['coachRow']['last']       = ($this->_sections['coachRow']['iteration'] == $this->_sections['coachRow']['total']);
?>
				<option value="<?php echo $this->_tpl_vars['Coach'][$this->_sections['coachRow']['index']]['user_id']; ?>
" <?php if ($this->_tpl_vars['u_id'] == $this->_tpl_vars['Coach'][$this->_sections['coachRow']['index']]['user_id']): ?> selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['Coach'][$this->_sections['coachRow']['index']]['name']; ?>
</option>
				<?php endfor; endif; ?>
				</select>
				<input type="hidden" name="dosearch" value="GO">
				</td>
				
				<td width="20%" align="left" valign="middle">
				<input type="text" name="Search_from_date" id="Search_from_date" class="datebox" readonly="" value="<?php echo $this->_tpl_vars['Search_from_date']; ?>
" style="width:80px;"/>
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_from_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				<input type="hidden" name="dosearch" value="GO">
				</td>

				<td width="20%" align="left" valign="middle">
				<input type="text" name="Search_to_date" id="Search_to_date" class="datebox" readonly="" value="<?php echo $this->_tpl_vars['Search_to_date']; ?>
" style="width:80px;"/>
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_to_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				<input type="hidden" name="dosearch" value="GO">
				</td>

				<td valign="middle">
				<input  name="submitdosearch" value="GO" type="button" class="go" onClick="ManagerGeneralForm('<?php echo $this->_tpl_vars['page_name']; ?>
?action=list_search','form_search')" />
				<input name="" type="button"  class="reset" value="RESET" onClick="window.location.href='<?php echo $this->_tpl_vars['page_name']; ?>
';"/></td>
			</tr>
		
		</table>
	</form>
<?php endif; ?>	

<div id="records_listing">

<?php if ($this->_tpl_vars['MessgReportText'] != ""): ?><div align="left" class="successful2" style="color:#FF0000; margin:-25px 0 0 0;"><?php echo $this->_tpl_vars['MessgReportText']; ?>
</div><?php endif; ?>	
		
<div class="newsName">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td colspan="6" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#9f9f9f">
				  <tr  bgcolor="#9f9f9f">
				  	<td  align="left" width="25%" valign="middle" class="padleft">
							<span class="whitetext"><strong>Review to</strong></span>									
										
						
					</td>
					<td  align="left" width="25%" valign="middle" class="padleft">
							<span class="whitetext"><strong>Review By</strong></span>									
					</td>
					<td  align="left" width="18%" valign="middle" class="padleft">
						<a href="#"  onClick="ManagerGeneral('<?php echo $this->_tpl_vars['page_name']; ?>
?action=list_order&do_order=GO&OrderByID=3&OrderType=<?php echo $this->_tpl_vars['OrderType']; ?>
&<?php echo $this->_tpl_vars['SearchLink']; ?>
&from=<?php echo $this->_tpl_vars['from']; ?>
')" >								
							<span class="whitetext"><strong>Review Date</strong></span>									
							<?php echo $this->_tpl_vars['ReturnSortingArr']['DisplaySortingImage'][1]; ?>
				
						</a>	
					</td>	
					<td width="10%" class="whitetext" align="center" valign="middle"><strong>View</strong></td>
					<td width="10%" class="whitetext" align="center" valign="middle"><strong>Active</strong></td>
					<td width="10%" class="whitetext" align="center" valign="middle"><strong>Actions</strong></td>
				  </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#878787">
				<?php if ($this->_tpl_vars['NumSelectCms'] > 0): ?>
			<?php unset($this->_sections['RowCms']);
$this->_sections['RowCms']['name'] = 'RowCms';
$this->_sections['RowCms']['loop'] = is_array($_loop=$this->_tpl_vars['SelectCmsArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['RowCms']['show'] = true;
$this->_sections['RowCms']['max'] = $this->_sections['RowCms']['loop'];
$this->_sections['RowCms']['step'] = 1;
$this->_sections['RowCms']['start'] = $this->_sections['RowCms']['step'] > 0 ? 0 : $this->_sections['RowCms']['loop']-1;
if ($this->_sections['RowCms']['show']) {
    $this->_sections['RowCms']['total'] = $this->_sections['RowCms']['loop'];
    if ($this->_sections['RowCms']['total'] == 0)
        $this->_sections['RowCms']['show'] = false;
} else
    $this->_sections['RowCms']['total'] = 0;
if ($this->_sections['RowCms']['show']):

            for ($this->_sections['RowCms']['index'] = $this->_sections['RowCms']['start'], $this->_sections['RowCms']['iteration'] = 1;
                 $this->_sections['RowCms']['iteration'] <= $this->_sections['RowCms']['total'];
                 $this->_sections['RowCms']['index'] += $this->_sections['RowCms']['step'], $this->_sections['RowCms']['iteration']++):
$this->_sections['RowCms']['rownum'] = $this->_sections['RowCms']['iteration'];
$this->_sections['RowCms']['index_prev'] = $this->_sections['RowCms']['index'] - $this->_sections['RowCms']['step'];
$this->_sections['RowCms']['index_next'] = $this->_sections['RowCms']['index'] + $this->_sections['RowCms']['step'];
$this->_sections['RowCms']['first']      = ($this->_sections['RowCms']['iteration'] == 1);
$this->_sections['RowCms']['last']       = ($this->_sections['RowCms']['iteration'] == $this->_sections['RowCms']['total']);
?>
				  <tr <?php if (( $this->_tpl_vars['templatelite']['section']['RowCms']['index'] % 2 ) == 0): ?> bgcolor="#ffffff" <?php else: ?> bgcolor="#ffffff" <?php endif; ?>>
				  
				  	<td  align="left" width="25%" height="39" valign="middle" class="simpletxt">
					<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['review_to']; ?>

					</td>
			        <td  align="left" width="25%" height="39" valign="middle" class="simpletxt">
					<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['review_by']; ?>

					</td>
					<td  align="left" width="18%" valign="middle" class="simpletxt">
					<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['date_added']; ?>

					</td>
					<td width="10%" align="center" valign="middle">
					<a href="#?w=400" rel="popup<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['user_review_id']; ?>
" class="poplight">
					<img src="images/view.gif" width="20" height="19" border="0" alt="View Review" /></a></td>
					<td width="10%" align="center" valign="middle"><a href="javascript:;" onClick="ManagerGeneral('<?php echo $this->_tpl_vars['page_name']; ?>
?action=activate&<?php echo $this->_tpl_vars['PreserveLink']; ?>
&record_id=<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['user_review_id']; ?>
')">
					<img src="images/<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['active_img']; ?>
" width="15" height="14" border="0" alt="<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['active_alt']; ?>
" /></a></td>
					
					 <td width="10%" class="simpletxt" align="center" valign="middle">
						  <a href="#" onClick="ConfirmDelete('<?php echo $this->_tpl_vars['page_name']; ?>
?action=del&<?php echo $this->_tpl_vars['PreserveLink']; ?>
','<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['user_review_id']; ?>
','<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['name_delete']; ?>
','Coach Review by:: ')">
						  <img src="images/delete_icon.gif" border="0" alt="" />
						  </a>
				     </td>
					
				 </tr>
				 <tr>
				 <td colspan="6">
				 <div id="popup<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['user_review_id']; ?>
" class="popup_block">
					<h4 style="color:#5B8F2A;">View Review Details</h4>
					<p>
					<div style="font-size:12px; color:#000000;">
					  <table style="font-size:14px; color:#000000;">
					    <tr><td colspan="2">&nbsp;</td></tr>
					    <tr>
						 <td valign="top"><b>Review:</b> </td><td><?php echo ((is_array($_tmp=$this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['review_comment'])) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</td>
						</tr>
						<tr><td><b>Overall:</b> </td><!--<td><?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['overall_comment']; ?>
</td>--><td><b>Rating:</b> </td><td><?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['rating']; ?>
</td></tr>
						<?php unset($this->_sections['review']);
$this->_sections['review']['name'] = 'review';
$this->_sections['review']['loop'] = is_array($_loop=$this->_tpl_vars['Individual'][$this->_sections['RowCms']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['review']['show'] = true;
$this->_sections['review']['max'] = $this->_sections['review']['loop'];
$this->_sections['review']['step'] = 1;
$this->_sections['review']['start'] = $this->_sections['review']['step'] > 0 ? 0 : $this->_sections['review']['loop']-1;
if ($this->_sections['review']['show']) {
    $this->_sections['review']['total'] = $this->_sections['review']['loop'];
    if ($this->_sections['review']['total'] == 0)
        $this->_sections['review']['show'] = false;
} else
    $this->_sections['review']['total'] = 0;
if ($this->_sections['review']['show']):

            for ($this->_sections['review']['index'] = $this->_sections['review']['start'], $this->_sections['review']['iteration'] = 1;
                 $this->_sections['review']['iteration'] <= $this->_sections['review']['total'];
                 $this->_sections['review']['index'] += $this->_sections['review']['step'], $this->_sections['review']['iteration']++):
$this->_sections['review']['rownum'] = $this->_sections['review']['iteration'];
$this->_sections['review']['index_prev'] = $this->_sections['review']['index'] - $this->_sections['review']['step'];
$this->_sections['review']['index_next'] = $this->_sections['review']['index'] + $this->_sections['review']['step'];
$this->_sections['review']['first']      = ($this->_sections['review']['iteration'] == 1);
$this->_sections['review']['last']       = ($this->_sections['review']['iteration'] == $this->_sections['review']['total']);
?>
						<tr><td><b><?php echo $this->_tpl_vars['Individual'][$this->_sections['RowCms']['index']][$this->_sections['review']['index']]['rating_category']; ?>
 :</b> </td><!--<td><?php echo $this->_tpl_vars['Individual'][$this->_sections['RowCms']['index']][$this->_sections['review']['index']]['comment']; ?>
</td>--><td><b>Rating:</b> </td><td><?php echo $this->_tpl_vars['Individual'][$this->_sections['RowCms']['index']][$this->_sections['review']['index']]['rating']; ?>
</td></tr>
						<?php endfor; endif; ?>
					  </table>
					</div>
					</p>
				</div>
				</td>
				</tr>
				  <?php endfor; endif; ?>
			<?php else: ?>
				<tr bgcolor="#f4f4f5"><td class="plaintxt" colspan="5" align="center">No Record Found</td></tr>
			<?php endif; ?>       
				  
				 
			  </table>
			</td>
		  </tr>
		 
  </table>
</div>
<br />
<div>	<?php if ($this->_tpl_vars['pagination_arr'][3] > 0): ?>
   <table>
	  <tr>
        <td height="30" valign="middle" align="center">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td align="left" class="plaintxt">					
					<?php if ($this->_tpl_vars['pagination_arr'][2] != ""): ?>					
						<?php echo $this->_tpl_vars['pagination_arr'][2]; ?>

					<?php endif; ?>					
					</td>
					<td align="right" class="plaintxt">					   	
					<?php if ($this->_tpl_vars['pagination_arr'][1] != ""): ?>					
						<?php echo $this->_tpl_vars['pagination_arr'][1]; ?>

					<?php endif; ?>					
					</td>
				</tr>
			</table>
		</td>
      </tr>
	  </table>
	<?php endif; ?> </div><br /><br /><br /><br /><br />
</div>

<?php if ($this->_tpl_vars['IsProcess'] != 'Y'): ?>  	
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "bottom.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>
<?php endif; ?>