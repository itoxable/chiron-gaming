<?php /* Smarty version 2.6.16, created on 2013-01-04 17:50:42
         compiled from home_image_update.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "top.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h2><?php echo $this->_tpl_vars['SubmitButton']; ?>
</h2>
<div class="tabarea">
<div class="tabcontent">
<p align="right" class="fields">Fields marked with <span class="red">*</span> are required</p>
<form action="home_image_update.php?action=trans" method="post" name="frm_admin" id="frm_admin" enctype="multipart/form-data">
		<table width="100%" border="0" cellspacing="4" cellpadding="0">
		  <?php if ($this->_tpl_vars['err_msgs'] != ""): ?>     
			<tr>
				<td class="simpleText" colspan="2"><ul><?php echo $this->_tpl_vars['err_msgs']; ?>
</ul></td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
			 </tr>
		  <?php endif; ?>
		 
		  
		 
			 
		 <tr>
			<td align="left" valign="top" class="plaintxt"><?php echo $this->_tpl_vars['DisplayImageControlStr']; ?>
</td>
			<td align="left" valign="top" class="plaintxt"></td>
			<td align="left" valign="top" class="plaintxt"></td>
		 </tr>
		 <tr>
		  	<td colspan="2"></td>
		 </tr>
		  <tr>
		  <td width="30%" align="left" valign="middle" class="plaintxt"></td>
			<td width="70%" align="left" valign="middle" class="plaintxt"><label>Image Size Should Be 684x304 px</label></td>			
		 </tr>
		 <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		 </tr>
		<tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Image Alt<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <input type="text" name="image_alt" id="image_alt" class="textbox" value="<?php echo $this->_tpl_vars['FabricArr']['image_alt']; ?>
"/>
			</label></td>
		 </tr>
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Title<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <input type="text" name="title" id="title" class="textbox" value="<?php echo $this->_tpl_vars['FabricArr']['title']; ?>
"/>
			</label></td>
		 </tr>
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Link :</td>
			<td width="70%" align="left" valign="middle"><label>
			  <input type="text" name="banner_link" id="banner_link" class="textbox" value="<?php echo $this->_tpl_vars['FabricArr']['banner_link']; ?>
"/>
			</label></td>
		 </tr>
		  <tr>
		  	<td width="30%" align="left" valign="middle" class="plaintxt"></td>
			<td width="70%" align="left" valign="middle" class="plaintxt"><label>(Example: http://www.noobskool.net)</label></td>			
		 </tr>
		 <tr>
			<td width="30%" align="left" class="plaintxt" valign="top">Description<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <textarea rows="3" cols="23"  name="description" id="description" class="bigger" style="height:110px;"><?php echo $this->_tpl_vars['FabricArr']['description']; ?>
</textarea>
			</label></td>
		 </tr>		 	   
		 <tr>
			<td height="28" align="left" class="plaintxt">Active:</td>
			<td height="28" align="left" class="maintext">
				<input name="is_active" id="is_active" type="checkbox" class="maintext" value="Y" <?php if ($this->_tpl_vars['FabricArr']['is_active'] == 'Y'): ?>checked<?php endif; ?>>
			</td>
		 </tr>
		 <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		 </tr>
		 <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">
			 <input name="image_id" id="image_id" type="hidden" value="<?php echo $this->_tpl_vars['image_id']; ?>
"/>
			 <input name="imageField" type="submit" class="addsadmin" value="<?php echo $this->_tpl_vars['SubmitButton']; ?>
" <?php if ($this->_tpl_vars['image_id'] == ''): ?> style="width:150px;"<?php else: ?>style="width:150px;"<?php endif; ?>/>
&nbsp;&nbsp;&nbsp;<input name="" type="button" class="cancel" value="Cancel" onclick="window.location.href='home_image_manager.php?IsPreserved=Y'"/></td>
		 </tr>
		 <tr>
		 	<td>&nbsp;</td>
		 	<td>&nbsp;</td>
		 </tr>
		</table>
</form>

	</div>
 </div>
    <p>&nbsp;</p>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "bottom.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>