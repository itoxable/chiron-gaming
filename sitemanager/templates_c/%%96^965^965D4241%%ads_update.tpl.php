<?php /* Smarty version 2.6.16, created on 2013-01-04 21:04:57
         compiled from ads_update.tpl */ ?>
<?php echo '
<script type="text/javascript">
function focusit(id,text){
val=document.getElementById(id).value;
if(val==text)
	document.getElementById(id).value=\'\';
}
function blurit(id,text){
val=document.getElementById(id).value;
if(val==\'\')
	document.getElementById(id).value=text;
}
</script>	
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "top.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h2><?php echo $this->_tpl_vars['SubmitButton']; ?>
</h2>
<div class="tabarea">
<div class="tabcontent">
<p align="right" class="fields">Fields marked with <span class="red">*</span> are required</p>
<form action="ads_update.php?action=trans" method="post" name="frm_admin" id="frm_admin" enctype="multipart/form-data">
		<table width="100%" border="0" cellspacing="4" cellpadding="0">
		  <?php if ($this->_tpl_vars['err_msgs'] != ""): ?>     
			<tr>
				<td class="simpleText" colspan="2"><ul><?php echo $this->_tpl_vars['err_msgs']; ?>
</ul></td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top">&nbsp;</td>
			 </tr>
		  <?php endif; ?>
		 <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		 </tr>
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Ad Position<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
				<select name="ads_position" id="ads_position" class="selectbox" style="height:35px;">
					<option value="">Select Position</option>
					<option value="1" <?php if ($this->_tpl_vars['FabricArr']['ads_position'] == 1): ?> selected="selected" <?php endif; ?>>Right Panel</option>
					<option value="2" <?php if ($this->_tpl_vars['FabricArr']['ads_position'] == 2): ?> selected="selected" <?php endif; ?>>Left Panel</option>
				</select>
			</label></td>
		 </tr>
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Ad Type<span class="red">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
				<select name="ads_type" id="ads_type" class="selectbox" style="height:35px;">
					<option value="">Select Type</option>
					<option value="1" <?php if ($this->_tpl_vars['FabricArr']['ads_type'] == 1): ?> selected="selected" <?php endif; ?>>Google Ads</option>
					<option value="2" <?php if ($this->_tpl_vars['FabricArr']['ads_type'] == 2): ?> selected="selected" <?php endif; ?>>Banner</option>
				</select>
			</label></td>
		 </tr>
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Ad Code<span class="red" id="code">*</span>:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <textarea rows="3" cols="23"  name="ads_code" id="ads_code" class="bigger" style="height:110px;"><?php echo $this->_tpl_vars['FabricArr']['ads_code']; ?>
</textarea>
			</label></td>
		 </tr>	
 		 <tr>
		  	<td width="30%" align="left" valign="middle" class="plaintxt"></td>
			<td width="70%" align="left" valign="middle" class="plaintxt"><label>Maximum width should be 300pixel</label></td>			
		 </tr>
		 <tr>
		  	<td colspan="2"></td>
		 </tr>
 		 <tr>
			<td align="left" valign="top" class="plaintxt"><?php echo $this->_tpl_vars['DisplayImageControlStr']; ?>
</td>
			<td align="left" valign="top" class="plaintxt"></td>
			<td align="left" valign="top" class="plaintxt"></td>
		 </tr>
		 <tr>
		  	<td colspan="2"></td>
		 </tr>
 	   	 <tr>
		  	<td width="30%" align="left" valign="middle" class="plaintxt"></td>
			<td width="70%" align="left" valign="middle" class="plaintxt"><label>Banner Size should be 300x300</label></td>			
		 </tr>
		 <tr>
		  	<td colspan="2"></td>
		 </tr>
		 <tr>
			<td width="30%" align="left" valign="middle" class="plaintxt">Set Banner Link:</td>
			<td width="70%" align="left" valign="middle"><label>
			  <input type="text"  name="ads_link" id="ads_link" value="<?php echo $this->_tpl_vars['FabricArr']['ads_link']; ?>
" class="textbox-a search-box" style="width:250px;" />
			</label></td>
		 </tr>	
 	   	 <tr>
		  	<td width="30%" align="left" valign="middle" class="plaintxt"></td>
			<td width="70%" align="left" valign="middle" class="plaintxt"><label>Link format should be like this http://www.noobskool.net</label></td>			
		 </tr>
		 <tr>
		  	<td colspan="2"></td>
		 </tr>
		 <tr>
			<td height="28" align="left" class="plaintxt">Set Validity:<span class="red">*</span></td>
			<td height="28" align="left" class="maintext">
			<table>
			<tr>
			<td><input class="datebox" style="width:80px;" name="ads_start_date" id="ads_start_date" readonly type="text" size="10" value="<?php echo $this->_tpl_vars['FabricArr']['ads_start_date']; ?>
" onfocus="focusit('ads_start_date','Start Date');" onblur="blurit('ads_start_date','Start Date');" /><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fStartPop(document.frm_admin.ads_start_date,'');return false;" HIDEFOCUS><img src="images/icon_calender.gif" alt="" border="0" /></a></td>
			<td><input class="datebox" style="width:80px;" name="ads_end_date" id="ads_end_date" readonly type="text" size="10" value="<?php echo $this->_tpl_vars['FabricArr']['ads_end_date']; ?>
" onfocus="focusit('ads_end_date','End Date');" onblur="blurit('ads_end_date','End Date');" /><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fEndPop(document.frm_admin.ads_end_date,document.frm_admin.ads_end_date);return false;" HIDEFOCUS><img src="images/icon_calender.gif" alt="" border="0" /></a>
		  </td>
		  	</tr>
			</table>
			</td>
		 </tr>
		 <tr>
			<td height="28" align="left" class="plaintxt">Active:</td>
			<td height="28" align="left" class="maintext">
				<input name="is_active" id="is_active" type="checkbox" class="maintext" value="Y" <?php if ($this->_tpl_vars['FabricArr']['is_active'] == 'Y'): ?>checked<?php endif; ?>>
			</td>
		 </tr>
		 <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">&nbsp;</td>
		 </tr>
		 <tr>
			<td align="left" valign="top">&nbsp;</td>
			<td align="left" valign="top">
			 <input name="ads_id" id="ads_id" type="hidden" value="<?php echo $this->_tpl_vars['FabricArr']['ads_id']; ?>
"/>
			 <input name="imageField" type="submit" class="addsadmin" value="<?php echo $this->_tpl_vars['SubmitButton']; ?>
" <?php if ($this->_tpl_vars['image_id'] == ''): ?> style="width:150px;"<?php else: ?>style="width:150px;"<?php endif; ?>/>
&nbsp;&nbsp;&nbsp;<input name="" type="button" class="cancel" value="Cancel" onclick="window.location.href='ads_manager.php?IsPreserved=Y'"/></td>
		 </tr>
		 <tr>
		 	<td>&nbsp;</td>
		 	<td>&nbsp;</td>
		 </tr>
		</table>
</form>

	</div>
 </div>
    <p>&nbsp;</p>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "bottom.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>