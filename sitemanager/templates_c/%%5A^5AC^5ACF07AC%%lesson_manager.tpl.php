<?php /* Smarty version 2.6.16, created on 2013-01-04 21:06:49
         compiled from lesson_manager.tpl */ ?>
<?php if ($this->_tpl_vars['IsProcess'] != 'Y'): ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "top.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '  
<script type="text/javascript">
var j = jQuery.noConflict();
j(document).ready(function(){
		
focus_blur(\'Search_lesson_title\',\'Search by title\');
focus_blur(\'Search_from_date\',\'From\');
focus_blur(\'Search_to_date\',\'To\');
focus_blur(\'Search_from_date_in\',\'From\');
focus_blur(\'Search_to_date_in\',\'To\');
});

function GetTotalIncome(){
	if(document.getElementById(\'tot_incm\').checked==true){
			document.getElementById(\'show_tot\').style.display=\'block\';
	}
	else{
			document.getElementById(\'show_tot\').style.display=\'none\';
	}
}

function GetTotalPrice(){
	if(document.getElementById(\'tot_price\').checked==true){
			document.getElementById(\'price_tot\').style.display=\'block\';
	}
	else{
			document.getElementById(\'price_tot\').style.display=\'none\';
	}
}

function ChangeLessonStatus(lesson_status_id,lesson_id){

	/*j.post("change_status.php", {lesson_status_id:lesson_status_id, lesson_id:lesson_id}, 
		function(data){
		alert(data);
		if(data.msg){
		    j(\'#msgbox\').fadeIn(0);
		    j(\'#msgbox\').html(data.msg);
		    j(\'#msgbox\').fadeOut(5000);
			location.reload();
		}
		if(data.option_list){
			j(\'#lesson_status_id\').html(data.option_list);
			location.reload();
		}
	});*/
	var params ={

		\'module\': \'chng_status\',
		\'lesson_status_id\': lesson_status_id,
		\'lesson_id\': lesson_id,
	};

	
	j.ajax({
		type: "POST",
		url:  \'change_status.php\',
		data: params,
		dataType : \'json\',
		success: function(data){
		//alert(data.lesson_status_id);
			if(data.msg != \'\'){
				j(\'#msgbox\').fadeIn(0);
				j(\'#msgbox\').html(data.msg);
				j(\'#msgbox\').fadeOut(5000);
			}
			if(data.op != \'\'){
				//alert(data.op);
				j(\'#lesson_status_id_\'+data.lesson_id).html(data.op);
				//location.reload();
			}
        }
      });
}

function Search_Income(formID){


	var frmID=\'#\'+formID;
	
	var params ={
	
		\'module\': \'contact\'
	};

	var paramsObj = j(frmID).serializeArray();

	j.each(paramsObj, function(i, field){

		params[field.name] = field.value;

	});
	
	j.ajax({
		type: "POST",
		url:  \'income_search.php\',
		data: params,
		dataType : \'json\',
		success: function(data){
		//alert(data.IncomeSql);
			if(data.total_in != \'\'){
				//alert(data.total_in);
				document.getElementById(\'lable_total\').style.display="block";
				j(\'#total_income\').html(data.total_in);
			}
        }
      });


}
</script>
'; ?>

<h2> Lesson <div id="TransMsgDisplay" align="right" style="height:3px" ></div></h2>

	<div style="padding:10px; color:#330000; font-weight:bold; font-size:16px; font-style:italic;">General Search</div>

	<form method="POST" name="form_search" id="form_search" action="<?php echo $this->_tpl_vars['page_name']; ?>
" onsubmit="return false;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td width="29%" align="left" valign="middle" style="padding-left:5px;">	
				<input size="30" type="text" name="Search_lesson_title" id="Search_lesson_title" value="<?php echo $this->_tpl_vars['Search_lesson_title']; ?>
" class="textbox-a search-box"/>	
		      </td>
				<td colspan="2">&nbsp;</td>
		  </tr>
			 <tr>
		  	  <td width="50%" align="left" valign="middle" style="padding-left:5px;">
				<div style="float:left;">
					<input type="text" name="Search_from_date" id="Search_from_date" class="datebox" value="<?php echo $this->_tpl_vars['Search_from_date']; ?>
" style="width:80px;" readonly=""/><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_from_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				</div>
				<div style="float:left; margin-left:10px;">
					<input type="text" name="Search_to_date" id="Search_to_date" class="datebox" value="<?php echo $this->_tpl_vars['Search_to_date']; ?>
" style="width:80px;" readonly=""/><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_to_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				</div>
			  </td>
				<td colspan="2" style=" color:#333333;"><table width="50%%" border="0" cellspacing="0" cellpadding="0">
       <tr>
             <th scope="col"><select name="Search_lesson_status" id="Search_lesson_status" class="selectbox">
					<option value="">--Select Status--</option>
					<?php unset($this->_sections['Rows']);
$this->_sections['Rows']['name'] = 'Rows';
$this->_sections['Rows']['loop'] = is_array($_loop=$this->_tpl_vars['SelectStatusArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['Rows']['show'] = true;
$this->_sections['Rows']['max'] = $this->_sections['Rows']['loop'];
$this->_sections['Rows']['step'] = 1;
$this->_sections['Rows']['start'] = $this->_sections['Rows']['step'] > 0 ? 0 : $this->_sections['Rows']['loop']-1;
if ($this->_sections['Rows']['show']) {
    $this->_sections['Rows']['total'] = $this->_sections['Rows']['loop'];
    if ($this->_sections['Rows']['total'] == 0)
        $this->_sections['Rows']['show'] = false;
} else
    $this->_sections['Rows']['total'] = 0;
if ($this->_sections['Rows']['show']):

            for ($this->_sections['Rows']['index'] = $this->_sections['Rows']['start'], $this->_sections['Rows']['iteration'] = 1;
                 $this->_sections['Rows']['iteration'] <= $this->_sections['Rows']['total'];
                 $this->_sections['Rows']['index'] += $this->_sections['Rows']['step'], $this->_sections['Rows']['iteration']++):
$this->_sections['Rows']['rownum'] = $this->_sections['Rows']['iteration'];
$this->_sections['Rows']['index_prev'] = $this->_sections['Rows']['index'] - $this->_sections['Rows']['step'];
$this->_sections['Rows']['index_next'] = $this->_sections['Rows']['index'] + $this->_sections['Rows']['step'];
$this->_sections['Rows']['first']      = ($this->_sections['Rows']['iteration'] == 1);
$this->_sections['Rows']['last']       = ($this->_sections['Rows']['iteration'] == $this->_sections['Rows']['total']);
?>
						<option value="<?php echo $this->_tpl_vars['SelectStatusArr'][$this->_sections['Rows']['index']]['lesson_status_id']; ?>
" <?php if ($this->_tpl_vars['SelectStatusArr'][$this->_sections['Rows']['index']]['lesson_status_id'] == $this->_tpl_vars['Search_lesson_status']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['SelectStatusArr'][$this->_sections['Rows']['index']]['lesson_status_title']; ?>
</option>
					<?php endfor; endif; ?>
				</select></th>
             <th scope="col"><select name="Search_student_id" id="Search_student_id" class="selectbox">
					<option value="">--Select Student--</option>
					<?php unset($this->_sections['Rows']);
$this->_sections['Rows']['name'] = 'Rows';
$this->_sections['Rows']['loop'] = is_array($_loop=$this->_tpl_vars['SelectStudentArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['Rows']['show'] = true;
$this->_sections['Rows']['max'] = $this->_sections['Rows']['loop'];
$this->_sections['Rows']['step'] = 1;
$this->_sections['Rows']['start'] = $this->_sections['Rows']['step'] > 0 ? 0 : $this->_sections['Rows']['loop']-1;
if ($this->_sections['Rows']['show']) {
    $this->_sections['Rows']['total'] = $this->_sections['Rows']['loop'];
    if ($this->_sections['Rows']['total'] == 0)
        $this->_sections['Rows']['show'] = false;
} else
    $this->_sections['Rows']['total'] = 0;
if ($this->_sections['Rows']['show']):

            for ($this->_sections['Rows']['index'] = $this->_sections['Rows']['start'], $this->_sections['Rows']['iteration'] = 1;
                 $this->_sections['Rows']['iteration'] <= $this->_sections['Rows']['total'];
                 $this->_sections['Rows']['index'] += $this->_sections['Rows']['step'], $this->_sections['Rows']['iteration']++):
$this->_sections['Rows']['rownum'] = $this->_sections['Rows']['iteration'];
$this->_sections['Rows']['index_prev'] = $this->_sections['Rows']['index'] - $this->_sections['Rows']['step'];
$this->_sections['Rows']['index_next'] = $this->_sections['Rows']['index'] + $this->_sections['Rows']['step'];
$this->_sections['Rows']['first']      = ($this->_sections['Rows']['iteration'] == 1);
$this->_sections['Rows']['last']       = ($this->_sections['Rows']['iteration'] == $this->_sections['Rows']['total']);
?>
						<option value="<?php echo $this->_tpl_vars['SelectStudentArr'][$this->_sections['Rows']['index']]['student_id']; ?>
" <?php if ($this->_tpl_vars['SelectStudentArr'][$this->_sections['Rows']['index']]['student_id'] == $this->_tpl_vars['Search_student_id']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['SelectStudentArr'][$this->_sections['Rows']['index']]['student_name']; ?>
</option>
					<?php endfor; endif; ?>
				</select></th>
             <th scope="col"><select name="Search_coach_id" id="Search_coach_id" class="selectbox">
					<option value="">--Select Coach--</option>
					<?php unset($this->_sections['Rows']);
$this->_sections['Rows']['name'] = 'Rows';
$this->_sections['Rows']['loop'] = is_array($_loop=$this->_tpl_vars['SelectCoachArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['Rows']['show'] = true;
$this->_sections['Rows']['max'] = $this->_sections['Rows']['loop'];
$this->_sections['Rows']['step'] = 1;
$this->_sections['Rows']['start'] = $this->_sections['Rows']['step'] > 0 ? 0 : $this->_sections['Rows']['loop']-1;
if ($this->_sections['Rows']['show']) {
    $this->_sections['Rows']['total'] = $this->_sections['Rows']['loop'];
    if ($this->_sections['Rows']['total'] == 0)
        $this->_sections['Rows']['show'] = false;
} else
    $this->_sections['Rows']['total'] = 0;
if ($this->_sections['Rows']['show']):

            for ($this->_sections['Rows']['index'] = $this->_sections['Rows']['start'], $this->_sections['Rows']['iteration'] = 1;
                 $this->_sections['Rows']['iteration'] <= $this->_sections['Rows']['total'];
                 $this->_sections['Rows']['index'] += $this->_sections['Rows']['step'], $this->_sections['Rows']['iteration']++):
$this->_sections['Rows']['rownum'] = $this->_sections['Rows']['iteration'];
$this->_sections['Rows']['index_prev'] = $this->_sections['Rows']['index'] - $this->_sections['Rows']['step'];
$this->_sections['Rows']['index_next'] = $this->_sections['Rows']['index'] + $this->_sections['Rows']['step'];
$this->_sections['Rows']['first']      = ($this->_sections['Rows']['iteration'] == 1);
$this->_sections['Rows']['last']       = ($this->_sections['Rows']['iteration'] == $this->_sections['Rows']['total']);
?>
						<option value="<?php echo $this->_tpl_vars['SelectCoachArr'][$this->_sections['Rows']['index']]['coach_id']; ?>
" <?php if ($this->_tpl_vars['SelectCoachArr'][$this->_sections['Rows']['index']]['coach_id'] == $this->_tpl_vars['Search_coach_id']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['SelectCoachArr'][$this->_sections['Rows']['index']]['coach_name']; ?>
</option>
					<?php endfor; endif; ?>
				</select></th>
        </tr>
</table>
</td>
		  </tr>
	  </table>
		 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:#006600 1px solid;">
			  <tr>
				<td width="29%" align="left" valign="middle" style="color:#000000; padding-left:5px;">
				<!--<select name="Search_student_id" id="Search_student_id" class="selectbox">
					<option value="">--Select Student--</option>
					<?php unset($this->_sections['Rows']);
$this->_sections['Rows']['name'] = 'Rows';
$this->_sections['Rows']['loop'] = is_array($_loop=$this->_tpl_vars['SelectStudentArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['Rows']['show'] = true;
$this->_sections['Rows']['max'] = $this->_sections['Rows']['loop'];
$this->_sections['Rows']['step'] = 1;
$this->_sections['Rows']['start'] = $this->_sections['Rows']['step'] > 0 ? 0 : $this->_sections['Rows']['loop']-1;
if ($this->_sections['Rows']['show']) {
    $this->_sections['Rows']['total'] = $this->_sections['Rows']['loop'];
    if ($this->_sections['Rows']['total'] == 0)
        $this->_sections['Rows']['show'] = false;
} else
    $this->_sections['Rows']['total'] = 0;
if ($this->_sections['Rows']['show']):

            for ($this->_sections['Rows']['index'] = $this->_sections['Rows']['start'], $this->_sections['Rows']['iteration'] = 1;
                 $this->_sections['Rows']['iteration'] <= $this->_sections['Rows']['total'];
                 $this->_sections['Rows']['index'] += $this->_sections['Rows']['step'], $this->_sections['Rows']['iteration']++):
$this->_sections['Rows']['rownum'] = $this->_sections['Rows']['iteration'];
$this->_sections['Rows']['index_prev'] = $this->_sections['Rows']['index'] - $this->_sections['Rows']['step'];
$this->_sections['Rows']['index_next'] = $this->_sections['Rows']['index'] + $this->_sections['Rows']['step'];
$this->_sections['Rows']['first']      = ($this->_sections['Rows']['iteration'] == 1);
$this->_sections['Rows']['last']       = ($this->_sections['Rows']['iteration'] == $this->_sections['Rows']['total']);
?>
						<option value="<?php echo $this->_tpl_vars['SelectStudentArr'][$this->_sections['Rows']['index']]['student_id']; ?>
" <?php if ($this->_tpl_vars['SelectStudentArr'][$this->_sections['Rows']['index']]['student_id'] == $this->_tpl_vars['Search_student_id']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['SelectStudentArr'][$this->_sections['Rows']['index']]['student_name']; ?>
</option>
					<?php endfor; endif; ?>
				</select>-->
			  </td>
				<td width="27%" align="left" valign="middle" style="color:#000000;">
				<!--<select name="Search_coach_id" id="Search_coach_id" class="selectbox">
					<option value="">--Select Coach--</option>
					<?php unset($this->_sections['Rows']);
$this->_sections['Rows']['name'] = 'Rows';
$this->_sections['Rows']['loop'] = is_array($_loop=$this->_tpl_vars['SelectCoachArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['Rows']['show'] = true;
$this->_sections['Rows']['max'] = $this->_sections['Rows']['loop'];
$this->_sections['Rows']['step'] = 1;
$this->_sections['Rows']['start'] = $this->_sections['Rows']['step'] > 0 ? 0 : $this->_sections['Rows']['loop']-1;
if ($this->_sections['Rows']['show']) {
    $this->_sections['Rows']['total'] = $this->_sections['Rows']['loop'];
    if ($this->_sections['Rows']['total'] == 0)
        $this->_sections['Rows']['show'] = false;
} else
    $this->_sections['Rows']['total'] = 0;
if ($this->_sections['Rows']['show']):

            for ($this->_sections['Rows']['index'] = $this->_sections['Rows']['start'], $this->_sections['Rows']['iteration'] = 1;
                 $this->_sections['Rows']['iteration'] <= $this->_sections['Rows']['total'];
                 $this->_sections['Rows']['index'] += $this->_sections['Rows']['step'], $this->_sections['Rows']['iteration']++):
$this->_sections['Rows']['rownum'] = $this->_sections['Rows']['iteration'];
$this->_sections['Rows']['index_prev'] = $this->_sections['Rows']['index'] - $this->_sections['Rows']['step'];
$this->_sections['Rows']['index_next'] = $this->_sections['Rows']['index'] + $this->_sections['Rows']['step'];
$this->_sections['Rows']['first']      = ($this->_sections['Rows']['iteration'] == 1);
$this->_sections['Rows']['last']       = ($this->_sections['Rows']['iteration'] == $this->_sections['Rows']['total']);
?>
						<option value="<?php echo $this->_tpl_vars['SelectCoachArr'][$this->_sections['Rows']['index']]['coach_id']; ?>
" <?php if ($this->_tpl_vars['SelectCoachArr'][$this->_sections['Rows']['index']]['coach_id'] == $this->_tpl_vars['Search_coach_id']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['SelectCoachArr'][$this->_sections['Rows']['index']]['coach_name']; ?>
</option>
					<?php endfor; endif; ?>
				</select>-->
			  </td>
			  <td width="44%" colspan="2" align="left" valign="middle" style="color:#000000;">
				<!--<select name="Search_lesson_status" id="Search_lesson_status" class="selectbox">
					<option value="">--Select Status--</option>
					<?php unset($this->_sections['Rows']);
$this->_sections['Rows']['name'] = 'Rows';
$this->_sections['Rows']['loop'] = is_array($_loop=$this->_tpl_vars['SelectStatusArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['Rows']['show'] = true;
$this->_sections['Rows']['max'] = $this->_sections['Rows']['loop'];
$this->_sections['Rows']['step'] = 1;
$this->_sections['Rows']['start'] = $this->_sections['Rows']['step'] > 0 ? 0 : $this->_sections['Rows']['loop']-1;
if ($this->_sections['Rows']['show']) {
    $this->_sections['Rows']['total'] = $this->_sections['Rows']['loop'];
    if ($this->_sections['Rows']['total'] == 0)
        $this->_sections['Rows']['show'] = false;
} else
    $this->_sections['Rows']['total'] = 0;
if ($this->_sections['Rows']['show']):

            for ($this->_sections['Rows']['index'] = $this->_sections['Rows']['start'], $this->_sections['Rows']['iteration'] = 1;
                 $this->_sections['Rows']['iteration'] <= $this->_sections['Rows']['total'];
                 $this->_sections['Rows']['index'] += $this->_sections['Rows']['step'], $this->_sections['Rows']['iteration']++):
$this->_sections['Rows']['rownum'] = $this->_sections['Rows']['iteration'];
$this->_sections['Rows']['index_prev'] = $this->_sections['Rows']['index'] - $this->_sections['Rows']['step'];
$this->_sections['Rows']['index_next'] = $this->_sections['Rows']['index'] + $this->_sections['Rows']['step'];
$this->_sections['Rows']['first']      = ($this->_sections['Rows']['iteration'] == 1);
$this->_sections['Rows']['last']       = ($this->_sections['Rows']['iteration'] == $this->_sections['Rows']['total']);
?>
						<option value="<?php echo $this->_tpl_vars['SelectStatusArr'][$this->_sections['Rows']['index']]['lesson_status_id']; ?>
" <?php if ($this->_tpl_vars['SelectStatusArr'][$this->_sections['Rows']['index']]['lesson_status_id'] == $this->_tpl_vars['Search_lesson_status']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['SelectStatusArr'][$this->_sections['Rows']['index']]['lesson_status_title']; ?>
</option>
					<?php endfor; endif; ?>
				</select>-->
			  </td>
			</tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr>
			<!--<td width="22%" align="left" valign="middle">
				<div style="float:left;">
					<input type="text" name="Search_from_date" id="Search_from_date" class="datebox" value="<?php echo $this->_tpl_vars['Search_from_date']; ?>
" style="width:80px;"/>
					<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_from_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				</div>
				<div style="float:right;">
					<input type="text" name="Search_to_date" id="Search_to_date" class="datebox" value="<?php echo $this->_tpl_vars['Search_to_date']; ?>
" style="width:80px;"/>
					<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_to_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				</div>
			  </td>-->
				<!--<td colspan="2" align="left" valign="middle" style="color:#000000; padding-left:5px;">
				Select Status:&nbsp;
				<select name="Search_lesson_status" id="Search_lesson_status" class="selectbox">
					<option value="">--Select--</option>
					<?php unset($this->_sections['Rows']);
$this->_sections['Rows']['name'] = 'Rows';
$this->_sections['Rows']['loop'] = is_array($_loop=$this->_tpl_vars['SelectStatusArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['Rows']['show'] = true;
$this->_sections['Rows']['max'] = $this->_sections['Rows']['loop'];
$this->_sections['Rows']['step'] = 1;
$this->_sections['Rows']['start'] = $this->_sections['Rows']['step'] > 0 ? 0 : $this->_sections['Rows']['loop']-1;
if ($this->_sections['Rows']['show']) {
    $this->_sections['Rows']['total'] = $this->_sections['Rows']['loop'];
    if ($this->_sections['Rows']['total'] == 0)
        $this->_sections['Rows']['show'] = false;
} else
    $this->_sections['Rows']['total'] = 0;
if ($this->_sections['Rows']['show']):

            for ($this->_sections['Rows']['index'] = $this->_sections['Rows']['start'], $this->_sections['Rows']['iteration'] = 1;
                 $this->_sections['Rows']['iteration'] <= $this->_sections['Rows']['total'];
                 $this->_sections['Rows']['index'] += $this->_sections['Rows']['step'], $this->_sections['Rows']['iteration']++):
$this->_sections['Rows']['rownum'] = $this->_sections['Rows']['iteration'];
$this->_sections['Rows']['index_prev'] = $this->_sections['Rows']['index'] - $this->_sections['Rows']['step'];
$this->_sections['Rows']['index_next'] = $this->_sections['Rows']['index'] + $this->_sections['Rows']['step'];
$this->_sections['Rows']['first']      = ($this->_sections['Rows']['iteration'] == 1);
$this->_sections['Rows']['last']       = ($this->_sections['Rows']['iteration'] == $this->_sections['Rows']['total']);
?>
						<option value="<?php echo $this->_tpl_vars['SelectStatusArr'][$this->_sections['Rows']['index']]['lesson_status_id']; ?>
" <?php if ($this->_tpl_vars['SelectStatusArr'][$this->_sections['Rows']['index']]['lesson_status_id'] == $this->_tpl_vars['Search_lesson_status']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['SelectStatusArr'][$this->_sections['Rows']['index']]['lesson_status_title']; ?>
</option>
					<?php endfor; endif; ?>
				</select>
			  </td>-->
			</tr>
			<tr>	
				<td valign="middle">
				<input type="hidden" name="dosearch" value="GO">
				<input  name="submitdosearch" value="GO" type="button" class="go" style="margin-bottom:10px;" onClick="ManagerGeneralForm('<?php echo $this->_tpl_vars['page_name']; ?>
?action=list_search','form_search')" />
				<input name="" type="button"  class="reset" value="RESET" onClick="window.location.href='<?php echo $this->_tpl_vars['page_name']; ?>
';"/></td>
				
				
			</tr>
	  </table>
</form>
	
	<div style="padding:10px; color:#330000; font-weight:bold; font-size:16px; font-style:italic;">Income Search</div>
	
<form method="POST" name="form_search_in" id="form_search_in" action="<?php echo $this->_tpl_vars['page_name']; ?>
" onsubmit="return false;">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="24%" style="color:#000000; padding-top:10px;">Check it to see total income: </td>
      <td width="76%" align="left" valign="bottom"><input type="checkbox" name="tot_incm" id="tot_incm" onclick="GetTotalIncome();" style="float:left;" />
      &nbsp;<span style="color:#000000; display:none; width:300px; float:left; padding-left:50px;" id="show_tot">Total Income: &nbsp;<b>$</b><?php echo $this->_tpl_vars['total']; ?>
&nbsp;&nbsp;Total Price: &nbsp;<b>$</b><?php echo $this->_tpl_vars['total_price']; ?>
</span> </td>
    </tr>
    <!--<tr>
		<td width="24%" style="color:#000000; padding-top:10px;">Check it to see total price: </td>
	  	<td width="50%"><input type="checkbox" name="tot_price" id="tot_price" onclick="GetTotalPrice();" style="float:left;" />&nbsp;<span style="color:#000000; display:none; width:300px; float:left; padding-left:50px;" id="price_tot">Total Price: &nbsp;<b>$</b><?php echo $this->_tpl_vars['total_price']; ?>
</span>
		</td>
		</tr>-->
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:#006600 1px solid;">
    <tr>
      <td width="35%" align="left" valign="middle"><div style="float:left;">
          <input type="text" name="Search_from_date_in" id="Search_from_date_in" class="datebox" value="<?php echo $this->_tpl_vars['Search_from_date_in']; ?>
" style="width:100px;" readonly=""/>
        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search_in.Search_from_date_in,document.form_search_in.Search_from_date_in);return false;" hidefocus><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17" /></a> </div>
          <div style="float:right;">
            <input type="text" name="Search_to_date_in" id="Search_to_date_in" class="datebox" value="<?php echo $this->_tpl_vars['Search_to_date_in']; ?>
" style="width:80px;" readonly=""/>
        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search_in.Search_from_date_in,document.form_search_in.Search_to_date_in);return false;" hidefocus><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17" /></a> </div></td>
      <td style="color:#000000; padding-left:35px;" width="65%"><span id="lable_total" style="display:none; float:left;">Total Income:&nbsp;&nbsp;<b>$</b></span><span id="total_income" style="float:left;"></span></td>
    </tr>
    <tr>
      <td valign="middle" colspan="2"><input type="hidden" name="in_search" value="DO" />
          <input  name="submitdosearch2" value="GO" type="button" class="go" style="margin-bottom:10px;" onclick="Search_Income('form_search_in');" />
          <input name="Input" type="button"  class="reset" value="RESET" onclick="window.location.href='<?php echo $this->_tpl_vars['page_name']; ?>
';"/></td>
    </tr>
  </table>
</form>
	<!--<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="24%" style="color:#000000; padding-top:10px;">Check it to see total income: </td>
	  	<td width="9%"><input type="checkbox" name="tot_incm" id="tot_incm" onclick="GetTotalIncome();" /></td>
		<td width="67%" style="color:#000000;"><div id="show_tot" style="display:none;">Total Income: &nbsp;<b>$</b><?php echo $this->_tpl_vars['total']; ?>
</div></td>
	</tr>
			<tr><td colspan="3">&nbsp;</td></tr><tr><td colspan="3">&nbsp;</td></tr>

	</table>-->
<!--<div align="right" class="addNews_secand"><a href="language_update.php">+ Add Language</a></div>-->
<table width="100%">   <tr><td colspan="2">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>  </table>
<?php endif; ?>
<div id="records_listing">	
<?php if ($this->_tpl_vars['MessgReportText'] != ""): ?><div align="left" class="successful2" style="color:#FF0000; margin:-25px 0 0 0;"><?php echo $this->_tpl_vars['MessgReportText']; ?>
</div><?php endif; ?>
<div id="msgbox" style="color:#009900;"></div>		
<div class="newsName">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td colspan="5" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#9f9f9f">
				  <tr  bgcolor="#9f9f9f">
					<td  align="center" width="10%" valign="middle" class="padleft">
						<a href="#"  onClick="ManagerGeneral('<?php echo $this->_tpl_vars['page_name']; ?>
?action=list_order&do_order=GO&OrderByID=1&OrderType=<?php echo $this->_tpl_vars['OrderType']; ?>
&<?php echo $this->_tpl_vars['SearchLink']; ?>
&from=<?php echo $this->_tpl_vars['from']; ?>
')" >								
							<span class="whitetext"><strong>Title</strong></span>									
							<?php echo $this->_tpl_vars['ReturnSortingArr']['DisplaySortingImage'][1]; ?>
				
						</a>	
					</td>
					<td  align="center" width="12%" valign="middle" class="padleft">
							<span class="whitetext"><strong>Coach</strong></span>		
					</td>
					<td  align="center" width="12%" valign="middle" class="padleft">
							<span class="whitetext"><strong>Student</strong></span>									
					</td>
					<td width="6%" align="center" valign="middle" >
					<a href="#"  onClick="ManagerGeneral('<?php echo $this->_tpl_vars['page_name']; ?>
?action=list_order&do_order=GO&OrderByID=4&OrderType=<?php echo $this->_tpl_vars['OrderType']; ?>
&<?php echo $this->_tpl_vars['SearchLink']; ?>
&from=<?php echo $this->_tpl_vars['from']; ?>
')" >								
							<span class="whitetext"><strong>Price</strong></span>
					</a>
					</td>
					<td width="12%" align="center" valign="middle" >
							<span class="whitetext"><strong>Commission</strong></span>
					</td>
					<td width="12%" align="center" valign="middle" >
					<a href="#"  onClick="ManagerGeneral('<?php echo $this->_tpl_vars['page_name']; ?>
?action=list_order&do_order=GO&OrderByID=3&OrderType=<?php echo $this->_tpl_vars['OrderType']; ?>
&<?php echo $this->_tpl_vars['SearchLink']; ?>
&from=<?php echo $this->_tpl_vars['from']; ?>
')" >								
							<span class="whitetext"><strong>Paid Date</strong></span>
					</a>
					</td>
					<td width="12%" align="center" valign="middle" >
					<a href="#"  onClick="ManagerGeneral('<?php echo $this->_tpl_vars['page_name']; ?>
?action=list_order&do_order=GO&OrderByID=2&OrderType=<?php echo $this->_tpl_vars['OrderType']; ?>
&<?php echo $this->_tpl_vars['SearchLink']; ?>
&from=<?php echo $this->_tpl_vars['from']; ?>
')" >								
							<span class="whitetext"><strong>Date Added</strong></span>
					</a>
					</td>
					<td width="8%" align="center" class="whitetext" valign="middle"><strong>Status</strong></td>	
					<td width="4%" align="center" class="whitetext" valign="middle"><strong>View</strong></td>					
					<td width="6%" align="center" class="whitetext" valign="middle"><strong>Actions</strong></td>
				  </tr>
				</table>
			</td>
		</tr>
		<tr>
		  <td colspan="5" align="left" valign="top">
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#878787">
				<?php if ($this->_tpl_vars['NumSelectCms'] > 0): ?>
			<?php unset($this->_sections['RowCms']);
$this->_sections['RowCms']['name'] = 'RowCms';
$this->_sections['RowCms']['loop'] = is_array($_loop=$this->_tpl_vars['SelectCmsArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['RowCms']['show'] = true;
$this->_sections['RowCms']['max'] = $this->_sections['RowCms']['loop'];
$this->_sections['RowCms']['step'] = 1;
$this->_sections['RowCms']['start'] = $this->_sections['RowCms']['step'] > 0 ? 0 : $this->_sections['RowCms']['loop']-1;
if ($this->_sections['RowCms']['show']) {
    $this->_sections['RowCms']['total'] = $this->_sections['RowCms']['loop'];
    if ($this->_sections['RowCms']['total'] == 0)
        $this->_sections['RowCms']['show'] = false;
} else
    $this->_sections['RowCms']['total'] = 0;
if ($this->_sections['RowCms']['show']):

            for ($this->_sections['RowCms']['index'] = $this->_sections['RowCms']['start'], $this->_sections['RowCms']['iteration'] = 1;
                 $this->_sections['RowCms']['iteration'] <= $this->_sections['RowCms']['total'];
                 $this->_sections['RowCms']['index'] += $this->_sections['RowCms']['step'], $this->_sections['RowCms']['iteration']++):
$this->_sections['RowCms']['rownum'] = $this->_sections['RowCms']['iteration'];
$this->_sections['RowCms']['index_prev'] = $this->_sections['RowCms']['index'] - $this->_sections['RowCms']['step'];
$this->_sections['RowCms']['index_next'] = $this->_sections['RowCms']['index'] + $this->_sections['RowCms']['step'];
$this->_sections['RowCms']['first']      = ($this->_sections['RowCms']['iteration'] == 1);
$this->_sections['RowCms']['last']       = ($this->_sections['RowCms']['iteration'] == $this->_sections['RowCms']['total']);
?>
			
				  <tr <?php if (( $this->_tpl_vars['templatelite']['section']['RowCms']['index'] % 2 ) == 0): ?> bgcolor="#ffffff" <?php else: ?> bgcolor="#ffffff" <?php endif; ?>>
					<td  align="left" width="10%" valign="middle" class="simpletxt">
					<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['lesson_title']; ?>

					</td>
					<td  align="left" width="12%" valign="middle" class="simpletxt">
					<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['coach_name']; ?>

					</td>
					<td  align="left" width="12%" valign="middle" class="simpletxt">
					<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['student_name']; ?>

					</td>
					<td width="6%" class="simpletxt" align="center" valign="middle">$<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['lesson_price']; ?>
</td>
					<td width="12%" class="simpletxt" align="center" valign="middle">$<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['admin_income']; ?>
<br />(<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['lesson_commision_admin']; ?>
 %)</td>
					<td width="12%" class="simpletxt" align="center" valign="middle"><?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['lesson_payment_date']; ?>
</td>
					<td width="12%" class="simpletxt" align="center" valign="middle">
					<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['date_added']; ?>

					</td>
					<td width="8%" height="28" align="center" valign="middle">
						<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['status_name']; ?>

					</td>
					<td width="4%" height="28" align="center" valign="middle">
					<?php if ($this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['lesson_status_id'] == '4' || $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['lesson_status_id'] == '5'): ?>
						<a href="lesson_update.php?lesson_id=<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['lesson_id']; ?>
">
						  <img src="images/view.gif" alt="" width="15" height="14" border="0" />
					  	</a>
					<?php else: ?>
						<a href="lesson_update.php?lesson_id=<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['lesson_id']; ?>
">
						  <img src="images/edit.gif" alt="" width="15" height="14" border="0" />
					  	</a>
					<?php endif; ?>
					</td>
					<td width="6%" align="center" valign="middle">
					<?php if ($this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['lesson_status_id'] != '2'): ?>
					<a href="#" onClick="ConfirmDelete('<?php echo $this->_tpl_vars['page_name']; ?>
?action=del&<?php echo $this->_tpl_vars['PreserveLink']; ?>
','<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['lesson_id']; ?>
','<?php echo $this->_tpl_vars['SelectCmsArr'][$this->_sections['RowCms']['index']]['lesson_title']; ?>
','Lesson:: ')">
						  <img src="images/delete_icon.gif" border="0" alt="" />
					  </a>					  <?php else: ?>N/A<?php endif; ?>
					</td>
				 </tr>
				  <?php endfor; endif; ?>
			<?php else: ?>
				<tr bgcolor="#f4f4f5"><td class="plaintxt" colspan="5" align="center">No Record Found</td></tr>
			<?php endif; ?>       
				  
				 
			  </table>
			</td>
		 <!-- <tr>
		  <td colspan="2">&nbsp;</td>
		  </tr>
  		  <tr>
		  <td colspan="2" style="color:#000000;font-family: 'Trebuchet MS','Myriad Pro',Arial,Verdana,sans-serif;font-size: 14px; border:1px solid #006600;">Total Income:&nbsp;<?php echo $this->_tpl_vars['total']; ?>
</td>
		  </tr>-->

	  </tr>
		 
  </table>
</div>
<br />
<div>	<?php if ($this->_tpl_vars['pagination_arr'][3] > 0): ?>
   <table>
	  <tr>
        <td height="30" valign="middle" align="center">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td align="left" class="plaintxt">					
					<?php if ($this->_tpl_vars['pagination_arr'][2] != ""): ?>					
						<?php echo $this->_tpl_vars['pagination_arr'][2]; ?>

					<?php endif; ?>					
					</td>
					<td align="right" class="plaintxt">					   	
					<?php if ($this->_tpl_vars['pagination_arr'][1] != ""): ?>					
						<?php echo $this->_tpl_vars['pagination_arr'][1]; ?>

					<?php endif; ?>					
					</td>
				</tr>
			</table>
		</td>
      </tr>
    </table>
	<?php endif; ?> </div>
</div>

<?php if ($this->_tpl_vars['IsProcess'] != 'Y'): ?>  	
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "bottom.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>
<?php endif; ?>