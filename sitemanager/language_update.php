<?php
/*******************************************************************************/
			#This page is to add/edit language details
			
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$language_id=$_REQUEST['language_id'];
$language_name=$_REQUEST['language_name'];
$language_abbreviation=$_REQUEST['language_abbreviation'];
$action=$_REQUEST['action'];

if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	
	/* Holding Data If Error Starts */

	$FabricArr['language_name']			 = htmlspecialchars(trim($language_name));
	$FabricArr['language_abbreviation']	 = htmlspecialchars(trim($language_abbreviation));
	$FabricArr['is_active']				 = $is_active;
	
	/* Holding Data If Error Ends */

	/* Error Checking Starts */
	
	$err_msgs="";
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_language","language_name",$language_name,"Language","language_id",$language_id);
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_language","language_abbreviation",$language_abbreviation,"Language abbreviation","language_id",$language_id);
		
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{		
		if(empty($language_id))
		{
			
			 $table_name = TABLEPREFIX."_language ";

			$fields_values = array( 
									'language_name'         => $language_name,	
									'language_abbreviation' => $language_abbreviation,								
									'is_active'			    => $is_active,
									'date_added'			=> date("Y-m-d H:i:s")									
									);		
			//print_r($fields_values);
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			
			$language_id=mysql_insert_id();	

			/* Insert Into Events Ends */
		}
		else if(!empty($language_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_language";

			$fields_values = array( 
									'language_name'         => $language_name,	
									'language_abbreviation' => $language_abbreviation,								
									'is_active'			    => $is_active,
									'date_edited'			=> date("Y-m-d H:i:s")		
									);		
			
			$where="language_id='$language_id'";										

			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */
		}
				
		echo "<script>window.location.href='language_manager.php?messg=".$msgreport."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($language_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_language WHERE language_id=".$language_id;
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
    
	$FabricArr['language_name']             = show_to_control($RsCatSql["language_name"]);
	$FabricArr['language_abbreviation']     = show_to_control($RsCatSql["language_abbreviation"]);
	$FabricArr['is_active']			        = $RsCatSql["is_active"];
	
	/* Get Record For Display Ends */

	$SubmitButton="Update Language ";	
	$flag=1;
}
else
{
	$SubmitButton=" Add Language ";
}


/* Assign Smarty Variables Starts */

$smarty->assign('CmsArr',$CmsArr);
$smarty->assign('language_id',$language_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("language_update.tpl");
?>