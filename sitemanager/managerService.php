<?php

include('general_include.php');
include "checklogin.php";
include_once("../adodb/dbconfig.php");

function logToFile($msg, $filename="chiron.log") {
    $fd = fopen($_SERVER['DOCUMENT_ROOT']."/logs/".$filename, "a");
	$str = "[" . date("Y/m/d h:i:s", mktime()) . "] " . $msg;
	fwrite($fd, $str . "\n");
	fclose($fd);
}
function addGame($game_name,$game_description,$is_default=0,$game_id=""){
    try{
        $ret = array();
        if($game_id != "")
            $sql = "INSERT INTO nk_game (`game_id`,`game_name`,`game_description`,`is_default`) VALUES ('$game_id','$game_name','$game_description','$is_default')";
        else
            $sql = "INSERT INTO nk_game (`game_name`,`game_description`,`is_default`) VALUES ('$game_name','$game_description','$is_default')";
        logToFile($sql);
        $db = new DBConnection();
	$db->getConnection();
        if(mysql_query($sql)==false){
            $ret['IsSuccess'] = false;
            $ret['Msg'] = mysql_error();
        }else{
            $ret['IsSuccess'] = true;
            $ret['Msg'] = 'add success';
//            $ret['Data'] = $date;
        }
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}
function changeGameName($game_id,$game_name){
	try{
        $ret = array();
        $sql = "UPDATE nk_game SET `game_name`= '$game_name' WHERE game_id = $game_id";
        logToFile($sql);
        $db = new DBConnection();
	$db->getConnection();
        if(mysql_query($sql)==false){
            $ret['IsSuccess'] = false;
            $ret['Msg'] = mysql_error();
        }else{
            $ret['IsSuccess'] = true;
            $ret['Msg'] = 'change success';

        }
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}
function changeCategoryName($category_id,$category_name){
	try{
        $ret = array();
        $sql = "UPDATE nk_game_categories SET `category_name`= '$category_name' WHERE category_id = $category_id";
        logToFile($sql);
        $db = new DBConnection();
	$db->getConnection();
        if(mysql_query($sql)==false){
            $ret['IsSuccess'] = false;
            $ret['Msg'] = mysql_error();
        }else{
            $ret['IsSuccess'] = true;
            $ret['Msg'] = 'change success';
        }
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}

function changePropertyName($property_id,$property_name){
	try{
        $ret = array();
        $sql = "UPDATE nk_game_categories SET `property_name`= '$property_name' WHERE property_id = $property_id";
        logToFile($sql);
        $db = new DBConnection();
	$db->getConnection();
        if(mysql_query($sql)==false){
            $ret['IsSuccess'] = false;
            $ret['Msg'] = mysql_error();
        }else{
            $ret['IsSuccess'] = true;
            $ret['Msg'] = 'change success';
        }
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}


function deleteGame($game_id){
    try{
        $ret = array();
//        $sql = "UPDATE nk_game SET `status`= 2 WHERE game_id = $game_id";
        $sql = "DELETE FROM nk_game WHERE game_id = $game_id";
        logToFile($sql);
        deleteCategory($game_id);
        $db = new DBConnection();
	$db->getConnection();
        if(mysql_query($sql)==false){
            $ret['IsSuccess'] = false;
            $ret['Msg'] = mysql_error();
        }else{
            $ret['IsSuccess'] = true;
            $ret['Msg'] = 'delete success';
//            deleteCategory($game_id);
//            $ret['Data'] = $date;
        }
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}

function deleteCategory($game_id,$category_id=""){
    try{
        $ret = array();
        
        if($category_id == ""){
            $sql = "DELETE FROM nk_game_categories WHERE game_id = $game_id";
        }else
            $sql = "DELETE FROM nk_game_categories WHERE game_id = $game_id AND category_id = $category_id";
        
        logToFile($sql);
        deleteProp($game_id, $category_id);
        $db = new DBConnection();
	$db->getConnection();
        if(mysql_query($sql)==false){
            $ret['IsSuccess'] = false;
            $ret['Msg'] = mysql_error();
        }else{
            $ret['IsSuccess'] = true;
            $ret['Msg'] = 'Delete success';
            deleteProp($game_id, $category_id);
            
        }
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}

function deleteProp($game_id, $category_id="", $property_id=""){
    try{
        $ret = array();
        
        if($property_id == ""){
            $sql = "DELETE FROM nk_game_categories_properties WHERE game_id = $game_id AND category_id = $category_id";
        }else
            $sql = "DELETE FROM nk_game_categories_properties WHERE game_id = $game_id AND category_id = $category_id AND property_id = $property_id";
        
        logToFile($sql);
        
        $db = new DBConnection();
	$db->getConnection();
        if(mysql_query($sql)==false){
            $ret['IsSuccess'] = false;
            $ret['Msg'] = mysql_error();
        }else{
            $ret['IsSuccess'] = true;
            $ret['Msg'] = 'Delete success';
            deleteUserProp($game_id, $category_id, $property_id);
            
        }
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}

function deleteUserProp($game_id, $category_id="", $property_id="",$user_id=""){
    try{
        $ret = array();
        
        if($category_id == ""){
            $sql = "DELETE FROM nk_user_game_property WHERE game_id = $game_id";
        }else if($property_id == ""){
            $sql = "DELETE FROM nk_user_game_property WHERE game_id = $game_id AND category_id = $category_id";
        }else if($user_id == ""){
            $sql = "DELETE FROM nk_user_game_property WHERE game_id = $game_id AND category_id = $category_id AND property_id = $property_id";
        }else{
            $sql = "DELETE FROM nk_user_game_property WHERE game_id = $game_id AND category_id = $category_id AND property_id = $property_id AND user_id=$user_id";
        }
          
        logToFile($sql);
        
        $db = new DBConnection();
	$db->getConnection();
        if(mysql_query($sql)==false){
            $ret['IsSuccess'] = false;
            $ret['Msg'] = mysql_error();
        }else{
            $ret['IsSuccess'] = true;
            $ret['Msg'] = 'Delete success';
//            deleteProp($game_id, $category_id);
            
        }
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}

function addCategory($game_id,$category_name,$category_id=""){
    try{
        $ret = array();
        if($category_id != "")
            $sql = "INSERT INTO nk_game_categories (`game_id`,`category_name`) VALUES ('$game_id','$category_name')";
        else
            $sql = "INSERT INTO nk_game_categories (`category_id`,`game_id`,`category_name`) VALUES ('$category_id','$game_id','$category_name')";
        $db = new DBConnection();
	$db->getConnection();
        if(mysql_query($sql)==false){
            $ret['IsSuccess'] = false;
            $ret['Msg'] = mysql_error();
            
        }else{
            $ret['IsSuccess'] = true;
            $ret['Msg'] = 'Delete success';
            $ret['id'] = mysql_insert_id();
//            $ret['Data'] = $date;
        }
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}
function addProp($game_id,$category_id,$property_name){
    try{
        $ret = array();
        $sql = "INSERT INTO nk_game_categories_properties (`game_id`,`category_id`,`property_name`) VALUES ('$game_id','$category_id','$property_name')";
        $db = new DBConnection();
	$db->getConnection();
        if(mysql_query($sql)==false){
            $ret['IsSuccess'] = false;
            $ret['Msg'] = mysql_error();
        }else{
            $ret['IsSuccess'] = true;
            $ret['Msg'] = 'add success';
//            $ret['Data'] = $date;
        }
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}

function migrate(){
    $ret = array();
    try{
        $db = new DBConnection();
        $db->getConnection();
        
        $trunc = "TRUNCATE TABLE  `x_nk_user_game_property`;TRUNCATE TABLE  `nk_game_categories_properties`;TRUNCATE TABLE  `nk_game_categories`;TRUNCATE TABLE  `nk_game_categories`;";
        mysql_query($trunc);
        
        $sql = "SELECT * FROM nk_game";
        $handle = mysql_query($sql);
        while ($game = mysql_fetch_object($handle)) {
            $is_default = 0;
            if($game->is_default == 'Y')
                $is_default = 1;
            addGame($game->game_name,$game->game_description,$is_default,$game->game_id);
            if($game->is_ladder == "Y"){
                $ret = addCategory($game->game_id,"Ladder");
                $laddersql = "SELECT * FROM nk_game_ladder WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->ladder_name);
                }
            }
            if($game->is_race == "Y"){
                $ret = addCategory($game->game_id,"Race");
                $laddersql = "SELECT * FROM nk_game_race WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->race_title);
                }
            }
            if($game->is_server == "Y"){
                $ret = addCategory($game->game_id,"Server");
                $laddersql = "SELECT * FROM nk_game_server WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->server_name);
                }
            }
            if($game->is_region == "Y"){
                $ret = addCategory($game->game_id,"Region");
                $laddersql = "SELECT * FROM nk_game_region WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->region_title);
                }
            }
            if($game->is_rating == "Y"){
                $ret = addCategory($game->game_id,"Rating");
                $laddersql = "SELECT * FROM nk_game_rating WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->rating_title);
                }
            }
            if($game->is_versus == "Y"){
                $ret = addCategory($game->game_id,"Versus");
                $laddersql = "SELECT * FROM nk_game_versus WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->versus_title);
                }
            }
            
            if($game->is_team == "Y"){
                $ret = addCategory($game->game_id,"Team");
                $laddersql = "SELECT * FROM nk_game_team WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->team_title);
                }
            }
            if($game->is_type == "Y"){
                $ret = addCategory($game->game_id,"Type");
                $laddersql = "SELECT * FROM nk_game_type WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->type_title);
                }
            }
            if($game->is_champion == "Y"){
                $ret = addCategory($game->game_id,"Champion");
                $laddersql = "SELECT * FROM nk_game_champion WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->champion_title);
                }
            }
            if($game->is_map == "Y"){
                $ret = addCategory($game->game_id,"Map");
                $laddersql = "SELECT * FROM nk_game_map WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->map_title);
                }
            }
            if($game->is_mode == "Y"){
                $ret = addCategory($game->game_id,"Mode");
                $laddersql = "SELECT * FROM nk_game_mode WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->mode_title);
                }
            }
            if($game->is_class == "Y"){
                $ret = addCategory($game->game_id,"Class");
                $laddersql = "SELECT * FROM nk_game_class WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->class_title);
                }
            }
            if($game->is_role == "Y"){
                $ret = addCategory($game->game_id,"Role");
                $laddersql = "SELECT * FROM nk_game_role WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->role_title);
                }
            }
            if($game->is_hero == "Y"){
                $ret = addCategory($game->game_id,"Hero");
                $laddersql = "SELECT * FROM nk_game_hero WHERE game_id = ".$game->game_id;
//                logToFile($laddersql);
                $ladderhandle = mysql_query($laddersql);
                while ($ladder = mysql_fetch_object($ladderhandle)) {
                    addProp($game->game_id, $ret['id'], $ladder->hero_title);
                }
            }

        }
        $usergamesql = "SELECT * FROM nk_user_game";
        $usergamehandle = mysql_query($usergamesql);
        while ($usergame = mysql_fetch_object($usergamehandle)) {
            
//            logToFile($laddersql);
            $sqlGame = "SELECT * FROM nk_game WHERE game_name = (SELECT game_name FROM nk_game WHERE game_id = ".$usergame->game_id.")";
            $gamehandle = mysql_query($sqlGame);
            $game = mysql_fetch_object($gamehandle);
            logToFile($game->game_id.": ".$game->game_name);
            if($usergame->ladder_id != null && $usergame->ladder_id != ""  && $usergame->ladder_id != "0"){
                $ids= str_split($usergame->ladder_id );
                foreach ($ids as $id){
                    $sqlCat = "SELECT * FROM nk_game_categories_properties WHERE property_name = (SELECT ladder_name FROM nk_game_ladder WHERE game_id = ".$usergame->game_id." AND ladder_id = $id)";
                    $handle = mysql_query($sqlCat);
                    $category = mysql_fetch_object($handle);
                    if($category->category_id == null || $category->category_id == "" || $category->property_id == null || $category->property_id == "")
                        continue;
                    
                    
                    
                    $laddersql = "INSERT INTO nk_user_game_property (`user_id`,`user_type_id`,`game_id`,`category_id`,`property_id`) 
                        VALUES ('$usergame->user_id','$usergame->user_type_id','$game->game_id','$category->category_id','$category->property_id')";
                    logToFile($laddersql);
                    if(mysql_query($laddersql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = mysql_error();
//                        return $ret;
                    }
                }
            }
            
            
            if($usergame->race_id != null && $usergame->race_id != ""  && $usergame->race_id != "0"){
                $ids= str_split($usergame->race_id );
                foreach ($ids as $id){
                    $sqlCat = "SELECT * FROM nk_game_categories_properties WHERE property_name = (SELECT race_title FROM nk_game_race WHERE game_id = ".$usergame->game_id." AND race_id = $id)";
                    $handle = mysql_query($sqlCat);
                    $category = mysql_fetch_object($handle);
                    if($category->category_id == null || $category->category_id == "" || $category->property_id == null || $category->property_id == "")
                        continue;
                    $racesql = "INSERT INTO nk_user_game_property (`user_id`,`user_type_id`,`game_id`,`category_id`,`property_id`) 
                        VALUES ('$usergame->user_id','$usergame->user_type_id','$game->game_id','$category->category_id','$category->property_id')";
                    logToFile($racesql);
                    if(mysql_query($racesql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = mysql_error();
//                        return $ret;
                    }
                }
            }
            
            if($usergame->server_id != null && $usergame->server_id != ""  && $usergame->server_id != "0"){
                $ids= str_split($usergame->server_id );
                foreach ($ids as $id){
                    $sqlCat = "SELECT * FROM nk_game_categories_properties WHERE property_name = (SELECT server_name FROM nk_game_server WHERE game_id = ".$usergame->game_id." AND server_id = $id)";
                    $handle = mysql_query($sqlCat);
                    $category = mysql_fetch_object($handle);
                    if($category->category_id == null || $category->category_id == "" || $category->property_id == null || $category->property_id == "")
                        continue;
                    $serversql = "INSERT INTO nk_user_game_property (`user_id`,`user_type_id`,`game_id`,`category_id`,`property_id`) 
                        VALUES ('$usergame->user_id','$usergame->user_type_id','$game->game_id','$category->category_id','$category->property_id')";
                    logToFile($serversql);
                    if(mysql_query($serversql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = mysql_error();
//                        return $ret;
                    }
                }
            }
            
            if($usergame->region_id != null && $usergame->region_id != ""  && $usergame->region_id != "0"){
                $ids= str_split($usergame->region_id );
                foreach ($ids as $id){
                    $sqlCat = "SELECT * FROM nk_game_categories_properties WHERE property_name = (SELECT region_title FROM nk_game_region WHERE game_id = ".$usergame->game_id." AND region_id = $id)";
                    $handle = mysql_query($sqlCat);
                    $category = mysql_fetch_object($handle);
                    if($category->category_id == null || $category->category_id == "" || $category->property_id == null || $category->property_id == "")
                        continue;
                    $regionsql = "INSERT INTO nk_user_game_property (`user_id`,`user_type_id`,`game_id`,`category_id`,`property_id`) 
                        VALUES ('$usergame->user_id','$usergame->user_type_id','$game->game_id','$category->category_id','$category->property_id')";
                    logToFile($regionsql);
                    if(mysql_query($regionsql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = mysql_error();
//                        return $ret;
                    }
                }
            }
            
            if($usergame->class_id != null && $usergame->class_id != ""  && $usergame->class_id != "0"){
                $ids= str_split($usergame->class_id );
                foreach ($ids as $id){
                    $sqlCat = "SELECT * FROM nk_game_categories_properties WHERE property_name = (SELECT class_title FROM nk_game_class WHERE game_id = ".$usergame->game_id." AND class_id = $id)";
                    $handle = mysql_query($sqlCat);
                    $category = mysql_fetch_object($handle);
                    if($category->category_id == null || $category->category_id == "" || $category->property_id == null || $category->property_id == "")
                        continue;
                    $classsql = "INSERT INTO nk_user_game_property (`user_id`,`user_type_id`,`game_id`,`category_id`,`property_id`) 
                        VALUES ('$usergame->user_id','$usergame->user_type_id','$game->game_id','$category->category_id','$category->property_id')";
                    logToFile($classsql);
                    if(mysql_query($classsql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = mysql_error();
//                        return $ret;
                    }
                }
            }
            
            if($usergame->mode_id != null && $usergame->mode_id != ""  && $usergame->mode_id != "0"){
                $ids= str_split($usergame->mode_id );
                foreach ($ids as $id){
                    $sqlCat = "SELECT * FROM nk_game_categories_properties WHERE property_name = (SELECT mode_title FROM nk_game_mode WHERE game_id = ".$usergame->game_id." AND mode_id = $id)";
                    $handle = mysql_query($sqlCat);
                    $category = mysql_fetch_object($handle);
                    if($category->category_id == null || $category->category_id == "" || $category->property_id == null || $category->property_id == "")
                        continue;
                    $modesql = "INSERT INTO nk_user_game_property (`user_id`,`user_type_id`,`game_id`,`category_id`,`property_id`) 
                        VALUES ('$usergame->user_id','$usergame->user_type_id','$game->game_id','$category->category_id','$category->property_id')";
                    logToFile($modesql);
                    if(mysql_query($modesql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = mysql_error();
//                        return $ret;
                    }
                }
            }
            
            if($usergame->versus_id != null && $usergame->versus_id != ""  && $usergame->versus_id != "0"){
                $ids= str_split($usergame->versus_id );
                foreach ($ids as $id){
                    $sqlCat = "SELECT * FROM nk_game_categories_properties WHERE property_name = (SELECT versus_title FROM nk_game_versus WHERE game_id = ".$usergame->game_id." AND versus_id = $id)";
                    $handle = mysql_query($sqlCat);
                    $category = mysql_fetch_object($handle);
                    if($category->category_id == null || $category->category_id == "" || $category->property_id == null || $category->property_id == "")
                        continue;
                    $versussql = "INSERT INTO nk_user_game_property (`user_id`,`user_type_id`,`game_id`,`category_id`,`property_id`) 
                        VALUES ('$usergame->user_id','$usergame->user_type_id','$game->game_id','$category->category_id','$category->property_id')";
                    logToFile($versussql);
                    if(mysql_query($versussql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = mysql_error();
//                        return $ret;
                    }
                }
            }
            
            if($usergame->team_id != null && $usergame->team_id != ""  && $usergame->team_id != "0"){
                $ids= str_split($usergame->team_id );
                foreach ($ids as $id){
                    $sqlCat = "SELECT * FROM nk_game_categories_properties WHERE property_name = (SELECT team_title FROM nk_game_team WHERE game_id = ".$usergame->game_id." AND team_id = $id)";
                    $handle = mysql_query($sqlCat);
                    $category = mysql_fetch_object($handle);
                    if($category->category_id == null || $category->category_id == "" || $category->property_id == nuteaml || $category->property_id == "")
                        continue;
                    $teamsql = "INSERT INTO nk_user_game_property (`user_id`,`user_type_id`,`game_id`,`category_id`,`property_id`) 
                        VALUES ('$usergame->user_id','$usergame->user_type_id','$game->game_id','$category->category_id','$category->property_id')";
                    logToFile($teamsql);
                    if(mysql_query($teamsql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = mysql_error();
//                        return $ret;
                    }
                }
            }
            
            if($usergame->type_id != null && $usergame->type_id != ""  && $usergame->type_id != "0"){
                $ids= str_split($usergame->type_id );
                foreach ($ids as $id){
                    $sqlCat = "SELECT * FROM nk_game_categories_properties WHERE property_name = (SELECT type_title FROM nk_game_type WHERE game_id = ".$usergame->game_id." AND type_id = $id)";
                    $handle = mysql_query($sqlCat);
                    $category = mysql_fetch_object($handle);
                    if($category->category_id == null || $category->category_id == "" || $category->property_id == null || $category->property_id == "")
                        continue;
                    $typesql = "INSERT INTO nk_user_game_property (`user_id`,`user_type_id`,`game_id`,`category_id`,`property_id`) 
                        VALUES ('$usergame->user_id','$usergame->user_type_id','$game->game_id','$category->category_id','$category->property_id')";
                    logToFile($typesql);
                    if(mysql_query($typesql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = mysql_error();
//                        return $ret;
                    }
                }
            }
            
            if($usergame->champion_id != null && $usergame->champion_id != ""  && $usergame->champion_id != "0"){
                $ids= str_split($usergame->champion_id );
                foreach ($ids as $id){
                    $sqlCat = "SELECT * FROM nk_game_categories_properties WHERE property_name = (SELECT champion_title FROM nk_game_champion WHERE game_id = ".$usergame->game_id." AND champion_id = $id)";
                    $handle = mysql_query($sqlCat);
                    $category = mysql_fetch_object($handle);
                    if($category->category_id == null || $category->category_id == "" || $category->property_id == null || $category->property_id == "")
                        continue;
                    $championsql = "INSERT INTO nk_user_game_property (`user_id`,`user_type_id`,`game_id`,`category_id`,`property_id`) 
                        VALUES ('$usergame->user_id','$usergame->user_type_id','$game->game_id','$category->category_id','$category->property_id')";
                    logToFile($championsql);
                    if(mysql_query($championsql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = mysql_error();
//                        return $ret;
                    }
                }
            }
            
            if($usergame->map_id != null && $usergame->map_id != ""  && $usergame->map_id != "0"){
                $ids= str_split($usergame->map_id );
                foreach ($ids as $id){
                    $sqlCat = "SELECT * FROM nk_game_categories_properties WHERE property_name = (SELECT map_title FROM nk_game_map WHERE game_id = ".$usergame->game_id." AND map_id = $id)";
                    $handle = mysql_query($sqlCat);
                    $category = mysql_fetch_object($handle);
                    if($category->category_id == null || $category->category_id == "" || $category->property_id == null || $category->property_id == "")
                        continue;
                    $mapsql = "INSERT INTO nk_user_game_property (`user_id`,`user_type_id`,`game_id`,`category_id`,`property_id`) 
                        VALUES ('$usergame->user_id','$usergame->user_type_id','$game->game_id','$category->category_id','$category->property_id')";
                    logToFile($mapsql);
                    if(mysql_query($mapsql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = mysql_error();
//                        return $ret;
                    }
                }
            }
            
            if($usergame->role_id != null && $usergame->role_id != ""  && $usergame->role_id != "0"){
                $ids= str_split($usergame->role_id );
                foreach ($ids as $id){
                    $sqlCat = "SELECT * FROM nk_game_categories_properties WHERE property_name = (SELECT role_title FROM nk_game_role WHERE game_id = ".$usergame->game_id." AND role_id = $id)";
                    $handle = mysql_query($sqlCat);
                    $category = mysql_fetch_object($handle);
                    if($category->category_id == null || $category->category_id == "" || $category->property_id == null || $category->property_id == "")
                        continue;
                    $rolesql = "INSERT INTO nk_user_game_property (`user_id`,`user_type_id`,`game_id`,`category_id`,`property_id`) 
                        VALUES ('$usergame->user_id','$usergame->user_type_id','$game->game_id','$category->category_id','$category->property_id')";
                    logToFile($rolesql);
                    if(mysql_query($rolesql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = mysql_error();
//                        return $ret;
                    }
                }
            }
            
            if($usergame->hero_id != null && $usergame->hero_id != ""  && $usergame->hero_id != "0"){
                $ids= str_split($usergame->hero_id );
                foreach ($ids as $id){
                    $sqlCat = "SELECT * FROM nk_game_categories_properties WHERE property_name = (SELECT hero_title FROM nk_game_hero WHERE game_id = ".$usergame->game_id." AND hero_id = $id)";
                    $handle = mysql_query($sqlCat);
                    $category = mysql_fetch_object($handle);
                    if($category->category_id == null || $category->category_id == "" || $category->property_id == null || $category->property_id == "")
                        continue;
                    $herosql = "INSERT INTO nk_user_game_property (`user_id`,`user_type_id`,`game_id`,`category_id`,`property_id`) 
                        VALUES ('$usergame->user_id','$usergame->user_type_id','$game->game_id','$category->category_id','$category->property_id')";
                    logToFile($herosql);
                    if(mysql_query($herosql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = mysql_error();
//                        return $ret;
                    }
                }
            }
            
        }
        logToFile(" ************************************* ");
        $ret['IsSuccess'] = true;
        return $ret;
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}


function getGameCategoriesProps($category_id){
    $ret = array();
    try{
        $db = new DBConnection();
        $db->getConnection();
        $sql = "SELECT * FROM nk_game_categories_properties WHERE category_id = $category_id";
        $handle = mysql_query($sql);
        while ($property = mysql_fetch_object($handle)) {
            $ret['properties'][] = $property;
        }
        $ret['IsSuccess'] = true;
        return $ret;
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}

function getGameCategories($game_id){
    $ret = array();
    try{
        $db = new DBConnection();
        $db->getConnection();
        $sql = "SELECT * FROM nk_game_categories WHERE game_id = $game_id";
        $handle = mysql_query($sql);
        while ($category = mysql_fetch_object($handle)) {
            $value = array();
            $props = getGameCategoriesProps($category->category_id);
            $value['category'] = $category;
            $value['properties'] = $props['properties'];
            
            $ret['categories'][]=$value;
        }
        $ret['IsSuccess'] = true;
        return $ret;
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}
function getAllGames(){
    $ret = array();
    try{
        $db = new DBConnection();
        $db->getConnection();
        $sql = "SELECT * FROM nk_game";
        $handle = mysql_query($sql);
        while ($game = mysql_fetch_object($handle)) {
            $value = array();
            $cats = getGameCategories($game->game_id);
            $value['game'] = $game;
            $value['categories'] = $cats['categories'];

            $ret['games'][]=$value;
        }
        $ret['IsSuccess'] = true;
        return $ret;
    }catch(Exception $e){
        $ret['IsSuccess'] = false;
        $ret['Msg'] = $e->getMessage();
    }
    return $ret;
}

if(isset($_GET['method']) || isset($_POST['method'])){
 //   header('Content-type:text/javascript;charset=UTF-8');

    $method = $_REQUEST["method"];

    switch ($method) {
        case "addGame":
            $ret = addGame($_REQUEST['game_name'],$_REQUEST['game_description']);
            break;
		case "changeGameName":
            $ret = changeGameName($_REQUEST['game_id'],$_REQUEST['game_name']);
            break;
        case "addProp":
            $ret = addProp($_REQUEST['game_id'],$_REQUEST['category_id'],$_REQUEST['property_name']);
            break;
        case "deleteProp":
            $ret = deleteProp($_REQUEST['game_id'], $_REQUEST['category_id'], $_REQUEST['property_id']);
            break;
        case "addCategory":
            $ret = addCategory($_REQUEST['game_id'],$_REQUEST['category_name']);
            break;
        case "deleteCategory":
            $ret = deleteCategory($_REQUEST['game_id'],$_REQUEST['category_id']);
            break;
        case "deleteGame":
            $ret = deleteGame($_REQUEST['game_id']);
            break;
        case "getAllGames":
            $ret = getAllGames();
            break;
        case "getGameCategories":
            $ret = getGameCategories($_REQUEST['game_id']);
            break;
        case "getGameCategoriesProps":
            $ret = getGameCategoriesProps($_REQUEST['category_id']);
            break;
        case "migrate":
            $ret = migrate();
            break;
        case "changeGameName":
            $ret = changeGameName($_REQUEST['game_id'],$_REQUEST['game_name']);
            break;
        case "changeCategoryName":
            $ret = changeCategoryName($_REQUEST['category_id'],$_REQUEST['category_name']);
            break;
        case "changePropertyName":
            $ret = changePropertyName($_REQUEST['property_id'],$_REQUEST['property_name']);
            break;	
			
        }
    echo json_encode($ret);
}
?>
