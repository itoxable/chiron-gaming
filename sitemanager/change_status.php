<?php
include('general_include.php');
include('/class/json.php');

$op="";
$lesson_status_id=$_REQUEST['lesson_status_id'];
$lesson_id=$_REQUEST['lesson_id'];	

$Select_Status_Title="SELECT st.lesson_status_title FROM ".TABLEPREFIX."_lesson_status AS st,".TABLEPREFIX."_lesson AS ls WHERE ls.lesson_status_id=st.lesson_status_id AND ls.lesson_id=".$lesson_id." LIMIT 0,1";
$Status=$AdminManagerObjAjax->GetRecords("Row",$Select_Status_Title);


if($Status['lesson_status_title']=='Open' && $Status['lesson_status_title']!='Closed')
{
	$Up_Sql="UPDATE ".TABLEPREFIX."_lesson SET lesson_status_id='".$lesson_status_id."' WHERE lesson_id=".$lesson_id;
	$Up=$AdminManagerObjAjax->Execute($Up_Sql);

	$Sql_Options="SELECT * FROM ".TABLEPREFIX."_lesson_status WHERE lesson_status_title NOT IN ('Open') AND is_active='Y'";
	$Options=$AdminManagerObjAjax->GetRecords("All",$Sql_Options);
	
	for($i=0;$i<count($Options);$i++){
		 $op.='<option value="'.$Options[$i]['lesson_status_id'].'"';
		 if($Options[$i]['lesson_status_id']==$lesson_status_id){
		 	$op.=' selected="selected"';
		 }
		 $op.='>'.$Options[$i]['lesson_status_title'].'</option>';
	}
	
	$msg =  '';	

}
else if($Status['lesson_status_title']!='Open' && $Status['lesson_status_title']!='Closed')
{
	$Up_Sql="UPDATE ".TABLEPREFIX."_lesson SET lesson_status_id='".$lesson_status_id."' WHERE lesson_id=".$lesson_id;
	$Up=$AdminManagerObjAjax->Execute($Up_Sql);

	$msg =  "Status Changed";
	$op = '';
}
else if($Status['lesson_status_title']=='Closed'){

	$Up_Sql="UPDATE ".TABLEPREFIX."_lesson SET lesson_status_id='".$lesson_status_id."' WHERE lesson_id=".$lesson_id;
	$Up=$AdminManagerObjAjax->Execute($Up_Sql);

	$op.='<option>Closed</option>';
	$msg =  '';	
}
echo json_encode(

				array(
					'lesson_id' => $lesson_id,
					'msg' => $msg,
					'op' => $op
				)
			);			
			?>
