<?php
/*******************************************************************************/
			#This page is to add/edit race details
			#last Updated : August 25 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$u_id =$_REQUEST['u_id'];
$user_game_id =$_REQUEST['user_game_id'];
$action=$_REQUEST['action'];
if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	/* Holding Data If Error Starts */

    $FabricArr['game_id']	     = $game_id;
	
	$FabricArr['comment']        = htmlspecialchars(trim($comment));
	$FabricArr['is_active']		 = $is_active;
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
	
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_user_game","game_id",$game_id,"Game","user_game_id",$user_game_id,"user_id",$u_id);
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{
		
			
		if(empty($user_game_id))
		{
			
			$table_name = TABLEPREFIX."_user_game ";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'user_id'                           => $u_id,
									'user_type_id'                      => 2,
									'comment'                           => $comment,                          
									'is_active'  						=> $is_active,
									'date_added' 						=> date("Y-m-d H:i:s")
									);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			$user_game_id=mysql_insert_id();
			
			/*print_r($fields_values);	
			exit();*/

			/* Insert Into Events Ends */
			
		}
		else if(!empty($user_game_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_user_game";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'user_id'                           => $u_id,
									'user_type_id'                      => 2,
									'comment'                           => $comment,                          
									'is_active'  						=> $is_active,
									'date_edited' 						=> date("Y-m-d H:i:s")
									);

			$where="user_game_id='$user_game_id'";										
			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);
			
			/*print_r($fields_values);	
			
			exit();*/

			/* Update Events Ends */	
		}		
		echo "<script>window.location.href='student_game_manager.php?messg=".$msgreport."&u_id=".$u_id."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}
$submited_ladder=array();
$submited_race=array();
$submited_rating=array();
$submited_region=array();
$submited_server=array();
if(!empty($user_game_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_user_game WHERE user_game_id='".$user_game_id."' AND user_id='".$u_id."'";
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	$FabricArr['game_id']                   = $RsCatSql["game_id"];
	$FabricArr['ladder_id']                 = $RsCatSql["ladder_id"];
	$FabricArr['experience'] 				    = show_to_control($RsCatSql["experience"]);
	$FabricArr['date_edited'] 				= $RsCatSql["date_edited"];
	$FabricArr['date_added'] 				= $RsCatSql["date_added"];
	$FabricArr['is_active']					= $RsCatSql["is_active"];
	
	
	$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game WHERE game_id=".$FabricArr['game_id'];
	$RsgameSql = $AdminManagerObjAjax->GetRecords("Row",$SelectgameSql);
	
	$FabricArr['is_ladder']					= $RsgameSql["is_ladder"];
	$FabricArr['is_race']					= $RsgameSql["is_race"];
	$FabricArr['is_server']					= $RsgameSql["is_server"];
	$FabricArr['is_region']					= $RsgameSql["is_region"];
	$FabricArr['is_rating']					= $RsgameSql["is_rating"];

	
	/* Get Record For Display Ends */
	
	
	$SubmitButton="Update";	
	$flag=1;
}
else
{
	$SubmitButton=" Add ";
}
			
		$gameSql = "SELECT game_id,game_name FROM ".TABLEPREFIX."_game where is_active='Y' ORDER BY game_name";
		$GameArr = $AdminManagerObjAjax->HtmlOptionArrayCreate($gameSql);
			
	
	
	
//print_r($submited_race);
//print_r($submited_ladder);
//print_r($submited_server);	

/* Assign Smarty Variables Starts */

$smarty->assign('u_id',$u_id);
$smarty->assign('GameArr',$GameArr);
$smarty->assign('user_game_id',$user_game_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);
$smarty->register_modifier("inarray","in_array");

/* Assign Smarty Variables Ends */

$smarty->display("student_game_update.tpl");
?>