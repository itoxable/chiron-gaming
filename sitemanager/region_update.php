<?php
/*******************************************************************************/
			#This page is to add/edit Region details
			#last Updated : August 25 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$game_id =$_REQUEST['game_id'];
$region_id =$_REQUEST['region_id'];
$region_title=$_REQUEST['region_title'];
$action=$_REQUEST['action'];
if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	/* Holding Data If Error Starts */

	$FabricArr['region_title']		 = htmlspecialchars(trim($region_title));	
	$FabricArr['is_active']			 = $is_active;
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
	
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_game_region","region_title",$region_title,"Region name","region_id",$region_id,"game_id",$game_id);
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{
			
		if(empty($region_id))
		{
		 	//$Region_order = $AdminManagerObjAjax->GetRecords("One","select ifnull(max(Region_order)+1,1) from ".TABLEPREFIX."_game_region WHERE game_id='".$game_id."'");
			
			$table_name = TABLEPREFIX."_game_region ";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'region_title'					    => $region_title,
									'is_active'  						=> $is_active,
									'date_added' 						=> date("Y-m-d H:i:s")
									);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			$region_id=mysql_insert_id();	

			/* Insert Into Events Ends */
			
		}
		else if(!empty($region_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_game_region";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'region_title'					    => $region_title,								
									'is_active'  						=> $is_active,
									'date_edited' 						=> date("Y-m-d H:i:s")
									);

			$where="region_id='$region_id'";										
			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */	
		}		
		echo "<script>window.location.href='region_manager.php?messg=".$msgreport."&game_id=".$game_id."&IsPreserved=Y'</script>";
		exit; 
	
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($region_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_game_region WHERE region_id='".$region_id."' AND game_id='".$game_id."'";
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	$FabricArr['region_title'] 				= show_to_control($RsCatSql["region_title"]);
	$FabricArr['date_edited'] 				= $RsCatSql["date_edited"];
	$FabricArr['date_added'] 				= $RsCatSql["date_added"];
	$FabricArr['is_active']					= $RsCatSql["is_active"];

	/* Get Record For Display Ends */

	$SubmitButton="Update Region";	
	$flag=1;
}
else
{
	$SubmitButton=" Add Region ";
}


/* Assign Smarty Variables Starts */

$smarty->assign('game_id',$game_id);
$smarty->assign('region_id',$region_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("region_update.tpl");
?>