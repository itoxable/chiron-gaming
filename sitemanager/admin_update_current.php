<?php
/*******************************************************************************/
	#This page works behind an Ajax call to update admin's detail
	#last Updated : June 29 , 2010
/*******************************************************************************/

	include('general_include.php');
		
	$sub_admin_email			= $_POST['sub_admin_email'];
	
   $admin_username				= $_POST['admin_username'];
	$admin_password				= $_POST['admin_password'];	
	
	$sub_admin_email = $sub_admin_email=='Enter Admin Email*'?'':$sub_admin_email;
	$admin_username = $admin_username=='Enter Admin Username*'?'':$admin_username;
	$admin_password = $admin_password=='Enter Admin Password*'?'':$admin_password;
	
	/* Error Checking Starts */
	$err_msgs="";
	$err_msgs .=$AdminManagerObjAjax->Validate($sub_admin_email,"Email Address","EMP",$type="EMAIL");
	if(!empty($sub_admin_email))
		$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_admin","admin_email",$sub_admin_email,"Email Address","admin_id",$_SESSION['SesGTAdminID']);
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_admin","admin_username",$admin_username,"Username","admin_id",$_SESSION['SesGTAdminID']);		
	$err_msgs .=$AdminManagerObjAjax->Validate($admin_password,"Password ","EMP",$type="");		
	/* Error Checking Ends */	
			
	if(!empty($err_msgs))
	{			
		echo "2-$err_msgs";
		
	}
	else
	{
		$table_name = TABLEPREFIX.'_admin';
		$fields_values=array(
							'admin_username'			=> $admin_username,
							'admin_password'			=> $admin_password,
							'admin_email'				=> $sub_admin_email,
							'date_edited'				=> date('Y-m-d')
							);		
		$where='admin_id='.mysql_quote($_SESSION['SesGTAdminID']);										
		$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);					
		
		echo "2-Admin details updated successfully.";
	}
		
?>