<?php
/*******************************************************************************/
	#This page lists all the studio we have in table
	#last Updated : April 13 , 2010
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$page_name="home_image_manager.php";

$action=$_REQUEST['action'];

$IsPreserved=$_REQUEST['IsPreserved'];

if($IsPreserved=="Y")
{
/* preserve select starts */
$PreserveVariableArr=$preserve_variable_object->PreserveVariableSelect();
foreach($PreserveVariableArr as $a=>$b){$$a=$b;}
/* preserve select ends */
}

/* Activate Operation Starts */
if($action=="activate")
{
	$ChangeStatusObjAjax->ChangeStatus($record_id,12);	
}
/* Activate Operation Ends */

/* Ranking Setting Starts */
//$RankingObjAjax->SetEventDetails($page_name,6);	
/* Ranking Setting Ends */
		

/* Delete Operation Starts */

if($action=="del")
{	
	if(!empty($delete_id))
	{	
		/*   Set order After delete Starts  */
		//$RankingObjAjax->RankingDelete($delete_id);			
		/*   Set order After delete Ends  */
		
		
		
		/* Delete Image Starts */
		$SqlDelete="DELETE FROM ".TABLEPREFIX."_home_image WHERE image_id='$delete_id'";
		$ResultDelete=$adodbcon->Execute($SqlDelete);		
		/* Delete Image Ends */		
		
		
		if($adodbcon->ErrorNo()) 
			$messg = 5;
		else 
			$messg = 6;
	}	
	else
	{
		$messg = 7;
	}		
}

/* Delete Operation Ends */

/* Ranking Operation Starts */

/*$RankingObjAjax->SetRankingMaxOrderNo("select ifnull(max(banner_order),1) from ".TABLEPREFIX."_image");
if($action=="ranking")
	$RankingObjAjax->Ranking($to,$id);		*/
		
/* Ranking Operation Ends */

/* IsProcess Starts */

$action_arr=array("list_order","ranking","activate","list_paginate","list_search","del");

if(in_array($action,$action_arr))
{
	$smarty->assign('IsProcess',"Y");
}

/* IsProcess Ends */

	/* listing Operation Starts */

	/* search section starts */

	if($dosearch=="GO")
	{ 
		$Search_image_title = $Search_image_title=='Search by Image Title'?'':$Search_image_title;
		
		if(!empty($Search_image_title))
		{
			 $searchSql =  $searchSql." AND image_alt like '%".mysql_quote($Search_image_title,"N")."%'"; 
		}
		
		if(!empty($Search_date_from))
		{
			$searchSql =  $searchSql." AND date_added >= '".$Search_date_from."'";
		}
		if(!empty($Search_date_to))
		{
			$searchSql =  $searchSql." AND date_added <= '".$Search_date_to."'";
		}
		$SearchLink="dosearch=GO&Search_image_title=".addslashes(show_to_control($Search_image_title))."&Search_date_from=$Search_date_from&Search_date_to=$Search_date_to";
	}	

	/* search section ends */

	/* Order section starts */	

	$SortingSequenceArr=array(1 => "image_alt");	
	$ReturnSortingArr=$SortingObjAjax->Sorting("date_added",$SortingSequenceArr,$do_order,$OrderByID,$OrderType);	
	$OrderBySql=$ReturnSortingArr['OrderBySql'];
	$OrderLink=$ReturnSortingArr['OrderLink'];
	$OrderType=$ReturnSortingArr['OrderType'];			

	/* Order section ends */

	echo $SqlSelectCat="SELECT *	FROM ".TABLEPREFIX."_home_image Where 1=1 ".$searchSql.$OrderBySql;
	/* pagination starts */	

	$pagination_arr=$PaginationObjAjax->PaginationAjax($SqlSelectCat,$page_name."?action=".$action."&".$SearchLink."&".$OrderLink,"ManagerGeneral");	

	/* pagination ends */ 

	/* preserve update starts */

	$PreserveLink="from=$from&".$SearchLink."&".$OrderLink;

	$preserve_variable_object->PreserveVariableUpdate($PreserveLink);

	/* preserve update ends */		


	#Fetch all sponsor and store them in an array
	$SelectCmsArr=$adodbcon->GetAll($pagination_arr[0]);	

	$NumSelectCms=count($SelectCmsArr);

	for($i=0;$i<$NumSelectCms;$i++)
	{	
		$SelectCmsArr[$i]['image_alt_delete'] 		= addslashes(show_to_control($SelectCmsArr[$i]['image_alt']));
		$SelectCmsArr[$i]['image_file'] 			= $SelectCmsArr[$i]['image_file'];
		$SelectCmsArr[$i]['active_img'] 			= $SelectCmsArr[$i]['is_active']=="Y" ? "true.gif" : "false.gif";
		$SelectCmsArr[$i]['active_alt'] 			= $SelectCmsArr[$i]['is_active']=="Y" ? "Active" : "Inactive";
		$SelectCmsArr[$i]['date_added']				= date_format_admin($SelectCmsArr[$i]['date_added']);		/*$date_added= explode('-',$SelectCmsArr[$i]['date_added']);
		$SelectCmsArr[$i]['date_added']				= date('m/d/y',mktime(0, 0, 0, $date_added[1],$date_added[2],$date_added[0]));*/
		//$banner_order 								= $SelectCmsArr[$i]['banner_order'];
		
		/*$SelectCmsArr[$i]['img_rank']=(!empty($dosearch)||!empty($do_order))?"--":$RankingObjAjax->RankingRowCreate($banner_order,$SelectCmsArr[$i]['image_id'],0);*/
	}
	//print_r($SelectCmsArr);

/* listing Operation Ends */

$MessgReportText=displayMessage($messg);



/* Assign Smarty Variables Starts */

$smarty->assign("MessgReportText",$MessgReportText);
$smarty->assign('page_name',$page_name);
$smarty->assign('from',$from);
$smarty->assign('PreserveLink',$PreserveLink);
$smarty->assign('SearchLink',$SearchLink);
$smarty->assign('dosearch',$dosearch);
$smarty->assign("Search_image_title",$Search_image_title);
$smarty->assign('OrderLink',$OrderLink);
$smarty->assign('do_order',$do_order);
$smarty->assign('OrderType',$OrderType);
$smarty->assign('OrderByID',$OrderByID);
$smarty->assign('ReturnSortingArr',$ReturnSortingArr);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('NumSelectCms',$NumSelectCms);
$smarty->assign('SelectCmsArr',$SelectCmsArr);

/* Assign Smarty Variables Ends */

$smarty->display("home_image_manager.tpl");
?>