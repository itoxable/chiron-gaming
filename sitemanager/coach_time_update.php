<?php
/*******************************************************************************/
			#This page is to add/edit race details
			#last Updated : August 25 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$user_id =$_REQUEST['user_id'];
$timing_id =$_REQUEST['timing_id'];
$action=$_REQUEST['action'];
if($action=="trans")
{
	
	/* Holding Data If Error Starts */

    $FabricArr['time_slot_id']	     = $time_slot_id;
	$FabricArr['time_from']          = htmlspecialchars(trim($time_from));
	$FabricArr['time_to']            = htmlspecialchars(trim($time_to));
	
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
	$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($time_slot_id),"Time slot","EMP",$type="");
	$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($time_from),"Time from","EMP",$type="");
	$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($time_to),"Time to","EMP",$type="");
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{
			
		if(empty($timing_id))
		{
			
			$table_name = TABLEPREFIX."_timings ";

			$fields_values = array( 
									'time_slot_id'						=> $time_slot_id,
									'user_id'                           => $user_id,
									'time_from'					        => $time_from,
									'time_to'                           => $time_to                          
								 );		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			$timing_id=mysql_insert_id();	

			/* Insert Into Events Ends */
			
		}
		else if(!empty($timing_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_timings";

			$fields_values = array( 
									'time_slot_id'						=> $time_slot_id,
									'user_id'                           => $user_id,
									'time_from'					        => $time_from,
									'time_to'                           => $time_to           
									);

			$where="timing_id='$timing_id'";										
			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */	
		}		
		echo "<script>window.location.href='coach_time_manager.php?messg=".$msgreport."&user_id=".$user_id."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($timing_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_timings WHERE timing_id='".$timing_id."' AND user_id='".$user_id."'";
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	$FabricArr['time_slot_id']                   = $RsCatSql["time_slot_id"];
	$FabricArr['time_from'] 				     = show_to_control($RsCatSql["time_from"]);
	$FabricArr['time_to'] 				         = show_to_control($RsCatSql["time_to"]);
	/* Get Record For Display Ends */

	$SubmitButton="Update Timing";	
	$flag=1;
}
else
{
	$SubmitButton=" Add Timing";
}
$timeSql = "SELECT time_slot_id,time_slot FROM ".TABLEPREFIX."_time_slot ORDER BY time_slot_id";
$TimeArr = $AdminManagerObjAjax->HtmlOptionArrayCreate($timeSql);


/* Assign Smarty Variables Starts */

$smarty->assign('user_id',$user_id);
$smarty->assign('TimeArr',$TimeArr);
$smarty->assign('timing_id',$timing_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("coach_time_update.tpl");
?>