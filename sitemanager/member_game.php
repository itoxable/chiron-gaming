<?php
/*******************************************************************************/
			#This page is to add/edit race details
			#last Updated : August 25 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$u_id =$_REQUEST['u_id'];
if($_REQUEST['u_type']=='') {
	$typeSql = "SELECT user_type_id FROM ".TABLEPREFIX."_user_type_user_relation WHERE user_id='".$u_id."' AND user_type_id!=2";
	$record = $AdminManagerObjAjax->GetRecords("Row",$typeSql);
	$user_type = $record['user_type_id'];
}
else
    $user_type = $_REQUEST['u_type'];
$user_game_id =$_REQUEST['user_game_id'];
$action=$_REQUEST['action'];
if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	/* Holding Data If Error Starts */
    $FabricArr['paypal_email_id'] = $paypal_email_id;
    $FabricArr['game_id']	      = $game_id;
	//$FabricArr['ladder_id']      = $submited_ladder;
	$FabricArr['ladder_id']      = $ladder_id;
	$FabricArr['race_id']        = $submited_race;
	$FabricArr['server_id']      = $submited_server;
	$FabricArr['rating_id']      = $submited_rating;
	$FabricArr['region_id']      = $submited_region;
	$FabricArr['rate']			 = $rate;
	$FabricArr['description'] = htmlspecialchars(trim($description));
	$FabricArr['experience']     = htmlspecialchars(trim($experience));
	$FabricArr['lesson_plan']    = htmlspecialchars(trim($lesson_plan));
	$FabricArr['need_improvement'] = htmlspecialchars(trim($need_improvement));
	$FabricArr['is_active']		 = $is_active;
	$FabricArr['peak_hours']     = $peak_hours;
	$FabricArr['availability_type1']	= htmlspecialchars(trim($availability_type1));
	$FabricArr['availability_type2']	= htmlspecialchars(trim($availability_type2));
	$FabricArr['availability_city']		= trim($availability_city);
	$FabricArr['acity_name']            = htmlspecialchars(trim($acity_name));
	//$FabricArr['availability_country']	= htmlspecialchars(trim($availability_country));
	$user_type = $_POST['user_type'];
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
	if($user_type == 1)
	    $err_msgs .= $AdminManagerObjAjax->Validate(strip_tags($paypal_email_id),"Payapl Email","EMP",$type="EMAIL");
	
	//$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_user_game","game_id",$game_id,"Game","user_game_id",$user_game_id,"user_id",$u_id);
	$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($game_id),"Game","EMP",$type="");
	/*$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($race_id),"Race","EMP",$type="");
	$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($server_id),"Server","EMP",$type="");*/
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{
		if(count($submited_ladder)>0)  $ladder_id=implode(",",$submited_ladder);
		if(count($submited_race)>0)  $race_id=implode(",",$submited_race);
		if(count($submited_server)>0)  $server_id=implode(",",$submited_server);
		if(count($submited_region)>0)  $region_id=implode(",",$submited_region);
		if(count($submited_rating)>0)  $rating_id=implode(",",$submited_rating);
			
		if(empty($user_game_id))
		{
			if($availability_type1!='' && $availability_type2!='')
			  $availability_type = $availability_type1.','.$availability_type2;
			if($availability_type1!='' && $availability_type2=='')   
			  $availability_type = $availability_type1;
			if($availability_type2!='' && $availability_type1=='')   
			  $availability_type = $availability_type2;  
			
			  if($availability_city!= '' && $availability_type1!='') 
				 {
				   $sqlCity = "SELECT * FROM ".TABLEPREFIX."_cities WHERE city_id='$availability_city'";
				   $rowCity = $AdminManagerObjAjax->GetRecords("Row",$sqlCity);
				   $availability_country = $rowCity['country_id'];
				 }  
			  $updateSQl = "UPDATE ".TABLEPREFIX."_user set availability_type='$availability_type',availability_city='$availability_city',
			  availability_country='$availability_country',paypal_email_id='$paypal_email_id',description='$description' WHERE user_id='".$u_id."'";
              $AdminManagerObjAjax->Execute($updateSQl);

			$table_name = TABLEPREFIX."_user_game ";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'ladder_id'                         => $ladder_id,
									'race_id'                           => $race_id,
									'server_id'                         => $server_id,
									'region_id'                         => $region_id,
									'rating_id'                         => $rating_id,
									'rate'                           	=> $rate,	
									'user_id'                           => $u_id,
									'user_type_id'                      => $user_type,
									'experience'					    => $experience,
									'need_improvement'                  => $need_improvement, 
									'lesson_plan'                       => $lesson_plan,                         
									'is_active'  						=> 'Y',
									'date_added' 						=> date("Y-m-d H:i:s")
									);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			//echo mysql_error();
			/*print_r($fields_values);	
			exit();*/

			/* Insert Into Events Ends */
			
		}
				
		echo "<script>window.location.href='user_manager.php?messg=".$msgreport."'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}
$submited_ladder=array();
$submited_race=array();
$submited_rating=array();
$submited_region=array();
$submited_server=array();
if(!empty($user_game_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_user_game WHERE user_game_id='".$user_game_id."' AND user_id='".$u_id."'";
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	$FabricArr['game_id']                   = $RsCatSql["game_id"];
	$FabricArr['ladder_id']                 = $RsCatSql["ladder_id"];
	/*$FabricArr['server_id']                 = $RsCatSql["server_id"];
	$FabricArr['race_id']                   = $RsCatSql["race_id"];*/
	
	$FabricArr['experience'] 				= show_to_control($RsCatSql["experience"]);
	$FabricArr['comment'] 				    = show_to_control($RsCatSql["comment"]);
	$FabricArr['date_edited'] 				= $RsCatSql["date_edited"];
	$FabricArr['date_added'] 				= $RsCatSql["date_added"];
	$FabricArr['is_active']					= $RsCatSql["is_active"];
	$FabricArr['rate']						= $RsCatSql['rate']	;
	/*$FabricArr['race_id']					= $RsCatSql["race_id"];
	$FabricArr['server_id']					= $RsCatSql["server_id"];
	$FabricArr['region_id']					= $RsCatSql["region_id"];
	$FabricArr['rating_id']					= $RsCatSql["rating_id"];*/
	
	$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game WHERE game_id=".$FabricArr['game_id'];
	$RsgameSql = $AdminManagerObjAjax->GetRecords("Row",$SelectgameSql);
	
	$FabricArr['is_ladder']					= $RsgameSql["is_ladder"];
	$FabricArr['is_race']					= $RsgameSql["is_race"];
	$FabricArr['is_server']					= $RsgameSql["is_server"];
	$FabricArr['is_region']					= $RsgameSql["is_region"];
	$FabricArr['is_rating']					= $RsgameSql["is_rating"];

	/*$SelectladderSql="SELECT * FROM ".TABLEPREFIX."_game_ladder WHERE game_id=".$user_game_id." AND ladder_id =".$RsCatSql["ladder_id"]."";
	$RsladderSql = $AdminManagerObjAjax->GetRecords("All",$SelectladderSql);	
	

	for($i=0;$i<count($RsladderSql);$i++)
	{		
			$submited_ladder[] = $RsladderSql[$i]['ladder_id'];
	}
	*/
	$SelectRaceSql="SELECT * FROM ".TABLEPREFIX."_game_race WHERE  game_id=".$FabricArr['game_id']." AND race_id IN (".$RsCatSql["race_id"].")";
	$RsRaceSql = $AdminManagerObjAjax->GetRecords("All",$SelectRaceSql);	
	

	for($i=0;$i<count($RsRaceSql);$i++)
	{		
			$submited_race[] = $RsRaceSql[$i]['race_id'];
	}
	
	$SelectratingSql="SELECT * FROM ".TABLEPREFIX."_game_rating WHERE  game_id=".$FabricArr['game_id']." AND rating_id IN (".$RsCatSql["rating_id"].")";
	$RsratingSql = $AdminManagerObjAjax->GetRecords("All",$SelectratingSql);	
	

	for($i=0;$i<count($RsratingSql);$i++)
	{		
			$submited_rating[] = $RsratingSql[$i]['rating_id'];
	}
	
	$SelectregionSql="SELECT * FROM ".TABLEPREFIX."_game_region WHERE  game_id=".$FabricArr['game_id']." AND region_id IN (".$RsCatSql["region_id"].")";
	$RsregionSql = $AdminManagerObjAjax->GetRecords("All",$SelectregionSql);	
	

	for($i=0;$i<count($RsregionSql);$i++)
	{		
			$submited_region[] = $RsregionSql[$i]['region_id'];
	}
	
	$SelectserverSql="SELECT * FROM ".TABLEPREFIX."_game_server WHERE game_id=".$FabricArr['game_id']." AND server_id IN (".$RsCatSql["server_id"].")";
	$RsserverSql = $AdminManagerObjAjax->GetRecords("All",$SelectserverSql);	
	

	for($i=0;$i<count($RsserverSql);$i++)
	{		
			$submited_server[] = $RsserverSql[$i]['server_id'];
	}
	/* Get Record For Display Ends */
	
	
	$SubmitButton="Update Coach Game";	
	$flag=1;
}
else
{
	$SubmitButton=" Add Coach Game";
}
			
		$gameSql = "SELECT game_id,game_name FROM ".TABLEPREFIX."_game where is_active='Y' ORDER BY game_name";
		$GameArr = $AdminManagerObjAjax->HtmlOptionArrayCreate($gameSql);
			
	
		$LadderSql = "SELECT ladder_id,ladder_name  FROM ".TABLEPREFIX."_game_ladder  ORDER BY date_added DESC ";  
		 $LadderArr = $AdminManagerObjAjax->HtmlOptionArrayCreate($LadderSql);
		//$LadderArr = $AdminManagerObjAjax->GetRecords("All",$LadderSql);
	
	
	
	$RaceSql = "SELECT *  FROM ".TABLEPREFIX."_game_race  ORDER BY date_added DESC ";   
	$RaceArr = $AdminManagerObjAjax->GetRecords("All",$RaceSql);
	
	
	
	$RatingSql = "SELECT *  FROM ".TABLEPREFIX."_game_rating   ORDER BY date_added DESC ";   
	$RatingArr = $AdminManagerObjAjax->GetRecords("All",$RatingSql);
	
		$RegionSql = "SELECT *  FROM ".TABLEPREFIX."_game_region ORDER BY date_added DESC ";   
		$RegionArr = $AdminManagerObjAjax->GetRecords("All",$RegionSql);
	
		$ServerSql = "SELECT *  FROM ".TABLEPREFIX."_game_server ORDER BY date_added DESC ";   
		$ServerArr = $AdminManagerObjAjax->GetRecords("All",$ServerSql);
	
//print_r($submited_race);
//print_r($submited_ladder);
//print_r($submited_server);	

/* Assign Smarty Variables Starts */
$countrySql = "SELECT country_id,country_name FROM ".TABLEPREFIX."_country ORDER BY country_name";
$CountryArr = $AdminManagerObjAjax->HtmlOptionArrayCreate($countrySql);
if($_REQUEST['u_type']!='')
{
        $is_coach = 0;
	    $is_partner = 0;
	    $typeSql = "SELECT * FROM ".TABLEPREFIX."_user_type_user_relation WHERE user_id='".$u_id."'";
		$typeArr = $adodbcon->GetAll($typeSql);
		for($x=0;$x<count($typeArr);$x++)
		{
		    if($typeArr[$x]['user_type_id'] == 1)
		     $is_coach = 1;
		    if($typeArr[$x]['user_type_id'] == 3)
		     $is_partner = 1;
			 	  
		}

}


$smarty->assign('u_id',$u_id);
$smarty->assign('is_coach',$is_coach);
$smarty->assign('is_partner',$is_partner);
$smarty->assign('user_type',$user_type);
$smarty->assign('GameArr',$GameArr);
$smarty->assign('CountryArr',$CountryArr);
$smarty->assign('user_game_id',$user_game_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);
$smarty->assign('LadderArr',$LadderArr);
$smarty->assign('RaceArr',$RaceArr);
$smarty->assign('RatingArr',$RatingArr);
$smarty->assign('RegionArr',$RegionArr);
$smarty->assign('ServerArr',$ServerArr);
$smarty->assign('submited_race',$submited_race);
$smarty->assign('submited_ladder',$submited_ladder);
$smarty->assign('submited_rating',$submited_rating);
$smarty->assign('submited_region',$submited_region);
$smarty->assign('submited_server',$submited_server);
$smarty->register_modifier("inarray","in_array");

/* Assign Smarty Variables Ends */

$smarty->display("member_game.tpl");
?>