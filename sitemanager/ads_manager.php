<?php
/*******************************************************************************/
	#This page lists all the studio we have in table
	#last Updated : April 13 , 2010
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$page_name="ads_manager.php";

$action=$_REQUEST['action'];

$IsPreserved=$_REQUEST['IsPreserved'];

if($IsPreserved=="Y")
{
/* preserve select starts */
$PreserveVariableArr=$preserve_variable_object->PreserveVariableSelect();
foreach($PreserveVariableArr as $a=>$b){$$a=$b;}
/* preserve select ends */
}

/* Activate Operation Starts */
if($action=="activate")
{
	$ChangeStatusObjAjax->ChangeStatus($record_id,16);	
}
/* Activate Operation Ends */

/* Delete Operation Starts */

if($action=="del")
{	
	if(!empty($delete_id))
	{	
		/* Delete Image Starts */
		$SqlDelete="DELETE FROM ".TABLEPREFIX."_ads WHERE ads_id='$delete_id'";
		$ResultDelete=$adodbcon->Execute($SqlDelete);		
		/* Delete Image Ends */		
		
		
		if($adodbcon->ErrorNo()) 
			$messg = 5;
		else 
			$messg = 6;
	}	
	else
	{
		$messg = 7;
	}		
}

/* Delete Operation Ends */
/* IsProcess Starts */

$action_arr=array("list_order","ranking","activate","list_paginate","list_search","del");

if(in_array($action,$action_arr))
{
	$smarty->assign('IsProcess',"Y");
}

/* IsProcess Ends */

	/* listing Operation Starts */

	/* search section starts */

	if($dosearch=="GO")
	{ 
		$Search_position = $Search_position=='Search by Position'?'':$Search_position;
		//echo $Search_position;
		$Search_type = $Search_type=='Search by Type'?'':$Search_type;
		$Search_date_from = $Search_date_from=='Date From'?'':$Search_date_from." 00:00:00";
		//echo $Search_date_from;
		$Search_date_to = $Search_date_to=='Date To'?'':$Search_date_to." 23:59:59";
		
		if(!empty($Search_position))
		{
			 $searchSql =  $searchSql." AND ads_position=".$Search_position.""; 
		}
		
		if(!empty($Search_type))
		{
			 $searchSql =  $searchSql." AND ads_type=".$Search_type.""; 
		}

		if(!empty($Search_date_from))
		{
			$searchSql =  $searchSql." AND date_added >= '".$Search_date_from."'";
		}
		
		if(!empty($Search_date_to))
		{
			$searchSql =  $searchSql." AND date_added <= '".$Search_date_to."'";
		}
		$SearchLink="dosearch=GO&Search_position=".$Search_position."&Search_type=".$Search_type."&Search_date_from=$Search_date_from&Search_date_to=$Search_date_to";
		//echo $SearchLink;
	}	

	/* search section ends */

	/* Order section starts */	

	$SortingSequenceArr=array(1 => "image_alt");	
	$ReturnSortingArr=$SortingObjAjax->Sorting("date_added",$SortingSequenceArr,$do_order,$OrderByID,$OrderType);	
	$OrderBySql=$ReturnSortingArr['OrderBySql'];
	$OrderLink=$ReturnSortingArr['OrderLink'];
	$OrderType=$ReturnSortingArr['OrderType'];			

	/* Order section ends */

	$SqlSelectCat="SELECT *	FROM ".TABLEPREFIX."_ads Where 1=1 ".$searchSql.$OrderBySql;
	//echo $SqlSelectCat;
	/* pagination starts */	

	$pagination_arr=$PaginationObjAjax->PaginationAjax($SqlSelectCat,$page_name."?action=".$action."&".$SearchLink."&".$OrderLink,"ManagerGeneral");	

	/* pagination ends */ 

	/* preserve update starts */

	$PreserveLink="from=$from&".$SearchLink."&".$OrderLink;

	$preserve_variable_object->PreserveVariableUpdate($PreserveLink);

	/* preserve update ends */		


	#Fetch all sponsor and store them in an array
	$SelectCmsArr=$adodbcon->GetAll($pagination_arr[0]);	

	$NumSelectCms=count($SelectCmsArr);

	for($i=0;$i<$NumSelectCms;$i++)
	{	
		$SelectCmsArr[$i]['ads_id'] 				= $SelectCmsArr[$i]['ads_id'];
		$SelectCmsArr[$i]['ads_title'] 				= addslashes(show_to_control($SelectCmsArr[$i]['ads_title']));
		$SelectCmsArr[$i]['ads_type'] 				= $SelectCmsArr[$i]['ads_type'];
		$SelectCmsArr[$i]['ads_position'] 			= $SelectCmsArr[$i]['ads_position'];
		$SelectCmsArr[$i]['active_img'] 			= $SelectCmsArr[$i]['is_active']=="Y" ? "true.gif" : "false.gif";
		$SelectCmsArr[$i]['active_alt'] 			= $SelectCmsArr[$i]['is_active']=="Y" ? "Active" : "Inactive";
		$SelectCmsArr[$i]['date_added']				= date_format_admin($SelectCmsArr[$i]['date_added']);
		/*$date_added= explode('-',$SelectCmsArr[$i]['date_added']);
		$SelectCmsArr[$i]['date_added']				= date('m/d/y',mktime(0, 0, 0, $date_added[1],$date_added[2],$date_added[0]));*/
	}
	/*echo '<pre>';
	print_r($SelectCmsArr);*/

/* listing Operation Ends */

$MessgReportText=displayMessage($messg);



/* Assign Smarty Variables Starts */

$smarty->assign("MessgReportText",$MessgReportText);
$smarty->assign('page_name',$page_name);
$smarty->assign('from',$from);
$smarty->assign('PreserveLink',$PreserveLink);
$smarty->assign('SearchLink',$SearchLink);
$smarty->assign('dosearch',$dosearch);
$smarty->assign("Search_image_title",$Search_image_title);
$smarty->assign('OrderLink',$OrderLink);
$smarty->assign('do_order',$do_order);
$smarty->assign('OrderType',$OrderType);
$smarty->assign('OrderByID',$OrderByID);
$smarty->assign('ReturnSortingArr',$ReturnSortingArr);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('NumSelectCms',$NumSelectCms);
$smarty->assign('SelectCmsArr',$SelectCmsArr);

/* Assign Smarty Variables Ends */

$smarty->display("ads_manager.tpl");
?>