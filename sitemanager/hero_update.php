<?php
/*******************************************************************************/
			#This page is to add/edit hero details
			#last Updated : August 24 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$game_id =$_REQUEST['game_id'];
$hero_id =$_REQUEST['hero_id'];
$hero_title=$_REQUEST['hero_title'];
$action=$_REQUEST['action'];
if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	/* Holding Data If Error Starts */

	$FabricArr['hero_title']		 = htmlspecialchars(trim($hero_title));
	$FabricArr['is_active']			 = $is_active;
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
	
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_game_hero","hero_title",$hero_title,"Mode title","hero_id",$hero_id,"game_id",$game_id);
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{
			
		if(empty($hero_id))
		{
			
			$table_name = TABLEPREFIX."_game_hero ";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'hero_title'					    => $hero_title,
									'is_active'  						=> $is_active,
									'date_added' 						=> date("Y-m-d H:i:s")
									);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			$class_id=mysql_insert_id();	

			/* Insert Into Events Ends */
			
		}
		else if(!empty($hero_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_game_hero";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'hero_title'					    => $hero_title,
									'is_active'  						=> $is_active,
									'date_edited' 						=> date("Y-m-d H:i:s")
									);

			$where="hero_id='$hero_id'";										
			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */	
		}		
		echo "<script>window.location.href='hero_manager.php?messg=".$msgreport."&game_id=".$game_id."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($hero_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_game_hero WHERE hero_id='".$hero_id."' AND game_id='".$game_id."'";
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	$FabricArr['hero_title'] 				= show_to_control($RsCatSql["hero_title"]);
	$FabricArr['date_edited'] 				= $RsCatSql["date_edited"];
	$FabricArr['date_added'] 				= $RsCatSql["date_added"];
	$FabricArr['is_active']					= $RsCatSql["is_active"];

	/* Get Record For Display Ends */

	$SubmitButton="Update hero";	
	$flag=1;
}
else
{
	$SubmitButton=" Add hero ";
}


/* Assign Smarty Variables Starts */

$smarty->assign('game_id',$game_id);
$smarty->assign('hero_id',$hero_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("hero_update.tpl");
?>