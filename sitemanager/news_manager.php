<?php
/*******************************************************************************/
	#This page lists all the game we have in table
	#last Updated : Aug 23 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$page_name="news_manager.php";

$action=$_REQUEST['action'];

$IsPreserved=$_REQUEST['IsPreserved'];

if($IsPreserved=="Y")
{
/* preserve select starts */
$PreserveVariableArr=$preserve_variable_object->PreserveVariableSelect();
foreach($PreserveVariableArr as $a=>$b){$$a=$b;}
/* preserve select ends */
}

/* Activate Operation Starts */
if($action=="activate")
{
	$ChangeStatusObjAjax->ChangeStatus($record_id,31);	
}
/* Activate Operation Ends */
/* Default Operation Starts */
/*
if($action=="default")
{
	$ChangeStatusObjAjax->ChangeStatus($record_id,8);	
}
*/
/* Default Operation Ends *

/* Delete Operation Starts */

if($action=="del")
{	
	if(!empty($delete_id))
	{
		
		
		
		/* Delete Events Starts */
		$SqlDelete="DELETE FROM ".TABLEPREFIX."_news WHERE news_id='$delete_id'";
		$ResultDelete=$adodbcon->Execute($SqlDelete);		
		/* Delete Events Ends */

		if($adodbcon->ErrorNo()) 
			$messg = 5;
		else 
			$messg = 6;
	}	
	else
	{
		$messg = 7;
	}		
}

/* Delete Operation Ends */


/* IsProcess Starts */

$action_arr=array("list_order","ranking","activate","default","list_paginate","list_search","del");

if(in_array($action,$action_arr))
{
	$smarty->assign('IsProcess',"Y");
}

/* IsProcess Ends */

	/* listing Operation Starts */

	/* search section starts */

	if($dosearch=="GO")
	{ 
		$Search_news = $Search_news=='Search by news title'?'':$Search_news;
		if(!empty($Search_news))
		{
			 $searchSql =  $searchSql." AND news_title like '%".mysql_quote($Search_news,"N")."%'"; 
		}
		$SearchLink="dosearch=GO&Search_news=$Search_news";
		
	}	
	/* search section ends */

	/* Order section starts */	

	$SortingSequenceArr=array(1 => "news_title",2 => "date_added");	
	$ReturnSortingArr=$SortingObjAjax->Sorting("news_title",$SortingSequenceArr,$do_order,$OrderByID,$OrderType);	
	$OrderBySql=$ReturnSortingArr['OrderBySql'];
	$OrderLink=$ReturnSortingArr['OrderLink'];
	$OrderType=$ReturnSortingArr['OrderType'];			

	/* Order section ends */

	$SqlSelectCat="SELECT * FROM ".TABLEPREFIX."_news Where 1=1 ".$searchSql.$OrderBySql;
	/* pagination starts */	

	$pagination_arr=$PaginationObjAjax->PaginationAjax($SqlSelectCat,$page_name."?action=".$action."&".$SearchLink."&".$OrderLink,"ManagerGeneral");	

	/* pagination ends */ 

	/* preserve update starts */

	$PreserveLink="from=$from&".$SearchLink."&".$OrderLink;

	$preserve_variable_object->PreserveVariableUpdate($PreserveLink);

	/* preserve update ends */		


	#Fetch all events and store them in an array
	$SelectCmsArr=$adodbcon->GetAll($pagination_arr[0]);	

	$NumSelectCms=count($SelectCmsArr);

	for($i=0;$i<$NumSelectCms;$i++)
	{				
		$SelectCmsArr[$i]['news_title'] 			    = $SelectCmsArr[$i]['news_title'];
		$SelectCmsArr[$i]['news_title_delete'] 	    = addslashes(show_to_control($SelectCmsArr[$i]['news_title']));
		$SelectCmsArr[$i]['active_img'] 			= $SelectCmsArr[$i]['is_active']=="Y" ? "true.gif" : "false.gif";
		$SelectCmsArr[$i]['active_alt'] 			= $SelectCmsArr[$i]['is_active']=="Y" ? "Active" : "Inactive";
		$SelectCmsArr[$i]['default_img'] 			= $SelectCmsArr[$i]['is_default']=="Y" ? "true.gif" : "false.gif";
		$SelectCmsArr[$i]['default_alt'] 			= $SelectCmsArr[$i]['is_default']=="Y" ? "Default" : "Not-Default";
		$SelectCmsArr[$i]['date_added']				= date_format_admin($SelectCmsArr[$i]['date_added']);
		
	
	}

/* listing Operation Ends */

$MessgReportText=displayMessage($messg);

/* Assign Smarty Variables Starts */

$smarty->assign("brcrumb", $breadcrumbArr);
$smarty->assign("MessgReportText",$MessgReportText);
$smarty->assign('page_name',$page_name);
$smarty->assign('from',$from);
$smarty->assign('PreserveLink',$PreserveLink);
$smarty->assign('SearchLink',$SearchLink);
$smarty->assign('dosearch',$dosearch);
$smarty->assign("Search_news",$Search_news);
$smarty->assign('OrderLink',$OrderLink);
$smarty->assign('do_order',$do_order);
$smarty->assign('OrderType',$OrderType);
$smarty->assign('OrderByID',$OrderByID);
$smarty->assign('ReturnSortingArr',$ReturnSortingArr);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('NumSelectCms',$NumSelectCms);
$smarty->assign('SelectCmsArr',$SelectCmsArr);

/* Assign Smarty Variables Ends */

$smarty->display("news_manager.tpl");
?>