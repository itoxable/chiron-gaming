
/* Check numbersonly starts */
/* Note: Use In Control Attribute as onKeyPress="return numbersonly(this, event)" */
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;
	if (window.event)
	key = window.event.keyCode;
	else if (e)
	key = e.which;
	else
	return true;
	keychar = String.fromCharCode(key);	
	// control keys
	if ((key==null) || (key==0) || (key==8) || 
	(key==9) || (key==13) || (key==27))
	return true;
	// numbers
	else if ((("0123456789").indexOf(keychar) > -1))
	return true;
	// decimal point jump
	else if (dec && (keychar == "."))
	{
	myfield.form.elements[dec].focus();
	return false;
	}
	else
	return false;
}
/* Check numbersonly ends */
/* Check floatnumbersonly starts */
/* Note: Use In Control Attribute as onKeyPress="return floatnumbersonly(this, event)" */
function floatnumbersonly(myfield, e, dec)
{
	var key;
	var keychar;
	if (window.event)
	key = window.event.keyCode;
	else if (e)
	key = e.which;
	else
	return true;
	keychar = String.fromCharCode(key);
	// control keys
	if ((key==null) || (key==0) || (key==8) || 
	(key==9) || (key==13) || (key==27)|| (key==46))
	return true;
	// numbers
	else if ((("0123456789").indexOf(keychar) > -1))
	return true;
	// decimal point jump
	else if (dec && (keychar == "."))
	{
	myfield.form.elements[dec].focus();
	return false;
	}
	else
	return false;
}
/* Check floatnumbersonly ends */
/* Removes leading whitespaces Starts */
		function LTrim( value ) {
			
			var re = /\s*((\S+\s*)*)/;
			return value.replace(re, "$1");
			
		}
		// Removes ending whitespaces
		function RTrim( value ) {
			
			var re = /((\s*\S+)*)\s*/;
			return value.replace(re, "$1");
			
		}
		
		// Removes leading and ending whitespaces
		function trim( value ) {
			
			return LTrim(RTrim(value));
			
		}
/* Removes leading whitespaces Ends */

/* Check submitbyenter starts */
/* Note: Use In Control Attribute as onKeyPress="return submitbyenter('frmLogin',this, event)" */
function submitbyenter(formid,myfield, e, dec)
{
	var key;
	var keychar;
	if (window.event)
	key = window.event.keyCode;
	else if (e)
	key = e.which;
	else
	return true;		
	if (key==13)
	{		
		//getLogin('frmLogin','general_process.php','&action=LoginCustomer');	
		$(formid).submit();			
	}	
}
/* Check submitbyenter ends */

/*------------------General functions-----------------------*/
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

	function CheckEmpty(ErrField,myID,FieldName)
	{
		var myVar=document.getElementById(myID).value;
		if(myVar=="")
		{
			document.getElementById(ErrField).innerHTML = "Please enter <b>"+FieldName+"</b>";
		}else{
			document.getElementById(ErrField).innerHTML='';
		}
	}
/*------------------General functions-----------------------*/