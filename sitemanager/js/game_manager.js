(function(window) {
	var GameManagement ={
            wrapper: '',
            start:  function (parent) {
                this.wrapper = parent;
                jQuery(this.wrapper).empty();
                jQuery(parent).append('<a class="new-game" onclick="GameManagement.newGame()" href="javascript:;"><span>New Game</span></a>');
                jQuery.post("managerService.php",{"method":"getAllGames"}, function(data){
                    var html = [];
                    if(data.IsSuccess){
                       var size = data.games.length
                       for(var i = 0; i<size; i++){
                           var game = data.games[i].game;
                           html.push('<div class="game-table"><table><tr valign"top"><td><span title="Delete" class="delete-game" onclick="GameManagement.deleteGame('+game.game_id+')"></span><span class="game-name" id="game-name-'+game.game_id+'" onclick="GameManagement.showMenu(\'');
                           html.push(game.game_id);
                           html.push('\')" title="');
                           html.push(game.game_description);
                           html.push('">');
                           html.push(game.game_name);
                           html.push('</span>');
                           html.push('<div class="game-menu" id="game-menu-'+game.game_id+'">');
                           html.push('<a class="category-name" onclick="GameManagement.newCat(\'');
                           html.push(game.game_id);
                           html.push('\')" href="javascript:;"><span>New Category</span></a><br/>');
                           html.push('<a class="category-name" onclick="GameManagement.changeGameName(\'');
                           html.push(game.game_name);
                           html.push('\',\'');
                           html.push(game.game_id);
                           html.push('\')" href="javascript:;"><span>Change Name</span></a></div>');
                           html.push('</td></tr>');
                           
                           if(data.games[i].categories == null){
                               html.push('</table>');
                               continue;
                           }
                               
                           html.push('<tr><td/><td>');
                           var catsize = data.games[i].categories.length;
                           for(var j = 0; j < catsize; j++){
                                var cat = data.games[i].categories[j].category;
                                html.push('<table><tr valign"top"><td> - <span class="category-name" onclick="GameManagement.showCategoryMenu(\'');
                                html.push(cat.category_id)
                                html.push('\')">');
                                html.push(cat.category_name);
                                html.push('</span><span class="delete-item" title="Delete" onclick="GameManagement.deleteCategory('+game.game_id+','+cat.category_id+')">x</span><div class="category-menu" id="category-menu-'+cat.category_id+'">');
                                html.push('<a class="category-name" onclick="GameManagement.newProp(\'');
                                html.push(game.game_id);
                                html.push('\',\'');
                                html.push(cat.category_id);
                                html.push('\')" href="javascript:;"><span>New Prop</span></a><br/>');
                                html.push('<a class="category-name" onclick="GameManagement.changeCategoryName(\'');
                                html.push(cat.category_name);
                                html.push('\',\'');
                                html.push(cat.category_id);
                                html.push('\')" href="javascript:;"><span>Change Name</span></a></div>')
                                
                                html.push('</td>');
                               
                                if(data.games[i].categories[j].properties == null){
                                    html.push('</tr></table>');
                                    continue;
                                }
                                    
                                html.push('<tr><td/><td>');
                                var propsize = data.games[i].categories[j].properties.length;
                                for(var k = 0; k < propsize; k++){
                                    var prop = data.games[i].categories[j].properties[k];
                                    html.push('<div> - ');
                                    html.push('<span style="color:#000">');
                                    html.push(prop.property_name);
                                    html.push('</span><span class="delete-item" title="Delete" onclick="GameManagement.deleteProp('+game.game_id+','+cat.category_id+','+prop.property_id+')">x</span>');
                                    html.push('</div>');
                                }
                               
                                html.push('</td></tr></table>');
                           }
                           html.push('</td></tr></table></div>');
                           console.log(html.join(""));
                       }
                       
                    }
                    
//                    console.log(html.join(""));
                    jQuery(parent).append(html.join(""));
                }, "json");
            },
            showMenu: function (game_id) {
                jQuery(".game-menu").slideUp("fast",function(){jQuery("#game-menu-"+game_id).slideDown("fast");});
                
            },
            showCategoryMenu: function (category_id) {
                jQuery(".category-menu").slideUp("fast",function(){jQuery("#category-menu-"+category_id).slideDown("fast");});
                
            },
            showDetail: function (game_id) {
//                this.showModal(game_id);
//                jQuery.post("managerService.php",{"method":"getAllGames"}, function(data){
//                    var html = [];
//                    if(data.IsSuccess){
//                       var size = data.games.length
//                       for(var i = 0; i<size; i++){
//                           var game = data.games[i];
//                           html.push('<a class="game-name" onclick="GameManagement.showDetail(\'');
//                           html.push(game.game_id);
//                           html.push('\')" href="javascript:;" title="');
//                           html.push(game.game_description);
//                           html.push('">');
//                           html.push(game.game_name);
//                           html.push('</a>');
//                       }
//                       jQuery(parent).append(html.join(""));
//                    }
//                    else{
//                    }
//                }, "json");
            },
            closeWindow: function (){
                jQuery("#game-manager-window").remove();
                jQuery("#overlay").fadeOut('fast');
            },
            showModal: function (content){
            	var html = [];
            	html.push('<div class="modalpopup" id="game-manager-window" style="display: block; min-height: 100px; min-width: 300px; margin-top: -150px;margin-left: -200px; position: fixed"><a onclick="GameManagement.closeWindow()" class="modalpopup-close-btn" href="javascript:;" style=""></a>');
                html.push(content);
            	
            	html.push('</div>');
            	jQuery("body").append(html.join(""));
            	jQuery("#overlay").fadeIn('fast');
            	return html.join("");

            },
            newCat:function (gameId){
                var html = [];
                html.push('<table><tr><td>Name</td><td><input id="new_category_game_id_');
                html.push(gameId);
                html.push('" type="text" maxlength="50" autocomplete="OFF">');
                html.push('</td></tr><tr><td></td><td><input onclick="GameManagement.newCategory(\''+gameId+'\')" type="button" value="Save" maxlength="10" title="to" autocomplete="OFF" style=""></td></tr></table>');
                this.showModal(html.join(""));
            },
            newGame:function (){
                var html = [];
                html.push('<table width="100%" border="0" cellspacing="3" cellpadding="0"><tr>');
                html.push('<td>Name: </td><td><input type="text" name="new_game_name" id="new_game_name" style="width:100px;"/></td></tr><tr>');
                html.push('<td>Description: </td><td><textarea rows="3" cols="23"  name="new_game_description" id="new_game_description" style="height:110px;"></textarea></td></tr>');
                html.push('<tr><td></td><td><input onclick="GameManagement.newGameAction()" type="button" value="Save" maxlength="10" title="to" autocomplete="OFF" style=""></td></tr></table>');
                this.showModal(html.join(""));
            },
            changeGameName:function (game_name,game_id){
                var html = [];
                html.push('<table width="100%" border="0" cellspacing="3" cellpadding="0"><tr>');
                html.push('<td>Name: </td><td><input type="text" name="new_game_name" id="game_new_name" value="'+game_name+'" style="width:100px;"/></td></tr>');
                html.push('<tr><td></td><td><input onclick="GameManagement.changeGameNameAction('+game_id+')" type="button" value="Save" maxlength="10" title="to" autocomplete="OFF" style=""></td></tr></table>');
                this.showModal(html.join(""));
            },
            changeGameNameAction:function (game_id){
                jQuery.post("managerService.php",{"method":"changeGameName","game_id":game_id,"game_name":jQuery("#game_new_name").val()}, function(data){
                    if(data.IsSuccess){
                        GameManagement.reload();
                    }
                }, "json");
            },
            
            changeCategoryName:function (category_name,category_id){
                var html = [];
                html.push('<table width="100%" border="0" cellspacing="3" cellpadding="0"><tr>');
                html.push('<td>Name: </td><td><input type="text" name="new_category_name" id="new_category_name" value="'+category_name+'" style="width:100px;"/></td></tr>');
                html.push('<tr><td></td><td><input onclick="GameManagement.changeCategoryNameAction('+category_id+')" type="button" value="Save" maxlength="10" title="to" autocomplete="OFF" style=""></td></tr></table>');
                this.showModal(html.join(""));
            },
            changeCategoryNameAction:function (category_id){
                jQuery.post("managerService.php",{"method":"changeCategoryName","category_id":category_id,"category_name":jQuery("#new_category_name").val()}, function(data){
                    if(data.IsSuccess){
                        GameManagement.reload();
                    }
                }, "json");
            },
            changePropertyName:function (property_name,property_id){
                var html = [];
                html.push('<table width="100%" border="0" cellspacing="3" cellpadding="0"><tr>');
                html.push('<td>Name: </td><td><input type="text" name="new_property_name" id="new_category_name" value="'+property_name+'" style="width:100px;"/></td></tr>');
                html.push('<tr><td></td><td><input onclick="GameManagement.changePropertyNameAction('+property_id+')" type="button" value="Save" maxlength="10" title="to" autocomplete="OFF" style=""></td></tr></table>');
                this.showModal(html.join(""));
            },
            changePropertyNameAction:function (property_id){
                jQuery.post("managerService.php",{"method":"changePropertyName","property_id":property_id,"property_name":jQuery("#new_property_name").val()}, function(data){
                    if(data.IsSuccess){
                        GameManagement.reload();
                    }
                }, "json");
            },
            newGameAction:  function () {
                jQuery.post("managerService.php",{"method":"addGame","game_description":jQuery("#new_game_description").val(),"game_name":jQuery("#new_game_name").val()}, function(data){
                    if(data.IsSuccess){
                        GameManagement.reload();
                    }
                }, "json");
            },
            newProp:function (gameId,catId){
                var html = [];
                html.push('<table><tr><td>Name</td><td><input id="new_property_game_id_');
                html.push(catId);
                html.push('" type="text" maxlength="50" autocomplete="OFF">');
                html.push('</td></tr><tr><td></td><td><input onclick="GameManagement.newProperty(\''+gameId+'\', \''+catId+'\')" type="button" value="Save" maxlength="10" title="to" autocomplete="OFF" style=""></td></tr></table>');
                this.showModal(html.join(""));
            },
            newCategory:  function (gameId) {
                jQuery.post("managerService.php",{"method":"addCategory","game_id":gameId,"category_name":jQuery("#new_category_game_id_"+gameId).val()}, function(data){
                    if(data.IsSuccess){
                        GameManagement.reload();
                    }
                }, "json");
            },
            newProperty:  function (gameId,catId) {
                jQuery.post("managerService.php",{"method":"addProp","game_id":gameId,"category_id":catId,"property_name":jQuery("#new_property_game_id_"+catId).val()}, function(data){
                    if(data.IsSuccess){
                        GameManagement.reload();
                    }
                }, "json");
            },
            reload: function () {
                this.closeWindow();
                this.start(this.wrapper);
            },
            test:  function (value) {
                alert(value);
            },
            deleteGame: function(game_id) {
                jQuery.post("managerService.php",{"method":"deleteGame","game_id":game_id}, function(data){
                    if(data.IsSuccess){
                        GameManagement.reload();
                    }
                }, "json");
            },
            deleteCategory: function(game_id, category_id){
                jQuery.post("managerService.php",{"method":"deleteCategory","game_id":game_id,"category_id":category_id}, function(data){
                    if(data.IsSuccess){
                        GameManagement.reload();
                    }
                }, "json");
            },
            deleteProp: function(game_id, category_id, property_id){
                jQuery.post("managerService.php",{"method":"deleteProp","game_id":game_id,"category_id":category_id,"property_id":property_id}, function(data){
                    if(data.IsSuccess){
                        GameManagement.reload();
                    }
                }, "json");
            } 
        }
        window.GameManagement = GameManagement;
    }
    
)(window);



