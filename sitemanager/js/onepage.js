function onepage(formID)
{
	var is_one_page = j("#is_one_page").is(':checked')?'Y':'N';
	
	var data = {'page_id':document.getElementById('page_id').value,'is_one_page':is_one_page};
	
	j.ajax({
			type:'POST',
			url:'set_onepage_status.php',
			data: data,
			dataType:'json',
			success:function(jData){
				if(jData.flag == 1)
				{
					j("#onepage_msg").hide();
					j("#onepage_msg").html(jData.html);
					j("#onepage_msg").show('slow');
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				j(".contactus-block").html("Some server error occoured. Please report to system administrator.");
			}
	});
}
//Contact us Portion Ends