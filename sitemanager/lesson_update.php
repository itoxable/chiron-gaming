<?php
/*******************************************************************************/
			#This page is to add/edit language details
			
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$lesson_id=$_REQUEST['lesson_id'];
/*$language_name=$_REQUEST['language_name'];
$language_abbreviation=$_REQUEST['language_abbreviation'];*/
$action=$_REQUEST['action'];

if($action=="trans")
{
		$FabricArr['lesson_title']	= htmlspecialchars(trim($lesson_title));
		$FabricArr['lesson_price']	= $lesson_price;
		$FabricArr['lesson_commision_admin']  = $lesson_commision_admin;
		$FabricArr['lesson_status_id']	= $lesson_status_id;
		
		$FabricArr['lesson_payment_date']	= $paid_date;
		$FabricArr['lesson_payment_transaction_no']	= $transaction_no;		
		
		/*$StatusTitleSql="SELECT lesson_status_title FROM ".TABLEPREFIX."_lesson_status WHERE lesson_status_id=".$lesson_status_id;
		$StatusTitle = $AdminManagerObjAjax->GetRecords("Row",$StatusTitleSql);*/
		
		$SelectStatusForMailSql="SELECT *,(SELECT email FROM ".TABLEPREFIX."_user WHERE user_id=".TABLEPREFIX."_lesson.coach_id) AS email,(SELECT name FROM ".TABLEPREFIX."_user WHERE user_id=".TABLEPREFIX."_lesson.coach_id) AS name FROM ".TABLEPREFIX."_lesson WHERE lesson_id=".$lesson_id;
		$SelectStatusForMail=$AdminManagerObjAjax->GetRecords("Row",$SelectStatusForMailSql);
		
		if($lesson_status_id != $SelectStatusForMail['lesson_status_id']){
		
					$to  	  = $SelectStatusForMail['email'];
					$subject  = 'Lesson Status Changed';
					$message  = '<h3>Dear '.$SelectStatusForMail['name'].',</h3>';
					$message .= 'The status of the lesson '.$SelectStatusForMail['lesson_title'].' has been changed to '.$StatusTitle['lesson_status_title'].' by the administrator';
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: info@citytechsoftware.com' . "\r\n" .
								'Reply-To: sales@citytechsoftware.com' . "\r\n" .
								'X-Mailer: PHP/' . phpversion();
					
					mail($to, $subject, $message, $headers);
		
		}
		
		$err_msgs="";
		
		$table_name = TABLEPREFIX."_lesson";
		
		if($lesson_status_id == 4 || $lesson_status_id == 5 ){
		
		$err_msgs .=$AdminManagerObjAjax->Validate($lesson_title,"Title","EMP",$type="");
		$err_msgs .=$AdminManagerObjAjax->Validate($lesson_price,"Price","EMP",$type="");
		$err_msgs .=$AdminManagerObjAjax->Validate($lesson_commision_admin,"Admin Commission","EMP",$type="");

			$fields_values = array( 
								'lesson_title'				=> $lesson_title,
								'lesson_description'		=> $lesson_description,
								'lesson_price'				=> $lesson_price,
								'lesson_commision_admin'	=> $lesson_commision_admin,
								'lesson_closed_date'		=> date("Y-m-d H:i:s"),
								'date_edited'				=> date("Y-m-d H:i:s"),
								'lesson_status_id'			=> $lesson_status_id		
							  );
		} 
		if($lesson_status_id == 2 ){
		
		$err_msgs .=$AdminManagerObjAjax->Validate($lesson_title,"Title","EMP",$type="");
		$err_msgs .=$AdminManagerObjAjax->Validate($lesson_price,"Price","EMP",$type="");
		$err_msgs .=$AdminManagerObjAjax->Validate($lesson_commision_admin,"Admin Commission","EMP",$type="");
		
		if($paid_date=='Date'){
			$err_msgs="<li>Field <span class='red'>Payment Date</span> is left empty.</li>";		
		}
		
		if($transaction_no=='Transaction No.'){
			$err_msgs="<li>Field <span class='red'>Transaction No.</span> is left empty.</li>";		
		}

			$fields_values = array( 
								'lesson_title'					=> $lesson_title,
								'lesson_description'			=> $lesson_description,
								'lesson_price'					=> $lesson_price,
								'lesson_commision_admin'		=> $lesson_commision_admin,
								'lesson_payment_date'			=> $paid_date,
								'lesson_payment_transaction_no'	=> $transaction_no,
								'date_edited'					=> date("Y-m-d H:i:s"),
								'lesson_status_id'				=> $lesson_status_id		
							  );
							  
		}
		else {

		$err_msgs .=$AdminManagerObjAjax->Validate($lesson_title,"Title","EMP",$type="");
		$err_msgs .=$AdminManagerObjAjax->Validate($lesson_price,"Price","EMP",$type="");
		$err_msgs .=$AdminManagerObjAjax->Validate($lesson_commision_admin,"Admin Commission","EMP",$type="");
      
			$fields_values = array( 
								'lesson_title'				=> $lesson_title,
								'lesson_description'		=> $lesson_description,
								'lesson_price'				=> $lesson_price,
								'lesson_commision_admin'	=> $lesson_commision_admin,
								'date_edited'				=> date("Y-m-d H:i:s"),
								'lesson_status_id'			=> $lesson_status_id		
							  );
		}		
			
		$where="lesson_id='$lesson_id'";										

		if($err_msgs==''){
			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);
			echo "<script>window.location.href='lesson_manager.php?messg=".$msgreport."&IsPreserved=Y'</script>";
		    exit; 
		}
		else
		{
		   $smarty->assign('err_msgs',$err_msgs);
		}  
				
		
		
}

if(!empty($lesson_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_lesson WHERE lesson_id=".$lesson_id;
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	
	$RsCatSql['lesson_title'] 			= addslashes(show_to_control($RsCatSql['lesson_title']));
	$RsCatSql['lesson_description'] 	= addslashes(show_to_control($RsCatSql['lesson_description']));
	
	$Sql_Coach="SELECT name FROM ".TABLEPREFIX."_user WHERE user_id=".$RsCatSql['coach_id'];
	$Coach_Name = $AdminManagerObjAjax->GetRecords("Row",$Sql_Coach);
	
	$RsCatSql['coach_name'] = $Coach_Name['name'];
	
	$Sql_CoachPaypal="SELECT paypal_email_id FROM ".TABLEPREFIX."_user WHERE user_id=".$RsCatSql['coach_id'];
	$Coach_Paypal = $AdminManagerObjAjax->GetRecords("Row",$Sql_CoachPaypal);
	
	$RsCatSql['paypal_email_id'] = $Coach_Paypal['paypal_email_id'];
	
	$Sql_Student="SELECT name FROM ".TABLEPREFIX."_user WHERE user_id=".$RsCatSql['student_id'];
	$Student_Name = $AdminManagerObjAjax->GetRecords("Row",$Sql_Student);
	
	$RsCatSql['student_name'] = $Student_Name['name'];
	
	$Sql_Status="SELECT lesson_status_title FROM ".TABLEPREFIX."_lesson_status WHERE lesson_status_id=".$RsCatSql['lesson_status_id'];
	$Status_Name = $AdminManagerObjAjax->GetRecords("Row",$Sql_Status);
	
	$RsCatSql['status_name'] = $Status_Name['lesson_status_title'];
	/* Get Record For Display Ends */

	$SubmitButton="View Lesson Details ";	
	$flag=1;
}
if($RsCatSql['lesson_status_id'] == 1){
	
	$Sql_Lesson_Status = "SELECT * FROM ".TABLEPREFIX."_lesson_status WHERE is_active='Y'";
	$Lesson_Status_Arr = $AdminManagerObjAjax->GetRecords("All",$Sql_Lesson_Status);
	
}else{

	$Sql_Lesson_Status="SELECT * FROM ".TABLEPREFIX."_lesson_status WHERE lesson_status_id <> 1 AND is_active='Y'";
	$Lesson_Status_Arr=$AdminManagerObjAjax->GetRecords("All",$Sql_Lesson_Status);
}

for($i=0;$i<count($Lesson_Status_Arr);$i++)
{	
$SelectStatusArr[$i]['lesson_status_id'] 		= $Lesson_Status_Arr[$i]['lesson_status_id'];
$SelectStatusArr[$i]['lesson_status_title'] 	= addslashes(show_to_control($Lesson_Status_Arr[$i]['lesson_status_title']));
}

	
if($RsCatSql['lesson_status_id'] == 2){
	$RsCatSql['lesson_payment_date'] = $RsCatSql['lesson_payment_date'];
	$RsCatSql['lesson_payment_transaction_no'] = $RsCatSql['lesson_payment_transaction_no'];
}else{
	$RsCatSql['lesson_payment_date'] = 'Date';
	$RsCatSql['lesson_payment_transaction_no'] = 'Transaction No.';
}



/*echo '<pre>';
print_r($SelectStatusArr);*/
/* Assign Smarty Variables Starts */

$smarty->assign('CmsArr',$CmsArr);
$smarty->assign('lesson_id',$lesson_id);
$smarty->assign('FabricArr',$RsCatSql);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);
$smarty->assign('SelectStatusArr',$SelectStatusArr);
/* Assign Smarty Variables Ends */

$smarty->display("lesson_update.tpl");
?>