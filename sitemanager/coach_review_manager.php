<?php
/*******************************************************************************/
	#This page lists all the ladder we have in table
	#last Updated : August 25 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$page_name="coach_review_manager.php";
$action=$_REQUEST['action'];



$IsPreserved=$_REQUEST['IsPreserved'];

if($IsPreserved=="Y")
{
/* preserve select starts */
$PreserveVariableArr=$preserve_variable_object->PreserveVariableSelect();
foreach($PreserveVariableArr as $a=>$b){$$a=$b;}
/* preserve select ends */
}

/* Activate Operation Starts */
if($action=="activate")
{
  $Reviewdtls = "SELECT * FROM ".TABLEPREFIX."_user_review WHERE user_review_id='$record_id'";
  $dtlsArr = $AdminManagerObjAjax->GetRecords("Row",$Reviewdtls);
  $rating = $dtlsArr['rating'];
  $u_id = $dtlsArr['user_id'];
  $is_active = $dtlsArr['is_active'];
  $totreview = "SELECT count(*),sum(rating) as totrate FROM ".TABLEPREFIX."_user_review WHERE user_id='".$u_id." AND user_type_id=1 AND is_active='Y'";
  $totArr = $AdminManagerObjAjax->GetRecords("Row",$totreview);
  $tot_count = $totArr[0];
  $tot_rate = $totArr['totrate'];
  if($is_active == 'N')
  {
    $rate = $tot_rate+$rating;
	$totreviewer = $tot_count+1;
  }	
  if($is_active == 'Y')
  {
    $rate = $tot_rate-$rating;
	$totreviewer = $tot_count-1;
  }	
	$avg = $rate/$totreviewer;
	$UpdateSql = "UPDATE ".TABLEPREFIX."_user_type_user_relation SET overall_rating='$avg' WHERE user_id='$u_id'";
	$adodbcon->Execute($UpdateSql);
    $ChangeStatusObjAjax->ChangeStatus($record_id,10);	
}
/* Activate Operation Ends */


/* Delete Operation Starts */

if($action=="del")
{	
	if(!empty($delete_id))
	{
	
	  $Reviewdtls = "SELECT * FROM ".TABLEPREFIX."_user_review WHERE user_review_id='$delete_id'";
	  $dtlsArr = $AdminManagerObjAjax->GetRecords("Row",$Reviewdtls);
	  $rating = $dtlsArr['rating'];
	  $review_to_id = $dtlsArr['user_id'];
	  $is_active = $dtlsArr['is_active'];
	  $totreview = "SELECT count(*),sum(rating) as totrate FROM ".TABLEPREFIX."_user_review WHERE user_id='".$review_to_id."' AND is_active='Y'";
	  $totArr = $AdminManagerObjAjax->GetRecords("Row",$totreview);
	  $tot_count = $totArr[0];
	  $tot_rate = $totArr['totrate'];
	  if($is_active == 'Y')
	  {
		$rate = $tot_rate-$rating;
		$totreviewer = $tot_count-1;
	  }	
		$avg = round($rate/$totreviewer);
		$UpdateSql = "UPDATE ".TABLEPREFIX."_user_type_user_relation SET overall_rating='$avg' WHERE user_id='$review_to_id' AND user_type_id=1";
		$adodbcon->Execute($UpdateSql);
		
		
		
		/* Delete review Starts */
		$SqlDelete="DELETE FROM ".TABLEPREFIX."_user_review WHERE user_review_id='$delete_id'";
		$ResultDelete=$adodbcon->Execute($SqlDelete);
		/* Delete review Ends */

		if($adodbcon->ErrorNo()) 
			$messg = 5;
		else 
			$messg = 6;
	}
	else
	{
		$messg = 7;
	}		
}

/* Delete Operation Ends */




/* IsProcess Starts */

$action_arr=array("list_order","ranking","activate","list_paginate","list_search","del","default");

if(in_array($action,$action_arr))
{
	$smarty->assign('IsProcess',"Y");
}

/* IsProcess Ends */

	/* listing Operation Starts */

	/* search section starts */

	if($dosearch=="GO")
	{ 
		
		$Search_from_date = $Search_from_date=='Search From'?'':$Search_from_date." 00:00:00";
		$Search_to_date = $Search_to_date=='Search To'?'':$Search_to_date." 23:59:59";
		
		if(!empty($u_id))
		{
			 $searchSql =  $searchSql." AND user_id='$u_id'"; 
		}

		if(!empty($Search_from_date))
		{
			 $searchSql =  $searchSql." AND date_added >= '".mysql_quote($Search_from_date,"N")."'"; 
		}
		if(!empty($Search_to_date))
		{
			 $searchSql =  $searchSql." AND date_added <= '".mysql_quote($Search_to_date,"N")."'"; 
		}
		$SearchLink="dosearch=GO&u_id=$u_id&Search_from_date=$Search_from_date&
		Search_to_date=$Search_to_date";
	}	

	/* search section ends */

	/* Order section starts */	

	$SortingSequenceArr=array(1 => "date_added");
	$ReturnSortingArr=$SortingObjAjax->Sorting("date_added DESC",$SortingSequenceArr,$do_order,$OrderByID,$OrderType);	
	$OrderBySql=$ReturnSortingArr['OrderBySql'];
	$OrderLink=$ReturnSortingArr['OrderLink'];
	$OrderType=$ReturnSortingArr['OrderType'];			

	/* Order section ends */

	$SqlSelectCat="SELECT * FROM ".TABLEPREFIX."_user_review  WHERE user_type_id=1 ".$searchSql.$OrderBySql;

	/* pagination starts */	

	$pagination_arr=$PaginationObjAjax->PaginationAjax($SqlSelectCat,$page_name."?action=".$action."&".$SearchLink."&".$OrderLink,"ManagerGeneral");	

	/* pagination ends */ 

	/* preserve update starts */

	$PreserveLink="from=$from&".$SearchLink."&".$OrderLink;

	$preserve_variable_object->PreserveVariableUpdate($PreserveLink);

	/* preserve update ends */		
    


	#Fetch all project gallery and store them in an array
	$SelectCmsArr=$adodbcon->GetAll($pagination_arr[0]);	

	$NumSelectCms=count($SelectCmsArr);

	for($i=0;$i<$NumSelectCms;$i++)
	{				
		$SelectCoach = "SELECT name FROM ".TABLEPREFIX."_user WHERE user_id='".$SelectCmsArr[$i]['user_id']."'";
		$CoachArr = $AdminManagerObjAjax->GetRecords("Row",$SelectCoach);
		
		$SelectReviewby = "SELECT name FROM ".TABLEPREFIX."_user WHERE user_id='".$SelectCmsArr[$i]['reviewed_by']."'";
		$UserArr = $AdminManagerObjAjax->GetRecords("Row",$SelectReviewby);
		$SelectCmsArr[$i]['review_to'] 		                = $CoachArr['name'];
		$SelectCmsArr[$i]['review_by'] 		                = $UserArr['name'];
		$SelectCmsArr[$i]['name_delete']                    = $UserArr['name'];
		$SelectCmsArr[$i]['date_added'] 			        = date_format_admin($SelectCmsArr[$i]['date_added']);
		$SelectCmsArr[$i]['active_img'] 				    = $SelectCmsArr[$i]['is_active']=="Y" ? "true.gif" : "false.gif";
		$SelectCmsArr[$i]['active_alt'] 				    = $SelectCmsArr[$i]['is_active']=="Y" ? "Active" : "Inactive";
		
		$Sql_Individual="select rat.comment,rat.rating,cat.rating_category from gt_user_rating as rat,gt_rating_category as cat where rat.rcat_id=cat.rcat_id and rat.user_review_id=".$SelectCmsArr[$i]['user_review_id'];
  
		$Individual[$i] =  $AdminManagerObjAjax->GetRecords("All",$Sql_Individual);
	
		for($k=0;$k<count($Individual[$i][$k]);$k++){
		
			$Individual[$i][$k]['comment']=$Individual[$i][$k]['comment'];
			$Individual[$i][$k]['rating']=$Individual[$i][$k]['rating'];
			$Individual[$i][$k]['rating_category']=$Individual[$i][$k]['rating_category'];
		
		}
	}

/* listing Operation Ends */

  $SelectCoach = "SELECT distinct(b.user_id),a.name FROM ".TABLEPREFIX."_user a, ".TABLEPREFIX."_user_review b WHERE b.user_id=a.user_id and b.user_type_id=1";
  $Coach = $AdminManagerObjAjax->GetRecords("All",$SelectCoach);

$MessgReportText=displayMessage($messg);

#Getting project title for this gallery

/* Assign Smarty Variables Starts */

$smarty->assign("MessgReportText",$MessgReportText);
$smarty->assign('page_name',$page_name);
$smarty->assign('from',$from);
$smarty->assign('Coach',$Coach);
$smarty->assign('PreserveLink',$PreserveLink);
$smarty->assign('SearchLink',$SearchLink);
$smarty->assign('dosearch',$dosearch);
$smarty->assign('user_id',$u_id);
$smarty->assign("Search_by_name",$Search_by_name);
$smarty->assign("Search_to_date",$Search_to_date);
$smarty->assign('OrderLink',$OrderLink);
$smarty->assign('do_order',$do_order);
$smarty->assign('OrderType',$OrderType);
$smarty->assign('OrderByID',$OrderByID);
$smarty->assign('ReturnSortingArr',$ReturnSortingArr);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('NumSelectCms',$NumSelectCms);
$smarty->assign('SelectCmsArr',$SelectCmsArr);
$smarty->assign('Individual',$Individual);

/* Assign Smarty Variables Ends */

$smarty->display("coach_review_manager.tpl");
?>