<?php
/*******************************************************************************/
	#This page lists all the game we have in table
	#last Updated : Aug 23 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$page_name="champion_manager.php";

$action=$_REQUEST['action'];
$game_id = $_REQUEST['game_id'];
//$id=$_REQUEST['id'];
$IsPreserved=$_REQUEST['IsPreserved'];

if($IsPreserved=="Y")
{
/* preserve select starts */
$PreserveVariableArr=$preserve_variable_object->PreserveVariableSelect();
foreach($PreserveVariableArr as $a=>$b){$$a=$b;}
/* preserve select ends */
}

/* Activate Operation Starts */
if($action=="activate")
{
	$ChangeStatusObjAjax->ChangeStatus($record_id,36);	
}
/* Activate Operation Ends */
/* Default Operation Starts */
/*
if($action=="default")
{
	$ChangeStatusObjAjax->ChangeStatus($record_id,8);	
}
*/
/* Default Operation Ends *

/* Delete Operation Starts */

if($action=="del")
{	
	if(!empty($delete_id))
	{
		
		
		
		/* Delete Events Starts */
		$SqlDelete="DELETE FROM ".TABLEPREFIX."_game_champion WHERE champion_id='$delete_id'";
		$ResultDelete=$adodbcon->Execute($SqlDelete);		
		/* Delete Events Ends */

		if($adodbcon->ErrorNo()) 
			$messg = 5;
		else 
			$messg = 6;
	}	
	else
	{
		$messg = 7;
	}		
}

/* Delete Operation Ends */


/* IsProcess Starts */

$action_arr=array("list_order","ranking","activate","default","list_paginate","list_search","del");

if(in_array($action,$action_arr))
{
	$smarty->assign('IsProcess',"Y");
}

/* IsProcess Ends */

	/* listing Operation Starts */

	/* search section starts */

	if($dosearch=="GO")
	{ 
		$Search_champion = $Search_champion=='Search by champion title'?'':$Search_champion;
		if(!empty($Search_champion))
		{
			 $searchSql =  $searchSql." AND champion_title like '%".mysql_quote($Search_champion,"N")."%'"; 
		}
		$SearchLink="dosearch=GO&Search_champion=$Search_champion";
		
	}	
	/* search section ends */

	/* Order section starts */	

	$SortingSequenceArr=array(1 => "champion_title");	
	$ReturnSortingArr=$SortingObjAjax->Sorting("champion_title",$SortingSequenceArr,$do_order,$OrderByID,$OrderType);	
	$OrderBySql=$ReturnSortingArr['OrderBySql'];
	$OrderLink=$ReturnSortingArr['OrderLink'];
	$OrderType=$ReturnSortingArr['OrderType'];			

	/* Order section ends */

	$SqlSelectCat="SELECT * FROM ".TABLEPREFIX."_game_champion Where 1=1 ".$searchSql.$OrderBySql;
	/* pagination starts */	

	$pagination_arr=$PaginationObjAjax->PaginationAjax($SqlSelectCat,$page_name."?action=".$action."&".$SearchLink."&".$OrderLink,"ManagerGeneral");	

	/* pagination ends */ 

	/* preserve update starts */

	$PreserveLink="from=$from&".$SearchLink."&".$OrderLink;

	$preserve_variable_object->PreserveVariableUpdate($PreserveLink);

	/* preserve update ends */		


	#Fetch all events and store them in an array
	$SelectCmsArr=$adodbcon->GetAll($pagination_arr[0]);	

	$NumSelectCms=count($SelectCmsArr);

	for($i=0;$i<$NumSelectCms;$i++)
	{				
		$SelectCmsArr[$i]['champion_title'] 			    = $SelectCmsArr[$i]['champion_title'];
		$SelectCmsArr[$i]['champion_delete'] 	    = addslashes(show_to_control($SelectCmsArr[$i]['champion_title']));
		$SelectCmsArr[$i]['active_img'] 			= $SelectCmsArr[$i]['is_active']=="Y" ? "true.gif" : "false.gif";
		$SelectCmsArr[$i]['date_added']				= date_format_admin($SelectCmsArr[$i]['date_added']);
		$game_id 										= $SelectCmsArr[$i]['game_id'];
	
	}

/* listing Operation Ends */

$MessgReportText=displayMessage($messg);

#Getting project title for this gallery
$CategoryTitle = $AdminManagerObjAjax->GetRecords("One","SELECT game_name FROM ".TABLEPREFIX."_game WHERE game_id=".$game_id);	

/* Assign Smarty Variables Starts */

$smarty->assign("CategoryTitle",$CategoryTitle);
$smarty->assign("game_id",$game_id);
$smarty->assign("MessgReportText",$MessgReportText);
$smarty->assign('page_name',$page_name);
$smarty->assign('from',$from);
$smarty->assign('PreserveLink',$PreserveLink);
$smarty->assign('SearchLink',$SearchLink);
$smarty->assign('dosearch',$dosearch);
$smarty->assign("Search_champion",$Search_champion);
$smarty->assign('OrderLink',$OrderLink);
$smarty->assign('do_order',$do_order);
$smarty->assign('OrderType',$OrderType);
$smarty->assign('OrderByID',$OrderByID);
$smarty->assign('ReturnSortingArr',$ReturnSortingArr);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('NumSelectCms',$NumSelectCms);
$smarty->assign('SelectCmsArr',$SelectCmsArr);

/* Assign Smarty Variables Ends */

$smarty->display("champion_manager.tpl");
?>