<?php
/*******************************************************************************/
	#This page works behind an Ajax call to check admin's login procedure 
	#and set related session values
	#last Updated : June 29 , 2010
/*******************************************************************************/


include('general_include.php');
include("../class/json.php");

$Username=trim($_POST['Username']);
$Password=trim($_POST['Password']);

#if username password not empty, check the vald entry in table
if(!empty($Username) && !empty($Password))
{

	$SqlLogin = "SELECT * FROM ".TABLEPREFIX."_admin WHERE admin_username=".mysql_quote($Username)." AND admin_password=".mysql_quote($Password)." AND is_active='Y' Limit 0,1 "; 

	$RsLogin=$adodbcon->Execute($SqlLogin);		

	$NumLogin = $RsLogin->NumRows();

	if($NumLogin>0)
	{ 

		#Record found in database, set the session values
		$RowLogin=$RsLogin->FetchRow();					

		if(stripslashes($RowLogin["admin_username"])==$Username && (stripslashes($RowLogin["admin_password"]))==$Password)

		{	
			session_register("SesGTAdminID");
			$_SESSION['SesGTAdminID'] = $RowLogin['admin_id'];
						
		    session_register("SesGTAdminKey");
			$_SESSION['SesGTdminKey'] = "gt_superadmin";	

			session_register("SesGTAdminName");
			$_SESSION['SesGTAdminName'] = $RowLogin['admin_username'];		


		echo json_encode(

				array(

					'flag'	=>	1
				)
			);
		}
		else
		{
			#Record not found in database, Authentication failed
			echo json_encode(

				array(
					'flag'	=>	2
				)
			);
			//echo "Authentication failed";
		}
	}
	else
	{
			echo json_encode(

				array(

					'flag'	=>	3
				)
			);
		//echo "Authentication failed";
	}
}
else
{
				echo json_encode(

				array(

					'flag'	=>	4
				)
			);
	//echo "Authentication failed";
}
?>