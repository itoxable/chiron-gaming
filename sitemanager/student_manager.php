<?php
/*******************************************************************************/
	#This page lists all the ladder we have in table
	#last Updated : August 24 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$page_name="student_manager.php";
$action=$_REQUEST['action'];

$IsPreserved=$_REQUEST['IsPreserved'];

if($IsPreserved=="Y")
{
/* preserve select starts */
$PreserveVariableArr=$preserve_variable_object->PreserveVariableSelect();
foreach($PreserveVariableArr as $a=>$b){$$a=$b;}
/* preserve select ends */
}

/* Activate Operation Starts */
if($action=="activate")
{
	$ChangeStatusObjAjax->ChangeStatus($record_id,6);	
}
/* Activate Operation Ends */


/* Delete Operation Starts */

if($action=="del")
{	
	if(!empty($delete_id))
	{
	
		/* Delete student Starts */
		$SqlDelete="DELETE FROM ".TABLEPREFIX."_user WHERE user_id='$delete_id'";
		$ResultDelete=$adodbcon->Execute($SqlDelete);
		
		$SqlDeletetype="DELETE FROM ".TABLEPREFIX."_user_type_user_relaton WHERE user_id='$delete_id'";
		$ResultDelete=$adodbcon->Execute($SqlDeletetype);		
		/* Delete student Ends */

		if($adodbcon->ErrorNo()) 
			$messg = 6;
		else 
			$messg = 5;
	}
	else
	{
		$messg = 7;
	}		
}

/* Delete Operation Ends */




/* IsProcess Starts */

$action_arr=array("list_order","ranking","activate","list_paginate","list_search","del","default");

if(in_array($action,$action_arr))
{
	$smarty->assign('IsProcess',"Y");
}

/* IsProcess Ends */

	/* listing Operation Starts */

	/* search section starts */

	if($dosearch=="GO")
	{ 
		$Search_by_name = $Search_by_name=='Search by Name'?'':$Search_by_name;
		$Search_by_email = $Search_by_email=='Search by Email'?'':$Search_by_email;
		$Search_from_date = $Search_from_date=='Search From'?'':$Search_from_date;
		$Search_to_date = $Search_to_date=='Search To'?'':$Search_to_date;
		if(!empty($Search_by_name))
		{
			 $searchSql =  $searchSql." AND a.name like '%".mysql_quote($Search_by_name,"N")."%'"; 
		}
		if(!empty($Search_by_email))
		{
			 $searchSql =  $searchSql." AND a.email like '%".mysql_quote($Search_by_email,"N")."%'"; 
		}

		if(!empty($Search_from_date))
		{
			 $searchSql =  $searchSql." AND a.date_registered >= '".mysql_quote($Search_from_date,"N")."'"; 
		}
		if(!empty($Search_to_date))
		{
			 $searchSql =  $searchSql." AND a.date_registered <= '".mysql_quote($Search_to_date,"N")."'"; 
		}
		$SearchLink=                                                           "dosearch=GO&Search_by_name=$Search_by_name&Search_by_email=$Search_by_email&Search_from_date=$Search_from_date&
		Search_to_date=$Search_to_date";
	}	

	/* search section ends */

	/* Order section starts */	

	$SortingSequenceArr=array(1 => "name",2 => "email",3 => "date_registered");
	$ReturnSortingArr=$SortingObjAjax->Sorting("date_registered DESC",$SortingSequenceArr,$do_order,$OrderByID,$OrderType);	
	$OrderBySql=$ReturnSortingArr['OrderBySql'];
	$OrderLink=$ReturnSortingArr['OrderLink'];
	$OrderType=$ReturnSortingArr['OrderType'];			

	/* Order section ends */

	$SqlSelectCat="SELECT a.*,b.* FROM ".TABLEPREFIX."_user a, ".TABLEPREFIX."_user_type_user_relation b 
	Where a.user_id=b.user_id AND b.user_type_id='2'".$searchSql.$OrderBySql;

	/* pagination starts */	

	$pagination_arr=$PaginationObjAjax->PaginationAjax($SqlSelectCat,$page_name."?action=".$action."&".$SearchLink."&".$OrderLink,"ManagerGeneral");	

	/* pagination ends */ 

	/* preserve update starts */

	$PreserveLink="from=$from&".$SearchLink."&".$OrderLink;

	$preserve_variable_object->PreserveVariableUpdate($PreserveLink);

	/* preserve update ends */		


	#Fetch all project gallery and store them in an array
	$SelectCmsArr=$adodbcon->GetAll($pagination_arr[0]);	

	$NumSelectCms=count($SelectCmsArr);

	for($i=0;$i<$NumSelectCms;$i++)
	{				
		$SelectCmsArr[$i]['name'] 		                = $SelectCmsArr[$i]['name'];
		$SelectCmsArr[$i]['name_delete']                = addslashes(show_to_control($SelectCmsArr[$i]['name']));
		$SelectCmsArr[$i]['date_registered'] 			= date_format_admin($SelectCmsArr[$i]['date_registered']);
		$SelectCmsArr[$i]['active_img'] 				= $SelectCmsArr[$i]['is_active']=="Y" ? "true.gif" : "false.gif";
		$SelectCmsArr[$i]['active_alt'] 				= $SelectCmsArr[$i]['is_active']=="Y" ? "Active" : "Inactive";
		
	
	    $FavCoach[$i] = "SELECT * FROM ".TABLEPREFIX."_user_fav_coach WHERE user_id='".$SelectCmsArr[$i]['user_id']."'";
		$FavCoachArr[$i] = $AdminManagerObjAjax->GetRecords('All',$FavCoach[$i]);
		
		$NumFavCoach[$i] = count($FavCoachArr[$i]);
		for($c=0;$c<$NumFavCoach[$i];$c++)
		{
		   $Coach = "SELECT * FROM ".TABLEPREFIX."_user WHERE user_id = '".$FavCoachArr[$i][$c]['coach_id']."'";
		   $CoachArr = $AdminManagerObjAjax->GetRecords('Row',$Coach);
		   $FavCoachArr[$i][$c]['name'] = $CoachArr['name'];
		   if($CoachArr['photo']!='')
		     $FavCoachArr[$i][$c]['image']= '../uploaded/user_images/thumbs/'.$CoachArr['photo'];
		   else
		     $FavCoachArr[$i][$c]['image']= '../images/avatar_smallest.jpg'; 	
			 $FavCoachArr[$i][$c]['user_about'] =  $CoachArr['user_about'];
			 $FavCoachArr[$i][$c]['coach_id'] = $CoachArr['user_id'];
		}
		
		$FavVideo[$i] = "SELECT * FROM ".TABLEPREFIX."_user_fav_video WHERE user_id='".$SelectCmsArr[$i]['user_id']."'";
		$FavVideoArr[$i] = $AdminManagerObjAjax->GetRecords('All',$FavVideo[$i]);
		
		$NumFavVideo[$i] = count($FavVideoArr[$i]);
		for($v=0;$v<$NumFavVideo[$i];$v++)
		{
		   $Video = "SELECT * FROM ".TABLEPREFIX."_video WHERE video_id = '".$FavVideoArr[$i][$v]['video_id']."'";
		   $VideoArr = $AdminManagerObjAjax->GetRecords('Row',$Video);
		   $FavVideoArr[$i][$v]['name'] = $VideoArr['video_title'];
		  
		     $FavVideoArr[$i][$v]['image']= '../uploaded/video_images/thumbs/'.$VideoArr['video_image'];
		   	 $FavVideoArr[$i][$v]['description'] =  $VideoArr['description'];
			 $FavVideoArr[$i][$v]['video_id'] = $VideoArr['video_id'];
			 $FavVideoArr[$i][$v]['coach_id'] = $VideoArr['user_id'];
		}
		
	}

/* listing Operation Ends */

$MessgReportText=displayMessage($messg);

#Getting project title for this gallery

/* Assign Smarty Variables Starts */

$smarty->assign("MessgReportText",$MessgReportText);
$smarty->assign('page_name',$page_name);
$smarty->assign('from',$from);
$smarty->assign('PreserveLink',$PreserveLink);
$smarty->assign('SearchLink',$SearchLink);
$smarty->assign('dosearch',$dosearch);
$smarty->assign("Search_by_name",$Search_by_name);
$smarty->assign("Search_by_email",$Search_by_email);
$smarty->assign("Search_from_date",$Search_from_date);
$smarty->assign("Search_to_date",$Search_to_date);
$smarty->assign('OrderLink',$OrderLink);
$smarty->assign('do_order',$do_order);
$smarty->assign('OrderType',$OrderType);
$smarty->assign('OrderByID',$OrderByID);
$smarty->assign('FavCoachArr',$FavCoachArr);
$smarty->assign('NumFavCoach',$NumFavCoach);
$smarty->assign('FavVideoArr',$FavVideoArr);
$smarty->assign('NumFavVideo',$NumFavVideo);
$smarty->assign('ReturnSortingArr',$ReturnSortingArr);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('NumSelectCms',$NumSelectCms);
$smarty->assign('SelectCmsArr',$SelectCmsArr);


/* Assign Smarty Variables Ends */

$smarty->display("student_manager.tpl");
?>