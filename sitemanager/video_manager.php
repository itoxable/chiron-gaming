<?php
/*******************************************************************************/
	#This page lists all the race we have in table
	#last Updated : August 25 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$page_name="video_manager.php";
$u_id = $_REQUEST['u_id'];
$utype = $_REQUEST['utype'];
$action=$_REQUEST['action'];
$id=$_REQUEST['id'];

$IsPreserved=$_REQUEST['IsPreserved'];

if($IsPreserved=="Y")
{
/* preserve select starts */
$PreserveVariableArr=$preserve_variable_object->PreserveVariableSelect();
foreach($PreserveVariableArr as $a=>$b){$$a=$b;}
/* preserve select ends */
}

/* Activate Operation Starts */
if($action=="featured")
{
	$ChangeStatusObjAjax->ChangeStatus($record_id,9);	
}
/* Activate Operation Ends */


if($action=="del")
{	
	if(!empty($delete_id))
	{
	
		/* Delete Starts */
		$SqlDelete="DELETE FROM ".TABLEPREFIX."_video WHERE video_id='$delete_id'";
		$ResultDelete=$adodbcon->Execute($SqlDelete);		
		
		/* Delete Ends */

		if($adodbcon->ErrorNo()) 
			$messg = 5;
		else 
			$messg = 6;
	}
	else
	{
		$messg = 7;
	}		
}

/* Delete Operation Ends */


/* IsProcess Starts */

$action_arr=array("list_order","ranking","featured","list_paginate","list_search","del","default");

if(in_array($action,$action_arr))
{
	$smarty->assign('IsProcess',"Y");
}

/* IsProcess Ends */

	/* listing Operation Starts */


	/* Order section starts */	


/*	$ReturnSortingArr=$SortingObjAjax->Sorting("video_title",$SortingSequenceArr,$do_order,$OrderByID,$OrderType);	
	$OrderBySql=$ReturnSortingArr['OrderBySql'];
	$OrderLink=$ReturnSortingArr['OrderLink'];
	$OrderType=$ReturnSortingArr['OrderType'];			
*/
	/* Order section ends */

	$SqlSelectCat="SELECT * FROM ".TABLEPREFIX."_video Where user_id=".$u_id.$searchSql.$OrderBySql;

	/* pagination starts */	

	$pagination_arr=$PaginationObjAjax->PaginationAjax($SqlSelectCat,$page_name."?action=".$action."&".$SearchLink."&".$OrderLink,"ManagerGeneral");	

	/* pagination ends */ 

	/* preserve update starts */

	$PreserveLink="from=$from&".$SearchLink."&".$OrderLink;

	$preserve_variable_object->PreserveVariableUpdate($PreserveLink);

	/* preserve update ends */		


	#Fetch all project gallery and store them in an array
	$SelectCmsArr=$adodbcon->GetAll($pagination_arr[0]);	

	$NumSelectCms=count($SelectCmsArr);

	for($i=0;$i<$NumSelectCms;$i++)
	{				
		
		$SelectCmsArr[$i]['active_img'] 				= $SelectCmsArr[$i]['is_featured']=="Y" ? "true.gif" : "false.gif";
		$SelectCmsArr[$i]['active_alt'] 				= $SelectCmsArr[$i]['is_featured']=="Y" ? "Featured" : "";
		$SelectCmsArr[$i]['video_title'] 				= $SelectCmsArr[$i]['video_title'];
		$SelectCmsArr[$i]['video_title_delete'] 		= addslashes(show_to_control($SelectCmsArr[$i]['video_title']));
		$SelectCmsArr[$i]['game_id']					= $SelectCmsArr[$i]['game_id'];
		$SelectCmsArr[$i]['game_name'] 					= $AdminManagerObjAjax->GetRecords("One","SELECT game_name FROM ".TABLEPREFIX."_game WHERE 
		                                 game_id=".$SelectCmsArr[$i]['game_id']);	
		
		$LikeNumSql = "SELECT count(*) AS numlike FROM ".TABLEPREFIX."_user_like WHERE video_id=".$SelectCmsArr[$i]['video_id']." AND selected_for='L'" ;
		$LikeArrNum = $AdminManagerObjAjax->GetRecords('Row',$LikeNumSql);
		
		$SelectCmsArr[$i]['numlike']=$LikeArrNum['numlike'];
		
		$DLikeNumSql = "SELECT count(*) AS numdlike FROM ".TABLEPREFIX."_user_like WHERE video_id=".$SelectCmsArr[$i]['video_id']." AND selected_for='D'" ;
		$DLikeArrNum = $AdminManagerObjAjax->GetRecords('Row',$DLikeNumSql);
		
		$SelectCmsArr[$i]['numdlike']=$DLikeArrNum['numdlike'];
       
	}

/* listing Operation Ends */

$MessgReportText=displayMessage($messg);

$UserName = $AdminManagerObjAjax->GetRecords("One","SELECT name FROM ".TABLEPREFIX."_user WHERE user_id=".$u_id);	
/* Assign Smarty Variables Starts */

$smarty->assign("UserName",$UserName);
$smarty->assign("u_id",$u_id);
$smarty->assign("utype",$utype);
$smarty->assign("MessgReportText",$MessgReportText);
$smarty->assign('page_name',$page_name);
$smarty->assign('from',$from);
$smarty->assign('PreserveLink',$PreserveLink);
$smarty->assign('SearchLink',$SearchLink);
$smarty->assign('dosearch',$dosearch);
$smarty->assign("Search_race_name",$Search_race_name);
$smarty->assign('OrderLink',$OrderLink);
$smarty->assign('do_order',$do_order);
$smarty->assign('OrderType',$OrderType);
$smarty->assign('OrderByID',$OrderByID);
$smarty->assign('ReturnSortingArr',$ReturnSortingArr);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('NumSelectCms',$NumSelectCms);
$smarty->assign('SelectCmsArr',$SelectCmsArr);

/* Assign Smarty Variables Ends */

$smarty->display("video_manager.tpl");
?>