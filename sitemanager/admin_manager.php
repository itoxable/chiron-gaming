<?php
/****************************************************************/
	#Page to edit admin's own detail
	#last Updated : June 29 , 2010
/****************************************************************/

include("top.php");
$page_name="admin_manager.php";	

$action=$_GET['action'];
if($IsPreserved=="Y")
{
	/* preserve select starts */
	$PreserveVariableArr=$preserve_variable_object->PreserveVariableSelect();
	foreach($PreserveVariableArr as $a=>$b){$$a=$b;}
	/* preserve select ends */
}

/* Delete Operation Starts */
if($action=="del")
{	
	if(!empty($delete_id))
	{	
		/* Delete Operation Starts */
		$SqlDelete="DELETE FROM ".TABLEPREFIX."_admin WHERE admin_id='$delete_id'";
		$ResultDelete=$adodbcon->Execute($SqlDelete);		
		/* Delete Operation Ends */						
		if($adodbcon->ErrorNo()) 
			$messg = 5;
		else 
			$messg = 6;
	}	
	else
	{
		$messg = 7;
	}		
}
/* Delete Operation Ends */

/* Activate Operation Starts */
	if($action=="activate")
		$ChangeStatusObjAjax->ChangeStatus($record_id,3);	
/* Activate Operation Ends */

/* IsProcess Starts */
	$action_arr=array("list_order","activate","list_paginate","del");
	if(in_array($action,$action_arr))
		$smarty->assign('IsProcess',"Y");
/* IsProcess Ends */

/* listing Operation Starts */	
	
/* Order section starts */			
	$SortingSequenceArr=array(1 => "admin_username", 2 => "date_added", 3 => "date_edited");	
	$ReturnSortingArr=$SortingObjAjax->Sorting("admin_id",$SortingSequenceArr,$do_order,$OrderByID,$OrderType);	
	$OrderBySql=$ReturnSortingArr['OrderBySql'];
	$OrderLink=$ReturnSortingArr['OrderLink'];
	$OrderType=$ReturnSortingArr['OrderType'];	
/* Order section ends */
	
	$SqlSelectAdmin="SELECT * FROM ".TABLEPREFIX."_admin Where admin_type_id !=1 ".$searchSql.$OrderBySql;
	
/* pagination starts */	
	$pagination_arr=$PaginationObjAjax->PaginationAjax($SqlSelectAdmin,$page_name."?action=".$action."&".$SearchLink."&".$OrderLink,"ManagerGeneral");	
/* pagination ends */ 
	
/* preserve update starts */
	$PreserveLink="from=$from&".$SearchLink."&".$OrderLink;
	$preserve_variable_object->PreserveVariableUpdate($PreserveLink);
/* preserve update ends */			
		
	$SelectAdminArr=$adodbcon->GetAll($pagination_arr[0]);	
	$NumSelectAdmin=count($SelectAdminArr);
	for($i=0;$i<$NumSelectAdmin;$i++)
	{		
		$SelectAdminArr[$i]['admin_username'] = stripslashes($SelectAdminArr[$i]['admin_username']);
		$SelectAdminArr[$i]['admin_username_delete'] = strip_quote($SelectAdminArr[$i]['admin_username']);
		$SelectAdminArr[$i]['active_img'] = $SelectAdminArr[$i]['is_active']=="Y" ? "true.gif" : "false.gif";
		$SelectAdminArr[$i]['active_alt'] = $SelectAdminArr[$i]['is_active']=="Y" ? "Active" : "Inactive";					
		$SelectAdminArr[$i]['date_added']=date_format_admin($SelectAdminArr[$i]['date_added']);
		$SelectAdminArr[$i]['date_edited']=date_format_admin($SelectAdminArr[$i]['date_edited']);							
	}
		
/* listing Operation Ends */

/* Current Admin Details Starts */
	$SqlAdminCurrentUpdate = "SELECT * FROM ".TABLEPREFIX."_admin WHERE admin_id=".sql_value($_SESSION['SesGTAdminID']);
	$AdminCurrentUpdateArr=$AdminManagerObjAjax->GetRecords("Row",$SqlAdminCurrentUpdate);
/* Current Admin Details Ends */

	$MessgReportText=displayMessage($messg);

/* Assign smarty Variables Starts */
	$smarty->assign("MessgReportText",$MessgReportText);
	$smarty->assign('page_name',$page_name);
	$smarty->assign('from',$from);
	$smarty->assign('PreserveLink',$PreserveLink);
	$smarty->assign('SearchLink',$SearchLink);
	$smarty->assign('dosearch',$dosearch);
	$smarty->assign("Searchadmin_username",$Searchadmin_username);
	$smarty->assign('OrderLink',$OrderLink);
	$smarty->assign('do_order',$do_order);
	$smarty->assign('OrderType',$OrderType);
	$smarty->assign('OrderByID',$OrderByID);
	$smarty->assign('ReturnSortingArr',$ReturnSortingArr);
	$smarty->assign('pagination_arr',$pagination_arr);
	$smarty->assign('NumSelectAdmin',$NumSelectAdmin);
	$smarty->assign('SelectAdminArr',$SelectAdminArr);
	$smarty->assign('SesAdminTypeID',$_SESSION['SesKITAdminTypeID']);
	$smarty->assign('AdminCurrentUpdateArr',$AdminCurrentUpdateArr);
/* Assign smarty Variables Ends */

	$smarty->display("admin_manager.tpl");
?>