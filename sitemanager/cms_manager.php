<?php
/*******************************************************************************/
	#This page lists all the CMS we have in the table
	#last Updated : June 29 , 2010
/*******************************************************************************/


include('general_include.php');
include"checklogin.php";

$page_name="cms_manager.php";

$action=$_REQUEST['action'];

$IsPreserved=$_REQUEST['IsPreserved'];

if($IsPreserved=="Y")
{
/* preserve select starts */
$PreserveVariableArr=$preserve_variable_object->PreserveVariableSelect();
foreach($PreserveVariableArr as $a=>$b){$$a=$b;}
/* preserve select ends */
}

/* Activate Operation Starts */

if($action=="activate")
{
	$ChangeStatusObjAjax->ChangeStatus($record_id,1);	
}

/* Activate Operation Ends */

/* IsProcess Starts */

$action_arr=array("list_order","ranking","activate","list_paginate","list_search","del");

if(in_array($action,$action_arr))
{
	$smarty->assign('IsProcess',"Y");
}

/* IsProcess Ends */

	/* listing Operation Starts */

	/* search section starts */

	if($dosearch=="GO")
	{ 
		$Search_cms_title = $Search_cms_title=='Search by CMS Title'?'':$Search_cms_title;
		if(!empty($Search_cms_title))
		{
			 $searchSql =  $searchSql." AND cms_title like '%".mysql_quote($Search_cms_title,"N")."%'"; 
		}
		$SearchLink="dosearch=GO&Search_cms_title=$Search_cms_title";
	}	

	/* search section ends */

	/* Order section starts */	

	$SortingSequenceArr=array(1 => "cms_title", 2 => "date_added", 3 => "date_edited");	
	$ReturnSortingArr=$SortingObjAjax->Sorting("cms_order",$SortingSequenceArr,$do_order,$OrderByID,$OrderType);	
	$OrderBySql=$ReturnSortingArr['OrderBySql'];
	$OrderLink=$ReturnSortingArr['OrderLink'];
	$OrderType=$ReturnSortingArr['OrderType'];			

	/* Order section ends */

	$SqlSelectCat="SELECT * FROM ".TABLEPREFIX."_cms Where 1=1 ".$searchSql.$OrderBySql;

	/* pagination starts */	

	$pagination_arr=$PaginationObjAjax->PaginationAjax($SqlSelectCat,$page_name."?action=".$action."&".$SearchLink."&".$OrderLink,"ManagerGeneral");	

	/* pagination ends */ 

	/* preserve update starts */

	$PreserveLink="from=$from&".$SearchLink."&".$OrderLink;

	$preserve_variable_object->PreserveVariableUpdate($PreserveLink);

	/* preserve update ends */	
	
	#Fecth all the cms records from	 database and store in array
    /****************************************************************/
	$SelectCmsArr=$adodbcon->GetAll($pagination_arr[0]);	

	$NumSelectCms=count($SelectCmsArr);

	for($i=0;$i<$NumSelectCms;$i++)
	{				
		$SelectCmsArr[$i]['cms_title'] 				= $SelectCmsArr[$i]['cms_title'];
		$SelectCmsArr[$i]['cms_title_delete'] 		= addslashes(show_to_control($SelectCmsArr[$i]['cms_title']));
		$SelectCmsArr[$i]['date_added']				= date_format_admin($SelectCmsArr[$i]['date_added']);
		$SelectCmsArr[$i]['date_edited']			= date_format_admin($SelectCmsArr[$i]['date_edited']);
	}
	/****************************************************************/

/* listing Operation Ends */

$MessgReportText=displayMessage($messg);

/* Assign Smarty Variables Starts */

$smarty->assign("MessgReportText",$MessgReportText);
$smarty->assign('page_name',$page_name);
$smarty->assign('from',$from);
$smarty->assign('PreserveLink',$PreserveLink);
$smarty->assign('SearchLink',$SearchLink);
$smarty->assign('dosearch',$dosearch);
$smarty->assign("Search_cms_title",$Search_cms_title);
$smarty->assign('OrderLink',$OrderLink);
$smarty->assign('do_order',$do_order);
$smarty->assign('OrderType',$OrderType);
$smarty->assign('OrderByID',$OrderByID);
$smarty->assign('ReturnSortingArr',$ReturnSortingArr);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('NumSelectCms',$NumSelectCms);
$smarty->assign('SelectCmsArr',$SelectCmsArr);

/* Assign Smarty Variables Ends */

$smarty->display("cms_manager.tpl");
?>