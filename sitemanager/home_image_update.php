<?php
/*******************************************************************************/
	#This page is to add/edit Image details
	#last Updated : April 13 , 2010
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$image_id		= $_REQUEST['image_id'];
$banner_title	= $_REQUEST['banner_title'];
$action			= $_REQUEST['action'];
$date			= date("Y-m-d H:i:s");

$image_object->SetImagePath("../uploaded/home_images");

if($action=="trans")
{
	//$is_active	= "Y";
	$is_active 		= $is_active =="Y" ? "Y" : "N";	
	$is_featured 	= $is_featured =="Y" ? "Y" : "N";	
		
	/* Holding Data If Error Starts */

	$FabricArr['image_alt']						= htmlspecialchars(trim($image_alt));	
	$FabricArr['title']						    = htmlspecialchars(trim($title));	
	$FabricArr['description']				    = htmlspecialchars(trim($description));	
	$FabricArr['is_active']						= $is_active;	
	$FabricArr['is_featured']					= $is_featured;	
	$FabricArr['event_id']						= $event_id;
	$FabricArr['user_id']						= $u_id;	
	/*$FabricArr['image_alt']					= $image_alt;*/
	
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
			
	/* Image(thumbnail) check starts */

	$http_image_name=$_FILES['img1']['name'];
	$http_image_size=$_FILES['img1']['size'];
	$http_image_type=$_FILES['img1']['type'];
	$http_image_temp=$_FILES['img1']['tmp_name'];
	$PhotoT1=$_POST['PhotoT1'];
	$imghidden1=$_POST['imghidden1'];
	
	//$err_msgs .=$image_object->CheckImage($PhotoT1,$http_image_name,$http_image_temp,$http_image_type,$http_image_size,$checktype=1,$Photolebel="the Image","Y");	
		$err_msgs .=$image_object->CheckImage($PhotoT1,$http_image_name,$http_image_temp,$http_image_type,$http_image_size,$checktype=1,$Photolebe1="the Image","Y",'',"Y",'',684,304);
	/* Image(thumbnail) check ends */
	$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($title),"Title","EMP",$type="");	
	$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($description),"Description","EMP",$type="");	
	$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($image_alt),"Image Alt","EMP",$type="");	
	if($banner_link!='')
	 $err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($banner_link),"Link","EMP",$type="URL");	
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
	{
		/* Gallery Image Upload Starts */

			$http_image_name = $_FILES['img1']['name'];
			$http_image_temp = $_FILES['img1']['tmp_name'];
		
			$Picc1 = $image_object->UploadImage($PhotoT1,$imghidden1,$http_image_name,$http_image_temp,90,$resize_type=2,'','Y',733,$resize_type=1,'140','Y',684,$resize_type=1,'304');			
		
		/* Gallery Image Upload Ends */
				
		if(empty($image_id))
		{
			
			/*$banner_order = $AdminManagerObjAjax->GetRecords("One","select ifnull(max(banner_order)+1,1) from ".TABLEPREFIX."_image");*/
			
			/* Insert Into Image Starts */

			$table_name = TABLEPREFIX."_home_image ";

			$fields_values = array( 
									'image_file'				=> $Picc1,	
									'image_alt'					=> $image_alt,
									'date_added'  				=> $date,													
									'is_active'  				=> $is_active,
									'title'                     => $title,
									'banner_link'               => $banner_link,
									'description'               => $description,
									'date_added'  				=> date("Y-m-d H:i:s")
							    	);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);

			$image_id=mysql_insert_id();	

			/* Insert Into Image Ends */
			
		}
		elseif(!empty($image_id))
		{
			/* Update Image Starts */

			$table_name = TABLEPREFIX."_home_image ";

			$fields_values = array( 
									'image_file'				=> $Picc1,	
									'image_alt'					=> $image_alt,																
									'is_active'  				=> $is_active,
									'title'                     => $title,
									'banner_link'               => $banner_link,
									'description'               => $description,									
								   );	

			$where="image_id='$image_id' ";										

			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Image Ends */	

		}		
		echo "<script>window.location.href='home_image_manager.php?messg=".$msgreport."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($image_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_home_image WHERE image_id=".$image_id;
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
    
	$FabricArr['is_active']					= $RsCatSql["is_active"];
	$FabricArr['image_alt']					= $RsCatSql["image_alt"];
	$FabricArr['description']               = $RsCatSql["description"];
	$FabricArr['title']                     = $RsCatSql["title"];
	$FabricArr['banner_link']               = $RsCatSql["banner_link"];
	
	
	/* Get Record For Display Ends */

	$SubmitButton="Update Home Image";	
	$flag=1;
}
else
{
	$SubmitButton="Add Home Image";
}

$DisplayImageControlStr=$image_object->DisplayImageControl("Image","img1",$RsCatSql['image_file'],"imghidden1","PhotoT1",$instuctiontype=1,$mandatory="Y","Normal",90);


/* Assign Smarty Variables Starts */


$smarty->assign('image_id',$image_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('DisplayImageControlStr',$DisplayImageControlStr);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("home_image_update.tpl");
?>