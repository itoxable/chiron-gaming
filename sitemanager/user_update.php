<?php
/*******************************************************************************/
	#This page is to add/edit user details
	#last Updated : August 25 , 2011
/*******************************************************************************/
error_reporting(0);
include('general_include.php');
include"checklogin.php";
include('/class/payment_class.php');
$u_id	      = $_REQUEST['u_id'];
$action		  = $_REQUEST['action'];
//echo "userID :- ".$u_id;
function phpbb_email_hash($email)
{
	return sprintf('%u', crc32(strtolower($email))) . strlen($email);
}

function OrderNumberGenerator($tablename,$field)
		{
				$OrderNumber="";
				function make_seed()
				{
				   list($usec, $sec) = explode(' ', microtime());
				   return (float) $sec + ((float) $usec * 100000);
				}
				while(!$OrderNumber)

				{
						mt_srand(make_seed());
						$randval = mt_rand();
						$randvalw=rand(65,90);
						$randvalw=chr($randvalw);
						$OrderNumber=$randvalw."-".$randval;
						$select_number="SELECT * FROM $tablename WHERE $field = '$OrderNumber' ";
						$result_number=mysql_query($select_number) or die("Error In Generating OrderNumber ".mysql_error());
						$num=mysql_num_rows($result_number);
						if($num>0)
							$OrderNumber="";

				}
				return  $OrderNumber;
		}

$image_object->SetImagePath("../uploaded/user_images");
if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	$availability_type = $availability_type =="O" ? "O" : "L";
	//$is_active = $is_active =="Y" ? "Y" : "N";
	if($availability_type=='O')
	{
		$availability_city		= '';
		$availability_country	= '';
	}
	/* Holding Data If Error Starts */
	$FabricArr['name']					= htmlspecialchars(trim($name));
	$FabricArr['email']				    = htmlspecialchars(trim($email));
	$FabricArr['paypal_email_id']		= htmlspecialchars(trim($paypal_email_id));
	$FabricArr['password']				= htmlspecialchars(trim($password));
	$FabricArr['contact_no']			= htmlspecialchars(trim($contact_no));
	$FabricArr['city']				    = trim($city);
	$FabricArr['city_name']             = htmlspecialchars(trim($citynm));
	$FabricArr['state']					= htmlspecialchars(trim($state));
	$FabricArr['user_about']			= htmlspecialchars(trim($user_about));
	$FabricArr['description']			= htmlspecialchars(trim($description));
	$FabricArr['date_of_birth']		    = htmlspecialchars(trim($date_of_birth));
	$FabricArr['skype_id']		        = htmlspecialchars(trim($skype_id));
	$FabricArr['rate']		        	= htmlspecialchars(trim($rate));
	$FabricArr['availability_type1']	= htmlspecialchars(trim($availability_type1));
	$FabricArr['availability_type2']	= htmlspecialchars(trim($availability_type2));
	$FabricArr['availability_city']		= trim($availability_city);
	$FabricArr['acity_name']            = htmlspecialchars(trim($acity_name));
	$FabricArr['description']           = htmlspecialchars(trim($description));
	 $lang_count=count($_POST['language']);
	  for($l=0; $l<$lang_count; $l++)
	  {
	    $language_id.= $language[$l].',';
	  }
	  $language_ids = substr($language_id,0,-1);
	$FabricArr['calendar_code']         = htmlspecialchars(trim($calendar_code));
	$FabricArr['active_calendar']        = $active_calendar;
	    
		
	//$FabricArr['user_type']             = $user_type;
	//count($language);
	/* Holding Data If Error Ends */
	//echo "USERID :--- ".$u_id;
	//print_r($FabricArr);
	//exit;
	/* Error Checking Starts */
    $http_image_name=$_FILES['img1']['name'];
	$http_image_size=$_FILES['img1']['size'];
	$http_image_type=$_FILES['img1']['type'];
	$http_image_temp=$_FILES['img1']['tmp_name'];
	$PhotoT1=$_POST['PhotoT1'];
	$imghidden1=$_POST['imghidden1'];
	
	//echo $date_of_birth;
	$err_msgs="";
	$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($email),"Email","EMP",$type="EMAIL");
	$err_msgs .=$AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_user","email",$email,"Email Address","user_id",$u_id);
	$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($password),"Password","EMP",$type="");
	$err_msgs .=$AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_user","name",$name,"UserID","user_id",$u_id);
	//$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($contact_no),"Contact Number","EMP",$type="PHONE");
	$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($city),"City","EMP",$type="");
	//$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($date_of_birth),"Date of Birth","EMP",$type="");
	//$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($skype_id),"Skype Id","EMP",$type="");
	if($is_coach == 1)
		$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($paypal_email_id),"Paypal Email ID","EMP",$type="EMAIL");
	if($availability_type1 == 'L')
	{
		$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($availability_city),"Available City","EMP",$type="");		
	}
	//$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($registration_phone),"Business Phone","EMP",$type="");
	
	/* Error Checking Ends */	
	//die($err_msgs);
	if(empty($err_msgs))// If Empty Error Starts 
	{
		$http_image_name = $_FILES['img1']['name'];
		$http_image_temp = $_FILES['img1']['tmp_name'];
		//$Picc1 = $image_object->UploadImage($PhotoT1,$imghidden1,$http_image_name,$http_image_temp,140,$resize_type=1,99);
		$Picc1 = $image_object->UploadImage($PhotoT1,$imghidden1,$http_image_name,$http_image_temp,37,$resize_type=5,'37','Y',198,$resize_type=5,172,'Y',57,$resize_type=5,50);
		$uploadfile = "../uploaded/user_images/".$Picc1;
		$mediumimagename = "thumbs/mid_" . $Picc1;
		$image_object->create_gdthumbnail($uploadfile,$mediumimagename,105,5,92);
		 
		 $avatar_path = SITE_URL."/uploaded/user_images/".$Picc1;
	 // updating avatar to forum user //
	 //mysql_query("UPDATE forum_users SET user_avatar='".SITE_URL."/uploaded/user_images/".$Picc1."', user_avatar_type='2', user_avatar_width='57', user_avatar_height='50' WHERE //username_clean='".$email."'");
	 ////			 
		
		if($city!= '') 
			 {
			   $sqlCity = "SELECT * FROM ".TABLEPREFIX."_cities WHERE city_id='$city'";
			   $rowCity = $AdminManagerObjAjax->GetRecords("Row",$sqlCity);
			   $country = $rowCity['country_id'];
			 }
			  if($availability_city != '' && $availability_type1!='') 
			 {
			   $sqlCity = "SELECT * FROM ".TABLEPREFIX."_cities WHERE city_id='$availability_city'";
			   $rowCity = $AdminManagerObjAjax->GetRecords("Row",$sqlCity);
			   $availability_country == $rowCity['country_id'];
			 }     
		
		if(empty($u_id))
		{
     		$PaymentNumber = OrderNumberGenerator(TABLEPREFIX.'_user','registration_number');
			/* Insert Into Sponsor Starts */
			if($active_calendar=='' && $calendar_code==''){
				$active_calendar='N';
				$calendar_code="";
			}
			if($availability_type1!='' && $availability_type2!='')
			  $availability_type = $availability_type1.','.$availability_type2;
			if($availability_type1!='' && $availability_type2=='')   
			  $availability_type = $availability_type1;
			if($availability_type2!='' && $availability_type1=='')   
			  $availability_type = $availability_type2;  
			$table_name = TABLEPREFIX."_user ";
			$fields_values = array( 
									'registration_number'			=> $PaymentNumber,
									'name'				            => $name,
									'email'                         => $email,
									'password'                      => $password,
									'paypal_email_id'				=> $paypal_email_id,
									'contact_no'			        => $contact_no,
									'city'			                => $city,
									'state'				            => $state,
									'country'			            => $country,
									'photo'  	                    => $Picc1,
									'user_about'				    => $user_about,
									'description'			        => $description,
									'language_ids'                  => $language_ids,
									'date_of_birth'			        => $date_of_birth,
									'skype_id'			            => $skype_id,
									'is_active'  					=> $is_active,
									'calendar_code'                 => $calendar_code,
									'active_calendar'               => $active_calendar,
									'rate'			           		=> $rate,
									'availability_type'  			=> $availability_type,
									'availability_city'             => $availability_city,
									'availability_country'          => $availability_country,
									'date_registered'				=> date("Y-m-d H:i:s")
						    	);	
//echo "Here I am <BR />".$table_name."<BR />";	
//print_r($fields_values);
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			$u_id=mysql_insert_id();	
			
			$table = TABLEPREFIX."_user_type_user_relation ";
			$fields =  array(
			                  'user_id'       => $u_id,
							  'user_type_id'  => 2,
							  'date_added'    => date("Y-m-d H:i:s")
							);    
			$report= $AdminManagerObjAjax->InsertRecords($table,$fields);   
			
			/// inserting into forum user table - prosenjit
	$fields = array(
		'username'			=> $name,
		'username_clean'	=> $name,
		'user_password'		=> 'new_password',
		'user_pass_convert'	=> 0,
		'user_email'		=> $email,
		'user_email_hash'	=> phpbb_email_hash($email),
		'group_id'			=> 2,
		'user_type'			=> 0,
		'user_permissions'	=> '',
		'user_timezone'		=> 0.00,
		'user_dateformat'	=> 'D M d, Y g:i a',
		'user_lang'			=> 'en',
		'user_style'		=> 1,
		'user_actkey'		=> '',
		'user_ip'			=> '',
		'user_regdate'		=> time(),
		'user_passchg'		=> time(),
		'user_options'		=> 230271,
		// We do not set the new flag here - registration scripts need to specify it
		'user_new'			=> 0,

		'user_inactive_reason'	=> 0,
		'user_inactive_time'	=> 0,
		'user_lastmark'			=> time(),
		'user_lastvisit'		=> 0,
		'user_lastpost_time'	=> 0,
		'user_lastpage'			=> '',
		'user_posts'			=> 0,
		'user_dst'				=> 0,
		'user_colour'			=> '',
		'user_occ'				=> '',
		'user_interests'		=> '',
		'user_avatar'			=> $avatar_path,
		'user_avatar_type'		=> 2,
		'user_avatar_width'		=> 57,
		'user_avatar_height'	=> 50,
		'user_new_privmsg'		=> 0,
		'user_unread_privmsg'	=> 0,
		'user_last_privmsg'		=> 0,
		'user_message_rules'	=> 0,
		'user_full_folder'		=> -3,
		'user_emailtime'		=> 0,

		'user_notify'			=> 0,
		'user_notify_pm'		=> 1,
		'user_notify_type'		=> 0,
		'user_allow_pm'			=> 1,
		'user_allow_viewonline'	=> 1,
		'user_allow_viewemail'	=> 1,
		'user_allow_massemail'	=> 1,

		'user_sig'					=> '',
		'user_sig_bbcode_uid'		=> '',
		'user_sig_bbcode_bitfield'	=> ''
		);
		$AdminManagerObjAjax->InsertRecords("forum_users",$fields);
		$userid = mysql_insert_id();
		
		
		//mysql_query("insert into forum_user_group set group_id=2, user_id='".$userid."', group_leader=0, user_pending=0");  // inserting into user group table
		//mysql_query("insert into forum_user_group set group_id=7, user_id='".$userid."', group_leader=0, user_pending=0");  // inserting into user group table
		////             
        $redirect_pg = 'user_type.php?u_id='.$u_id;
			/* Insert Into Sponsor Ends */	
		}
		elseif(!empty($u_id))
		{
			
			/* Update Sponsor Starts */
			if($availability_type1!='' && $availability_type2!='')
			  $availability_type = $availability_type1.','.$availability_type2;
			if($availability_type2!='' && $availability_type1=='')
			{   
			  $availability_city ='';
			  $availability_country ='';
			  $availability_type = $availability_type2;
			}  
			if($availability_type1!='' && $availability_type2=='')   
			  $availability_type = $availability_type1;  
			
			$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_user WHERE user_id=".$u_id;
			$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);

			$table_name = TABLEPREFIX."_user ";

			$fields_values = array( 
									'name'				            => $name,
									'email'                         => $email,
									'password'                      => $password,
									'contact_no'			        => $contact_no,
									'paypal_email_id'				=> $paypal_email_id,
									'city'			                => $city,
									'state'				            => $state,
									'country'			            => $country,
									'photo'  	                    => $Picc1,
									'user_about'				    => $user_about,
									'description'			        => $description,
									'language_ids'                  => $language_ids,
									'date_of_birth'			        => $date_of_birth,
									'skype_id'			            => $skype_id,
									'is_active'  					=> $is_active,
									'calendar_code'                 => $calendar_code,
									'active_calendar'               => $active_calendar,
									'rate'			           		=> $rate,
									'availability_type'  			=> $availability_type,
									'availability_city'             => $availability_city,
									'availability_country'          => $availability_country,
									'date_edited'				    => date("Y-m-d H:i:s")
								   );	

			$where="user_id='$u_id' ";										

			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);
			
			// updating forum user table //
			$fields = array(
					'username'=> $name,
					'username_clean'	=> $name,
					'user_password'		=> 'new_password',		
					'user_email'		=> $email,
					'user_email_hash'	=> phpbb_email_hash($email),
					'user_avatar'		=> $avatar_path,
					'user_avatar_type'	=> 2,
					'user_avatar_width'	=> 57,
					'user_avatar_height'=> 50
					);
			$where = "username='".trim($RsCatSql['email'])."'";	
			//$AdminManagerObjAjax->UpdateRecords("forum_users",$fields,$where);
			if($user_type!='')
			{
			   $table_name = TABLEPREFIX."_user_type_user_relation";
			   $fields_val = array(
				   'user_id'      => $u_id,
				   'user_type_id' => $user_type,
				   'date_added'   => date("Y-m-d H:i:s")
			   ); 
			   $AdminManagerObjAjax->InsertRecords($table_name,$fields_val);
			   $redirect_pg = "member_game.php?u_id=".$u_id."&u_type=".$user_type;
			}
			else
			  $redirect_pg = "user_manager.php?messg=".$msgreport."&IsPreserved=Y";
			////

			/* Update Sponsor Ends */	
			

		}		
		
	         echo "<script>window.location.href='$redirect_pg'</script>";		 
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}
$language=array();
if(!empty($u_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_user WHERE user_id=".$u_id;
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
    
	$FabricArr['registration_number'] 	  	= show_to_control($RsCatSql["registration_number"]);
	$FabricArr['name'] 	  		            = show_to_control($RsCatSql["name"]);
	$FabricArr['email'] 	  		        = show_to_control($RsCatSql["email"]);
	$FabricArr['paypal_email_id'] 	  		= show_to_control($RsCatSql["paypal_email_id"]);
	$FabricArr['password'] 	  		        = show_to_control($RsCatSql["password"]);
	$FabricArr['contact_no'] 	  	        = show_to_control($RsCatSql["contact_no"]);
	$FabricArr['city'] 	  		            = show_to_control($RsCatSql["city"]);
	$FabricArr['state'] 	  		        = show_to_control($RsCatSql["state"]);
	$FabricArr['country'] 	  		        = $RsCatSql["country"];
	$FabricArr['date_of_birth'] 	        = show_to_control($RsCatSql["date_of_birth"]);
	$FabricArr['skype_id'] 	  	            = show_to_control($RsCatSql["skype_id"]);
	$FabricArr['is_active']			        = $RsCatSql["is_active"];
	$FabricArr['availability_type']			= $RsCatSql["availability_type"];
	$FabricArr['availability_city']			= $RsCatSql["availability_city"];
	$FabricArr['description']               = $RsCatSql["description"];
	$FabricArr['char_left']                 = 300 - strlen($RsCatSql["description"]);
	if($RsCatSql["availability_country"]!='')
		$FabricArr['availability_country']		= $RsCatSql["availability_country"];
	else
		$FabricArr['availability_country']		= $RsCatSql["country"];
 	$language_id = explode(',',$RsCatSql['language_ids']);
    $language_ids = $language_id;
	$is_coach = 0;
	$is_partner = 0;
	    $typeSql = "SELECT * FROM ".TABLEPREFIX."_user_type_user_relation WHERE user_id='".$u_id."'";
		$typeArr = $adodbcon->GetAll($typeSql);
		for($x=0;$x<count($typeArr);$x++)
		{
		    if($typeArr[$x]['user_type_id'] == 1)
		     $is_coach = 1;
		    if($typeArr[$x]['user_type_id'] == 3)
		     $is_partner = 1;
			 	  
		}
	$sqlCity = "SELECT * FROM ".TABLEPREFIX."_cities WHERE city_id='".$FabricArr['city']."'";
	$sqlArr = $AdminManagerObjAjax->GetRecords("Row",$sqlCity);
	$FabricArr['city_name'] = $sqlArr['city_key'];
	
	$sqlAcity = "SELECT * FROM ".TABLEPREFIX."_cities WHERE city_id='".$FabricArr['availability_city']."'";
	$sqlAcityArr = $AdminManagerObjAjax->GetRecords("Row",$sqlAcity);
	$FabricArr['acity_name'] = $sqlAcityArr['city_key'];
	
	/* Get Record For Display Ends */
     $SubmitButton="Update Profile";	
	 $link="user_manager.php";
      	
	$flag=1;
}
else
{
	  $is_coach = 0;
	  $is_partner = 0;
	  $SubmitButton="Add User";
	  $link="user_manager.php";
  
}


$countrySql = "SELECT country_id,country_name FROM ".TABLEPREFIX."_country ORDER BY country_name";
$CountryArr = $AdminManagerObjAjax->HtmlOptionArrayCreate($countrySql);
//print_r($CountryArr);

$langSql = "SELECT language_id,language_name FROM ".TABLEPREFIX."_language ORDER BY language_name";
$langArr = $AdminManagerObjAjax->GetRecords("All",$langSql);

$DisplayImageControlStr=$image_object->DisplayImageControl("Upload Photo","img1",$RsCatSql['photo'],"imghidden1","PhotoT1",$instuctiontype=1,$mandatory="N","Normal",90);
/* Assign Smarty Variables Starts */

$smarty->assign('is_coach',$is_coach);
$smarty->assign('is_partner',$is_partner);
$smarty->assign('user_type_id',$user_type_id);
$smarty->assign('user_id',$u_id);
$smarty->assign('CountryArr',$CountryArr);
$smarty->assign('langArr',$langArr);
$smarty->assign('language_ids',$language_ids);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('DisplayImageControlStr',$DisplayImageControlStr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('link',$link);
$smarty->assign('flag',$flag);
$smarty->register_modifier("inarray","in_array");

/* Assign Smarty Variables Ends */

$smarty->display("user_update.tpl");
?>