<?php 
/*
TinyBrowser 1.30 - A TinyMCE file browser (C) 2008  Bryn Jones
Default Language 'en'
Author: Laurent Hayoun
email: contact[at]lh2lsoft[dot]com
*/
		define("BROWSE", "Naviguer");
		define("EDIT", "Editer");
		define("UPLOAD", "Upload");
		define("VIEW", "Voir: ");
		define("SHOW", "Montrer: ");
		define("PAGE", "Page");
		define("FILENAME", "Nom");
		define("SIZE", "Taille");
		define("DIMENSIONS", "Dimensions");
		define("TYPE", "Type");
		define("DATE", "Date Modification");
		define("SEARCH", "Rechercher");	
		define("FILES", "Fichiers");	
		define("BROWSEFILES", "Navigateur de fichiers");
		define("DETAILS", "D�tails");
		define("THUMBS", "Vignettes");
		define("EDITFILES", "Editer Fichiers");
		define("ACTION", "Action");
		define("DELETE", "Supprimer");
		define("RENAME", "Renommer");
		define("ROTATE", "Rotation");
		define("RESIZE", "Redimensionner");
		define("UPLOADFILES", "Upload Fichier");
		define("PROGRESS", "Progression");
		define("REMOVE", "Annuler");
		define("BYTES", "bytes");
		define("WIDTH", "longueur");
		define("HEIGHT", "largeur");
		define("NONE", "Non");
		define("TYPEIMAGE", "Images");
		define("TYPEMEDIA", "Media");
		define("TYPEFILE", "Tous Fichiers");
		define("ROTATECW", "Rotation 90 degr�s horaire");
		define("ROTATECCW", "Rotation 90 degr�s anti-horaire");
		define("MSGNEWTHUMBS", "%s nouvelles vignettes cr��es.");
		define("MSGMKDIR", "Le '%s' r�pertoire a �t� cr�� avec succ�s.");
		define("MSGMKDIRFAIL", "Impossible de cr�er le r�pertoire '%s' - v�rifier vos permissions.");
		define("MSGDELETE", "%s fichiers ont �t� supprim�s avec succ�s.");
		define("MSGRENAME", "%s fichiers ont �t� renomm�s avec succ�s.");
		define("MSGRESIZE", "%s fichiers ont �t� redimensionn�s avec succ�s.");
		define("MSGROTATE", "%s images ont �t� modifi�es avec succ�s.");
		define("MSGEDITERR", "%s fichiers en �tat erreur pendant l'op�ration.");
		define("MSGUPGOOD", "%s fichiers ont �t� upload�s avec succ�s.");
		define("MSGUPBAD", "%s fichiers n'ont pas �t� upload�s, � cause des restrictions de s�curit�.");
		define("MSGUPFAIL", "Echec d'pload de fichier - verifier vos permissions pour '%s'.");
		define("MSGMAXSIZE", "ne peut placer en liste d'upload: limite de taille de fichier maximum d�pass�");
		define("TTLMAXSIZE", "Erreur de taille de fichier");
		define("DENIED", "Vous devez poss�der la permission pour voir cette page.");
		define("UPDENIED", "Vous devez poss�der la permission upload pour voir cette page.");
		define("EDDENIED", "Vous devez poss�der la permission edition ou suppression pour voir cette page.");
?>