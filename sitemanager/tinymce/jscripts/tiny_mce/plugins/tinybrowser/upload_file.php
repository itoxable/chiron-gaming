<?php
require_once("config_tinybrowser.php");
require_once("fns_tinybrowser.php");

$ext = end(explode('.',$_FILES['Filedata']['name']));
if(!validateExtension($ext, $tinybrowser['prohibited'])) { exit; }

$folder_name = $tinybrowser['path'][$_REQUEST['type']];

if ($_FILES['Filedata']['tmp_name'] && $_FILES['Filedata']['name'])
	{	
	$source_file = $_FILES['Filedata']['tmp_name'];
	$file_name = stripslashes($_FILES['Filedata']['name']);
	//-- $folder_name is the folder for file uploads
	$tmp_folder = $_REQUEST['folder'];
	if(!is_dir($tinybrowser['docroot'].$folder_name.$tmp_folder))
		{
	   //-- Create the folder to upload the images to
		mkdir($tinybrowser['docroot'].$folder_name.$tmp_folder, $tinybrowser['unixpermissions']);
		chmod($tinybrowser['docroot'].$folder_name.$tmp_folder, $tinybrowser['unixpermissions']);
		}
	copy($source_file,$tinybrowser['docroot'].$folder_name.$tmp_folder.'/'.$file_name);
	}		
?>
