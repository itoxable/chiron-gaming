<?php
require_once("config_tinybrowser.php");
require_once("fns_tinybrowser.php");

// Initialise files array and error vars
$files = array();
$good = 0;
$bad = 0;
$exist = 0;

// Assign get variables
$upload_dir = $tinybrowser['path'][$_GET['type']];
$folder = $tinybrowser['docroot'].$upload_dir.$_GET['folder'].'/';
$passfeid = (isset($_GET['feid']) ? '&feid='.$_GET['feid'] : '');

// Check for problem during upload
if(!file_exists($folder)) 
	{
	// try create folder, to check for permission errors
	$success = createfolder($folder,$tinybrowser['unixpermissions']);
	// if folder created, remove it and redirect (outcome = no files selected for upload)
	if($success)
		{
		rmdir($folder);
		Header('Location: ./upload.php?type='.$_GET['type'].$passfeid);
		}
	// if folder not created, redirect (outcome = permission error)
	else
		{
		Header('Location: ./upload.php?type='.$_GET['type'].$passfeid.'&permerror=1');
		}
	}

if ($handle = opendir($folder))
	{
	while (false !== ($file = readdir($handle)))
		{
		if ($file != "." && $file != "..")
			{
			//-- File Naming
			$tmp_filename = $folder.$file;
			$dest_filename	 = $upload_dir.$file;
        
			//-- Good mime-types (for image file types)
			if($_GET['type']=='image')
				{
				$imginfo = getimagesize($tmp_filename);
				$mime = $imginfo['mime'];
				$types = array('image/jpeg', 'image/png', 'image/gif');
	   		$mime  = validateMimeType($tmp_filename, $types);
	   		if($mime === false) { unlink($tmp_filename); $bad++; continue; }
				}

			//-- Duplicate Files
			$ext = end(explode('.',$tmp_filename));
			if(file_exists($dest_filename)) { unlink($tmp_filename); $bad++; continue; }

			//-- Bad extensions
			$ext = end(explode('.',$tmp_filename));
			if(!validateExtension($ext, $tinybrowser['prohibited'])) { unlink($tmp_filename); $bad++; continue; }
        
			//-- Copy the uploaded file to final destination
			copy($tmp_filename, $tinybrowser['docroot'].$dest_filename);
			//-- Remove our temp file
			unlink($tmp_filename);
			$good++;
			
			//-- if image, perform additional processing
			if($_GET['type']=='image')
				{
				// resize image to maximum height and width, if set
				if($tinybrowser['imageresize']['width'] > 0 || $tinybrowser['imageresize']['height'] > 0)
					{
					$resizeimg = $tinybrowser['docroot'].$upload_dir.$file;

					// assign new width and height values, only if they are less than existing image size
					$widthnew  = ($tinybrowser['imageresize']['width'] < $imginfo[0] ? $tinybrowser['imageresize']['width'] : $imginfo[0]);
					$heightnew = ($tinybrowser['imageresize']['height'] < $imginfo[1] ? $tinybrowser['imageresize']['height'] :  $imginfo[1]);

					// only resize if width or height values are different
					if($widthnew != $imginfo[0] || $heightnew != $imginfo[1])
						{
						$im = convert_image($resizeimg,$mime);
						resizeimage($im,$widthnew,$heightnew,$resizeimg,$tinybrowser['imagequality']);
						imagedestroy($im);
						}
					}


				// generate thumbnail
				$thumbimg = $tinybrowser['docroot'].$upload_dir."thumbs/_".$file;
				if (!file_exists($thumbimg))
					{
					$im = convert_image($tinybrowser['docroot'].$dest_filename,$mime);
					resizeimage	($im,$tinybrowser['thumbsize'],$tinybrowser['thumbsize'],$thumbimg,$tinybrowser['thumbquality']);
					imagedestroy ($im);
					}
				}

      	}
		}
		closedir($handle);
		//-- Remove out temp folder
		rmdir($folder);
	}

Header('Location: ./upload.php?type='.$_GET['type'].$passfeid.'&badfiles='.$bad.'&goodfiles='.$good);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>TinyBrowser :: Process Upload</title>
	</head>
	<body>
		<p>You won't see this.</p>
	</body>
</html>
