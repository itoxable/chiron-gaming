<?php
/****************************************************************/
	#This is the common page to be included in each admin file
	#last Updated : June 29 , 2010
/****************************************************************/

session_start();
ini_set("memory_limit","30000M");

//Disabling magic quotes at runtime
/********************************************/
if (get_magic_quotes_gpc()) {

    function stripslashes_deep($value)
    {
        $value = is_array($value) ?
                    array_map('stripslashes_deep', $value) :
                    stripslashes($value);
        return $value;
    }

    $_POST = array_map('stripslashes_deep', $_POST);
    $_GET = array_map('stripslashes_deep', $_GET);
    $_COOKIE = array_map('stripslashes_deep', $_COOKIE);
    $_REQUEST = array_map('stripslashes_deep', $_REQUEST);
}
/********************************************/

//Include required classes
/***********************************************/
include("../include/admin_smarty.php");
include("../include/config.php");
include("../include/general_functions.php");
include("../include/registerglobal2general.php");
include("../include/message_config.php");
include("../class/validation_class.php");
include("../class/pagination_class_ajax.php");
include("../class/change_status_class.php");
include("../class/ranking_class.php");
include("../class/sorting_class.php");
include("../class/document_class.php");
include("../class/class.phpmailer.php");
include("../class/image_class.php");
include("../class/preserve_variable_class.php");
include("../class/adminmanager_class.php");
include("../include/setorder.php");
include("../class/video_class.php");
include("spaw/spaw.inc.php"); 
/***********************************************/

//Document object creation
$document_object=new DocumentClass();	
	
//Image object creation
$image_object=new ImageClass($create_type,100,1);

//Video object creation
$video_object = new VideoClass();

/* Pagination Object Creation Starts */	
$item_per_page_selected=10;
$PaginationObjAjax=new PaginationClassAjax($item_per_page_selected,"bluelinks",$adodbcon);
/* Pagination Object Creation Ends */

/* Status Change Object Creation Starts */	
$ChangeStatusObjAjax=new ChangeStatusClass(TABLEPREFIX."_change_status",$adodbcon);
/* Status Change Object Creation Ends */

/* Ranking Object Creation Starts */	
$RankingObjAjax=new RankingClass(TABLEPREFIX."_rankingpages","images","arrow_up.gif","arrow_down.gif",$adodbcon);
/* Ranking Object Creation Ends */

/* Sorting Object Creation Starts */	
$SortingObjAjax=new SortingClass;
/* Sorting Object Creation Ends */

/* Status Admin Manager Object Creation Starts */	
$AdminManagerObjAjax=new AdminManagerClass($adodbcon);
/* Status Admin Manager Object Creation Ends */

/* Image Object Creation Starts */	
define('CHMOD_FILES', 0777);

$create_type="GDThumbnail"; /* Normal or GDThumbnail or NETPBMThumbnail */
$image_object=new ImageClass($create_type,100,1);
/* Image Object Creation Ends */	

/* Preserve Variable Object Creation Starts */	
$preserve_variable_object=new PreserveVariableClass(TABLEPREFIX."_preserve_variable",$adodbcon);
/* Preserve Variable Object Creation Ends */	


/* Validation Class Object Creation Starts*/
$validation	= new Validator();
/* Validation Class Object Creation Ends*/
define('PAYPAL_ID', 1);
define('PAYPAL_URL', 2);
define('FACEBOOK_URL', 3);
define('TWITTER_ID', 4);
?>