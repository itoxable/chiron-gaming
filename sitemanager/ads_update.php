<?php
/*******************************************************************************/
	#This page is to add/edit Image details
	#last Updated : April 13 , 2010
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$action			= $_REQUEST['action'];
$date			= date("Y-m-d H:i:s");

$image_object->SetImagePath("../uploaded/ads_images");

$FabricArr['ads_start_date']				= 'Start Date';	
$FabricArr['ads_end_date']					= 'End Date';	

if($action=="trans")
{
	//$is_active	= "Y";
	$is_active 		= $is_active =="Y" ? "Y" : "N";	
	//$is_featured 	= $is_featured =="Y" ? "Y" : "N";	
		
	/* Holding Data If Error Starts */

	$FabricArr['ads_title']						= addslashes(trim($ads_title));	
	$FabricArr['ads_type']					    = $ads_type;	
	$FabricArr['ads_code']				    	= addslashes(trim($ads_code));	
	$FabricArr['ads_link']				    	= addslashes(trim($ads_link));	
	$FabricArr['ads_position']					= $ads_position;
	$FabricArr['ads_start_date']				= $ads_start_date;	
	$FabricArr['ads_end_date']					= $ads_end_date;	
	$FabricArr['is_active']						= $is_active;	
	
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
			
	/* Image(thumbnail) check starts */
	if($_REQUEST['ads_type']==2){
		$http_image_name=$_FILES['img1']['name'];
		$http_image_size=$_FILES['img1']['size'];
		$http_image_type=$_FILES['img1']['type'];
		$http_image_temp=$_FILES['img1']['tmp_name'];
		$PhotoT1=$_POST['PhotoT1'];
		$imghidden1=$_POST['imghidden1'];
	
		$err_msgs .=$image_object->CheckImage($PhotoT1,$http_image_name,$http_image_temp,$http_image_type,$http_image_size,$checktype=1,$Photolebe1="Banner","Y",'',"Y",'',300,300);
	}
	/* Image(thumbnail) check ends */
	/*$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($ads_title),"Title","EMP",$type="");*/	
	$err_msgs .=$AdminManagerObjAjax->Validate($ads_type,"Type","EMP",$type="");	
	$err_msgs .=$AdminManagerObjAjax->Validate($ads_position,"Position","EMP",$type="");
	$err_msgs .=$AdminManagerObjAjax->Validate($ads_start_date,"Validity Start Date","EMP",$type="");	
	$err_msgs .=$AdminManagerObjAjax->Validate($ads_end_date,"Validity End Date","EMP",$type="");	
	if($_REQUEST['ads_type']==1){	
		$err_msgs .=$AdminManagerObjAjax->Validate(strip_tags($ads_code),"Code","EMP",$type="");	
	}
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
	{
		/* Gallery Image Upload Starts */
		if($_REQUEST['ads_type']==2){
			$http_image_name = $_FILES['img1']['name'];
			$http_image_temp = $_FILES['img1']['tmp_name'];
		
			$Picc1 = $image_object->UploadImage($PhotoT1,$imghidden1,$http_image_name,$http_image_temp,256,$resize_type=2,'','Y',256,$resize_type=1,'256','Y',300,$resize_type=1,'300');			
		}else{
			$Picc1 = '';
		}
		/* Gallery Image Upload Ends */
				
		if(empty($ads_id))
		{
			
			/* Insert Into Image Starts */

			$table_name = TABLEPREFIX."_ads";

			$fields_values = array( 
									/*'ads_title'					=> htmlspecialchars(trim($ads_title)),*/
									'ads_type'                  => $ads_type,
									'ads_code'                  => addslashes(trim($ads_code)),
									'ads_position'              => $ads_position,
									'ads_banner'				=> $Picc1,
									'ads_link'					=> $ads_link,
									'ads_start_date'			=> $ads_start_date,
									'ads_end_date'				=> $ads_end_date,	
									'is_active'  				=> $is_active,
									'date_added'  				=> date("Y-m-d H:i:s")
							    	);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);

			$ads_id=mysql_insert_id();	

			/* Insert Into Image Ends */
			
		}
		elseif(!empty($ads_id))
		{
			/* Update Image Starts */

			$table_name = TABLEPREFIX."_ads";

			$fields_values = array( 
									/*'ads_title'					=> htmlspecialchars(trim($ads_title)),*/
									'ads_type'                  => $ads_type,
									'ads_code'                  => addslashes(trim($ads_code)),
									'ads_position'              => $ads_position,
									'ads_banner'				=> $Picc1,	
									'ads_link'					=> $ads_link,
									'ads_start_date'			=> $ads_start_date,
									'ads_end_date'				=> $ads_end_date,	
									'is_active'  				=> $is_active,
									'date_edited'  				=> date("Y-m-d H:i:s")
							    	);		

			$where="ads_id='$ads_id' ";										

			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Image Ends */	

		}		
		echo "<script>window.location.href='ads_manager.php?messg=".$msgreport."&IsPreserved=Y'</script>";
		exit; 
		
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($ads_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_ads WHERE ads_id=".$ads_id;
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	
	$FabricArr['ads_id']					= $RsCatSql["ads_id"];
	$FabricArr['ads_start_date']			= $RsCatSql["ads_start_date"];
	$FabricArr['ads_end_date']				= $RsCatSql["ads_end_date"];
	$FabricArr['ads_link']				   	= addslashes(trim($RsCatSql["ads_link"]));
	$FabricArr['ads_code']				   	= addslashes(trim($RsCatSql["ads_code"]));	
	$FabricArr['ads_type']               	= $RsCatSql["ads_type"];
	$FabricArr['ads_position']              = $RsCatSql["ads_position"];
	$FabricArr['is_active']					= $RsCatSql["is_active"];
	
	/* Get Record For Display Ends */

	$SubmitButton="Update Ads";	
	$flag=1;
}
else
{
	$SubmitButton="Add Ads";
}

$DisplayImageControlStr=$image_object->DisplayImageControl("Banner","img1",$RsCatSql['ads_banner'],"imghidden1","PhotoT1",$instuctiontype=1,$mandatory="Y","Normal",90);


/* Assign Smarty Variables Starts */


$smarty->assign('ads_id',$ads_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('DisplayImageControlStr',$DisplayImageControlStr);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("ads_update.tpl");
?>