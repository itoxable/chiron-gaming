<?php
/*******************************************************************************/
	#This page lists all the Rating we have in table
	#last Updated : August 25 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$page_name="rating_manager.php";
$game_id = $_REQUEST['game_id'];
$action=$_REQUEST['action'];
$id=$_REQUEST['id'];

$IsPreserved=$_REQUEST['IsPreserved'];

if($IsPreserved=="Y")
{
/* preserve select starts */
$PreserveVariableArr=$preserve_variable_object->PreserveVariableSelect();
foreach($PreserveVariableArr as $a=>$b){$$a=$b;}
/* preserve select ends */
}

/* Activate Operation Starts */
if($action=="activate")
{
	$ChangeStatusObjAjax->ChangeStatus($record_id,14);	
}
/* Activate Operation Ends */
/* Ranking Setting Starts */
//$RankingObjAjax->SetPageDetails($page_name,1);	
/* Ranking Setting Ends */

/* Delete Operation Starts */

if($action=="del")
{	
	if(!empty($delete_id))
	{
	
	    /*   Set order After delete Starts  */
		//$RankingObjAjax->RankingDelete($delete_id,'Y',$game_id);			
		/*   Set order After delete Ends  */
		/* Delete Starts */
		$SqlDelete="DELETE FROM ".TABLEPREFIX."_game_rating WHERE rating_id='$delete_id'";
		$ResultDelete=$adodbcon->Execute($SqlDelete);		
		/* Delete Ends */

		if($adodbcon->ErrorNo()) 
			$messg = 5;
		else 
			$messg = 6;
	}
	else
	{
		$messg = 7;
	}		
}

/* Delete Operation Ends */

/* Ranking Operation Starts */

//$RankingObjAjax->SetRankingMaxOrderNo("select ifnull(max(ladder_order),1) from ".TABLEPREFIX."_game_rating where game_id=".$game_id);
if($action=="ranking")
	$RankingObjAjax->Ranking($to,$id,'Y',$game_id);		
		
/* Ranking Operation Ends */


/* IsProcess Starts */

$action_arr=array("list_order","ranking","activate","list_paginate","list_search","del","default");

if(in_array($action,$action_arr))
{
	$smarty->assign('IsProcess',"Y");
}

/* IsProcess Ends */

	/* listing Operation Starts */

	/* search section starts */

	if($dosearch=="GO")
	{ 
		$Search_rating_title = $Search_rating_title=='Search by Rating Name'?'':$Search_rating_title;
		if(!empty($Search_image_title))
		{
			 $searchSql =  $searchSql." AND rating_title like '%".mysql_quote($Search_rating_title,"N")."%'"; 
		}
		$SearchLink="dosearch=GO&Search_rating_title=$Search_rating_title";
	}	

	/* search section ends */

	/* Order section starts */	

	$SortingSequenceArr=array(1 => "rating_title");	
	$ReturnSortingArr=$SortingObjAjax->Sorting("rating_id",$SortingSequenceArr,$do_order,$OrderByID,$OrderType);	
	$OrderBySql=$ReturnSortingArr['OrderBySql'];
	$OrderLink=$ReturnSortingArr['OrderLink'];
	$OrderType=$ReturnSortingArr['OrderType'];			

	/* Order section ends */

	$SqlSelectCat="SELECT * FROM ".TABLEPREFIX."_game_rating Where game_id=".$game_id.$searchSql.$OrderBySql;

	/* pagination starts */	

	$pagination_arr=$PaginationObjAjax->PaginationAjax($SqlSelectCat,$page_name."?action=".$action."&".$SearchLink."&".$OrderLink,"ManagerGeneral");	

	/* pagination ends */ 

	/* preserve update starts */

	$PreserveLink="from=$from&".$SearchLink."&".$OrderLink;

	$preserve_variable_object->PreserveVariableUpdate($PreserveLink);

	/* preserve update ends */		


	#Fetch all project gallery and store them in an array
	$SelectCmsArr=$adodbcon->GetAll($pagination_arr[0]);	

	$NumSelectCms=count($SelectCmsArr);

	for($i=0;$i<$NumSelectCms;$i++)
	{				
		$SelectCmsArr[$i]['rating_title'] 		        = $SelectCmsArr[$i]['rating_title'];
		$SelectCmsArr[$i]['rating_title_delete']         = addslashes(show_to_control($SelectCmsArr[$i]['rating_title']));
		$SelectCmsArr[$i]['active_img'] 				= $SelectCmsArr[$i]['is_active']=="Y" ? "true.gif" : "false.gif";
		$SelectCmsArr[$i]['active_alt'] 				= $SelectCmsArr[$i]['is_active']=="Y" ? "Active" : "Inactive";
		//$ladder_order 								    = $SelectCmsArr[$i]['ladder_order'];
		$game_id 										= $SelectCmsArr[$i]['game_id'];



	   // $SelectCmsArr[$i]['img_rank']=(!empty($dosearch)||!empty($do_order))? "--":$RankingObjAjax->RankingRowCreate($ladder_order,$SelectCmsArr[$i]['rating_id'],"game_id=$game_id");
		
	}

/* listing Operation Ends */

$MessgReportText=displayMessage($messg);

#Getting project title for this gallery
$CategoryTitle = $AdminManagerObjAjax->GetRecords("One","SELECT game_name FROM ".TABLEPREFIX."_game WHERE game_id=".$game_id);	

/* Assign Smarty Variables Starts */

$smarty->assign("CategoryTitle",$CategoryTitle);
$smarty->assign("game_id",$game_id);
$smarty->assign("MessgReportText",$MessgReportText);
$smarty->assign('page_name',$page_name);
$smarty->assign('from',$from);
$smarty->assign('PreserveLink',$PreserveLink);
$smarty->assign('SearchLink',$SearchLink);
$smarty->assign('dosearch',$dosearch);
$smarty->assign("Search_rating_title",$Search_rating_title);
$smarty->assign('OrderLink',$OrderLink);
$smarty->assign('do_order',$do_order);
$smarty->assign('OrderType',$OrderType);
$smarty->assign('OrderByID',$OrderByID);
$smarty->assign('ReturnSortingArr',$ReturnSortingArr);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('NumSelectCms',$NumSelectCms);
$smarty->assign('SelectCmsArr',$SelectCmsArr);

/* Assign Smarty Variables Ends */

$smarty->display("rating_manager.tpl");
?>