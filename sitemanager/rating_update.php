<?php
/*******************************************************************************/
			#This page is to add/edit Rating details
			#last Updated : August 25 , 2011
/*******************************************************************************/

include('general_include.php');
include"checklogin.php";

$game_id =$_REQUEST['game_id'];
$rating_id =$_REQUEST['rating_id'];
$rating_title=$_REQUEST['rating_title'];
$action=$_REQUEST['action'];
if($action=="trans")
{
	$is_active = $is_active =="Y" ? "Y" : "N";
	/* Holding Data If Error Starts */

	$FabricArr['rating_title']		 = htmlspecialchars(trim($rating_title));	
	$FabricArr['is_active']			 = $is_active;
	/* Holding Data If Error Ends */

	/* Error Checking Starts */

	$err_msgs="";
	
	$err_msgs .= $AdminManagerObjAjax->DuplicateCheck(TABLEPREFIX."_game_rating","rating_title",$rating_title,"Rating name","rating_id",$rating_id,"game_id",$game_id);
	
	/* Error Checking Ends */	

	if(empty($err_msgs))// If Empty Error Starts 
		
	{
			
		if(empty($rating_id))
		{
		 	//$Rating_order = $AdminManagerObjAjax->GetRecords("One","select ifnull(max(Rating_order)+1,1) from ".TABLEPREFIX."_game_rating WHERE game_id='".$game_id."'");
			
			$table_name = TABLEPREFIX."_game_rating ";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'rating_title'					    => $rating_title,
									'is_active'  						=> $is_active,
									'date_added' 						=> date("Y-m-d H:i:s")
									);		
			
			$msgreport= $AdminManagerObjAjax->InsertRecords($table_name,$fields_values);
			$rating_id=mysql_insert_id();	

			/* Insert Into Events Ends */
			
		}
		else if(!empty($rating_id))
		{
			/* Update Events Starts */

			$table_name = TABLEPREFIX."_game_rating";

			$fields_values = array( 
									'game_id'							=> $game_id,
									'rating_title'					    => $rating_title,								
									'is_active'  						=> $is_active,
									'date_edited' 						=> date("Y-m-d H:i:s")
									);

			$where="rating_id='$rating_id'";										
			$msgreport= $AdminManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);

			/* Update Events Ends */	
		}		
		echo "<script>window.location.href='rating_manager.php?messg=".$msgreport."&game_id=".$game_id."&IsPreserved=Y'</script>";
		exit; 
	
	}// If Empty Error Ends 
	else
	{
		$smarty->assign('err_msgs',$err_msgs);
	}
}

if(!empty($rating_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_game_rating WHERE rating_id='".$rating_id."' AND game_id='".$game_id."'";
	$RsCatSql = $AdminManagerObjAjax->GetRecords("Row",$SelectCatSql);
	$FabricArr['rating_title'] 				= show_to_control($RsCatSql["rating_title"]);
	$FabricArr['date_edited'] 				= $RsCatSql["date_edited"];
	$FabricArr['date_added'] 				= $RsCatSql["date_added"];
	$FabricArr['is_active']					= $RsCatSql["is_active"];

	/* Get Record For Display Ends */

	$SubmitButton="Update Rating";	
	$flag=1;
}
else
{
	$SubmitButton=" Add Rating ";
}


/* Assign Smarty Variables Starts */

$smarty->assign('game_id',$game_id);
$smarty->assign('rating_id',$rating_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('flag',$flag);

/* Assign Smarty Variables Ends */

$smarty->display("rating_update.tpl");
?>