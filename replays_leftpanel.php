<?php
/*
// Availability //
$availSql = "SELECT count(u.user_id) FROM ".TABLEPREFIX."_user u,".TABLEPREFIX."_user_type_user_relation ur,".TABLEPREFIX."_user_game ug WHERE u.availability_type='O' AND ur.user_type_id=1 AND ur.user_id= u.user_id AND ug.user_id=u.user_id AND ug.game_id='$game_id'";
$avail1Arr = $UserManagerObjAjax->GetRecords("Row",$availSql);
$avail1Num = $avail1Arr[0];

$availSql = "SELECT count(u.user_id) FROM ".TABLEPREFIX."_user u,".TABLEPREFIX."_user_type_user_relation ur,".TABLEPREFIX."_user_game ug WHERE u.availability_type='L' AND ur.user_type_id=2 AND ur.user_id = u.user_id AND ug.user_id=u.user_id AND ug.game_id='$game_id'";
$avail2Arr = $UserManagerObjAjax->GetRecords("Row",$availSql);
$avail2Num = $avail2Arr[0];

// Language //
$languageSql = "SELECT lng.language_id AS language_id, lng.language_name AS language_name, count( lng.language_id ) AS cnt
FROM ".TABLEPREFIX."_language lng
JOIN ".TABLEPREFIX."_user st ON FIND_IN_SET( lng.language_id, st.language_ids )
AND lng.is_active = 'Y'
AND FIND_IN_SET( lng.language_id, (
SELECT u.language_ids
FROM ".TABLEPREFIX."_user u, ".TABLEPREFIX."_user_type_user_relation ur
WHERE u.is_active = 'Y'
AND ur.user_type_id =1
AND ur.user_id = u.user_id
AND u.user_id = st.user_id
) )
GROUP BY lng.language_id, lng.language_name
ORDER BY lng.language_name ASC ";
$languageArr = $UserManagerObjAjax->GetRecords("All",$languageSql);
$Numlanguage = count($languageArr);
*/

$smarty->assign('Numladder',$Numladder);
$smarty->assign('ladderArr',$ladderArr);
$smarty->assign('Numrace',$Numrace);
$smarty->assign('raceArr',$raceArr);
$smarty->assign('Numserver',$Numserver);
$smarty->assign('serverArr',$serverArr);
$smarty->assign('Numregion',$Numregion);
$smarty->assign('regionArr',$regionArr);
$smarty->assign('Numrating',$Numrating);
$smarty->assign('ratingArr',$ratingArr);

$smarty->assign('is_ladder',$is_ladder);
$smarty->assign('is_versus',$is_versus);
$smarty->assign('is_race',$is_race);
$smarty->assign('is_champion',$is_champion);
$smarty->assign('is_class',$is_class);
$smarty->assign('is_map',$is_map);
$smarty->assign('is_mode',$is_mode);
$smarty->assign('is_team',$is_team);
$smarty->assign('is_type',$is_type);

$smarty->assign('is_server',$is_server);
$smarty->assign('is_region',$is_region);
$smarty->assign('is_rating',$is_rating);

$smarty->assign('avail1Num',$avail1Num);
$smarty->assign('avail2Num',$avail2Num);
$smarty->assign('Numlanguage',$Numlanguage);
$smarty->assign('languageArr',$languageArr);
$smarty->display('replays_leftpanel.tpl');   

?>