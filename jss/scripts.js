function getGMTTime(){
	var d = new Date();
	var localtime = d.getTime();
	var offset = d.getTimezoneOffset()*60000;
	return localtime + offset; 
}


function showLoading(ladingselector,overlayselector){
    jQuery(ladingselector).fadeIn('fast');
    jQuery(overlayselector).fadeIn('slow');
}
function hideLoading(ladingselector,overlayselector){
    jQuery(ladingselector).fadeOut('fast');
    jQuery(overlayselector).fadeOut('slow');
}

function setToLocalTime(gmtTime){

	var d = new Date();
	var localtime = d.getTime();
	var offset = d.getTimezoneOffset()*60000;
	return localtime + offset; 
}

function checkWithdrawForm(){
    jQuery('#FormErrorMsg').empty();
    var quant = jQuery('#quantity').val();
    if(quant == ""){
        jQuery('#FormErrorMsg').text("You must set the amount of points");
        //jQuery('#FormErrorMsg').show("fast");
        return false;
    }
	if(jQuery('#paypal').val() == ""){
        jQuery('#FormErrorMsg').text("You must set your Paypal ID");
        return false;
    }else if(!jQuery("#paypal").emailCheck()){
        jQuery('#FormErrorMsg').text("Invalid email");
        return false;
    }else if(jQuery("#txtCaptcha").val()==""){	
        jQuery("#txtCaptcha").attr('style','border:1px solid #FF0000');
        return false;
    }     
    var txtCaptcha = jQuery('#txtCaptcha').val();
    var pass = jQuery('#password').val();
    if(pass == ""){
        jQuery('#FormErrorMsg').text("You must insert your password");
        //jQuery('#FormErrorMsg').show("fast");
        return false;
    }else{
        jQuery.post('actionService.php', {"method": "validateWithdraw","key":pass,"quant":quant,"txtCaptcha":txtCaptcha},function(data){
         
            if(data.IsSuccess){
                jQuery.post('actionService.php', {"method": "doWithdraw","quant":quant,"paypalId":jQuery('#paypal').val(),"totalpoints":data.totalPoints},function(data){
                    if(data.IsSuccess)
                        showWarning("done", '', "window.location.reload()");
                },'json' );
            }else{
                document.getElementById('captcha').src='/captcha.php?'+Math.random();
                jQuery('#FormErrorMsg').text(data.message); 
            }
         },'json' );
         
    }
}
    
function getCookie(id){
	stat_str=Get_Cookie(id);
	return stat_str;
}

function setCookie(name,value){
	Set_Cookie(name,value);
}

function addAsFriend(event,user_id, username,showWarning){
	event.stopPropagation();
	jQuery.post("actionService.php",{"method": "addAsFriend", "userid": user_id}, function(data){
				var message;
				if(data.IsSuccess){
					jQuery("#"+user_id+"_friendStar").removeClass('star_not_friend').addClass('star_friend');
					message = "User "+username+" is now in your friend list";
				}else{
					message = "Something went wrong, please try again later";
				}
                                if(showWarning)
                                    showWarning(message);
			}
	,"json");
}
function removeFriend(event,user_id, username,showWarning){
	event.stopPropagation();
	jQuery.post("actionService.php",{"method": "removeFriend", "userid": user_id}, function(data){
				var message;
				if(data.IsSuccess){
					jQuery("#"+user_id+"_friendStar").removeClass('star_friend').addClass('star_not_friend');
					message = "User "+username+" is now out of your friend list";
				}else{
					message = "Something went wrong, please try again later";
				}
                                if(showWarning)
                                    showWarning(message);
			}
	,"json");
}

function setPhonoSessionId(sessionId){
	jQuery.post("actionService.php",{"method": "addPhonoSessionId","sessionId":sessionId}, function(data){
//		showWarning(data.Msg);
	},"json");
}

function getPhonoSessionId(userId){
	jQuery.post("actionService.php",{"method": "getPhonoSessionId","userId":userId}, function(data){
			
	},"json");
}

function friendActionClick(event,user_id, username){
	event.stopPropagation();
	window.location.href = 'http://noobskool.net/newsite/coachdetails.php?coach_id='+user_id;
}
function filterFriend(event,letter){
	event.stopPropagation();
	if(letter == 'All'){
		jQuery('[class^="players_link_"]').fadeOut('fast',function(){});
		jQuery('[class^="players_link_"]').fadeIn('fast');
		return;
	}
	
	var select = 'players_link_'+letter;
	jQuery('[class^="players_link_"]').fadeOut('fast',function(){});
	jQuery('.'+select).fadeIn('fast');
	
}
function submitloginbyenter(formid,myfield, e, dec){
	var key;
	var keychar;
	if (window.event)
            key = window.event.keyCode;
	else if (e)
            key = e.which;
	else
            return true;		
	if (key==13)
	{		
		GetLogin('frmLogin');		
	}
    return true;	
}

function submit_frm(){	
	GetLogin('frmLogin');			
}

function open_lostPwd(){
  jQuery('#overlay').fadeIn("fast",function(){
		var width = jQuery("#lostPwd").width();
		jQuery("#lostPwd").css('margin-left', -(width/2)); 
		jQuery('#lostPwd').fadeIn("fast");
	});
}  

function submitenter(formid,myfield, e, dec)
{
	var key;
	var keychar;
	if (window.event)
	key = window.event.keyCode;
	else if (e)
	key = e.which;
	else
	return true;		
	if (key==13)
	{		
		GetPwd('frmLostpwd',function(){jQuery('#overlay').fadeOut('fast')});		
	}	
}
function Get_pwd(){	
  GetPwd('frmLostpwd',function(){jQuery('#overlay').fadeOut('fast')});	
}

function openLogin(dest){
           jQuery.post("/security_control.php", {"ctrl":dest},function(data){},"json");
	  jQuery('#overlay').fadeIn("fast",function(){
		var width = jQuery("#loginwindow").width();
		jQuery("#loginwindow").css('margin-left', -(width/2)); 
		jQuery('#loginwindow').fadeIn("fast",function(){
			jQuery('#email').focus(function(){ if( jQuery(this).val()== 'Enter Your Email' ) jQuery(this).val('');});
			jQuery('#email').blur(function(){ if( jQuery(this).val()== '' ) jQuery(this).val('Enter Your Email');});
			jQuery('#password').focus(function(){ if( jQuery(this).val()== 'password' ) jQuery(this).val('');});
			jQuery('#password').blur(function(){ if( jQuery(this).val()== '' ) jQuery(this).val('password');});
			jQuery('#email_id').focus(function(){ if( jQuery(this).val()== 'Enter Your Email' ) jQuery(this).val('');});
			jQuery('#email_id').blur(function(){ if( jQuery(this).val()== '' ) jQuery(this).val('Enter Your Email');});
		});
		
	  });
}
function openReview(title){
    
    jQuery('#overlay').fadeIn("fast",function(){
        jQuery("#boxtitle").html(title);
        var width = jQuery("#reviewwindow").width();
        jQuery("#reviewwindow").css('margin-left', -(width/2)); 
        jQuery('#reviewwindow').fadeIn("fast");
    });
}

function openRegister(){
         clearFields();
	 jQuery('#overlay').fadeIn("fast",function(){
		var width = jQuery("#registerwindow").width();
		jQuery("#registerwindow").css('margin-left', -(width/2)); 
		jQuery('#registerwindow').fadeIn("fast",function(){
			document.getElementById('captcha').src='/captcha.php?'+Math.random();
		});
	 });
}

function CheckFields(){
		  
	if(jQuery("#register-email").val()=="")
		 jQuery("#register-email").attr('style','border:1px solid #FF0000');
	if(jQuery("#username").val()=="")
		 jQuery("#username").attr('style','border:1px solid #FF0000');
	if(jQuery("#password1").val()=="")
		 jQuery("#password1").attr('style','border:1px solid #FF0000');
	if(jQuery("#password2").val()=="")
		 jQuery("#password2").attr('style','border:1px solid #FF0000');
	if(jQuery("#txtCaptcha").val()=="")	
		 jQuery("#txtCaptcha").attr('style','border:1px solid #FF0000');
	 
	if(!jQuery("#register-email").emailCheck() || document.getElementById('register-email').value=='' ) {
	
		jQuery('#FormErrorMsg').show();
		jQuery('#FormErrorMsg').html("Please Enter a Valid email");
		jQuery("#register-email").attr('style','border:1px solid #FF0000');
		return false;
	}
	if(document.getElementById('username').value==''){
		jQuery('#FormErrorMsg').show();
		jQuery('#FormErrorMsg').html("Please enter your UserID");
		jQuery("#name").attr('style','border:1px solid #FF0000');
		return false;
	}
	if(document.getElementById('password1').value=='' ) {
	
		jQuery('#FormErrorMsg').show();
		jQuery('#FormErrorMsg').html("Please Enter a password");
		jQuery("#password1").attr('style','border:1px solid #FF0000');
		return false;
	}
	else
	{
		if(jQuery('#password1').val().length<7){
		 jQuery('#FormErrorMsg').show();
		 jQuery('#FormErrorMsg').html("Password Minimum <b>7 Characters</b>");
		 jQuery("#password1").attr('style','border:1px solid #FF0000');
		 return false;
		} 
	}
	if(document.getElementById('password2').value=='' ) {
		jQuery('#FormErrorMsg').show();
		jQuery('#FormErrorMsg').html("Please Enter confirm password");
		jQuery("#password2").attr('style','border:1px solid #FF0000');
		return false;
	}
	if(document.getElementById('password2').value!=document.getElementById('password1').value){
		jQuery('#FormErrorMsg').show();
		jQuery('#FormErrorMsg').html("Password doesn't match");
		jQuery("#password2").attr('style','border:1px solid #FF0000');
		return false;
	}
	register();
}

function clearFields(){
	var form = document.getElementById('registerForm').reset();

}

function register(){
    var url = "/registration.php";	
    var params = jQuery('#registerForm').serializeArray();
//    jQuery('#regloading').fadeIn('fast');
//    jQuery('#regoverlay').fadeIn('slow');
    showLoading('#regloading','#regoverlay');
    jQuery.post("/forum/ucp.php?mode=registe showLoading('#regloading','#regoverlay');r", params,function(data){
        if(!data.IsSuccess){
            jQuery('#regloading').fadeOut('fast');
            jQuery('#regoverlay').fadeOut('fast');
            
            jQuery('#FormErrorMsg').html(data.Message);
            jQuery('#FormErrorMsg').show();
	}else{
            jQuery.post(url, params,function(data){
                if(!data.IsSuccess){
//                    jQuery('#regloading').fadeOut('fast');
//                    jQuery('#regoverlay').fadeOut('fast');
                    hideLoading('#regloading','#regoverlay');
                    jQuery('#FormErrorMsg').html(data.Message);
                    jQuery('#FormErrorMsg').show();
                }else{
                   
                   jQuery.post("/forum/ucp.php?mode=login", {"login":"Login","username":data.username,"password":data.password},function(data){
                        if(data.IsSuccess){
                            jQuery('#regloading').fadeOut('fast');
                            jQuery('#regoverlay').fadeOut('fast');
                            jQuery('#registeroverlay').fadeIn('fast',function(){
                                jQuery('#registerSuccesswindow').fadeIn('fast',function(){jQuery('#closeRegisterBtn').fadeOut('slow');});
                            });
                            window.location.href="/profile.php"; 
                        }
                            // window.location.href="/schedule.php"; 
                    },"json");
               }
           },"json");
        }
    },"json");

}	

function closeRegister(event){
	event.stopPropagation();
	jQuery('#registerwindow').fadeOut("fast", function(){jQuery('#overlay').fadeOut('fast',function(){document.location.href='/profile.php';})});
}
 
function closeOpen(close, open,event){
	event.stopPropagation();
	jQuery("#"+close).fadeOut("fast", function(){
		var width = jQuery("#"+open).width();
		jQuery("#"+open).css('margin-left', -(width/2)); 
		jQuery("#"+open).fadeIn();
	});
} 
function showWarning(message, img, callback){
	var html = [];
	html.push('<div class="modalpopup" id="chiron_warning" style="display: block; height: 150px; width: 400px; margin-top: -150px;margin-left: -200px; position: fixed">');
	if(!img || img == '')
		img = "";
	else
		img = '<tr><td rowspan="3">'+img+'</td></tr>'
	html.push('<table style="width: 100%;">'+img+'<tr align="center" valign="middle"><td style="height: 95px;" ><span class="time_calendar_warnig_text">');
	html.push(message);
	html.push('</span></td></tr><tr align="center" valign="middle"><td style="height: 35px;">');
	if(!callback)
		callback = "";
	html.push('<div class="button-flex" style="margin: 0 auto;width: 70px;"><a href="#" class="button" onclick="jQuery(\'#chiron_warning\').remove(); jQuery(\'#overlay\').fadeOut(\'fast\');'+callback+'"><span>close</span></a></div></td></tr>');
	
        var warnig = jQuery(html.join(""));//.draggable();
        try{
            warnig.draggable();
        }catch(err){
        //Handle errors here
        }
        
	jQuery("body").append(warnig);
	jQuery("#overlay").fadeIn('fast');
	return html.join("");

}
function showYesNo(message, img, id, callbackYes, callbackNo, yesText, noText, yesClass, noClass){
	var html = [];
	if(!id)
		id = "chiron_yesno";
	html.push('<div class="modalpopup" id="'+id+'" style="display: block; height: 150px; width: 400px; margin-top: -150px;margin-left: -200px; position: fixed">');
	if(!img || img == ''){
		img = "";
	}else{
//		img = '<tr align="right"><td rowspan="2"><img width="50" height="50" src="'+img+'"/></td></tr>';
                img = '<td align="right"><img width="90" height="90" src="'+img+'" style="padding:5px;padding-right: 30px;"/></td>';
	}
        html.push('<table style="width: 100%;"><tr valign="middle">'+img+'<td align='+((img == "")?'center':'left')+' style="height: 95px;" ><div class="time_calendar_warnig_text">');
	html.push(message);
	html.push('</div></td></tr><tr align="center" valign="middle"><td style="height: 35px;" colspan="2">');
	if(!callbackYes)
		callbackYes = "";
	if(!callbackNo)
		callbackNo = "";
	if(!yesText)
		yesText = "Yes";
	if(!noText)
		noText = "No";
	if(!yesClass)
		yesClass = "button";
	if(!noClass)
		noClass = "button";
	
	html.push('<div class="button-flex" style="margin: 0 auto;"><a href="#" class="'+yesClass+'" onclick="'+callbackYes+';jQuery(\'#'+id+'\').remove(); jQuery(\'#overlay\').fadeOut(\'fast\');"><span>'+yesText+'</span></a><a href="#" class="'+noClass+'" onclick="'+callbackNo+';jQuery(\'#'+id+'\').remove(); jQuery(\'#overlay\').fadeOut(\'fast\');"><span>'+noText+'</span></a></div></td></tr>');
	var warnig = jQuery(html.join("")).draggable();
	jQuery("body").append(warnig);
	jQuery("#overlay").fadeIn('fast');
	return html.join("");
}


function loadChiron(){
	jQuery('#chironWrapper').attr('height',(window.innerHeight-35));
	jQuery('#chironWrapper').attr('width',window.innerWidth);
}

function sendMessageWindow(event,toid, toname, fromname){
	event.stopPropagation();
	var html = [];

	html.push('<div class="modalpopup" id="send-message-window" style="display: block; height: 170px; width: 350px; margin-top: -150px;margin-left: -200px; position: fixed">');
	var textId = "window_send_message_"+toid;
	html.push('<div class="title">New message</div>');
	html.push('<div style="margin-top: 10px;"><span style="font-weight: bold;font-size: 16px;">To: </span><span style="font-size: 16px;">'+toname+'</span></div>');
	html.push('<div class="window_send_message"><textarea rows="5" cols="40" class="window_send_message_text" toid="'+toid+'" fromname="'+fromname+'"  id="'+textId+'"></textarea>');
	textId = "#"+textId;
	
	html.push('<div class="" style="margin: 5px auto;text-align: right;"><a href="#" class="button" onclick="sendMessageFromWindow(\''+toid+'\',\''+fromname+'\',\''+toname+'\');"><span>Send</span></a>');
	html.push('<a href="#" class="button" onclick=";jQuery(\'#send-message-window\').remove(); jQuery(\'#overlay\').fadeOut(\'fast\');"><span>Close</span></a></div></td></tr>');
	
	var warnig = jQuery(html.join("")).draggable();
	jQuery("body").append(warnig);
	jQuery("#overlay").fadeIn('fast');
	return html.join("");
}

function getSystemTimeForMessages(time){
	time = parseInt(time)*100;
	var d = new Date() ;
	var offset = d.getTimezoneOffset()*60000;
	//var sentTime = new Date(time + offset); 
	var sentTime = new Date(time);
	return sentTime.getFullYear()+"-"+((sentTime.getMonth()+1)<10?("0"+(sentTime.getMonth()+1)):(sentTime.getMonth()+1))+"-"+(sentTime.getDate()<10?("0"+sentTime.getDate()):sentTime.getDate())+" "+(sentTime.getHours()<10?("0"+sentTime.getHours()):sentTime.getHours())+":"+(sentTime.getMinutes()<10?("0"+sentTime.getMinutes()):sentTime.getMinutes())+":"+(sentTime.getSeconds()<10?("0"+sentTime.getSeconds()):sentTime.getSeconds());
}

function sendMessage(message,toid,fromname, window){
	message = message.replace(/[\r\n]/g, "<br/>");
	var id = "#inner_messages_list_"+toid
	var wrapid = "#inner_messages_list_wrap_"+toid
	//var messagehtml = '<div class="messages_wrap"><div class="messages_sender">'+fromname+'<div class="messages_date">'+new Date()+'</div></div><div class="messages_content">'+message+'</div></div>';
	
	var url = "actionService.php";				
	jQuery.post(url, {"method": "sendMessage", "message":message, "toid":toid,"fromname":fromname,"gmttime":getGMTTime()},function(data){
            if(data.IsSuccess){
                clearInterval(window.messagetimer);
                var sentDate = getSystemTimeForMessages(data.msg.time);
                var messagehtml = '<div class="messages_wrap"><div class="messages_sender">'+data.msg.from_name+'<div class="messages_date">'+sentDate+'</div></div><div class="messages_content">'+data.msg.message+'</div></div>';
                jQuery(wrapid).append(messagehtml);
                var textid="#send_message_"+toid;
                jQuery(textid).val("");
                jQuery(textid).caretToStart();
                jQuery(textid).focus();
                var jscroll = jQuery(id).jScrollPane({stickToBottom:true,maintainPosition:true});
                var api = jscroll.data('jsp');
                api.scrollToPercentY(100);
                window.messagetimer = setInterval('getlastMessages(\''+data.msg.id+'\')',10000);
            }
	},"json");
        var jscroll = jQuery(id).jScrollPane({stickToBottom:true,maintainPosition:true});
        var api = jscroll.data('jsp');
        api.scrollToPercentY(100);
}

function sendMessageFromWindow(toid,fromname,toname){

	var textId = "#window_send_message_"+toid;
	var url = "actionService.php";		
	var message = jQuery(textId).val();
	message = message.replace(/[\r\n]/g, "<br/>");	
	jQuery.post(url, {"method": "sendMessage", "message":message, "toid":toid,"fromname":fromname,"gmttime":getGMTTime(),"toname":toname},function(data){
		
		if(data.IsSuccess){
			jQuery('#send-message-window').remove(); jQuery('#overlay').fadeOut('fast');
		}else {
			alert("Error sending message, please try again later");
		}
	},"json");

}
function openChatTab(event,user_id, username,isfriend){
	event.stopPropagation();
//        if(isfriend == 'n')
//            addAsFriend(event,user_id, username,false);
	parent.FreiChat.createChatBoxmesg(event,username,user_id)
}

function countLenght(elem, counter){
    var size = jQuery(elem).val().length;
    var maxSize = jQuery(elem).attr('maxlength');
    jQuery("#"+counter).text(size+"/"+maxSize);
}

function currentLenght(elem, counter){
    var size = jQuery("#"+elem).val().length;
    var maxSize = jQuery("#"+elem).attr('maxlength');
    jQuery("#"+counter).text(size+"/"+maxSize);
}

function changePageTitle(title){
    var disp = "Chiron Gaming";
    if(title && title != '')
        disp = title+" - Chiron Gaming";
    
    parent.document.title = disp;
}
function changeStatus(event,status){
    event.stopPropagation();
    jQuery.post("actionService.php",{"method": "changeOnlineStatus", "status": status}, function(data){
        var newStatus = "online";
        if(status == 0)
                newStatus = "offline";
        else if(status == 2)
                newStatus = "away";
        else if(status == 3)
                newStatus = "busy";
        jQuery('#online_status_button').removeClass('offline online busy away').addClass(newStatus);
    },'json');
}

function buildPageLink(userid){
    try{
        jQuery('#pageloading').fadeIn('fast');
        jQuery.post("datafeed.php",{"method":"getUserType","userid":userid},function(data){
          jQuery('#pageloading').fadeOut('fast');
          if(data && data.link){
              parent.location = data.link;
          }
         },'json');
     }catch(err){
         console.log("Exception: "+err);
         jQuery('#pageloading').fadeOut('fast');
     }
}

function test(){
    showYesNo("Anonymous call", "images/coach_thumb.jpg",'incommingcallwarning', '', '','Answer','Reject');
}
function selectAll(selector){
    jQuery(selector).each(function(i) {
            this.checked = true;
        
    });
}
function selectNone(selector){
    jQuery(selector).each(function(i) {
        this.checked = false;
    });
}

function getlastMessages(lastId){
	jQuery.post("datafeed.php",{"method": "getNewMessages", "lastId": lastId}, function(data){
                var message;

                if(data.IsSuccess && data.messages){
                    var size = data.messages.length;
                    var ownerId = data.ownerId;
                    clearInterval(window.messagetimer);
                    for(var i=0;i<size;i++){
                        message = data.messages[i];
                        var userId = "";
                        if(ownerId == message.to)
                            userId = message.from;
                        else
                            userId = message.to;
                        
                        var id = "#inner_messages_list_"+userId;
                        var wrapid = "#inner_messages_list_wrap_"+userId;
                        var sentDate = getSystemTimeForMessages(message.time);
			var messagehtml = '<div class="messages_wrap"><div class="messages_sender">'+message.from_name+'<div class="messages_date">'+sentDate+'</div></div><div class="messages_content">'+message.message+'</div></div>';
			jQuery(wrapid).append(messagehtml);
			var textid="#send_message_"+userId;
			jQuery(textid).val("");
			jQuery(textid).caretToStart();
			jQuery(textid).focus();
			var jscroll = jQuery(id).jScrollPane({stickToBottom:true,maintainPosition:true});
			var api = jscroll.data('jsp');
			api.scrollToPercentY(100);
                    }
                    window.messagetimer = setInterval('getlastMessages(\''+message.id+'\')',10000);
                }
            
        },"json");
}

function loadTransactions(){
    var mygrid1 = jQuery("#historyTable").ingrid({
            url: 'remote_cashier.php',
            recordsPerPage:9,
            extraParams:{rows:'9'},
            height: 225,
            initialLoad: true,
            rowClasses: ['grid-row-style1'],
            rowSelection: false,
            //totalRecords: 20,
            colWidths: [50,125,60,50,230,70,50,90,50,50],
            resizableCols: false,
            sorting: false,
            //scrollbarW: (15+ 50+4 + 125+4 + 60+4 + 50+4 + 230+4 + 70+4 + 50+4 + 90+4 + 50+4 + 50+4),
            resultEmptyMessage: 'No Result'
    });
}

function showNotif(text){
    jQuery.notify({
        inline: true,
        html: text
    }, 5000)
}