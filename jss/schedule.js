var ownerinfo={};
var fullDayTime = ["0:00","0:30","1:00","1:30","2:00","2:30","3:00","3:30","4:00","4:30","5:00","5:30","6:00","6:30","7:00","7:30",
"8:00","8:30","9:00","9:30","10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30","15:00",
"15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00","19:30","20:00","20:30","21:00","21:30","22:00","22:30","23:00","23:30","0:00"];
var calendar={};
var selectedSlots={};
var selectedHour={};
var selectedOption=1;

function loadToday(shift){
    selectedOption = 1;
    jQuery('#schedule_loading').fadeIn('fast');
    jQuery("#calendar-wrap").empty();
    jQuery("#calendar-info").empty();
    var yesterday = new Date();
    yesterday.setHours(0,0,0,0);
    yesterday = yesterday
    yesterday=yesterday.add(Date.DAY, shift);    
    var tomorrow = new Date();
    tomorrow.setHours(23,59,59,999);
    tomorrow=tomorrow.add(Date.DAY, shift);      
    //jQuery("#calendar-wrap").empty();
    var lesson;
    var quantEvents = 0;
    for(var i = 0; i<calendar.length; i++){

        lesson = calendar[i];
        var start = Date.parseDate(lesson.start,"Y-m-d H:i:s");  
        start=start.add(Date.HOUR, -(parseInt(lesson.time_zone_offset)));
        if(start.between(yesterday,tomorrow)){
            buildLessonBlock(lesson);
            quantEvents++;
        }

    }
    if(quantEvents == 0)
            jQuery('#calendar-wrap').append("No Events");

    jQuery('#schedule_loading').fadeOut('fast');
}
function loadMonth(){
    selectedOption = 4;
    jQuery('#schedule_loading').fadeIn('fast');
    jQuery("#calendar-wrap").empty();
    jQuery("#calendar-info").empty();
    var first = new Date();
    first=first.getFirstDateOfMonth();    
    first.setHours(0,0,0,0);
    var last = new Date();

    last=last.getLastDateOfMonth();   
    last.setHours(23,59,59,999);
//    jQuery("#calendar-wrap").empty();
    var lesson;
    var quantEvents = 0;
    for(var i = 0; i<calendar.length; i++){

        lesson = calendar[i];
        var start = Date.parseDate(lesson.start,"Y-m-d H:i:s");  
        start=start.add(Date.HOUR, -(parseInt(lesson.time_zone_offset)));
        if(start.between(first,last)){
            buildLessonBlock(lesson);
            quantEvents++;
        }

    }
    if(quantEvents == 0)
            jQuery('#calendar-wrap').append("No Events");

    jQuery('#schedule_loading').fadeOut('fast');
}

function loadPast(){
    selectedOption = 0;
    jQuery('#schedule_loading').fadeIn('fast');
    jQuery("#calendar-wrap").empty();
    jQuery("#calendar-info").empty();
    var today = new Date();
//                                            today.setHours(0,0,0,0);

//    jQuery("#calendar-wrap").empty();
    var lesson;
    var quantEvents = 0;
    for(var i = 0; i<calendar.length; i++){

        lesson = calendar[i];
        var start = Date.parseDate(lesson.start,"Y-m-d H:i:s");  
        start=start.add(Date.HOUR, -(parseInt(lesson.time_zone_offset)));
        if(start.getTime() < today.getTime()){
            buildLessonBlock(lesson);
            quantEvents++;
        }

    }
    if(quantEvents == 0)
            jQuery('#calendar-wrap').append("No Events");

    jQuery('#schedule_loading').fadeOut('fast');
}



function loadWeek(){
    selectedOption = 2;
    jQuery('#schedule_loading').fadeIn('fast');
    jQuery("#calendar-wrap").empty();
    jQuery("#calendar-info").empty();
    var today = new Date();
    today.setHours(0,0,0,0);

//    jQuery("#calendar-wrap").empty();
    var lesson;
    var quantEvents = 0;
    for(var i = 0; i<calendar.length; i++){

        lesson = calendar[i];
        var start = Date.parseDate(lesson.start,"Y-m-d H:i:s");  
        start=start.add(Date.HOUR, -(parseInt(lesson.time_zone_offset)));
        if(start.getWeekOfYear() == today.getWeekOfYear() && start.getYear()==today.getYear()){
            buildLessonBlock(lesson);
            quantEvents++;
        }

    }
    if(quantEvents == 0)
            jQuery('#calendar-wrap').append("No Events");

    jQuery('#schedule_loading').fadeOut('fast');
}

function loadAll(){
        selectedOption = 5;
        jQuery('#schedule_loading').fadeIn('fast');
        jQuery("#calendar-wrap").empty();
        jQuery("#calendar-info").empty();
        var fixdate = new Date();
        fixdate.setHours(0,0,0,0);

//        jQuery("#calendar-wrap").empty();
        var lesson;
        var quantEvents = 0;
        for(var i = 0; i<calendar.length; i++){

            lesson = calendar[i];
            buildLessonBlock(lesson);
            quantEvents++;
        }
        if(quantEvents == 0)
                jQuery('#calendar-wrap').append("No Events");
        jQuery('#schedule_loading').fadeOut('fast');
}

function changeSelection(element){
        jQuery(".schedule-links a").removeClass("selected");
        jQuery(element).addClass("selected");
}

function bindHover(div, lesson) {
    return div.bind('mouseout', function(event) {
            jQuery(this).removeClass("schedule_event_over");
        })
        .bind('mouseover', function(event) {
            jQuery(this).addClass("schedule_event_over");
        })
        .bind('click', function(event) {
            jQuery(".schedule_event").removeClass("schedule_event_selected");
            jQuery(this).addClass("schedule_event_selected");
            getScheduleInfo(lesson);
        });
}
function buildLessonBlock(lesson){
    var html = [];
    var start = Date.parseDate(lesson.start,"Y-m-d H:i:s");
    var end = Date.parseDate(lesson.end,"Y-m-d H:i:s");

    html.push('<div class="lessonID_'+lesson.lesson_id+' schedule_event');
    if(lesson.lesson_status_id == 5)
            html.push(' schedule_canceled');

    html.push('">');

    if(lesson.student_id == ownerinfo.user_id){
        if(lesson.hide_from_student == 1)
            return;
        start=start.add(Date.HOUR, -(parseInt(lesson.time_zone_offset)));
        end=end.add(Date.HOUR, -(parseInt(lesson.time_zone_offset)));
        html.push('<div class="schedule_type"><div>Traning session</div><div style="margin-top: 15px;"><span style="font-weight: bold">Coach: </span><span>');
        html.push(lesson.coach_name);
        html.push('</span></div></div>');
    }else{
        if(lesson.hide_from_coach == 1)
            return;
        html.push('<div class="schedule_type"><div>Coach session</div><div style="margin-top: 15px;"><span style="font-weight: bold">Trainee: </span><span>');
        html.push(lesson.student_name);
        html.push('</span></div></div>');
    }
            
    html.push('<div class="schedule_date">'+start.format("Y-m-d H:i:s")+'</div>');
    html.push('<div class="clear"></div>');
    
    html.push('</div>');

    var div = jQuery(html.join(""));
    jQuery('#calendar-wrap').append(bindHover(div,lesson));
}
function getScheduleInfo(lesson){ 

        var html = [];
        html.push('<div class="schedule-info-wrap"><div class="scheduleTitle">Schedule information</div>');		
        jQuery("#calendar-info").empty();
        jQuery('#info_loading').fadeIn('fast');
        var start = Date.parseDate(lesson.start,"Y-m-d H:i:s");
        var end = Date.parseDate(lesson.end,"Y-m-d H:i:s");    
        if(lesson.student_id == ownerinfo.user_id){ //is trainee

            start=start.add(Date.HOUR, -(parseInt(lesson.time_zone_offset)));
            end=end.add(Date.HOUR, -(parseInt(lesson.time_zone_offset)));
            jQuery('#calendarloading').fadeIn('fast');
            jQuery('#calendaroverlay').fadeIn('fast');
            var urlUser = "datafeed.php?method=getUserInfo&userid="+lesson.coach_id;
            jQuery.get(urlUser, function(data){
                var coachname = data.username;
                var gamename = "";
                for (var j = 0; j < data.games.length; j++){
                    if(lesson.game_id == data.games[j].game_id){
                        gamename = data.games[j].game_name;
                        break;
                    }
                }		
                html.push('<table cellspacing="7px" cellpadding="0">');		
                html.push(getTwoCellRow('Coach:', coachname));
                html.push(getTwoCellRow('Game:', gamename));
                html.push(getTwoCellRow('Points:', lesson.lesson_price));
                html.push(getTwoCellRow('Start:', start.format("Y-m-d H:i:s")));
                html.push(getTwoCellRow('End:', end.format("Y-m-d H:i:s") ));    
                html.push('</table>');
                if(lesson.lesson_status_id == 5){
                    html.push('<div style="font-weight:bold; color: #c22c04; font-size: 16px">This schedule have been canceled</div>');
                    html.push('<div><table cellspacing="7px" cellpadding="0">');
                    html.push(getTwoCellRow('Reason:', lesson.status_reason));
                    html.push('</table></div>');
                    html.push('<div style="margin-top: 10px;"><a href="javascript:;" onclick="removeCanceled(\''+lesson.lesson_id+'\', \'hide_from_student\')" class="button">Remove From List</a></div>');
                }else if(lesson.lesson_type == 0){
                    var now = new Date();
                    if(end.getTime() < now.getTime()){
                        html.push('<div style="font-weight:bold; color: #4D90FE; font-size: 16px">Schedule completed</div>');
                        html.push('<div style="margin-top: 10px;"><a href="javascript:;" onclick="removeCanceled(\''+lesson.lesson_id+'\', \'hide_from_student\')" class="button">Remove From List</a></div>');
                    }else if(start.getTime() < now.getTime()){
                        html.push('<div style="font-weight:bold; color: #58A222; font-size: 16px">Schedule on going</div>');
                    }
                    else{
                        var lessThan24hr = 0;
                        var delta = start.getTime()- now.getTime();
                        if(delta < (3600*1000*24))
                            lessThan24hr = 1;
                        html.push('<a href="javascript:;" onclick="cancelReason(\''+lesson.lesson_id+'\', \''+start.format("Y-m-d")+'\', \''+lesson.coach_id+'\', \''+lesson.student_id+'\', \''+lesson.lesson_price+'\',false,\''+lessThan24hr+'\')" class="button">Cancel</a>');
                    }
                }
                
                html.push('</div>');
                jQuery('#info_loading').fadeOut('fast');
                jQuery("#calendar-info").html(html.join(""));
            }, "json");

        }else if(lesson.coach_id == ownerinfo.user_id){//is Coach
            var urlUser = "datafeed.php?method=getUserInfo&userid="+lesson.student_id;
            jQuery.get(urlUser, function(data){
                var gamename = "";
                    for (var j = 0; j < ownerinfo.games.length; j++){
                        if(lesson.game_id  ==  ownerinfo.games[j].game_id){
                            gamename =  ownerinfo.games[j].game_name;
                            break;
                        } 
                    }
                var traineename = data.username;
                html.push('<table cellspacing="7px" cellpadding="0">');		
                html.push(getTwoCellRow('Trainee:', traineename));
                html.push(getTwoCellRow('Game:', gamename));
                html.push(getTwoCellRow('Points:', lesson.lesson_price));
                html.push(getTwoCellRow('Start:', start.format("Y-m-d H:i:s")));
                html.push(getTwoCellRow('End:', end.format("Y-m-d H:i:s") )); 
                html.push('</table>');
                if(lesson.lesson_status_id == 5){
                    html.push('<div style="font-weight:bold; color: #c22c04; font-size: 16px">This schedule have been canceled</div>');
                    html.push('<div><table cellspacing="7px" cellpadding="0">');
                    html.push(getTwoCellRow('Reason:', lesson.status_reason));
                    html.push('</table></div>');
                    html.push('<div style="margin-top: 10px;"><a href="javascript:;" onclick="removeCanceled(\''+lesson.lesson_id+'\', \'hide_from_coach\')" class="button">Remove From List</a></div>');
                }else if(lesson.lesson_type == 0){
                    var now = new Date();
                    if(end.getTime() < now.getTime()){
                        html.push('<div style="font-weight:bold; color: #4D90FE; font-size: 16px">Schedule completed</div>');
                        html.push('<div style="margin-top: 10px;"><a href="javascript:;" onclick="removeCanceled(\''+lesson.lesson_id+'\', \'hide_from_student\')" class="button">Remove From List</a></div>');
                    }else if(start.getTime() < now.getTime()){
                        html.push('<div style="font-weight:bold; color: #58A222; font-size: 16px">Schedule on going</div>');
                    }
                    else
                        html.push('<a href="javascript:;" onclick="cancelReason(\''+lesson.lesson_id+'\', \''+start.format("Y-m-d")+'\', \''+lesson.coach_id+'\', \''+lesson.student_id+'\', \''+lesson.lesson_price+'\',true)" class="button">Cancel</a>');
                }
                html.push('</div>');
                jQuery('#info_loading').fadeOut('fast');
                jQuery("#calendar-info").html(html.join(""));
            }, "json");
    }
}
function getTwoCellRow(arg0, arg1){
        var html = [];
        html.push('<tr valign="top"><td><div class="scheduleLabel">');
        html.push(arg0);
        html.push('</div></td><td>');
        html.push(arg1);
        html.push('</td></tr>');
        return html.join("");
}
function cancelReason(lessonId, lessondate, coach_id, student_id, lessonPrice, isCoach, lessThan24hr){
    var html = [];
    html.push('<div class="modalpopup" id="cancelLessonReason" style="display: block; margin-top: -150px;margin-left: -200px; position: fixed">');
    var cancellationPolicy = "";
    if(isCoach)
        cancellationPolicy = "Coaches can cancel a lesson anytime - however, it is highly recommended for coaches to keep the lesson appointments, to receive positive feedback from students.";
    else
        cancellationPolicy = "To get a full refund of points, a student must cancel a lesson 24 hours (1 day) prior to the scheduled lesson. Otherwise, no refund can be made.";
    
    html.push('<table style="width: 100%;"><tr align="left" valign="middle"><td style="" ><span class="time_calendar_warnig_text">');
    html.push('Please tell us the reason');
    html.push('</span></td></tr>');
    html.push('<tr align="left" valign="middle"><td><textarea id="cancelLessonReasonText" required="required" rows="6" cols="46"/></td></tr>');
    html.push('<tr align="left" valign="middle"><td><span id="cancelLessonReasonTextError" style="color: red; display:none; ">Please write something</span></td></tr>');
    html.push('<tr align="right" valign="middle"><td><div class="tip" id="cancellationPolicy" style="font-weight: bold; cursor: pointer;" title="');
    html.push(cancellationPolicy);
    html.push('">Cancellation Policy</div></td></tr><tr align="center" valign="middle"><td style="height: 35px;">');
    html.push('<div class="button-flex" style="margin: 0 auto;"><a href="#" class="button" onclick="cancel(\''+lessonId+'\', \''+lessondate+'\', \''+coach_id+'\', \''+student_id+'\', \''+lessonPrice+'\', \''+lessThan24hr+'\');"><span>Confirm</span></a><a href="#" class="button" onclick="jQuery(\'#cancelLessonReason\').remove(); jQuery(\'#overlay\').fadeOut(\'fast\');"><span>Cancel</span></a></div></td></tr>');
    var warnig = jQuery(html.join("")).draggable();
    jQuery("body").append(warnig);
    jQuery("#overlay").fadeIn('fast');
    jQuery("#cancellationPolicy").tipTip({defaultPosition: "right",left: 0,delay: 100,fadeOut: 3000,maxWidth: "300px"});
    return html.join("");
}

function cancel(lessonId, lessondate, coach_id, student_id, lessonPrice,lessThan24hr){
var reason = jQuery('#cancelLessonReasonText').val()
if(reason ==''){
    jQuery('#cancelLessonReasonTextError').fadeIn();
    return;
}
jQuery('#cancelLessonReason').remove(); 
jQuery('#overlay').fadeOut('fast');    
//ownerinfo
jQuery.post("datafeed.php",{
    "method": "cancelLesson",
    "lessonId": lessonId,
    "reason": reason,
    "date": lessondate,
    "coachId": coach_id,
    "studentId":student_id,
    "lessonPrice":lessonPrice,
    "lessThan24hr":lessThan24hr}, function(data){
        if(data.IsSuccess){
            loadCalendar();   
        }
},"json");
}

function removeCanceled(lessonId,utype){
    jQuery('#schedule_loading').fadeIn('fast');
    var selector = '.lessonID_'+lessonId;
    jQuery.post("datafeed.php",{"method": "removeCanceledLesson", "lessonId": lessonId, "utype": utype}, function(data){
        if(data.IsSuccess){
            for(var i = 0; i<calendar.length; i++){
                if(calendar[i].lesson_id == lessonId){
                    if(utype == 'hide_from_coach')
                        calendar[i].hide_from_coach = 1;
                    else if(utype == 'hide_from_student')
                        calendar[i].hide_from_student = 1;
                    break;
                }
            }
            jQuery(selector).fadeOut('slow',function(){
                jQuery(selector).remove();
                jQuery("#calendar-info").empty();
                
            });
            jQuery('#schedule_loading').fadeOut('fast');
        }
    },"json");
}
function loadCalendar(){
        calendar={};
        selectedSlots={};
        selectedHour={};
        jQuery('#schedule_loading').fadeIn('fast');
        var url = "datafeed.php?method=getAllLessons";
        jQuery.get(url, function(data){
            calendar = data;
            var urlUser = "datafeed.php?method=getUserInfo&userid="+getUserId();
            jQuery.get(urlUser, function(data){
                ownerinfo = data;
                switch (selectedOption){
                    case 0: loadPast();
                                    break;
                    case 1: loadToday(0);
                                    break;
                    case 2: loadToday(1);
                                    break;
                    case 3: loadWeek();
                                    break;
                    case 4: loadMonth();
                                    break;
                    case 5: loadAll();
                                    break;
                    default: loadToday(0);;
                }
            },"json");
        },"json");
}


