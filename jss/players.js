
(function(window) {
	
	function Players() {
		this.data = {};
		this.html=[];
		
		this.getNotifications= function () {
			var url = "datafeed.php?method=getNotifications";
			
			$.get(url, function(data){
				notifications.data = data;
				if(data.unreaded > 0){
					$('.notifications_quantity').text(data.unreaded);
					$('.notifications_quantity').slideDown();
				}else
					$('.notifications_quantity').slideUp();
			},"json");
			
			setTimeout('window.notifications.getNotifications()',20000);
		};
		
		this.showNotifications = function(event){
			event.stopPropagation();
			if($('.notificationsWrapper').is(':visible')){
				try{
					$(".notificationsInner").getNiceScroll().remove(); 
				}catch(e){}
				$('.notificationsWrapper').slideUp('fast',function() {
				    $(this).remove(); 
				  });
				return;
			}
			var notif;
			var not = [];
			
			var wrapper = $('<div class="notificationsWrapper"/>');
			if(this.data.notifications.length > 4){
				not.push('<div class="notifications_title"><span style="margin-left: 10px">Notifications</span></div><div class="notificationsInner" style="height: 446px">');
			}else{
				not.push('<div class="notifications_title"><span style="margin-left: 10px">Notifications</span></div><div class="notificationsInner">');
			}
			
			var ids = [];
			if(this.data.notifications.length == 0){
				not.push('<div class="notification"><a href="javascript:;" class="notification_inner"><div class="notification_title"></div><div class="notification_text">No Notifications</div><div style="font-weight: bold;" class="notification_date"></div></a></div>');
			}
			else{
				for (var i = 0; i < this.data.notifications.length; i++){
					notif = this.data.notifications[i][0];
					not.push('<div class="notification"><a class="notification_inner')
					if(notif.new == 'Y'){
						ids.push(notif.notification_id);
						not.push(' notification_unread')
					}
					not.push('">');
					if(notif.title)not.push('<div class="notification_title">'+notif.title+'</div>');
					if(notif.text)not.push('<div class="notification_text">'+notif.text+'</div>');
					if(notif.modified_date)not.push('<div class="notification_date">'+notif.modified_date+'</div>');
					not.push('</a></div>');					
				}
			}
			not.push('</div></div>');
			wrapper.html(not.join(""));

			$('body').append(wrapper);
			wrapper.slideDown('fast',function() {
				$('.notifications_title').width(wrapper.width());
			    if(notifications.data.notifications.length > 4){
			    	$('.notificationsInner').niceScroll();
				}
			    notifications.markAsRead(ids.join("#"),notifications.getNotifications());
			  });
			
			wrapper.click(function(event){
			     event.stopPropagation();
			 });
		};
		this.markAsRead = function(notifIds,callback){
			if(!notifIds || notifIds == '')
				return;
			var url = "datafeed.php";
			$.post(url, {"method":"markAsRead", "notificationIds": notifIds},function(data){
			},"json");
		};
		this.addNotification = function(){
			var url = "datafeed.php";
			$.post(url, {"method":"insertNotifications", "title": "AAA", "introduction": "VVVV", "text": "MMMMMMMMMMm"},function(data){
				if(callback)
					callback();
			},"json");
		};
	}
	$(document).click(function() {
		if($('.notificationsWrapper').is(':visible')){
			try{
				$(".notificationsInner").getNiceScroll().remove(); 
			}catch(e){}
			$('.notificationsWrapper').slideUp('fast',function() {
			    $(this).remove(); 
			  });
			return;
		}
	 });
	window.notifications = new Notification();
	window.notifications.getNotifications();
})(window);