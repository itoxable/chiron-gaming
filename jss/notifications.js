(function(window) {
	function Notification() {
        this.interval = 10000;
		this.data = {};
		this.html=[];
		this.timer;
		this.getNotifications= function () {
			var url = "datafeed.php?method=getNotifications";
			jQuery.get(url, function(data){
				notifications.data = data;
				if(data.unreaded > 0){
					jQuery('.notifications_quantity').text(data.unreaded);
					jQuery('.notifications_quantity').slideDown();
				}else
					jQuery('.notifications_quantity').slideUp();
				var notifsSize = notifications.data.notifications.length;
				for (var i = 0; i < notifsSize; i++){
					var notif = notifications.data.notifications[i][0];
					if(notif.new == "Y" && (notif.type == '3' || notif.type == '5')){
						clearInterval(window.notifications.timer);
						var html= [];
						html.push('<div class="modalpopup notif_warning" style="display:block;" id="notificationWarning"><div class="login"><div class="title">');
						html.push(notif.title);
						html.push('</div><br/><div style="text-align: center;">');
						html.push(notif.text);
						html.push('</div><br/><div style="text-align: center;"><a class="button" href="#" style="margin-top: 20px" ');
						
                                                if(notif.type == '5'){
                                                    html.push(' onclick="window.notifications.markAsRead(');
                                                    html.push(notif.notification_id);
                                                    html.push(');window.notifications.removeWarningWindow();window.notifications.restart()">OK</a>');
                                                }
                                                if(notif.type == '3'){
                                                    html.push(' onclick="window.notifications.dealInstantLesson(event,1,');
                                                    html.push(notif.notification_id);
                                                    html.push(',');
                                                    html.push(notif.extra_data==undefined?"0":notif.extra_data);
                                                    html.push(')">Accept</a><a class="button" href="#" style="margin-top: 20px" onclick="window.notifications.dealInstantLesson(event,0,');
                                                    html.push(notif.notification_id);
                                                    html.push(',');
                                                    html.push(notif.extra_data==undefined?"0":notif.extra_data);
                                                    html.push(');">Refuse</a></div></div></div>');
                                                }
                                                
						html.push('</div></div></div>');
                                                console.log(html.join(""));
                                                
						var warnig = jQuery(html.join(""));
						jQuery("body").append(warnig);
						warnig.draggable()
						if(jQuery('#overlay').length > 0){
							jQuery('#overlay').fadeIn();
						}
					}
				}
			},"json");
		};
		
                this.restart = function(){
                    window.notifications.getNotifications();
                    window.notifications.timer = setInterval('window.notifications.getNotifications()',window.notifications.interval);
                };
                
		this.dealInstantLesson = function(event,accept,id,lessonId){
			event.stopPropagation();
           
			jQuery.post("datafeed.php",{"method":"acceptInstantLesson","accept":accept,"lessonId":lessonId, "notifId": id}, function(data){
				window.notifications.getNotifications();
				window.notifications.timer = setInterval('window.notifications.getNotifications()',window.notifications.interval);
				jQuery('#notificationWarning').remove();
				if(jQuery('#overlay').length > 0){
					jQuery('#overlay').fadeOut('fast');
				}
			}, "json");
		};
		this.removeWarningWindow = function(){
                    jQuery('#notificationWarning').remove();
                    if(jQuery('#overlay').length > 0){
                            jQuery('#overlay').fadeOut('fast');
                    }
                };
		this.showNotifications = function(event){
			event.stopPropagation();
			if(jQuery('.notificationsWrapper').is(':visible')){
				try{
					jQuery(".notificationsInner").getNiceScroll().remove(); 
				}catch(e){}
				jQuery('.notificationsWrapper').slideUp('fast',function() {
				    jQuery(this).remove(); 
				});
				return;
			}
			var notif;
			var not = [];
			
			var wrapper = jQuery('<div class="notificationsWrapper"/>');
			if(this.data.notifications.length > 4){
				not.push('<div class="notifications_title"><span style="margin-left: 10px">Notifications</span></div><div><div class="notificationsInner" style="height: 446px;">');
			}else{
				not.push('<div class="notifications_title"><span style="margin-left: 10px">Notifications</span></div><div><div class="notificationsInner">');
			}
			
			var ids = [];
			if(this.data.notifications.length == 0){
				not.push('<div class="notification"><a href="javascript:;" class="notification_inner"><div class="notification_title"></div><div class="notification_text">No Notifications</div><div style="font-weight: bold;" class="notification_date"></div></a></div>');
			}
			else{
				for (var i = 0; i < this.data.notifications.length; i++){
					notif = this.data.notifications[i][0];
					not.push('<div class="notification" id="notification_'+notif.notification_id+'"><a class="notification_inner')
					if(notif.new == 'Y'){
						ids.push(notif.notification_id);
						not.push(' notification_unread')
					}
					var link = notif.link==null?"javascript:;":notif.link;
					not.push('" href="'+link+'">');
					if(notif.title)not.push('<div class="notification_title">'+notif.title+'</div>');
					if(notif.text)not.push('<div class="notification_text">'+notif.text+'</div>');
					if(notif.modified_date)not.push('<div class="notification_date">'+notif.modified_date+'</div>');
					not.push('</a></div>');					
				}
			}
			not.push('</div></div></div>');
			wrapper.html(not.join(""));

			jQuery('body').append(wrapper);
			wrapper.slideDown('fast',function() {
				jQuery('.notifications_title').width(wrapper.width());
			    if(notifications.data.notifications.length > 4){
			    	jQuery('.notificationsInner').niceScroll();
				}
			    notifications.markAsRead(ids.join("#"),notifications.getNotifications());
			  });
			
			wrapper.click(function(event){
			     event.stopPropagation();
			 });
		};
		this.markAsRead = function(notifIds,callback){
			if(!notifIds || notifIds == '')
				return;
			var url = "datafeed.php";
			jQuery.post(url, {"method":"markAsRead", "notificationIds": notifIds},function(data){
				if(callback)
					callback;
			},"json");
		};
	
		this.close = function(){
			if(jQuery('.notificationsWrapper').is(':visible')){
				try{
					jQuery(".notificationsInner").getNiceScroll().remove(); 
				}catch(e){}
				jQuery('.notificationsWrapper').slideUp('fast',function() {
				    jQuery(this).remove(); 
				  });
				return;
			}
		};
	}
	jQuery(document).click(function() {
		window.notifications.close();
	});
	
	window.notifications = new Notification();
	window.notifications.getNotifications();
	window.notifications.timer = setInterval('window.notifications.getNotifications()',window.notifications.interval);
})(window);