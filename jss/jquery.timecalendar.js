
/*!
 * jQuery UI Datepicker 1.8.21
 *
 * Copyright 2012, AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * http://docs.jquery.com/UI/Datepicker
 *
 * Depends:
 *	jquery.ui.core.js
 */
(function( jQuery, undefined ) {

jQuery.extend(jQuery.ui, { timecalendar: { version: "1.8.21" } });

var PROP_NAME = 'timecalendar';
var dpuuid = new Date().getTime();
var instActive;


function Timecalendar(value, id) {
	if(value){
		this.daysAvailability = jQuery.parseJSON(value);
	}else
		this.daysAvailability = {"":""};
	if(id)
		this.userId = id;
	this.fullDayTime = ["0:00","0:30","1:00","1:30","2:00","2:30","3:00","3:30","4:00","4:30","5:00","5:30","6:00","6:30","7:00","7:30",
		"8:00","8:30","9:00","9:30","10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30","15:00",
		"15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00","19:30","20:00","20:30","21:00","21:30","22:00","22:30","23:00","23:30","0:00"];
	this.dayArray = [];
        this.timeOffSet = 0;
	this.allGamesArray = [];
	this.scheduled=[];
	this.youarecoach = false;
	this.isowncalendar = true;
	this.jscheduled = [];
	this.selectedDay = new Date();
	this.ownerinfo = {"":""};
	this.selectedSpots = 0;
	this.calendarTarget = "calendar";
	this.debug = false; // Change this to true to start debugging
	this._curInst = null; // The current instance in use
	this._keyEvent = false; // If the last event was a key event
	this._disabledInputs = []; // List of date picker inputs that have been disabled
	this._datepickerShowing = false; // True if the popup picker is showing , false if not
	this._inDialog = false; // True if showing within a "dialog", false if not
	this._mainDivId = 'ui-timecalendar-div'; // The ID of the main timecalendar division
	this._inlineClass = 'ui-timecalendar-inline'; // The name of the inline marker class
	this._appendClass = 'ui-timecalendar-append'; // The name of the append marker class
	this._triggerClass = 'ui-timecalendar-trigger'; // The name of the trigger marker class
	this._dialogClass = 'ui-timecalendar-dialog'; // The name of the dialog marker class
	this._disableClass = 'ui-timecalendar-disabled'; // The name of the disabled covering marker class
	this._unselectableClass = 'ui-timecalendar-unselectable'; // The name of the unselectable cell marker class
	this._currentClass = 'ui-timecalendar-current-day'; // The name of the current day marker class
	this._dayOverClass = 'ui-timecalendar-days-cell-over'; // The name of the day hover marker class
	this.regional = []; // Available regional settings, indexed by language code
	this.regional[''] = { // Default regional settings
		closeText: 'Done', // Display text for close link
		prevText: 'Prev', // Display text for previous month link
		nextText: 'Next', // Display text for next month link
		currentText: 'Today', // Display text for current month link
		monthNames: ['January','February','March','April','May','June',
			'July','August','September','October','November','December'], // Names of months for drop-down and formatting
		monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'], // For formatting
		dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'], // For formatting
		dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'], // For formatting
		dayNamesMin: ['Su','Mo','Tu','We','Th','Fr','Sa'], // Column headings for days starting at Sunday
		weekHeader: 'Wk', // Column header for week of the year
		dateFormat: 'mm/dd/yy', // See format options on parseDate
		firstDay: 0, // The first day of the week, Sun = 0, Mon = 1, ...
		isRTL: false, // True if right-to-left language, false if left-to-right
		showMonthAfterYear: false, // True if the year select precedes month, false for month then year
		yearSuffix: '' // Additional text to append to the year in the month headers
	};
	this._defaults = {
		showOn: 'focus', showAnim: 'fadeIn', showOptions: {}, defaultDate: null, appendText: '', buttonText: '...', buttonImage: '', buttonImageOnly: false, hideIfNoPrevNext: false, navigationAsDateFormat: false, gotoCurrent: false, changeMonth: false, changeYear: false, yearRange: 'c-10:c+10', showOtherMonths: false, selectOtherMonths: false, showWeek: false, calculateWeek: this.iso8601Week, shortYearCutoff: '+10',minDate: null, maxDate: null, duration: 'fast', beforeShowDay: null, beforeShow: null, onSelect: null, onChangeMonthYear: null, onClose: null, numberOfMonths: 1, showCurrentAtPos: 0, stepMonths: 1, stepBigMonths: 12, altField: '', altFormat: '', constrainInput: true, showButtonPanel: false, autoSize: false, disabled: false };
	jQuery.extend(this._defaults, this.regional['']);
	this.dpDiv = bindHover(jQuery('<div id="' + this._mainDivId + '" class="ui-timecalendar ui-widget-timecalendar ui-widget-timecalendar-content ui-helper-clearfix ui-corner-all"></div>'));
}

jQuery.extend(Timecalendar.prototype, {
	/* Class name added to elements to indicate already configured with a date picker. */
	markerClassName: 'hasDatepicker',
	
	//Keep track of the maximum number of rows displayed (see #7043)
	maxRows: 4,

	/* Debug logging (if enabled). */
	log: function () {
		if (this.debug)
			console.log.apply('', arguments);
	},
	_widgetDatepicker: function() {
		return this.dpDiv;
	},

	/* Override the default settings for all instances of the date picker.
	   @param  settings  object - the new settings to use as defaults (anonymous object)
	   @return the manager object */
	setDefaults: function(settings) {
		extendRemove(this._defaults, settings || {});
		return this;
	},

	/* Attach the date picker to a jQuery selection.
	   @param  target    element - the target input field or division or span
	   @param  settings  object - the new settings to use for this date picker instance (anonymous) */
	_attachDatepicker: function(target, settings) {
		// check for settings on the control itself - in namespace 'date:'
		var inlineSettings = null;
		for (var attrName in this._defaults) {
			var attrValue = target.getAttribute('date:' + attrName);
			if (attrValue) {
				inlineSettings = inlineSettings || {};
				try {
					inlineSettings[attrName] = eval(attrValue);
				} catch (err) {
					inlineSettings[attrName] = attrValue;
				}
			}
		}
		var nodeName = target.nodeName.toLowerCase();
		var inline = (nodeName == 'div' || nodeName == 'span');
		if (!target.id) {
			this.uuid += 1;
			target.id = 'dp' + this.uuid;
		}
		var inst = this._newInst(jQuery(target), inline);
		inst.settings = jQuery.extend({}, settings || {}, inlineSettings || {});
		if (nodeName == 'input') {
			this._connectDatepicker(target, inst);
		} else if (inline) {
			this._inlineDatepicker(target, inst);
		}
	},

	/* Create a new instance object. */
	_newInst: function(target, inline) {
		var id = target[0].id.replace(/([^A-Za-z0-9_-])/g, '\\\\jQuery1'); // escape jQuery meta chars
		return {id: id, input: target, // associated target
			selectedDay: 0, selectedMonth: 0, selectedYear: 0, // current selection
			drawMonth: 0, drawYear: 0, // month being drawn
			inline: inline, // is timecalendar inline or not
			dpDiv: (!inline ? this.dpDiv : // presentation div
			bindHover(jQuery('<div class="' + this._inlineClass + ' ui-timecalendar ui-widget-timecalendar ui-widget-timecalendar-content ui-helper-clearfix ui-corner-all"></div>')))};
	},

	/* Attach the date picker to an input field. */
	_connectDatepicker: function(target, inst) {
		var input = jQuery(target);
		inst.append = jQuery([]);
		inst.trigger = jQuery([]);
		if (input.hasClass(this.markerClassName))
			return;
		this._attachments(input, inst);
		input.addClass(this.markerClassName).keydown(this._doKeyDown).
			keypress(this._doKeyPress).keyup(this._doKeyUp).
			bind("setData.timecalendar", function(event, key, value) {
				inst.settings[key] = value;
			}).bind("getData.timecalendar", function(event, key) {
				return this._get(inst, key);
			});
		this._autoSize(inst);
		jQuery.data(target, PROP_NAME, inst);
		//If disabled option is true, disable the timecalendar once it has been attached to the input (see ticket #5665)
		if( inst.settings.disabled ) {
			this._disableDatepicker( target );
		}
	},

	/* Make attachments based on settings. */
	_attachments: function(input, inst) {
		var appendText = this._get(inst, 'appendText');
		var isRTL = this._get(inst, 'isRTL');
		if (inst.append)
			inst.append.remove();
		if (appendText) {
			inst.append = jQuery('<span class="' + this._appendClass + '">' + appendText + '</span>');
			input[isRTL ? 'before' : 'after'](inst.append);
		}
		input.unbind('focus', this._showDatepicker);
		if (inst.trigger)
			inst.trigger.remove();
		var showOn = this._get(inst, 'showOn');
		if (showOn == 'focus' || showOn == 'both') // pop-up date picker when in the marked field
			input.focus(this._showDatepicker);
		if (showOn == 'button' || showOn == 'both') { // pop-up date picker when button clicked
			var buttonText = this._get(inst, 'buttonText');
			var buttonImage = this._get(inst, 'buttonImage');
			inst.trigger = jQuery(this._get(inst, 'buttonImageOnly') ?
				jQuery('<img/>').addClass(this._triggerClass).
					attr({ src: buttonImage, alt: buttonText, title: buttonText }) :
				jQuery('<button type="button"></button>').addClass(this._triggerClass).
					html(buttonImage == '' ? buttonText : jQuery('<img/>').attr(
					{ src:buttonImage, alt:buttonText, title:buttonText })));
			input[isRTL ? 'before' : 'after'](inst.trigger);
			inst.trigger.click(function() {
				if (jQuery.timecalendar._datepickerShowing && jQuery.timecalendar._lastInput == input[0])
					jQuery.timecalendar._hideDatepicker();
				else if (jQuery.timecalendar._datepickerShowing && jQuery.timecalendar._lastInput != input[0]) {
					jQuery.timecalendar._hideDatepicker(); 
					jQuery.timecalendar._showDatepicker(input[0]);
				} else
					jQuery.timecalendar._showDatepicker(input[0]);
				return false;
			});
		}
	},

	/* Apply the maximum length for the date format. */
	_autoSize: function(inst) {
		if (this._get(inst, 'autoSize') && !inst.inline) {
			var date = new Date(2009, 12 - 1, 20); // Ensure double digits
			var dateFormat = this._get(inst, 'dateFormat');
			if (dateFormat.match(/[DM]/)) {
				var findMax = function(names) {
					var max = 0;
					var maxI = 0;
					for (var i = 0; i < names.length; i++) {
						if (names[i].length > max) {
							max = names[i].length;
							maxI = i;
						}
					}
					return maxI;
				};
				date.setMonth(findMax(this._get(inst, (dateFormat.match(/MM/) ?
					'monthNames' : 'monthNamesShort'))));
				date.setDate(findMax(this._get(inst, (dateFormat.match(/DD/) ?
					'dayNames' : 'dayNamesShort'))) + 20 - date.getDay());
			}
			inst.input.attr('size', this._formatDate(inst, date).length);
		}
	},

	/* Attach an inline date picker to a div. */
	_inlineDatepicker: function(target, inst) {
		var divSpan = jQuery(target);
		if (divSpan.hasClass(this.markerClassName))
			return;
		divSpan.addClass(this.markerClassName).append(inst.dpDiv).
			bind("setData.timecalendar", function(event, key, value){
				inst.settings[key] = value;
			}).bind("getData.timecalendar", function(event, key){
				return this._get(inst, key);
			});
		jQuery.data(target, PROP_NAME, inst);
		this._setDate(inst, this._getDefaultDate(inst), true);
		this._updateDatepicker(inst);
		this._updateAlternate(inst);
		//If disabled option is true, disable the timecalendar before showing it (see ticket #5665)
		if( inst.settings.disabled ) {
			this._disableDatepicker( target );
		}
		// Set display:block in place of inst.dpDiv.show() which won't work on disconnected elements
		// http://bugs.jqueryui.com/ticket/7552 - A Timecalendar created on a detached div has zero height
		inst.dpDiv.css( "display", "block" );
	},

	/* Pop-up the date picker in a "dialog" box.
	   @param  input     element - ignored
	   @param  date      string or Date - the initial date to display
	   @param  onSelect  function - the function to call when a date is selected
	   @param  settings  object - update the dialog date picker instance's settings (anonymous object)
	   @param  pos       int[2] - coordinates for the dialog's position within the screen or
	                     event - with x/y coordinates or
	                     leave empty for default (screen centre)
	   @return the manager object */
	_dialogDatepicker: function(input, date, onSelect, settings, pos) {
		var inst = this._dialogInst; // internal instance
		if (!inst) {
			this.uuid += 1;
			var id = 'dp' + this.uuid;
			this._dialogInput = jQuery('<input type="text" id="' + id +
				'" style="position: absolute; top: -100px; width: 0px; z-index: -10;"/>');
			this._dialogInput.keydown(this._doKeyDown);
			jQuery('body').append(this._dialogInput);
			inst = this._dialogInst = this._newInst(this._dialogInput, false);
			inst.settings = {};
			jQuery.data(this._dialogInput[0], PROP_NAME, inst);
		}
		extendRemove(inst.settings, settings || {});
		date = (date && date.constructor == Date ? this._formatDate(inst, date) : date);
		this._dialogInput.val(date);

		this._pos = (pos ? (pos.length ? pos : [pos.pageX, pos.pageY]) : null);
		if (!this._pos) {
			var browserWidth = document.documentElement.clientWidth;
			var browserHeight = document.documentElement.clientHeight;
			var scrollX = document.documentElement.scrollLeft || document.body.scrollLeft;
			var scrollY = document.documentElement.scrollTop || document.body.scrollTop;
			this._pos = // should use actual width/height below
				[(browserWidth / 2) - 100 + scrollX, (browserHeight / 2) - 150 + scrollY];
		}

		// move input on screen for focus, but hidden behind dialog
		this._dialogInput.css('left', (this._pos[0] + 20) + 'px').css('top', this._pos[1] + 'px');
		inst.settings.onSelect = onSelect;
		this._inDialog = true;
		this.dpDiv.addClass(this._dialogClass);
		this._showDatepicker(this._dialogInput[0]);
		if (jQuery.blockUI)
			jQuery.blockUI(this.dpDiv);
		jQuery.data(this._dialogInput[0], PROP_NAME, inst);
		return this;
	},

	/* Detach a timecalendar from its control.
	   @param  target    element - the target input field or division or span */
	_destroyDatepicker: function(target) {
		var jQuerytarget = jQuery(target);
		var inst = jQuery.data(target, PROP_NAME);
		if (!jQuerytarget.hasClass(this.markerClassName)) {
			return;
		}
		var nodeName = target.nodeName.toLowerCase();
		jQuery.removeData(target, PROP_NAME);
		if (nodeName == 'input') {
			inst.append.remove();
			inst.trigger.remove();
			jQuerytarget.removeClass(this.markerClassName).
				unbind('focus', this._showDatepicker).
				unbind('keydown', this._doKeyDown).
				unbind('keypress', this._doKeyPress).
				unbind('keyup', this._doKeyUp);
		} else if (nodeName == 'div' || nodeName == 'span')
			jQuerytarget.removeClass(this.markerClassName).empty();
	},

	/* Enable the date picker to a jQuery selection.
	   @param  target    element - the target input field or division or span */
	_enableDatepicker: function(target) {
		var jQuerytarget = jQuery(target);
		var inst = jQuery.data(target, PROP_NAME);
		if (!jQuerytarget.hasClass(this.markerClassName)) {
			return;
		}
		var nodeName = target.nodeName.toLowerCase();
		if (nodeName == 'input') {
			target.disabled = false;
			inst.trigger.filter('button').
				each(function() { this.disabled = false; }).end().
				filter('img').css({opacity: '1.0', cursor: ''});
		}
		else if (nodeName == 'div' || nodeName == 'span') {
			var inline = jQuerytarget.children('.' + this._inlineClass);
			inline.children().removeClass('ui-state-disabled');
			inline.find("select.ui-timecalendar-month, select.ui-timecalendar-year").
				removeAttr("disabled");
		}
		this._disabledInputs = jQuery.map(this._disabledInputs,
			function(value) { return (value == target ? null : value); }); // delete entry
	},

	/* Disable the date picker to a jQuery selection.
	   @param  target    element - the target input field or division or span */
	_disableDatepicker: function(target) {
		var jQuerytarget = jQuery(target);
		var inst = jQuery.data(target, PROP_NAME);
		if (!jQuerytarget.hasClass(this.markerClassName)) {
			return;
		}
		var nodeName = target.nodeName.toLowerCase();
		if (nodeName == 'input') {
			target.disabled = true;
			inst.trigger.filter('button').
				each(function() { this.disabled = true; }).end().
				filter('img').css({opacity: '0.5', cursor: 'default'});
		}
		else if (nodeName == 'div' || nodeName == 'span') {
			var inline = jQuerytarget.children('.' + this._inlineClass);
			inline.children().addClass('ui-state-disabled');
			inline.find("select.ui-timecalendar-month, select.ui-timecalendar-year").
				attr("disabled", "disabled");
		}
		this._disabledInputs = jQuery.map(this._disabledInputs,
			function(value) { return (value == target ? null : value); }); // delete entry
		this._disabledInputs[this._disabledInputs.length] = target;
	},

	/* Is the first field in a jQuery collection disabled as a timecalendar?
	   @param  target    element - the target input field or division or span
	   @return boolean - true if disabled, false if enabled */
	_isDisabledDatepicker: function(target) {
		if (!target) {
			return false;
		}
		for (var i = 0; i < this._disabledInputs.length; i++) {
			if (this._disabledInputs[i] == target)
				return true;
		}
		return false;
	},

	/* Retrieve the instance data for the target control.
	   @param  target  element - the target input field or division or span
	   @return  object - the associated instance data
	   @throws  error if a jQuery problem getting data */
	_getInst: function(target) {
		try {
			return jQuery.data(target, PROP_NAME);
		}
		catch (err) {
			throw 'Missing instance data for this timecalendar';
		}
	},

	/* Update or retrieve the settings for a date picker attached to an input field or division.
	   @param  target  element - the target input field or division or span
	   @param  name    object - the new settings to update or
	                   string - the name of the setting to change or retrieve,
	                   when retrieving also 'all' for all instance settings or
	                   'defaults' for all global defaults
	   @param  value   any - the new value for the setting
	                   (omit if above is an object or to retrieve a value) */
	_optionDatepicker: function(target, name, value) {
		var inst = this._getInst(target);
		if (arguments.length == 2 && typeof name == 'string') {
			return (name == 'defaults' ? jQuery.extend({}, jQuery.timecalendar._defaults) :
				(inst ? (name == 'all' ? jQuery.extend({}, inst.settings) :
				this._get(inst, name)) : null));
		}
		var settings = name || {};
		if (typeof name == 'string') {
			settings = {};
			settings[name] = value;
		}
		if (inst) {
			if (this._curInst == inst) {
				this._hideDatepicker();
			}
			var date = this._getDateDatepicker(target, true);
			var minDate = this._getMinMaxDate(inst, 'min');
			var maxDate = this._getMinMaxDate(inst, 'max');
			extendRemove(inst.settings, settings);
			// reformat the old minDate/maxDate values if dateFormat changes and a new minDate/maxDate isn't provided
			if (minDate !== null && settings['dateFormat'] !== undefined && settings['minDate'] === undefined)
				inst.settings.minDate = this._formatDate(inst, minDate);
			if (maxDate !== null && settings['dateFormat'] !== undefined && settings['maxDate'] === undefined)
				inst.settings.maxDate = this._formatDate(inst, maxDate);
			this._attachments(jQuery(target), inst);
			this._autoSize(inst);
			this._setDate(inst, date);
			this._updateAlternate(inst);
			this._updateDatepicker(inst);
		}
	},

	// change method deprecated
	_changeDatepicker: function(target, name, value) {
		this._optionDatepicker(target, name, value);
	},

	/* Redraw the date picker attached to an input field or division.
	   @param  target  element - the target input field or division or span */
	_refreshDatepicker: function(target) {
		var inst = this._getInst(target);
		if (inst) {
			this._updateDatepicker(inst);
		}
	},

	/* Set the dates for a jQuery selection.
	   @param  target   element - the target input field or division or span
	   @param  date     Date - the new date */
	_setDateDatepicker: function(target, date) {
		var inst = this._getInst(target);
		if (inst) {
			this._setDate(inst, date);
			this._updateDatepicker(inst);
			this._updateAlternate(inst);
		}
	},

	/* Get the date(s) for the first entry in a jQuery selection.
	   @param  target     element - the target input field or division or span
	   @param  noDefault  boolean - true if no default date is to be used
	   @return Date - the current date */
	_getDateDatepicker: function(target, noDefault) {
		var inst = this._getInst(target);
		if (inst && !inst.inline)
			this._setDateFromField(inst, noDefault);
		return (inst ? this._getDate(inst) : null);
	},

	/* Handle keystrokes. */
	_doKeyDown: function(event) {
		var inst = jQuery.timecalendar._getInst(event.target);
		var handled = true;
		var isRTL = inst.dpDiv.is('.ui-timecalendar-rtl');
		inst._keyEvent = true;
		if (jQuery.timecalendar._datepickerShowing)
			switch (event.keyCode) {
				case 9: jQuery.timecalendar._hideDatepicker();
						handled = false;
						break; // hide on tab out
				case 13: var sel = jQuery('td.' + jQuery.timecalendar._dayOverClass + ':not(.' + 
									jQuery.timecalendar._currentClass + ')', inst.dpDiv);
						if (sel[0])
							jQuery.timecalendar._selectDay(event.target, inst.selectedMonth, inst.selectedYear, sel[0]);
							var onSelect = jQuery.timecalendar._get(inst, 'onSelect');
							if (onSelect) {
								var dateStr = jQuery.timecalendar._formatDate(inst);

								// trigger custom callback
								onSelect.apply((inst.input ? inst.input[0] : null), [dateStr, inst]);
							}
						else
							jQuery.timecalendar._hideDatepicker();
						return false; // don't submit the form
						break; // select the value on enter
				case 27: jQuery.timecalendar._hideDatepicker();
						break; // hide on escape
				case 33: jQuery.timecalendar._adjustDate(event.target, (event.ctrlKey ?
							-jQuery.timecalendar._get(inst, 'stepBigMonths') :
							-jQuery.timecalendar._get(inst, 'stepMonths')), 'M');
						break; // previous month/year on page up/+ ctrl
				case 34: jQuery.timecalendar._adjustDate(event.target, (event.ctrlKey ?
							+jQuery.timecalendar._get(inst, 'stepBigMonths') :
							+jQuery.timecalendar._get(inst, 'stepMonths')), 'M');
						break; // next month/year on page down/+ ctrl
				case 35: if (event.ctrlKey || event.metaKey) jQuery.timecalendar._clearDate(event.target);
						handled = event.ctrlKey || event.metaKey;
						break; // clear on ctrl or command +end
				case 36: if (event.ctrlKey || event.metaKey) jQuery.timecalendar._gotoToday(event.target);
						handled = event.ctrlKey || event.metaKey;
						break; // current on ctrl or command +home
				case 37: if (event.ctrlKey || event.metaKey) jQuery.timecalendar._adjustDate(event.target, (isRTL ? +1 : -1), 'D');
						handled = event.ctrlKey || event.metaKey;
						// -1 day on ctrl or command +left
						if (event.originalEvent.altKey) jQuery.timecalendar._adjustDate(event.target, (event.ctrlKey ?
									-jQuery.timecalendar._get(inst, 'stepBigMonths') :
									-jQuery.timecalendar._get(inst, 'stepMonths')), 'M');
						// next month/year on alt +left on Mac
						break;
				case 38: if (event.ctrlKey || event.metaKey) jQuery.timecalendar._adjustDate(event.target, -7, 'D');
						handled = event.ctrlKey || event.metaKey;
						break; // -1 week on ctrl or command +up
				case 39: if (event.ctrlKey || event.metaKey) jQuery.timecalendar._adjustDate(event.target, (isRTL ? -1 : +1), 'D');
						handled = event.ctrlKey || event.metaKey;
						// +1 day on ctrl or command +right
						if (event.originalEvent.altKey) jQuery.timecalendar._adjustDate(event.target, (event.ctrlKey ?
									+jQuery.timecalendar._get(inst, 'stepBigMonths') :
									+jQuery.timecalendar._get(inst, 'stepMonths')), 'M');
						// next month/year on alt +right
						break;
				case 40: if (event.ctrlKey || event.metaKey) jQuery.timecalendar._adjustDate(event.target, +7, 'D');
						handled = event.ctrlKey || event.metaKey;
						break; // +1 week on ctrl or command +down
				default: handled = false;
			}
		else if (event.keyCode == 36 && event.ctrlKey) // display the date picker on ctrl+home
			jQuery.timecalendar._showDatepicker(this);
		else {
			handled = false;
		}
		if (handled) {
			event.preventDefault();
			event.stopPropagation();
		}
	},

	/* Filter entered characters - based on date format. */
	_doKeyPress: function(event) {
		var inst = jQuery.timecalendar._getInst(event.target);
		if (jQuery.timecalendar._get(inst, 'constrainInput')) {
			var chars = jQuery.timecalendar._possibleChars(jQuery.timecalendar._get(inst, 'dateFormat'));
			var chr = String.fromCharCode(event.charCode == undefined ? event.keyCode : event.charCode);
			return event.ctrlKey || event.metaKey || (chr < ' ' || !chars || chars.indexOf(chr) > -1);
		}
	},

	/* Synchronise manual entry and field/alternate field. */
	_doKeyUp: function(event) {
		var inst = jQuery.timecalendar._getInst(event.target);
		if (inst.input.val() != inst.lastVal) {
			try {
				var date = jQuery.timecalendar.parseDate(jQuery.timecalendar._get(inst, 'dateFormat'),
					(inst.input ? inst.input.val() : null),
					jQuery.timecalendar._getFormatConfig(inst));
				if (date) { // only if valid
					jQuery.timecalendar._setDateFromField(inst);
					jQuery.timecalendar._updateAlternate(inst);
					jQuery.timecalendar._updateDatepicker(inst);
				}
			}
			catch (err) {
				jQuery.timecalendar.log(err);
			}
		}
		return true;
	},

	/* Pop-up the date picker for a given input field.
       If false returned from beforeShow event handler do not show. 
	   @param  input  element - the input field attached to the date picker or
	                  event - if triggered by focus */
	_showDatepicker: function(input) {
		input = input.target || input;
		if (input.nodeName.toLowerCase() != 'input') // find from button/image trigger
			input = jQuery('input', input.parentNode)[0];
		if (jQuery.timecalendar._isDisabledDatepicker(input) || jQuery.timecalendar._lastInput == input) // already here
			return;
		var inst = jQuery.timecalendar._getInst(input);
		if (jQuery.timecalendar._curInst && jQuery.timecalendar._curInst != inst) {
			jQuery.timecalendar._curInst.dpDiv.stop(true, true);
			if ( inst && jQuery.timecalendar._datepickerShowing ) {
				jQuery.timecalendar._hideDatepicker( jQuery.timecalendar._curInst.input[0] );
			}
		}
		var beforeShow = jQuery.timecalendar._get(inst, 'beforeShow');
		var beforeShowSettings = beforeShow ? beforeShow.apply(input, [input, inst]) : {};
		if(beforeShowSettings === false){
            //false
			return;
		}
		extendRemove(inst.settings, beforeShowSettings);
		inst.lastVal = null;
		jQuery.timecalendar._lastInput = input;
		jQuery.timecalendar._setDateFromField(inst);
		if (jQuery.timecalendar._inDialog) // hide cursor
			input.value = '';
		if (!jQuery.timecalendar._pos) { // position below input
			jQuery.timecalendar._pos = jQuery.timecalendar._findPos(input);
			jQuery.timecalendar._pos[1] += input.offsetHeight; // add the height
		}
		var isFixed = false;
		jQuery(input).parents().each(function() {
			isFixed |= jQuery(this).css('position') == 'fixed';
			return !isFixed;
		});
		if (isFixed && jQuery.browser.opera) { // correction for Opera when fixed and scrolled
			jQuery.timecalendar._pos[0] -= document.documentElement.scrollLeft;
			jQuery.timecalendar._pos[1] -= document.documentElement.scrollTop;
		}
		var offset = {left: jQuery.timecalendar._pos[0], top: jQuery.timecalendar._pos[1]};
		jQuery.timecalendar._pos = null;
		//to avoid flashes on Firefox
		inst.dpDiv.empty();
		// determine sizing offscreen
		inst.dpDiv.css({position: 'absolute', display: 'block', top: '-1000px'});
		jQuery.timecalendar._updateDatepicker(inst);
		// fix width for dynamic number of date pickers
		// and adjust position before showing
		offset = jQuery.timecalendar._checkOffset(inst, offset, isFixed);
		inst.dpDiv.css({position: (jQuery.timecalendar._inDialog && jQuery.blockUI ?
			'static' : (isFixed ? 'fixed' : 'absolute')), display: 'none',
			left: offset.left + 'px', top: offset.top + 'px'});
		if (!inst.inline) {
			var showAnim = jQuery.timecalendar._get(inst, 'showAnim');
			var duration = jQuery.timecalendar._get(inst, 'duration');
			var postProcess = function() {
				var cover = inst.dpDiv.find('iframe.ui-timecalendar-cover'); // IE6- only
				if( !! cover.length ){
					var borders = jQuery.timecalendar._getBorders(inst.dpDiv);
					cover.css({left: -borders[0], top: -borders[1],
						width: inst.dpDiv.outerWidth(), height: inst.dpDiv.outerHeight()});
				}
			};
			inst.dpDiv.zIndex(jQuery(input).zIndex()+1);
			jQuery.timecalendar._datepickerShowing = true;
			if (jQuery.effects && jQuery.effects[showAnim])
				inst.dpDiv.show(showAnim, jQuery.timecalendar._get(inst, 'showOptions'), duration, postProcess);
			else
				inst.dpDiv[showAnim || 'show']((showAnim ? duration : null), postProcess);
			if (!showAnim || !duration)
				postProcess();
			if (inst.input.is(':visible') && !inst.input.is(':disabled'))
				inst.input.focus();
			jQuery.timecalendar._curInst = inst;
		}
	},

	/* Generate the date picker content. */
	_updateDatepicker: function(inst) {
		var self = this;
		self.maxRows = 4; //Reset the max number of rows being displayed (see #7043)
		var borders = jQuery.timecalendar._getBorders(inst.dpDiv);
		instActive = inst; // for delegate hover events
		inst.dpDiv.empty().append(this._generateHTML(inst));
		var cover = inst.dpDiv.find('iframe.ui-timecalendar-cover'); // IE6- only
		if( !!cover.length ){ //avoid call to outerXXXX() when not in IE6
			cover.css({left: -borders[0], top: -borders[1], width: inst.dpDiv.outerWidth(), height: inst.dpDiv.outerHeight()})
		}
		inst.dpDiv.find('.' + this._dayOverClass + ' a').mouseover();
		var numMonths = this._getNumberOfMonths(inst);
		var cols = numMonths[1];
		var width = 17;
		inst.dpDiv.removeClass('ui-timecalendar-multi-2 ui-timecalendar-multi-3 ui-timecalendar-multi-4').width('');
		if (cols > 1)
			inst.dpDiv.addClass('ui-timecalendar-multi-' + cols).css('width', (width * cols) + 'em');
		inst.dpDiv[(numMonths[0] != 1 || numMonths[1] != 1 ? 'add' : 'remove') +
			'Class']('ui-timecalendar-multi');
		inst.dpDiv[(this._get(inst, 'isRTL') ? 'add' : 'remove') +
			'Class']('ui-timecalendar-rtl');
		if (inst == jQuery.timecalendar._curInst && jQuery.timecalendar._datepickerShowing && inst.input &&
				// #6694 - don't focus the input if it's already focused
				// this breaks the change event in IE
				inst.input.is(':visible') && !inst.input.is(':disabled') && inst.input[0] != document.activeElement)
			inst.input.focus();
		// deffered render of the years select (to avoid flashes on Firefox) 
		if( inst.yearshtml ){
			var origyearshtml = inst.yearshtml;
			setTimeout(function(){
				//assure that inst.yearshtml didn't change.
				if( origyearshtml === inst.yearshtml && inst.yearshtml ){
					inst.dpDiv.find('select.ui-timecalendar-year:first').replaceWith(inst.yearshtml);
				}
				origyearshtml = inst.yearshtml = null;
			}, 0);
		}
	},

	/* Retrieve the size of left and top borders for an element.
	   @param  elem  (jQuery object) the element of interest
	   @return  (number[2]) the left and top borders */
	_getBorders: function(elem) {
		var convert = function(value) {
			return {thin: 1, medium: 2, thick: 3}[value] || value;
		};
		return [parseFloat(convert(elem.css('border-left-width'))),
			parseFloat(convert(elem.css('border-top-width')))];
	},

	/* Check positioning to remain on screen. */
	_checkOffset: function(inst, offset, isFixed) {
		var dpWidth = inst.dpDiv.outerWidth();
		var dpHeight = inst.dpDiv.outerHeight();
		var inputWidth = inst.input ? inst.input.outerWidth() : 0;
		var inputHeight = inst.input ? inst.input.outerHeight() : 0;
		var viewWidth = document.documentElement.clientWidth + jQuery(document).scrollLeft();
		var viewHeight = document.documentElement.clientHeight + jQuery(document).scrollTop();

		offset.left -= (this._get(inst, 'isRTL') ? (dpWidth - inputWidth) : 0);
		offset.left -= (isFixed && offset.left == inst.input.offset().left) ? jQuery(document).scrollLeft() : 0;
		offset.top -= (isFixed && offset.top == (inst.input.offset().top + inputHeight)) ? jQuery(document).scrollTop() : 0;

		// now check if timecalendar is showing outside window viewport - move to a better place if so.
		offset.left -= Math.min(offset.left, (offset.left + dpWidth > viewWidth && viewWidth > dpWidth) ?
			Math.abs(offset.left + dpWidth - viewWidth) : 0);
		offset.top -= Math.min(offset.top, (offset.top + dpHeight > viewHeight && viewHeight > dpHeight) ?
			Math.abs(dpHeight + inputHeight) : 0);

		return offset;
	},

	/* Find an object's position on the screen. */
	_findPos: function(obj) {
		var inst = this._getInst(obj);
		var isRTL = this._get(inst, 'isRTL');
        while (obj && (obj.type == 'hidden' || obj.nodeType != 1 || jQuery.expr.filters.hidden(obj))) {
            obj = obj[isRTL ? 'previousSibling' : 'nextSibling'];
        }
        var position = jQuery(obj).offset();
	    return [position.left, position.top];
	},

	/* Hide the date picker from view.
	   @param  input  element - the input field attached to the date picker */
	_hideDatepicker: function(input) {
		var inst = this._curInst;
		if (!inst || (input && inst != jQuery.data(input, PROP_NAME)))
			return;
		if (this._datepickerShowing) {
			var showAnim = this._get(inst, 'showAnim');
			var duration = this._get(inst, 'duration');
			var postProcess = function() {
				jQuery.timecalendar._tidyDialog(inst);
			};
			if (jQuery.effects && jQuery.effects[showAnim])
				inst.dpDiv.hide(showAnim, jQuery.timecalendar._get(inst, 'showOptions'), duration, postProcess);
			else
				inst.dpDiv[(showAnim == 'slideDown' ? 'slideUp' :
					(showAnim == 'fadeIn' ? 'fadeOut' : 'hide'))]((showAnim ? duration : null), postProcess);
			if (!showAnim)
				postProcess();
			this._datepickerShowing = false;
			var onClose = this._get(inst, 'onClose');
			if (onClose)
				onClose.apply((inst.input ? inst.input[0] : null),
					[(inst.input ? inst.input.val() : ''), inst]);
			this._lastInput = null;
			if (this._inDialog) {
				this._dialogInput.css({ position: 'absolute', left: '0', top: '-100px' });
				if (jQuery.blockUI) {
					jQuery.unblockUI();
					jQuery('body').append(this.dpDiv);
				}
			}
			this._inDialog = false;
		}
	},

	/* Tidy up after a dialog display. */
	_tidyDialog: function(inst) {
		inst.dpDiv.removeClass(this._dialogClass).unbind('.ui-timecalendar-calendar');
	},

	/* Close date picker if clicked elsewhere. */
	_checkExternalClick: function(event) {
		if (!jQuery.timecalendar._curInst)
			return;

		var jQuerytarget = jQuery(event.target),
			inst = jQuery.timecalendar._getInst(jQuerytarget[0]);

		if ( ( ( jQuerytarget[0].id != jQuery.timecalendar._mainDivId &&
				jQuerytarget.parents('#' + jQuery.timecalendar._mainDivId).length == 0 &&
				!jQuerytarget.hasClass(jQuery.timecalendar.markerClassName) &&
				!jQuerytarget.closest("." + jQuery.timecalendar._triggerClass).length &&
				jQuery.timecalendar._datepickerShowing && !(jQuery.timecalendar._inDialog && jQuery.blockUI) ) ) ||
			( jQuerytarget.hasClass(jQuery.timecalendar.markerClassName) && jQuery.timecalendar._curInst != inst ) )
			jQuery.timecalendar._hideDatepicker();
	},

	/* Adjust one of the date sub-fields. */
	_adjustDate: function(id, offset, period) {
		var target = jQuery(id);
		var inst = this._getInst(target[0]);
		if (this._isDisabledDatepicker(target[0])) {
			return;
		}
		this._adjustInstDate(inst, offset +
			(period == 'M' ? this._get(inst, 'showCurrentAtPos') : 0), // undo positioning
			period);
		this._updateDatepicker(inst);
	},

	/* Action for current link. */
	_gotoToday: function(id) {
		var target = jQuery(id);
		var inst = this._getInst(target[0]);
		if (this._get(inst, 'gotoCurrent') && inst.currentDay) {
			inst.selectedDay = inst.currentDay;
			inst.drawMonth = inst.selectedMonth = inst.currentMonth;
			inst.drawYear = inst.selectedYear = inst.currentYear;
		}
		else {
			var date = new Date();
			inst.selectedDay = date.getDate();
			inst.drawMonth = inst.selectedMonth = date.getMonth();
			inst.drawYear = inst.selectedYear = date.getFullYear();
		}
		this._notifyChange(inst);
		this._adjustDate(target);
	},

	/* Action for selecting a new month/year. */
	_selectMonthYear: function(id, select, period) {
		var target = jQuery(id);
		var inst = this._getInst(target[0]);
		inst['selected' + (period == 'M' ? 'Month' : 'Year')] =
		inst['draw' + (period == 'M' ? 'Month' : 'Year')] =
			parseInt(select.options[select.selectedIndex].value,10);
		this._notifyChange(inst);
		this._adjustDate(target);
	},

	/* Action for selecting a day. */
	_selectDay: function(id, month, year, td) {
		var target = jQuery(id);
		if (jQuery(td).hasClass(this._unselectableClass) || this._isDisabledDatepicker(target[0])) {
			return;
		}
		var inst = this._getInst(target[0]);
		inst.selectedDay = inst.currentDay = jQuery('a', td).html();
		inst.selectedMonth = inst.currentMonth = month;
		inst.selectedYear = inst.currentYear = year;
		this._selectDate(id, this._formatDate(inst,inst.currentDay, inst.currentMonth, inst.currentYear));
		
		//new Date(inst.currentYear, inst.currentMonth, inst.currentDay, 0, 0, 0, 0)
		
		this._customSelectDayAction(new Date(inst.currentYear, inst.currentMonth, inst.currentDay, 0, 0, 0, 0));
	},

	/* Erase the input field and hide the date picker. */
	_clearDate: function(id) {
		var target = jQuery(id);
		var inst = this._getInst(target[0]);
		this._selectDate(target, '');
	},

	/* Update the input field with the selected date. */
	_selectDate: function(id, dateStr) {
		var target = jQuery(id);
		var inst = this._getInst(target[0]);
		dateStr = (dateStr != null ? dateStr : this._formatDate(inst));
		if (inst.input)
			inst.input.val(dateStr);
		this._updateAlternate(inst);
		var onSelect = this._get(inst, 'onSelect');
		if (onSelect)
			onSelect.apply((inst.input ? inst.input[0] : null), [dateStr, inst]);  // trigger custom callback
		else if (inst.input)
			inst.input.trigger('change'); // fire the change event
		if (inst.inline)
			this._updateDatepicker(inst);
		else {
			this._hideDatepicker();
			this._lastInput = inst.input[0];
			if (typeof(inst.input[0]) != 'object')
				inst.input.focus(); // restore focus
			this._lastInput = null;
		}
//		this._customSelectDayAction(dateStr);
	},

	/* Update any alternate field to synchronise with the main field. */
	_updateAlternate: function(inst) {
		var altField = this._get(inst, 'altField');
		if (altField) { // update alternate field too
			var altFormat = this._get(inst, 'altFormat') || this._get(inst, 'dateFormat');
			var date = this._getDate(inst);
			var dateStr = this.formatDate(altFormat, date, this._getFormatConfig(inst));
			jQuery(altField).each(function() { jQuery(this).val(dateStr); });
		}
	},

	/* Set as beforeShowDay function to prevent selection of weekends.
	   @param  date  Date - the date to customise
	   @return [boolean, string] - is this date selectable?, what is its CSS class? */
	noWeekends: function(date) {
		var day = date.getDay();
		return [(day > 0 && day < 6), ''];
	},

	/* Set as calculateWeek to determine the week of the year based on the ISO 8601 definition.
	   @param  date  Date - the date to get the week for
	   @return  number - the number of the week within the year that contains this date */
	iso8601Week: function(date) {
		var checkDate = new Date(date.getTime());
		// Find Thursday of this week starting on Monday
		checkDate.setDate(checkDate.getDate() + 4 - (checkDate.getDay() || 7));
		var time = checkDate.getTime();
		checkDate.setMonth(0); // Compare with Jan 1
		checkDate.setDate(1);
		return Math.floor(Math.round((time - checkDate) / 86400000) / 7) + 1;
	},

	/* Parse a string value into a date object.
	   See formatDate below for the possible formats.

	   @param  format    string - the expected format of the date
	   @param  value     string - the date in the above format
	   @param  settings  Object - attributes include:
	                     shortYearCutoff  number - the cutoff year for determining the century (optional)
	                     dayNamesShort    string[7] - abbreviated names of the days from Sunday (optional)
	                     dayNames         string[7] - names of the days from Sunday (optional)
	                     monthNamesShort  string[12] - abbreviated names of the months (optional)
	                     monthNames       string[12] - names of the months (optional)
	   @return  Date - the extracted date value or null if value is blank */
	parseDate: function (format, value, settings) {
		if (format == null || value == null)
			throw 'Invalid arguments';
		value = (typeof value == 'object' ? value.toString() : value + '');
		if (value == '')
			return null;
		var shortYearCutoff = (settings ? settings.shortYearCutoff : null) || this._defaults.shortYearCutoff;
		shortYearCutoff = (typeof shortYearCutoff != 'string' ? shortYearCutoff :
				new Date().getFullYear() % 100 + parseInt(shortYearCutoff, 10));
		var dayNamesShort = (settings ? settings.dayNamesShort : null) || this._defaults.dayNamesShort;
		var dayNames = (settings ? settings.dayNames : null) || this._defaults.dayNames;
		var monthNamesShort = (settings ? settings.monthNamesShort : null) || this._defaults.monthNamesShort;
		var monthNames = (settings ? settings.monthNames : null) || this._defaults.monthNames;
		var year = -1;
		var month = -1;
		var day = -1;
		var doy = -1;
		var literal = false;
		// Check whether a format character is doubled
		var lookAhead = function(match) {
			var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) == match);
			if (matches)
				iFormat++;
			return matches;
		};
		// Extract a number from the string value
		var getNumber = function(match) {
			var isDoubled = lookAhead(match);
			var size = (match == '@' ? 14 : (match == '!' ? 20 :
				(match == 'y' && isDoubled ? 4 : (match == 'o' ? 3 : 2))));
			var digits = new RegExp('^\\d{1,' + size + '}');
			var num = value.substring(iValue).match(digits);
			if (!num)
				throw 'Missing number at position ' + iValue;
			iValue += num[0].length;
			return parseInt(num[0], 10);
		};
		// Extract a name from the string value and convert to an index
		var getName = function(match, shortNames, longNames) {
			var names = jQuery.map(lookAhead(match) ? longNames : shortNames, function (v, k) {
				return [ [k, v] ];
			}).sort(function (a, b) {
				return -(a[1].length - b[1].length);
			});
			var index = -1;
			jQuery.each(names, function (i, pair) {
				var name = pair[1];
				if (value.substr(iValue, name.length).toLowerCase() == name.toLowerCase()) {
					index = pair[0];
					iValue += name.length;
					return false;
				}
			});
			if (index != -1)
				return index + 1;
			else
				throw 'Unknown name at position ' + iValue;
		};
		// Confirm that a literal character matches the string value
		var checkLiteral = function() {
			if (value.charAt(iValue) != format.charAt(iFormat))
				throw 'Unexpected literal at position ' + iValue;
			iValue++;
		};
		var iValue = 0;
		for (var iFormat = 0; iFormat < format.length; iFormat++) {
			if (literal)
				if (format.charAt(iFormat) == "'" && !lookAhead("'"))
					literal = false;
				else
					checkLiteral();
			else
				switch (format.charAt(iFormat)) {
					case 'd':
						day = getNumber('d');
						break;
					case 'D':
						getName('D', dayNamesShort, dayNames);
						break;
					case 'o':
						doy = getNumber('o');
						break;
					case 'm':
						month = getNumber('m');
						break;
					case 'M':
						month = getName('M', monthNamesShort, monthNames);
						break;
					case 'y':
						year = getNumber('y');
						break;
					case '@':
						var date = new Date(getNumber('@'));
						year = date.getFullYear();
						month = date.getMonth() + 1;
						day = date.getDate();
						break;
					case '!':
						var date = new Date((getNumber('!') - this._ticksTo1970) / 10000);
						year = date.getFullYear();
						month = date.getMonth() + 1;
						day = date.getDate();
						break;
					case "'":
						if (lookAhead("'"))
							checkLiteral();
						else
							literal = true;
						break;
					default:
						checkLiteral();
				}
		}
		if (iValue < value.length){
			throw "Extra/unparsed characters found in date: " + value.substring(iValue);
		}
		if (year == -1)
			year = new Date().getFullYear();
		else if (year < 100)
			year += new Date().getFullYear() - new Date().getFullYear() % 100 +
				(year <= shortYearCutoff ? 0 : -100);
		if (doy > -1) {
			month = 1;
			day = doy;
			do {
				var dim = this._getDaysInMonth(year, month - 1);
				if (day <= dim)
					break;
				month++;
				day -= dim;
			} while (true);
		}
		var date = this._daylightSavingAdjust(new Date(year, month - 1, day));
		if (date.getFullYear() != year || date.getMonth() + 1 != month || date.getDate() != day)
			throw 'Invalid date'; // E.g. 31/02/00
		return date;
	},

	/* Standard date formats. */
	ATOM: 'yy-mm-dd', // RFC 3339 (ISO 8601)
	COOKIE: 'D, dd M yy',
	ISO_8601: 'yy-mm-dd',
	RFC_822: 'D, d M y',
	RFC_850: 'DD, dd-M-y',
	RFC_1036: 'D, d M y',
	RFC_1123: 'D, d M yy',
	RFC_2822: 'D, d M yy',
	RSS: 'D, d M y', // RFC 822
	TICKS: '!',
	TIMESTAMP: '@',
	W3C: 'yy-mm-dd', // ISO 8601

	_ticksTo1970: (((1970 - 1) * 365 + Math.floor(1970 / 4) - Math.floor(1970 / 100) +
		Math.floor(1970 / 400)) * 24 * 60 * 60 * 10000000),

	/* Format a date object into a string value.
	   The format can be combinations of the following:
	   d  - day of month (no leading zero)
	   dd - day of month (two digit)
	   o  - day of year (no leading zeros)
	   oo - day of year (three digit)
	   D  - day name short
	   DD - day name long
	   m  - month of year (no leading zero)
	   mm - month of year (two digit)
	   M  - month name short
	   MM - month name long
	   y  - year (two digit)
	   yy - year (four digit)
	   @ - Unix timestamp (ms since 01/01/1970)
	   ! - Windows ticks (100ns since 01/01/0001)
	   '...' - literal text
	   '' - single quote

	   @param  format    string - the desired format of the date
	   @param  date      Date - the date value to format
	   @param  settings  Object - attributes include:
	                     dayNamesShort    string[7] - abbreviated names of the days from Sunday (optional)
	                     dayNames         string[7] - names of the days from Sunday (optional)
	                     monthNamesShort  string[12] - abbreviated names of the months (optional)
	                     monthNames       string[12] - names of the months (optional)
	   @return  string - the date in the above format */
	formatDate: function (format, date, settings) {
		if (!date)
			return '';
		var dayNamesShort = (settings ? settings.dayNamesShort : null) || this._defaults.dayNamesShort;
		var dayNames = (settings ? settings.dayNames : null) || this._defaults.dayNames;
		var monthNamesShort = (settings ? settings.monthNamesShort : null) || this._defaults.monthNamesShort;
		var monthNames = (settings ? settings.monthNames : null) || this._defaults.monthNames;
		// Check whether a format character is doubled
		var lookAhead = function(match) {
			var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) == match);
			if (matches)
				iFormat++;
			return matches;
		};
		// Format a number, with leading zero if necessary
		var formatNumber = function(match, value, len) {
			var num = '' + value;
			if (lookAhead(match))
				while (num.length < len)
					num = '0' + num;
			return num;
		};
		// Format a name, short or long as requested
		var formatName = function(match, value, shortNames, longNames) {
			return (lookAhead(match) ? longNames[value] : shortNames[value]);
		};
		var output = '';
		var literal = false;
		if (date)
			for (var iFormat = 0; iFormat < format.length; iFormat++) {
				if (literal)
					if (format.charAt(iFormat) == "'" && !lookAhead("'"))
						literal = false;
					else
						output += format.charAt(iFormat);
				else
					switch (format.charAt(iFormat)) {
						case 'd':
							output += formatNumber('d', date.getDate(), 2);
							break;
						case 'D':
							output += formatName('D', date.getDay(), dayNamesShort, dayNames);
							break;
						case 'o':
							output += formatNumber('o',
								Math.round((new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime() - new Date(date.getFullYear(), 0, 0).getTime()) / 86400000), 3);
							break;
						case 'm':
							output += formatNumber('m', date.getMonth() + 1, 2);
							break;
						case 'M':
							output += formatName('M', date.getMonth(), monthNamesShort, monthNames);
							break;
						case 'y':
							output += (lookAhead('y') ? date.getFullYear() :
								(date.getYear() % 100 < 10 ? '0' : '') + date.getYear() % 100);
							break;
						case '@':
							output += date.getTime();
							break;
						case '!':
							output += date.getTime() * 10000 + this._ticksTo1970;
							break;
						case "'":
							if (lookAhead("'"))
								output += "'";
							else
								literal = true;
							break;
						default:
							output += format.charAt(iFormat);
					}
			}
		return output;
	},

	/* Extract all possible characters from the date format. */
	_possibleChars: function (format) {
		var chars = '';
		var literal = false;
		// Check whether a format character is doubled
		var lookAhead = function(match) {
			var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) == match);
			if (matches)
				iFormat++;
			return matches;
		};
		for (var iFormat = 0; iFormat < format.length; iFormat++)
			if (literal)
				if (format.charAt(iFormat) == "'" && !lookAhead("'"))
					literal = false;
				else
					chars += format.charAt(iFormat);
			else
				switch (format.charAt(iFormat)) {
					case 'd': case 'm': case 'y': case '@':
						chars += '0123456789';
						break;
					case 'D': case 'M':
						return null; // Accept anything
					case "'":
						if (lookAhead("'"))
							chars += "'";
						else
							literal = true;
						break;
					default:
						chars += format.charAt(iFormat);
				}
		return chars;
	},

	/* Get a setting value, defaulting if necessary. */
	_get: function(inst, name) {
		return inst.settings[name] !== undefined ?
			inst.settings[name] : this._defaults[name];
	},

	/* Parse existing date and initialise date picker. */
	_setDateFromField: function(inst, noDefault) {
		if (inst.input.val() == inst.lastVal) {
			return;
		}
		var dateFormat = this._get(inst, 'dateFormat');
		var dates = inst.lastVal = inst.input ? inst.input.val() : null;
		var date, defaultDate;
		date = defaultDate = this._getDefaultDate(inst);
		var settings = this._getFormatConfig(inst);
		try {
			date = this.parseDate(dateFormat, dates, settings) || defaultDate;
		} catch (event) {
			this.log(event);
			dates = (noDefault ? '' : dates);
		}
		inst.selectedDay = date.getDate();
		inst.drawMonth = inst.selectedMonth = date.getMonth();
		inst.drawYear = inst.selectedYear = date.getFullYear();
		inst.currentDay = (dates ? date.getDate() : 0);
		inst.currentMonth = (dates ? date.getMonth() : 0);
		inst.currentYear = (dates ? date.getFullYear() : 0);
		this._adjustInstDate(inst);
	},

	/* Retrieve the default date shown on opening. */
	_getDefaultDate: function(inst) {
		return this._restrictMinMax(inst,
			this._determineDate(inst, this._get(inst, 'defaultDate'), new Date()));
	},

	/* A date may be specified as an exact value or a relative one. */
	_determineDate: function(inst, date, defaultDate) {
		var offsetNumeric = function(offset) {
			var date = new Date();
			date.setDate(date.getDate() + offset);
			return date;
		};
		var offsetString = function(offset) {
			try {
				return jQuery.timecalendar.parseDate(jQuery.timecalendar._get(inst, 'dateFormat'),
					offset, jQuery.timecalendar._getFormatConfig(inst));
			}
			catch (e) {
				// Ignore
			}
			var date = (offset.toLowerCase().match(/^c/) ?
				jQuery.timecalendar._getDate(inst) : null) || new Date();
			var year = date.getFullYear();
			var month = date.getMonth();
			var day = date.getDate();
			var pattern = /([+-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g;
			var matches = pattern.exec(offset);
			while (matches) {
				switch (matches[2] || 'd') {
					case 'd' : case 'D' :
						day += parseInt(matches[1],10); break;
					case 'w' : case 'W' :
						day += parseInt(matches[1],10) * 7; break;
					case 'm' : case 'M' :
						month += parseInt(matches[1],10);
						day = Math.min(day, jQuery.timecalendar._getDaysInMonth(year, month));
						break;
					case 'y': case 'Y' :
						year += parseInt(matches[1],10);
						day = Math.min(day, jQuery.timecalendar._getDaysInMonth(year, month));
						break;
				}
				matches = pattern.exec(offset);
			}
			return new Date(year, month, day);
		};
		var newDate = (date == null || date === '' ? defaultDate : (typeof date == 'string' ? offsetString(date) :
			(typeof date == 'number' ? (isNaN(date) ? defaultDate : offsetNumeric(date)) : new Date(date.getTime()))));
		newDate = (newDate && newDate.toString() == 'Invalid Date' ? defaultDate : newDate);
		if (newDate) {
			newDate.setHours(0);
			newDate.setMinutes(0);
			newDate.setSeconds(0);
			newDate.setMilliseconds(0);
		}
		return this._daylightSavingAdjust(newDate);
	},

	/* Handle switch to/from daylight saving.
	   Hours may be non-zero on daylight saving cut-over:
	   > 12 when midnight changeover, but then cannot generate
	   midnight datetime, so jump to 1AM, otherwise reset.
	   @param  date  (Date) the date to check
	   @return  (Date) the corrected date */
	_daylightSavingAdjust: function(date) {
		if (!date) return null;
		date.setHours(date.getHours() > 12 ? date.getHours() + 2 : 0);
		return date;
	},

	/* Set the date(s) directly. */
	_setDate: function(inst, date, noChange) {
		var clear = !date;
		var origMonth = inst.selectedMonth;
		var origYear = inst.selectedYear;
		var newDate = this._restrictMinMax(inst, this._determineDate(inst, date, new Date()));
		inst.selectedDay = inst.currentDay = newDate.getDate();
		inst.drawMonth = inst.selectedMonth = inst.currentMonth = newDate.getMonth();
		inst.drawYear = inst.selectedYear = inst.currentYear = newDate.getFullYear();
		if ((origMonth != inst.selectedMonth || origYear != inst.selectedYear) && !noChange)
			this._notifyChange(inst);
		this._adjustInstDate(inst);
		if (inst.input) {
			inst.input.val(clear ? '' : this._formatDate(inst));
		}
	},

	/* Retrieve the date(s) directly. */
	_getDate: function(inst) {
		var startDate = (!inst.currentYear || (inst.input && inst.input.val() == '') ? null :
			this._daylightSavingAdjust(new Date(
			inst.currentYear, inst.currentMonth, inst.currentDay)));
			return startDate;
	},

	/* Generate the HTML for the current state of the date picker. */
	_generateHTML: function(inst) {
		var today = new Date();
		today = this._daylightSavingAdjust(
			new Date(today.getFullYear(), today.getMonth(), today.getDate())); // clear time
		var isRTL = this._get(inst, 'isRTL');
		var showButtonPanel = this._get(inst, 'showButtonPanel');
		var hideIfNoPrevNext = this._get(inst, 'hideIfNoPrevNext');
		var navigationAsDateFormat = this._get(inst, 'navigationAsDateFormat');
		var numMonths = this._getNumberOfMonths(inst);
		var showCurrentAtPos = this._get(inst, 'showCurrentAtPos');
		var stepMonths = this._get(inst, 'stepMonths');
		var isMultiMonth = (numMonths[0] != 1 || numMonths[1] != 1);
		var currentDate = this._daylightSavingAdjust((!inst.currentDay ? new Date(9999, 9, 9) :
			new Date(inst.currentYear, inst.currentMonth, inst.currentDay,0,0,0,0)));
		var minDate = this._getMinMaxDate(inst, 'min');
		var maxDate = this._getMinMaxDate(inst, 'max');
		var drawMonth = inst.drawMonth - showCurrentAtPos;
		var drawYear = inst.drawYear;
		if (drawMonth < 0) {
			drawMonth += 12;
			drawYear--;
		}
		if (maxDate) {
			var maxDraw = this._daylightSavingAdjust(new Date(maxDate.getFullYear(),
				maxDate.getMonth() - (numMonths[0] * numMonths[1]) + 1, maxDate.getDate()));
			maxDraw = (minDate && maxDraw < minDate ? minDate : maxDraw);
			while (this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1)) > maxDraw) {
				drawMonth--;
				if (drawMonth < 0) {
					drawMonth = 11;
					drawYear--;
				}
			}
		}
		inst.drawMonth = drawMonth;
		inst.drawYear = drawYear;
		var prevText = this._get(inst, 'prevText');
		prevText = (!navigationAsDateFormat ? prevText : this.formatDate(prevText,
			this._daylightSavingAdjust(new Date(drawYear, drawMonth - stepMonths, 1)),
			this._getFormatConfig(inst)));
		var prev = (this._canAdjustMonth(inst, -1, drawYear, drawMonth) ?
			'<a class="ui-timecalendar-prev ui-corner-all" onclick="DP_jQuery_' + dpuuid +
			'.timecalendar._adjustDate(\'#' + inst.id + '\', -' + stepMonths + ', \'M\');"' +
			' title="' + prevText + '"><span class="ui-icon ui-icon-circle-triangle-' + ( isRTL ? 'e' : 'w') + '">' + prevText + '</span></a>' :
			(hideIfNoPrevNext ? '' : '<a class="ui-timecalendar-prev ui-corner-all ui-state-disabled" title="'+ prevText +'"><span class="ui-icon ui-icon-circle-triangle-' + ( isRTL ? 'e' : 'w') + '">' + prevText + '</span></a>'));
		var nextText = this._get(inst, 'nextText');
		nextText = (!navigationAsDateFormat ? nextText : this.formatDate(nextText,
			this._daylightSavingAdjust(new Date(drawYear, drawMonth + stepMonths, 1)),
			this._getFormatConfig(inst)));
		var next = (this._canAdjustMonth(inst, +1, drawYear, drawMonth) ?
			'<a class="ui-timecalendar-next ui-corner-all" onclick="DP_jQuery_' + dpuuid +
			'.timecalendar._adjustDate(\'#' + inst.id + '\', +' + stepMonths + ', \'M\');"' +
			' title="' + nextText + '"><span class="ui-icon ui-icon-circle-triangle-' + ( isRTL ? 'w' : 'e') + '">' + nextText + '</span></a>' :
			(hideIfNoPrevNext ? '' : '<a class="ui-timecalendar-next ui-corner-all ui-state-disabled" title="'+ nextText + '"><span class="ui-icon ui-icon-circle-triangle-' + ( isRTL ? 'w' : 'e') + '">' + nextText + '</span></a>'));
		var currentText = this._get(inst, 'currentText');
		var gotoDate = (this._get(inst, 'gotoCurrent') && inst.currentDay ? currentDate : today);
		currentText = (!navigationAsDateFormat ? currentText :
			this.formatDate(currentText, gotoDate, this._getFormatConfig(inst)));
		var controls = (!inst.inline ? '<button type="button" class="ui-timecalendar-close ui-state-default ui-priority-primary ui-corner-all" onclick="DP_jQuery_' + dpuuid +
			'.timecalendar._hideDatepicker();">' + this._get(inst, 'closeText') + '</button>' : '');
		var buttonPanel = (showButtonPanel) ? '<div class="ui-timecalendar-buttonpane ui-widget-timecalendar-content">' + (isRTL ? controls : '') +
			(this._isInRange(inst, gotoDate) ? '<button type="button" class="ui-timecalendar-current ui-state-default ui-priority-secondary ui-corner-all" onclick="DP_jQuery_' + dpuuid +
			'.timecalendar._gotoToday(\'#' + inst.id + '\');"' +
			'>' + currentText + '</button>' : '') + (isRTL ? '' : controls) + '</div>' : '';
		var firstDay = parseInt(this._get(inst, 'firstDay'),10);
		firstDay = (isNaN(firstDay) ? 0 : firstDay);
		var showWeek = this._get(inst, 'showWeek');
		var dayNames = this._get(inst, 'dayNames');
		var dayNamesShort = this._get(inst, 'dayNamesShort');
		var dayNamesMin = this._get(inst, 'dayNamesMin');
		var monthNames = this._get(inst, 'monthNames');
		var monthNamesShort = this._get(inst, 'monthNamesShort');
		var beforeShowDay = this._get(inst, 'beforeShowDay');
		var showOtherMonths = this._get(inst, 'showOtherMonths');
		var selectOtherMonths = this._get(inst, 'selectOtherMonths');
		var calculateWeek = this._get(inst, 'calculateWeek') || this.iso8601Week;
		var defaultDate = this._getDefaultDate(inst);
		var html = '';
		for (var row = 0; row < numMonths[0]; row++) {
			var group = '';
			this.maxRows = 4;
			for (var col = 0; col < numMonths[1]; col++) {
				var selectedDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, inst.selectedDay));
				var cornerClass = ' ui-corner-all';
				var calender = '';
				if (isMultiMonth) {
					calender += '<div class="ui-timecalendar-group';
					if (numMonths[1] > 1)
						switch (col) {
							case 0: calender += ' ui-timecalendar-group-first';
								cornerClass = ' ui-corner-' + (isRTL ? 'right' : 'left'); break;
							case numMonths[1]-1: calender += ' ui-timecalendar-group-last';
								cornerClass = ' ui-corner-' + (isRTL ? 'left' : 'right'); break;
							default: calender += ' ui-timecalendar-group-middle'; cornerClass = ''; break;
						}
					calender += '">';
				}
				calender += '<div class="ui-timecalendar-header ui-widget-timecalendar-header ui-helper-clearfix' + cornerClass + '">' +
					(/all|left/.test(cornerClass) && row == 0 ? (isRTL ? next : prev) : '') +
					(/all|right/.test(cornerClass) && row == 0 ? (isRTL ? prev : next) : '') +
					this._generateMonthYearHeader(inst, drawMonth, drawYear, minDate, maxDate,
					row > 0 || col > 0, monthNames, monthNamesShort) + // draw month headers
					'</div><table class="ui-timecalendar-calendar" cellspacing="0" cellpadding="0"><thead>' +
					'<tr>';
				var thead = (showWeek ? '<th class="ui-timecalendar-week-col">' + this._get(inst, 'weekHeader') + '</th>' : '');
				for (var dow = 0; dow < 7; dow++) { // days of the week
					var day = (dow + firstDay) % 7;
					thead += '<th' + ((dow + firstDay + 6) % 7 >= 5 ? ' class="ui-timecalendar-week-end"' : '') + '>' +
						'<span title="' + dayNames[day] + '">' + dayNamesMin[day] + '</span></th>';
				}
				calender += thead + '</tr></thead><tbody>';
				var daysInMonth = this._getDaysInMonth(drawYear, drawMonth);
				if (drawYear == inst.selectedYear && drawMonth == inst.selectedMonth)
					inst.selectedDay = Math.min(inst.selectedDay, daysInMonth);
				var leadDays = (this._getFirstDayOfMonth(drawYear, drawMonth) - firstDay + 7) % 7;
				var curRows = Math.ceil((leadDays + daysInMonth) / 7); // calculate the number of rows to generate
				var numRows = (isMultiMonth ? this.maxRows > curRows ? this.maxRows : curRows : curRows); //If multiple months, use the higher number of rows (see #7043)
				this.maxRows = numRows;
				var printDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1 - leadDays));
				for (var dRow = 0; dRow < numRows; dRow++) { // create date picker rows
					calender += '<tr>';
					var tbody = (!showWeek ? '' : '<td class="ui-timecalendar-week-col">' +
						this._get(inst, 'calculateWeek')(printDate) + '</td>');
					for (var dow = 0; dow < 7; dow++) { // create date picker days
						var customcss = this._customGetDayCss(printDate);
						var daySettings = (beforeShowDay ? beforeShowDay.apply((inst.input ? inst.input[0] : null), [printDate]) : [true, '']);
						var otherMonth = (printDate.getMonth() != drawMonth);
						var key = this._getDayId(printDate);
						var state = this.daysAvailability[key];
						var fullybooked = this.daysAvailability[key] == "ALL_BOOKED";
						var unselectable = (otherMonth && !selectOtherMonths) 
						|| !daySettings[0] 
						|| (minDate && printDate < minDate) 
						|| (maxDate && printDate > maxDate) 
						|| (!this.isowncalendar && !state) 
						|| (!this.isowncalendar && customcss == 'NOT_AVAILABLE') 
						|| (!this.isowncalendar && this._compareDatesWithoutTime(new Date(), printDate) < 0)
						|| (!this.isowncalendar && fullybooked);
						tbody += '<td class="' +
							((dow + firstDay + 6) % 7 >= 5 ? ' ui-timecalendar-week-end' : '') + // highlight weekends
							(otherMonth ? ' ui-timecalendar-other-month' : '') + // highlight days from other months
							((printDate.getTime() == selectedDate.getTime() && drawMonth == inst.selectedMonth && inst._keyEvent) || // user pressed key
							(defaultDate.getTime() == printDate.getTime() && defaultDate.getTime() == selectedDate.getTime()) ?
							// or defaultDate is current printedDate and defaultDate is selectedDate
							' ' + this._dayOverClass : '') + // highlight selected day
							(unselectable ? ' ' + this._unselectableClass + ' ui-state-disabled': '') +  // highlight unselectable days
							(otherMonth && !showOtherMonths ? '' : ' ' + daySettings[1] + // highlight custom dates
							(printDate.getTime() == currentDate.getTime() ? ' ' + this._currentClass : '') + // highlight selected day
							(printDate.getTime() == today.getTime() ? ' ui-timecalendar-today' : '')) + '"' + // highlight today (if different)
							((!otherMonth || showOtherMonths) && daySettings[2] ? ' title="' + daySettings[2] + '"' : '') + // cell title
							(unselectable ? '' : ' onclick="DP_jQuery_' + dpuuid + '.timecalendar._selectDay(\'#' +
							inst.id + '\',' + printDate.getMonth() + ',' + printDate.getFullYear() + ', this);return false;"') + '>' + // actions
							(otherMonth && !showOtherMonths ? '&#xa0;' : // display for other months
							(unselectable ? '<span class="'+customcss+' ui-state-default">' + printDate.getDate() + '</span>' : '<a id="'+key+'" class="'+customcss+' ui-state-default' +
							(printDate.getTime() == today.getTime() ? ' ui-state-highlight' : '') +
							(printDate.getTime() == currentDate.getTime() ? ' ui-state-active' : '') + // highlight selected day
							(otherMonth ? ' ui-priority-secondary' : '') + // distinguish dates from other months
							'" href="'+this._getDayLink(printDate)+'" target="'+this.calendarTarget+'">' + printDate.getDate() + '</a>')) + '</td>'; // display selectable date
						printDate.setDate(printDate.getDate() + 1);
						printDate = this._daylightSavingAdjust(printDate);
					}
					calender += tbody + '</tr>';
				}
				drawMonth++;
				if (drawMonth > 11) {
					drawMonth = 0;
					drawYear++;
				}
				calender += '</tbody></table>' + (isMultiMonth ? '</div>' + 
							((numMonths[0] > 0 && col == numMonths[1]-1) ? '<div class="ui-timecalendar-row-break"></div>' : '') : '');
				group += calender;
			}
			html += group;
		}
		html += buttonPanel + (jQuery.browser.msie && parseInt(jQuery.browser.version,10) < 7 && !inst.inline ?
			'<iframe src="javascript:false;" class="ui-timecalendar-cover" frameborder="0"></iframe>' : '');
		inst._keyEvent = false;
		return html;
	},

	/* Generate the month and year header. */
	_generateMonthYearHeader: function(inst, drawMonth, drawYear, minDate, maxDate,
			secondary, monthNames, monthNamesShort) {
		var changeMonth = this._get(inst, 'changeMonth');
		var changeYear = this._get(inst, 'changeYear');
		var showMonthAfterYear = this._get(inst, 'showMonthAfterYear');
		var html = '<div class="ui-timecalendar-title">';
		var monthHtml = '';
		// month selection
		if (secondary || !changeMonth)
			monthHtml += '<span class="ui-timecalendar-month">' + monthNames[drawMonth] + '</span>';
		else {
			var inMinYear = (minDate && minDate.getFullYear() == drawYear);
			var inMaxYear = (maxDate && maxDate.getFullYear() == drawYear);
			monthHtml += '<select class="ui-timecalendar-month" ' +
				'onchange="DP_jQuery_' + dpuuid + '.timecalendar._selectMonthYear(\'#' + inst.id + '\', this, \'M\');" ' +
			 	'>';
			for (var month = 0; month < 12; month++) {
				if ((!inMinYear || month >= minDate.getMonth()) &&
						(!inMaxYear || month <= maxDate.getMonth()))
					monthHtml += '<option value="' + month + '"' +
						(month == drawMonth ? ' selected="selected"' : '') +
						'>' + monthNamesShort[month] + '</option>';
			}
			monthHtml += '</select>';
		}
		if (!showMonthAfterYear)
			html += monthHtml + (secondary || !(changeMonth && changeYear) ? '&#xa0;' : '');
		// year selection
		if ( !inst.yearshtml ) {
			inst.yearshtml = '';
			if (secondary || !changeYear)
				html += '<span class="ui-timecalendar-year">' + drawYear + '</span>';
			else {
				// determine range of years to display
				var years = this._get(inst, 'yearRange').split(':');
				var thisYear = new Date().getFullYear();
				var determineYear = function(value) {
					var year = (value.match(/c[+-].*/) ? drawYear + parseInt(value.substring(1), 10) :
						(value.match(/[+-].*/) ? thisYear + parseInt(value, 10) :
						parseInt(value, 10)));
					return (isNaN(year) ? thisYear : year);
				};
				var year = determineYear(years[0]);
				var endYear = Math.max(year, determineYear(years[1] || ''));
				year = (minDate ? Math.max(year, minDate.getFullYear()) : year);
				endYear = (maxDate ? Math.min(endYear, maxDate.getFullYear()) : endYear);
				inst.yearshtml += '<select class="ui-timecalendar-year" ' +
					'onchange="DP_jQuery_' + dpuuid + '.timecalendar._selectMonthYear(\'#' + inst.id + '\', this, \'Y\');" ' +
					'>';
				for (; year <= endYear; year++) {
					inst.yearshtml += '<option value="' + year + '"' +
						(year == drawYear ? ' selected="selected"' : '') +
						'>' + year + '</option>';
				}
				inst.yearshtml += '</select>';
				
				html += inst.yearshtml;
				inst.yearshtml = null;
			}
		}
		html += this._get(inst, 'yearSuffix');
		if (showMonthAfterYear)
			html += (secondary || !(changeMonth && changeYear) ? '&#xa0;' : '') + monthHtml;
		html += '</div>'; // Close datepicker_header
		return html;
	},

	/* Adjust one of the date sub-fields. */
	_adjustInstDate: function(inst, offset, period) {
		var year = inst.drawYear + (period == 'Y' ? offset : 0);
		var month = inst.drawMonth + (period == 'M' ? offset : 0);
		var day = Math.min(inst.selectedDay, this._getDaysInMonth(year, month)) +
			(period == 'D' ? offset : 0);
		var date = this._restrictMinMax(inst,
			this._daylightSavingAdjust(new Date(year, month, day)));
		inst.selectedDay = date.getDate();
		inst.drawMonth = inst.selectedMonth = date.getMonth();
		inst.drawYear = inst.selectedYear = date.getFullYear();
		if (period == 'M' || period == 'Y')
			this._notifyChange(inst);
	},

	/* Ensure a date is within any min/max bounds. */
	_restrictMinMax: function(inst, date) {
		var minDate = this._getMinMaxDate(inst, 'min');
		var maxDate = this._getMinMaxDate(inst, 'max');
		var newDate = (minDate && date < minDate ? minDate : date);
		newDate = (maxDate && newDate > maxDate ? maxDate : newDate);
		return newDate;
	},

	/* Notify change of month/year. */
	_notifyChange: function(inst) {
		var onChange = this._get(inst, 'onChangeMonthYear');
		if (onChange)
			onChange.apply((inst.input ? inst.input[0] : null),
				[inst.selectedYear, inst.selectedMonth + 1, inst]);
	},

	/* Determine the number of months to show. */
	_getNumberOfMonths: function(inst) {
		var numMonths = this._get(inst, 'numberOfMonths');
		return (numMonths == null ? [1, 1] : (typeof numMonths == 'number' ? [1, numMonths] : numMonths));
	},

	/* Determine the current maximum date - ensure no time components are set. */
	_getMinMaxDate: function(inst, minMax) {
		return this._determineDate(inst, this._get(inst, minMax + 'Date'), null);
	},

	/* Find the number of days in a given month. */
	_getDaysInMonth: function(year, month) {
		return 32 - this._daylightSavingAdjust(new Date(year, month, 32)).getDate();
	},

	/* Find the day of the week of the first of a month. */
	_getFirstDayOfMonth: function(year, month) {
		return new Date(year, month, 1).getDay();
	},

	/* Determines if we should allow a "next/prev" month display change. */
	_canAdjustMonth: function(inst, offset, curYear, curMonth) {
		var numMonths = this._getNumberOfMonths(inst);
		var date = this._daylightSavingAdjust(new Date(curYear,
			curMonth + (offset < 0 ? offset : numMonths[0] * numMonths[1]), 1));
		if (offset < 0)
			date.setDate(this._getDaysInMonth(date.getFullYear(), date.getMonth()));
		return this._isInRange(inst, date);
	},

	/* Is the given date in the accepted range? */
	_isInRange: function(inst, date) {
		var minDate = this._getMinMaxDate(inst, 'min');
		var maxDate = this._getMinMaxDate(inst, 'max');
		return ((!minDate || date.getTime() >= minDate.getTime()) &&
			(!maxDate || date.getTime() <= maxDate.getTime()));
	},

	/* Provide the configuration settings for formatting/parsing. */
	_getFormatConfig: function(inst) {
		var shortYearCutoff = this._get(inst, 'shortYearCutoff');
		shortYearCutoff = (typeof shortYearCutoff != 'string' ? shortYearCutoff :
			new Date().getFullYear() % 100 + parseInt(shortYearCutoff, 10));
		return {shortYearCutoff: shortYearCutoff,
			dayNamesShort: this._get(inst, 'dayNamesShort'), dayNames: this._get(inst, 'dayNames'),
			monthNamesShort: this._get(inst, 'monthNamesShort'), monthNames: this._get(inst, 'monthNames')};
	},

	/* Format the given date for display. */
	_formatDate: function(inst, day, month, year) {
		if (!day) {
			inst.currentDay = inst.selectedDay;
			inst.currentMonth = inst.selectedMonth;
			inst.currentYear = inst.selectedYear;
		}
		var date = (day ? (typeof day == 'object' ? day :
			this._daylightSavingAdjust(new Date(year, month, day))) :
			this._daylightSavingAdjust(new Date(inst.currentYear, inst.currentMonth, inst.currentDay)));
		return this.formatDate(this._get(inst, 'dateFormat'), date, this._getFormatConfig(inst));
	},
	
	_compareDatesWithoutTime: function(dateB, dateA){
		
		if(dateA.getFullYear() == dateB.getFullYear() && dateA.getMonth() == dateB.getMonth() && dateA.getDate() == dateB.getDate())
			return 0;
		if(dateA.getFullYear() > dateB.getFullYear())
			return 1;
		else if(dateA.getFullYear() < dateB.getFullYear())
			return -1;
		else if(dateA.getMonth() > dateB.getMonth())
			return 1;
		else if(dateA.getMonth() < dateB.getMonth())
			return -1;
		else if(dateA.getDate() > dateB.getDate())
			return 1;
		else if(dateA.getDate() < dateB.getDate())
			return -1;
		
	},
	_customGetDayCss: function(day){
		var comp = this._compareDatesWithoutTime(new Date(),day);
		if(comp == 0)
			return "TODAY";
		
		if(!this.daysAvailability)
			return "NOT_AVAILABLE";
		
		var key = this._getDayId(day);
		
		var state = this.daysAvailability[key];
		if(!state)
			return "NOT_AVAILABLE";
		
		return state;
	},
	
	_getDayId: function(date){
		
		var month = date.getMonth()<9?("0"+(date.getMonth()+1)):(date.getMonth()+1);
		var day = date.getDate()<10?("0"+date.getDate()):date.getDate();
		var key = "D"+day+"_"+month+"_"+date.getFullYear();
		//console.log(key);
		return key;
	},

	_customSelectDayAction: function(day){
		day = new Date(day);
		
		jQuery('#calendarloading').fadeIn('fast');
		this.selectedDay = day;

		var url = "datafeed.php?method=list&userid="+this.userId+"&date="+day.getTime()+"&timezoneOffset="+jQuery.timecalendar.timeOffset;

		jQuery.get(url, function(data){
			var html = [];
			html.push('<form method="POST" id="calendarform"><div style="width: 100px; margin: 0 auto;"><h2>'+(day.getMonth()+1)+'/'+day.getDate()+'/'+day.getFullYear()+'</h2></div>');
			var tablerows = [];
	    	var calendarId = "na";
	    	var calendarIdaux;
	    	var isBefore = (jQuery.timecalendar._compareDatesWithoutTime(new Date(), day) < 0)
	    	//console.debug(isBefore);
	    	this.dayArray = [];
	    	if(jQuery.timecalendar.isowncalendar){
	    		if(data.events[0])
	    			jQuery.timecalendar.dayArray = data.events[0][6];
	    		var size = jQuery.timecalendar.fullDayTime.length;
	    		var hour;
	    		var infolink = "";
	    		jQuery.timecalendar.scheduled = [];
	    		
	    		for (i=0; i<size-1; i++){
	    			if(data.events && data.events.length > 0){
	    				hour = data.events[0][6][i];
	    			}else
	    				hour = eval ("({isEmpty:true})");
	    			var start = jQuery.timecalendar.fullDayTime[i];
	    			var end = jQuery.timecalendar.fullDayTime[i+1];
					
					var gamesList = [];
					if(hour.gameId){
						if(hour.gameId.length > 0){
							gamesList.push('<div class="calendarTooltip">');
							for (index=0; index < hour.gameId.length; index++){
								var id = hour.gameId[index];
								for (a=0; a<jQuery.timecalendar.allGamesArray.length; a++){
									if(id == jQuery.timecalendar.allGamesArray[a].game_id){
										gamesList.push('<div class="calendarTooltipIn">');
										gamesList.push(jQuery.timecalendar.allGamesArray[a].game_name);
										gamesList.push('</div>');
									}
								}
							}
							gamesList.push('</div>');
						}
					}
	    			if(isBefore){
	    				if(hour.isEmpty){
	    					if(jQuery.timecalendar.youarecoach){
	    						var tr = '<tr align="center" class="calendar_time_row "><td><div class="infolinkwrapper"><div class="timeinfonolink"/></div></td><td><span>'+start+'</span></td><td><span>'+end+'</span></td><td><input type="checkbox" disabled="disabled" name="'+start+'#'+end+'" value="'+i+'" class=""></td></tr>';
	    					}else{
		    					var tr = '<tr align="center" class="calendar_time_disabled"><td><div class="infolinkwrapper"><div class="timeinfonolink"/></div></td><td><span>'+start+'</span></td><td><span>'+end+'</span></td><td><input type="checkbox" disabled="disabled" name="'+start+'#'+end+'" value="'+i+'" class=""></td></tr>';
	    					}
	    						
	    				}else if(hour.availableSpot==1){
							var tr = '<tr align="center" class="calendar_time_row calendar_time_used"><td><div class="infolinkwrapper"><div class="timeinfonolink"/></div></td><td><span>'+start+'</span></td><td><span>'+end+'</span></td><td class=""><input type="checkbox" disabled="disabled" name="'+start+'#'+end+'" value="'+i+'" class=""></td></tr>';
						}else{
		    				var statusCss = jQuery.timecalendar._getSstatusCss(hour.status);
		    				infolink = '<div class="infolinkwrapper"><a onclick="DP_jQuery_' + dpuuid + '.timecalendar._getScheduleInfo('+i+','+hour.status+')" href="#" class="'+statusCss+'">?</a></div>';
							var tr = '<tr align="center" class="calendar_time_row calendar_time_used"><td>'+infolink+'</td><td><span>'+start+'</span></td><td><span>'+end+'</span></td><td class=""><input type="checkbox" disabled="disabled" name="'+start+'#'+end+'" value="'+i+'" class=""></td></tr>';
		    			}
	    			}else {
	    				if(hour.isEmpty){
	    					if(jQuery.timecalendar.youarecoach){
			    				var tr = '<tr align="center" class="calendar_time_row"><td><div class="infolinkwrapper"><div class="timeinfonolink"/></div></td><td><span>'+start+'</span></td><td><span>'+end+'</span></td><td><input type="checkbox" name="'+start+'#'+end+'" value="'+i+'" class=""></td></tr>';
		    				}else{
		    					var tr = '<tr align="center" class="calendar_time_disabled"><td><div class="infolinkwrapper"><div class="timeinfonolink"/></div></td><td><span>'+start+'</span></td><td><span>'+end+'</span></td><td><input type="checkbox" disabled="disabled" name="'+start+'#'+end+'" value="'+i+'" class=""></td></tr>';
		    				}
	    				}else if(hour.availableSpot==1){
							var tr = '<tr align="center" class="calendar_time_row calendar_time_used"><td>'+gamesList.join("")+'<div class="infolinkwrapper"><div class="timeinfonolink"/></div></td><td><span>'+start+'</span></td><td><span>'+end+'</span></td><td class=""><input type="checkbox" name="'+start+'#'+end+'" value="'+i+'" class=""></td></tr>';
							jQuery.timecalendar.scheduled.push(i);
		    			}else{
		    				var statusCss = jQuery.timecalendar._getSstatusCss(hour.status);
		    				infolink = '<div class="infolinkwrapper"><a onclick="DP_jQuery_' + dpuuid + '.timecalendar._getScheduleInfo('+i+','+hour.status+')" href="#" class="'+statusCss+'">?</a></div>';
							var tr = '<tr align="center" class="calendar_time_row calendar_time_used"><td>'+infolink+gamesList.join("")+'</td><td><span>'+start+'</span></td><td><span>'+end+'</span></td><td class=""><input type="checkbox" name="'+start+'#'+end+'" value="'+i+'" class=""></td></tr>';
		    			}
	    			}
	    			tablerows.push(tr);
	    		}
	    	}else{
	    		var size = jQuery.timecalendar.fullDayTime.length;
	    		var hour;
	    		if(data.events[0])
	    			jQuery.timecalendar.dayArray = data.events[0][6];
                        
//                        var offSet = parseFloat(jQuery.timecalendar.ownerinfo.time_zone)-parseFloat(jQuery.timecalendar.ownerinfo.loggedUser.time_zone);
//                        offSet = offSet*2;
                        
                        var today = new Date();
	    		for (i=jQuery.timecalendar.timeOffSet; i<(size+jQuery.timecalendar.timeOffSet)-1; i++){
					
	    			if(data.events && data.events.length > 0 && i>-1 && (data.events[0][6][i])) 
	    				hour = data.events[0][6][i];
	    			else
	    				hour = eval ("({isEmpty:true})");
	    			//console.log(i);
				var start = jQuery.timecalendar.fullDayTime[(i-jQuery.timecalendar.timeOffSet)];
	    			var end = jQuery.timecalendar.fullDayTime[(i-jQuery.timecalendar.timeOffSet)+1];
					
                                var gamesList = [];
                                if(hour.gameId){
                                        if(hour.gameId.length>0){
                                                gamesList.push('<div class="calendarTooltip">');
                                                for (index=0; index<hour.gameId.length; index++){
                                                        var id = hour.gameId[index];
                                                        for (a=0; a<jQuery.timecalendar.allGamesArray.length; a++){
                                                                if(id == jQuery.timecalendar.allGamesArray[a].game_id){
                                                                        gamesList.push('<div class="calendarTooltipIn">');
                                                                        gamesList.push(jQuery.timecalendar.allGamesArray[a].game_name);
                                                                        gamesList.push('</div>');
                                                                }
                                                        }
                                                }
                                                gamesList.push('</div>');
                                        }
                                }
                               
	    			if(hour.isEmpty || jQuery.timecalendar._isPast(today,day,start))
	    				var tr = '<tr align="center" class="calendar_time_disabled"><td><div class="infolinkwrapper"><div class="timeinfonolink"/></div></td><td><span>'+start+'</span></td><td><span>'+end+'</span></td><td><input type="checkbox" disabled="disabled" name="'+start+'#'+end+'" value="'+i+'" class=""></td></tr>';
	    			else if(hour.availableSpot==1){
                                        var tr = '<tr align="center" class="calendar_time_row"><td>'+gamesList.join("")+'<div class="infolinkwrapper"><div class="timeinfonolink"/></div></td><td><span>'+start+'</span></td><td><span>'+end+'</span></td><td class=""><input type="checkbox" name="'+start+'#'+end+'" value="'+i+'" class=""></td></tr>';
	    			}else{
                                        var tr = '<tr align="center" class="calendar_time_row calendar_time_taken">'+gamesList.join("")+'<td><div class="infolinkwrapper"><div class="timeinfonolink"/></div></td><td><span>'+start+'</span></td><td><span>'+end+'</span></td><td class=""><input type="checkbox" disabled="disabled" name="'+start+'#'+end+'" value="'+i+'" class=""></td></tr>';
	    			}
	    			tablerows.push(tr);
	    		}
	    	}
			var tabletop = '<div style="float:left; margin: 3px;"><table cellpadding="2" cellspacing="0" class="timetable"><thead><tr><th class="timeheader"></th><th class="timeheader time">Start time</th><th class="timeheader time">End time</th><th class="timeheader"></th></tr></thead><tbody id="timetableid">';
            var tablebottom = '</tbody></table></div>';
            html.push(tabletop);
            for (var i = 0; i<(tablerows.length); i++){
				if((i == 16 || i == 32)){
					html.push(tablebottom);
					html.push(tabletop.replace("timetableid",("timetableid"+i)));
				}
			}
			html.push(tablebottom);
			html.push('<div style="clear: both;">');
			html.push('<a href="#" class="calendarbutton" onclick="DP_jQuery_' + dpuuid + '.timecalendar._selectall(1)"><span>select all</span></a>');
			html.push('<a href="#" class="calendarbutton" onclick="DP_jQuery_' + dpuuid + '.timecalendar._selectall(2)"><span>deselect all</span></a>');
			html.push('<a href="#" class="calendarbutton" onclick="DP_jQuery_' + dpuuid + '.timecalendar._selectall(3)"><span>invert selection</span></a>');
			html.push('</div></form></div>');
			html.push('<div style="clear:both;"/>');
			html.push('<div class="calendarinstruct">*Press Shift Key to select multiple time slots - select the start time & end time</div>');
			html.push('<div class="button-flex ">');
			if(jQuery.timecalendar.isowncalendar){
				html.push('<a href="#" class="calendarbutton button" onclick="DP_jQuery_' + dpuuid + '.timecalendar._cancel()"><span>Cancel</span></a>');
				if(jQuery.timecalendar.youarecoach){
					html.push('<a href="#" class="calendarbutton button" onclick="DP_jQuery_' + dpuuid + '.timecalendar._setupschedule()"><span>Schedule</span></a>');
				}
				
			}else{
				if(data.events[0]){
					html.push('<a href="#" class="calendarbutton button" onclick="DP_jQuery_' + dpuuid + '.timecalendar._schedule()"><span>Schedule</span></a>');
				}else{
                                    html.push('<a href="#" class="calendarbutton button" onclick="return false;" style="position: relative;" title="The Instant Lesson feature is available when the coach is online"><div class="disabledLink">&nbsp;</div><span>Schedule</span></a>');
                                }
                                if(jQuery.timecalendar.ownerinfo.onlinestatus == 'online'){
					html.push('<a href="#" class="calendarbutton button" onclick="DP_jQuery_' + dpuuid + '.timecalendar._instantLesson()" style="display:none;position: relative;" title="The Instant Lesson feature is available when the coach is online"><span>Instant lesson</span></a>');
				}else{
					html.push('<a href="#" class="calendarbutton button" onclick="return false;" style="display:none;position: relative;" title="The Instant Lesson feature is available when the coach is online"><div class="disabledLink">&nbsp;</div><span>Instant lesson</span></a>');
				}
			}
			html.push('</div>');
			jQuery("#timeslotselect").html(html.join(""));
			var idaux = "";
			for (var i = 0; i<(tablerows.length); i++){
				
				if(i == 16){
					var idaux = "16";
				}
				if(i == 32){
					var idaux = "32";
				}
				var selector = '#timetableid'+idaux;
				jQuery(selector).append(bindMouseEvents(tablerows[i]));
			}
			jQuery('#calendarloading').fadeOut('fast');
		}, "json");
	},
	
	_getSstatusCss: function(status){
		if(status == 3)
			return "timewarninglink";
		return "timeinfolink";
	},
        
	_isPast:function(today,day,end){
            
            if(jQuery.timecalendar._compareDatesWithoutTime(today, day) > 0)
                return false;
            
            
            var endArray = end.split(":");
            var hours = today.getHours();
            var minutes = today.getMinutes();
            
            
            var h = parseInt(endArray[0]);
            var m = parseInt(endArray[1]);
            
            
            
            if(hours > h){
                return true;
            }
            else if(hours == h &&  minutes > m){
                return true;
            }
            
            return false;
        },
	_instantLesson:function(overlay, loading,finalstep,wrapper){
                
                if(!loading)
                    loading = '#calendarloading';
                jQuery(loading).fadeIn('fast');
                
                if(!overlay)
                    overlay = '#calendaroverlay';
                
                jQuery(overlay).fadeIn('fast');
                
                if(!finalstep)
                    finalstep = '#finalstep';
		jQuery(finalstep).empty();
		
		var html = [];
		var rates = [];
		var size = 0;
		var gamesOptions = [];
		for (var i = 0; i<this.ownerinfo.games.length; i++){
			for (var j = 0; j<this.allGamesArray.length; j++){
				if(this.allGamesArray[j].game_id == this.ownerinfo.games[i].game_id){
					gamesOptions.push('<option value="'+this.allGamesArray[j].game_id+'">'+this.ownerinfo.games[i].game_name+'</option>');
					size++;
					break;
				}
			}
		}
		var rate_a = this.ownerinfo.games[0].rate_A;
		var rate_b = (this.ownerinfo.games[0].rate_B == 0)?rate_a:this.ownerinfo.games[0].rate_B;
		var rate_c = (this.ownerinfo.games[0].rate_C == 0)?rate_b:this.ownerinfo.games[0].rate_C;
		
		html.push('<div class="calendarTitle">Instant Lesson</div><div style="margin-left: 10px; height: 100px"><form id="instantForm"><div style="margin-bottom: 10px;"><span style="margin-right: 5px;float: left">Game:</span>');
		html.push('<select name ="_selectedGame" style="width: 200px; padding: 3px;" id="gameselection" onchange="DP_jQuery_' + dpuuid + '.timecalendar._changeInstantRate(this)" >'+gamesOptions.join("")+'</select></div>');
		html.push('<input type="hidden" value="'+rate_a+'" id="_ratefield" name="_ratefield" /><div id="_instantRateWrapper"><input type="radio" onclick="document.getElementById(\'_ratefield\').value='+rate_a+'" name="instantRadio" value="'+rate_a+'#1"><span> 1hr/'+rate_a+'pts</span><br>');
		html.push('<input type="radio" onclick="document.getElementById(\'_ratefield\').value='+rate_b+'" name="instantRadio" value="'+(parseInt(rate_b))+'#2"><span> 2hrs/'+rate_b+'pts</span><br>');
		html.push('<input type="radio" onclick="document.getElementById(\'_ratefield\').value='+rate_c+'" name="instantRadio" value="'+(parseInt(rate_c))+'#3"><span> 3hrs/'+rate_c+'pts</span><br>');
		html.push('</div></div></form><div class="formerrormessage" id="_formerrormessage" ></div><div class="button-flex ">');
		html.push('<a href="javascript:;" class="calendarbutton button" onclick="DP_jQuery_' + dpuuid + '.timecalendar._doInstantLesson(\''+overlay+'\',\''+loading+'\',\''+finalstep+'\',\''+wrapper+'\')"><span>Finish</span></a><a href="javascript:;" class="calendarbutton button" onclick="jQuery(\''+finalstep+'\').hide(); jQuery(\''+finalstep+'\').empty();jQuery(\''+overlay+'\').fadeOut(\'fast\');"><span>Close</span></a></div>');
		jQuery(finalstep).html(html.join(""));
		var width = jQuery(finalstep).width();
		jQuery(finalstep).css('margin-left', -(width/2));
		
                jQuery(loading).fadeOut('fast');
		jQuery(finalstep).show('fast',function(){jQuery(finalstep).draggable();});
	},
	
	_changeInstantRate: function(item){ 
		jQuery("#_instantRateWrapper").empty();
		var html = [];
		var rates = [];
		for (var j = 0; j<this.ownerinfo.games.length; j++){
			if(item.value == this.ownerinfo.games[j].game_id){
				var rate_a = this.ownerinfo.games[j].rate_A;
				var rate_b = (this.ownerinfo.games[j].rate_B == 0)?rate_a:this.ownerinfo.games[j].rate_B;
				var rate_c = (this.ownerinfo.games[j].rate_C == 0)?rate_b:this.ownerinfo.games[j].rate_C;
				html.push('<input type="radio" onclick="document.getElementById(\'_ratefield\').value='+rate_a+'" name="instantRadio" value="'+rate_a+'#1"><span> 1hr/'+rate_a+'pts</span><br>');
				html.push('<input type="radio" onclick="document.getElementById(\'_ratefield\').value='+rate_b+'" name="instantRadio" value="'+(parseInt(rate_b))+'#2"><span> 2hrs/'+rate_b+'pts</span><br>');
				html.push('<input type="radio" onclick="document.getElementById(\'_ratefield\').value='+rate_c+'" name="instantRadio" value="'+(parseInt(rate_c))+'#3"><span> 3hrs/'+rate_c+'pts</span><br>');
				jQuery("#_instantRateWrapper").append(html.join(""));
				break;
			}
		}
	},
	
	_doInstantLesson:function(overlay,loading,finalstep,wrapper){
           
                if(!jQuery("#instantForm").serializeArray()[2]){
                    jQuery('#_formerrormessage').text("you must choose one");
                    jQuery('#_formerrormessage').show();
                    return;
                }
                   
		jQuery(finalstep).hide();
		var value = jQuery("#instantForm").serializeArray()[2].value; 
		if(!value || value=="")
			return;
                var holder = value.split("#");
		var gameId = jQuery("#instantForm").serializeArray()[0].value; 
		if(!gameId || gameId=="")
			return;
			
		if(parseInt(holder[0]) > parseInt(this.ownerinfo.loggedUser.total_points)){
			jQuery(finalstep).hide();
                        
			jQuery.timecalendar._showWarning("You don't have enough credits click <a href=\"cashier.php\">here</a> to buy more credits",'DP_jQuery_' + dpuuid + '.timecalendar._customSelectDayAction(\''+jQuery.timecalendar.selectedDay+'\')',overlay,wrapper);
			return;
		}

		jQuery.post("datafeed.php",{"method":"doInstantLessonWarning","offSet":this.timeOffSet,"value":holder[0],"rate":jQuery('#_ratefield').val(),"gameId":gameId, "userid": this.ownerinfo.user_id,"hours":holder[1]}, function(data){
                        if(data.IsSuccess){
				jQuery.timecalendar._showWarning("You have successfully send an instant lesson request.<br/>Please wait for the coach response","", overlay,wrapper);
                                jQuery.timecalendar._addAsFriend(jQuery.timecalendar.ownerinfo.user_id);
			}
			else{
				jQuery.timecalendar._showWarning("Something went wrong, please try again later","", overlay,wrapper);
			}
		}, 
		"json");
	},
        _addAsFriend:function (user_id){
                jQuery.post("datafeed.php",{"method": "addAsFriend", "userid": user_id}, function(data){
                       console.log(data);
                },"json");
        },
	_getScheduleInfo: function(value,action){ 
		var INFO = 2;
		var DELETE = 3;
		if(!action)
			action = 2;
		if(jQuery.timecalendar.isowncalendar){
			if(action == DELETE){
				if(jQuery.timecalendar.ownerinfo.user_id == jQuery.timecalendar.dayArray[value].requestoruserId){//is trainee
					jQuery.timecalendar._showWarning("Your booking have been canceled");
					jQuery.timecalendar._removeCanceled();
				}else if(jQuery.timecalendar.ownerinfo.user_id == jQuery.timecalendar.dayArray[value].userId){//is Coach
					jQuery.timecalendar._showWarning("Your booking have been canceled, the canceled slots will now be availabel for schedule");
					jQuery.timecalendar._removeCanceled();
				}
			}
			if(action == INFO){
				var html = [];
				html.push('<div class="calendarTitle">Schedule information</div>');		
				jQuery("#finalstep").empty();
				
				if(jQuery.timecalendar.ownerinfo.user_id == jQuery.timecalendar.dayArray[value].requestoruserId){//is trainee
					jQuery('#calendarloading').fadeIn('fast');
					jQuery('#calendaroverlay').fadeIn('fast');
					var urlcalendarId = "datafeed.php?method=getcalendarbyid&calendarId="+jQuery.timecalendar.dayArray[value].calendarRef;
					jQuery.get(urlcalendarId, function(data){
						var urlUser = "datafeed.php?method=getUserInfo&userid="+data.user_id;
						jQuery.get(urlUser, function(data){
							var coachname = data.username;
							var gamename = "";
							var rate = "";
							for (var i = 0; i<jQuery.timecalendar.dayArray[value].gameId.length; i++){
								for (var j = 0; j < data.games.length; j++){
									if(jQuery.timecalendar.dayArray[value].gameId[i] == data.games[j].game_id){
										gamename = data.games[j].game_name;
										rate = data.games[j].rate;
									}
								}
							}
							var url = "datafeed.php?method=getcalendarrefsequence&index="+value+"&calendarId="+jQuery.timecalendar.dayArray[value].calendarId;
							jQuery.get(url, function(data){
								var x;
								var seq = jQuery.parseJSON(data);
								var values = seq.values;
								var slots = values.split('#');
								slots.sort();
								var table = [];
								table.push('<div class"wrap">');
								for(var i = 0; i<(slots.length); i++){
									if(i == 0 || i == 16 || i == 32)
										table.push('<div style="float: left; margin-right: 15px;">');
									var a = jQuery.timecalendar.fullDayTime[parseInt(slots[i])];
									var b = jQuery.timecalendar.fullDayTime[parseInt(slots[i+1])];
									if(!b){
										var aux = parseInt(slots[i]);
										b =jQuery.timecalendar.fullDayTime[aux+1];
									}
									table.push('<span>'+a+' - '+b+'</span><br/>');
									if(i == 15 || i == 31)
										table.push('</div>');
								}
								html.push('<table cellspacing="7px" cellpadding="0">');		
								html.push(jQuery.timecalendar._getTwoCellRow('Coach:', coachname));
								html.push(jQuery.timecalendar._getTwoCellRow('Game:', gamename));
								html.push(jQuery.timecalendar._getTwoCellRow('Rate:', rate));
								html.push(jQuery.timecalendar._getTwoCellRow('Sequence:', table.join("")));
								html.push('</table>');
								html.push('<div class="button-flex ">');
								html.push('<a href="#" class="calendarbutton button" onclick="jQuery(\'#finalstep\').hide(); jQuery(\'#finalstep\').empty();jQuery(\'#calendaroverlay\').fadeOut(\'fast\');"><span>Close</span></a>');
								html.push('</div>');
								jQuery('#calendarloading').fadeOut('fast');
								jQuery("#finalstep").html(html.join(""));
								var width = jQuery("#finalstep").width();
								jQuery("#finalstep").css('margin-left', -(width/2));
								jQuery("#finalstep").show('fast',function(){jQuery("#finalstep").draggable();});
							}, "json");
						}, "json");
					
					}, "json");
					
				}else if(jQuery.timecalendar.ownerinfo.user_id == jQuery.timecalendar.dayArray[value].userId){//is Coach
					jQuery('#calendarloading').fadeIn('fast');
					jQuery('#calendaroverlay').fadeIn('fast');
					var urlUser = "datafeed.php?method=getUserInfo&userid="+jQuery.timecalendar.dayArray[value].requestoruserId;
					jQuery.get(urlUser, function(data){
						var gamename = "";
						var rate = "";
						for (var i = 0; i<jQuery.timecalendar.dayArray[value].gameId.length; i++){
							for (var j = 0; j < jQuery.timecalendar.ownerinfo.games.length; j++){
								if(jQuery.timecalendar.dayArray[value].gameId[i] ==  jQuery.timecalendar.ownerinfo.games[j].game_id){
									gamename =  jQuery.timecalendar.ownerinfo.games[j].game_name;
									rate =  jQuery.timecalendar.ownerinfo.games[j].rate;
								}
							}
						}
						var traineename = data.username;
						var url = "datafeed.php?method=getSequence&index="+value+"&calendarId="+jQuery.timecalendar.dayArray[value].calendarId;
						jQuery.get(url, function(data){
							var x;
							var seq = jQuery.parseJSON(data);
							var values = seq.values;
							var slots = values.split('#');
							slots.sort();
							
							var table = [];
							table.push('<div class"wrap">');
							for(var i = 0; i<(slots.length); i++){
								if(i == 0 || i == 16 || i == 32)
									table.push('<div style="float: left; margin-right: 15px;">');
								
								var a = jQuery.timecalendar.fullDayTime[parseInt(slots[i])];
								var b = jQuery.timecalendar.fullDayTime[parseInt(slots[i+1])];
								if(!b){
									var aux = parseInt(slots[i]);
									b =jQuery.timecalendar.fullDayTime[aux+1];
								}
								table.push('<span>'+a+' - '+b+'</span><br/>');
								if(i == 15 || i == 31)
									table.push('</div>');
							}
							html.push('<table cellspacing="7px" cellpadding="0">');		
							html.push(jQuery.timecalendar._getTwoCellRow('Trainee:', traineename));
							html.push(jQuery.timecalendar._getTwoCellRow('Game:', gamename));
							html.push(jQuery.timecalendar._getTwoCellRow('Sequence:', table.join("")));
							html.push('</table>');
							html.push('<div class="button-flex ">');
							html.push('<a href="#" class="calendarbutton button" onclick="jQuery(\'#finalstep\').hide(); jQuery(\'#finalstep\').empty();jQuery(\'#calendaroverlay\').fadeOut(\'fast\');"><span>Close</span></a>');
							html.push('</div>');
							jQuery('#calendarloading').fadeOut('fast');
							jQuery("#finalstep").html(html.join(""));
							var width = jQuery("#finalstep").width();
							jQuery("#finalstep").css('margin-left', -(width/2));
							//jQuery("#finalstep").show();
							jQuery("#finalstep").show('fast',function(){jQuery( "#finalstep" ).draggable();});
						}, "json");
						
					}, "json");
				}
			}	
		}
	},
	_removeCanceled: function(){
		
		var array = [];
		for(var i = 0; i<this.dayArray.length; i++){
			if(this.dayArray[i] && this.dayArray[i].status == 3)
				array.push(i);
		}
		jQuery.post("datafeed.php",{"method": "removeSlots", "userid": this.userId, "schedules": array.join(),"date": this.selectedDay.getTime()}, function(data){
			jQuery.timecalendar._customSelectDayAction(jQuery.timecalendar.selectedDay);
			
			//TODO: add/remove points
		}, 
		"json");
	},
	
	_selectall: function (value){
		lastChecked = undefined;
		var SELECT_ALL = 1;
		var DESELECT_ALL = 2;
		var INVERT_SELECTION = 3;
		
		var form = jQuery("#calendarform");
		var formelements = form[0];
		var selectedSpots = 0;
		this.jscheduled = [];
		if(!value)
			value = 1;
		
		if(value == SELECT_ALL){
			for (var i = 0; i<formelements.length; i++){
				if(formelements[i].disabled)
					continue;
				formelements[i].checked = true;
			}
		}else if(value == DESELECT_ALL){
			for (var i = 0; i<formelements.length; i++){
				if(formelements[i].disabled)
					continue;
				formelements[i].checked = false;
			}
		}else if(value == INVERT_SELECTION){
			for (var i = 0; i<formelements.length; i++){
				if(formelements[i].disabled)
					continue;
				
				if(formelements[i].checked == false)
					formelements[i].checked = true;
				else
					formelements[i].checked = false;
			}
		}
	},
	
    _destroy: function(value){
		//jQuery(value).hide('fast',function(){jQuery(value).empty();}); 
		
    	jQuery(value).remove();
    },
     
    _showWarning: function(message,callback, overlay, wrapper){
        if(!overlay)
            overlay = '#calendaroverlay';
        if(!wrapper)
            wrapper = '#calendarwrapper';
        
        var position = 'absolute';
        if(wrapper != '#calendarwrapper')
            position = 'fixed';
    	
        var html = [];
        html.push('<div class="time_calendar_warnig" id="time_calendar_warnig" style="position: '+position+';">');

        html.push('<table style="width: 100%;"><tr align="center" valign="middle"><td style="height: 95px;" ><span class="time_calendar_warnig_text">');
        html.push(message);
        html.push('</span></td></tr><tr align="center" valign="middle"><td style="height: 35px;">');
        if(!callback)
                callback = "";
        html.push('<div class="button-flex" style="margin: 0 auto;width: 70px;"><a href="#" class="time_calendar_warnig_okbutton button" onclick="DP_jQuery_' + dpuuid + '.timecalendar._destroy(\'#time_calendar_warnig\'); jQuery(\''+overlay+'\').fadeOut(\'fast\');'+callback+'"><span>close</span></a></div></td></tr>');

        //jQuery("#finalstep").show('fast',function(){jQuery("#finalstep");});
        var warnig = jQuery(html.join("")).draggable();
        jQuery(wrapper).append(warnig);
        jQuery(overlay).fadeIn('fast');
        return html.join("");
    },
    
    _showYesNoValidation: function(message,yesCallback,noCallback){
    	var html = [];
		html.push('<div class="time_calendar_warnig" id="time_calendar_validation" style="position: absolute;">');
		html.push('<table style="width: 100%;"><tr align="center" valign="middle"><td style="height: 95px;"  ><span class="time_calendar_warnig_text">');
		html.push(message);
		html.push('</span></td></tr><tr align="center" valign="middle"><td style="height: 35px;">');
		if(!yesCallback)
			yesCallback = "";
		if(!noCallback)
			noCallback = "";
		html.push('<div class="button-flex" style="margin: 0 auto;width: 130px;">');
		html.push('<a style="float: left;" href="#" class="time_calendar_warnig_okbutton button" onclick="DP_jQuery_' + dpuuid + '.timecalendar._destroy(\'#time_calendar_validation\'); jQuery(\'#calendaroverlay\').fadeOut(\'fast\');'+yesCallback+'"><span>Yes</span></a>');
		html.push('<a style="float: left;" href="#" class="time_calendar_warnig_okbutton button" onclick="DP_jQuery_' + dpuuid + '.timecalendar._destroy(\'#time_calendar_validation\'); jQuery(\'#calendaroverlay\').fadeOut(\'fast\');'+noCallback+'"><span>No</span></a>');
		html.push('</td></tr></div>');
		
		var warnig = jQuery(html.join("")).draggable();
		jQuery("#calendarwrapper").append(warnig);
		jQuery("#calendaroverlay").fadeIn('fast');
		return html.join("");
    },
    
    _checkContinuity: function(formelements, i){
    	if(i == 0){
    		if(!formelements[i+1].checked)
    			return false;
    		else 
    			return true;
    	}else if(i == (formelements.length-1)){
    		if(!formelements[i-1].checked)
    			return false;
    		else 
    			return true;
    	}else if(!formelements[i+1].checked){
			if(!formelements[i-1].checked){
				return false;
			}
		}	
		return true; 
    },
    
    _setupschedule: function(){
    	jQuery("#finalstep").empty();
    	var form = jQuery("#calendarform");
		var formelements = form[0];
		var selectedSpots = 0;
		this.jscheduled = [];
		this.jscheduledName = [];
		
		for (var i = 0; i<formelements.length; i++){
			if(formelements[i].checked){
				selectedSpots++;
				if(!this._checkContinuity(formelements, i)){
					this._showWarning("There cannot be single half hour selected. You must select at least 1 hour (2 slots)")
					return;
				}
				this.jscheduled.push(formelements[i].value);
				this.jscheduledName.push(formelements[i].name);
			}
		}
		if(selectedSpots == 0){
			this._showWarning("Please select slots")
			return;
		}else if((selectedSpots % 2) != 0){
                    this._showWarning("Sorry, you can only select slots in grouping of 2 (1h)");
                    return;
		}
		var html = [];
		html.push('<table cellspacing="7px" cellpadding="0">');		
		var options = [];
		options.push('<select name="games" id="_games" multiple="multiple" size="5" style="width: 200px; padding: 3px;" >');
		for (var i = 0; i<this.ownerinfo.games.length; i++){
			options.push('<option value="'+this.ownerinfo.games[i].game_id+'">'+this.ownerinfo.games[i].game_name+'</option>');
		}
		options.push('</select>');
		
		
		html.push(this._getTwoCellRow('Game:', options.join("")));
		html.push('<tr><td colspan="2" align="right">*Press Ctrl or Shift select multiple games</td></tr>');
		var times = [];
		times.push('<div class"wrap">');
		for (var i = 0; i<this.jscheduledName.length; i++){
			var time = this.jscheduledName[i].split('#')
			if(i == 0 || i == 16 || i == 32)
				times.push('<div style="float: left; margin-right: 15px;">'); 
			times.push('<div>'+time[0]+' - '+time[1]+'</div>');
			if(i == 15 || i == 31)
				times.push('</div>');
		}
		times.push('</div>');
		html.push(this._getTwoCellRow('Time:', times.join("")));		
		
		html.push('</table>'); 
		html.push('<div style=" margin-bottom: 10px "><input id="_recursive" onclick="if(jQuery(this).is(\':checked\')){jQuery(\'#_recusivityData\').slideDown();}else{jQuery(\'#_recusivityData\').slideUp();}" name="_recursive" type="checkbox" title="Recursive"><label for="_recursive" title="Recursive">Repeat</label></div>');
		html.push('<div id="_recusivityData" style="display:none;"><table cellspacing="5px" cellpadding="0">');			
		var day = (this.selectedDay.getMonth()+1)+'/'+this.selectedDay.getDate()+'/'+this.selectedDay.getFullYear();
		html.push(this._getTwoCellRow('From:', '<div id="_fromWrapper"><input name="_from" id="_from" type="text" maxlength="10" title="from" value="'+day+'" autocomplete="OFF" style=""><div/>'));
		html.push(this._getTwoCellRow('To:', '<div id="_toWrapper"><input name="_to" id="_to" type="text" maxlength="10" title="to" autocomplete="OFF" style=""><div/>'));
		var weekDayCheckBoxes = [];
		weekDayCheckBoxes.push('<span class="" style="margin-right: 10px"><input id="SundayCheck" name="SU" type="checkbox" title="Sunday"><label for="SundayCheck" title="Sunday">Sun</label></span>');	
		weekDayCheckBoxes.push('<span class="" style="margin-right: 10px"><input id="MondayCheck" name="MO" type="checkbox" title="Monday"><label for="MondayCheck" title="Wednesday">Mon</label></span>');	
		weekDayCheckBoxes.push('<span class="" style="margin-right: 10px"><input id="TuesdayCheck" name="TU" type="checkbox" title="Tuesday"><label for="Tuesday" title="Tuesday">Tue</label></span>');	
		weekDayCheckBoxes.push('<span class="" style="margin-right: 10px"><input id="WednesdayCheck" name="WE" type="checkbox" title="Wednesday"><label for="WednesdayCheck" title="Wednesday">Wed</label></span><br/>');	
		weekDayCheckBoxes.push('<span class="" style="margin-right: 10px"><input id="ThursdayCheck" name="TH" type="checkbox" title="Thursday"><label for="ThursdayCheck" title="Thursday">Thu</label></span>');	
		weekDayCheckBoxes.push('<span class="" style="margin-right: 10px"><input id="FridayCheck" name="FR" type="checkbox" title="Friday"><label for="FridayCheck" title="Friday">Fri</label></span>');	
		weekDayCheckBoxes.push('<span class="" style="margin-right: 10px"><input id="SaturdayCheck" name="SA" type="checkbox" title="Saturday"><label for="SaturdayCheck" title="Saturday">Sat</label></span>');	
		html.push(this._getTwoCellRow('Repeat Every:', weekDayCheckBoxes.join("")));
		html.push('</table></div>');
		html.push('<div class="button-flex " style="margin-top: 10px">');
		html.push('<a href="#" class="calendarbutton button" onclick="DP_jQuery_' + dpuuid + '.timecalendar._makeavailable()"><span>Finish</span></a>');
		html.push('<a href="#" class="calendarbutton button" onclick="jQuery(\'#finalstep\').hide(); jQuery(\'#finalstep\').empty();jQuery(\'#calendaroverlay\').fadeOut(\'fast\');"><span>Close</span></a>');
		html.push('</div>');
		jQuery("#finalstep").html(html.join(""));
		jQuery('#calendaroverlay').fadeIn('fast'); 
		var width = jQuery("#finalstep").width();
		jQuery("#finalstep").css('margin-left', -(width/2));
		jQuery("#finalstep").show('fast', function(){
			jQuery('#_from').datepicker({disabled: true,altFormat: "mm/dd/YYYY" });
			jQuery('#_to').datepicker({ altFormat: "mm/dd/YYYY", minDate: jQuery.timecalendar.selectedDay });
			jQuery( "#finalstep" ).draggable();
		});
    },
    
    _makeavailable: function(overwrite){
    	if(!overwrite)
    		var overwrite = 0;
    	
    	var games = jQuery("#_games").val();
    	if(!games){
    		games = [];
    		for (var i = 0; i<jQuery("#_games")[0].length; i++){
    			games.push(jQuery("#_games")[0][i].value);
    		}
    	}
    	
    	var weekDays = '';
    	if(jQuery('#SundayCheck').is(':checked'))
    		weekDays = weekDays+'0';
    	if(jQuery('#MondayCheck').is(':checked'))
    		weekDays = weekDays+'1';
    	if(jQuery('#TuesdayCheck').is(':checked'))
    		weekDays = weekDays+'2';
    	if(jQuery('#WednesdayCheck').is(':checked'))
    		weekDays = weekDays+'3';
    	if(jQuery('#ThursdayCheck').is(':checked'))
    		weekDays = weekDays+'4';
    	if(jQuery('#FridayCheck').is(':checked'))
    		weekDays = weekDays+'5';
    	if(jQuery('#SaturdayCheck').is(':checked'))
    		weekDays = weekDays+'6';
    	var fromTime = "";
    	var toTime = "";
    	var fromAux = jQuery("#_from").val();
    	if(fromAux != ''){
	    	var a = fromAux.split('/');
	    	var amonth= a[0].charAt(0)=='0'?a[0].charAt(1):a[0];
	    	var aday=a[1].charAt(0)=='0'?a[1].charAt(1):a[1];
	    	var from = new Date(a[2], (parseInt(amonth)-1), aday, 0, 0, 0, 0);
	    	fromTime = from.getTime()
	    }
    	var toAux = jQuery("#_to").val();
    	
    	if(toAux != ''){
	    	var b = toAux.split('/');
	    	var bmonth= b[0].charAt(0)=='0'?b[0].charAt(1):b[0];
	    	var bday=b[1].charAt(0)=='0'?b[1].charAt(1):b[1];
	    	var to = new Date(b[2],(parseInt(bmonth)-1), bday, 0, 0, 0, 0);
	    	toTime=to.getTime();
    	}
    	var description = "_";
    	var url = "datafeed.php";

		jQuery.post(url,{"method": "addslots", "recursive": jQuery('#_recursive').is(':checked'), "userid": this.userId,"from": fromTime,"to": toTime, "schedules": this.jscheduled.join(),"date": this.selectedDay.getTime(),"description":description,"overwrite":overwrite,"dateFormat":this._defaults.dateFormat,"games":games.join('#'),"weekDays":weekDays}, function(data){
			jQuery("#finalstep").hide();
			if(data.IsSuccess){
				jQuery.timecalendar._refreshCalendarpicker(jQuery.timecalendar.userId, function(data){
					//jQuery('#timecalendar').timecalendar({inline: true});
					var target = jQuery('#timecalendar');
					var inst = jQuery.timecalendar._getInst(target[0]); 
					jQuery.timecalendar._updateDatepicker(inst);
				});
				jQuery.timecalendar._showWarning("Done",'DP_jQuery_' + dpuuid + '.timecalendar._customSelectDayAction(\''+jQuery.timecalendar.selectedDay+'\')');
			}
			else if(data.ErrorCode == -2){
				//jQuery.timecalendar._showYesNoValidation("Overwrite?",'DP_jQuery_' + dpuuid + '.timecalendar._makeavailable(\'1\')');
                                jQuery.timecalendar._showWarning("You can not overwrite slots that are already scheduled. <br/> You must cancel it first in the <a class='calendarLink' href=\"schedule.php\">schedule</a> screen ");
			}
		}, 
		"json");
    },
    

	
	_refreshCalendarpicker : function(value, callback){
		var url = "datafeed.php?method=getmonthcalendar&userid="+value;
		jQuery.get(url, function(data){
			jQuery.timecalendar.daysAvailability = jQuery.parseJSON(data);
			if(callback)
				callback(data);		
		}, "json");
		
	},
    
    _clearElement: function(value,callback){
    	jQuery(value).empty();
    	if(callback)
    		callback();
    },
    
    _getFullSequence: function(jscheduled){
    	var array = [];
    	var mainElement;
    	var elem;
    	
    	for(var i = 0; i<jscheduled.length; i++){
    		var index = parseInt(jscheduled[i]);
    		mainElement = this.dayArray[index]
    		array.push(index);
    		for(var j = (index+1); j< 48; j++){
    			elem = this.dayArray[j];
    			if(elem && !elem.isEmpty && elem.calendarRef == mainElement.calendarRef ){
    				array.push(j);
    			}else
    				break;
    		}

    		for(var j = (index-1); j >= 0; j--){
    			elem = this.dayArray[j];
    			if(elem && !elem.isEmpty && elem.calendarRef == mainElement.calendarRef ){
    				array.push(j);
    			}else
    				break;
    		}
    	}
    	array.sort(function(a,b){return a-b});
    	//Remove repeated
    	var ret = [];
    	for(var i = 0; i<array.length; i++){
    		if(i>0){
    			if(array[i] != array[i-1])
    				ret.push(array[i]);
    		}else
    			ret.push(array[i]);
    			
    	}
    	
    	return ret;
    	
    },
    
    _getCoachFullSequence: function(jscheduled){
    	var array = [];
    	var mainElement;
    	var elem;
    	
    	for(var i = 0; i<jscheduled.length; i++){
    		var index = parseInt(jscheduled[i]);
    		mainElement = this.dayArray[index]
    		array.push(index);
    		for(var j = (index+1); j< 48; j++){
    			elem = this.dayArray[j];
    			if(elem && !elem.isEmpty && elem.calendarRef == mainElement.calendarRef ){
    				array.push(j);
    			}else
    				break;
    		}

    		for(var j = (index-1); j >= 0; j--){
    			elem = this.dayArray[j];
    			if(elem && !elem.isEmpty && elem.calendarRef == mainElement.calendarRef ){
    				array.push(j);
    			}else
    				break;
    		}
    	}
    	array.sort(function(a,b){return a-b});
    	//Remove repeated
    	var ret = [];
    	for(var i = 0; i<array.length; i++){
    		if(i>0){
    			if(array[i] != array[i-1])
    				ret.push(array[i]);
    		}else
    			ret.push(array[i]);
    			
    	}
    	
    	return ret;
    	
    },
    
    _getOrfanSlots: function(jscheduled){
    	var array = [];
    	var mainElement;
    	var elem;
    	
    	for(var i = 0; i<jscheduled.length; i++){
    		var index = parseInt(jscheduled[i]);
    		mainElement = this.dayArray[index]
    		array.push(index);
    		for(var j = (index+1); j< 48; j++){
    			elem = this.dayArray[j];
    			if(elem && !elem.isEmpty && elem.calendarRef == mainElement.calendarRef ){
    				array.push(j);
    			}else
    				break;
    		}

    		for(var j = (index-1); j >= 0; j--){
    			elem = this.dayArray[j];
    			if(elem && !elem.isEmpty && elem.calendarRef == mainElement.calendarRef ){
    				array.push(j);
    			}else
    				break;
    		}
    	}
    	array.sort(function(a,b){return a-b});
    	//Remove repeated
    	var ret = [];
    	for(var i = 0; i<array.length; i++){
    		if(i>0){
    			if(array[i] != array[i-1])
    				ret.push(array[i]);
    		}else
    			ret.push(array[i]);
    			
    	}
    	
    	for(var i = 0; i<jscheduled.length; i++){
    		
    	}
    	
    	for(var i = 0; i<ret.length; i++){
    	
    	}
    	
    	return ret;
    	
    },
   
    _cancel: function(){
    	var form = jQuery("#calendarform");
        var formelements = form[0];
        var selectedSpots = 0;
        this.jscheduledName = [];
        this.jscheduled = [];
        var aux = [];
        for (var i = 0; i<formelements.length; i++){
            if(formelements[i].checked){
                selectedSpots++;
                this.jscheduled.push(formelements[i].value);
                var index = parseInt(formelements[i].value);
                this.jscheduledName.push(this.fullDayTime[index]+"#"+this.fullDayTime[index+1]);
            }
        }

        if(selectedSpots == 0){
            this._showWarning("Please select slots")
            return;
        }

        var times = [];
        times.push('<div class"wrap">');
        for (var i = 0; i<this.jscheduledName.length; i++){
            var time = this.jscheduledName[i].split('#')
            if(i == 0 || i == 16 || i == 32)
                   times.push('<div style="float: left; margin-right: 15px;">');
            times.push('<div>'+time[0]+' - '+time[1]+'</div>');
            if(i == 15 || i == 31)
                times.push('</div>');
        }
        times.push('</div>');

        var html = [];
        html.push('<div class="calendarTitle">Cancel</div>');	
        html.push('<div class="warningmessage" style="display: none;">By cancelling a time slot the whole  sequence of time slots with the same coach will cancelled, check the list below to see what is going to canceled.</div>');	
        html.push('<table cellspacing="7px" cellpadding="0">');		
        html.push(this._getTwoCellRow('Time:', times.join("")));
        html.push('</table>');
        html.push('<div class="button-flex " style="margin-top: 10px">');
        html.push('<a href="#" class="calendarbutton button" onclick="jQuery(\'#finalstep\').hide(); jQuery(\'#finalstep\').empty();jQuery(\'#calendaroverlay\').fadeOut(\'fast\');"><span>Close</span></a>');
        if(this.ownerinfo.user_id == jQuery.timecalendar.dayArray[this.jscheduled[0]].requestoruserId){//is trainee
                html.push('<a href="#" class="calendarbutton button" onclick="DP_jQuery_' + dpuuid + '.timecalendar._cancelAction(0,true)"><span>Finish</span></a>');
        }else{
            html.push('<a href="#" class="calendarbutton button" onclick="DP_jQuery_' + dpuuid + '.timecalendar._cancelAction(0,false)"><span>Finish</span></a>');
        }
            html.push('</div>');
            jQuery("#finalstep").html(html.join(""));
            jQuery('#calendaroverlay').fadeIn('fast');
            var width = jQuery("#finalstep").width();
            jQuery("#finalstep").css('margin-left', -(width/2));
            jQuery("#finalstep").show('fast',function(){jQuery("#finalstep").draggable();});
    },
    
    _cancelAction: function(cancelClientEvents,isTrainee){
	
    	if(!cancelClientEvents)
    		var cancelClientEvents = 0;
                
		jQuery.post("datafeed.php",{"method": "cancel", "userid": this.userId, "schedules": this.jscheduled.join(),"date": this.selectedDay.getTime(),"cancelClientEvents":cancelClientEvents}, function(data){
			jQuery("#finalstep").hide();
			if(data.IsSuccess){
				jQuery.timecalendar._refreshCalendarpicker(jQuery.timecalendar.userId, function(data){
					//jQuery('#timecalendar').timecalendar({inline: true});
					var target = jQuery('#timecalendar');
					var inst = jQuery.timecalendar._getInst(target[0]); 
					jQuery.timecalendar._updateDatepicker(inst);
				});
				
    			jQuery.timecalendar._showWarning("Done",'DP_jQuery_' + dpuuid + '.timecalendar._customSelectDayAction(\''+jQuery.timecalendar.selectedDay+'\')');
    			//TODO; add/remove points
       		}else if(data.ErrorCode == -4 ){
       			//jQuery.timecalendar._showYesNoValidation("You are cancelling events that have alredy been booked by a trainee, are you sure?",'DP_jQuery_' + dpuuid + '.timecalendar._cancelAction(\'1\')');
       			if(isTrainee)
                            jQuery.timecalendar._showWarning("You can not cancel slots that have already been scheduled by players in this screen. <br/> You must cancel it in the <a class='calendarLink' href=\"schedule.php\">schedule</a> screen ");
                        else
                            jQuery.timecalendar._showWarning("You can not cancel slots that have already been scheduled by players in this screen. <br/> You must cancel it in the <a class='calendarLink' href=\"schedule.php\">schedule</a> screen ");
                        
       		}
    	},"json");
    },
    
	_schedule: function(){
		this.jscheduled = [];
		jQuery("#finalstep").empty();
		
		var form = jQuery("#calendarform");
		var formelements = form[0];
		var selectedSpots = 0;
		this.jscheduledName = [];
		
		for (var i = 0; i<formelements.length; i++){
			if(formelements[i].checked){
				selectedSpots++;
				if(!this._checkContinuity(formelements, i)){
                                    this._showWarning("There cannot be single half hour selected.You must select at least 1 hour (2 slots)")
                                    return;
				}
				this.jscheduled.push(formelements[i].value);
				this.jscheduledName.push(formelements[i].name);
			}
		}
		
		if(selectedSpots == 0){
                    this._showWarning("Please select slots");
                    return;
		}else if(selectedSpots > 6){
                    this._showWarning("Sorry, you can only select 6 slots(3hours) at a time");
                    return;
                }else if((selectedSpots % 2) != 0){
                    this._showWarning("Sorry, you can only select slots in grouping of 2 (1h)");
                    return;
		}
		var games = this.dayArray[this.jscheduled[0]].gameId;

		var gamesOptions = [];
		//gamesOptions.push();
		var size = 0;
		var rate ="";
                var rate_a = 0;
                var rate_b = 0;
                var rate_c = 0;
		var total ="";
		for (var i = 0; i<games.length; i++){
			for (var j = 0; j<this.ownerinfo.games.length; j++){
				if(games[i] == this.ownerinfo.games[j].game_id){
					gamesOptions.push('<option value="'+games[i]+'">'+this.ownerinfo.games[j].game_name+'</option>');
                                        rate_a = this.ownerinfo.games[j].rate_A;
                                        rate_b = (this.ownerinfo.games[j].rate_B == 0)?rate_a:this.ownerinfo.games[j].rate_B;
                                        rate_c = (this.ownerinfo.games[j].rate_C == 0)?rate_b:this.ownerinfo.games[j].rate_C;
					size++;
				}
			}
		}
		if(size > 1){
			gamesOptions.unshift('<option value="">Select...</option>');
			rate = "";
		}else{
                   
                    if(selectedSpots > 5)
                            rate = rate_c;
                    else if(selectedSpots > 3)
                            rate = rate_b;
                    else if(selectedSpots > 1)
                            rate = rate_a;
                    
			total = selectedSpots*(rate/2);
		}
		var html = [];
		html.push('<div class="calendarTitle">Schedule information</div>');	
		html.push('<table cellspacing="7px" cellpadding="0">');
		html.push(this._getTwoCellRow('Coach:', this.ownerinfo.username));
		html.push(this._getTwoCellRow('Game:', '<select style="width: 200px; padding: 3px;" id="gameselection" onchange="DP_jQuery_' + dpuuid + '.timecalendar._changeRate(this,'+selectedSpots+')" >'+gamesOptions.join("")+'</select>'));
		html.push(this._getTwoCellRow('Rate:', '<span id="_ratefield">'+rate+'</span>'));
		html.push(this._getTwoCellRow('Date:', (this.selectedDay.getMonth()+1)+'/'+this.selectedDay.getDate()+'/'+this.selectedDay.getFullYear()));//TODO: check
		var times = [];
		times.push('<div class"wrap">');
		for (var i = 0; i<this.jscheduledName.length; i++){
			var time = this.jscheduledName[i].split('#')
			if(i == 0 || i == 16 || i == 32)
				times.push('<div style="float: left; margin-right: 15px;">');
			times.push('<div>'+time[0]+' - '+time[1]+'</div>');
			if(i == 15 || i == 31)
				times.push('</div>');
		}
		html.push(this._getTwoCellRow('Time:', times.join("")));
		//html.push(this._getTwoCellRow('Total:', '<span id="_totalfield">'+total+'</span>'));
		html.push('<tr><td colspan="2"><span class="formerrormessage" id="_formerrormessage"></span></td></tr>');
		
		html.push('</table>');
		html.push('<div class="button-flex ">');
		html.push('<a href="#" class="calendarbutton button" id="_finishButton" onclick="DP_jQuery_' + dpuuid + '.timecalendar._finalize()"><span>Finish</span></a>');
		html.push('<a href="#" class="calendarbutton button" onclick="jQuery(\'#finalstep\').hide(); jQuery(\'#finalstep\').empty();jQuery(\'#calendaroverlay\').fadeOut(\'fast\');"><span>Close</span></a>');
		html.push('</div>');
		jQuery("#finalstep").html(html.join(""));
		jQuery('#calendaroverlay').fadeIn('fast');
		var width = jQuery("#finalstep").width();
		jQuery("#finalstep").css('margin-left', -(width/2));
		jQuery("#finalstep").show('fast',function(){jQuery("#finalstep").draggable();});
	},
	
	_finalize: function(){
		var game = jQuery('#gameselection').val();
		if(game == ""){
			jQuery('#_formerrormessage').text("you must select a game");
			jQuery('#_formerrormessage').show();
			return;
		}
		var total = jQuery('#_ratefield')[0].textContent;

                if(parseInt(total) > parseInt(this.ownerinfo.loggedUser.total_points)){
			jQuery("#finalstep").hide();
			jQuery.timecalendar._showWarning("You don't have enough credits <br/> Click <a class='calendarLink' href=\"cashier.php\">here</a> to buy more credits",'DP_jQuery_' + dpuuid + '.timecalendar._customSelectDayAction(\''+jQuery.timecalendar.selectedDay+'\')');
			return;
		}
//                var offSet = parseFloat(jQuery.timecalendar.ownerinfo.time_zone)-parseFloat(jQuery.timecalendar.ownerinfo.loggedUser.time_zone);
//		offSet = offSet*2;
                var url = "datafeed.php";
                jQuery.post(url, {"method": "checkIfAlreadyUsed","date": this.selectedDay.getTime(),"schedules": this.jscheduled.join(),"offSet":this.timeOffSet},function(data){
                    jQuery("#finalstep").hide();
                    if(data && data.IsSuccess){
                        
                        jQuery.post(url, {
                            "sendEmail":jQuery.timecalendar.ownerinfo.mail_on_schedule, 
                            "email":jQuery.timecalendar.ownerinfo.email, 
                            "method": "scheduleLesson",
                            "coachname":jQuery.timecalendar.ownerinfo.username,
                            "requestorname":jQuery.timecalendar.ownerinfo.loggedUser.username,
                            "total":total,"coachPoints":jQuery.timecalendar.ownerinfo.total_points,
                            "studentPoints":jQuery.timecalendar.ownerinfo.loggedUser.total_points,
                            "userid": jQuery.timecalendar.userId,
                            "schedules": jQuery.timecalendar.jscheduled.join(),
                            "date": jQuery.timecalendar.selectedDay.getTime(),
                            "description":"_",
                            "gameid":jQuery('#gameselection').val(),
                            "rate":jQuery('#_ratefield').text(),
                            "offSet":jQuery.timecalendar.timeOffSet},function(data){
                            if(data && data.IsSuccess){
                                jQuery.timecalendar._showWarning("Done",'DP_jQuery_' + dpuuid + '.timecalendar._customSelectDayAction(\''+jQuery.timecalendar.selectedDay+'\')');  
                                jQuery.timecalendar._addAsFriend(jQuery.timecalendar.ownerinfo.user_id);
                            }else{
                                jQuery.timecalendar._showWarning("We could not proceed with your request, please try again later..");
                            }
                        }, "json");
                        
                        
                    }else if(data.ErrorCode == -2){
                            jQuery.timecalendar._showWarning("You already have scheduled in one of the selected slots, check your calendar.");
                    }else{
                            jQuery.timecalendar._showWarning("We could not proceed with your request, please try again later..");
                    }
                }, "json");
	},
	
	_getTwoCellRow: function(arg0, arg1){
		var html = [];
		html.push('<tr valign="top"><td><div class="labelstyle">');
		html.push(arg0);
		html.push('</div></td><td>');
		html.push(arg1);
		html.push('</td></tr>');
		return html.join("");
	},
	
	_split: function(value, separator){},
	
	_newInstance: function(value, callback){
		var url = "datafeed.php?method=getmonthcalendar&userid="+value;
		jQuery.get(url, function(data){
			jQuery.timecalendar = new Timecalendar(data,value);
			var urlUser = "datafeed.php?method=getUserInfo&userid="+value;
			jQuery.get(urlUser, function(data){
				jQuery.timecalendar.ownerinfo = data;
				jQuery.timecalendar.isowncalendar = (data.owncalendar == 1);
                                if(jQuery.timecalendar.isowncalendar){
                                    jQuery.timecalendar.timeOffSet = 0;
                                }else{
                                    var offset = parseFloat(jQuery.timecalendar.ownerinfo.time_zone)-parseFloat(jQuery.timecalendar.ownerinfo.loggedUser.time_zone);
                                    jQuery.timecalendar.timeOffSet = offset*2;
                                }
                                
				if(jQuery.timecalendar.isowncalendar){
					for (var j = 0; j < jQuery.timecalendar.ownerinfo.user_types.length; j++){
						if(jQuery.timecalendar.ownerinfo.user_types[j].user_type_id == 1)
							jQuery.timecalendar.youarecoach = true;
					}
				}
				var gamesurl = "datafeed.php?method=getGames";
				jQuery.get(gamesurl, function(data){
					jQuery.timecalendar.allGamesArray = data;
					callback(data);
				}, "json");
				
			}, "json");
						
		}, "json");
		
	},
		_getDayLink: function(date){
		return "javascript:;";
	},
	
	_changeRate: function(item, quant){ 
		jQuery("#_ratefield").empty();
		var rate = 1;
		for (var j = 0; j<this.ownerinfo.games.length; j++){
			if(item.value == this.ownerinfo.games[j].game_id){
				var rate_a = this.ownerinfo.games[j].rate_A;
				var rate_b = (this.ownerinfo.games[j].rate_B == 0)?rate_a:this.ownerinfo.games[j].rate_B;
				var rate_c = (this.ownerinfo.games[j].rate_C == 0)?rate_b:this.ownerinfo.games[j].rate_C;
			
				if(quant > 5)
					rate = rate_c;
				else if(quant > 3)
					rate = rate_b;
				else if(quant > 1)
					rate = rate_a;
				jQuery("#_ratefield").append(rate);
			}
		}
		jQuery("#_totalfield").empty();
		jQuery("#_totalfield").append(quant*(rate/2));
	}
});


/*
 * Bind hover events for timecalendar elements.
 * Done via delegate so the binding only occurs once in the lifetime of the parent div.
 * Global instActive, set by _updateDatepicker allows the handlers to find their way back to the active picker.
 */ 
function bindHover(dpDiv) {
	var selector = 'button, .ui-timecalendar-prev, .ui-timecalendar-next, .ui-timecalendar-calendar td a';
	return dpDiv.bind('mouseout', function(event) {
			var elem = jQuery( event.target ).closest( selector );
			if ( !elem.length ) {
				return;
			}
			elem.removeClass( "ui-state-hover ui-timecalendar-prev-hover ui-timecalendar-next-hover" );
		})
		.bind('mouseover', function(event) {
			var elem = jQuery( event.target ).closest( selector );
			if (jQuery.timecalendar._isDisabledDatepicker( instActive.inline ? dpDiv.parent()[0] : instActive.input[0]) ||
					!elem.length ) {
				return;
			}
			elem.parents('.ui-timecalendar-calendar').find('a').removeClass('ui-state-hover');
			elem.addClass('ui-state-hover');
			if (elem.hasClass('ui-timecalendar-prev')) elem.addClass('ui-timecalendar-prev-hover');
			if (elem.hasClass('ui-timecalendar-next')) elem.addClass('ui-timecalendar-next-hover');
		});
}
var lastChecked;
function bindMouseEvents(element){
    return jQuery(element).bind('click', function(event) {
        if(event.target.tagName =="A")
            return;
        if(event.target.type =="checkbox"){
            if(event.target.disabled)
                return;
            if(event.target.checked){

                if(lastChecked){
                    if(event.shiftKey){
                        selectFromTo(lastChecked,event.target.value);
                    }
                }
                lastChecked = event.target.value;
            }else
                lastChecked = undefined;
            return;
        }if (this.children[3].children[0].checked){
            if(this.children[3].children[0].disabled)
                    return;
            this.children[3].children[0].checked = false;
            lastChecked = undefined;
        }else{
            if(this.children[3].children[0].disabled)
                    return;
            this.children[3].children[0].checked = true;
            if(lastChecked){
                    if(event.shiftKey){
                            selectFromTo(lastChecked,this.children[3].children[0].value);
                    }
            }
            lastChecked = this.children[3].children[0].value;	

        }


    }).bind('mouseover', function(event) {

            jQuery(this).addClass('calendar_time_row_hover');
    }).bind('mouseout', function(event) {
            jQuery(this).removeClass( "calendar_time_row_hover" );
    });
}
function selectFromTo(from,to){
	var form = jQuery("#calendarform");
	var formelements = form[0];
	to = parseInt(to) - parseInt(jQuery.timecalendar.timeOffSet);
	from = parseInt(from) - parseInt(jQuery.timecalendar.timeOffSet);
	if(to > from){
		for (var i = from; i < to; i++){
			if(formelements[i].disabled)
				continue;
			formelements[i].checked = true;
		}
	}else{
		for (var i = to; i < from; i++){
			if(formelements[i].disabled)
				continue;
			formelements[i].checked = true;
		}
	}
}

/* jQuery extend now ignores nulls! */
function extendRemove(target, props) {
	jQuery.extend(target, props);
	for (var name in props)
		if (props[name] == null || props[name] == undefined)
			target[name] = props[name];
	return target;
};

/* Determine whether an object is an array. */
function isArray(a) {
	return (a && ((jQuery.browser.safari && typeof a == 'object' && a.length) ||
		(a.constructor && a.constructor.toString().match(/\Array\(\)/))));
};

/* Invoke the timecalendar functionality.
   @param  options  string - a command, optionally followed by additional parameters or
                    Object - settings for attaching new timecalendar functionality
   @return  jQuery object */
jQuery.fn.timecalendar = function(options){
	
	/* Verify an empty collection wasn't passed - Fixes #6976 */
	if ( !this.length ) {
		return this;
	}
	
	/* Initialise the date picker. */
	if (!jQuery.timecalendar.initialized) {
		jQuery(document).mousedown(jQuery.timecalendar._checkExternalClick).
			find('body').append(jQuery.timecalendar.dpDiv);
		jQuery.timecalendar.initialized = true;
	}

	var otherArgs = Array.prototype.slice.call(arguments, 1);
	if (typeof options == 'string' && (options == 'isDisabled' || options == 'getDate' || options == 'widget'))
		return jQuery.timecalendar['_' + options + 'Timecalendar'].
			apply(jQuery.timecalendar, [this[0]].concat(otherArgs));
	if (options == 'option' && arguments.length == 2 && typeof arguments[1] == 'string')
		return jQuery.timecalendar['_' + options + 'Timecalendar'].
			apply(jQuery.timecalendar, [this[0]].concat(otherArgs));
	return this.each(function() {
		typeof options == 'string' ?
			jQuery.timecalendar['_' + options + 'Timecalendar'].
				apply(jQuery.timecalendar, [this].concat(otherArgs)) :
			jQuery.timecalendar._attachDatepicker(this, options);
	});
};

jQuery.timecalendar = new Timecalendar(); // singleton instance
jQuery.timecalendar.initialized = false;
jQuery.timecalendar.uuid = new Date().getTime();
jQuery.timecalendar.version = "1.8.21";

window['DP_jQuery_' + dpuuid] = jQuery;

})(jQuery);


function showCalendar(value,inpage){
	var closebtn = '<a id="sbox-btn-close" href="#" onclick="closeCalendar()"></a>';
	if(inpage)closebtn = '';
	if(!inpage){
		jQuery('body').append('<div id="calendarwrapper" style=""></div>');
		jQuery('body').append('<div id="timecalendaroverlay"></div>',jQuery('#timecalendaroverlay'));
		jQuery('#timecalendaroverlay').bind('click', function(event) {closeCalendar();});
		jQuery(document).bind('keyup', function(event) {if (event.keyCode == 27) { closeCalendar(); }});
	}
	var innerhtml = closebtn+'<div class="calendarwrapper" style="float: left; margin-right: 30px;margin-top: 25px;"><div id="timecalendar"></div>'
	+'<table id="calendarlegend" class="calendarlegend" cellspacing="1" cellpadding="2"><tbody><tr align="center"><td class="calendarColor ALL_FREE">&nbsp;</td><td class="calendarLabel">Slot</td><td class="calendarColor AVAILABLE_SPOTS">&nbsp;</td><td class="calendarLabel">Partly booked</td><td class="calendarColor ALL_BOOKED">&nbsp;</td><td class="calendarLabel">Fully booked</td><td class="calendarColor TODAY">&nbsp;</td><td class="calendarLabel">Today</td><td class="calendarColor NOT_AVAILABLE hidden">&nbsp;</td><td class="calendarLabel hidden">Day off</td></tr></tbody></table>'
	+'<div id="calendaroverlay"></div><div id="calendarloading" class="loading"></div></div>'
	+'<div id="timeslotselect" style="float: left"></div><div id="finalstep"></div>';
	
	jQuery('#calendarwrapper').html(innerhtml);
	if(inpage){
		jQuery('#calendarwrapper').addClass("calendarwrapperinpage");
	}else{
		jQuery('#calendarwrapper').addClass("maincalendarwrapper");
		jQuery('#timecalendaroverlay').show();
	}
	jQuery('#calendarwrapper').show();
	
	jQuery.timecalendar._newInstance(value, function(data){
			try{
				jQuery('#timecalendar').timecalendar({inline: true});
				var target = jQuery('#timecalendar');
				var inst = jQuery.timecalendar._getInst(target[0]); 
				jQuery.timecalendar._updateDatepicker(inst);
				var today = new Date();
				today.setHours(0,0,0,0);
				jQuery.timecalendar._customSelectDayAction(today);
				jQuery('#calendarloading').fadeOut('fast',function(){jQuery('#calendaroverlay').fadeOut('fast',function(){jQuery('#calendarlegend').fadeIn('fast',function(){if(!inpage){jQuery( "#calendarwrapper" ).draggable();}/**/});});});
				jQuery('#calendaroverlay').bind('click', function(event) {jQuery.timecalendar._destroy('#time_calendar_warnig');jQuery('#finalstep').hide(); jQuery('#finalstep').empty();jQuery('#calendaroverlay').fadeOut('fast');});
				jQuery(document).bind('keyup', function(event) {if (event.keyCode == 27) { closeCalendar(); }});
				
				
			}catch(e){
				alert(e);
				jQuery('#timecalendaroverlay').fadeOut('fast');
				jQuery('#calendarwrapper').fadeOut('fast',function(){});
				jQuery('body').remove('#calendarwrapper');
				jQuery('body').remove('#timecalendaroverlay');
			}
    	});
}
function closeCalendar(){
    jQuery('#calendarwrapper').fadeOut('fast');
	jQuery('#timecalendaroverlay').fadeOut('slow');
	jQuery('body').remove('#calendarwrapper');
	jQuery('body').remove('#timecalendaroverlay');
}

function outsideInstant(value){
    jQuery.timecalendar._newInstance(value, function(){
        jQuery.timecalendar._instantLesson('#overlay','#pageloading','#outfinalstep','body');
    });
}