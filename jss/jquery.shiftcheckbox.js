/**
 * JQuery shiftcheckbox plugin
 *
 * shiftcheckbox provides a simpler and faster way to select/unselect multiple checkboxes within a given range with just two clicks.
 * Inspired from GMail checkbox functionality
 *
 * Just call $('.<class-name>').shiftcheckbox() in $(document).ready
 *
 * @name shiftcheckbox
 * @type jquery
 * @cat Plugin/Form
 * @return JQuery
 *
 * @URL http://www.sanisoft.com/blog/2009/07/02/jquery-shiftcheckbox-plugin
 *
 * Copyright (c) 2009 Aditya Mooley <adityamooley@sanisoft.com>
 * Dual licensed under the MIT (MIT-LICENSE.txt) and GPL (GPL-LICENSE.txt) licenses
 */
prevChecked = {"":'null'};
selectorStr = {"":'null'};
(function ($) {
    $.fn.shiftcheckbox = function()
    {
        
        var sel = this;
        selectorStr[sel.selector]=sel;
        prevChecked[sel.selector]='null';
        $(sel).bind("click", handleClick);
    };

    function handleClick(event)
    {
        var val = this.value;
        var checkStatus = this.checked;
        //get the checkbox number which the user has checked
        var size =this.classList.length;
        var selector ="";
        var prev;
        for(var i=0; i<size; i++){
            selector = "."+this.classList[i];
            if(prevChecked.hasOwnProperty(selector)){
                prev = prevChecked[selector];
                break;
            }

        }
        if (event.shiftKey) {
            if (prev != 'null') {
                var ind = 0, found = 0, currentChecked;
                currentChecked = getSelected(val,selector);

                ind = 0;
                if (currentChecked < prev) {
                    $(selector).each(function(i) {
                        if (ind >= currentChecked && ind <= prev) {
                            this.checked = checkStatus;
                        }
                        ind++;
                    });
                } else {
                    $(selector).each(function(i) {
                        if (ind >= prev && ind <= currentChecked) {
                            this.checked = checkStatus;
                        }
                        ind++;
                    });
                }

                prevChecked[selector] = currentChecked;
            }else {
                if (checkStatus) {
                    prevChecked[selector]=getSelected(val,selector);
                }
            }
            
        } else {
            if (checkStatus) {
                prevChecked[selector]=getSelected(val,selector);
            }
        }
    };

    function getSelected(val,selector)
    {
        var ind = 0, found = 0, checkedIndex;

        $(selector).each(function(i) {
            if (val == this.value && found != 1) {
                checkedIndex = ind;
                found = 1;
            }
            ind++;
        });

        return checkedIndex;
    };
})(jQuery);