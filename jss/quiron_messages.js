function startChironMessages(parentDiv){
	jQuery.get("actionService.php?method=getMessages",function(data){
		if(data.IsSuccess){
			//var html = buildContent(data);
			buildFriends(parentDiv,data);
			buildMessages(parentDiv,data);
			//jQuery(".messages_user_list").niceScroll();
			//jQuery(".messages_list").niceScroll({autohidemode:"false", zindex:5});
			
			jQuery(".messages_user_wrap").click(function(){
				
				
				var aux = this.id;
				
				var mid=aux.replace("message_user_","messages_list_");
				mid = "#"+mid;
				if(jQuery(mid).is(':visible'))
					return;
					
				jQuery(".messages_user_wrap").removeClass('messages_user_wrap_selected');
				jQuery(this).addClass('messages_user_wrap_selected');
				
				jQuery(".messages_list_wrap").hide('fast');
				jQuery(mid).show('fast');
				jQuery(mid).niceScroll({autohidemode:"false", zindex:5});
				});
				jQuery('.send_message_text').keyup(function(e) {
					if (e.keyCode == 13) {
						var id = "#send_message_"+jQuery(this).attr('toid');
						
						sendMessage(jQuery(id).val(),jQuery(this).attr('toid'), jQuery(this).attr('fromname')); 
					}   
				});
				jQuery('.button_message').click(function(e) {
					var id = "#send_message_"+jQuery(this).attr('toid');
					sendMessage(jQuery(id).val(),jQuery(this).attr('toid'), jQuery(this).attr('fromname')); 
				});
			
		}
	},"json");
}

function buildFriends(parentDiv,data){
	
			
	var html= [];
	html.push('<div class="messages_user_list">');
	
	var messageshtml= [];
	//data.groupedMessages
	var size = data.users.length
	for (var i = 0; i < size; i++){
		var user = data.users[i];
		var photo = user.photo != null ? ('uploaded/user_images/thumbs/mid_'+user.photo) : ('images/featured_img.jpg');
		html.push('<div class="messages_user_wrap ');
		if(i == 0)
			html.push('messages_user_wrap_selected" style=" border-top: 1px solid #CCC"');
		else
			html.push('"');
		
		html.push(' id="message_user_'+user.user_id+'"><div class="messages_user_image">');
		html.push('<img width="35px" height="35px" src="'+photo+'" alt="" border="0" />');
		html.push('</div><div class="messages_user_name">');					
		html.push(user.name);			
		html.push('</div></div>');	
		
	}
	
	html.push('</div>');
	//html.push(buildMessages(data));	
	//return html.join("");	
	jQuery(parentDiv).append(html.join(""));	
}					
function buildMessages(parentDiv,data){
	var size = data.users.length
	var html= [];
	
	for (var i = 0; i < size; i++){
		var user = data.users[i];
		var messagesSize = data.groupedMessages[user.user_id].length;
		
		html.push('<div class="messages_list_wrap" style="');
		if(i == 0)
			html.push('display: block;');
		html.push('" id="messages_list_');
		html.push(user.user_id);
		html.push('">');
		
		html.push('<div class="messages_list" id="messages_');
		html.push(user.user_id);
		html.push('"><div style="">');
		
		var messages= [];
		for (var j = 0; j < messagesSize; j++){
			
			var message = data.groupedMessages[user.user_id][j];
			
			messages.push('<div class="messages_wrap">');
			messages.push('<div class="messages_sender">')
			messages.push(message.from_name);
			messages.push('<div class="messages_date">');
			messages.push(message.sent);
			messages.push('</div></div><div class="messages_content">');
			messages.push(message.message);
			messages.push('</div></div>');
			
		}
		html.push(messages.join(""));
		html.push('</div></div><div class="send_message"><textarea rows="4" cols="50" class="send_message_text" toid="');
		html.push(user.user_id);
		html.push('" fromname="');
		html.push(user.user_id);
		html.push('"  id="send_message_');
		html.push(user.user_id);				
		html.push('"></textarea><div style="text-align: right"><a href="javascript:;" class="button_message button" toid="');
		html.push(user.user_id);
		html.push('" fromname="');
		html.push(user.user_id);
		html.push('">Send</a>');
		html.push('</div></div></div>');
		
	}
	//console.log(html.join(""));
	//return html.join("");
	jQuery(parentDiv).append(html.join(""));	
}					
