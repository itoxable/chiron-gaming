/*
 
 Phono call state
    CONNECTED: 0
    RINGING: 1
    DISCONNECTED: 2
    PROGRESS: 3
    INITIAL: 4
 
 Phono call direction
    OUTBOUND: 0
    INBOUND: 1
 
 */

window.phono = jQuery.phono({
    apiKey: "7dc3cacb3694d75bea38adbab0df388e",
    gateway: "gw-v3.d.phono.com",
    onReady: function() {
        setPhonoSessionId(this.sessionId);
    },
    audio: {
        type: "flash",
        direct: false,
        jar: "/plugins/audio/phono.audio.jar",
        swf: "/plugins/audio/phono.audio.swf",
        onPermissionBoxShow: function(event) {},
        onPermissionBoxHide: function(event) {
            this.showPermissionBox();
        }
    },	  
    phone: {
        ringTone: "/audio/Diggztone_DigitalSanity.mp3",
        ringbackTone: "/audio/ringback-us.mp3",
        headset: true,
        onIncomingCall: function(event) {
            window.phonoCall = event.call;
            incomingCall();

            window.phonoCall.bind({
                onHangup: function(event){
                    if(jQuery('#incommingcallwarning').length > 0){
                        if(jQuery('#incommingcallwarning').is(':visible')){
                            jQuery('#incommingcallwarning').remove(); 
                            jQuery('#overlay').fadeOut('fast');
                        }
                    }
                    FreiChat.changeToMakeCallCallButton(getHeader("x-fromuserid"));
                    console.log("Call ended -- onIncomingCall");
                },
                onError: function(event){
                    console.log("Phone error2: " + event.reason);
                    var fromname = getHeader('x-fromname');
                    var fromid = getHeader("x-fromuserid");
                    FreiChat.sendExternalMessage(event,fromid,fromname,"Call Error.",3);
                }
            });
        },
        onError: function(event) {
            console.log("Phone error: " + event.reason);
        },
        onUnready: function(event) {
            console.log("Phono disconnected");  
        }
    }
});
function makeCall(fromname,fromUserId,toUserId,user,event){
    jQuery.post("actionService.php",{"method": "getUser","userId":toUserId}, function(data){
        window.phonoCall = phono.phone.dial(data.phono_session, {
            gain: 25,
            volume: 75,
            mute: false,
            pushToTalk: false,
            event: event,
            toId: toUserId,
            toUsername: user,
            username: fromname,

            // Events
            onRing: function() {
                    console.log("Ringing");
                    FreiChat.sendExternalMessage(this.config.event,this.config.toId,this.config.toUsername,"Calling!!",3);
                    FreiChat.changeToRingingButton(getHeader("x-touserid"));
                    goBusy();
            },
            onAnswer: function() {
                    console.log("Answered");
                    FreiChat.sendExternalMessage(this.config.event,this.config.toId,this.config.username,"Call connected.",3);
                    FreiChat.changeToHangUpCallButton(getHeader("x-touserid"));
            },
            onHangup: function() {	        	
                    FreiChat.changeToMakeCallCallButton(getHeader("x-touserid"));
                    FreiChat.sendExternalMessage(this.config.event,this.config.toId,this.config.username,"Call ended.",3);
                    console.log("Call ended -- makeCall");
                    goOnline();
            },
            onError: function() {
                    console.log("Call Error");
                    FreiChat.sendExternalMessage(this.config.event,this.config.toId,this.config.username,"Call Error.",3);
                    goOnline();
            },
            headers: [{
                    name: "x-fromname",
                    value: fromname
                },
                {
                    name: "x-fromuserid",
                    value: fromUserId
                },
                {
                    name: "x-touserid",
                    value: toUserId
                }
            ]
        });
    },"json");
}
function incomingCall(){	
    var message = "Anonymous call";
    var fromname = getHeader('x-fromname');
    if(fromname)
            message = "<b>"+fromname+"</b> is calling";
    var id = getHeader("x-fromuserid");
    
    goBusy();
    showYesNo(message, "",'incommingcallwarning', 'FreiChat.changeToHangUpCallButton('+id+');answer();', 'hangup()','Answer','Reject');
}
function getHeader(name){
    if(window.phonoCall && window.phonoCall.headers){
        var headerSize = window.phonoCall.headers.length;
        for(var i = 0; i < headerSize; i++){
                var header = window.phonoCall.headers[i];
                if(header.name == name){
                        return header.value;
                        break;
                }
        }
    }
    
    return undefined;
}
function answer(){
    window.phonoCall.answer();
}
function hangup(id){
    window.phonoCall.hangup();
    window.phonoCall.stopAudio();
    FreiChat.changeToMakeCallCallButton(id);
    goOnline();
}
function mute(mute){
    window.phonoCall.mute(mute);
}
function talking(talk){
    window.phonoCall.talking(talk);
}
function goBusy(){
//   changeStatus(event,3); 
}
function goOnline(){
//   changeStatus(event,1); 
}