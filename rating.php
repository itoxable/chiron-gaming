<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.custom.min.js?v=1.8"></script>
<script type="text/javascript" src="js/jquery.ui.stars.js?v=3.0.0b38"></script>
	<link rel="stylesheet" type="text/css" href="style/jquery.ui.stars.css?v=3.0.0b38"/>
	<script type="text/javascript">
		$(function(){
		
			$("#Contact").stars({
				inputType: "select",
				//captionEl: $caption, // point to our newly created element
/*				callback: function(ui, type, value)
				{
					$.post("demo2.php", {rate: value}, function(data)
					{
						$("#ajax_response").html(data);
					});
				}
*/
			});

			// Make it available in DOM tree
			//$caption.appendTo("#ratings");
		});
	</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>

<body>
<form id="Contact" action="demo2.php" method="post">

			<select name="rate">
				<option value="1">Very poor</option>
				<option value="2">Not that bad</option>
				<option value="3">Average</option>
				<option value="4">Good</option>
				<option value="5">Perfect</option>
			</select>

			<input type="submit" value="Rate it!" />

		</form>

</body>
</html>
