<table id="table1">
	<thead>
		<tr>
			<th>Id</th>
			<th>Date</th>
			<th>Points</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>

<script type="text/javascript">
	var mygrid1 = $("#table1").ingrid({
		url: 'remote_cashier.php',
		height: 220,
		initialLoad: true,
		rowClasses: ['grid-row-style1'],
		rowSelection: false,
		totalRecords: <?php echo $numberOfTransactions; ?>,
		colWidths: [50,155,60,380],
		resizableCols: false,
		sorting: false,
		resultEmptyMessage: 'No Result'
	});
</script>