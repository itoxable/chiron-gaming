<?php
include('general_include.php');
include('class/json.php');

$game_id = $_REQUEST['type'];
$ladder_id = $_REQUEST['id'];
$race_id = $_REQUEST['rid'];
$server_id = $_REQUEST['sid'];

$html = "<select name='ladder_id' id='ladder_id' class='con-req'>
         <option value=''>Select</option>";

					$LadderSql = "SELECT * FROM ".TABLEPREFIX."_game_ladder WHERE game_id='$game_id' and is_active='Y'";
					$LadderArr = $UserManagerObjAjax->GetRecords("All",$LadderSql);
					for($i=0;$i<count($LadderArr);$i++)
					{
						if($ladder_id == $LadderArr[$i]['ladder_id'])
							$selected = "selected";
						else
							$selected = "";
							
						$html .= "<option value='".$LadderArr[$i]['ladder_id']."' $selected>".$LadderArr[$i]['ladder_name']."</option>";
					}
					
$html .= "</select>";
$html1 = "<select name='race_id' id='race_id' class='con-req'>
         <option value=''>Select</option>";

					$RaceSql = "SELECT * FROM ".TABLEPREFIX."_game_race WHERE game_id='$game_id' and is_active='Y'";
					$RaceArr = $UserManagerObjAjax->GetRecords("All",$RaceSql);
					for($i=0;$i<count($RaceArr);$i++)
					{
						if($race_id == $RaceArr[$i]['race_id'])
							$selected = "selected";
						else
							$selected = "";
							
						$html1 .= "<option value='".$RaceArr[$i]['race_id']."' $selected>".$RaceArr[$i]['race_title']."</option>";
					}
					
$html1 .= "</select>";
$html2 = "<select name='server_id' id='server_id' class='con-req'>
         <option value=''>Select</option>";

					$ServerSql = "SELECT * FROM ".TABLEPREFIX."_game_server WHERE game_id='$game_id' and is_active='Y'";
					$ServerArr = $UserManagerObjAjax->GetRecords("All",$ServerSql);
					for($i=0;$i<count($ServerArr);$i++)
					{
						if($server_id == $ServerArr[$i]['server_id'])
							$selected = "selected";
						else
							$selected = "";
							
						$html2 .= "<option value='".$ServerArr[$i]['server_id']."' $selected>".$ServerArr[$i]['server_name']."</option>";
					}
					
$html2 .= "</select>";


echo json_encode(
	array(
		'flag' 	=> 1,
		'html'	=> $html,
		'html1'	=> $html1,
		'html2' => $html2
	)
);
?>