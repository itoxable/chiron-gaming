<?php
include "general_include.php";
include "checklogin.php";
include "top.php";
//include "left.php";
include('class/image_class_front.php');
include("class/video_class_front.php");

//Image object creation
$create_type="GDThumbnail";
$image_object=new ImageClass($create_type,100,1);

//Video object creation
$video_object = new VideoClass();

$video_id = $_REQUEST['video_id'];


$image_object->SetImagePath("uploaded/video_images");
$video_object->SetVideoPath("uploaded/videos");

if(isset($_POST['submit']))
{
    $Formval=$_POST;
	$video_title = $_POST['video_title'];
	$game_id = $_POST['game_id'];
    $ladder_id = $_POST['ladder_id'];
    $race_id = $_POST['race_id'];
	$video_url = $_POST['video_url'];
	$description = $_POST['description'];
	$is_video_url = $_POST['is_video_url'];
	
	/*$submited_versus       = $_REQUEST['submited_versus'];
	$submited_class         		= $submited_class;
	$submited_team      	   		= $submited_team;
	$submited_type          		= $submited_type;
	$submited_champion      = $submited_champion;
	$submited_map      	   	= $submited_map;
	$submited_mode      	   		= $submited_mode;*/
	
	$valid=1;
	
	$http_video_name=$_FILES['video_doc']['name'];
	$http_video_size=$_FILES['video_doc']['size'];
	$http_video_type=$_FILES['video_doc']['type'];
	$http_video_temp=$_FILES['video_doc']['tmp_name'];	
	if(empty($video_id))
	{
	  $ermsg .=$video_object->CheckVideo($VideoRadio,$http_video_name,$http_video_temp,$http_video_type,$http_video_size,"Upload File",$mandatory=$man_file,$bytesize=10);
	}  
	
	$http_image_name=$_FILES['img1']['name'];
	$http_image_size=$_FILES['img1']['size'];
	$http_image_type=$_FILES['img1']['type'];
	$http_image_temp=$_FILES['img1']['tmp_name'];
	$PhotoT1=$_POST['PhotoT1'];
	$imghidden1=$_POST['imghidden1'];
	if(empty($video_id))
	{
	  $ermsg .=$image_object->CheckImage($PhotoT1,$http_image_name,$http_image_temp,$http_image_type,$http_image_size,$checktype=1,$Photolebel="Upload Image",$man_file);
	}
	if($ermsg != '')
	{
	   $valid=0;
	   $smarty->assign('ermsg',$ermsg);
	}
	if($valid==1)
	{  
			if(count($submited_versus)>0)  $versus_id=implode(",",$submited_versus);
		if(count($submited_class)>0)  $class_id=implode(",",$submited_class);
		if(count($submited_team)>0)  $team_id=implode(",",$submited_team);
		if(count($submited_type)>0)  $type_id=implode(",",$submited_type);
		if(count($submited_champion)>0)  $champion_id=implode(",",$submited_champion);
		if(count($submited_map)>0)  $map_id=implode(",",$submited_map);
		if(count($submited_mode)>0)  $mode_id=implode(",",$submited_mode);
		

			$http_video_name=$_FILES['video_doc']['name'];
			$http_video_temp=$_FILES['video_doc']['tmp_name'];																		
			$Vid1=$video_object->UploadVideo($VideoRadio,$videohidden1,$http_video_name,$http_video_temp);
		
	
			$http_image_name=$_FILES['img1']['name'];
			$http_image_temp=$_FILES['img1']['tmp_name'];
			$Picc1 = $image_object->UploadImage($PhotoT1,$imghidden1,$http_image_name,$http_image_temp,57,$resize_type=2,'','Y',226,$resize_type=5,'169');
	        $uploadfile = "uploaded/video_images/".$Picc1;
		    $mediumimagename = "thumbs/mid_" . $Picc1;
			$smallimagename = "thumbs/small_" . $Picc1;
		    $image_object->create_gdthumbnail($uploadfile,$smallimagename,120,1,90);
		    $image_object->create_gdthumbnail($uploadfile,$mediumimagename,190,1,142);
			if(empty($video_id))
				{
					/* Insert Into Video Starts */
		
					$table_name = TABLEPREFIX."_video ";
		
					$fields_values = array( 
											'video_title'  			=> $video_title,
											'is_video_url'  		=> $is_video_url,
											'description'           => addslashes($description),
											'time_length'  			=> $time_length ,
											'video_url'				=> $video_url,
											'video_file'			=> $Vid1,	
											'video_image'			=> $Picc1,
											'game_id'				=> $game_id,	
											'ladder_id'				=> $ladder_id,											
											'versus_id'     		=> $versus_id,
											'class_id'      		=> $class_id,
											'team_id'      			=> $team_id,
											'type_id'      			=> $type_id,
											'champion_id'      		=> $champion_id,
											'map_id'   				=> $map_id,
											'mode_id' 				=> $mode_id,
											'race_id'               => $race_id,
											'user_id'               => $_SESSION['user_id'],
											'date_added'			=> date('Y-m-d H:i:s')
											);		
					
					$UserManagerObjAjax->InsertRecords($table_name,$fields_values);
		
				}
		
			 elseif(!empty($video_id))
				{
					/* Update Video Starts */
		
					$table_name = TABLEPREFIX."_video ";
		            $Sql1= "UPDATE ".TABLEPREFIX."_video set ladder_id='',versus_id='',class_id='', team_id='', type_id='',champion_id='',map_id='',
			        mode_id='' where video_id='$video_id'";  
		            $UserManagerObjAjax->Execute($Sql1);
					$fields_values = array( 
											'video_title'  			=> $video_title,
											'is_video_url'  		=> $is_video_url,
											'description'           => $description,
											'time_length'  			=> $time_length ,
											'video_url'				=> $video_url,
											'video_file'			=> $Vid1,	
											'video_image'			=> $Picc1,
											'game_id'				=> $game_id,	
											'ladder_id'				=> $ladder_id,
											'versus_id'     		=> $versus_id,
											'class_id'      		=> $class_id,
											'team_id'      			=> $team_id,
											'type_id'      			=> $type_id,
											'champion_id'      		=> $champion_id,
											'map_id'   				=> $map_id,
											'mode_id' 				=> $mode_id,
											'race_id'               => $race_id,
											'date_edited'			=> date('Y-m-d H:i:s')
										   );	
		
					$where=" video_id='$video_id' ";										
		
					$UserManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);
		
					/* Update Video Ends */	
		
				}	
					
				echo "<script>window.location.href='upload_videos.php'</script>";
				exit;  
	}		

}
$submited_ladder=array();
$submited_versus=array();
$submited_team=array();
$submited_type=array();
$submited_champion=array();
$submited_map=array();
$submited_mode=array();
$submited_class=array();
if(!empty($video_id))
{
	/* Get Record For Display Starts */	

	$SelectCatSql="SELECT * FROM ".TABLEPREFIX."_video WHERE video_id=".$video_id;
	$Formval = $UserManagerObjAjax->GetRecords("Row",$SelectCatSql);
   
	$Formval['description'] 			= stripslashes($Formval["description"]);
	$FabricArr['ladder_id'] 			= $Formval["ladder_id"];
	$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game WHERE game_id=".$Formval['game_id'];
	$RsgameSql = $UserManagerObjAjax->GetRecords("Row",$SelectgameSql);
	
	$FabricArr['is_ladder']					= $RsgameSql["is_ladder"];
	$FabricArr['is_versus']					= $RsgameSql["is_versus"];
	$FabricArr['is_team']					= $RsgameSql["is_team"];
	$FabricArr['is_type']					= $RsgameSql["is_type"];
	$FabricArr['is_champion']				= $RsgameSql["is_champion"];
	$FabricArr['is_map']					= $RsgameSql["is_map"];
	$FabricArr['is_mode']					= $RsgameSql["is_mode"];
	$FabricArr['is_class']					= $RsgameSql["is_class"];

	
	$SelectVersusSql="SELECT * FROM ".TABLEPREFIX."_game_versus WHERE  game_id=".$Formval['game_id']." AND versus_id IN (".$Formval["versus_id"].")";
	$RsVersusSql = $UserManagerObjAjax->GetRecords("All",$SelectVersusSql);	
	

	for($i=0;$i<count($RsVersusSql);$i++)
	{		
			$submited_versus[] = $RsVersusSql[$i]['versus_id'];
	}
	
	$SelectteamSql="SELECT * FROM ".TABLEPREFIX."_game_team WHERE  game_id=".$Formval['game_id']." AND team_id IN (".$Formval["team_id"].")";
	$RsteamSql = $UserManagerObjAjax->GetRecords("All",$SelectteamSql);	
	

	for($i=0;$i<count($RsteamSql);$i++)
	{		
			$submited_team[] = $RsteamSql[$i]['team_id'];
	}
	
	$SelecttypeSql="SELECT * FROM ".TABLEPREFIX."_game_type WHERE  game_id=".$Formval['game_id']." AND type_id IN (".$Formval["type_id"].")";
	$RstypeSql = $UserManagerObjAjax->GetRecords("All",$SelecttypeSql);	
	

	for($i=0;$i<count($RstypeSql);$i++)
	{		
			$submited_type[] = $RstypeSql[$i]['type_id'];
	}
	
	$SelectchampionSql="SELECT * FROM ".TABLEPREFIX."_game_champion WHERE game_id=".$Formval['game_id']." AND champion_id IN (".$Formval["champion_id"].")";
	$RschampionSql = $UserManagerObjAjax->GetRecords("All",$SelectchampionSql);	
	

	for($i=0;$i<count($RschampionSql);$i++)
	{		
			$submited_champion[] = $RschampionSql[$i]['champion_id'];
	}
	
	$SelectmapSql="SELECT * FROM ".TABLEPREFIX."_game_map WHERE game_id=".$Formval['game_id']." AND map_id IN (".$Formval["map_id"].")";
	$RsmapSql = $UserManagerObjAjax->GetRecords("All",$SelectmapSql);	
	

	for($i=0;$i<count($RsmapSql);$i++)
	{		
			$submited_map[] = $RsmapSql[$i]['map_id'];
	}
	
	$SelectmodeSql="SELECT * FROM ".TABLEPREFIX."_game_mode WHERE game_id=".$Formval['game_id']." AND mode_id IN (".$Formval["mode_id"].")";
	$RsmodeSql = $UserManagerObjAjax->GetRecords("All",$SelectmodeSql);	
	

	for($i=0;$i<count($RsmodeSql);$i++)
	{		
			$submited_mode[] = $RsmodeSql[$i]['mode_id'];
	}
	
	$SelectclassSql="SELECT * FROM ".TABLEPREFIX."_game_class WHERE game_id=".$Formval['game_id']." AND class_id IN (".$Formval["class_id"].")";
	$RsclassSql = $UserManagerObjAjax->GetRecords("All",$SelectclassSql);	
	

	for($i=0;$i<count($RsclassSql);$i++)
	{		
			$submited_class[] = $RsclassSql[$i]['class_id'];
	}
	
	$SelectmapSql="SELECT * FROM ".TABLEPREFIX."_game_map WHERE game_id=".$Formval['game_id']." AND map_id IN (".$Formval["map_id"].")";
	$RsmapSql = $UserManagerObjAjax->GetRecords("All",$SelectmapSql);	
	

	for($i=0;$i<count($RsmapSql);$i++)
	{		
			$submited_map[] = $RsmapSql[$i]['map_id'];
	}
	
	$SelecttypeSql="SELECT * FROM ".TABLEPREFIX."_game_type WHERE game_id=".$Formval['game_id']." AND type_id IN (".$Formval["type_id"].")";
	$RstypeSql = $UserManagerObjAjax->GetRecords("All",$SelecttypeSql);	
	

	for($i=0;$i<count($RstypeSql);$i++)
	{		
			$submited_type[] = $RstypeSql[$i]['type_id'];
	}
	
	if($Formval['is_video_url'] =="N")
	{
		$man_file="Y";
		$url_link="N";
	}
	else
	{
		$man_file="N";
		$url_link="Y";
	}
	
	/* Get Record For Display Ends */

	$SubmitButton="Edit";	
	$flag=1;
}
else
{
	$SubmitButton="Add";
}

$DisplayVideoControlStr=$video_object->DisplayVideoControl("Upload File","video_doc",$Formval['video_file'],"videohidden1","VideoRadio",$mandatory=$man_file,$display_video="Y",$videosize=10);

$DisplayImageControlStr=$image_object->DisplayImageControl("Upload Image","img1",$Formval['video_image'],"imghidden1","PhotoT1",$instuctiontype=1,$mandatory="Y","Normal",90);

$gameSql = "SELECT game_id,game_name FROM ".TABLEPREFIX."_game where is_active='Y' ORDER BY game_name";
$GameArr = $UserManagerObjAjax->HtmlOptionArrayCreate($gameSql);

	
	$LadderSql = "SELECT ladder_id,ladder_name  FROM ".TABLEPREFIX."_game_ladder  ORDER BY date_added DESC ";  
	$LadderArr = $UserManagerObjAjax->HtmlOptionArrayCreate($LadderSql);
	
	$VersusSql = "SELECT *  FROM ".TABLEPREFIX."_game_versus  ORDER BY date_added DESC ";   
	$VersusArr = $UserManagerObjAjax->GetRecords("All",$VersusSql);	
	
	$TeamSql = "SELECT *  FROM ".TABLEPREFIX."_game_team   ORDER BY date_added DESC ";   
	$TeamArr = $UserManagerObjAjax->GetRecords("All",$TeamSql);
	
	$TypeSql = "SELECT *  FROM ".TABLEPREFIX."_game_type ORDER BY date_added DESC ";   
	$TypeArr = $UserManagerObjAjax->GetRecords("All",$TypeSql);
	
	$ChampionSql = "SELECT *  FROM ".TABLEPREFIX."_game_champion ORDER BY date_added DESC ";   
	$ChampionArr = $UserManagerObjAjax->GetRecords("All",$ChampionSql);
	
	$ClassSql = "SELECT *  FROM ".TABLEPREFIX."_game_class ORDER BY date_added DESC ";   
	$ClassArr = $UserManagerObjAjax->GetRecords("All",$ClassSql);
	
	$ModeSql = "SELECT *  FROM ".TABLEPREFIX."_game_mode ORDER BY date_added DESC ";   
	$ModeArr = $UserManagerObjAjax->GetRecords("All",$ModeSql);
	
	$MapSql = "SELECT *  FROM ".TABLEPREFIX."_game_map ORDER BY date_added DESC ";   
	$MapArr = $UserManagerObjAjax->GetRecords("All",$MapSql);

$smarty->assign('is_coach',$is_coach);
$smarty->assign('is_partner',$is_partner);
$smarty->assign('video_id',$video_id);
$smarty->assign('FabricArr',$FabricArr);
$smarty->assign('DisplayVideoControlStr',$DisplayVideoControlStr);
$smarty->assign('DisplayImageControlStr',$DisplayImageControlStr);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('GameArr',$GameArr);
$smarty->assign('Formval',$Formval);
$smarty->assign('LadderArr',$LadderArr);
$smarty->assign('VersusArr',$VersusArr);
$smarty->assign('TeamArr',$TeamArr);
$smarty->assign('TypeArr',$TypeArr);
$smarty->assign('ChampionArr',$ChampionArr);
$smarty->assign('ClassArr',$ClassArr);
$smarty->assign('ModeArr',$ModeArr);
$smarty->assign('MapArr',$MapArr);
$smarty->assign('submited_versus',$submited_versus);
$smarty->assign('submited_ladder',$submited_ladder);
$smarty->assign('submited_team',$submited_team);
$smarty->assign('submited_type',$submited_type);
$smarty->assign('submited_champion',$submited_champion);
$smarty->assign('submited_class',$submited_class);
$smarty->assign('submited_mode',$submited_mode);
$smarty->assign('submited_map',$submited_map);
$smarty->register_modifier("inarray","in_array");
$smarty->display('my_video_update.tpl');
include "footer.php";
?>