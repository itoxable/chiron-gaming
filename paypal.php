<?php

	$page_name=basename($_SERVER['SCRIPT_FILENAME']);
	
	include("general_include.php");
	
        $paypalid = "";
        $paypalurl = "";
        $facebook = "";
        $twitter = "";
        $SqlSettings = "SELECT * FROM ".TABLEPREFIX."_settings";
        $settingsArr = $UserManagerObjAjax->GetRecords("All",$SqlSettings);
        foreach($settingsArr as $setting){
            if($setting['id'] == PAYPAL_ID){
                $paypalid = $setting['value'];
            }else if($setting['id'] == PAYPAL_URL){
                $paypalurl = $setting['value'];
            }else if($setting['id'] == FACEBOOK_URL){
                $facebook = $setting['value'];
            }else if($setting['id'] == TWITTER_ID){
                $twitter = $setting['value'];
            }
        }
        
        
	if (empty($_GET['action'])) $_GET['action'] = 'process';
	
	if ($_GET['action'] <> 'ipn') include "checklogin.php";
	
	require_once('paypal.class.php');  // include the class file
	include_once('transaction_status.class.php');
	
	$p = new paypal_class;             // initiate an instance of the class
	$p->paypal_url = $paypalurl;   // testing paypal url
	//$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
	
	$p->test_enable = true;
	// By default is disable, here it is being activated.

	// setup a variable for this script (ie: 'http://www.micahcarrick.com/paypal.php')
	$this_script = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
	$cashier = 'http://'. $_SERVER['HTTP_HOST'] .'/cashier.php';

	// if there is not action variable, set the default action of 'process'
	if (empty($_GET['action'])) $_GET['action'] = 'process';  
	
	switch ($_GET['action']) {
		
		case 'process':      // Process and order...

			// There should be no output at this point.  To process the POST data,
			// the submit_paypal_post() function will output all the HTML tags which
			// contains a FORM which is submited instantaneously using the BODY onload
			// attribute.  In other words, don't echo or printf anything when you're
			// going to be calling the submit_paypal_post() function.
				 
			// This is where you would have your form validation  and all that jazz.
			// You would take your POST vars and load them into the class like below,
			// only using the POST values instead of constant string expressions.
				 
			// For example, after ensureing all the POST variables from your custom
			// order form are valid, you might have:
			//
			// $p->add_field('first_name', $_POST['first_name']);
			// $p->add_field('last_name', $_POST['last_name']);
			
			if (isset($_POST['id']) || isset($_GET['id'])) {
				// Transaction ID
				$txDate = date("d/m/Y : H:i:s", time());
				
				$txId = md5($_SESSION['user_id'].$txDate);
				
				$ratePointId = isset($_POST['id']) ? $_POST['id'] : $_GET['id'];
                                
                                
                                $sqlQuery = "INSERT INTO ". TABLEPREFIX ."_user_transaction(nk_user_id, tx_date, rate_point_id, status, nk_tx_id, description) VALUES
                                    (". $_SESSION['user_id'] .", STR_TO_DATE('". $txDate ."', '%d/%m/%Y : %H:%i:%s'),". $ratePointId .", ". TransactionStatus::WAITING_CONF .",'". $txId ."','')";
				
                                $UserManagerObjAjax->Execute($sqlQuery);

				if(!mysql_error()) {

					$sqlQuery = "SELECT * FROM ". TABLEPREFIX ."_rate_points WHERE rate_point_id=". $ratePointId;
					$record = $UserManagerObjAjax->GetRecords("Row",$sqlQuery);
					
					$p->add_field('business', $paypalid);
					$p->add_field('return', $this_script.'?action=success');
					$p->add_field('cancel_return', $this_script.'?action=cancel&custom='.$txId);
					$p->add_field('notify_url', $this_script.'?action=ipn');
					$p->add_field('undefined_quantity', '1');
					$p->add_field('item_name', $record['description']);
					$p->add_field('amount', $record['rate']);
					$p->add_field('custom', $txId);
					logToFile("PayPal: ".$p);
					$p->submit_paypal_post(); // submit the fields to paypal
				}
			}
			//$p->dump_fields();      // for debugging, output a table of all the fields
		break;

		case 'success':      // Order was successful...

				
			echo 	"<html>
						<head>
							<title>Success</title>
							<meta HTTP-EQUIV=\"REFRESH\" content=\"5; url=". $cashier."\">
						</head>
						<body style=\"background-color: #F2F2F2\">
							<div style=\"margin: 0 auto; margin-top: 50px; text-align: center;\">
								<img src=\"../images/FINAL-LOGO.png\" alt=\"Chirongaming logo\" \>

								<h1>Your transaction has been successful</h1>
								<h2>In a few seconds you will be redirected to Chirongaming</h2>
								<h3>If you don't wish to wait click <a href=\"/cashier.php\">here</a></h3>
							</div>
						</body>
					</html>";
		

		break;

		case 'cancel':       // Order was canceled...
		
			$addSelect = "SELECT * ";
			$addDelete = "DELETE ";
			
			$txId = $_GET['custom'];
			
			$restQuery = " FROM ". TABLEPREFIX ."_user_transaction WHERE nk_tx_id='". $txId ."' AND status = ". TransactionStatus::WAITING_CONF;
		
			$sqlQuery = $addSelect . $restQuery;
			$record = $UserManagerObjAjax->GetRecords("Row",$sqlQuery);
			if (sizeof($record) > 0) {
				$sqlQuery = $addDelete . $restQuery;
				$UserManagerObjAjax->Execute($sqlQuery);
				$plusUrl="?txStatus=2&txMsg=The order was canceled.";
				$cashier = $cashier . $plusUrl;
			}
			// The order was canceled before being completed.
			echo 	"<html>
						<head>
							<title>Success</title>
							<meta HTTP-EQUIV=\"REFRESH\" content=\"0; url=". $cashier."\">
						</head>
					</html>";

		break;

		case 'ipn':   

			if ($p->validate_ipn()) {
				
				$qtyBought	=	$p->ipn_data['quantity'];
				$txId		=	$p->ipn_data['custom'];
				$payPalAcct	=	$p->ipn_data['address_name'];
				
				$sqlQuery = "SELECT nk_user_id, tx_date, DATE_FORMAT(tx_date , '%d/%m/%Y : %H:%i:%s') AS formated_tx_date, nk_tx_id, rate_point_id FROM ". TABLEPREFIX ."_user_transaction WHERE nk_tx_id='". $txId ."' AND status = ". TransactionStatus::WAITING_CONF;
				$record = $UserManagerObjAjax->GetRecords("Row",$sqlQuery);
				
				$serverTxDate	=   $record['tx_date'];
				$txDate		=   $record['formated_tx_date'];
				$nkUserId	=   $record['nk_user_id'];
				$ratePointId	=   $record['rate_point_id'];
				
				if (sizeof($record) > 0 && md5($nkUserId.$txDate)==$txId) {
					$sqlQuery = "UPDATE ". TABLEPREFIX ."_user_transaction SET status = ". TransactionStatus::ACTIVE .", qty_points = '". $qtyBought ."' WHERE nk_tx_id='". $txId ."' AND status = ". TransactionStatus::WAITING_CONF;
					$UserManagerObjAjax->Execute($sqlQuery);
					
					$sqlQuery = "SELECT * FROM ". TABLEPREFIX ."_rate_points WHERE rate_point_id=". $ratePointId;
					$record = $UserManagerObjAjax->GetRecords("Row",$sqlQuery);
					
					$rateDescription = $record['description'];
					$totalPoints = $record['qty_points']*$qtyBought;
					$totalCharged = $record['rate']*$qtyBought;
					
					$sqlQuery = "UPDATE ". TABLEPREFIX ."_user SET total_points=". $totalPoints ."+total_points WHERE user_id=". $nkUserId;
					$UserManagerObjAjax->Execute($sqlQuery);
					
					$sqlQuery = "SELECT * FROM ". TABLEPREFIX ."_user WHERE user_id=". $nkUserId;
					$record = $UserManagerObjAjax->GetRecords("Row",$sqlQuery);
					$to = $record['email'];

					$_SESSION['ch_user'] = $record;
                                        
					$subject = 'Payment Notification - '.$rateDescription;
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: Chirongaming - cashier <cashier@chirongaming.com>';
					$TemplateFile="email_templates/transaction_mail.html";
					$TemplateFileHandler=fopen($TemplateFile,"r");
					$TemplateMessage=fread($TemplateFileHandler,filesize($TemplateFile));

					fclose($TemplateFileHandler);
					$TemplateMessage=str_replace("[TXID]",$txId,$TemplateMessage);
					$TemplateMessage=str_replace("[DATE]",$serverTxDate,$TemplateMessage);
					$TemplateMessage=str_replace("[QUANT_BOUGHT]",$qtyBought,$TemplateMessage);
					$TemplateMessage=str_replace("[TOTAL_ADDED]",$totalPoints,$TemplateMessage);
					$TemplateMessage=str_replace("[TOTAL_CHARGED]",$totalCharged,$TemplateMessage);
					$TemplateMessage=str_replace("[TOTAL_POINTS]",$record['total_points'],$TemplateMessage);
					
					$sent = mail($to, $subject, $TemplateMessage, $headers);
					
				}
				
			}
		break;
	}
	
	function insertTransaction($userId, $txStatus, $txType, $value, $oldPoints, $description=""){
		$ret = array();
		try{
		
			$txDate = date("d/m/Y : H:i:s", time());
			$txId = md5($userId.$txDate);
			$points = 0;
			if($txType == TransactionType::CREDIT)
				$points = $oldPoints+$value;
			else
				$points = $oldPoints-$value;
				
			
			$db = new DBConnection();
			$db->getConnection();
			$sql = "INSERT INTO nk_user_transaction(nk_user_id, tx_date, rate_point_id, status, nk_tx_id,transaction_type, qty_points, description, total_before , total_after) VALUES(". $userId .", STR_TO_DATE('". $txDate ."', '%d/%m/%Y : %H:%i:%s'),-1, ". $txStatus .",'". $txId ."',".$txType.",".$value.",'".$description."',".$oldPoints.", ".$points.")";
			
			logToFile($sql);
			
			if(mysql_query($sql)==false){
				$ret['IsSuccess'] = false;
				$ret['Msg'] = mysql_error();
			}else{
				$ret['IsSuccess'] = true;
				$ret['Msg'] = 'add success';
			}
			
			updateUserPoints($points, $userId);
			
		}catch(Exception $e){
			$ret['error'] = $e->getMessage();
		}
		return $ret;
	}
?>