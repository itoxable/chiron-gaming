<!DOCTYPE html>
<html><head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="how it works">  
<meta name="description" content="how it works">
<title>How it works - Chirongaming</title>

<link rel="icon" href="images/icon_32.png" sizes="32x32">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link href="style/style.css" rel="stylesheet" type="text/css">
<link href="style/graphite.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="style/default.css" type="text/css" media="screen">


<script type="text/javascript" async="" src="https://ssl.google-analytics.com/ga.js"></script><script type="text/javascript" src="jss/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="jss/jquery.nicescroll.js"></script>
<link rel="stylesheet" href="style/jquery-ui-1.8.23.custom.css" type="text/css">
<script type="text/javascript" src="jss/jquery-ui-1.8.23.customDEV.js"></script> 
<script type="text/javascript" src="jss/jquery.caret.js"></script>

<link type="text/css" href="style/jquery.jscrollpane.css" rel="stylesheet" media="all">
<script type="text/javascript" src="jss/jquery.mousewheel.js"></script>
<script type="text/javascript" src="jss/jquery.jscrollpane.min.js"></script>

<link rel="stylesheet" href="style/ingrid.css" type="text/css" media="screen">
<script type="text/javascript" src="js/ingrid.js"></script>
<script language="javascript" type="text/javascript" src="_scripts/jQuery.functions.pack.js"></script>
<script type="text/javascript" src="js/jquery.dateLists.min.js"></script>
<script language="javascript" src="js/contact_us.js"></script>
<script language="javascript" type="text/javascript" src="includeajax/AjaxUserInterface.js"></script>
<script language="javascript" type="text/javascript" src="_scripts/common.js"></script>
<script language="javascript" type="text/javascript" src="js/common.js"></script>
<script language="javascript" type="text/javascript" src="sitemanager/js/jquery-impromptu.2.4.min.js"></script>
<link href="style/alert.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="includeajax/user_login.js"></script>

<link rel="stylesheet" type="text/css" href="style/ui.dropdownchecklist.css">
<link rel="stylesheet" type="text/css" href="style/jquery.dateLists.css">
<script type="text/javascript" src="js/ui.core.js"></script>
<script type="text/javascript" src="js/ui.dropdownchecklist.js"></script>
<link rel="stylesheet" href="style/autosuggest.css" type="text/css">
<script type="text/javascript" src="js/jquery.ausu-autosuggest.min.js"></script>


<script type="text/javascript" src="jss/scripts.js"></script> 

</head>

<body>
<div class="navigation">
  <div class="menu">
        <a href="index.php" class="logo"></a>
    <ul class="menuUl">
      
            <li class="menuLi">
              <a class="menuA active" href="howitworks.php">
                <span>How It Works</span>
              </a>
            </li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
            <li class="menuLi">
              <a class="menuA " href="findcoach.php">
                <span>Find Coach</span>
              </a>
            </li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
            <li class="menuLi">
              <a class="menuA " href="find_training_partner.php">
                <span>Find Training Partner</span>
        </a>
      </li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi">
        <a class="menuA " href="javascript:;" onclick="getGMTTime();">
          <span>Master Class</span>
        </a>
      </li>
            <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi"><a class="menuA" href="forum"><span>Forum</span></a></li>
    </ul>
    <ul style="float: right;" class="menuUl">
            <li class="menuLi"><a class="menuA" href="javascript:;" onclick="openLogin()"><span>Login</span></a></li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi"><a class="menuA" href="javascript:;" onclick="openRegister()"><span>Register</span></a></li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi"><a class="menuA" href="javascript:;" onclick="open_lostPwd()"><span>Lost password?</span></a></li>
              </ul>
    </div>

</div>
<div id="bodywrapper">
<div id="overlay"></div>
<div class="loading" id="pageloading" style="margin-left: -30px;margin-top: -50px;display: none;"></div>
<div class="modalpopup ui-draggable" id="registerwindow">
  
  <a id="closeRegisterBtn" class="modalpopup-close-btn" href="#" style=""></a>
  <div id="registeroverlay" class="overlay" style="position: absolute; z-index: 8"></div>
  <div id="registerSuccesswindow" style="">
    <div>
        You have successfully registered
    </div>
    <a class="button" href="#" onclick="closeRegister(event)" style="margin-left: 110px;margin-top: 20px;">Ok</a>
  </div>
  <div class="">
  
    <div class="title">Registration</div>
        <div class="clear"></div>
    
    <div class="register">
      <div id="FormErrorMsg" style="color:red; text-align:center; padding-bottom:10px;"> </div>
      <div class="loading" id="regloading" style="margin-left: -30px;margin-top: -50px;display: none;"></div>
      <div class="overlay" id="regoverlay" style="z-index: 999998; position: absolute"></div>
      <form name="registerForm" id="registerForm" enctype="multipart/form-data" method="post" onsubmit="" action="">
        <fieldset>
            <input type="hidden" name="agreed" value="true">
            <input type="hidden" name="lang" value="en">
            <input type="hidden" name="change_lang" value="0">
            <input type="hidden" name="creation_time" value="1360542858">
            <input type="hidden" name="main_site" value="/">

            <label>Email :</label>
            <input name="email" type="text" id="register-email" class="con-req" value="">
            <label>Username :</label>
            <input name="username" id="username" type="text" value="" class="con-req">
            <p class="clear"></p>
            <label>Password :</label>
            <input name="new_password" type="password" id="password1" class="con-req" value="">
            <label>Confirm Password :</label>
            <input name="password_confirm" type="password" id="password2" class="con-req" value="">
            <p class="clear"></p>
            
            <label>Timezone :</label>
            <select name="tz" id="tz" tabindex="7" class="" style="width: 300px;">
                                     <option value="-12">[UTC - 12] Baker Island Time</option>
                                     <option value="-11">[UTC - 11] Niue Time, Samoa Standard Time</option>
                                     <option value="-10">[UTC - 10] Hawaii-Aleutian Standard Time, Cook Island Time</option>
                                     <option value="-9.5">[UTC - 9:30] Marquesas Islands Time</option>
                                     <option value="-9">[UTC - 9] Alaska Standard Time, Gambier Island Time</option>
                                     <option value="-8">[UTC - 8] Pacific Standard Time</option>
                                     <option value="-7">[UTC - 7] Mountain Standard Time</option>
                                     <option value="-6">[UTC - 6] Central Standard Time</option>
                                     <option value="-5">[UTC - 5] Eastern Standard Time</option>
                                     <option value="-4.5">[UTC - 4:30] Venezuelan Standard Time</option>
                                     <option value="-4">[UTC - 4] Atlantic Standard Time</option>
                                     <option value="-3.5">[UTC - 3:30] Newfoundland Standard Time</option>
                                     <option value="-3">[UTC - 3] Amazon Standard Time, Central Greenland Time</option>
                                     <option value="-2">[UTC - 2] Fernando de Noronha Time, South Georgia &amp; the South Sandwich Islands Time</option>
                                     <option value="-1">[UTC - 1] Azores Standard Time, Cape Verde Time, Eastern Greenland Time</option>
                                     <option selected="" value="0">[UTC] Western European Time, Greenwich Mean Time</option>
                                     <option value="1">[UTC + 1] Central European Time, West African Time</option>
                                     <option value="2">[UTC + 2] Eastern European Time, Central African Time</option>
                                     <option value="3">[UTC + 3] Moscow Standard Time, Eastern African Time</option>
                                     <option value="3.5">[UTC + 3:30] Iran Standard Time</option>
                                     <option value="4">[UTC + 4] Gulf Standard Time, Samara Standard Time</option>
                                     <option value="4.5">[UTC + 4:30] Afghanistan Time</option>
                                     <option value="5">[UTC + 5] Pakistan Standard Time, Yekaterinburg Standard Time</option>
                                     <option value="5.5">[UTC + 5:30] Indian Standard Time, Sri Lanka Time</option>
                                     <option value="5.75">[UTC + 5:45] Nepal Time</option>
                                     <option value="6">[UTC + 6] Bangladesh Time, Bhutan Time, Novosibirsk Standard Time</option>
                                     <option value="6.5">[UTC + 6:30] Cocos Islands Time, Myanmar Time</option>
                                     <option value="7">[UTC + 7] Indochina Time, Krasnoyarsk Standard Time</option>
                                     <option value="8">[UTC + 8] Chinese Standard Time, Australian Western Standard Time, Irkutsk Standard Time</option>
                                     <option value="8.75">[UTC + 8:45] Southeastern Western Australia Standard Time</option>
                                     <option value="9">[UTC + 9] Japan Standard Time, Korea Standard Time, Chita Standard Time</option>
                                     <option value="9.5">[UTC + 9:30] Australian Central Standard Time</option>
                                     <option value="10">[UTC + 10] Australian Eastern Standard Time, Vladivostok Standard Time</option>
                                     <option value="10.5">[UTC + 10:30] Lord Howe Standard Time</option>
                                     <option value="11">[UTC + 11] Solomon Island Time, Magadan Standard Time</option>
                                     <option value="11.5">[UTC + 11:30] Norfolk Island Time</option>
                                     <option value="12">[UTC + 12] New Zealand Time, Fiji Time, Kamchatka Standard Time</option>
                                     <option value="12.75">[UTC + 12:45] Chatham Islands Time</option>
                                     <option value="13">[UTC + 13] Tonga Time, Phoenix Islands Time</option>
                                     <option value="14">[UTC + 14] Line Island Time</option>
                            </select>
           
            
            <p class="clear"></p>

            <label>Captcha :</label>
            <div style="float:left; width:180px;"><!--img src="CaptchaSecurityImages.php?characters=7" /--> <img src="#" id="captcha">
            <br>
            <a href="javascript:;" onclick="document.getElementById('captcha').src='/captcha.php?'+Math.random();document.getElementById('txtCaptcha').focus();" id="change-image">Not readable? Change text.</a>

            </div>

            <label>Security Code :</label>
            <input name="txtCaptcha" id="txtCaptcha" type="text" value="">

            <p class="clear"></p>
            <div style="margin-top:10px;">&nbsp;</div>

            <div style="font-size: 12px;float: right;margin-bottom: 10px;">By clicking Submit, I acknowledge that I have read and agree to our <a href="termsconditions.php">Terms &amp; Conditions</a> and <a href="privacypolicy.php">Privacy Policy</a>.</div>
            <!--<label>&nbsp;</label>-->
            <div style="margin-left: 100px;">
              <a style="float: left;" class="button" href="javascript:;" onclick="return CheckFields();">Submit</a>
              <a style="float: left;" class="button" href="javascript:;" onclick="clearFields()">Reset</a>
            </div>
            <input type="hidden" name="submit" value="Submit">
        </fieldset>
      </form>
    </div>
    <div class="clear"></div>
  </div>
</div>
  <div class="modalpopup ui-draggable" id="loginwindow">
    <a class="modalpopup-close-btn" href="javascript:;" style=""></a>
    <div class="login">
      <div class="title">Login</div>
      <div style="width:100%">
      </div>  
      <br>
      <form name="frmLogin" id="frmLogin" method="post" action="">
     
          <input type="hidden" name="login" value="Login">
        <table>
          <tbody><tr>
            <td>
              <label for="email">E-Mail</label>
            </td>
            <td>
              <input name="email" type="text" value="Enter Your Email" id="email" onkeypress="return submitloginbyenter('frmLogin',this, event)">
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <div id="FormErrorMsg1" style="color:red;">&nbsp;</div>
            </td>
          </tr>
          <tr>
            <td>
              <label for="password">Password</label>
            </td>
            <td>
              <input name="password" type="password" value="password" id="password" onkeypress="return submitloginbyenter('frmLogin',this, event)">
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <div id="FormErrorMsg2" style="color:red;">&nbsp;</div>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <a class="button" href="javascript:;" style="margin-top: 10px" onclick="return submit_frm();">Login</a>
            </td>
          </tr>
          
        </tbody></table>
      </form>
      <div class="clear"></div>
      <p><a href="javascript:;" onclick="closeOpen('loginwindow','registerwindow',event)">Register</a> or <a href="#" onclick="closeOpen('loginwindow','lostPwd',event)">Lost password?</a></p>
    </div>
  </div>
    
  <div class="modalpopup ui-draggable" id="lostPwd">
    <a class="modalpopup-close-btn" href="javascript:;"></a>
    <div class="login">
      <div class="title">Lost your password?</div>
      <div style="width:100%">
        <div id="ErrorMsg" style="color:red; text-align:left; float:left; width:45%">&nbsp;</div>
      </div>  
      <br>
      <form name="frmLostpwd" id="frmLostpwd" method="post" action="">
        <table>
          <tbody><tr>
            <td>
              <label for="email_id">E-Mail</label>
            </td>
            <td>
              <input name="email_id" type="text" value="" id="email_id" onkeypress="return submitenter('frmLostpwd',this, event)">
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <a class="button" href="javascript:;" style="margin-top: 20px" onclick="return Get_pwd();">Submit</a>
            </td>
          </tr>
        </tbody></table>
      </form>
    </div>
    <div class="clear"></div>
        <p style="float: right;"><a onclick="closeOpen('lostPwd','registerwindow',event)">Register</a> or <a href="#" onclick="closeOpen('lostPwd','loginwindow',event)">Login</a></p>
  </div> 

  



<div class="main">


<div class="container-inner">
<link href="https://fonts.googleapis.com/css?family=Dosis:400,700|Prosto+One|Advent+Pro:600,400|Homenaje|Quantico:400,700" rel="stylesheet" type="text/css">
   <div class="">
		<h1 style="font-family: 'Quantico', sans-serif; color: #013469;">What is Chiron Gaming</h1>
		<div class="howWorksText"><div style="margin: 15px;"><div id="Vivamus">
<p><span style="font-size: small; font-family: helvetica;">Chiron Gaming is a social platform for video game coaching and training. We help you connect with gaming coaches, training partners, and other players who share the same passion in video games and desire to train together.</span></p>
<p>&nbsp;</p>
<p><span style="font-size: small; font-family: helvetica;">You are the coach, and you are also the student. Chiron Gaming allows anyone to freely sign up and teach at any rates equivalent of their gaming expertise.</span><span style="font-family: helvetica; font-size: small;">&nbsp;Within this community we hope to encourage players to share, train, and grow together to reach whatever goals they've set before them. Whether it's getting out of ELO hell, or making that push to get out of Bronze league, Chiron Gaming can help you get there.</span></p>
</div></div></div>
		<h1 style="font-family: 'Quantico', sans-serif;  color: #013469;">How it works</h1>
		<div class="howWorksText">
            <div style="margin: 15px;">
            <h2 style="font-family: 'Quantico', sans-serif; color: rgb(75, 75, 75);margin: 50px 0 10px 10px;border-bottom: 2px solid rgb(200,200,200);">Overview</h2>
			<img src="../images/step1.png" height="235px" width="235px">
			<img src="../images/step2.png" height="235px" width="235px">
            <img src="../images/step3.png" height="235px" width="235px">
            <img src="../images/step4.png" height="235px" width="235px">
            <h2 style="font-family: 'Quantico', sans-serif; color: rgb(75, 75, 75);margin: 50px 0 11px 10px;border-bottom: 2px solid rgb(200, 200, 200);">How to Teach Players</h2>
                   <div class="howtovideos" style="width: 640px; height: 360px;float: left;"><iframe width="640" height="360" src="http://www.youtube.com/embed/kXbSvEKLJl8?rel=0" frameborder="0" allowfullscreen=""></iframe></div>
                   <div style="
    float: right;
    margin: 10px 130px 10px 0;">
<h3 style="font-family: 'calibri' sans-serif; color: #013469;">&lt; STEPS &gt;</h3></div>
 <div class="clear"></div>


            <h2 style="font-family: 'Quantico', sans-serif; color: rgb(75, 75, 75);margin: 50px 0 10px 10px;border-bottom: 2px solid rgb(200, 200, 200);">Dashboard</h2>
              <div style="
    float: left;
    margin: 10px 0 10px 130px;">
<h3 style="font-family: 'calibri' sans-serif; color: #013469;">&lt; STEPS &gt;</h3></div>     
              <div class="howtovideos" style="
    float: right;
"><iframe width="640px" height="360" src="http://www.youtube.com/embed/kXbSvEKLJl8?rel=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
<div class="clear"></div>

<h2 style="font-family: 'Quantico', sans-serif; color: rgb(75, 75, 75);margin: 50px 0 11px 10px;border-bottom: 2px solid rgb(200,200,200);">How to Find Coaches</h2>
                   <div class="howtovideos" style="width: 640px; height: 360px;float: left;"><iframe width="640" height="360" src="http://www.youtube.com/embed/kXbSvEKLJl8?rel=0" frameborder="0" allowfullscreen=""></iframe></div>
                   <div style="
    float: right;
    margin: 10px 130px 10px 0;">
<h3 style="font-family: 'calibri' sans-serif; color: #013469;">&lt; STEPS &gt;</h3></div>
 <div class="clear"></div><div class="clear"></div>
		</div>
    </div>
		<h2 style="font-family: 'Quantico', sans-serif; color: #013469;">Students &amp; Training Partners</h2>
		<div class="howWorksText"><div style="margin: 10px;"><p><span style="font-size: small; font-family: helvetica;">If you are a player looking for a coach or other training partners Chiron Gaming's rating system allows you to find only the best members of our community to train with. At Chiron Gaming we're committed to one thing above all else: helping you improve your game. Whether that is through our Masterclass sessions (coming soon), our streamlined system to find a coach, or just finding someone in your skill range to practice with, Chiron Gaming will help you reach whatever goals you have.</span></p></div></div>
		<h2 style="font-family: 'Quantico', sans-serif; color:#013469;">Coaches</h2>
		<div class="howWorksText"><div style="margin: 10px;"><p><span style="font-size: small; font-family: helvetica;">Chiron Gaming provides coaches with the thing they want most: Students. With a growing community, Chiron Gaming will offer unprecedented access to a variety of students from all skill levels. We also offer an easy scheduling system to help you plan and manage your lessons efficiently, allowing you to schedule sessions with your students based on your availability. With our payment system, you can expect secure, fast, and accurate transfers of points for all your lessons. Whether you're an established coach looking for more students or a player simply wishing to share gaming expertise, Chiron Gaming has all the tools to help you achieve your goal.</span></p></div></div>
   </div>
    <script language="javascript">
        changePageTitle('How it works');
    </script><div class="clear"></div>
</div>

<div class="push"></div>
</div>	
<div class="footer">
   <div class="footer-inner">
		<div class="footer-right">
			<ul>
				<li><span>© 2012 ChironGaming. All rights reserved.</span></li>
				<li>|</li>
				<li><a href="termsconditions.php">Terms &amp; Conditions</a></li>
				<li>|</li>
				<li><a href="privacypolicy.php">Privacy Policy</a></li>
				
			</ul>
		</div>
      <div class="footer-left">
         <ul style="float: left;">
			
            <li><a href="contactus.php">Contact Us</a></li>
            <li>|</li>
            <!--li><a href="#">Forum</a></li>
            <li>|</li-->
            
            <li><a href="support.php">Support</a></li>
            <li>|</li>
			<li><a href="find_training_partner.php">Find Training Partner</a></li>
            <li>|</li>
            <li><a href="findcoach.php">Find Coach</a></li>
            <li>|</li>
            <li><a href="howitworks.php">How It Works</a></li>
            <li>|</li>
            <li><a href="index.php">Home</a></li>
         </ul>
      </div>
   </div>
</div>	


</div>

<script type="text/javascript" src="jss/bottomJs.js"></script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37366835-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body></html>