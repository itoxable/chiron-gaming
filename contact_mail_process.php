<?php
include ("general_include.php");

$code = trim(strtolower($_POST['contactTxtCaptcha']));
if($code != $_SESSION['captcha'] && $valid ==1){
    $ermsg="Enter security code correctly";
    $valid=0;
    $ret['IsSuccess'] = false;
    $ret['Message'] = $ermsg;
    
    echo json_encode(
        array(
            'flag' =>3,
            'html'	=> "Enter security code correctly"
        )
    );
    
    
}else{

    $ContactSql = "SELECT admin_email FROM ".TABLEPREFIX."_admin WHERE  admin_id=1";
    $Contact_email = $UserManagerObjAjax->GetRecords("Row",$ContactSql);
    $fromName = "Mail Send";
	
    $c_name		= isset($_POST['c_name'])?stripslashes($_REQUEST['c_name']):'';     
    $c_email	= isset($_POST['c_email'])?stripslashes($_REQUEST['c_email']):'';
    $c_subject	= isset($_POST['subject_id'])?stripslashes($_REQUEST['subject_id']):'';
    $c_comment	= isset($_POST['c_comment'])?stripslashes($_REQUEST['c_comment']):'';
    //$newsletter	= isset($_POST['newsletter'])?stripslashes($_REQUEST['newsletter']):'';
    //$newsletter = $newsletter =="Y" ? "Yes" : "No";

    //$admin_email="phpteam4@citytechcorp.com";
    $admin_email=$Contact_email['admin_email'];


    $tpl_file="email_templates/contact_mail.html";
    $tpl_handler=fopen($tpl_file,"r");
    $tpl_message=fread($tpl_handler,filesize($tpl_file));
    fclose($tpl_handler);

    $fromEmail	= $admin_email;
    $to		= $admin_email;

    $url=$site_url;
    $tpl_message=str_replace("[SITEURL]",$url,$tpl_message);
    $tpl_message=str_replace("[HEADING]",'Administrator',$tpl_message);
    $tpl_message=str_replace("[NAME]",$c_name,$tpl_message);
    $tpl_message=str_replace("[EMAIL]",$c_email,$tpl_message);
    $tpl_message=str_replace("[SUBJECT]",$c_subject,$tpl_message);	
    $tpl_message=str_replace("[COMMENTS]",$c_comment,$tpl_message);

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From:'.$admin_email . "\r\n" . 'X-Mailer: PHP/' . phpversion();
    mail($to, 'New Contactus', $tpl_message, $headers);
	
    echo json_encode(
            array(
                    'flag' =>1,
                    'html'	=> "<div style=\"text-align:center; color:#000000;\"><strong>Thank you for contacting us.We will get back to you soon.</strong> </div>"
            )
    );
}
?>
