<?php
	include("general_include.php");
	include "checklogin.php";
	include('transaction_status.class.php');
	
        $rows = isset($_GET['rows'])?$_GET['rows']:9;
	$cols = isset($_GET['cols'])?$_GET['cols']:10;
	$page = isset($_GET['page'])?$_GET['page']:1;
	$sort = isset($_GET['sort'])?$_GET['sort']:-1;
        
        $start = $rows*($page-1);
        /*$sqlCount = "SELECT COUNT(*) FROM ".TABLEPREFIX."_user_transaction ut, ".TABLEPREFIX."_transaction_status ts, WHERE ut.nk_user_id=".$_SESSION['user_id']." AND ut.status = ts.nk_transaction_status_id";
        $count = $UserManagerObjAjax->GetRecords("All",$sqlCount);*/
        
	$sqlQuery = "SELECT 
	ut.nk_transaction_id as id, 
	ut.tx_date, 
	ut.qty_points*rp.qty_points AS transaction_points,
        ut.commission,
	CONCAT(rp.description, '. ', ts.description) AS description,
	ut.transaction_type , 
	ut.method, 
        ts.name, 
        total_before,
	ut.total_after, 
	ut.description AS dscr 
	FROM ".TABLEPREFIX."_user_transaction ut, ".TABLEPREFIX."_transaction_status ts, ".TABLEPREFIX."_rate_points rp WHERE ut.nk_user_id=".$_SESSION['user_id']." AND ut.status = ts.nk_transaction_status_id AND ut.rate_point_id = rp.rate_point_id ORDER BY nk_transaction_id  DESC LIMIT $start, $rows";
	//echo $sqlQuery."\n";
        logToFile("nk_transaction: ".$sqlQuery);
	$transactionList = $UserManagerObjAjax->GetRecords("All",$sqlQuery);				
	
	$noTransactions = count($transactionList);
	
	

?>
<table data-total="<?php echo $noTransactions ?>">
<tbody>
	<?php 
//		$size = (($rows*($page-1))+$rows);
//		if($page>=0 && ($rows*($page-1))<$noTransactions){
                    for ($row=0; $row<$noTransactions; $row++){
                        //print_r($transactionList[$row]);
                        if($transactionList[$row] == null)
                            break;
	?>
			<tr>
                        <?php
                        for ($column=0; $column<$cols; $column++) {
                   
                            if($column == 5){

                                if($transactionList[$row][$column] == TransactionType::WITHDRAW){
                                        $type = "Withdraw";
                                }else if($transactionList[$row][$column] == TransactionType::CREDIT){
                                        $type = "Credit";	
                                }else if($transactionList[$row][$column] == TransactionType::DEBIT){
                                        $type = "Debit";	
                                }else if($transactionList[$row][$column] == TransactionType::CREDIT_REFUND){
                                        $type = "Credit Refund";	
                                }else if($transactionList[$row][$column] == TransactionType::DEBIT_REFUND){
                                        $type = "Debit Refund";	
                                }else{
                                    $type = $transactionList[$row][$column];
                                }	
                                echo "<td>".$type."</td>";
                            }else if($column == 4){
                                if($transactionList[$row][10] == null || $transactionList[$row][10] == ""){
                                    echo "<td>".$transactionList[$row][$column]."</td>";
                                }else
                                   echo "<td>".$transactionList[$row][10]."</td>";
                            }else if($column == 3){
                                
                                if($transactionList[$row][5] == TransactionType::DEBIT || 
                                        $transactionList[$row][5] == TransactionType::DEBIT_REFUND || 
                                        $transactionList[$row][5] == TransactionType::CREDIT_REFUND){
                                    echo "<td><div style='text-align:center'> - </div></td>";
                                }else{
                                    echo "<td>".$transactionList[$row][$column]."%</td>";
                                }
                                
                            }else{
                                echo "<td>". $transactionList[$row][$column] ."</td>";
                                
                            }
                        }
				?>
			</tr>
	<?php } ?>
</tbody>
</table>