<?php /* Smarty version 2.6.16, created on 2013-04-25 16:48:48
         compiled from training_partner_leftpanel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'training_partner_leftpanel.tpl', 140, false),array('modifier', 'in_array', 'training_partner_leftpanel.tpl', 175, false),)), $this); ?>
<?php echo '
<script type="text/javascript" language="javascript">

jQuery(document).ready(function(){
jQuery(\'#user_name\').focus(function(){ if( jQuery(this).val()== \'Keyword\' ) jQuery(this).val(\'\');});
jQuery(\'#user_name\').blur(function(){
 if( jQuery(this).val()== \'\' ) jQuery(this).val(\'Keyword\');
});
jQuery.fn.autosugguest({   
           className: \'ausu-suggest\',
          methodType: \'POST\',
            minChars: 2,
              rtnIDs: true,
            dataFile: \'autopopulate.php?action=populateCity\'
    });
/*right panel filter more/less links */
	var filterThreshold = 5;
	jQuery(\'.listing\').each(function(i){
	if(jQuery(this).find(\'a.selected\').length == 0){
		jQuery(this).find(\'li:gt(\'+(filterThreshold-1)+\')\').hide();
		if(jQuery(this).find(\'li\').length > filterThreshold)
			jQuery(this).after(\'<li class="more"><div class="button" style="float:right;margin: 5px;">+ more</div></li><li style="clear:both; width:100%"></li>\');
		else
			jQuery(this).after(\'\');
	}
	});

	jQuery(\'.more\').each(function(i){
		jQuery(this).toggle(function(){
		jQuery(this).prev().find(\'li:gt(\'+(filterThreshold-1)+\')\').slideDown(\'fast\');
		jQuery(this).html(\'<li class="more"><div class="button" style="float:right;margin: 5px;">- less</div></li><li style="clear:both; width:100%"></li>\');
		},function(){
		jQuery(this).prev().find(\'li:gt(\'+(filterThreshold-1)+\')\').slideUp(\'fast\');
		jQuery(this).html(\'<li class="more"><div class="button" style="float:right;margin: 5px;">+ more</div></li><li style="clear:both; width:100%"></li>\');
		});
	});
	jQuery(\'.refine\').click(function(){
            var propid = jQuery(this).attr(\'propid\');
            if(propid && propid != ""){
                if(jQuery(\'#prop_\'+propid).attr(\'checked\'))
                    jQuery(\'#prop_\'+propid).removeAttr(\'checked\');
                else
                    jQuery(\'#prop_\'+propid).attr(\'checked\',\'true\');
            }else{
                var val = jQuery(this).attr(\'typeId\');
                if(jQuery(\'#language_\'+val).attr(\'checked\'))
                    jQuery(\'#language_\'+val).removeAttr(\'checked\');
                else
                    jQuery(\'#language_\'+val).attr(\'checked\',\'true\');
            }
            frmRefine_POST();
	});
  });	
</script>
<link rel="stylesheet" href="slider/stylesheets/jslider.css" type="text/css">
<link rel="stylesheet" href="slider/stylesheets/jslider.plastic.css" type="text/css">
<script type="text/javascript" src="slider/javascripts/jquery.dependClass.js"></script>
<script type="text/javascript" src="slider/javascripts/jquery.slider-min.js"></script>
<script type=\'text/javascript\'>
function frmRefine_POST()
{
  var data = jQuery(document.frmRefine).serialize();
      
   jQuery(\'#loading\').html(\'<p><img src="images/bar-loader.gif" alt="Loading..." /></p>\');
  jQuery.ajax({
	type: "GET",
	url: \'find_training_partner2.php\',
	data: data+\'&game_id=\'+ jQuery(\'#game_id option:selected\').val(),			
	dataType: \'html\',
	success: function(data)
	{				
	   jQuery(\'.container-inner\').html(data+"<div class=\'clear\'></div>");
	   //alert(data);
		 jQuery(\'#loading\').html();
	}
});
}
function CreateCountry()
{
    var cityid = jQuery(\'#cityid\').val();
	var city = jQuery(\'#city\').val();
	
	if(cityid !=\'\' && city !=\'\')
	{
		 document.getElementById(\'availability_city\').value=cityid;
		 frmRefine_POST();
	}
	else
	{
	     document.getElementById(\'availability_city\').value=\'\';
		 frmRefine_POST();
	}
}
function subForm(a){
//alert(a);
	document.getElementById(\'listfor\').value=\'sorting\';
	document.getElementById(\'sortingby\').value=a;
	frmRefine_POST();
}
function avail_submit(c){
	document.getElementById(\'availability_country\').value=c;
	frmRefine_POST();
}
function searchUser()
{
   var username = jQuery(\'#user_name\').val();
   if(username == \'\' || username ==\'Keyword\')
    jQuery(\'#user_name\').css(\'border\',\'1px solid red\');
   else
   {
     document.getElementById(\'unm\').value=username;
	 frmRefine_POST();
   }	 	
}
function resetSearch()
{
     document.getElementById(\'unm\').value=\'\';
	 frmRefine_POST();
   	 	
}
//function goto(){
//	frmRefine_POST();
//}
 function submitbyenter(event){
	var key;
	key = event.keyCode;
		
	if (key==13){		
		searchUser()		
	}
            return false;
}
</script>
'; ?>

<div class="left-panel">
<div class="left-search">
        <form action="find_training_partner.php" method="post" id="leftsearchfindtrainningpartner">
            <fieldset>
                 <select name="game_id" id="game_id">
                  <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['GameArr'],'selected' => $this->_tpl_vars['game_id']), $this);?>

                  </select>
                  <a class="button" href="#" style="float: right;min-width: 30px;padding: 0 3px; margin: 0;" onclick="document.getElementById('leftsearchfindtrainningpartner').submit();">Go</a> 
                 
            </fieldset>
        </form>
        <form action="find_training_partner.php" method="post" id="" onsubmit="return false">
            <fieldset>
                 <div style="background:#ccc; width:250px; float:left; display:block; top:40px; left:0px; height:37px; border:1px solid #ededed; padding:3px 0 0 5px; position:absolute;">
                      <input name="unm" type="text" id="user_name" <?php if ($this->_tpl_vars['searchusername'] != ''): ?>value="<?php echo $this->_tpl_vars['searchusername']; ?>
"<?php else: ?>value="Keyword"<?php endif; ?> onkeyup="submitbyenter(event);"/>
                      <input type="button" value="" onclick="searchUser()" />
                  </div>
            </fieldset>
        </form>
	   
 </div>
   <div class="graphite demo-container">
	<ul class="accordion" id="accordion-1">
	<form action="find_training_partner.php" method="post" name="frmRefine">
	<input type="hidden" name="list_for" id="listfor" value="" />
	<input type="hidden" name="sorting_by" id="sortingby" value=""/>
	<input type="hidden" name="unm" id="unm" value="<?php echo $this->_tpl_vars['searchusername']; ?>
"/>
	<input type="hidden" name="availability_country" id="availability_country" value="<?php echo $this->_tpl_vars['availability_country']; ?>
">
	<input type="hidden" name="availability_city" id="availability_city" value="<?php echo $this->_tpl_vars['availability_city']; ?>
"/>
	<input type="hidden" name="action" value="send" />
	    
        <?php unset($this->_sections['categoryIndex']);
$this->_sections['categoryIndex']['name'] = 'categoryIndex';
$this->_sections['categoryIndex']['loop'] = is_array($_loop=$this->_tpl_vars['categoriesArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['categoryIndex']['show'] = true;
$this->_sections['categoryIndex']['max'] = $this->_sections['categoryIndex']['loop'];
$this->_sections['categoryIndex']['step'] = 1;
$this->_sections['categoryIndex']['start'] = $this->_sections['categoryIndex']['step'] > 0 ? 0 : $this->_sections['categoryIndex']['loop']-1;
if ($this->_sections['categoryIndex']['show']) {
    $this->_sections['categoryIndex']['total'] = $this->_sections['categoryIndex']['loop'];
    if ($this->_sections['categoryIndex']['total'] == 0)
        $this->_sections['categoryIndex']['show'] = false;
} else
    $this->_sections['categoryIndex']['total'] = 0;
if ($this->_sections['categoryIndex']['show']):

            for ($this->_sections['categoryIndex']['index'] = $this->_sections['categoryIndex']['start'], $this->_sections['categoryIndex']['iteration'] = 1;
                 $this->_sections['categoryIndex']['iteration'] <= $this->_sections['categoryIndex']['total'];
                 $this->_sections['categoryIndex']['index'] += $this->_sections['categoryIndex']['step'], $this->_sections['categoryIndex']['iteration']++):
$this->_sections['categoryIndex']['rownum'] = $this->_sections['categoryIndex']['iteration'];
$this->_sections['categoryIndex']['index_prev'] = $this->_sections['categoryIndex']['index'] - $this->_sections['categoryIndex']['step'];
$this->_sections['categoryIndex']['index_next'] = $this->_sections['categoryIndex']['index'] + $this->_sections['categoryIndex']['step'];
$this->_sections['categoryIndex']['first']      = ($this->_sections['categoryIndex']['iteration'] == 1);
$this->_sections['categoryIndex']['last']       = ($this->_sections['categoryIndex']['iteration'] == $this->_sections['categoryIndex']['total']);
?>
       <li>
           <a href="#"><?php echo $this->_tpl_vars['categoriesArr'][$this->_sections['categoryIndex']['index']]['category_name']; ?>
</a>
           <?php if ($this->_tpl_vars['categoriesArr'][$this->_sections['categoryIndex']['index']]['propsSize'] > 0): ?>
           <ul>	
               <div class="listing">
                   <?php unset($this->_sections['propIndex']);
$this->_sections['propIndex']['name'] = 'propIndex';
$this->_sections['propIndex']['loop'] = is_array($_loop=$this->_tpl_vars['categoriesArr'][$this->_sections['categoryIndex']['index']]['props']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['propIndex']['show'] = true;
$this->_sections['propIndex']['max'] = $this->_sections['propIndex']['loop'];
$this->_sections['propIndex']['step'] = 1;
$this->_sections['propIndex']['start'] = $this->_sections['propIndex']['step'] > 0 ? 0 : $this->_sections['propIndex']['loop']-1;
if ($this->_sections['propIndex']['show']) {
    $this->_sections['propIndex']['total'] = $this->_sections['propIndex']['loop'];
    if ($this->_sections['propIndex']['total'] == 0)
        $this->_sections['propIndex']['show'] = false;
} else
    $this->_sections['propIndex']['total'] = 0;
if ($this->_sections['propIndex']['show']):

            for ($this->_sections['propIndex']['index'] = $this->_sections['propIndex']['start'], $this->_sections['propIndex']['iteration'] = 1;
                 $this->_sections['propIndex']['iteration'] <= $this->_sections['propIndex']['total'];
                 $this->_sections['propIndex']['index'] += $this->_sections['propIndex']['step'], $this->_sections['propIndex']['iteration']++):
$this->_sections['propIndex']['rownum'] = $this->_sections['propIndex']['iteration'];
$this->_sections['propIndex']['index_prev'] = $this->_sections['propIndex']['index'] - $this->_sections['propIndex']['step'];
$this->_sections['propIndex']['index_next'] = $this->_sections['propIndex']['index'] + $this->_sections['propIndex']['step'];
$this->_sections['propIndex']['first']      = ($this->_sections['propIndex']['iteration'] == 1);
$this->_sections['propIndex']['last']       = ($this->_sections['propIndex']['iteration'] == $this->_sections['propIndex']['total']);
?>
                   <li>
                       <input type="checkbox" name="prop_id[]" id='prop_<?php echo $this->_tpl_vars['categoriesArr'][$this->_sections['categoryIndex']['index']]['props'][$this->_sections['propIndex']['index']]['property_id']; ?>
' value="<?php echo $this->_tpl_vars['categoriesArr'][$this->_sections['categoryIndex']['index']]['props'][$this->_sections['propIndex']['index']]['property_id']; ?>
" 
                          style="display:none" <?php if (((is_array($_tmp=$this->_tpl_vars['categoriesArr'][$this->_sections['categoryIndex']['index']]['props'][$this->_sections['propIndex']['index']]['property_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['prop_id']) : in_array($_tmp, $this->_tpl_vars['prop_id']))): ?> checked="checked" <?php endif; ?>/>
                       <a href="javascript:;" class="refine <?php if (((is_array($_tmp=$this->_tpl_vars['categoriesArr'][$this->_sections['categoryIndex']['index']]['props'][$this->_sections['propIndex']['index']]['property_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['prop_id']) : in_array($_tmp, $this->_tpl_vars['prop_id']))): ?>selected<?php endif; ?>" propid="<?php echo $this->_tpl_vars['categoriesArr'][$this->_sections['categoryIndex']['index']]['props'][$this->_sections['propIndex']['index']]['property_id']; ?>
"> 
                           <?php echo $this->_tpl_vars['categoriesArr'][$this->_sections['categoryIndex']['index']]['props'][$this->_sections['propIndex']['index']]['property_name']; ?>

                       </a>
                   </li>
                   <?php endfor; endif; ?> 
               </div>
           </ul>
           <?php endif; ?>
       </li>
       <?php endfor; endif; ?>   
           
	<?php if ($this->_tpl_vars['Numlanguage'] != '0'): ?>	
        <li>
            <a href="#">Language</a>
            <ul>
		<div class="listing">
                <?php unset($this->_sections['lang']);
$this->_sections['lang']['name'] = 'lang';
$this->_sections['lang']['loop'] = is_array($_loop=$this->_tpl_vars['languageArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['lang']['show'] = true;
$this->_sections['lang']['max'] = $this->_sections['lang']['loop'];
$this->_sections['lang']['step'] = 1;
$this->_sections['lang']['start'] = $this->_sections['lang']['step'] > 0 ? 0 : $this->_sections['lang']['loop']-1;
if ($this->_sections['lang']['show']) {
    $this->_sections['lang']['total'] = $this->_sections['lang']['loop'];
    if ($this->_sections['lang']['total'] == 0)
        $this->_sections['lang']['show'] = false;
} else
    $this->_sections['lang']['total'] = 0;
if ($this->_sections['lang']['show']):

            for ($this->_sections['lang']['index'] = $this->_sections['lang']['start'], $this->_sections['lang']['iteration'] = 1;
                 $this->_sections['lang']['iteration'] <= $this->_sections['lang']['total'];
                 $this->_sections['lang']['index'] += $this->_sections['lang']['step'], $this->_sections['lang']['iteration']++):
$this->_sections['lang']['rownum'] = $this->_sections['lang']['iteration'];
$this->_sections['lang']['index_prev'] = $this->_sections['lang']['index'] - $this->_sections['lang']['step'];
$this->_sections['lang']['index_next'] = $this->_sections['lang']['index'] + $this->_sections['lang']['step'];
$this->_sections['lang']['first']      = ($this->_sections['lang']['iteration'] == 1);
$this->_sections['lang']['last']       = ($this->_sections['lang']['iteration'] == $this->_sections['lang']['total']);
?>
                    <li>
                        <input type="checkbox" name="language_id[]" id="language_<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id']; ?>
" value="<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id']; ?>
" style="display:none;" 
                        <?php if (((is_array($_tmp=$this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['language_id']) : in_array($_tmp, $this->_tpl_vars['language_id']))): ?> checked="checked" <?php endif; ?> />				
                        <a href="javascript:;" class="refine <?php if (((is_array($_tmp=$this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['language_id']) : in_array($_tmp, $this->_tpl_vars['language_id']))): ?> selected <?php endif; ?>" stype="Z" typeId="<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id']; ?>
" >
                        <?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_name']; ?>
 (<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['cnt']; ?>
)</a>
                    </li>
                  <?php endfor; endif; ?>
                </div> 
            </ul>
        </li>	
        <?php endif; ?>	
            <li><a href="#">Availability</a>
           <ul>
                <div class="listing">
                    <li> 
                        <input type="checkbox" name="availability[]" id="avail_O" value="O" style="display:none" <?php if (((is_array($_tmp='O')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> checked="checked" <?php endif; ?> />
                        <a href="javascript:;" class="refine <?php if (((is_array($_tmp='O')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> selected <?php endif; ?>" stype="A" typeId="O"> Online(<?php echo $this->_tpl_vars['avail1Num']; ?>
)</a>
                    </li>
                    <li> 
                        <input type="checkbox" name="availability[]" id="avail_L" value="L" style="display:none" <?php if (((is_array($_tmp='L')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> checked="checked" <?php endif; ?> />
                        <a href="javascript:;" class="refine <?php if (((is_array($_tmp='L')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> selected <?php endif; ?>" stype="A" typeId="L" > Local meet-up(<?php echo $this->_tpl_vars['avail2Num']; ?>
)</a>
                    </li>
                    <li id="avail_coun" <?php if (((is_array($_tmp='L')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> style="display:block;" <?php else: ?> style="display:none;" <?php endif; ?>>
                        &nbsp;&nbsp;&nbsp;
                        <div id="coun">
                            <select name="avail_country" id="avail_country" class="drop-down" onchange="avail_submit(this.value)">
                                <option value="">Availability Country</option>
                                <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['AvailcounArr'],'selected' => $this->_tpl_vars['availability_country']), $this);?>

                            </select>
                        </div>
                        <br />
                        <div style="margin-top: 5px;">
                            <div class="ausu-suggest" style="margin: 5px;">
                                <input type="text" style="padding: 3px; width: 190px;" name="city" id="city" value="<?php echo $this->_tpl_vars['city']; ?>
" autocomplete="off"/>
                                <input name="cityid" id="cityid" value="<?php echo $this->_tpl_vars['availability_city']; ?>
" type="hidden"  autocomplete="off" />
                            </div>
                            <div class='clear'></div>
                            <div>
                                <input style="float:right;margin: 5px;margin-right: 55px;" type="button" value="Go" onclick="CreateCountry()" class="button" />
                            </div>
                        </div>
                        <br/>
                     </li>
                </div>  
            </ul>
        </li>   
	   
	  <input type="hidden" name="record_per_page" id="record_per_page" value="<?php echo $this->_tpl_vars['record_per_page']; ?>
" style="visibility:hidden">
	  <input type="submit" style="visibility:hidden">
	 </form> 	
    </ul>
   </div>   
   </div>