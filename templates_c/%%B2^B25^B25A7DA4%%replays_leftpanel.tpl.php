<?php /* Smarty version 2.6.16, created on 2013-01-18 20:15:57
         compiled from replays_leftpanel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'replays_leftpanel.tpl', 199, false),array('modifier', 'sizeof', 'replays_leftpanel.tpl', 226, false),array('modifier', 'in_array', 'replays_leftpanel.tpl', 226, false),)), $this); ?>
<?php echo '
<script type="text/javascript" language="javascript">

jQuery(document).ready(function(){
jQuery(\'#vnm\').focus(function(){ if( jQuery(this).val()== \'Keyword\' ) jQuery(this).val(\'\');});
jQuery(\'#vnm\').blur(function(){ if( jQuery(this).val()== \'\' ) jQuery(this).val(\'Keyword\');});


/* right panel filter more/less links */
	var filterThreshold = 5;
	jQuery(\'.listing\').each(function(i){
	if(jQuery(this).find(\'a.selected\').length == 0){
		jQuery(this).find(\'li:gt(\'+(filterThreshold-1)+\')\').hide();
		if(jQuery(this).find(\'li\').length > filterThreshold)
			jQuery(this).after(\'<li class="more"><div class="button" style="float:right;margin: 5px;">+ more</div></li><li style="clear:both; width:100%"></li>\');
		else
			jQuery(this).after(\'\');
	}
	});

	jQuery(\'.more\').each(function(i){
		jQuery(this).toggle(function(){
		jQuery(this).prev().find(\'li:gt(\'+(filterThreshold-1)+\')\').slideDown(\'fast\');
		jQuery(this).html(\'<li class="more"><div class="button" style="float:right;margin: 5px;">- less</div></li><li style="clear:both; width:100%"></li>\');
		},function(){
		jQuery(this).prev().find(\'li:gt(\'+(filterThreshold-1)+\')\').slideUp(\'fast\');
		jQuery(this).html(\'<li class="more"><div class="button" style="float:right;margin: 5px;">+ more</div></li><li style="clear:both; width:100%"></li>\');
		});
	});
	jQuery(\'.refine\').click(function(){
	
		var val = jQuery(this).attr(\'typeId\');
		var type = jQuery(this).attr(\'stype\');
		
		if(type ==\'L\')
		{
			
			if(jQuery(\'#ladder_\'+val).attr(\'checked\'))
			jQuery(\'#ladder_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#ladder_\'+val).attr(\'checked\',\'true\');
		}
		else if(type ==\'CH\')
		{
			if(jQuery(\'#champion_\'+val).attr(\'checked\'))
			jQuery(\'#champion_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#champion_\'+val).attr(\'checked\',\'true\');
		}	
		else if(type ==\'CL\')
		{
			if(jQuery(\'#class_\'+val).attr(\'checked\'))
			jQuery(\'#class_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#class_\'+val).attr(\'checked\',\'true\');
		}	
		else if(type ==\'MP\')
		{
			if(jQuery(\'#map_\'+val).attr(\'checked\'))
			jQuery(\'#map_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#map_\'+val).attr(\'checked\',\'true\');
		}	
		else if(type ==\'MD\')
		{
			if(jQuery(\'#mode_\'+val).attr(\'checked\'))
			jQuery(\'#mode_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#mode_\'+val).attr(\'checked\',\'true\');
		}	
		else if(type ==\'TM\')
		{
			if(jQuery(\'#team_\'+val).attr(\'checked\'))
			jQuery(\'#team_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#team_\'+val).attr(\'checked\',\'true\');
		}	
		else if(type ==\'TP\')
		{
			if(jQuery(\'#type_\'+val).attr(\'checked\'))
			jQuery(\'#type_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#type_\'+val).attr(\'checked\',\'true\');
		}	
		else if(type ==\'V\')
		{
			if(jQuery(\'#versus_\'+val).attr(\'checked\'))
			jQuery(\'#versus_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#versus_\'+val).attr(\'checked\',\'true\');
		}	
		else if(type ==\'R\')
		{
			if(jQuery(\'#race_\'+val).attr(\'checked\'))
			jQuery(\'#race_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#race_\'+val).attr(\'checked\',\'true\');
		}	
		else if(type ==\'S\')
		{
			if(jQuery(\'#server_\'+val).attr(\'checked\'))
			jQuery(\'#server_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#server_\'+val).attr(\'checked\',\'true\');
		}
		else if(type ==\'X\')
		{
			if(jQuery(\'#region_\'+val).attr(\'checked\'))
			jQuery(\'#region_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#region_\'+val).attr(\'checked\',\'true\');
		}
		else if(type ==\'Y\')
		{
			if(jQuery(\'#rating_\'+val).attr(\'checked\'))
			jQuery(\'#rating_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#rating_\'+val).attr(\'checked\',\'true\');
		}		
		else if(type ==\'A\')
		{
			if(jQuery(\'#avail_\'+val).attr(\'checked\'))
			jQuery(\'#avail_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#avail_\'+val).attr(\'checked\',\'true\');
		}
		else
		{
			if(jQuery(\'#language_\'+val).attr(\'checked\'))
			jQuery(\'#language_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#language_\'+val).attr(\'checked\',\'true\');
		}
		
		//document.frmRefine.submit();
		frmRefine_POST();
	});
  });	
</script>
<link rel="stylesheet" href="slider/stylesheets/jslider.css" type="text/css">
<link rel="stylesheet" href="slider/stylesheets/jslider.plastic.css" type="text/css">
<script type="text/javascript" src="slider/javascripts/jquery.dependClass.js"></script>
<script type="text/javascript" src="slider/javascripts/jquery.slider-min.js"></script>
<script type=\'text/javascript\'>
function frmRefine_POST()
{
  var data = jQuery(document.frmRefine).serialize();
  jQuery(\'#loading\').html(\'<p><img src="images/bar-loader.gif" alt="Loading..." /></p>\');
  //alert(data);
  jQuery.ajax({
	type: "GET",
	url: \'replays2.php\',
	data: data,			
	dataType: \'html\',
	success: function(data)
	{				
	   
	   jQuery(\'.container-inner\').html(data + "<div class=\'clear\'></div>");
	   //alert(data);
	   jQuery(\'#loading\').html();
	}
});
}
 
function subForm(a){
//alert(a);
	document.getElementById(\'listfor\').value=\'sorting\';
	document.getElementById(\'sortingby\').value=a;
	frmRefine_POST();
}
function searchVdo()
{
   var vnm = jQuery(\'#vnm\').val();
   if(vnm == \'\' || vnm ==\'Keyword\')
    jQuery(\'#vnm\').css(\'border\',\'1px solid red\');
   else
   {
     document.getElementById(\'vtitle\').value=vnm;
	 frmRefine_POST();
   }	 	
}
function resetSearch()
{
     document.getElementById(\'vtitle\').value=\'\';
	 frmRefine_POST();
   	 	
}

</script>
'; ?>

<?php if ($this->_tpl_vars['FromPage'] == 'videodetails'): ?>
<div class="container-inner">
<?php endif; ?>
<div class="left-panel">
<div class="left-search">
       <form action="replays.php" method="post" name="frmRefine" id="leftsearchfindvideo">
       <fieldset>
       <select name="game_id" id="game_id">
	  <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['GameArr'],'selected' => $this->_tpl_vars['game_id']), $this);?>

	</select>

        <a class="button" href="#" style="float: right;min-width: 30px;padding: 0 3px; margin: 0;" onclick="document.getElementById('leftsearchfindvideo').submit();">Go</a>
       <div style="background:#ccc; width:250px; float:left; display:block; top:40px; left:0px; height:37px; border:1px solid #ededed; padding:3px 0 0 5px; position:absolute;">
           <input name="vnm" class="text" type="text" id="vnm" <?php if ($this->_tpl_vars['vdotitle'] != ''): ?>value="<?php echo $this->_tpl_vars['vdotitle']; ?>
"<?php else: ?>value="Keyword"<?php endif; ?> />
           <input type="button" value="" onclick="searchVdo()" />
        </div>
       </fieldset>
       </form>
 </div>
   <div class="graphite demo-container">
	<ul class="accordion" id="accordion-1">
	<form action="replays.php" method="post" name="frmRefine">
	<input type="hidden" name="vtitle" id="vtitle" value="<?php echo $this->_tpl_vars['vdotitle']; ?>
"/>

	<input type="hidden" name="list_for" id="listfor" value="" />
	   <input type="hidden" name="sorting_by" id="sortingby" value=""/>
	<input type="hidden" name="action" value="send" />
	<?php if ($this->_tpl_vars['is_ladder'] == 'Y'): ?>
	 <?php if ($this->_tpl_vars['Numladder'] != '0'): ?>
	    <li><a href="#">Ladder</a>
		<ul>	
		 <div class="listing">
		 <?php unset($this->_sections['ladder']);
$this->_sections['ladder']['name'] = 'ladder';
$this->_sections['ladder']['loop'] = is_array($_loop=$this->_tpl_vars['LadderArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['ladder']['show'] = true;
$this->_sections['ladder']['max'] = $this->_sections['ladder']['loop'];
$this->_sections['ladder']['step'] = 1;
$this->_sections['ladder']['start'] = $this->_sections['ladder']['step'] > 0 ? 0 : $this->_sections['ladder']['loop']-1;
if ($this->_sections['ladder']['show']) {
    $this->_sections['ladder']['total'] = $this->_sections['ladder']['loop'];
    if ($this->_sections['ladder']['total'] == 0)
        $this->_sections['ladder']['show'] = false;
} else
    $this->_sections['ladder']['total'] = 0;
if ($this->_sections['ladder']['show']):

            for ($this->_sections['ladder']['index'] = $this->_sections['ladder']['start'], $this->_sections['ladder']['iteration'] = 1;
                 $this->_sections['ladder']['iteration'] <= $this->_sections['ladder']['total'];
                 $this->_sections['ladder']['index'] += $this->_sections['ladder']['step'], $this->_sections['ladder']['iteration']++):
$this->_sections['ladder']['rownum'] = $this->_sections['ladder']['iteration'];
$this->_sections['ladder']['index_prev'] = $this->_sections['ladder']['index'] - $this->_sections['ladder']['step'];
$this->_sections['ladder']['index_next'] = $this->_sections['ladder']['index'] + $this->_sections['ladder']['step'];
$this->_sections['ladder']['first']      = ($this->_sections['ladder']['iteration'] == 1);
$this->_sections['ladder']['last']       = ($this->_sections['ladder']['iteration'] == $this->_sections['ladder']['total']);
?>
			<li>
			<input type="checkbox" name="ladder_id[]" id='ladder_<?php echo $this->_tpl_vars['LadderArr'][$this->_sections['ladder']['index']]['ladder_id']; ?>
' value="<?php echo $this->_tpl_vars['LadderArr'][$this->_sections['ladder']['index']]['ladder_id']; ?>
" style="display:none;"
			<?php if (sizeof($this->_tpl_vars['ladder_id']) != 0 && ((is_array($_tmp=$this->_tpl_vars['LadderArr'][$this->_sections['ladder']['index']]['ladder_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['ladder_id']) : in_array($_tmp, $this->_tpl_vars['ladder_id']))): ?> checked="checked" <?php endif; ?>/>
			<a href="#" class="refine <?php if (sizeof($this->_tpl_vars['ladder_id']) != 0 && ((is_array($_tmp=$this->_tpl_vars['LadderArr'][$this->_sections['ladder']['index']]['ladder_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['ladder_id']) : in_array($_tmp, $this->_tpl_vars['ladder_id']))): ?> selected <?php endif; ?>" stype="L" typeId="<?php echo $this->_tpl_vars['LadderArr'][$this->_sections['ladder']['index']]['ladder_id']; ?>
"> 
			<?php echo $this->_tpl_vars['LadderArr'][$this->_sections['ladder']['index']]['ladder_name']; ?>
 (<?php echo $this->_tpl_vars['LadderArr'][$this->_sections['ladder']['index']]['countrow']; ?>
) </a>
			</li>
                 <?php endfor; endif; ?>
				 
		 </div>		 
		 </ul>
	   </li>	 
	 <?php endif; ?>	
	<?php endif; ?> 
	<?php if ($this->_tpl_vars['is_race'] == 'Y'): ?>
	 <?php if ($this->_tpl_vars['Numrace'] != '0'): ?>
	    <li><a href="#">Race</a>
		<ul>	
		 <div class="listing">
			 <?php unset($this->_sections['race']);
$this->_sections['race']['name'] = 'race';
$this->_sections['race']['loop'] = is_array($_loop=$this->_tpl_vars['RaceArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['race']['show'] = true;
$this->_sections['race']['max'] = $this->_sections['race']['loop'];
$this->_sections['race']['step'] = 1;
$this->_sections['race']['start'] = $this->_sections['race']['step'] > 0 ? 0 : $this->_sections['race']['loop']-1;
if ($this->_sections['race']['show']) {
    $this->_sections['race']['total'] = $this->_sections['race']['loop'];
    if ($this->_sections['race']['total'] == 0)
        $this->_sections['race']['show'] = false;
} else
    $this->_sections['race']['total'] = 0;
if ($this->_sections['race']['show']):

            for ($this->_sections['race']['index'] = $this->_sections['race']['start'], $this->_sections['race']['iteration'] = 1;
                 $this->_sections['race']['iteration'] <= $this->_sections['race']['total'];
                 $this->_sections['race']['index'] += $this->_sections['race']['step'], $this->_sections['race']['iteration']++):
$this->_sections['race']['rownum'] = $this->_sections['race']['iteration'];
$this->_sections['race']['index_prev'] = $this->_sections['race']['index'] - $this->_sections['race']['step'];
$this->_sections['race']['index_next'] = $this->_sections['race']['index'] + $this->_sections['race']['step'];
$this->_sections['race']['first']      = ($this->_sections['race']['iteration'] == 1);
$this->_sections['race']['last']       = ($this->_sections['race']['iteration'] == $this->_sections['race']['total']);
?>
				   	<li>
					<input type="checkbox" name="race_id[]" id='race_<?php echo $this->_tpl_vars['RaceArr'][$this->_sections['race']['index']]['race_id']; ?>
' value="<?php echo $this->_tpl_vars['RaceArr'][$this->_sections['race']['index']]['race_id']; ?>
" style="display:none" 
					<?php if (sizeof($this->_tpl_vars['race_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['RaceArr'][$this->_sections['race']['index']]['race_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['race_id']) : in_array($_tmp, $this->_tpl_vars['race_id']))): ?> checked="checked" <?php endif; ?>/>
					<a href="#" class="refine <?php if (sizeof($this->_tpl_vars['race_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['RaceArr'][$this->_sections['race']['index']]['race_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['race_id']) : in_array($_tmp, $this->_tpl_vars['race_id']))): ?> selected <?php endif; ?>" stype="R" typeId="<?php echo $this->_tpl_vars['RaceArr'][$this->_sections['race']['index']]['race_id']; ?>
"> 
					<?php echo $this->_tpl_vars['RaceArr'][$this->_sections['race']['index']]['race_title']; ?>
 (<?php echo $this->_tpl_vars['RaceArr'][$this->_sections['race']['index']]['countrow']; ?>
) </a> </li>
			<?php endfor; endif; ?>
				 
		 </div>		 
	      </ul>
	   </li>	 
	 <?php endif; ?>	
	<?php endif; ?>
	<!-- Chandan Added this Starts-->
	<?php if ($this->_tpl_vars['is_champion'] == 'Y'): ?>
	 <?php if ($this->_tpl_vars['NumChampion'] != '0'): ?>
	    <li><a href="#">Champion</a>
		<ul>	
		 <div class="listing">
				 <?php unset($this->_sections['champion']);
$this->_sections['champion']['name'] = 'champion';
$this->_sections['champion']['loop'] = is_array($_loop=$this->_tpl_vars['ChampionArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['champion']['show'] = true;
$this->_sections['champion']['max'] = $this->_sections['champion']['loop'];
$this->_sections['champion']['step'] = 1;
$this->_sections['champion']['start'] = $this->_sections['champion']['step'] > 0 ? 0 : $this->_sections['champion']['loop']-1;
if ($this->_sections['champion']['show']) {
    $this->_sections['champion']['total'] = $this->_sections['champion']['loop'];
    if ($this->_sections['champion']['total'] == 0)
        $this->_sections['champion']['show'] = false;
} else
    $this->_sections['champion']['total'] = 0;
if ($this->_sections['champion']['show']):

            for ($this->_sections['champion']['index'] = $this->_sections['champion']['start'], $this->_sections['champion']['iteration'] = 1;
                 $this->_sections['champion']['iteration'] <= $this->_sections['champion']['total'];
                 $this->_sections['champion']['index'] += $this->_sections['champion']['step'], $this->_sections['champion']['iteration']++):
$this->_sections['champion']['rownum'] = $this->_sections['champion']['iteration'];
$this->_sections['champion']['index_prev'] = $this->_sections['champion']['index'] - $this->_sections['champion']['step'];
$this->_sections['champion']['index_next'] = $this->_sections['champion']['index'] + $this->_sections['champion']['step'];
$this->_sections['champion']['first']      = ($this->_sections['champion']['iteration'] == 1);
$this->_sections['champion']['last']       = ($this->_sections['champion']['iteration'] == $this->_sections['champion']['total']);
?>
				   	<li>
					<input type="checkbox" name="champion_id[]" id='champion_<?php echo $this->_tpl_vars['ChampionArr'][$this->_sections['champion']['index']]['champion_id']; ?>
' value="<?php echo $this->_tpl_vars['ChampionArr'][$this->_sections['champion']['index']]['champion_id']; ?>
" style="display:none" 
					<?php if (sizeof($this->_tpl_vars['champion_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['ChampionArr'][$this->_sections['champion']['index']]['champion_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['champion_id']) : in_array($_tmp, $this->_tpl_vars['champion_id']))): ?> checked="checked" <?php endif; ?>/>
					<a href="#" class="refine <?php if (sizeof($this->_tpl_vars['champion_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['ChampionArr'][$this->_sections['champion']['index']]['champion_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['champion_id']) : in_array($_tmp, $this->_tpl_vars['champion_id']))): ?> selected <?php endif; ?>" stype="CH" typeId="<?php echo $this->_tpl_vars['ChampionArr'][$this->_sections['champion']['index']]['champion_id']; ?>
"> 
					<?php echo $this->_tpl_vars['ChampionArr'][$this->_sections['champion']['index']]['champion_title']; ?>
 (<?php echo $this->_tpl_vars['ChampionArr'][$this->_sections['champion']['index']]['countrow']; ?>
) </a> </li>
                 <?php endfor; endif; ?>
				 
		 </div>		 
	      </ul>
	   </li>	 
	 <?php endif; ?>	
	<?php endif; ?>
        <!-- Chandan Added This Ends-->
	
	<!-- Chandan Added this Starts-->
	<?php if ($this->_tpl_vars['is_class'] == 'Y'): ?>
	 <?php if ($this->_tpl_vars['NumClass'] != '0'): ?>
	    <li><a href="#">Class</a>
		<ul>	
		 <div class="listing">
				 <?php unset($this->_sections['class']);
$this->_sections['class']['name'] = 'class';
$this->_sections['class']['loop'] = is_array($_loop=$this->_tpl_vars['ClassArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['class']['show'] = true;
$this->_sections['class']['max'] = $this->_sections['class']['loop'];
$this->_sections['class']['step'] = 1;
$this->_sections['class']['start'] = $this->_sections['class']['step'] > 0 ? 0 : $this->_sections['class']['loop']-1;
if ($this->_sections['class']['show']) {
    $this->_sections['class']['total'] = $this->_sections['class']['loop'];
    if ($this->_sections['class']['total'] == 0)
        $this->_sections['class']['show'] = false;
} else
    $this->_sections['class']['total'] = 0;
if ($this->_sections['class']['show']):

            for ($this->_sections['class']['index'] = $this->_sections['class']['start'], $this->_sections['class']['iteration'] = 1;
                 $this->_sections['class']['iteration'] <= $this->_sections['class']['total'];
                 $this->_sections['class']['index'] += $this->_sections['class']['step'], $this->_sections['class']['iteration']++):
$this->_sections['class']['rownum'] = $this->_sections['class']['iteration'];
$this->_sections['class']['index_prev'] = $this->_sections['class']['index'] - $this->_sections['class']['step'];
$this->_sections['class']['index_next'] = $this->_sections['class']['index'] + $this->_sections['class']['step'];
$this->_sections['class']['first']      = ($this->_sections['class']['iteration'] == 1);
$this->_sections['class']['last']       = ($this->_sections['class']['iteration'] == $this->_sections['class']['total']);
?>
				   	<li>
					<input type="checkbox" name="class_id[]" id='class_<?php echo $this->_tpl_vars['ClassArr'][$this->_sections['class']['index']]['class_id']; ?>
' value="<?php echo $this->_tpl_vars['ClassArr'][$this->_sections['class']['index']]['class_id']; ?>
" style="display:none" 
					<?php if (sizeof($this->_tpl_vars['class_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['ClassArr'][$this->_sections['class']['index']]['class_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['class_id']) : in_array($_tmp, $this->_tpl_vars['class_id']))): ?> checked="checked" <?php endif; ?>/>
					<a href="#" class="refine <?php if (sizeof($this->_tpl_vars['class_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['ClassArr'][$this->_sections['class']['index']]['class_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['class_id']) : in_array($_tmp, $this->_tpl_vars['class_id']))): ?> selected <?php endif; ?>" stype="CL" typeId="<?php echo $this->_tpl_vars['ClassArr'][$this->_sections['class']['index']]['class_id']; ?>
"> 
					<?php echo $this->_tpl_vars['ClassArr'][$this->_sections['class']['index']]['class_title']; ?>
 (<?php echo $this->_tpl_vars['ClassArr'][$this->_sections['class']['index']]['countrow']; ?>
) </a> </li>
                 <?php endfor; endif; ?>
				 
		 </div>		 
	      </ul>
	   </li>	 
	 <?php endif; ?>	
	<?php endif; ?>
        <!-- Chandan Added This Ends-->
	<!-- Chandan Added this Starts-->
	<?php if ($this->_tpl_vars['is_map'] == 'Y'): ?>
	 <?php if ($this->_tpl_vars['NumMap'] != '0'): ?>
	    <li><a href="#">Map</a>
		<ul>	
		 <div class="listing">
				 <?php unset($this->_sections['map']);
$this->_sections['map']['name'] = 'map';
$this->_sections['map']['loop'] = is_array($_loop=$this->_tpl_vars['MapArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['map']['show'] = true;
$this->_sections['map']['max'] = $this->_sections['map']['loop'];
$this->_sections['map']['step'] = 1;
$this->_sections['map']['start'] = $this->_sections['map']['step'] > 0 ? 0 : $this->_sections['map']['loop']-1;
if ($this->_sections['map']['show']) {
    $this->_sections['map']['total'] = $this->_sections['map']['loop'];
    if ($this->_sections['map']['total'] == 0)
        $this->_sections['map']['show'] = false;
} else
    $this->_sections['map']['total'] = 0;
if ($this->_sections['map']['show']):

            for ($this->_sections['map']['index'] = $this->_sections['map']['start'], $this->_sections['map']['iteration'] = 1;
                 $this->_sections['map']['iteration'] <= $this->_sections['map']['total'];
                 $this->_sections['map']['index'] += $this->_sections['map']['step'], $this->_sections['map']['iteration']++):
$this->_sections['map']['rownum'] = $this->_sections['map']['iteration'];
$this->_sections['map']['index_prev'] = $this->_sections['map']['index'] - $this->_sections['map']['step'];
$this->_sections['map']['index_next'] = $this->_sections['map']['index'] + $this->_sections['map']['step'];
$this->_sections['map']['first']      = ($this->_sections['map']['iteration'] == 1);
$this->_sections['map']['last']       = ($this->_sections['map']['iteration'] == $this->_sections['map']['total']);
?>
				   	<li>
					<input type="checkbox" name="map_id[]" id='map_<?php echo $this->_tpl_vars['MapArr'][$this->_sections['map']['index']]['map_id']; ?>
' value="<?php echo $this->_tpl_vars['MapArr'][$this->_sections['map']['index']]['map_id']; ?>
" style="display:none" 
					<?php if (sizeof($this->_tpl_vars['map_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['MapArr'][$this->_sections['map']['index']]['map_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['map_id']) : in_array($_tmp, $this->_tpl_vars['map_id']))): ?> checked="checked" <?php endif; ?>/>
					<a href="#" class="refine <?php if (sizeof($this->_tpl_vars['map_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['MapArr'][$this->_sections['map']['index']]['map_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['map_id']) : in_array($_tmp, $this->_tpl_vars['map_id']))): ?> selected <?php endif; ?>" stype="MP" typeId="<?php echo $this->_tpl_vars['MapArr'][$this->_sections['map']['index']]['map_id']; ?>
"> 
					<?php echo $this->_tpl_vars['MapArr'][$this->_sections['map']['index']]['map_title']; ?>
 (<?php echo $this->_tpl_vars['MapArr'][$this->_sections['map']['index']]['countrow']; ?>
) </a> </li>
                 <?php endfor; endif; ?>
				 
		 </div>		 
	      </ul>
	   </li>	 
	 <?php endif; ?>	
	<?php endif; ?>
        <!-- Chandan Added This Ends-->
	<!-- Chandan Added this Starts-->
	<?php if ($this->_tpl_vars['is_mode'] == 'Y'): ?>
	 <?php if ($this->_tpl_vars['NumMode'] != '0'): ?>
	    <li><a href="#">Mode</a>
		<ul>	
		 <div class="listing">
				 <?php unset($this->_sections['mode']);
$this->_sections['mode']['name'] = 'mode';
$this->_sections['mode']['loop'] = is_array($_loop=$this->_tpl_vars['ModeArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['mode']['show'] = true;
$this->_sections['mode']['max'] = $this->_sections['mode']['loop'];
$this->_sections['mode']['step'] = 1;
$this->_sections['mode']['start'] = $this->_sections['mode']['step'] > 0 ? 0 : $this->_sections['mode']['loop']-1;
if ($this->_sections['mode']['show']) {
    $this->_sections['mode']['total'] = $this->_sections['mode']['loop'];
    if ($this->_sections['mode']['total'] == 0)
        $this->_sections['mode']['show'] = false;
} else
    $this->_sections['mode']['total'] = 0;
if ($this->_sections['mode']['show']):

            for ($this->_sections['mode']['index'] = $this->_sections['mode']['start'], $this->_sections['mode']['iteration'] = 1;
                 $this->_sections['mode']['iteration'] <= $this->_sections['mode']['total'];
                 $this->_sections['mode']['index'] += $this->_sections['mode']['step'], $this->_sections['mode']['iteration']++):
$this->_sections['mode']['rownum'] = $this->_sections['mode']['iteration'];
$this->_sections['mode']['index_prev'] = $this->_sections['mode']['index'] - $this->_sections['mode']['step'];
$this->_sections['mode']['index_next'] = $this->_sections['mode']['index'] + $this->_sections['mode']['step'];
$this->_sections['mode']['first']      = ($this->_sections['mode']['iteration'] == 1);
$this->_sections['mode']['last']       = ($this->_sections['mode']['iteration'] == $this->_sections['mode']['total']);
?>
				   	<li>
					<input type="checkbox" name="mode_id[]" id='mode_<?php echo $this->_tpl_vars['ModeArr'][$this->_sections['mode']['index']]['mode_id']; ?>
' value="<?php echo $this->_tpl_vars['ModeArr'][$this->_sections['mode']['index']]['mode_id']; ?>
" style="display:none" 
					<?php if (sizeof($this->_tpl_vars['mode_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['ModeArr'][$this->_sections['mode']['index']]['mode_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['mode_id']) : in_array($_tmp, $this->_tpl_vars['mode_id']))): ?> checked="checked" <?php endif; ?>/>
					<a href="#" class="refine <?php if (sizeof($this->_tpl_vars['mode_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['ModeArr'][$this->_sections['mode']['index']]['mode_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['mode_id']) : in_array($_tmp, $this->_tpl_vars['mode_id']))): ?> selected <?php endif; ?>" stype="MD" typeId="<?php echo $this->_tpl_vars['ModeArr'][$this->_sections['mode']['index']]['mode_id']; ?>
"> 
					<?php echo $this->_tpl_vars['ModeArr'][$this->_sections['mode']['index']]['mode_title']; ?>
 (<?php echo $this->_tpl_vars['ModeArr'][$this->_sections['mode']['index']]['countrow']; ?>
) </a> </li>
				<?php endfor; endif; ?>
				 
		 </div>		 
	      </ul>
	   </li>	 
	 <?php endif; ?>	
	<?php endif; ?>
        <!-- Chandan Added This Ends-->
	<!-- Chandan Added this Starts-->
	<?php if ($this->_tpl_vars['is_team'] == 'Y'): ?>
	 <?php if ($this->_tpl_vars['NumTeam'] != '0'): ?>
	    <li><a href="#">Team</a>
		<ul>	
		 <div class="listing">
				 <?php unset($this->_sections['team']);
$this->_sections['team']['name'] = 'team';
$this->_sections['team']['loop'] = is_array($_loop=$this->_tpl_vars['TeamArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['team']['show'] = true;
$this->_sections['team']['max'] = $this->_sections['team']['loop'];
$this->_sections['team']['step'] = 1;
$this->_sections['team']['start'] = $this->_sections['team']['step'] > 0 ? 0 : $this->_sections['team']['loop']-1;
if ($this->_sections['team']['show']) {
    $this->_sections['team']['total'] = $this->_sections['team']['loop'];
    if ($this->_sections['team']['total'] == 0)
        $this->_sections['team']['show'] = false;
} else
    $this->_sections['team']['total'] = 0;
if ($this->_sections['team']['show']):

            for ($this->_sections['team']['index'] = $this->_sections['team']['start'], $this->_sections['team']['iteration'] = 1;
                 $this->_sections['team']['iteration'] <= $this->_sections['team']['total'];
                 $this->_sections['team']['index'] += $this->_sections['team']['step'], $this->_sections['team']['iteration']++):
$this->_sections['team']['rownum'] = $this->_sections['team']['iteration'];
$this->_sections['team']['index_prev'] = $this->_sections['team']['index'] - $this->_sections['team']['step'];
$this->_sections['team']['index_next'] = $this->_sections['team']['index'] + $this->_sections['team']['step'];
$this->_sections['team']['first']      = ($this->_sections['team']['iteration'] == 1);
$this->_sections['team']['last']       = ($this->_sections['team']['iteration'] == $this->_sections['team']['total']);
?>
				   	<li>
					<input type="checkbox" name="team_id[]" id='team_<?php echo $this->_tpl_vars['TeamArr'][$this->_sections['team']['index']]['team_id']; ?>
' value="<?php echo $this->_tpl_vars['TeamArr'][$this->_sections['team']['index']]['team_id']; ?>
" style="display:none" 
					<?php if (sizeof($this->_tpl_vars['team_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['TeamArr'][$this->_sections['team']['index']]['team_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['team_id']) : in_array($_tmp, $this->_tpl_vars['team_id']))): ?> checked="checked" <?php endif; ?>/>
					<a href="#" class="refine <?php if (sizeof($this->_tpl_vars['team_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['TeamArr'][$this->_sections['team']['index']]['team_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['team_id']) : in_array($_tmp, $this->_tpl_vars['team_id']))): ?> selected <?php endif; ?>" stype="TM" typeId="<?php echo $this->_tpl_vars['TeamArr'][$this->_sections['team']['index']]['team_id']; ?>
"> 
					<?php echo $this->_tpl_vars['TeamArr'][$this->_sections['team']['index']]['team_title']; ?>
 (<?php echo $this->_tpl_vars['TeamArr'][$this->_sections['team']['index']]['countrow']; ?>
) </a> </li>
				 <?php endfor; endif; ?>
				 
		 </div>		 
	      </ul>
	   </li>	 
	 <?php endif; ?>	
	<?php endif; ?>
        <!-- Chandan Added This Ends-->
	<!-- Chandan Added this Starts-->
	<?php if ($this->_tpl_vars['is_type'] == 'Y'): ?>
	 <?php if ($this->_tpl_vars['NumType'] != '0'): ?>
	    <li><a href="#">Type</a>
		<ul>	
		 <div class="listing">
				 <?php unset($this->_sections['type']);
$this->_sections['type']['name'] = 'type';
$this->_sections['type']['loop'] = is_array($_loop=$this->_tpl_vars['TypeArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['type']['show'] = true;
$this->_sections['type']['max'] = $this->_sections['type']['loop'];
$this->_sections['type']['step'] = 1;
$this->_sections['type']['start'] = $this->_sections['type']['step'] > 0 ? 0 : $this->_sections['type']['loop']-1;
if ($this->_sections['type']['show']) {
    $this->_sections['type']['total'] = $this->_sections['type']['loop'];
    if ($this->_sections['type']['total'] == 0)
        $this->_sections['type']['show'] = false;
} else
    $this->_sections['type']['total'] = 0;
if ($this->_sections['type']['show']):

            for ($this->_sections['type']['index'] = $this->_sections['type']['start'], $this->_sections['type']['iteration'] = 1;
                 $this->_sections['type']['iteration'] <= $this->_sections['type']['total'];
                 $this->_sections['type']['index'] += $this->_sections['type']['step'], $this->_sections['type']['iteration']++):
$this->_sections['type']['rownum'] = $this->_sections['type']['iteration'];
$this->_sections['type']['index_prev'] = $this->_sections['type']['index'] - $this->_sections['type']['step'];
$this->_sections['type']['index_next'] = $this->_sections['type']['index'] + $this->_sections['type']['step'];
$this->_sections['type']['first']      = ($this->_sections['type']['iteration'] == 1);
$this->_sections['type']['last']       = ($this->_sections['type']['iteration'] == $this->_sections['type']['total']);
?>
				   	<li>
					<input type="checkbox" name="type_id[]" id='type_<?php echo $this->_tpl_vars['TypeArr'][$this->_sections['type']['index']]['type_id']; ?>
' value="<?php echo $this->_tpl_vars['TypeArr'][$this->_sections['type']['index']]['type_id']; ?>
" style="display:none" 
					<?php if (sizeof($this->_tpl_vars['type_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['TypeArr'][$this->_sections['type']['index']]['type_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['type_id']) : in_array($_tmp, $this->_tpl_vars['type_id']))): ?> checked="checked" <?php endif; ?>/>
					<a href="#" class="refine <?php if (sizeof($this->_tpl_vars['type_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['TypeArr'][$this->_sections['type']['index']]['type_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['type_id']) : in_array($_tmp, $this->_tpl_vars['type_id']))): ?> selected <?php endif; ?>" stype="TP" 
					typeId="<?php echo $this->_tpl_vars['TypeArr'][$this->_sections['type']['index']]['type_id']; ?>
" title="<?php echo $this->_tpl_vars['TypeArr'][$this->_sections['type']['index']]['type_full_title']; ?>
"> 
					<?php echo $this->_tpl_vars['TypeArr'][$this->_sections['type']['index']]['type_title']; ?>
 (<?php echo $this->_tpl_vars['TypeArr'][$this->_sections['type']['index']]['countrow']; ?>
) </a> </li>
				 <?php endfor; endif; ?>
				 
		 </div>		 
	      </ul>
	   </li>	 
	 <?php endif; ?>	
	<?php endif; ?>
        <!-- Chandan Added This Ends-->
	<!-- Chandan Added this Starts-->
	<?php if ($this->_tpl_vars['is_versus'] == 'Y'): ?>
	 <?php if ($this->_tpl_vars['NumVersus'] != '0'): ?>
	    <li><a href="#">Versus</a>
		<ul>	
		 <div class="listing">
				 <?php unset($this->_sections['versus']);
$this->_sections['versus']['name'] = 'versus';
$this->_sections['versus']['loop'] = is_array($_loop=$this->_tpl_vars['VersusArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['versus']['show'] = true;
$this->_sections['versus']['max'] = $this->_sections['versus']['loop'];
$this->_sections['versus']['step'] = 1;
$this->_sections['versus']['start'] = $this->_sections['versus']['step'] > 0 ? 0 : $this->_sections['versus']['loop']-1;
if ($this->_sections['versus']['show']) {
    $this->_sections['versus']['total'] = $this->_sections['versus']['loop'];
    if ($this->_sections['versus']['total'] == 0)
        $this->_sections['versus']['show'] = false;
} else
    $this->_sections['versus']['total'] = 0;
if ($this->_sections['versus']['show']):

            for ($this->_sections['versus']['index'] = $this->_sections['versus']['start'], $this->_sections['versus']['iteration'] = 1;
                 $this->_sections['versus']['iteration'] <= $this->_sections['versus']['total'];
                 $this->_sections['versus']['index'] += $this->_sections['versus']['step'], $this->_sections['versus']['iteration']++):
$this->_sections['versus']['rownum'] = $this->_sections['versus']['iteration'];
$this->_sections['versus']['index_prev'] = $this->_sections['versus']['index'] - $this->_sections['versus']['step'];
$this->_sections['versus']['index_next'] = $this->_sections['versus']['index'] + $this->_sections['versus']['step'];
$this->_sections['versus']['first']      = ($this->_sections['versus']['iteration'] == 1);
$this->_sections['versus']['last']       = ($this->_sections['versus']['iteration'] == $this->_sections['versus']['total']);
?>
				   	<li>
					<input type="checkbox" name="versus_id[]" id='versus_<?php echo $this->_tpl_vars['VersusArr'][$this->_sections['versus']['index']]['versus_id']; ?>
' value="<?php echo $this->_tpl_vars['VersusArr'][$this->_sections['versus']['index']]['versus_id']; ?>
" style="display:none" 
					<?php if (sizeof($this->_tpl_vars['versus_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['VersusArr'][$this->_sections['versus']['index']]['versus_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['versus_id']) : in_array($_tmp, $this->_tpl_vars['versus_id']))): ?> checked="checked" <?php endif; ?>/>
					<a href="#" class="refine <?php if (sizeof($this->_tpl_vars['versus_id']) != '' && ((is_array($_tmp=$this->_tpl_vars['VersusArr'][$this->_sections['versus']['index']]['versus_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['versus_id']) : in_array($_tmp, $this->_tpl_vars['versus_id']))): ?> selected <?php endif; ?>" stype="V" typeId="<?php echo $this->_tpl_vars['VersusArr'][$this->_sections['versus']['index']]['versus_id']; ?>
"> 
					<?php echo $this->_tpl_vars['VersusArr'][$this->_sections['versus']['index']]['versus_title']; ?>
 (<?php echo $this->_tpl_vars['VersusArr'][$this->_sections['versus']['index']]['countrow']; ?>
) </a> </li>
				 <?php endfor; endif; ?>
				 
		 </div>		 
	      </ul>
	   </li>	 
	 <?php endif; ?>	
	<?php endif; ?>
	<li><a href="#">Duration (min)</a> 
           <ul>	
				  <li style="height:30px; padding:15px 5px 5px 5px;"> 
				   <span style="display: inline-block; width: 230px; padding: 0 5px;">
				   <input id="Slider2" type="slider" name="duration" value="<?php echo $this->_tpl_vars['duration']; ?>
"/></span> 
                    <?php echo '
					 <script type="text/javascript" charset="utf-8">
					  jQuery("#Slider2").slider({ from: 0, to: 60, limits: false, step: 1,smooth: true, round: 0, dimension: \'\', skin: "plastic", callback: 
					  function( value ){ /* document.frmRefine.submit() */ frmRefine_POST(); } });
					</script>
					'; ?>

				 </li>	
		   </ul>
	</li>
        <!-- Chandan Added This Ends-->

	<!-- <?php if ($this->_tpl_vars['is_server'] == 'Y'): ?>
	 <?php if ($this->_tpl_vars['Numserver'] != '0'): ?>
	    <li><a href="#">Server</a>
        <ul>	
		 <div class="listing">
				 <?php unset($this->_sections['server']);
$this->_sections['server']['name'] = 'server';
$this->_sections['server']['loop'] = is_array($_loop=$this->_tpl_vars['serverArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['server']['show'] = true;
$this->_sections['server']['max'] = $this->_sections['server']['loop'];
$this->_sections['server']['step'] = 1;
$this->_sections['server']['start'] = $this->_sections['server']['step'] > 0 ? 0 : $this->_sections['server']['loop']-1;
if ($this->_sections['server']['show']) {
    $this->_sections['server']['total'] = $this->_sections['server']['loop'];
    if ($this->_sections['server']['total'] == 0)
        $this->_sections['server']['show'] = false;
} else
    $this->_sections['server']['total'] = 0;
if ($this->_sections['server']['show']):

            for ($this->_sections['server']['index'] = $this->_sections['server']['start'], $this->_sections['server']['iteration'] = 1;
                 $this->_sections['server']['iteration'] <= $this->_sections['server']['total'];
                 $this->_sections['server']['index'] += $this->_sections['server']['step'], $this->_sections['server']['iteration']++):
$this->_sections['server']['rownum'] = $this->_sections['server']['iteration'];
$this->_sections['server']['index_prev'] = $this->_sections['server']['index'] - $this->_sections['server']['step'];
$this->_sections['server']['index_next'] = $this->_sections['server']['index'] + $this->_sections['server']['step'];
$this->_sections['server']['first']      = ($this->_sections['server']['iteration'] == 1);
$this->_sections['server']['last']       = ($this->_sections['server']['iteration'] == $this->_sections['server']['total']);
?>
				   	<li>
					<input type="checkbox" name="server_id[]" id='server_<?php echo $this->_tpl_vars['serverArr'][$this->_sections['server']['index']]['server_id']; ?>
' value="<?php echo $this->_tpl_vars['serverArr'][$this->_sections['server']['index']]['server_id']; ?>
" style="display:none" 
					<?php if (((is_array($_tmp=$this->_tpl_vars['serverArr'][$this->_sections['server']['index']]['server_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['server_id']) : in_array($_tmp, $this->_tpl_vars['server_id']))): ?> checked="checked" <?php endif; ?>/>
					<a href="#" class="refine <?php if (((is_array($_tmp=$this->_tpl_vars['serverArr'][$this->_sections['server']['index']]['server_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['server_id']) : in_array($_tmp, $this->_tpl_vars['server_id']))): ?> selected <?php endif; ?>" stype="S" typeId="<?php echo $this->_tpl_vars['serverArr'][$this->_sections['server']['index']]['server_id']; ?>
"> 
					<?php echo $this->_tpl_vars['serverArr'][$this->_sections['server']['index']]['server_name']; ?>
 (<?php echo $this->_tpl_vars['serverArr'][$this->_sections['server']['index']]['countrow']; ?>
) </a> </li>
                 <?php endfor; endif; ?>
				 
		 </div>		 
		 </ul>
	   </li>	 
	 <?php endif; ?>	
	<?php endif; ?> 
	<?php if ($this->_tpl_vars['is_region'] == 'Y'): ?>
	 <?php if ($this->_tpl_vars['Numregion'] != '0'): ?>
	    <li><a href="#">Region</a>
        <ul>	
		 <div class="listing">
				 <?php unset($this->_sections['region']);
$this->_sections['region']['name'] = 'region';
$this->_sections['region']['loop'] = is_array($_loop=$this->_tpl_vars['regionArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['region']['show'] = true;
$this->_sections['region']['max'] = $this->_sections['region']['loop'];
$this->_sections['region']['step'] = 1;
$this->_sections['region']['start'] = $this->_sections['region']['step'] > 0 ? 0 : $this->_sections['region']['loop']-1;
if ($this->_sections['region']['show']) {
    $this->_sections['region']['total'] = $this->_sections['region']['loop'];
    if ($this->_sections['region']['total'] == 0)
        $this->_sections['region']['show'] = false;
} else
    $this->_sections['region']['total'] = 0;
if ($this->_sections['region']['show']):

            for ($this->_sections['region']['index'] = $this->_sections['region']['start'], $this->_sections['region']['iteration'] = 1;
                 $this->_sections['region']['iteration'] <= $this->_sections['region']['total'];
                 $this->_sections['region']['index'] += $this->_sections['region']['step'], $this->_sections['region']['iteration']++):
$this->_sections['region']['rownum'] = $this->_sections['region']['iteration'];
$this->_sections['region']['index_prev'] = $this->_sections['region']['index'] - $this->_sections['region']['step'];
$this->_sections['region']['index_next'] = $this->_sections['region']['index'] + $this->_sections['region']['step'];
$this->_sections['region']['first']      = ($this->_sections['region']['iteration'] == 1);
$this->_sections['region']['last']       = ($this->_sections['region']['iteration'] == $this->_sections['region']['total']);
?>
				   	<li>
					<input type="checkbox" name="region_id[]" id='region_<?php echo $this->_tpl_vars['regionArr'][$this->_sections['region']['index']]['region_id']; ?>
' value="<?php echo $this->_tpl_vars['regionArr'][$this->_sections['region']['index']]['region_id']; ?>
" style="display:none;"
					<?php if (((is_array($_tmp=$this->_tpl_vars['regionArr'][$this->_sections['region']['index']]['region_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['region_id']) : in_array($_tmp, $this->_tpl_vars['region_id']))): ?> checked="checked" <?php endif; ?>/>
					<a href="#" class="refine <?php if (((is_array($_tmp=$this->_tpl_vars['regionArr'][$this->_sections['region']['index']]['region_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['region_id']) : in_array($_tmp, $this->_tpl_vars['region_id']))): ?> selected <?php endif; ?>" stype="X" typeId="<?php echo $this->_tpl_vars['regionArr'][$this->_sections['region']['index']]['region_id']; ?>
"> 
					<?php echo $this->_tpl_vars['regionArr'][$this->_sections['region']['index']]['region_title']; ?>
 (<?php echo $this->_tpl_vars['regionArr'][$this->_sections['region']['index']]['countrow']; ?>
) </a> </li>
                 <?php endfor; endif; ?>
				 
		 </div>		 
		 </ul>
	   </li>	 
	 <?php endif; ?>	
	<?php endif; ?> 
	<?php if ($this->_tpl_vars['is_rating'] == 'Y'): ?>
	 <?php if ($this->_tpl_vars['Numrating'] != '0'): ?>
	    <li><a href="#">Rating</a>
        <ul>	
		 <div class="listing">
				 <?php unset($this->_sections['rating']);
$this->_sections['rating']['name'] = 'rating';
$this->_sections['rating']['loop'] = is_array($_loop=$this->_tpl_vars['ratingArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rating']['show'] = true;
$this->_sections['rating']['max'] = $this->_sections['rating']['loop'];
$this->_sections['rating']['step'] = 1;
$this->_sections['rating']['start'] = $this->_sections['rating']['step'] > 0 ? 0 : $this->_sections['rating']['loop']-1;
if ($this->_sections['rating']['show']) {
    $this->_sections['rating']['total'] = $this->_sections['rating']['loop'];
    if ($this->_sections['rating']['total'] == 0)
        $this->_sections['rating']['show'] = false;
} else
    $this->_sections['rating']['total'] = 0;
if ($this->_sections['rating']['show']):

            for ($this->_sections['rating']['index'] = $this->_sections['rating']['start'], $this->_sections['rating']['iteration'] = 1;
                 $this->_sections['rating']['iteration'] <= $this->_sections['rating']['total'];
                 $this->_sections['rating']['index'] += $this->_sections['rating']['step'], $this->_sections['rating']['iteration']++):
$this->_sections['rating']['rownum'] = $this->_sections['rating']['iteration'];
$this->_sections['rating']['index_prev'] = $this->_sections['rating']['index'] - $this->_sections['rating']['step'];
$this->_sections['rating']['index_next'] = $this->_sections['rating']['index'] + $this->_sections['rating']['step'];
$this->_sections['rating']['first']      = ($this->_sections['rating']['iteration'] == 1);
$this->_sections['rating']['last']       = ($this->_sections['rating']['iteration'] == $this->_sections['rating']['total']);
?>
				   	<li>
					<input type="checkbox" name="rating_id[]" id='rating_<?php echo $this->_tpl_vars['ratingArr'][$this->_sections['rating']['index']]['rating_id']; ?>
' value="<?php echo $this->_tpl_vars['ratingArr'][$this->_sections['rating']['index']]['rating_id']; ?>
" style="display:none" 
					<?php if (((is_array($_tmp=$this->_tpl_vars['ratingArr'][$this->_sections['rating']['index']]['rating_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['rating_id']) : in_array($_tmp, $this->_tpl_vars['rating_id']))): ?> checked="checked" <?php endif; ?>/>
					<a href="#" class="refine <?php if (((is_array($_tmp=$this->_tpl_vars['ratingArr'][$this->_sections['rating']['index']]['rating_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['rating_id']) : in_array($_tmp, $this->_tpl_vars['rating_id']))): ?> selected <?php endif; ?>" stype="Y" typeId="<?php echo $this->_tpl_vars['ratingArr'][$this->_sections['rating']['index']]['rating_id']; ?>
"> 
					<?php echo $this->_tpl_vars['ratingArr'][$this->_sections['rating']['index']]['rating_title']; ?>
 (<?php echo $this->_tpl_vars['ratingArr'][$this->_sections['rating']['index']]['countrow']; ?>
) </a> </li>
                 <?php endfor; endif; ?>
				 
		 </div>		 
		 </ul>
	   </li>	 
	 <?php endif; ?>	
	<?php endif; ?> 
	<li><a href="#">Rate ($)</a> 
           <ul>	
		  <li style="height:30px; padding:15px 5px 5px 5px;"> 
		   <span style="display: inline-block; width: 230px; padding: 0 5px;">
		   <input id="Slider1" type="slider" name="price" value="<?php echo $this->_tpl_vars['price']; ?>
"/></span> 
			<?php echo '
			 <script type="text/javascript" charset="utf-8">
			  jQuery("#Slider1").slider({ from: 0, to: 100, limits: false, step: 1, dimension: \'\', skin: "plastic", callback: 
			  function( value ){ /* document.frmRefine.submit() */ frmRefine_POST(); } });
			</script>
			'; ?>

		 </li>	
	  </ul>
	</li>  
	<?php if ($this->_tpl_vars['Numlanguage'] != '0'): ?>	
	     <li><a href="#">Language</a>
            <ul>
			 <div class="listing">
                   
				  <?php unset($this->_sections['lang']);
$this->_sections['lang']['name'] = 'lang';
$this->_sections['lang']['loop'] = is_array($_loop=$this->_tpl_vars['languageArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['lang']['show'] = true;
$this->_sections['lang']['max'] = $this->_sections['lang']['loop'];
$this->_sections['lang']['step'] = 1;
$this->_sections['lang']['start'] = $this->_sections['lang']['step'] > 0 ? 0 : $this->_sections['lang']['loop']-1;
if ($this->_sections['lang']['show']) {
    $this->_sections['lang']['total'] = $this->_sections['lang']['loop'];
    if ($this->_sections['lang']['total'] == 0)
        $this->_sections['lang']['show'] = false;
} else
    $this->_sections['lang']['total'] = 0;
if ($this->_sections['lang']['show']):

            for ($this->_sections['lang']['index'] = $this->_sections['lang']['start'], $this->_sections['lang']['iteration'] = 1;
                 $this->_sections['lang']['iteration'] <= $this->_sections['lang']['total'];
                 $this->_sections['lang']['index'] += $this->_sections['lang']['step'], $this->_sections['lang']['iteration']++):
$this->_sections['lang']['rownum'] = $this->_sections['lang']['iteration'];
$this->_sections['lang']['index_prev'] = $this->_sections['lang']['index'] - $this->_sections['lang']['step'];
$this->_sections['lang']['index_next'] = $this->_sections['lang']['index'] + $this->_sections['lang']['step'];
$this->_sections['lang']['first']      = ($this->_sections['lang']['iteration'] == 1);
$this->_sections['lang']['last']       = ($this->_sections['lang']['iteration'] == $this->_sections['lang']['total']);
?>
				  	<li>
					<input type="checkbox" name="language_id[]" id="language_<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id']; ?>
" value="<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id']; ?>
" style="display:none;" 
					<?php if (((is_array($_tmp=$this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['language_id']) : in_array($_tmp, $this->_tpl_vars['language_id']))): ?> checked="checked" <?php endif; ?>/>				
					<a class="refine <?php if (((is_array($_tmp=$this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['language_id']) : in_array($_tmp, $this->_tpl_vars['language_id']))): ?> selected <?php endif; ?>" stype="Z" typeId="<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id']; ?>
" >
					<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_name']; ?>
 (<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['cnt']; ?>
)</a></a></li>
                  <?php endfor; endif; ?>
			  </div> 
			</ul>
		</li>	
	  <?php endif; ?>	
	<li><a href="#">Availability</a>
           <ul>
		   <div class="listing">
			 
			  <li> <input type="checkbox" name="availability[]" id="avail_O" value="O" style="display:none" <?php if (((is_array($_tmp='O')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> checked="checked" <?php endif; ?> />
			  <a href="#" class="refine <?php if (((is_array($_tmp='O')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> selected <?php endif; ?>" stype="A" typeId="O"> Online(<?php echo $this->_tpl_vars['avail1Num']; ?>
)</a></li>
			  <li> <input type="checkbox" name="availability[]" id="avail_L" value="L" style="display:none" <?php if (((is_array($_tmp='L')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> checked="checked" <?php endif; ?> />
			  <a href="#" class="refine <?php if (((is_array($_tmp='L')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> selected <?php endif; ?>" stype="A" typeId="L" > Local meet-up(<?php echo $this->_tpl_vars['avail2Num']; ?>
)</a></li>
		   </div>  
	   </ul>
	</li> -->   
	   
	  <input type="hidden" name="record_per_page" id="record_per_page" value="<?php echo $this->_tpl_vars['record_per_page']; ?>
" style="visibility:hidden">

	  <input type="submit" style="visibility:hidden">
	 </form> 	
    </ul>
   </div>   
   </div>