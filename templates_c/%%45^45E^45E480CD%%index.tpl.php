<?php /* Smarty version 2.6.16, created on 2013-03-27 19:15:07
         compiled from index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'index.tpl', 205, false),)), $this); ?>
<?php echo '
<link rel="stylesheet" href="style/nivo-slider.css" type="text/css" media="screen" />
<script type="text/javascript" src="scripts/jquery.nivo.slider.js"></script>
<script type="text/javascript">
jQuery(window).load(function() {
        jQuery(\'#slider\').nivoSlider({pauseTime:5000});
    });
</script>
<link href=\'https://fonts.googleapis.com/css?family=Dosis:400,200,600\' rel=\'stylesheet\' type=\'text/css\'>
<div id="fb-root"></div>
<script>
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
	fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));
</script>
'; ?>


<div class="content-right">
    <div class="face_book"><!--img src="images/facebook_snap.jpg" height="285" width="300" /-->
	<div class="fb-like-box shadowed" data-href="<?php echo $this->_tpl_vars['facebook']; ?>
" data-width="290" data-height="285" data-show-faces="true" data-stream="false" data-header="true"></div>
    </div>

     
    <div class="advertise" style="overflow:hidden;">
		<h2 title="Advertisement"></h2>
	   <script charset="utf-8" src="https://widgets.twimg.com/j/2/widget.js"></script>
		<script>
			new TWTR.Widget({
			  version: 2,
			  type: 'profile',
			  rpp: 4,
			  interval: 30000,
			  width: 290,
			  height: 375,
			  theme: {
			    shell: {
				  background: '#003366',
				  color: '#ffffff'
				},
				tweets: {
				  background: '#ffffff',
				  color: '#000000',
				  links: '#0084b4'
				}
			  },
			  features: {
			    scrollbar: false,
			    loop: false,
			    live: false,
			    behavior: 'all'
			  }
			}).render().setUser('<?php echo $this->_tpl_vars['twitter']; ?>
').start();
		</script>
		
    </div>
</div>
<div class="content-left">
    <div class="slider-wrapper theme-default">
        <div class="ribbon"></div>
        <div id="slider" class="nivoSlider">
            <?php unset($this->_sections['img']);
$this->_sections['img']['name'] = 'img';
$this->_sections['img']['loop'] = is_array($_loop=$this->_tpl_vars['homenevoArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['img']['show'] = true;
$this->_sections['img']['max'] = $this->_sections['img']['loop'];
$this->_sections['img']['step'] = 1;
$this->_sections['img']['start'] = $this->_sections['img']['step'] > 0 ? 0 : $this->_sections['img']['loop']-1;
if ($this->_sections['img']['show']) {
    $this->_sections['img']['total'] = $this->_sections['img']['loop'];
    if ($this->_sections['img']['total'] == 0)
        $this->_sections['img']['show'] = false;
} else
    $this->_sections['img']['total'] = 0;
if ($this->_sections['img']['show']):

            for ($this->_sections['img']['index'] = $this->_sections['img']['start'], $this->_sections['img']['iteration'] = 1;
                 $this->_sections['img']['iteration'] <= $this->_sections['img']['total'];
                 $this->_sections['img']['index'] += $this->_sections['img']['step'], $this->_sections['img']['iteration']++):
$this->_sections['img']['rownum'] = $this->_sections['img']['iteration'];
$this->_sections['img']['index_prev'] = $this->_sections['img']['index'] - $this->_sections['img']['step'];
$this->_sections['img']['index_next'] = $this->_sections['img']['index'] + $this->_sections['img']['step'];
$this->_sections['img']['first']      = ($this->_sections['img']['iteration'] == 1);
$this->_sections['img']['last']       = ($this->_sections['img']['iteration'] == $this->_sections['img']['total']);
?>
            <?php $this->assign('i', $this->_sections['img']['index']+1); ?>
            <img src="uploaded/home_images/<?php echo $this->_tpl_vars['homenevoArr'][$this->_sections['img']['index']]['image_file']; ?>
" alt="<?php echo $this->_tpl_vars['homenevoArr'][$this->_sections['img']['index']]['image_alt']; ?>
" title="#htmlcaption<?php echo $this->_tpl_vars['i']; ?>
" />
            <?php endfor; endif; ?>
        </div>
        <?php unset($this->_sections['desc']);
$this->_sections['desc']['name'] = 'desc';
$this->_sections['desc']['loop'] = is_array($_loop=$this->_tpl_vars['homenevoArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['desc']['show'] = true;
$this->_sections['desc']['max'] = $this->_sections['desc']['loop'];
$this->_sections['desc']['step'] = 1;
$this->_sections['desc']['start'] = $this->_sections['desc']['step'] > 0 ? 0 : $this->_sections['desc']['loop']-1;
if ($this->_sections['desc']['show']) {
    $this->_sections['desc']['total'] = $this->_sections['desc']['loop'];
    if ($this->_sections['desc']['total'] == 0)
        $this->_sections['desc']['show'] = false;
} else
    $this->_sections['desc']['total'] = 0;
if ($this->_sections['desc']['show']):

            for ($this->_sections['desc']['index'] = $this->_sections['desc']['start'], $this->_sections['desc']['iteration'] = 1;
                 $this->_sections['desc']['iteration'] <= $this->_sections['desc']['total'];
                 $this->_sections['desc']['index'] += $this->_sections['desc']['step'], $this->_sections['desc']['iteration']++):
$this->_sections['desc']['rownum'] = $this->_sections['desc']['iteration'];
$this->_sections['desc']['index_prev'] = $this->_sections['desc']['index'] - $this->_sections['desc']['step'];
$this->_sections['desc']['index_next'] = $this->_sections['desc']['index'] + $this->_sections['desc']['step'];
$this->_sections['desc']['first']      = ($this->_sections['desc']['iteration'] == 1);
$this->_sections['desc']['last']       = ($this->_sections['desc']['iteration'] == $this->_sections['desc']['total']);
?>
        <?php $this->assign('x', $this->_sections['desc']['index']+1); ?>
			
        <div id="htmlcaption<?php echo $this->_tpl_vars['x']; ?>
" class="nivo-html-caption">
            <h2><?php echo $this->_tpl_vars['homenevoArr'][$this->_sections['desc']['index']]['title']; ?>
</h2>
            <p><?php echo $this->_tpl_vars['homenevoArr'][$this->_sections['desc']['index']]['description']; ?>
</p>
            <a href="<?php if ($this->_tpl_vars['homenevoArr'][$this->_sections['desc']['index']]['banner_link'] != ''):  echo $this->_tpl_vars['homenevoArr'][$this->_sections['desc']['index']]['banner_link'];  else: ?>news.php<?php endif; ?>"><span class="more_news"></span></a>
        </div>
        <?php endfor; endif; ?>      
    </div>
	
    <div class="window shadowed"> 
        <div class="windowtitle">Popular Coaches</div>
		<div class="windowcontent">
			<div class="coachresume" style="border-right: 1px solid #003366;">
				<div class="coachtitle">Most Lesson</div>
				<div class="coach_list">
					<?php unset($this->_sections['lCoach']);
$this->_sections['lCoach']['name'] = 'lCoach';
$this->_sections['lCoach']['loop'] = is_array($_loop=$this->_tpl_vars['lessonsCoachesArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['lCoach']['show'] = true;
$this->_sections['lCoach']['max'] = $this->_sections['lCoach']['loop'];
$this->_sections['lCoach']['step'] = 1;
$this->_sections['lCoach']['start'] = $this->_sections['lCoach']['step'] > 0 ? 0 : $this->_sections['lCoach']['loop']-1;
if ($this->_sections['lCoach']['show']) {
    $this->_sections['lCoach']['total'] = $this->_sections['lCoach']['loop'];
    if ($this->_sections['lCoach']['total'] == 0)
        $this->_sections['lCoach']['show'] = false;
} else
    $this->_sections['lCoach']['total'] = 0;
if ($this->_sections['lCoach']['show']):

            for ($this->_sections['lCoach']['index'] = $this->_sections['lCoach']['start'], $this->_sections['lCoach']['iteration'] = 1;
                 $this->_sections['lCoach']['iteration'] <= $this->_sections['lCoach']['total'];
                 $this->_sections['lCoach']['index'] += $this->_sections['lCoach']['step'], $this->_sections['lCoach']['iteration']++):
$this->_sections['lCoach']['rownum'] = $this->_sections['lCoach']['iteration'];
$this->_sections['lCoach']['index_prev'] = $this->_sections['lCoach']['index'] - $this->_sections['lCoach']['step'];
$this->_sections['lCoach']['index_next'] = $this->_sections['lCoach']['index'] + $this->_sections['lCoach']['step'];
$this->_sections['lCoach']['first']      = ($this->_sections['lCoach']['iteration'] == 1);
$this->_sections['lCoach']['last']       = ($this->_sections['lCoach']['iteration'] == $this->_sections['lCoach']['total']);
?>
						<?php $this->assign('i', $this->_sections['lCoach']['index']+1); ?>
							<a href="javascript:;" onclick="showCoachDetails('lessonscoachdetails_<?php echo $this->_tpl_vars['lessonsCoachesArr'][$this->_sections['lCoach']['index']]['user_id']; ?>
')"><span>#<?php echo $this->_tpl_vars['i']; ?>
 <?php echo $this->_tpl_vars['lessonsCoachesArr'][$this->_sections['lCoach']['index']]['username']; ?>
</span></a>
						
					<?php endfor; endif; ?> 
				</div>
			</div>
			<div class="coachresume" style="border-right: 1px solid #003366;">
				<div class="coachtitle">Highest Ratings</div>
				<div class="coach_list">
					<?php unset($this->_sections['rCoach']);
$this->_sections['rCoach']['name'] = 'rCoach';
$this->_sections['rCoach']['loop'] = is_array($_loop=$this->_tpl_vars['ratingCoachesArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rCoach']['show'] = true;
$this->_sections['rCoach']['max'] = $this->_sections['rCoach']['loop'];
$this->_sections['rCoach']['step'] = 1;
$this->_sections['rCoach']['start'] = $this->_sections['rCoach']['step'] > 0 ? 0 : $this->_sections['rCoach']['loop']-1;
if ($this->_sections['rCoach']['show']) {
    $this->_sections['rCoach']['total'] = $this->_sections['rCoach']['loop'];
    if ($this->_sections['rCoach']['total'] == 0)
        $this->_sections['rCoach']['show'] = false;
} else
    $this->_sections['rCoach']['total'] = 0;
if ($this->_sections['rCoach']['show']):

            for ($this->_sections['rCoach']['index'] = $this->_sections['rCoach']['start'], $this->_sections['rCoach']['iteration'] = 1;
                 $this->_sections['rCoach']['iteration'] <= $this->_sections['rCoach']['total'];
                 $this->_sections['rCoach']['index'] += $this->_sections['rCoach']['step'], $this->_sections['rCoach']['iteration']++):
$this->_sections['rCoach']['rownum'] = $this->_sections['rCoach']['iteration'];
$this->_sections['rCoach']['index_prev'] = $this->_sections['rCoach']['index'] - $this->_sections['rCoach']['step'];
$this->_sections['rCoach']['index_next'] = $this->_sections['rCoach']['index'] + $this->_sections['rCoach']['step'];
$this->_sections['rCoach']['first']      = ($this->_sections['rCoach']['iteration'] == 1);
$this->_sections['rCoach']['last']       = ($this->_sections['rCoach']['iteration'] == $this->_sections['rCoach']['total']);
?>
						<?php $this->assign('i', $this->_sections['rCoach']['index']+1); ?>
							<a href="javascript:;" onclick="showCoachDetails('ratingcoachdetails_<?php echo $this->_tpl_vars['ratingCoachesArr'][$this->_sections['rCoach']['index']]['user_id']; ?>
')"><span>#<?php echo $this->_tpl_vars['i']; ?>
 <?php echo $this->_tpl_vars['ratingCoachesArr'][$this->_sections['rCoach']['index']]['username']; ?>
</span></a>
						
					<?php endfor; endif; ?> 
				</div>
			</div>
			<div class="coachresume" style="border-right: 1px solid #003366;">
				<div class="coachtitle">Featured Coaches</div>
				<div class="coach_list">
					<?php unset($this->_sections['fCoach']);
$this->_sections['fCoach']['name'] = 'fCoach';
$this->_sections['fCoach']['loop'] = is_array($_loop=$this->_tpl_vars['featuredCoachesArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['fCoach']['show'] = true;
$this->_sections['fCoach']['max'] = $this->_sections['fCoach']['loop'];
$this->_sections['fCoach']['step'] = 1;
$this->_sections['fCoach']['start'] = $this->_sections['fCoach']['step'] > 0 ? 0 : $this->_sections['fCoach']['loop']-1;
if ($this->_sections['fCoach']['show']) {
    $this->_sections['fCoach']['total'] = $this->_sections['fCoach']['loop'];
    if ($this->_sections['fCoach']['total'] == 0)
        $this->_sections['fCoach']['show'] = false;
} else
    $this->_sections['fCoach']['total'] = 0;
if ($this->_sections['fCoach']['show']):

            for ($this->_sections['fCoach']['index'] = $this->_sections['fCoach']['start'], $this->_sections['fCoach']['iteration'] = 1;
                 $this->_sections['fCoach']['iteration'] <= $this->_sections['fCoach']['total'];
                 $this->_sections['fCoach']['index'] += $this->_sections['fCoach']['step'], $this->_sections['fCoach']['iteration']++):
$this->_sections['fCoach']['rownum'] = $this->_sections['fCoach']['iteration'];
$this->_sections['fCoach']['index_prev'] = $this->_sections['fCoach']['index'] - $this->_sections['fCoach']['step'];
$this->_sections['fCoach']['index_next'] = $this->_sections['fCoach']['index'] + $this->_sections['fCoach']['step'];
$this->_sections['fCoach']['first']      = ($this->_sections['fCoach']['iteration'] == 1);
$this->_sections['fCoach']['last']       = ($this->_sections['fCoach']['iteration'] == $this->_sections['fCoach']['total']);
?>
						<?php $this->assign('i', $this->_sections['fCoach']['index']+1); ?>
							<a href="javascript:;" onclick="showCoachDetails('featuredcoachdetails_<?php echo $this->_tpl_vars['featuredCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')"><span>#<?php echo $this->_tpl_vars['i']; ?>
 <?php echo $this->_tpl_vars['featuredCoachesArr'][$this->_sections['fCoach']['index']]['username']; ?>
</span></a>
						
					<?php endfor; endif; ?> 
				</div>
			</div>
			<div class="coachresume" style="width: 210px;">
				<?php unset($this->_sections['fCoach']);
$this->_sections['fCoach']['name'] = 'fCoach';
$this->_sections['fCoach']['loop'] = is_array($_loop=$this->_tpl_vars['featuredCoachesArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['fCoach']['show'] = true;
$this->_sections['fCoach']['max'] = $this->_sections['fCoach']['loop'];
$this->_sections['fCoach']['step'] = 1;
$this->_sections['fCoach']['start'] = $this->_sections['fCoach']['step'] > 0 ? 0 : $this->_sections['fCoach']['loop']-1;
if ($this->_sections['fCoach']['show']) {
    $this->_sections['fCoach']['total'] = $this->_sections['fCoach']['loop'];
    if ($this->_sections['fCoach']['total'] == 0)
        $this->_sections['fCoach']['show'] = false;
} else
    $this->_sections['fCoach']['total'] = 0;
if ($this->_sections['fCoach']['show']):

            for ($this->_sections['fCoach']['index'] = $this->_sections['fCoach']['start'], $this->_sections['fCoach']['iteration'] = 1;
                 $this->_sections['fCoach']['iteration'] <= $this->_sections['fCoach']['total'];
                 $this->_sections['fCoach']['index'] += $this->_sections['fCoach']['step'], $this->_sections['fCoach']['iteration']++):
$this->_sections['fCoach']['rownum'] = $this->_sections['fCoach']['iteration'];
$this->_sections['fCoach']['index_prev'] = $this->_sections['fCoach']['index'] - $this->_sections['fCoach']['step'];
$this->_sections['fCoach']['index_next'] = $this->_sections['fCoach']['index'] + $this->_sections['fCoach']['step'];
$this->_sections['fCoach']['first']      = ($this->_sections['fCoach']['iteration'] == 1);
$this->_sections['fCoach']['last']       = ($this->_sections['fCoach']['iteration'] == $this->_sections['fCoach']['total']);
?>
				<?php $this->assign('i', $this->_sections['fCoach']['index']+1); ?>
					<div class="coaches" id="featuredcoachdetails_<?php echo $this->_tpl_vars['featuredCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
" style="<?php if ($this->_sections['fCoach']['index'] == '0'): ?> display: block; <?php endif; ?>">
						<div style="padding: 5px">
							<a href="javascript:;" <?php if ($_SESSION['user_id'] != ''): ?>onclick="buildPageLink('<?php echo $this->_tpl_vars['featuredCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')"<?php else: ?>onClick="openLogin('/coachdetails.php?coach_id=<?php echo $this->_tpl_vars['featuredCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')"<?php endif; ?> class="coaches_photo"> 
								<img width="60px" height="60px" src="<?php if ($this->_tpl_vars['featuredCoachesArr'][$this->_sections['fCoach']['index']]['photo']): ?>uploaded/user_images/thumbs/mid_<?php echo $this->_tpl_vars['featuredCoachesArr'][$this->_sections['fCoach']['index']]['photo'];  else: ?>images/featured_img.jpg<?php endif; ?>" alt="" border="0"  />
							</a>
							<div style="" class="coaches_name">
								<a href="javascript:;" <?php if ($_SESSION['user_id'] != ''): ?> onclick="buildPageLink('<?php echo $this->_tpl_vars['featuredCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')" <?php else: ?>onClick="openLogin('/coachdetails.php?coach_id=<?php echo $this->_tpl_vars['featuredCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')"<?php endif; ?>><?php echo $this->_tpl_vars['featuredCoachesArr'][$this->_sections['fCoach']['index']]['username']; ?>
</a>
							</div>
							<div class="clear"></div>
							
							<div class="coaches_description" id="coaches_description_<?php echo $this->_tpl_vars['featuredCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
">
								<div style="overflow: hidden;height: 90px;">
									<?php echo $this->_tpl_vars['featuredCoachesArr'][$this->_sections['fCoach']['index']]['about']; ?>

								</div>
								<div class="btn">
									<?php if ($_SESSION['user_id'] != ''): ?><a onclick="buildPageLink('<?php echo $this->_tpl_vars['featuredCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')" href="javascript:;" target="_self">More</a><?php endif; ?>
								</div>
							</div>
						</div>
					</div>
			  
			 <?php endfor; endif; ?>  
			 
			 <?php unset($this->_sections['fCoach']);
$this->_sections['fCoach']['name'] = 'fCoach';
$this->_sections['fCoach']['loop'] = is_array($_loop=$this->_tpl_vars['ratingCoachesArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['fCoach']['show'] = true;
$this->_sections['fCoach']['max'] = $this->_sections['fCoach']['loop'];
$this->_sections['fCoach']['step'] = 1;
$this->_sections['fCoach']['start'] = $this->_sections['fCoach']['step'] > 0 ? 0 : $this->_sections['fCoach']['loop']-1;
if ($this->_sections['fCoach']['show']) {
    $this->_sections['fCoach']['total'] = $this->_sections['fCoach']['loop'];
    if ($this->_sections['fCoach']['total'] == 0)
        $this->_sections['fCoach']['show'] = false;
} else
    $this->_sections['fCoach']['total'] = 0;
if ($this->_sections['fCoach']['show']):

            for ($this->_sections['fCoach']['index'] = $this->_sections['fCoach']['start'], $this->_sections['fCoach']['iteration'] = 1;
                 $this->_sections['fCoach']['iteration'] <= $this->_sections['fCoach']['total'];
                 $this->_sections['fCoach']['index'] += $this->_sections['fCoach']['step'], $this->_sections['fCoach']['iteration']++):
$this->_sections['fCoach']['rownum'] = $this->_sections['fCoach']['iteration'];
$this->_sections['fCoach']['index_prev'] = $this->_sections['fCoach']['index'] - $this->_sections['fCoach']['step'];
$this->_sections['fCoach']['index_next'] = $this->_sections['fCoach']['index'] + $this->_sections['fCoach']['step'];
$this->_sections['fCoach']['first']      = ($this->_sections['fCoach']['iteration'] == 1);
$this->_sections['fCoach']['last']       = ($this->_sections['fCoach']['iteration'] == $this->_sections['fCoach']['total']);
?>
				<?php $this->assign('i', $this->_sections['fCoach']['index']+1); ?>
					<div class="coaches" id="ratingcoachdetails_<?php echo $this->_tpl_vars['ratingCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
" style="">
						<div style="padding: 5px">
							<a href="javascript:;" <?php if ($_SESSION['user_id'] != ''): ?>onclick="buildPageLink('<?php echo $this->_tpl_vars['ratingCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')"<?php else: ?>onClick="openLogin('/coachdetails.php?coach_id=<?php echo $this->_tpl_vars['ratingCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')"<?php endif; ?> class="coaches_photo"> 
								<img width="60px" height="60px" src="<?php if ($this->_tpl_vars['ratingCoachesArr'][$this->_sections['fCoach']['index']]['photo']): ?>uploaded/user_images/thumbs/mid_<?php echo $this->_tpl_vars['ratingCoachesArr'][$this->_sections['fCoach']['index']]['photo'];  else: ?>images/featured_img.jpg<?php endif; ?>" alt="" border="0"  />
							</a>
							<div style="" class="coaches_name">
								<a href="javascript:;" <?php if ($_SESSION['user_id'] != ''): ?>onclick="buildPageLink('<?php echo $this->_tpl_vars['ratingCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')"<?php else: ?>onClick="openLogin('/coachdetails.php?coach_id=<?php echo $this->_tpl_vars['ratingCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')"<?php endif; ?>><?php echo $this->_tpl_vars['ratingCoachesArr'][$this->_sections['fCoach']['index']]['username']; ?>
</a>
							</div>
							<div class="clear"></div>
							
							<div class="coaches_description" id="coaches_description_<?php echo $this->_tpl_vars['ratingCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
">
								<div style="overflow: hidden;height: 90px;">
									<?php echo $this->_tpl_vars['ratingCoachesArr'][$this->_sections['fCoach']['index']]['about']; ?>

								</div>
								<div class="btn">
									<?php if ($_SESSION['user_id'] != ''): ?><a onclick="buildPageLink('<?php echo $this->_tpl_vars['ratingCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')" href="javascript:;" target="_self">More</a><?php endif; ?>
								</div>
							</div>
						</div>
					</div>
			  
			 <?php endfor; endif; ?> 
			 <?php unset($this->_sections['fCoach']);
$this->_sections['fCoach']['name'] = 'fCoach';
$this->_sections['fCoach']['loop'] = is_array($_loop=$this->_tpl_vars['lessonsCoachesArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['fCoach']['show'] = true;
$this->_sections['fCoach']['max'] = $this->_sections['fCoach']['loop'];
$this->_sections['fCoach']['step'] = 1;
$this->_sections['fCoach']['start'] = $this->_sections['fCoach']['step'] > 0 ? 0 : $this->_sections['fCoach']['loop']-1;
if ($this->_sections['fCoach']['show']) {
    $this->_sections['fCoach']['total'] = $this->_sections['fCoach']['loop'];
    if ($this->_sections['fCoach']['total'] == 0)
        $this->_sections['fCoach']['show'] = false;
} else
    $this->_sections['fCoach']['total'] = 0;
if ($this->_sections['fCoach']['show']):

            for ($this->_sections['fCoach']['index'] = $this->_sections['fCoach']['start'], $this->_sections['fCoach']['iteration'] = 1;
                 $this->_sections['fCoach']['iteration'] <= $this->_sections['fCoach']['total'];
                 $this->_sections['fCoach']['index'] += $this->_sections['fCoach']['step'], $this->_sections['fCoach']['iteration']++):
$this->_sections['fCoach']['rownum'] = $this->_sections['fCoach']['iteration'];
$this->_sections['fCoach']['index_prev'] = $this->_sections['fCoach']['index'] - $this->_sections['fCoach']['step'];
$this->_sections['fCoach']['index_next'] = $this->_sections['fCoach']['index'] + $this->_sections['fCoach']['step'];
$this->_sections['fCoach']['first']      = ($this->_sections['fCoach']['iteration'] == 1);
$this->_sections['fCoach']['last']       = ($this->_sections['fCoach']['iteration'] == $this->_sections['fCoach']['total']);
?>
				<?php $this->assign('i', $this->_sections['fCoach']['index']+1); ?>
					<div class="coaches" id="lessonscoachdetails_<?php echo $this->_tpl_vars['lessonsCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
" style="">
						<div style="padding: 5px">
							<a href="javascript:;" <?php if ($_SESSION['user_id'] != ''): ?>onclick="buildPageLink(<?php echo $this->_tpl_vars['lessonsCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
)"<?php else: ?>onClick="openLogin('/coachdetails.php?coach_id=<?php echo $this->_tpl_vars['lessonsCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')"<?php endif; ?> class="coaches_photo"> 
								<img width="60px" height="60px" src="<?php if ($this->_tpl_vars['lessonsCoachesArr'][$this->_sections['fCoach']['index']]['photo']): ?>uploaded/user_images/thumbs/mid_<?php echo $this->_tpl_vars['lessonsCoachesArr'][$this->_sections['fCoach']['index']]['photo'];  else: ?>images/featured_img.jpg<?php endif; ?>" alt="" border="0"  />
							</a>
							<div style="" class="coaches_name">
								<a href="javascript:;" <?php if ($_SESSION['user_id'] != ''): ?>onclick="buildPageLink('<?php echo $this->_tpl_vars['lessonsCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')"<?php else: ?>onClick="openLogin('/coachdetails.php?coach_id=<?php echo $this->_tpl_vars['lessonsCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')"<?php endif; ?>><?php echo $this->_tpl_vars['lessonsCoachesArr'][$this->_sections['fCoach']['index']]['username']; ?>
</a>
							</div>
							<div class="clear"></div>
							
							<div class="coaches_description" id="coaches_description_<?php echo $this->_tpl_vars['lessonsCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
">
								<div style="overflow: hidden;height: 90px;">
									<?php echo $this->_tpl_vars['lessonsCoachesArr'][$this->_sections['fCoach']['index']]['about']; ?>

								</div>
								<div class="btn">
									<?php if ($_SESSION['user_id'] != ''): ?><a onclick="buildPageLink('<?php echo $this->_tpl_vars['lessonsCoachesArr'][$this->_sections['fCoach']['index']]['user_id']; ?>
')" href="javascript:;" target="_self">More</a><?php endif; ?>
								</div>
							</div>
						</div>
					</div>
			  
			 <?php endfor; endif; ?> 
			
			</div>
			<div class="clear"></div>
		
		</div>
		
    </div>   
						
	<div class="window shadowed"> 
		<div class="windowtitle">In the Forums</div>
		<div class="windowcontent">
			<div class="coachresume" style="border-right: 1px solid #003366;width: 340px;">
				<div class="coachtitle">Popular Threads</div>
				<div class="coach_list">
				
					<?php unset($this->_sections['popularIndex']);
$this->_sections['popularIndex']['name'] = 'popularIndex';
$this->_sections['popularIndex']['loop'] = is_array($_loop=$this->_tpl_vars['ForumPopularArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['popularIndex']['show'] = true;
$this->_sections['popularIndex']['max'] = $this->_sections['popularIndex']['loop'];
$this->_sections['popularIndex']['step'] = 1;
$this->_sections['popularIndex']['start'] = $this->_sections['popularIndex']['step'] > 0 ? 0 : $this->_sections['popularIndex']['loop']-1;
if ($this->_sections['popularIndex']['show']) {
    $this->_sections['popularIndex']['total'] = $this->_sections['popularIndex']['loop'];
    if ($this->_sections['popularIndex']['total'] == 0)
        $this->_sections['popularIndex']['show'] = false;
} else
    $this->_sections['popularIndex']['total'] = 0;
if ($this->_sections['popularIndex']['show']):

            for ($this->_sections['popularIndex']['index'] = $this->_sections['popularIndex']['start'], $this->_sections['popularIndex']['iteration'] = 1;
                 $this->_sections['popularIndex']['iteration'] <= $this->_sections['popularIndex']['total'];
                 $this->_sections['popularIndex']['index'] += $this->_sections['popularIndex']['step'], $this->_sections['popularIndex']['iteration']++):
$this->_sections['popularIndex']['rownum'] = $this->_sections['popularIndex']['iteration'];
$this->_sections['popularIndex']['index_prev'] = $this->_sections['popularIndex']['index'] - $this->_sections['popularIndex']['step'];
$this->_sections['popularIndex']['index_next'] = $this->_sections['popularIndex']['index'] + $this->_sections['popularIndex']['step'];
$this->_sections['popularIndex']['first']      = ($this->_sections['popularIndex']['iteration'] == 1);
$this->_sections['popularIndex']['last']       = ($this->_sections['popularIndex']['iteration'] == $this->_sections['popularIndex']['total']);
?>
					<a class="forumlink" href="/forum/viewtopic.php?f=<?php echo $this->_tpl_vars['ForumPopularArr'][$this->_sections['popularIndex']['index']]['forum_id']; ?>
&t=<?php echo $this->_tpl_vars['ForumPopularArr'][$this->_sections['popularIndex']['index']]['topic_id']; ?>
" onclick="">
						<div class="forumtitle" style="float: left;"><span style="color: #004b68;">[<?php echo ((is_array($_tmp=$this->_tpl_vars['ForumPopularArr'][$this->_sections['popularIndex']['index']]['forum_name'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 8, "", true) : smarty_modifier_truncate($_tmp, 8, "", true)); ?>
] </span><?php echo ((is_array($_tmp=$this->_tpl_vars['ForumPopularArr'][$this->_sections['popularIndex']['index']]['topic_title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 36, "...", true) : smarty_modifier_truncate($_tmp, 36, "...", true)); ?>
</div>
						<div style="float: right; color: #666;"><?php echo $this->_tpl_vars['ForumPopularArr'][$this->_sections['popularIndex']['index']]['topic_views']; ?>
 views</div>
						<div class="clear"></div>
					</a>
					<?php endfor; endif; ?> 
				
				</div>
			</div>
			<div class="coachresume" style="width: 341px;">
				<div class="coachtitle">Latest Threads</div>
				<div class="coach_list">
                                    <?php unset($this->_sections['latestIndex']);
$this->_sections['latestIndex']['name'] = 'latestIndex';
$this->_sections['latestIndex']['loop'] = is_array($_loop=$this->_tpl_vars['ForumRecentArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['latestIndex']['show'] = true;
$this->_sections['latestIndex']['max'] = $this->_sections['latestIndex']['loop'];
$this->_sections['latestIndex']['step'] = 1;
$this->_sections['latestIndex']['start'] = $this->_sections['latestIndex']['step'] > 0 ? 0 : $this->_sections['latestIndex']['loop']-1;
if ($this->_sections['latestIndex']['show']) {
    $this->_sections['latestIndex']['total'] = $this->_sections['latestIndex']['loop'];
    if ($this->_sections['latestIndex']['total'] == 0)
        $this->_sections['latestIndex']['show'] = false;
} else
    $this->_sections['latestIndex']['total'] = 0;
if ($this->_sections['latestIndex']['show']):

            for ($this->_sections['latestIndex']['index'] = $this->_sections['latestIndex']['start'], $this->_sections['latestIndex']['iteration'] = 1;
                 $this->_sections['latestIndex']['iteration'] <= $this->_sections['latestIndex']['total'];
                 $this->_sections['latestIndex']['index'] += $this->_sections['latestIndex']['step'], $this->_sections['latestIndex']['iteration']++):
$this->_sections['latestIndex']['rownum'] = $this->_sections['latestIndex']['iteration'];
$this->_sections['latestIndex']['index_prev'] = $this->_sections['latestIndex']['index'] - $this->_sections['latestIndex']['step'];
$this->_sections['latestIndex']['index_next'] = $this->_sections['latestIndex']['index'] + $this->_sections['latestIndex']['step'];
$this->_sections['latestIndex']['first']      = ($this->_sections['latestIndex']['iteration'] == 1);
$this->_sections['latestIndex']['last']       = ($this->_sections['latestIndex']['iteration'] == $this->_sections['latestIndex']['total']);
?>
                                    <a class="forumlink" href="/forum/viewtopic.php?f=<?php echo $this->_tpl_vars['ForumRecentArr'][$this->_sections['latestIndex']['index']]['forum_id']; ?>
&t=<?php echo $this->_tpl_vars['ForumRecentArr'][$this->_sections['latestIndex']['index']]['topic_id']; ?>
" onclick="">
                                        <div class="forumtitle" style="float: left;"><span style="color: #004b68;">[<?php echo ((is_array($_tmp=$this->_tpl_vars['ForumRecentArr'][$this->_sections['latestIndex']['index']]['forum_name'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 8, "", true) : smarty_modifier_truncate($_tmp, 8, "", true)); ?>
] </span><?php echo ((is_array($_tmp=$this->_tpl_vars['ForumRecentArr'][$this->_sections['latestIndex']['index']]['topic_title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 36, "...", true) : smarty_modifier_truncate($_tmp, 36, "...", true)); ?>
</div>
                                        <div style="float: right; color: #666;"><?php echo $this->_tpl_vars['ForumRecentArr'][$this->_sections['latestIndex']['index']]['topic_views']; ?>
 views</div>
                                        <div class="clear"></div>
                                    </a>
                                    <?php endfor; endif; ?> 
				
				</div>
			</div>
			<div class="clear"></div>
		</div>
		
    </div>
		<?php echo '
			<script>
				function showCoachDetails(coach_id){

					if(jQuery("#"+coach_id).is(\':visible\')){
						return;
					}
					var i = 0;
					jQuery.each(jQuery(\'.coaches\'), function() {
						if(jQuery(this).is(\':visible\')){
							jQuery(this).fadeOut(\'fast\', function(){jQuery("#"+coach_id).fadeIn(\'fast\');});
							return false;
						}
						i++;
						
				   });
				   if(i == jQuery(\'.coaches\').length)
						jQuery("#"+coach_id).fadeIn(\'slow\');
				 
				}
			</script>
		'; ?>

 </div>  
<script language="javascript">
       changePageTitle();
</script>
	 
 