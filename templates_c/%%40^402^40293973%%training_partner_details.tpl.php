<?php /* Smarty version 2.6.16, created on 2013-04-20 20:14:20
         compiled from training_partner_details.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'nl2br', 'training_partner_details.tpl', 280, false),)), $this); ?>
<?php echo '

<link type="text/css" rel="stylesheet" href="style/lightbox-form.css">
<script src="js/lightbox-form.js" type="text/javascript"></script>
<script language="javascript" src="js/contact_us.js"></script>
<link type="text/css" href="jss/tipTip.css"  rel="stylesheet"  />
<script src="jss/jquery.tipTip.js" type="text/javascript"></script>


<script language="javascript">
jQuery(function(){
    jQuery(".someClass").tipTip();
});

function CheckFields(formID,NumRatingCat){
    var r_comment = document.getElementById(\'review_comment\').value;
    var NumRatingCat = NumRatingCat;	 	 	 	 
    var training_partner_id = document.getElementById(\'training_partner_id\').value;
    var reviewed_by = document.getElementById(\'logged_user\').value;
    if(document.getElementById(\'review_comment\').value==\'\'){
        jQuery(\'#review_comment\').css(\'border\',\'1px solid #FF0000\');
        return false;
    }
    else 
        jQuery(\'#review_comment\').css(\'border\',\'1px solid #ccc\');

    var frmID=\'#\'+formID;
	
    var params ={
        \'action\': \'sendreview\',
        \'NumCoachRatingCat\': NumRatingCat,
        \'IsProcess\': \'Y\',
        \'reviewed_by\': reviewed_by
    };

    var paramsObj = jQuery(frmID).serializeArray();
    jQuery.each(paramsObj, function(i, field){
        params[field.name] = field.value;
    });
	
   if(r_comment!=\'\'){
        jQuery.ajax({
             type: "POST",
             url: \'training_partner_details.php?training_partner_id=\'+training_partner_id,
             data: params,
             dataType : \'text\',
             success: function(data){
                 jQuery(\'.signin-message\').fadeIn(0);
                 jQuery(\'#msgbox\').fadeIn(0);
                 jQuery(\'#msgbox\').html(\'Review posted successfully.\');
                 jQuery(\'.signin-message\').fadeOut(5000);
                 jQuery(\'#paging\').html(data);
             }
         });
         document.getElementById("review_comment").value=\'\';
         document.getElementById(\'serialStar\').value=\'\';
         jQuery("#reviewwindow").fadeOut("fast", function(){jQuery(\'#overlay\').fadeOut();});
     }
  }		

function closereviewbox(){
    document.getElementById("review_comment").value=\'\';
    document.getElementById("serialStar").value=\'\';
    jQuery(\'#review_comment\').css(\'border\',\'1px solid #ccc\');
    jQuery("#reviewwindow").fadeOut("fast", function(){jQuery(\'#overlay\').fadeOut();});
}

</script>  
'; ?>

<?php if ($this->_tpl_vars['IsProcess'] != 'Y'): ?>
<div class="right-panel">
    <div class="findcoach-title" style="margin-bottom: 5px;">
        <div class="title" style="float: left;padding: 0;border: none;">
            <a style="text-decoration: none;" href="find_training_partner.php">Find Training Partner</a>
        </div>

        <div class="breadcrumb"><?php echo $this->_tpl_vars['tpArr']['username']; ?>
</div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div class="game-list-block">
	  
    <div class="game-image-block">
	<?php if ($this->_tpl_vars['tpArr']['photo'] != ''): ?>
        <img src="uploaded/user_images/thumbs/big_<?php echo $this->_tpl_vars['tpArr']['photo']; ?>
" alt="Training Partner" border="0" />
	<?php else: ?>
	<img src="images/coach_thumb.jpg" border="0" /> 
	<?php endif; ?> 
    </div> 
		    
    <div class="game-details-block">
        <div class="score_title listcoachname" data-id="<?php echo $this->_tpl_vars['tpArr']['user_id']; ?>
" >
            <h4 class="gameshead">
                <a class="<?php echo $this->_tpl_vars['online_status']; ?>
" title="<?php echo $this->_tpl_vars['online_status']; ?>
"></a>
                <?php echo $this->_tpl_vars['tpArr']['username']; ?>
 
                <a id="<?php echo $this->_tpl_vars['tpArr']['user_id']; ?>
_friendStar" class="<?php if ($this->_tpl_vars['isfriend'] == 'y'): ?>star_friend<?php else: ?>star_not_friend<?php endif; ?>" title="<?php if ($this->_tpl_vars['isfriend'] == 'y'): ?>in your friend list<?php else: ?>not your friend list<?php endif; ?>"></a>
            </h4>
            <?php if ($_SESSION['user_id'] != '' && $_SESSION['user_id'] != $this->_tpl_vars['tpArr']['user_id']): ?>
            <div class="addAsFriendWindow" id="addAsFriendWindow_<?php echo $this->_tpl_vars['tpArr']['user_id']; ?>
">
                <a class="addAsFriendLink" href="javascript:;" onclick="openChatTab(event,'<?php echo $this->_tpl_vars['tpArr']['user_id']; ?>
','<?php echo $this->_tpl_vars['tpArr']['username']; ?>
')">Message</a><br/>
                <?php if ($this->_tpl_vars['isfriend'] == 'n'): ?>
                <a class="addAsFriendLink addFriend" href="javascript:;" onclick="addAsFriend(event,'<?php echo $this->_tpl_vars['tpArr']['user_id']; ?>
','<?php echo $this->_tpl_vars['tpArr']['username']; ?>
',true)">Add as friend</a>
                <?php else: ?>
                <a class="addAsFriendLink removeFriend" href="javascript:;" onclick="removeFriend(event,'<?php echo $this->_tpl_vars['tpArr']['user_id']; ?>
','<?php echo $this->_tpl_vars['tpArr']['username']; ?>
',true)">Remove friend</a>
                <?php endif; ?>
            </div>
            <?php endif; ?>
        </div>
    <div class="score_card">
        <div id="scoreCard">
            <ul>
                <?php if ($this->_tpl_vars['tpOverallArr']['overall_rating'] != 0): ?>
                <li id="Overall" class="tTip">
                    <span class="someClass" title="Would you train with this partner again?">
                    <?php echo $this->_tpl_vars['tpOverallArr']['overall_rating']; ?>

                    <span>Overall</span>
                    </span>
                </li>
                <?php endif; ?>
                <?php unset($this->_sections['k']);
$this->_sections['k']['name'] = 'k';
$this->_sections['k']['loop'] = is_array($_loop=$this->_tpl_vars['CatIndividualRec']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['k']['show'] = true;
$this->_sections['k']['max'] = $this->_sections['k']['loop'];
$this->_sections['k']['step'] = 1;
$this->_sections['k']['start'] = $this->_sections['k']['step'] > 0 ? 0 : $this->_sections['k']['loop']-1;
if ($this->_sections['k']['show']) {
    $this->_sections['k']['total'] = $this->_sections['k']['loop'];
    if ($this->_sections['k']['total'] == 0)
        $this->_sections['k']['show'] = false;
} else
    $this->_sections['k']['total'] = 0;
if ($this->_sections['k']['show']):

            for ($this->_sections['k']['index'] = $this->_sections['k']['start'], $this->_sections['k']['iteration'] = 1;
                 $this->_sections['k']['iteration'] <= $this->_sections['k']['total'];
                 $this->_sections['k']['index'] += $this->_sections['k']['step'], $this->_sections['k']['iteration']++):
$this->_sections['k']['rownum'] = $this->_sections['k']['iteration'];
$this->_sections['k']['index_prev'] = $this->_sections['k']['index'] - $this->_sections['k']['step'];
$this->_sections['k']['index_next'] = $this->_sections['k']['index'] + $this->_sections['k']['step'];
$this->_sections['k']['first']      = ($this->_sections['k']['iteration'] == 1);
$this->_sections['k']['last']       = ($this->_sections['k']['iteration'] == $this->_sections['k']['total']);
?>
                <li id="$CatIndividualRec[k].rating_category" class="tTip">
                   <span class="someClass" title="<?php echo $this->_tpl_vars['CatIndividualRec'][$this->_sections['k']['index']]['rating_category_tooltip']; ?>
">
                   <?php echo $this->_tpl_vars['CatIndividualRec'][$this->_sections['k']['index']]['rating']; ?>

                   <span><?php echo $this->_tpl_vars['CatIndividualRec'][$this->_sections['k']['index']]['rating_category']; ?>
</span>
                   </span>
                </li>   
                <?php endfor; endif; ?> 
                
                
             </ul>
     </div>
		      </div>
            <br />
            <p class="clear"></p>
			<div class="games-desc partner_game_details" style="margin-top: 10px;">
				<dl>
                                    <dt>Language:</dt>
                                    <dd><?php if ($this->_tpl_vars['tpArr']['language'] != ''):  echo $this->_tpl_vars['tpArr']['language'];  else: ?>N/A<?php endif; ?></dd>
				</dl>
				<?php if ($this->_tpl_vars['tpArr']['avail_local'] == 'Y'): ?>
				<dl>
                                    <dt>City (Local meet-up):</dt>
                                    <dd><?php if ($this->_tpl_vars['tpArr']['availability_city'] != ''):  echo $this->_tpl_vars['tpArr']['availability_city'];  else: ?>N/A<?php endif; ?></dd>
				</dl>
				<?php endif; ?>
				<dl>
                                    <dt>Availability:</dt>
                                    <dd><?php if ($this->_tpl_vars['tpArr']['availability_type'] != ''):  echo $this->_tpl_vars['tpArr']['availability_type'];  else: ?>N/A<?php endif; ?></dd>
				</dl> 
				<dl>
                                    <dt>Introduction:</dt>
                                    <dd><div><?php if ($this->_tpl_vars['tpOverallArr']['about'] != ''):  echo $this->_tpl_vars['tpOverallArr']['about'];  else: ?>N/A<?php endif; ?></div></dd>
				</dl>
			</div>
                        <?php unset($this->_sections['game']);
$this->_sections['game']['name'] = 'game';
$this->_sections['game']['loop'] = is_array($_loop=$this->_tpl_vars['tpgameArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['game']['show'] = true;
$this->_sections['game']['max'] = $this->_sections['game']['loop'];
$this->_sections['game']['step'] = 1;
$this->_sections['game']['start'] = $this->_sections['game']['step'] > 0 ? 0 : $this->_sections['game']['loop']-1;
if ($this->_sections['game']['show']) {
    $this->_sections['game']['total'] = $this->_sections['game']['loop'];
    if ($this->_sections['game']['total'] == 0)
        $this->_sections['game']['show'] = false;
} else
    $this->_sections['game']['total'] = 0;
if ($this->_sections['game']['show']):

            for ($this->_sections['game']['index'] = $this->_sections['game']['start'], $this->_sections['game']['iteration'] = 1;
                 $this->_sections['game']['iteration'] <= $this->_sections['game']['total'];
                 $this->_sections['game']['index'] += $this->_sections['game']['step'], $this->_sections['game']['iteration']++):
$this->_sections['game']['rownum'] = $this->_sections['game']['iteration'];
$this->_sections['game']['index_prev'] = $this->_sections['game']['index'] - $this->_sections['game']['step'];
$this->_sections['game']['index_next'] = $this->_sections['game']['index'] + $this->_sections['game']['step'];
$this->_sections['game']['first']      = ($this->_sections['game']['iteration'] == 1);
$this->_sections['game']['last']       = ($this->_sections['game']['iteration'] == $this->_sections['game']['total']);
?>
			<div class="games-desc partner_game_details" style="margin-top: 15px;">
                            <h2><?php echo $this->_tpl_vars['tpgameArr'][$this->_sections['game']['index']]['game_name']; ?>
</h2>
                            <div style="margin-left: 15px;">
                                <?php unset($this->_sections['categoryIndex']);
$this->_sections['categoryIndex']['name'] = 'categoryIndex';
$this->_sections['categoryIndex']['loop'] = is_array($_loop=$this->_tpl_vars['tpgameArr'][$this->_sections['game']['index']]['categoriesArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['categoryIndex']['show'] = true;
$this->_sections['categoryIndex']['max'] = $this->_sections['categoryIndex']['loop'];
$this->_sections['categoryIndex']['step'] = 1;
$this->_sections['categoryIndex']['start'] = $this->_sections['categoryIndex']['step'] > 0 ? 0 : $this->_sections['categoryIndex']['loop']-1;
if ($this->_sections['categoryIndex']['show']) {
    $this->_sections['categoryIndex']['total'] = $this->_sections['categoryIndex']['loop'];
    if ($this->_sections['categoryIndex']['total'] == 0)
        $this->_sections['categoryIndex']['show'] = false;
} else
    $this->_sections['categoryIndex']['total'] = 0;
if ($this->_sections['categoryIndex']['show']):

            for ($this->_sections['categoryIndex']['index'] = $this->_sections['categoryIndex']['start'], $this->_sections['categoryIndex']['iteration'] = 1;
                 $this->_sections['categoryIndex']['iteration'] <= $this->_sections['categoryIndex']['total'];
                 $this->_sections['categoryIndex']['index'] += $this->_sections['categoryIndex']['step'], $this->_sections['categoryIndex']['iteration']++):
$this->_sections['categoryIndex']['rownum'] = $this->_sections['categoryIndex']['iteration'];
$this->_sections['categoryIndex']['index_prev'] = $this->_sections['categoryIndex']['index'] - $this->_sections['categoryIndex']['step'];
$this->_sections['categoryIndex']['index_next'] = $this->_sections['categoryIndex']['index'] + $this->_sections['categoryIndex']['step'];
$this->_sections['categoryIndex']['first']      = ($this->_sections['categoryIndex']['iteration'] == 1);
$this->_sections['categoryIndex']['last']       = ($this->_sections['categoryIndex']['iteration'] == $this->_sections['categoryIndex']['total']);
?>
                                <dl>
                                    <dt><?php echo $this->_tpl_vars['tpgameArr'][$this->_sections['game']['index']]['categoriesArr'][$this->_sections['categoryIndex']['index']]['category_name']; ?>
:</dt>
                                    <dd><?php if ($this->_tpl_vars['tpgameArr'][$this->_sections['game']['index']]['categoriesArr'][$this->_sections['categoryIndex']['index']]['properties'] != ''):  echo $this->_tpl_vars['tpgameArr'][$this->_sections['game']['index']]['categoriesArr'][$this->_sections['categoryIndex']['index']]['properties'];  else: ?>&nbsp;<?php endif; ?></dd>
                                </dl>
                                <?php endfor; endif; ?>

                                <dl>
                                        <dt>Peak Hours:</dt>
                                        <dd><?php if ($this->_tpl_vars['tpgameArr'][$this->_sections['game']['index']]['peak_hours'] != ''): ?> <?php echo $this->_tpl_vars['tpgameArr'][$this->_sections['game']['index']]['peak_hours'];  else: ?>N/A<?php endif; ?></dd>
                                </dl> 

                                <dl>
                                        <dt>Rating:</dt>
                                        <dd><?php if ($this->_tpl_vars['tpgameArr'][$this->_sections['game']['index']]['rating'] != ''): ?> <?php echo $this->_tpl_vars['tpgameArr'][$this->_sections['game']['index']]['rating']; ?>
 <?php else: ?> N/A <?php endif; ?></dd>
                                </dl> 
                                <dl>
                                        <dt>Experience:</dt>
                                        <dd><div><?php if ($this->_tpl_vars['tpgameArr'][$this->_sections['game']['index']]['experience'] != ''):  echo $this->_tpl_vars['tpgameArr'][$this->_sections['game']['index']]['experience'];  else: ?>N/A<?php endif; ?></div></dd>
                                </dl> 
                                <dl>
                                        <dt>Need Improvement In:</dt>
                                        <dd><div><?php if ($this->_tpl_vars['tpgameArr'][$this->_sections['game']['index']]['need_improvement'] != ''):  echo $this->_tpl_vars['tpgameArr'][$this->_sections['game']['index']]['need_improvement'];  else: ?>N/A<?php endif; ?></div></dd>
                                </dl> 
                            </div>
			</div>
			<?php endfor; endif; ?>
            <div class="clear" style="padding-top:10px;"></div>
			<?php if ($_SESSION['user_id'] != '' && $_SESSION['user_id'] != $this->_tpl_vars['tpArr']['user_id']): ?>
			<div style="margin-top:10px;float: right;">
				<a style="margin-top:10px;float: right;" class="button" href="javascript:void(0);" onClick="openReview('Review and Rating for <b><?php echo $this->_tpl_vars['tpArr']['username']; ?>
</b>')"><span>Review</span></a>
				<a style="margin-top:10px;float: right;" class="button" href="javascript:void(0);" onClick="openChatTab(event,'<?php echo $this->_tpl_vars['tpArr']['user_id']; ?>
','<?php echo $this->_tpl_vars['tpArr']['username']; ?>
','<?php echo $this->_tpl_vars['isfriend']; ?>
')"><span>Message</span></a>
			</div>
			
			  <br /> <div class="clear"></div>
		          
			  
			  
		   <?php endif; ?>
         </div>
         <div class="clear"></div>
      </div>
	  
	  <?php if ($this->_tpl_vars['Numvideo'] > 99999): ?>
      <div class="game-list">
         <h2 title="Video">Videos</h2>
         <div class="clear"></div>
		 <?php unset($this->_sections['video']);
$this->_sections['video']['name'] = 'video';
$this->_sections['video']['loop'] = is_array($_loop=$this->_tpl_vars['tpvideoArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['video']['show'] = true;
$this->_sections['video']['max'] = $this->_sections['video']['loop'];
$this->_sections['video']['step'] = 1;
$this->_sections['video']['start'] = $this->_sections['video']['step'] > 0 ? 0 : $this->_sections['video']['loop']-1;
if ($this->_sections['video']['show']) {
    $this->_sections['video']['total'] = $this->_sections['video']['loop'];
    if ($this->_sections['video']['total'] == 0)
        $this->_sections['video']['show'] = false;
} else
    $this->_sections['video']['total'] = 0;
if ($this->_sections['video']['show']):

            for ($this->_sections['video']['index'] = $this->_sections['video']['start'], $this->_sections['video']['iteration'] = 1;
                 $this->_sections['video']['iteration'] <= $this->_sections['video']['total'];
                 $this->_sections['video']['index'] += $this->_sections['video']['step'], $this->_sections['video']['iteration']++):
$this->_sections['video']['rownum'] = $this->_sections['video']['iteration'];
$this->_sections['video']['index_prev'] = $this->_sections['video']['index'] - $this->_sections['video']['step'];
$this->_sections['video']['index_next'] = $this->_sections['video']['index'] + $this->_sections['video']['step'];
$this->_sections['video']['first']      = ($this->_sections['video']['iteration'] == 1);
$this->_sections['video']['last']       = ($this->_sections['video']['iteration'] == $this->_sections['video']['total']);
?>
		 
		 <?php $this->assign('i', $this->_sections['video']['index']+1); ?>
          <div <?php if ($this->_tpl_vars['i']%3 == '0'): ?> class="games-replay-last" <?php else: ?> class="games-replay" <?php endif; ?>>
          
          		<div class="replay-content">
			   <a href="videodetails.php?video_id=<?php echo $this->_tpl_vars['tpvideoArr'][$this->_sections['video']['index']]['video_id']; ?>
">
			   <img src="uploaded/video_images/thumbs/mid_<?php echo $this->_tpl_vars['tpvideoArr'][$this->_sections['video']['index']]['video_image']; ?>
" alt="" title="<?php echo $this->_tpl_vars['tpvideoArr'][$this->_sections['video']['index']]['video_title']; ?>
" border="0"  /></a>
			   </div>
			   <div class="replay-content">
				  <div class="replay-title"><a href="videodetails.php?video_id=<?php echo $this->_tpl_vars['tpvideoArr'][$this->_sections['video']['index']]['video_id']; ?>
"><?php echo $this->_tpl_vars['tpvideoArr'][$this->_sections['video']['index']]['video_title']; ?>
</a><br />
                  <span class="date"><?php echo $this->_tpl_vars['tpvideoArr'][$this->_sections['video']['index']]['video_date']; ?>
<br />
				  <?php if ($this->_tpl_vars['tpvideoArr'][$this->_sections['video']['index']]['time_length'] != ''):  echo $this->_tpl_vars['tpvideoArr'][$this->_sections['video']['index']]['time_length']; ?>
 min<?php else: ?> N/A <?php endif; ?>
				  </span>
                                </div>
				  <div class="game-rating">
				  
                                  
                                  <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                    <tr>
                                        <td><b>Likes:</b></td>
                                        <td><?php echo $this->_tpl_vars['tpvideoArr'][$this->_sections['video']['index']]['numlike']; ?>
</td>
                                    </tr>
                                    <tr>
                                        <td><b>Disikes:</b></td>
                                        <td><?php echo $this->_tpl_vars['tpvideoArr'][$this->_sections['video']['index']]['numunlike']; ?>
</td>
                                    </tr>
                                    <tr>
                                        <td><b>Views:</b></td>
                                        <td><?php echo $this->_tpl_vars['tpvideoArr'][$this->_sections['video']['index']]['view_count']; ?>
</td>
                                    </tr>
                                </table>
                                  
                                  
				  </div>
                  <div class="replay-title" style="padding-top:0px; width:90%"><span class="date">Game : <em><?php echo $this->_tpl_vars['tpvideoArr'][$this->_sections['video']['index']]['game']; ?>
</em></span></div>
			   </div>
				  
          </div>
         
		  <?php if ($this->_tpl_vars['i']%3 == '0'): ?>
            <div class="clear"></div>
	      <?php endif; ?>		
		 <?php endfor; endif; ?>
		<div class="clear"></div> 
      </div>
	  <?php endif;  endif; ?>
      <div class="clear"></div>
      
    <div class="signin-message" style="display:none;">
        <div id="msgbox" style="color:#0077BC;"></div>
    </div>
    <div id="paging"> 
    <div id="review">
    <div class="review-list">
        <h2 title="Review">Reviews</h2>
	<?php if ($this->_tpl_vars['NumReview'] > 0): ?>
        <div class="clear"></div>
        <?php unset($this->_sections['rrow']);
$this->_sections['rrow']['name'] = 'rrow';
$this->_sections['rrow']['loop'] = is_array($_loop=$this->_tpl_vars['UserReview']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rrow']['show'] = true;
$this->_sections['rrow']['max'] = $this->_sections['rrow']['loop'];
$this->_sections['rrow']['step'] = 1;
$this->_sections['rrow']['start'] = $this->_sections['rrow']['step'] > 0 ? 0 : $this->_sections['rrow']['loop']-1;
if ($this->_sections['rrow']['show']) {
    $this->_sections['rrow']['total'] = $this->_sections['rrow']['loop'];
    if ($this->_sections['rrow']['total'] == 0)
        $this->_sections['rrow']['show'] = false;
} else
    $this->_sections['rrow']['total'] = 0;
if ($this->_sections['rrow']['show']):

            for ($this->_sections['rrow']['index'] = $this->_sections['rrow']['start'], $this->_sections['rrow']['iteration'] = 1;
                 $this->_sections['rrow']['iteration'] <= $this->_sections['rrow']['total'];
                 $this->_sections['rrow']['index'] += $this->_sections['rrow']['step'], $this->_sections['rrow']['iteration']++):
$this->_sections['rrow']['rownum'] = $this->_sections['rrow']['iteration'];
$this->_sections['rrow']['index_prev'] = $this->_sections['rrow']['index'] - $this->_sections['rrow']['step'];
$this->_sections['rrow']['index_next'] = $this->_sections['rrow']['index'] + $this->_sections['rrow']['step'];
$this->_sections['rrow']['first']      = ($this->_sections['rrow']['iteration'] == 1);
$this->_sections['rrow']['last']       = ($this->_sections['rrow']['iteration'] == $this->_sections['rrow']['total']);
?>
        <div class="review-block">
             
			 <div class="review-left">
			 <a href="<?php echo $this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['userlink']; ?>
">
			  <img src="<?php echo $this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['img']; ?>
" height="92" width="105"  alt="<?php echo $this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['review_by']; ?>
" border="0" />
			 </a>
			 </div>
			 <div class="review-right">
			 <div class="review-sub-title">
             
             	<div class="review_thumb_block">
				 <h4 class="reviewshead">Reviewed By : <a href="<?php echo $this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['userlink']; ?>
"><?php echo $this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['review_by']; ?>
</a></h4><br/><br />
	 			 
				 <span><em>Posted on</em></span> : <?php echo $this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['r_date']; ?>
<br /><br />
				 <?php if ($this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['review_comment'] != ''): ?>
				 <p><i>Review</i>:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['review_comment'])) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</p><br />
				 <?php endif; ?>
				</div>
                
                <div class="review_card_block">
                 <!-- Score Area -->
                <div id="scoreCard">
                    <ul>
			<?php if ($this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['overall_r'] != 0): ?>
                        <li id="Overall" class="tTip">
                            <span class="someClass" title="Would you train with this partner again?">
                            <?php echo $this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['overall_r']; ?>

                            <span>Overall</span>
                            </span>
                        </li>
                        <?php endif; ?>
                        <?php unset($this->_sections['k']);
$this->_sections['k']['name'] = 'k';
$this->_sections['k']['loop'] = is_array($_loop=$this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['individual']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['k']['show'] = true;
$this->_sections['k']['max'] = $this->_sections['k']['loop'];
$this->_sections['k']['step'] = 1;
$this->_sections['k']['start'] = $this->_sections['k']['step'] > 0 ? 0 : $this->_sections['k']['loop']-1;
if ($this->_sections['k']['show']) {
    $this->_sections['k']['total'] = $this->_sections['k']['loop'];
    if ($this->_sections['k']['total'] == 0)
        $this->_sections['k']['show'] = false;
} else
    $this->_sections['k']['total'] = 0;
if ($this->_sections['k']['show']):

            for ($this->_sections['k']['index'] = $this->_sections['k']['start'], $this->_sections['k']['iteration'] = 1;
                 $this->_sections['k']['iteration'] <= $this->_sections['k']['total'];
                 $this->_sections['k']['index'] += $this->_sections['k']['step'], $this->_sections['k']['iteration']++):
$this->_sections['k']['rownum'] = $this->_sections['k']['iteration'];
$this->_sections['k']['index_prev'] = $this->_sections['k']['index'] - $this->_sections['k']['step'];
$this->_sections['k']['index_next'] = $this->_sections['k']['index'] + $this->_sections['k']['step'];
$this->_sections['k']['first']      = ($this->_sections['k']['iteration'] == 1);
$this->_sections['k']['last']       = ($this->_sections['k']['iteration'] == $this->_sections['k']['total']);
?>
                         <li id="$UserReview[rrow].individual[k].rating_category" class="tTip">
                            <span class="someClass" title="<?php echo $this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['individual'][$this->_sections['k']['index']]['rating_category_tooltip']; ?>
">
                            <?php echo $this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['individual'][$this->_sections['k']['index']]['rating']; ?>

                            <span><?php echo $this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['individual'][$this->_sections['k']['index']]['rating_category']; ?>
</span>
                            </span>
                        </li>   
                         <?php endfor; endif; ?>   
                       
                    </ul>
                </div>
                </div>
                
                <div class="clear"></div>
				<!-- Score Area END -->
                 
			 </div>
			 <!--p class="post"><span>Posted on</span> : <?php echo $this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['r_date']; ?>
</p-->
			 </div>
			 <div class="clear"></div>
		</div>
		 
	    <?php endfor; endif; ?>
      
	  <?php if ($this->_tpl_vars['pagination_arr'][1]): ?>
      <div class="clear"></div>
      <div class="pagin">
         <ul>
            <?php echo $this->_tpl_vars['pagination_arr'][1]; ?>

         </ul>
      </div>
	  <?php endif; ?>
	  <?php else: ?>
	   No reviews
	<?php endif; ?> 
	</div>
   </div>	
  </div>	 
  </div> 

<div class="modalpopup" id="reviewwindow">
    <a class="modalpopup-close-btn" href="javascript:;"></a>
    <div id="boxtitle" style="font-weight: normal;" class="title"></div>
    <div class="review">
    <div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:5px;"><?php if ($this->_tpl_vars['ermsg'] != ''): ?> <?php echo $this->_tpl_vars['ermsg'];  endif; ?> </div>
    <form name="contact" id="Rating_Coach"  method='post' action="" onsubmit="return false;">
        <input type="hidden" name="training_partner_id" id="training_partner_id" value="<?php echo $this->_tpl_vars['training_partner_id']; ?>
" />
        <input type="hidden" name="logged_user" id="logged_user" value="<?php echo $this->_tpl_vars['logged_user']; ?>
" />
        <input type="hidden" name="user_type_id" id="user_type_id" value="3" />
        <table cellpadding="0" cellspacing="10">
            <tr>
                <td class="">
                    <span class="someClass" title="Would you train with this partner again?">Overall :</span> 
                </td>
                <td>
                    <div class="">
                        <table cellpadding="5" cellspacing="5">
                            <tr valign="middle">
                                <td>1&nbsp;<input type="radio" value="1" name="Rating_overall" id="serialStar" title="Poor"></td>
                                <td>2&nbsp;<input type="radio" value="2" name="Rating_overall" id="serialStar" title="Ok"></td>
                                <td>3&nbsp;<input type="radio" value="3" name="Rating_overall" id="serialStar" title="Good"></td>
                                <td>4&nbsp;<input type="radio" value="4" name="Rating_overall" id="serialStar" title="Better"></td>
                                <td>5&nbsp;<input type="radio" value="5" name="Rating_overall" id="serialStar" title="Awesome"></td>
                            </tr>
                        </table>
                        	 
                    </div>  
                </td>
            </tr>
            <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['RatingCatArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
            <tr>
                <td class="">
                    <span class="someClass" title="<?php echo $this->_tpl_vars['RatingCatArr'][$this->_sections['i']['index']]['rating_category_tooltip']; ?>
"><?php echo $this->_tpl_vars['RatingCatArr'][$this->_sections['i']['index']]['rating_category']; ?>
:</span>
                </td>
                <td colspan="2"> 
                    <div class="">
                        <table cellpadding="5" cellspacing="5">
                            <tr>
                                <td>1&nbsp;<input type="radio" value="1:<?php echo $this->_tpl_vars['RatingCatArr'][$this->_sections['i']['index']]['rcat_id']; ?>
" name="Rating_<?php echo $this->_sections['i']['index']; ?>
" id="serialStar" title="Poor"></td>
                                <td>2&nbsp;<input type="radio" value="2:<?php echo $this->_tpl_vars['RatingCatArr'][$this->_sections['i']['index']]['rcat_id']; ?>
" name="Rating_<?php echo $this->_sections['i']['index']; ?>
" id="serialStar" title="Ok"></td>
                                <td>3&nbsp;<input type="radio" value="3:<?php echo $this->_tpl_vars['RatingCatArr'][$this->_sections['i']['index']]['rcat_id']; ?>
" name="Rating_<?php echo $this->_sections['i']['index']; ?>
" id="serialStar" title="Good"></td>
                                <td>4&nbsp;<input type="radio" value="4:<?php echo $this->_tpl_vars['RatingCatArr'][$this->_sections['i']['index']]['rcat_id']; ?>
" name="Rating_<?php echo $this->_sections['i']['index']; ?>
" id="serialStar" title="Better"></td>
                                <td>5&nbsp;<input type="radio" value="5:<?php echo $this->_tpl_vars['RatingCatArr'][$this->_sections['i']['index']]['rcat_id']; ?>
" name="Rating_<?php echo $this->_sections['i']['index']; ?>
" id="serialStar" title="Awesome"></td>
                            </tr>
                        </table>
                        
                    </div>
                </td>
            </tr>
            <?php endfor; endif; ?>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td width="20%" valign="top">Review :</td>
                <td width="54%"><textarea style="width: 300px;" name="review_comment" id="review_comment" cols="15" rows="5" ><?php echo $this->_tpl_vars['Formval']['user_about']; ?>
</textarea></td>
            </tr>

            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" align="center">    
                    <input name="submit" class="button" value="Submit" type="submit" onclick="return CheckFields('Rating_Coach',<?php echo $this->_tpl_vars['NumRatingCat']; ?>
)" />
                    <input name="" class="button" value="Cancel" type="button" onClick="closereviewbox()" />		
                </td>
            </tr>
        </table>
    </form>
    </div> 
</div>   
<script language="javascript">
    changePageTitle('Training partner - <?php echo $this->_tpl_vars['tpArr']['username']; ?>
 ');
</script>