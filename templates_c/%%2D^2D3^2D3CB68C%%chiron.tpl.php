<?php /* Smarty version 2.6.16, created on 2013-03-28 17:08:24
         compiled from chiron.tpl */ ?>
<!DOCTYPE html>
	<html>
	<head>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
                <html xmlns="http://www.w3.org/1999/xhtml">
		<?php if ($_SESSION['user_id'] == ''): ?>
		<script type="text/javascript">
		  	window.location = "/index.php";
		</script> 
		<?php endif; ?>
		<script type="text/javascript">
			
			var dest = '<?php echo $this->_tpl_vars['dest']; ?>
';
                       // alert(dest);
			if(dest != '<?php echo $this->_tpl_vars['currentpage']; ?>
'){
				window.location = "/chiron.php?dest=<?php echo $this->_tpl_vars['currentpage']; ?>
";
			}
			
		</script> 
                        
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                
                <title><?php if ($this->_tpl_vars['pagetitle'] != ''):  echo $this->_tpl_vars['pagetitle']; ?>
 - <?php endif; ?> Chiron Gaming</title>
		<link rel="icon" href="images/icon_32.png" sizes="32x32">
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		<link href="style/style.css" rel="stylesheet" type="text/css" />
                <link href="style/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
                <link href="style/jquery.pnotify.default.css" rel="stylesheet" type="text/css" />
                <link href="style/jquery.pnotify.default.icons.css" media="all" rel="stylesheet" type="text/css" />
		<link type="text/css" href="style/notification.css" rel="stylesheet" />
                <link href="style/notify.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jss/jquery-1.8.0.min.js"></script>
		<script type="text/javascript" src="jss/jquery-ui-1.8.23.customDEV.js"></script> 
                <script type="text/javascript" src="jss/jquery.pnotify.js"></script>
		<script type="text/javascript" src="jss/jquery.caret.js"></script>
		<script type="text/javascript" src="jss/jquery.nicescroll.js"></script>
		<link type="text/css" href="jss/tipTip.css"  rel="stylesheet"  />
                <script src="jss/jquery.tipTip.js" type="text/javascript"></script>
		<link type="text/css" href="style/jquery.jscrollpane.css" rel="stylesheet" media="all" />
		<script type="text/javascript" src="jss/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="jss/jquery.jscrollpane.min.js"></script>
		<script type="text/javascript" src="jss/notifications.js"></script>
		<script type="text/javascript" src="jss/ping.js"></script>
                <script type="text/javascript" src="jss/jquery-notify.js"></script>
		<script type="text/javascript" src="jss/scripts.js"></script>
		<link rel="stylesheet" href="/freichat/client/jquery/freichat_themes/freichatcss.php" type="text/css">              
                    
	</head>
	<body style="background: none;overflow: hidden">
		<div class="loading" id="pageloading" style="margin-left: -30px;margin-top: -50px;display: none;"></div>
		<div id="overlay"></div>
		<iframe name="chironWrapper" id="chironWrapper" frameborder="0" src="<?php echo $this->_tpl_vars['req']; ?>
" height="1" width="1" scrolling="auto"></iframe>
		<?php echo '
		<script type="text/javascript" >
//                    jQuery.pnotify.defaults.styling = "jqueryui";
//                    jQuery.pnotify.defaults.history = false;
                    jQuery(function(){
                        jQuery(".tip").tipTip({
                                            leftoffset: 110,
                                            defaultPosition: "top",
                                            left: 0,
                                            delay: 100,
                                            fadeOut: 1000,
                                            maxWidth: "px",
                                            edgeOffset: 10
                                        });
                    });
                    loadChiron();
                     
		</script>
		'; ?>

		<div id="bottombar" style=""> 
			<ul>
				<li id="freicontain_wrap3" class="freicontain_wrap">
					<a href="javascript:;" class="chat_button" id="chat_button3" style="display: none;"> 
					</a>
					<div class='freicontain freicontain3' id='freicontain3'></div>
				</li>
				<li id="freicontain_wrap2" class="freicontain_wrap">
					<a href="javascript:;" class="chat_button" id="chat_button2" style="display: none;"> 
					</a>
					<div class='freicontain freicontain2' id='freicontain2'></div>
				</li>
				<li id="freicontain_wrap1" class="freicontain_wrap">
					<a href="javascript:;" class="chat_button" id="chat_button1" style="display: none;"> 
					</a>
					<div class='freicontain freicontain1' id='freicontain1'></div>
				</li>
				<li id="freicontain_wrap0" class="freicontain_wrap">
					<a href="javascript:;" class="chat_button" id="chat_button0" style="display: none;"> 
					</a>
					<div class='freicontain freicontain0' id='freicontain0'></div>
				</li>
				<li><a href="javascript:;" onclick="notifications.showNotifications(event)" class="notifications_button"> Notifications <div class="notifications_quantity"></div> </a></li>
				<li class="players_li" id="players_li">
					<a href="javascript:;" class="players_button">Friends<div class="new_messages"></div>
					</a>
					<div class="players_list" id="players_list" style="">
						<div class="status_changer_title" >Friends</div> 
						<div style="" class="players_list_wrapper">
						
						</div>
					</div>
				</li>
					
				<li class="online_status_li">
					<a id="online_status_button" href="javascript:;" class="online_status <?php echo $this->_tpl_vars['online_status']; ?>
"> 
						<div class="">
						</div>
					</a> 
					<div class="online_status_changer" style="bottom: 35px;">
						<div class="status_changer_title" >Online Status</div>
						<div class="online status_changer" onclick="changeStatus(event,1);" >online</div>
						<div class="away status_changer" onclick="changeStatus(event,2);">away</div>
						<div class="busy status_changer" onclick="changeStatus(event,3);">busy</div>
						<div class="offline status_changer" onclick="changeStatus(event,0);">offline</div>
					</div>
				</li>
                                        <li class="tip" title="<?php echo $this->_tpl_vars['time_zone']; ?>
"><div class="nohover" style="width: 97px;" >[UTC <?php if ($this->_tpl_vars['tz'] > 0): ?>+<?php endif; ?> <?php if ($this->_tpl_vars['tz'] != 0):  echo $this->_tpl_vars['tz'];  endif; ?>]</div></li>
			</ul>
		</div>	
		<script type="text/javascript" language="javascipt" src="freichat/client/main.php?id=<?php echo $this->_tpl_vars['user_id']; ?>
&xhash=<?php echo $this->_tpl_vars['xhash']; ?>
&key=<?php echo $this->_tpl_vars['microtime']; ?>
"></script>
		<script type="text/javascript" language="javascipt" src="jss/jquery.phono.js"></script>
		<script type="text/javascript" language="javascipt" src="jss/phono_chat.js"></script>
		<script type="text/javascript" src="jss/bottomJs.js"></script>
        <?php echo '
        <script type="text/javascript">
            jQuery(window).resize(function() {
              loadChiron();
            });
        </script>
        '; ?>

	</body>
</html>