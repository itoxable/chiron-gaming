<?php /* Smarty version 2.6.16, created on 2013-03-01 11:19:18
         compiled from messages.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'messages.tpl', 46, false),)), $this); ?>
<div id="show">
		
	 <div class="clear"></div>
		<div align="right">
			<?php if ($this->_tpl_vars['is_coach'] == '0'): ?>
			<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['is_partner'] == '0'): ?>
			<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
			<?php endif; ?> 
		</div>
		<div class="content"> 
   		
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'user_tabs.tpl', 'smarty_include_vars' => array('active_tab' => 'message')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				
        <div class="clear"></div>
        <div class="tabular-content" style="height: 580px;">
			<div class="total-message">&nbsp;&nbsp;</div> 
			<div class="clear"></div> 
			
			<?php if ($_SESSION['user_id'] != ''): ?>
			<script type="text/javascript" src="jss/quiron_messages.js"></script> 
			<?php endif; ?>

			<div class="content-wrapper" style="position: relative;margin-left: 100px;">
                            <?php if ($this->_tpl_vars['messagesSize'] > 0): ?>
				<div class="messages_user_list" style="">
				<?php if ($this->_tpl_vars['usersSize'] > 0): ?>
	  				<?php unset($this->_sections['userIndex']);
$this->_sections['userIndex']['name'] = 'userIndex';
$this->_sections['userIndex']['loop'] = is_array($_loop=$this->_tpl_vars['usersArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['userIndex']['show'] = true;
$this->_sections['userIndex']['max'] = $this->_sections['userIndex']['loop'];
$this->_sections['userIndex']['step'] = 1;
$this->_sections['userIndex']['start'] = $this->_sections['userIndex']['step'] > 0 ? 0 : $this->_sections['userIndex']['loop']-1;
if ($this->_sections['userIndex']['show']) {
    $this->_sections['userIndex']['total'] = $this->_sections['userIndex']['loop'];
    if ($this->_sections['userIndex']['total'] == 0)
        $this->_sections['userIndex']['show'] = false;
} else
    $this->_sections['userIndex']['total'] = 0;
if ($this->_sections['userIndex']['show']):

            for ($this->_sections['userIndex']['index'] = $this->_sections['userIndex']['start'], $this->_sections['userIndex']['iteration'] = 1;
                 $this->_sections['userIndex']['iteration'] <= $this->_sections['userIndex']['total'];
                 $this->_sections['userIndex']['index'] += $this->_sections['userIndex']['step'], $this->_sections['userIndex']['iteration']++):
$this->_sections['userIndex']['rownum'] = $this->_sections['userIndex']['iteration'];
$this->_sections['userIndex']['index_prev'] = $this->_sections['userIndex']['index'] - $this->_sections['userIndex']['step'];
$this->_sections['userIndex']['index_next'] = $this->_sections['userIndex']['index'] + $this->_sections['userIndex']['step'];
$this->_sections['userIndex']['first']      = ($this->_sections['userIndex']['iteration'] == 1);
$this->_sections['userIndex']['last']       = ($this->_sections['userIndex']['iteration'] == $this->_sections['userIndex']['total']);
?>
					
						<div class="messages_user_wrap <?php if ($this->_sections['userIndex']['index'] == 0): ?>messages_user_wrap_selected<?php endif; ?>" style="<?php if ($this->_sections['userIndex']['index'] == 0): ?>border-top: 1px solid #CCC<?php endif; ?>" id="message_user_<?php echo $this->_tpl_vars['usersArr'][$this->_sections['userIndex']['index']]['user_id']; ?>
">
							<div class="messages_user_image">
								<img width="35px" height="35px" src="<?php if ($this->_tpl_vars['usersArr'][$this->_sections['userIndex']['index']]['photo']): ?>uploaded/user_images/thumbs/mid_<?php echo $this->_tpl_vars['usersArr'][$this->_sections['userIndex']['index']]['photo'];  else: ?>images/featured_img.jpg<?php endif; ?>" alt="" border="0"  />
							</div>
							<div class="messages_user_name">
								<?php echo $this->_tpl_vars['usersArr'][$this->_sections['userIndex']['index']]['username']; ?>

							</div>
						</div>
					<?php endfor; endif; ?>
				<?php endif; ?>
				</div>
				<?php if ($this->_tpl_vars['groupedMessagesSize'] > 0): ?>
					<?php $_from = $this->_tpl_vars['groupedMessages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['messagegroupname'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['messagegroupname']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['userid'] => $this->_tpl_vars['messagegroup']):
        $this->_foreach['messagegroupname']['iteration']++;
?>
					<div class="messages_list_wrap" style="<?php if (($this->_foreach['messagegroupname']['iteration'] <= 1)): ?>display: block<?php endif; ?>" id="messages_list_<?php echo ((is_array($_tmp=$this->_tpl_vars['userid'])) ? $this->_run_mod_handler('replace', true, $_tmp, '\'', '') : smarty_modifier_replace($_tmp, '\'', '')); ?>
">
						<div class="messages_list" id="inner_messages_list_<?php echo ((is_array($_tmp=$this->_tpl_vars['userid'])) ? $this->_run_mod_handler('replace', true, $_tmp, '\'', '') : smarty_modifier_replace($_tmp, '\'', '')); ?>
">
							<div style="" id="inner_messages_list_wrap_<?php echo ((is_array($_tmp=$this->_tpl_vars['userid'])) ? $this->_run_mod_handler('replace', true, $_tmp, '\'', '') : smarty_modifier_replace($_tmp, '\'', '')); ?>
">
							<?php $_from = $this->_tpl_vars['messagegroup']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['chave'] => $this->_tpl_vars['message']):
?>
								<div class="messages_wrap">
									<div class="messages_sender" >
										<?php echo $this->_tpl_vars['message']['from_name']; ?>
	
										<div class="messages_date" taux="<?php echo $this->_tpl_vars['message']['time']; ?>
">
											<?php echo $this->_tpl_vars['message']['sent']; ?>
				
										</div>
									</div>
									<div class="messages_content">
										<?php echo $this->_tpl_vars['message']['message']; ?>

									</div>
								</div>
							<?php endforeach; endif; unset($_from); ?>
							</div>
						</div>
						<div class="send_message">
        					<textarea rows="4" cols="50" class="send_message_text" toid="<?php echo ((is_array($_tmp=$this->_tpl_vars['userid'])) ? $this->_run_mod_handler('replace', true, $_tmp, '\'', '') : smarty_modifier_replace($_tmp, '\'', '')); ?>
" fromname="<?php echo $this->_tpl_vars['user_name']; ?>
"  id="send_message_<?php echo ((is_array($_tmp=$this->_tpl_vars['userid'])) ? $this->_run_mod_handler('replace', true, $_tmp, '\'', '') : smarty_modifier_replace($_tmp, '\'', '')); ?>
"></textarea>
							<div style="text-align: right">
								<span id="checkbox_for_<?php echo ((is_array($_tmp=$this->_tpl_vars['userid'])) ? $this->_run_mod_handler('replace', true, $_tmp, '\'', '') : smarty_modifier_replace($_tmp, '\'', '')); ?>
" class="chironcheckbox" onclick="changeClass(event,this)">Press Enter to send</span>
								<a href="javascript:;" class="button_message button" toid="<?php echo ((is_array($_tmp=$this->_tpl_vars['userid'])) ? $this->_run_mod_handler('replace', true, $_tmp, '\'', '') : smarty_modifier_replace($_tmp, '\'', '')); ?>
" fromname="<?php echo $this->_tpl_vars['user_name']; ?>
">Send</a>
							</div>	
						</div>	
					</div>
					
					<?php endforeach; endif; unset($_from); ?>
				<?php endif; ?>
                                <?php else: ?>
                            <div>No messages......</div>
                        <?php endif; ?>
			</div>
                        
		</div>
	</div>
</div>

<?php echo '
<script>

	jQuery(\'.messages_date\').each(function(index) {
		var date = jQuery(this);
		var time = date.attr(\'taux\');
		date.text(getSystemTimeForMessages(time));
	});

	function changeClass(event,element){
		if(jQuery(element).hasClass(\'selected\'))
			jQuery(element).removeClass(\'selected\');
		else
			jQuery(element).addClass(\'selected\');
	}
	jQuery(\'.messages_list\').each(function(index) {
		var jscroll = jQuery(this).jScrollPane({stickToBottom:true,maintainPosition:true});
		var api = jscroll.data(\'jsp\');
		api.scrollToPercentY(100);
	});
	jQuery(".messages_user_wrap").click(function(){
		var aux = this.id;
		var mid = aux.replace("message_user_","messages_list_");
                    
		var inner = \'#inner_\'+mid;
		mid = "#"+mid;
		if(jQuery(mid).is(\':visible\'))
			return;
			
		jQuery(".messages_user_wrap").removeClass(\'messages_user_wrap_selected\');
		jQuery(this).addClass(\'messages_user_wrap_selected\');
		
		jQuery(".messages_list_wrap").hide(\'fast\');
		jQuery(mid).show(\'fast\');
		
		var jscroll = jQuery(inner).jScrollPane({stickToBottom:true,maintainPosition:true});
		var api = jscroll.data(\'jsp\');
		api.scrollToPercentY(100);
                    
	});
	
	jQuery(\'.send_message_text\').keyup(function(e) {
		var id = jQuery(this).attr(\'toid\');
		if(jQuery("#checkbox_for_"+id).hasClass(\'selected\')){
			if (e.keyCode == 13) {
				var textid = "#send_message_"+id;
				sendMessage(jQuery(textid).val(),id, jQuery(this).attr(\'fromname\')); 
			}
		}
	});
	
	jQuery(\'.button_message\').click(function(e) {
		var id = "#send_message_"+jQuery(this).attr(\'toid\');
		sendMessage(jQuery(id).val(),jQuery(this).attr(\'toid\'), jQuery(this).attr(\'fromname\')); 
	});
        
        
        
	
</script>
'; ?>

<script language="javascript">
       window.messagetimer = setInterval('getlastMessages(\'<?php echo $this->_tpl_vars['lastId']; ?>
\')',10000);
       
       changePageTitle('Messages');
</script>