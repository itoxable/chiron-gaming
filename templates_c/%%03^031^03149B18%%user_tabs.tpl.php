<?php /* Smarty version 2.6.16, created on 2013-02-12 17:57:53
         compiled from user_tabs.tpl */ ?>

		<!-- tab -->
    	<ul class="tabflex">
       
			<li class="left-shadow">
				<?php if ($this->_tpl_vars['active_tab'] == 'schedule'): ?>
					<a href="#" class="active">
				<?php else: ?>
					<a href="schedule.php">
				<?php endif; ?>
					<span>Schedule</span>
				</a>
			</li>
            <li>
				<?php if ($this->_tpl_vars['active_tab'] == 'message'): ?>
					<a href="#" class="active">
				<?php else: ?>
					<a href="messages.php">
				<?php endif; ?>
					<span>Messages</span>
				</a>
			</li>
			<li>
				<?php if ($this->_tpl_vars['active_tab'] == 'profile'): ?>
					<a href="#" class="active">
				<?php else: ?>
					<a href="profile.php">
				<?php endif; ?>
					<span>Profile</span>
				</a>
			</li>
			<?php if ($this->_tpl_vars['is_coach'] == '1'): ?>
				<li>
					<?php if ($this->_tpl_vars['active_tab'] == 'coach'): ?>
					<a href="#" class="active">
					<?php else: ?>
						<a href="calendar.php">
					<?php endif; ?>
						<span>Coach</span>
					</a>
					
				</li>
			<?php endif; ?>		
				
			
			<?php if ($this->_tpl_vars['is_partner'] == '1'): ?>
				<li>
					<?php if ($this->_tpl_vars['active_tab'] == 'training partner'): ?>
						<a href="#" class="active">
					<?php else: ?>
						<a href="training_partner_game.php">
					<?php endif; ?>
						<span>Training Partner</span>
					</a>
				</li>
			<?php endif; ?>
			
			<li>
				<?php if ($this->_tpl_vars['active_tab'] == 'cashier'): ?>
					<a href="#" class="active">
				<?php else: ?>
					<a href="cashier.php">
				<?php endif; ?>
					<span>Cashier</span>
				</a>
			</li>
			
        </ul>