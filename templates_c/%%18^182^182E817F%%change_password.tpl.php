<?php /* Smarty version 2.6.16, created on 2013-02-09 13:18:23
         compiled from change_password.tpl */ ?>
<?php echo '
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jQuery.functions.pack.js"></script>
<script language="javascript" src="js/contact_us.js"></script>

<script language="javascript">

function CheckFields()
{
  
 
   if(document.getElementById(\'old_password\').value==\'\')
   {
    jQuery(\'#FormErrorMsg\').show();
    jQuery(\'#FormErrorMsg\').html("Please enter your old password");
	//jQuery(\'#FormErrorMsg\').fadeOut(2000);
    return false;
   }
    if(document.getElementById(\'new_password\').value==\'\') 
   {
     jQuery(\'#FormErrorMsg\').show();
    jQuery(\'#FormErrorMsg\').html("Please enter a new password");
   // jQuery(\'#FormErrorMsg\').fadeOut(2000);

    return false;
   }
   if(document.getElementById(\'conf_password\').value==\'\')
   {
    jQuery(\'#FormErrorMsg\').show();
    jQuery(\'#FormErrorMsg\').html("Please enter password again");
	//jQuery(\'#FormErrorMsg\').fadeOut(2000);
    return false;
   }     
	if(document.getElementById(\'new_password\').value!=document.getElementById(\'conf_password\').value)
   {
    jQuery(\'#FormErrorMsg\').show();
    jQuery(\'#FormErrorMsg\').html("Password did not match");
	//jQuery(\'#FormErrorMsg\').fadeOut(2000);
    return false;
   } 
   var email = jQuery(\'#chpwd_email\').val();
   var password = jQuery(\'#new_password\').val();
   
   jQuery.post("change_password.php",{"submit":"Submit", "old_password":jQuery(\'#old_password\').val(),"new_password":password, "conf_password":password},function(data){   	
   		if(data == 1){
			jQuery(\'#FormErrorMsg\').html(\'Wrong old password given\');
		}
		else{
   		jQuery(\'#FormErrorMsg\').html(data);
		   jQuery.post("forum/ucp.php?i=profile&mode=reg_details&main_site=true",{"username":email,"email_confirm":email,"email":email,"new_password":password,"password_confirm":password,"cur_password":jQuery(\'#old_password\').val(),"redirect":"","submit":"Submit"}, function(data){																																																																							
				
			});
		}
   });
  return false; 
}
</script>
'; ?>

<div class="content">
   	<!-- tab -->
	<ul class="tabflex">
		<li><a href="schedule.php"><span>Schedule</span></a></li>
		<li><a href="messages.php"><span>Messages</span></a></li>
		<li><a href="profile.php" class="active"><span>Profile</span></a></li>
		<?php if ($this->_tpl_vars['is_coach'] == '1'): ?><li><a href="coach_game.php"><span>Coach</span></a></li><?php endif; ?>
		<?php if ($this->_tpl_vars['is_partner'] == '1'): ?><li><a href="training_partner_game.php"><span>Training Partner</span></a></li><?php endif; ?>
		<li><a href="cashier.php"><span>Cashier</a></li>
	  <!--    <li><a href="#"><span>Students</span></a></li>-->
	</ul>
        <!-- tab -->
	<div class="clear"></div>
	<div class="tabular-content"> 
		<div class="total-message">&nbsp;&nbsp;</div>
		<div class="clear"></div>                     
		<div style="margin-top: 30px;">
			<div class="register">
				<div id='FormErrorMsg' style="color:red; text-align:left; padding-bottom:10px; padding-left:100px;"><?php if ($this->_tpl_vars['ermsg'] != ''): ?> <?php echo $this->_tpl_vars['ermsg'];  endif; ?> </div>
				<form name="contact" id="contact" method='post'>
					<input type="hidden" name="chpwd_email" id="chpwd_email" value="<?php echo $this->_tpl_vars['user_email']; ?>
" />
						<fieldset>
							<label>Old Password :</label>
							<input name="old_password" id="old_password" type="password" value="" class="con-req" />
							<p class="clear"></p>
							<label>New Password :</label>
							<input name="new_password" id="new_password" type="password" value="<?php echo $this->_tpl_vars['Formval']['new_password']; ?>
" class="con-req" />
							<p class="clear"></p>
							<label>Confirm Password :</label>
							<input name="conf_password" id="conf_password" type="password" value="<?php echo $this->_tpl_vars['Formval']['conf_password']; ?>
" class="con-req" />
							
							<p class="clear"></p>
							
							<label>&nbsp;</label>
							<!--<input name="submit" class="submit" value="Edit" type="submit" onclick='return CheckFields()' />-->
							<a href="javascript:;" onclick='return CheckFields()' class="button">Ok</a>
							<a href="profile.php" class="button">Cancel</a> 
							<!--<input name="" class="cancel" value="Cancel" onclick="window.location.href='profile.php'" />-->
						</fieldset>
				</form>
			</div>
		  
		</div>
    </div>
</div>