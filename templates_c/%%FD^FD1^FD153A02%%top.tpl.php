<?php /* Smarty version 2.6.16, created on 2013-04-25 17:20:37
         compiled from top.tpl */ ?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<?php if ($_SESSION['user_id'] != ''): ?>
<script type="text/javascript">
  if(window.name != "chironWrapper" ){
	window.location = "/chiron.php";
  }
</script> 
<?php endif; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="<?php echo $this->_tpl_vars['CmsArr']['metatag_keywords']; ?>
" />  
<meta name="description" content="<?php echo $this->_tpl_vars['CmsArr']['metatag_description']; ?>
" />
<title><?php if ($this->_tpl_vars['pagetitle'] != ''):  echo $this->_tpl_vars['pagetitle']; ?>
 - <?php endif; ?> Chiron Gaming</title>

<link rel="icon" href="images/icon_32.png" sizes="32x32">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link href="style/style.css" rel="stylesheet" type="text/css" />
<link href="style/graphite.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="style/default.css" type="text/css" media="screen" />
<link rel="stylesheet" href="style/jquery-ui-1.8.23.custom.css" type="text/css" />
<link href="style/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
<link href="style/jquery.pnotify.default.css" rel="stylesheet" type="text/css" />
<link href="style/jquery.pnotify.default.icons.css" media="all" rel="stylesheet" type="text/css" />
<link href="style/notify.css" rel="stylesheet" type="text/css" />
<?php echo '
<script type="text/javascript" src="jss/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="jss/jquery.pnotify.js"></script>
<script type="text/javascript" src="jss/jquery.nicescroll.js"></script>
<script type="text/javascript" src="jss/jquery-notify.js"></script>

<script type="text/javascript" src="jss/jquery-ui-1.8.23.customDEV.js"></script> 
<script type="text/javascript" src="jss/jquery.caret.js"></script>
<script type="text/javascript" src="jss/jquery.shiftcheckbox.js"></script>
<link type="text/css" href="style/jquery.jscrollpane.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="jss/jquery.mousewheel.js"></script>
<script type="text/javascript" src="jss/jquery.jscrollpane.min.js"></script>

<link rel="stylesheet" href="style/ingrid.css" type="text/css" media="screen" />
<script type="text/javascript" src="js/ingrid.js"></script>
<script language="javascript" type="text/javascript" src="_scripts/jQuery.functions.pack.js"></script>
<script type="text/javascript" src="js/jquery.dateLists.min.js"></script>
<script language="javascript" src="js/contact_us.js"></script>
<script language="javascript" type="text/javascript" src="includeajax/AjaxUserInterface.js"></script>
<script language="javascript" type="text/javascript" src="_scripts/common.js"></script>
<script language="javascript" type="text/javascript" src="js/common.js"></script>
<script language="javascript" type="text/javascript" src="sitemanager/js/jquery-impromptu.2.4.min.js"></script>
<link href="style/alert.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="includeajax/user_login.js"></script>

<link rel="stylesheet" type="text/css" href="style/ui.dropdownchecklist.css" />
<link rel="stylesheet" type="text/css" href="style/jquery.dateLists.css" />
<script type="text/javascript" src="js/ui.core.js"></script>
<script type="text/javascript" src="js/ui.dropdownchecklist.js"></script>
<link rel="stylesheet" href="style/autosuggest.css" type="text/css" />
<script type="text/javascript" src="js/jquery.ausu-autosuggest.min.js"></script>
'; ?>


<?php if ($_SESSION['user_id'] != ''):  echo '

<link type="text/css" href="style/players.css" rel="stylesheet" />

'; ?>

<?php endif; ?>
<script type="text/javascript" src="jss/scripts.js"></script> 

</head>

<body>
<div class="navigation">
  <div class="menu">
        <a href="index.php" class="logo"></a>
    <ul class="menuUl">
      
            <li class="menuLi">
              <a class="menuA <?php if ($this->_tpl_vars['pg_name'] == 'howitworks.php'): ?>active<?php endif; ?>" href="howitworks.php" >
                <span>How It Works</span>
              </a>
            </li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
            <li class="menuLi">
              <a class="menuA <?php if ($this->_tpl_vars['pg_name'] == 'findcoach.php'): ?>active<?php endif; ?>" href="findcoach.php">
                <span>Find Coach</span>
              </a>
            </li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
            <li class="menuLi">
              <a class="menuA <?php if ($this->_tpl_vars['pg_name'] == 'find_training_partner.php'): ?>active<?php endif; ?>" href="find_training_partner.php">
                <span>Find Training Partner</span>
        </a>
      </li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi">
          <a class="menuA <?php if ($this->_tpl_vars['pg_name'] == 'masterclass.php'): ?>active<?php endif; ?>" href="javascript:;">
          <span>Master Class</span>
        </a>
      </li>
            <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi"><a class="menuA" href="forum<?php if ($this->_tpl_vars['sid'] != ''): ?>?sid=<?php echo $this->_tpl_vars['sid'];  endif; ?>" <?php if ($this->_tpl_vars['pg_name'] == 'forum.php'): ?>  class="active" <?php endif; ?>><span>Forum</span></a></li>
    </ul>
    <ul style="float: right;" class="menuUl">
      <?php if ($_SESSION['user_id'] == ''): ?>
      <li class="menuLi"><a class="menuA" href="javascript:;" onClick="openLogin()"><span>Login</span></a></li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi"><a class="menuA" href="javascript:;" onClick="openRegister()" ><span>Register</span></a></li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi"><a class="menuA" href="javascript:;" onClick="open_lostPwd()"><span>Lost password?</span></a></li>
      <?php else: ?>
      <li class="menuLi"><a class="menuA" href="profile.php" style="text-decoration:none;color: #FC0;" id="username"><?php echo $this->_tpl_vars['username']; ?>
</a></li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi"><a class="menuA" href="schedule.php">Dashboard</a> </li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi usermenuli" style="margin-left: 0">
        <a href="javascript:;" id="arrowmenu" class="menuA downarrow" style="height: 20px"></a>  
        <div class="topsub_menu_wrapper" style="">
          <div class="topsub_menu" style="">
            <ul style="">
              <li><a href="cashier.php">Cashier (<?php echo $this->_tpl_vars['totalPoints']; ?>
 Pts)</a></li>
              <li><a href="support.php">Support</a></li>
              <li><a href="contactus.php">Contact</a></li>
              <li style="border: none"><a href="logout.php">Logout</a></li>
            </ul>
          </div>
        </div>
      </li>
      <?php endif; ?>
        </ul>
    </div>

</div>
<div id="bodywrapper">
<div id="overlay"></div>
<div class="loading" id="pageloading" style="margin-left: -30px;margin-top: -50px;display: none;"></div>
<?php if ($_SESSION['user_id'] == ''): ?>
<div class="modalpopup" id="registerwindow">
  
  <a id="closeRegisterBtn" class="modalpopup-close-btn" href="#" style=""></a>
  <div id="registeroverlay" class="overlay" style="position: absolute; z-index: 8"></div>
  <div id="registerSuccesswindow" style="">
    <div>
        You have successfully registered
    </div>
    <a class="button" href="#" onClick="closeRegister(event)" style="margin-left: 110px;margin-top: 20px;">Ok</a>
  </div>
  <div class="">
  
    <div class="title">Registration</div>
        <div class="clear"></div>
    
    <div class="register">
      <div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:10px;"><?php if ($this->_tpl_vars['ermsg'] != ''): ?> <?php echo $this->_tpl_vars['ermsg'];  endif; ?> </div>
      <div class="loading" id="regloading" style="margin-left: -30px;margin-top: -50px;display: none;"></div>
      <div class="overlay" id="regoverlay" style="z-index: 999998; position: absolute"></div>
      <form name="registerForm" id="registerForm" enctype="multipart/form-data" method='post' onsubmit="" action="">
        <fieldset>
            <input type="hidden" name="agreed" value="true" />
            <input type="hidden" name="lang" value="en" />
            <input type="hidden" name="change_lang" value="0" />
            <input type="hidden" name="creation_time" value="<?php echo $this->_tpl_vars['now']; ?>
" />
            <input type="hidden" name="main_site" value="/" />

            <label>Email :</label>
            <input name="email" type="text" id="register-email" class="con-req" value="" />
            <label>Username :</label>
            <input name="username" id="username" type="text" value="" class="con-req" />
            <p class="clear"></p>
            <label>Password :</label>
            <input name="new_password" type="password" id="password1" class="con-req" value="" />
            <label>Confirm Password :</label>
            <input name="password_confirm" type="password" id="password2" class="con-req" value="" />
            <p class="clear"></p>
            
            <label>Timezone :</label>
            <select name="tz" id="tz" tabindex="7" class="" style="width: 300px;">
                <?php $_from = $this->_tpl_vars['tz_zones']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
                     <option <?php if ($this->_tpl_vars['k'] == '0'): ?>selected<?php endif; ?> value="<?php echo $this->_tpl_vars['k']; ?>
"><?php echo $this->_tpl_vars['v']; ?>
</option>
                <?php endforeach; endif; unset($_from); ?>
            </select>
           
            <p class="clear"></p>

            <label>Captcha :</label>
            <div style="float:left; width:180px;"><!--img src="CaptchaSecurityImages.php?characters=7" /--> 
                <img src="#" id="captcha" />
                <br />
                <a href="javascript:;" onclick="document.getElementById('captcha').src='/captcha.php?'+Math.random();document.getElementById('txtCaptcha').focus();" id="change-image">Not readable? Change text.</a>
            </div>

            <label>Security Code :</label>
            <input name="txtCaptcha" id="txtCaptcha" type="text" value="" />


            <p class="clear"></p>
            <div   style="margin-top:10px;">&nbsp;</div>

            <div class="register-footnote">By clicking Submit, I acknowledge that I have read and agree to our <a href="termsconditions.php">Terms & Conditions</a> and <a href="privacypolicy.php">Privacy Policy</a>.</div>
            <!--<label>&nbsp;</label>-->
            <div style="margin-left: 100px;">
              <a style="float: left;" class="button" href="javascript:;" onClick="return CheckFields();">Submit</a>
              <a style="float: left;" class="button" href="javascript:;" onClick="clearFields()">Reset</a>
            </div>
            <input type="hidden" name="submit" value="Submit" />
        </fieldset>
      </form>
    </div>
    <div class="clear"></div>
  </div>
</div>
  <div class="modalpopup" id="loginwindow">
    <a class="modalpopup-close-btn" href="javascript:;" style=""></a>
    <div class="loading" id="loginloading" style="margin-left: -30px;margin-top: -50px;display: none;"></div>
    <div class="overlay" id="loginoverlay" style="z-index: 999998; position: absolute"></div>
    <div class="login">
      <div class="title">Login</div>
      <div style="width:100%">
      </div>  
      <br />
      <form name="frmLogin" id="frmLogin" method="post" action="" >
     
          <input type="hidden" name="login" value="Login" />
        <table>
          <tr>
            <td>
              <label for="email">E-Mail</label>
            </td>
            <td>
              <input name="email" type="text" value="Enter Your Email" id="email" onKeyPress="return submitloginbyenter('frmLogin',this, event)" />
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <div id='FormErrorMsg1' style="color:red;">&nbsp;</div>
            </td>
          </tr>
          <tr>
            <td>
              <label for="password">Password</label>
            </td>
            <td>
              <input name="password" type="password" value="password" id="password" onKeyPress="return submitloginbyenter('frmLogin',this, event)" />
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <div id='FormErrorMsg2' style="color:red;">&nbsp;</div>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <a class="button" href="javascript:;" style="margin-top: 10px" onclick="return submit_frm();">Login</a>
            </td>
          </tr>
          
        </table>
      </form>
      <div class="clear"></div>
      <p class="register-login-links">
          <a href="javascript:;" onclick="closeOpen('loginwindow','registerwindow',event)">Register</a> or <a href="javascript:;" onClick="closeOpen('loginwindow','lostPwd',event)">Lost password?</a></p>
    </div>
  </div>
    
  <div class="modalpopup" id="lostPwd" >
    <a class="modalpopup-close-btn" href="javascript:;"></a>
    <div class="loading" id="forgotloading" style="margin-left: -30px;margin-top: -50px;display: none;"></div>
    <div class="overlay" id="forgotoverlay" style="z-index: 999998; position: absolute"></div>
    <div class="login">
      <div class="title">Lost your password?</div>
      <div style="width:100%">
        <div id='ErrorMsg' style="color:red; text-align:left; float:left; width:45%">&nbsp;</div>
      </div>  
      <br />
      <form name="frmLostpwd" id="frmLostpwd" method="post" action="" >
        <table>
          <tr>
            <td>
              <label for="email_id">E-Mail</label>
            </td>
            <td>
              <input name="email_id" type="text" value="" id="email_id" onKeyPress="return submitenter('frmLostpwd',this, event)" />
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <a class="button" href="javascript:;" style="margin-top: 20px" onclick="return Get_pwd();">Submit</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div class="clear"></div>
        <p style="float: right;" class="register-login-links">
            <a href="javascript:;" onClick="closeOpen('lostPwd','registerwindow',event)">Register</a> or <a href="javascript:;" onClick="closeOpen('lostPwd','loginwindow',event)">Login</a>
        </p>
  </div> 

  
<?php endif; ?>



<div class="main">


<?php if ($this->_tpl_vars['pg_name'] == 'index.php' || $this->_tpl_vars['pg_name'] == 'my_lesson.php'): ?>
<div class="container">
<?php elseif ($this->_tpl_vars['pg_name'] == 'videodetails.php'): ?>
<div class="video-outer">
<?php else: ?>
<div class="container-inner">
<?php endif; ?>