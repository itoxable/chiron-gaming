<?php /* Smarty version 2.6.16, created on 2012-12-30 15:17:08
         compiled from videodetails.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'nl2br', 'videodetails.tpl', 341, false),)), $this); ?>
<?php echo '
<link type="text/css" rel="stylesheet" href="style/lightbox_form.css">
<script src="js/lightbox-form.js" type="text/javascript"></script>
<script language="javascript" src="js/contact_us.js"></script>

<script language="javascript">
var jQuery = jQuery.noConflict();
jQuery(document).ready(function(){

		    jQuery(".rating").rating();
		    jQuery("#serialStar").rating();
			
		});		
		
function OpenDiv(){
document.getElementById(\'open_close_div\').style.display="block";
}

function CloseDiv(){
document.getElementById(\'open_close_div\').style.display="none";
}

function CheckFields(formID,NumRatingCat)
{
   var r_comment = document.getElementById(\'review_comment\').value;
   var video_id = document.getElementById(\'video_id\').value;
   var reviewed_by = document.getElementById(\'logged_user\').value;
   
   if(document.getElementById(\'review_comment\').value==\'\')
   {
    j(\'#review_comment\').css(\'border\',\'1px solid #FF0000\');
		return false;
   }
   else 
   j(\'#review_comment\').css(\'border\',\'1px solid #ccc\');
   
   
   var frmID=\'#\'+formID;
	
	var params ={

		\'action\': \'review_post\',
		\'IsProcess\': \'Y\',
		//\'video_id\': video_id,
		\'reviewed_by\': reviewed_by,
		\'review_comment\': r_comment
	};

   
    if(r_comment!=\'\')
   {
	  jQuery.ajax({
		type: "POST",
		url: \'videodetails.php?video_id=\'+video_id,
		data: params,
		dataType : \'text\',
		success: function(data){
		
		//if (data.flag == \'1\')
		  //{
		    jQuery(\'#review-message\').fadeIn(0);
			jQuery(\'#msgbox\').fadeIn(0);
		    jQuery(\'#msgbox\').html(\'Review posted successfully.\');
		    jQuery(\'#review-message\').fadeOut(5000);
			jQuery(\'#paging\').html(data);
		 
		 // }
        }
      });
	  document.getElementById("review_comment").value=\'\';
	  //document.getElementById(\'serialStar\').value=\'\';
	  jQuery("#reviewwindow").fadeOut("fast", function(){jQuery(\'#overlay\').fadeOut();});
	}
  }	
/*function removeDiv()
{
clearInterval(ti);
document.getElementById(\'msgbox\').style.display = \'none\';
}	
*/
function closereviewbox()
{
	document.getElementById("review_comment").value=\'\';
	//document.getElementById("serialStar").value=\'\';
	jQuery(\'#review_comment\').css(\'border\',\'1px solid #ccc\');
	jQuery("#reviewwindow").fadeOut("fast", function(){jQuery(\'#overlay\').fadeOut();});
}
</script>
<script type="text/javascript">
function SortingList(sorting_url,sorting_by)
{
var params = {
	\'list_for\':\'sorting\',
	\'sorting_by\': sorting_by,
	\'IsProcess\': \'Y\',
	\'action\': \'send\',
};

jQuery.ajax({
	type: "GET",
	url: sorting_url,
	data: params,			
	dataType: \'html\',
	success: function(data)
	{				
		jQuery(".container-inner").html(data+"<div class=\'clear\'></div>");
	}
});
return false;		
}
</script>

<script type="text/javascript">
function redirect_function(id)
{
//alert(id);
  document.forms["videoform"+id].submit();
}
</script>
<script type="text/javascript">

 function LikeVideo(coach_id,video_id,like)
 { 	 
 
 	if(like == \'D\')
	{ 
	 	jQuery.ajax({
		   url:\'like_post.php\',
		   data:\'coach_id=\'+coach_id+\'&video_id=\'+video_id+\'&selected_for=\'+like,
		   type:\'post\',		 
		   dataType:\'html\', 
		   success:function(resp){	
				var arr = resp.split("-");
						  
				jQuery(\'#showMsgs\').fadeIn(0);
				jQuery(\'#showMsgs\').html(\'You have dislike this video\');
				jQuery(\'#showMsgs\').fadeOut(5000);
				
				jQuery(\'#like-dislike-bar\').html(arr[0]);
				jQuery(\'#rating-like\').html(arr[1]);
				jQuery(\'#rating-dislike\').html(arr[2]);
							
			}			   
		});
	}
	else
	{
		 jQuery.ajax({
		   url:\'like_post.php\',
		   data:\'coach_id=\'+coach_id+\'&video_id=\'+video_id+\'&selected_for=\'+like,
		   type:\'post\',		 
		   dataType:\'html\', 
		   success:function(resp){	
				var arr = resp.split("-");
				
				jQuery(\'#showMsgs\').fadeIn(0);
				jQuery(\'#showMsgs\').html(\'You have liked this video\');
				jQuery(\'#showMsgs\').fadeOut(5000);
				
				jQuery(\'#like-dislike-bar\').html(arr[0]);
				jQuery(\'#rating-like\').html(arr[1]);
				jQuery(\'#rating-dislike\').html(arr[2]);

			}			   
		});	
	}
}


</script>   		
'; ?>

<?php if ($this->_tpl_vars['IsProcess'] != 'Y'): ?>

<div class="video-content">
   <div class="vdetails">
     <h2><?php echo $this->_tpl_vars['VideoArr']['video_title']; ?>
</h2>
	 <ul class="games-desc" style="padding-top:0px; width:50%;">
      <li><span>Uploaded By : </span><?php echo $this->_tpl_vars['VideoArr']['name']; ?>
</li>
	  </ul>
	 <ul class="games-desc" style="padding-top:0px;text-align: right; width:50%;"><?php echo $this->_tpl_vars['VideoArr']['v_dt']; ?>
&nbsp;</ul>
   </div>	 
     <div class="video-placeholder">
	 <?php if ($this->_tpl_vars['VideoArr']['is_video_url'] == 'Y'): ?>

        <object width="490" height="376">
            <param name="movie" value="http://www.youtube.com/v/<?php echo $this->_tpl_vars['video_num']; ?>
?fs=1"</param>
            <param name="allowFullScreen" value="true"></param>
            <param name="allowScriptAccess" value="always"></param>
            <param name="wmode" value="transparent" />
            <embed src="http://www.youtube.com/v/<?php echo $this->_tpl_vars['video_num']; ?>
?fs=1"
            type="application/x-shockwave-flash" allowfullscreen="true" wmode="opaque" allowscriptaccess="always" width="490" height="376">
            </embed>
        </object>

            
	  <?php else: ?>	
			<OBJECT ID="MediaPlayer" width="490" height="376" CLASSID="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95"
			STANDBY="Loading Windows Media Player components..." TYPE="application/x-oleobject">
			<PARAM NAME="FileName" VALUE="uploaded/videos/<?php echo $this->_tpl_vars['VideoArr']['video_file']; ?>
">
			<PARAM name="ShowControls" VALUE="true">
			<param name="ShowStatusBar" value="false">
			<PARAM name="ShowDisplay" VALUE="false">
			<PARAM name="autostart" VALUE="true">
			<param name="wmode" value="transparent" />
            <param name="windowlessVideo" value="true" >
			<EMBED TYPE="application/x-mplayer2" SRC="uploaded/videos/<?php echo $this->_tpl_vars['VideoArr']['video_file']; ?>
" NAME="MediaPlayer"
			WIDTH="490" HEIGHT="376" ShowControls="1" wmode="opaque" windowlessVideo="true" ShowStatusBar="0" ShowDisplay="0" autostart="1"> </EMBED>
            </OBJECT>
	   <?php endif; ?>		
	 </div>
     <div class="video-list">
     <div class="vdetails">
           <!-- <p><span>Votes :</span><span id="votes_<?php echo $this->_tpl_vars['VideoArr']['video_id']; ?>
"> <?php echo $this->_tpl_vars['VideoArr']['numlike']; ?>
</span></p>-->
		   
           <?php if ($_SESSION['user_id'] != ''): ?>
		   <?php if ($this->_tpl_vars['VideoArr']['user_id'] != $_SESSION['user_id']): ?>
				  <div id="game-rating_<?php echo $this->_tpl_vars['VideoArr']['video_id']; ?>
">			 
				  <div class="like-dislike" style="float:left;">
					  <a href="javascript:;" onclick="return LikeVideo(<?php echo $this->_tpl_vars['VideoArr']['user_id']; ?>
,<?php echo $this->_tpl_vars['VideoArr']['video_id']; ?>
,'L')">
					  <img src="images/like.png" alt="Like"/> </a>
					  <a href="javascript:;" onclick="return LikeVideo(<?php echo $this->_tpl_vars['VideoArr']['user_id']; ?>
,<?php echo $this->_tpl_vars['VideoArr']['video_id']; ?>
,'D')">
					  <img src="images/unlike.png" alt="Unlike"/> </a>
				  </div>
		   <?php else: ?>		  
				<div id="game-rating_<?php echo $this->_tpl_vars['VideoArr']['video_id']; ?>
">			 
				  <div class="like-dislike" style="float:left;">
					 
				  </div>  
		   <?php endif; ?>
		   	  
		   <?php else: ?>
		    <div id="game-rating_<?php echo $this->_tpl_vars['VideoArr']['video_id']; ?>
">			 
			  <div class="like-dislike" style="float:left;">
				  <a href="javascript:;" onclick="OpenDiv();">
				  <img src="images/like.png" alt="Like"/> </a>
				  <a href="javascript:;" onclick="OpenDiv();">
				  <img src="images/unlike.png" alt="Dislike"/> </a> 
			  </div>
		    <?php endif; ?>	  		  
				  <div class="like-dislike-bar">
				  <span id="like-dislike-bar">
				  <?php if ($this->_tpl_vars['RatingLike'] == '0' && $this->_tpl_vars['RatingUnlike'] == '0'): ?>
					 <img src='images/null.gif' alt='' width='210px' height="4px" border='0' />
				  <?php else: ?>  
					  <img src='images/green.gif' alt='' width='<?php echo $this->_tpl_vars['WidthGreen']; ?>
px' height="4px" border='0' />
					  <img src='images/red.gif' alt='' width='<?php echo $this->_tpl_vars['WidthRed']; ?>
px' height="4px"  border='0' />
				  <?php endif; ?>
				  </span>
				  <br />
				  <span id="rating-like"><?php echo $this->_tpl_vars['RatingLike']; ?>
</span> Likes, <span id="rating-dislike"><?php echo $this->_tpl_vars['RatingUnlike']; ?>
</span> Dislikes
				  </div>  
				  </div>  
			
           <br>
           <div class="clear"></div>
		   <div id="showMsgs" style="color:#0077BC;"></div>
           
           <div class="clear"></div>
		   <div class="signin-message" style="display:none;" id="open_close_div">
           <a href="login.php" title="Sign In">Sign in</a> or <a href="register.php" title="Sign Up">Sign up</a> now!
           <a href="javascript:;" onclick="CloseDiv();"><img src="images/close.png" border="0" alt="Close" class="close" title="Close" /></a>
		   </div>
		   <br>
           <div class="clear"></div>
           <p><?php echo $this->_tpl_vars['VideoArr']['description']; ?>
</p>
           <p class="clear"></p>
           <ul class="games-desc">
               <li><label class="gametype">Game :</label><label class="gamedetail"><?php echo $this->_tpl_vars['VideoArr']['game']; ?>
</label></li>
			   <?php if ($this->_tpl_vars['VideoArr']['ladder'] != ''): ?>
               <li><label class="gametype">Ladder :</label><label class="gamedetail"><?php echo $this->_tpl_vars['VideoArr']['ladder']; ?>
</label></li>
			   <?php endif; ?>
			   <?php if ($this->_tpl_vars['VideoArr']['type'] != ''): ?>
			   <li><label class="gametype">Type :</label><label class="gamedetail"><?php echo $this->_tpl_vars['VideoArr']['type']; ?>
</label></li>
			   <?php endif; ?>
			   <?php if ($this->_tpl_vars['VideoArr']['champion'] != ''): ?>
			   <li><label class="gametype">Versus :</label><label class="gamedetail"><?php echo $this->_tpl_vars['VideoArr']['champion']; ?>
</label></li>
			   <?php endif; ?>
			   <?php if ($this->_tpl_vars['VideoArr']['mode'] != ''): ?>
			   <li><label class="gametype">Mode :</label><label class="gamedetail"><?php echo $this->_tpl_vars['VideoArr']['mode']; ?>
</label></li>
			   <?php endif; ?>
            </ul>
            <ul class="games-desc">
			   <li><label class="gametype">Duration :</label><label class="gamedetail"><?php echo $this->_tpl_vars['VideoArr']['time_length']; ?>
 min</label></li>
			   <?php if ($this->_tpl_vars['VideoArr']['versus'] != ''): ?>
			   <li><label class="gametype">Versus :</label><label class="gamedetail"><?php echo $this->_tpl_vars['VideoArr']['versus']; ?>
</label></li>
			   <?php endif; ?>
			   <?php if ($this->_tpl_vars['VideoArr']['team'] != ''): ?>
			   <li><label class="gametype">Team :</label><label class="gamedetail"><?php echo $this->_tpl_vars['VideoArr']['team']; ?>
</label></li>
			   <?php endif; ?>
			   <?php if ($this->_tpl_vars['VideoArr']['map'] != ''): ?>
			   <li><label class="gametype">Map :</label><label class="gamedetail"><?php echo $this->_tpl_vars['VideoArr']['map']; ?>
</label></li>
			   <?php endif; ?>
			   <?php if ($this->_tpl_vars['VideoArr']['class'] != ''): ?>
			   <li><label class="gametype">Class :</label><label class="gamedetail"><?php echo $this->_tpl_vars['VideoArr']['class']; ?>
</label></li>
			   <?php endif; ?>
             </ul>
			
            <div class="clear"></div>
          
		   <div style="padding-right:15px; padding-bottom:10px;">
		  
		   <?php if ($_SESSION['user_id'] != '' && $_SESSION['user_id'] != $this->_tpl_vars['VideoArr']['user_id']): ?>
              <div class="button-flex button70" style="margin-right:<?php if ($_SESSION['user_type'] == '2' && $this->_tpl_vars['is_fav'] == '0'): ?>20px;
			  <?php elseif ($_SESSION['user_type'] == '2' && $this->_tpl_vars['is_fav'] == '1'): ?>10px;<?php else: ?>0px;<?php endif; ?>"><a href="javascript:void(0);" 
			  onClick="openReview('Review and Rating for <b><?php echo $this->_tpl_vars['VideoArr']['video_title']; ?>
</b>')"><span>Review</span></a></div> 
		   <?php endif; ?>
		   <br /> 
			         <div class="clear"></div>
		             <div class="signin-message" id="review-message" style="display:none;">
                     <div id="msgbox" style="color:#0077BC;"></div>
		             </div>
		   </div>
          </div> 
       <div class="clear"></div>
	  </div>	
	   <?php endif; ?> 
	   
	   <div id="paging"> 
	 
     <div class="review-list-small">
     <h2 title="Review">Reviews</h2>
	  <?php if ($this->_tpl_vars['NumReview'] > 0): ?>
       <div class="clear"></div>
	    <?php unset($this->_sections['rrow']);
$this->_sections['rrow']['name'] = 'rrow';
$this->_sections['rrow']['loop'] = is_array($_loop=$this->_tpl_vars['VdoReview']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rrow']['show'] = true;
$this->_sections['rrow']['max'] = $this->_sections['rrow']['loop'];
$this->_sections['rrow']['step'] = 1;
$this->_sections['rrow']['start'] = $this->_sections['rrow']['step'] > 0 ? 0 : $this->_sections['rrow']['loop']-1;
if ($this->_sections['rrow']['show']) {
    $this->_sections['rrow']['total'] = $this->_sections['rrow']['loop'];
    if ($this->_sections['rrow']['total'] == 0)
        $this->_sections['rrow']['show'] = false;
} else
    $this->_sections['rrow']['total'] = 0;
if ($this->_sections['rrow']['show']):

            for ($this->_sections['rrow']['index'] = $this->_sections['rrow']['start'], $this->_sections['rrow']['iteration'] = 1;
                 $this->_sections['rrow']['iteration'] <= $this->_sections['rrow']['total'];
                 $this->_sections['rrow']['index'] += $this->_sections['rrow']['step'], $this->_sections['rrow']['iteration']++):
$this->_sections['rrow']['rownum'] = $this->_sections['rrow']['iteration'];
$this->_sections['rrow']['index_prev'] = $this->_sections['rrow']['index'] - $this->_sections['rrow']['step'];
$this->_sections['rrow']['index_next'] = $this->_sections['rrow']['index'] + $this->_sections['rrow']['step'];
$this->_sections['rrow']['first']      = ($this->_sections['rrow']['iteration'] == 1);
$this->_sections['rrow']['last']       = ($this->_sections['rrow']['iteration'] == $this->_sections['rrow']['total']);
?>
		 
	    <div class="review-block-small">
             
			 <div class="review-left">
			 <a href="<?php echo $this->_tpl_vars['VdoReview'][$this->_sections['rrow']['index']]['userlink']; ?>
">
			  <img src="<?php echo $this->_tpl_vars['VdoReview'][$this->_sections['rrow']['index']]['img']; ?>
" height="92" width="105"  alt="<?php echo $this->_tpl_vars['VdoReview'][$this->_sections['rrow']['index']]['review_by']; ?>
" border="0" />
			 </a>
			 </div>
			 <div class="review-right">
			 <div class="review-sub-title">
             
             	<div class="review_thumb_block">
				 <h4 class="reviewshead">Reviewed By : <a href="<?php echo $this->_tpl_vars['VdoReview'][$this->_sections['rrow']['index']]['userlink']; ?>
"><?php echo $this->_tpl_vars['VdoReview'][$this->_sections['rrow']['index']]['review_by']; ?>
</a></h4><br/><br />
	 			 
				 <span><em>Posted on</em></span> : <?php echo $this->_tpl_vars['VdoReview'][$this->_sections['rrow']['index']]['r_date']; ?>
<br /><br />
				 <?php if ($this->_tpl_vars['VdoReview'][$this->_sections['rrow']['index']]['review_comment'] != ''): ?>
				 <p><i>Review</i>:&nbsp;<span style="width:100px;"><?php echo ((is_array($_tmp=$this->_tpl_vars['VdoReview'][$this->_sections['rrow']['index']]['review_comment'])) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</span></p><br />
				 <?php endif; ?>
				</div>
                
                <div class="clear"></div>
			 </div>
			 </div>
			 <div class="clear"></div>
		</div>
		 
	    <?php endfor; endif; ?>
      
	  <?php if ($this->_tpl_vars['pagination_arr'][1]): ?>
      <div class="clear"></div>
      <div class="pagin">
         <ul>
            <?php echo $this->_tpl_vars['pagination_arr'][1]; ?>

         </ul>
      </div>
	  <?php endif; ?>
	  <?php else: ?>
	   No reviews
	<?php endif; ?> 
	</div>
  </div>
	   
   </div>

	

  
<div class="modalpopup" id="reviewwindow">
    <a class="modalpopup-close-btn" href="javascript:;"></a>
    <div id="boxtitle" style="font-weight: normal;" class="title"></div>
    <div class="review">
        <form name="Video_Review" id="Video_Review"  method='post' action="" onsubmit="return false;">
            <input type="hidden" name="video_id" id="video_id" value="<?php echo $this->_tpl_vars['video_id']; ?>
" />
            <input type="hidden" name="logged_user" id="logged_user" value="<?php echo $this->_tpl_vars['logged_user']; ?>
" />
            <fieldset>   
                <label>Review :</label>
                <textarea name="review_comment" id="review_comment" cols="" rows="" style="font-weight:bold;"><?php echo $this->_tpl_vars['Formval']['user_about']; ?>
</textarea>
                     <p class="clear"></p>

                <label>&nbsp;</label>
                <input name="submit" class="button" value="Submit" type="submit" onclick="return CheckFields('Video_Review','<?php echo $this->_tpl_vars['NumRatingCat']; ?>
')" />
                <input name="" class="button" value="Cancel" type="button" onClick="closereviewbox()" />
                </p>
            </fieldset>	
        </form>
    </div> 
</div>
<!--<?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['start'] = (int)1;
$this->_sections['j']['loop'] = is_array($_loop=6) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
if ($this->_sections['j']['start'] < 0)
    $this->_sections['j']['start'] = max($this->_sections['j']['step'] > 0 ? 0 : -1, $this->_sections['j']['loop'] + $this->_sections['j']['start']);
else
    $this->_sections['j']['start'] = min($this->_sections['j']['start'], $this->_sections['j']['step'] > 0 ? $this->_sections['j']['loop'] : $this->_sections['j']['loop']-1);
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = min(ceil(($this->_sections['j']['step'] > 0 ? $this->_sections['j']['loop'] - $this->_sections['j']['start'] : $this->_sections['j']['start']+1)/abs($this->_sections['j']['step'])), $this->_sections['j']['max']);
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>
	<?php if ($this->_sections['j']['index'] <= $this->_tpl_vars['UserReview'][$this->_sections['rrow']['index']]['overall_r']): ?>
		<img src='images/star-c.gif' alt='' border='0' />
	<?php else: ?>
		<img src='images/star-g.gif' alt='' border='0' />
	<?php endif; ?>
<?php endfor; endif; ?>-->