<?php /* Smarty version 2.6.16, created on 2013-01-01 14:52:04
         compiled from findstudent.tpl */ ?>
<?php echo '
<script type="text/javascript">
function redirect_function(id)
{
  document.forms["studentform"+id].submit();
}
function expand(id)
{
  jQuery(\'#small_\'+id).css(\'display\',\'none\');
  jQuery(\'#big_\'+id).css(\'display\',\'block\');
  
}
function collapse(id)
{
  jQuery(\'#big_\'+id).css(\'display\',\'none\');
  jQuery(\'#small_\'+id).css(\'display\',\'block\');
  
}
</script>
'; ?>

<div id="paging"> 

 <div class="right-panel">
      <h1 title="Find Student"><span>Find Student</span></h1>
	  <div class="searchpanel">
         <form action="" method="post">
              <fieldset>
			  <label></label>
              <select for="sirt" name="sort_by" onchange="subForm(this.value);">
			      <option value="">Sort by</option>
                  <option value="rate" <?php if ($this->_tpl_vars['sort_by'] == 'rate'): ?> selected="selected" <?php endif; ?>>Budget (Low)</option>
				  <option value="rate-DESC" <?php if ($this->_tpl_vars['sort_by'] == 'rate-DESC'): ?> selected="selected" <?php endif; ?>>Budget (High)</option>
               </select>
               </fieldset>
            </form>
           <div class="clear"></div>
      </div>
      <div class="clear"></div>
 

   <?php if ($this->_tpl_vars['SearchTxt'] != ''): ?><div><b>Search By</b>  <?php echo $this->_tpl_vars['SearchTxt']; ?>
</div><?php endif; ?>
	 <?php if ($this->_tpl_vars['NumStudent'] > 0): ?>
	  <?php unset($this->_sections['studentRow']);
$this->_sections['studentRow']['name'] = 'studentRow';
$this->_sections['studentRow']['loop'] = is_array($_loop=$this->_tpl_vars['StudentArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['studentRow']['show'] = true;
$this->_sections['studentRow']['max'] = $this->_sections['studentRow']['loop'];
$this->_sections['studentRow']['step'] = 1;
$this->_sections['studentRow']['start'] = $this->_sections['studentRow']['step'] > 0 ? 0 : $this->_sections['studentRow']['loop']-1;
if ($this->_sections['studentRow']['show']) {
    $this->_sections['studentRow']['total'] = $this->_sections['studentRow']['loop'];
    if ($this->_sections['studentRow']['total'] == 0)
        $this->_sections['studentRow']['show'] = false;
} else
    $this->_sections['studentRow']['total'] = 0;
if ($this->_sections['studentRow']['show']):

            for ($this->_sections['studentRow']['index'] = $this->_sections['studentRow']['start'], $this->_sections['studentRow']['iteration'] = 1;
                 $this->_sections['studentRow']['iteration'] <= $this->_sections['studentRow']['total'];
                 $this->_sections['studentRow']['index'] += $this->_sections['studentRow']['step'], $this->_sections['studentRow']['iteration']++):
$this->_sections['studentRow']['rownum'] = $this->_sections['studentRow']['iteration'];
$this->_sections['studentRow']['index_prev'] = $this->_sections['studentRow']['index'] - $this->_sections['studentRow']['step'];
$this->_sections['studentRow']['index_next'] = $this->_sections['studentRow']['index'] + $this->_sections['studentRow']['step'];
$this->_sections['studentRow']['first']      = ($this->_sections['studentRow']['iteration'] == 1);
$this->_sections['studentRow']['last']       = ($this->_sections['studentRow']['iteration'] == $this->_sections['studentRow']['total']);
?>
      <div class="game-list-block">
		  
		  <form name="student<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
" id="studentform<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
" method="post" 
			action="studentdetails.php?student_id=<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
">
			<input type="hidden" name="price" value="<?php echo $this->_tpl_vars['price']; ?>
" />
			<input type="hidden" name="languages" value="<?php echo $this->_tpl_vars['languages']; ?>
" />
			<input type="hidden" name="availabilitys" value="<?php echo $this->_tpl_vars['availabilitys']; ?>
" />
			<input type="hidden" name="games" value="<?php echo $this->_tpl_vars['games']; ?>
" />
			<div class="game-image-block">
			<a href="#" onclick="redirect_function(<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
)">
			<?php if ($this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['photo'] != ''): ?>
		     <img src="uploaded/user_images/thumbs/big_<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['photo']; ?>
" alt="<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['name']; ?>
" border="0"  />
			<?php else: ?> 
			 <img src="images/student_thumb.jpg" alt="Student" border="0" />
			<?php endif; ?>
			</a>
		  </div>
		  <div class="game-details-block" style="height:110px;" id="small_<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
">
		  <h2><a href="#" onclick="redirect_function(<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
)" style="text-decoration:none;"><?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['name']; ?>
</a></h2>
          <!--p><?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['about']; ?>
</p>
		   <p class="clear"></p-->
            <ul class="games-desc">
               <li><span>Language :</span> <?php if ($this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['language'] != ''):  echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['language'];  else: ?>N/A<?php endif; ?></li>
			   <li><span>Availability :</span> <?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['availability_type']; ?>
</li>
			  
            </ul>
            <ul class="games-desc">
			   <li><span>Budget :</span> <?php if ($this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['rate'] != ''): ?>$<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['rate']; ?>
 <?php else: ?> N/A <?php endif; ?></li>
			   <li>&nbsp;</li>
			  
		    </ul>
			
		    <div class="clear"></div>
			
            <div class="view"><!--a href="studentdetails.php?student_id=<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
">view</a-->
			<a href="#" onclick="redirect_function(<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
)">view</a>
			</div>
			<div class="clear"></div>
			<div class="preview">
			<a href="javascript:;" style="float:right;" onclick="expand(<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
)">preview</a>
			</div>
		  </div>
		  
		  <div class="game-details" style="display:none" id="big_<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
">
            <h2><a href="#" onclick="redirect_function(<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
)" style="text-decoration:none;"><?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['name']; ?>
</a></h2>
            <!--p><?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['about']; ?>
</p>
            <p class="clear"></p-->
            <ul class="games-desc">
               <li><span>Language :</span> <?php if ($this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['language'] != ''):  echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['language'];  else: ?>N/A<?php endif; ?></li>
			   <li><span>Availability :</span> <?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['availability_type']; ?>
</li>
			   <?php if ($this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['avail_local'] == 'Y'): ?>
			   <li><span>City (Local meet-up) :</span> <?php if ($this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['availability_city'] != ''):  echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['availability_city'];  else: ?>N/A<?php endif; ?></li>
			   <?php endif; ?>
            </ul>
            <ul class="games-desc">
			   <li><span>Budget :</span> <?php if ($this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['rate'] != ''): ?>$<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['rate']; ?>
 <?php else: ?> N/A <?php endif; ?></li>
			   <li>&nbsp;</li>
			   <?php if ($this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['avail_local'] == 'Y'): ?>
			   <li><span>Country (Local meet-up) :</span> <?php if ($this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['availability_country'] != ''):  echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['availability_country'];  else: ?>N/A<?php endif; ?></li>
               <?php endif; ?>
		    </ul>
			<p class="clear" style="padding-bottom:7px;"></p>
			<?php unset($this->_sections['game']);
$this->_sections['game']['name'] = 'game';
$this->_sections['game']['loop'] = is_array($_loop=$this->_tpl_vars['StudentgameArr'][$this->_sections['studentRow']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['game']['show'] = true;
$this->_sections['game']['max'] = $this->_sections['game']['loop'];
$this->_sections['game']['step'] = 1;
$this->_sections['game']['start'] = $this->_sections['game']['step'] > 0 ? 0 : $this->_sections['game']['loop']-1;
if ($this->_sections['game']['show']) {
    $this->_sections['game']['total'] = $this->_sections['game']['loop'];
    if ($this->_sections['game']['total'] == 0)
        $this->_sections['game']['show'] = false;
} else
    $this->_sections['game']['total'] = 0;
if ($this->_sections['game']['show']):

            for ($this->_sections['game']['index'] = $this->_sections['game']['start'], $this->_sections['game']['iteration'] = 1;
                 $this->_sections['game']['iteration'] <= $this->_sections['game']['total'];
                 $this->_sections['game']['index'] += $this->_sections['game']['step'], $this->_sections['game']['iteration']++):
$this->_sections['game']['rownum'] = $this->_sections['game']['iteration'];
$this->_sections['game']['index_prev'] = $this->_sections['game']['index'] - $this->_sections['game']['step'];
$this->_sections['game']['index_next'] = $this->_sections['game']['index'] + $this->_sections['game']['step'];
$this->_sections['game']['first']      = ($this->_sections['game']['iteration'] == 1);
$this->_sections['game']['last']       = ($this->_sections['game']['iteration'] == $this->_sections['game']['total']);
?>
				<h4 class="gameshead" style="padding-bottom:0px; padding-left:2px;"> <?php echo $this->_tpl_vars['StudentgameArr'][$this->_sections['studentRow']['index']][$this->_sections['game']['index']]['game']; ?>
 </h4>
				<p style="padding-top:2px; padding-left:2px;"><b>Experience : </b><?php if ($this->_tpl_vars['StudentgameArr'][$this->_sections['studentRow']['index']][$this->_sections['game']['index']]['experience'] != ''):  echo $this->_tpl_vars['StudentgameArr'][$this->_sections['studentRow']['index']][$this->_sections['game']['index']]['experience']; ?>
 
				<?php else: ?> N/A <?php endif; ?></p>
				<p class="clear" style="padding-bottom:5px;"></p>
			<?php endfor; endif; ?>
            <div class="clear"></div>
            <div class="view"><!--a href="studentdetails.php?student_id=<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
">view</a-->
			<a href="#" onclick="redirect_function(<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
)">view</a>
			</div>
			<div class="clear"></div>
			<div class="preview">
			<a href="javascript:;" style="float:right;" onclick="collapse(<?php echo $this->_tpl_vars['StudentArr'][$this->_sections['studentRow']['index']]['user_id']; ?>
)">fold</a>
			</div>
			</div>
			<div class="clear"></div>
			</form>
      
         <div class="clear"></div>
      </div>
	 <?php endfor; endif; ?> 
     <?php if ($this->_tpl_vars['pagination_arr'][1]): ?>
	 <div class="clear"></div>
      <div class="pagin">
         <ul>
           <?php echo $this->_tpl_vars['pagination_arr'][1]; ?>

         </ul>
      </div>
	 <?php endif; ?>
	 <?php else: ?>
	    No Student is found......
     <?php endif; ?>
	</div> 
   </div>