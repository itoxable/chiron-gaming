<?php /* Smarty version 2.6.16, created on 2013-01-01 15:14:50
         compiled from studentdetails.tpl */ ?>
<?php echo '
<link type="text/css" rel="stylesheet" href="style/lightbox-form.css">
<script src="js/lightbox-form.js" type="text/javascript"></script>
<script language="javascript" src="js/contact_us.js"></script>
<script language="javascript">
function submit_message_text()
{	
	var message_text=jQuery(\'#message_text\').val();
	var user_id_to=jQuery(\'#user_id_to\').val();
	var user_id_from=jQuery(\'#user_id_from\').val();
	var reply_of_msg=jQuery(\'#reply_of_msg\').val();
	
	if(message_text == ""){
		jQuery(\'#message_text\').css(\'border\',\'1px solid #FF0000\');
		return false;
	}	
	else{
		jQuery(\'#message_text\').css(\'border\',\'1px solid #000\');
	
		jQuery.ajax({
			   url:\'reply.php\',
			   data:\'flag=1&message_text=\'+message_text+\'&reply_of_msg=\'+reply_of_msg+\'&user_id_to=\'+user_id_to+\'&user_id_from=\'+user_id_from,
			   type:\'post\',		  
			   success:function(resp){
					jQuery(\'#msgbox\').fadeIn(0);
					jQuery(\'#msgbox\').html(\'Messege sent successfully.An e-mail notification has been successfully sent with this message!\');
					jQuery(\'#msgbox\').fadeOut(5000);
			   } 		   
		});
		
		document.getElementById("message_text").value=\'\';
		closereplybox();
		//ti = setInterval(\'removeDiv()\', 5000);
	}
	
}
/*function removeDiv()
{
clearInterval(ti);
document.getElementById(\'msgbox\').style.display = \'none\';
}	
*/
function closereply()
{
	document.getElementById("message_text").value=\'\';
	jQuery(\'#message_text\').css(\'border\',\'1px solid #ccc\');
	closereplybox();
}
</script>
'; ?>


<div class="right-panel">
      <div class="findstudent-title" style="margin-bottom: 5px;">
        <div class="title" style="float: left;padding: 0;border: none;"><a style="text-decoration: none;" href="findstudent.php">Find Student</a></div>
        <div class="breadcrumb"><?php echo $this->_tpl_vars['StudentArr']['name']; ?>
 </div>
        <div class="clear"></div>
      </div>
	  <div class="clear"></div>    
      <div class="game-list-block">
         <!--h1><?php echo $this->_tpl_vars['StudentArr']['name']; ?>
</h1-->
		    <div class="game-image-block">
			<?php if ($this->_tpl_vars['StudentArr']['photo'] != ''): ?>
                        <img src="uploaded/user_images/thumbs/big_<?php echo $this->_tpl_vars['StudentArr']['photo']; ?>
" alt="<?php echo $this->_tpl_vars['StudentArr']['name']; ?>
" border="0"/>
			<?php else: ?>
			 <img src="images/student_thumb.jpg" border="0" alt="<?php echo $this->_tpl_vars['StudentArr']['name']; ?>
" />
			<?php endif; ?> 
                    </div> 
		    
         <div class="game-details-block">
                     <div class="score_title coachname" >
				<!--<h4 class="gameshead"><?php echo $this->_tpl_vars['StudentArr']['name']; ?>
</h4>-->
				<h2>
					<a class="<?php echo $this->_tpl_vars['online_status']; ?>
" title="<?php echo $this->_tpl_vars['online_status']; ?>
"></a><a href="javascript:;" class="" onclick="redirect_function(<?php echo $this->_tpl_vars['StudentArr']['user_id']; ?>
)" style="text-decoration:none;"><?php echo $this->_tpl_vars['StudentArr']['name']; ?>
</a> 
					<a id="<?php echo $this->_tpl_vars['StudentArr']['user_id']; ?>
_friendStar" class="<?php if ($this->_tpl_vars['isfriend'] == 'y'): ?>star_friend<?php else: ?>star_not_friend<?php endif; ?>" title="<?php if ($this->_tpl_vars['isfriend'] == 'y'): ?>in your friend list<?php else: ?>not your friend list<?php endif; ?>"></a>
				</h2>
				<?php if ($_SESSION['user_id'] != '' && $_SESSION['user_id'] != $this->_tpl_vars['StudentArr']['user_id']): ?>
				<div class="addAsFriendWindow">
					<a class="addAsFriendLink" href="javascript:;" onclick="openChatTab(event,'<?php echo $this->_tpl_vars['StudentArr']['user_id']; ?>
','<?php echo $this->_tpl_vars['StudentArr']['name']; ?>
')">Message</a><br/>
					<?php if ($this->_tpl_vars['isfriend'] == 'n'): ?>
					<a class="addAsFriendLink addFriend" href="javascript:;" onclick="addAsFriend(event,'<?php echo $this->_tpl_vars['StudentArr']['user_id']; ?>
','<?php echo $this->_tpl_vars['StudentArr']['name']; ?>
')">Add as friend</a>
					<?php else: ?>
					<a class="addAsFriendLink removeFriend" href="javascript:;" onclick="removeFriend(event,'<?php echo $this->_tpl_vars['StudentArr']['user_id']; ?>
','<?php echo $this->_tpl_vars['StudentArr']['name']; ?>
')">Remove friend</a>
					<?php endif; ?>
				</div>
				<?php endif; ?>
				
			</div>
                                
                      <div class="clear"></div>          
                    <div>
			<ul class="games-desc">
			   <li><span>Language : </span><?php if ($this->_tpl_vars['StudentArr']['language'] != ''):  echo $this->_tpl_vars['StudentArr']['language'];  else: ?>N/A<?php endif; ?></li>
			  <li><span>Budget : </span><?php if ($this->_tpl_vars['StudentArr']['rate'] != ''): ?>$<?php echo $this->_tpl_vars['StudentArr']['rate'];  else: ?>N/A <?php endif; ?></li>
			  <?php if ($this->_tpl_vars['StudentArr']['avail_local'] == 'Y'): ?>
			  <li><span>City (Local meet-up) : </span><?php if ($this->_tpl_vars['StudentArr']['availability_city'] != ''):  echo $this->_tpl_vars['StudentArr']['availability_city'];  else: ?>N/A<?php endif; ?></li>
			   <?php endif; ?>
			</ul> 
			<ul class="games-desc">
			   <li><span>Country : </span><?php echo $this->_tpl_vars['StudentArr']['country']; ?>
</li>
			   <li><span>Availability : </span><?php echo $this->_tpl_vars['StudentArr']['availability_type']; ?>
</li>
			   <?php if ($this->_tpl_vars['StudentArr']['avail_local'] == 'Y'): ?>
			    <li><span>Country (Local meet-up) : </span><?php if ($this->_tpl_vars['StudentArr']['availability_country'] != ''):  echo $this->_tpl_vars['StudentArr']['availability_country'];  else: ?>N/A<?php endif; ?></li>
			   <?php endif; ?>
			</ul>   
			<p class="clear" style="padding-bottom:5px;"></p><br  />
			
			<?php unset($this->_sections['game']);
$this->_sections['game']['name'] = 'game';
$this->_sections['game']['loop'] = is_array($_loop=$this->_tpl_vars['StudentGameArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['game']['show'] = true;
$this->_sections['game']['max'] = $this->_sections['game']['loop'];
$this->_sections['game']['step'] = 1;
$this->_sections['game']['start'] = $this->_sections['game']['step'] > 0 ? 0 : $this->_sections['game']['loop']-1;
if ($this->_sections['game']['show']) {
    $this->_sections['game']['total'] = $this->_sections['game']['loop'];
    if ($this->_sections['game']['total'] == 0)
        $this->_sections['game']['show'] = false;
} else
    $this->_sections['game']['total'] = 0;
if ($this->_sections['game']['show']):

            for ($this->_sections['game']['index'] = $this->_sections['game']['start'], $this->_sections['game']['iteration'] = 1;
                 $this->_sections['game']['iteration'] <= $this->_sections['game']['total'];
                 $this->_sections['game']['index'] += $this->_sections['game']['step'], $this->_sections['game']['iteration']++):
$this->_sections['game']['rownum'] = $this->_sections['game']['iteration'];
$this->_sections['game']['index_prev'] = $this->_sections['game']['index'] - $this->_sections['game']['step'];
$this->_sections['game']['index_next'] = $this->_sections['game']['index'] + $this->_sections['game']['step'];
$this->_sections['game']['first']      = ($this->_sections['game']['iteration'] == 1);
$this->_sections['game']['last']       = ($this->_sections['game']['iteration'] == $this->_sections['game']['total']);
?>
			 <?php $this->assign('i', $this->_sections['game']['index']+1); ?>
				<p style="padding-left:2px;"><b><?php echo $this->_tpl_vars['StudentGameArr'][$this->_sections['game']['index']]['game']; ?>
</b></p>
				<p style="padding-left:2px;">
				   <b>Experience : </b><?php if ($this->_tpl_vars['StudentGameArr'][$this->_sections['game']['index']]['experience'] != ''):  echo $this->_tpl_vars['StudentGameArr'][$this->_sections['game']['index']]['experience'];  else: ?>N/A<?php endif; ?></li>
				</p><br />
			<?php endfor; endif; ?>
          </div>
            <div class="clear"></div>
			<?php if ($_SESSION['user_id'] != '' && $_SESSION['user_id'] != $this->_tpl_vars['StudentArr']['user_id']): ?>
              <div class="button-flex button70" style="margin-right:<?php if ($_SESSION['user_type'] == '2'):  if ($this->_tpl_vars['is_fav'] == '0'): ?>10<?php else: ?>10<?php endif;  else: ?>10<?php endif; ?>px;">
				<a href="javascript:void(0);" class="button" style="float: right" onClick="openChatTab(event,'<?php echo $this->_tpl_vars['StudentArr']['user_id']; ?>
','<?php echo $this->_tpl_vars['StudentArr']['name']; ?>
')">Message</a>
			</div>
			  <br /> 
			  <div id="msgbox" style="color:#0077BC;"></div>
		    <?php endif; ?>
         </div>
         <div class="clear"></div>
      </div>
	  
      <div class="clear"></div>
  </div> 
<div id="shadowreply"></div>
<div id="replybox">
  <h2><span id="replytitle"></span></h2>
  
   <div class="review">   
   <form name="contact" id="Contact"  method='post' action="" onsubmit="return false;">
<!--   <input type="hidden" name="coach_id" id="coach_id" value="<?php echo $this->_tpl_vars['coach_id']; ?>
" />
   <fieldset>   
    <label>Review :</label>
            <textarea name="review_comment" id="review_comment" cols="" rows="" ><?php echo $this->_tpl_vars['Formval']['user_about']; ?>
</textarea>
   
    <p class="clear"></p>
    <label>&nbsp;</label>
            <input name="submit" class="submit" value="Submit" type="submit" onclick='return CheckFields()' />
            <input name="" class="cancel" value="Cancel" type="button" onClick="closereplybox()" />
			
    </p>
   </fieldset>	-->
   <fieldset> 
   <label>Message :</label>
      <textarea rows="10" cols="40"  name="message_text" id="message_text" class=""></textarea>	  
    <input name="flag" type="hidden" value="1" />
	<input name="user_id_to" id="user_id_to" type="hidden" value="<?php echo $this->_tpl_vars['StudentArr']['user_id']; ?>
" />
    <input name="user_id_from" id="user_id_from" type="hidden" value="<?php echo $_SESSION['user_id']; ?>
" />
    <input name="reply_of_msg" id="reply_of_msg" type="hidden" value="0" /> 
	<p class="clear"></p>
	 <label>&nbsp;</label>            
			<input name="snd_message_text" type="submit" class="submit" value="Submit" onclick="return submit_message_text()"/> 
            <input name="" class="cancel" value="Cancel" type="button" onClick="closereply()" />
			
   </fieldset> 
  </form> 
 </div> 
</div>
