<?php /* Smarty version 2.6.16, created on 2013-02-16 14:10:21
         compiled from profile.tpl */ ?>
<div align="right">
	<?php if ($this->_tpl_vars['is_coach'] == '0'): ?>
	<!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
	<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['is_partner'] == '0'): ?>
	<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
	<!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
	<?php endif; ?> 
</div>
<div class="content">
   		
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'user_tabs.tpl', 'smarty_include_vars' => array('active_tab' => 'profile')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				 
        <div class="clear"></div>
        <div class="tabular-content"> 
			
		 <div class="total-message">&nbsp;&nbsp;</div> 
		 <div class="clear"></div>                     
          <div class="profile-list">
          <br />
		 <?php if ($this->_tpl_vars['userArr']['photo'] != ''): ?> 
		  <div class="game-image"><img src="uploaded/user_images/thumbs/big_<?php echo $this->_tpl_vars['userArr']['photo']; ?>
"></div>
		  <?php else: ?>
		    <?php if ($this->_tpl_vars['is_coach'] == '1'): ?>
			 <div class="game-image"><img src="images/coach_thumb.jpg"></div>
			<?php else: ?>
			 <div class="game-image"><img src="images/student_thumb.jpg"></div>
			<?php endif; ?>
		 <?php endif; ?>  
		 
         <div class="game-details user_details" style="width: 80%">
		    <!--div><?php if ($this->_tpl_vars['msg'] != ''): ?> <?php echo $this->_tpl_vars['msg']; ?>
 <?php endif; ?></div-->
			<div class="clear"></div>
			<dl>
				<dt>Profile Type:</dt>
				<dd><?php echo $this->_tpl_vars['userArr']['type']; ?>
</dd>
			</dl>
			<dl>
				<dt>Username:</dt>
				<dd><?php echo $this->_tpl_vars['userArr']['username']; ?>
</dd>
			</dl>
			<dl>
				<dt>Email:</dt>
				<dd><?php echo $this->_tpl_vars['userArr']['email']; ?>
</dd>
			</dl>
                        
                        <dl>
				<dt>Timezone:</dt>
				<dd><?php echo $this->_tpl_vars['time_zone']; ?>
</dd>
			</dl>
                        
			<dl>
				<dt>Password:</dt>
				<dd><?php echo $this->_tpl_vars['userArr']['pass_word']; ?>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="change_password.php" style="color:#0033FF;">Change</a></dd>
			</dl>
		
			<?php unset($this->_sections['type']);
$this->_sections['type']['name'] = 'type';
$this->_sections['type']['loop'] = is_array($_loop=$this->_tpl_vars['typeArrr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['type']['show'] = true;
$this->_sections['type']['max'] = $this->_sections['type']['loop'];
$this->_sections['type']['step'] = 1;
$this->_sections['type']['start'] = $this->_sections['type']['step'] > 0 ? 0 : $this->_sections['type']['loop']-1;
if ($this->_sections['type']['show']) {
    $this->_sections['type']['total'] = $this->_sections['type']['loop'];
    if ($this->_sections['type']['total'] == 0)
        $this->_sections['type']['show'] = false;
} else
    $this->_sections['type']['total'] = 0;
if ($this->_sections['type']['show']):

            for ($this->_sections['type']['index'] = $this->_sections['type']['start'], $this->_sections['type']['iteration'] = 1;
                 $this->_sections['type']['iteration'] <= $this->_sections['type']['total'];
                 $this->_sections['type']['index'] += $this->_sections['type']['step'], $this->_sections['type']['iteration']++):
$this->_sections['type']['rownum'] = $this->_sections['type']['iteration'];
$this->_sections['type']['index_prev'] = $this->_sections['type']['index'] - $this->_sections['type']['step'];
$this->_sections['type']['index_next'] = $this->_sections['type']['index'] + $this->_sections['type']['step'];
$this->_sections['type']['first']      = ($this->_sections['type']['iteration'] == 1);
$this->_sections['type']['last']       = ($this->_sections['type']['iteration'] == $this->_sections['type']['total']);
?>
			 <?php if ($this->_tpl_vars['typeArr'][$this->_sections['type']['index']]['user_type_id'] == '1'): ?><div style="padding:3px 0 3px 0;">Paypal Email ID : <?php echo $this->_tpl_vars['userArr']['paypal_email_id']; ?>
</div><?php endif; ?>
			<?php endfor; endif; ?>
			
			
			<dl>
				<dt>Language:</dt>
				<dd><?php if ($this->_tpl_vars['userArr']['language'] != ''):  echo $this->_tpl_vars['userArr']['language'];  else: ?>N/A<?php endif; ?></dd>
			</dl>

            <p class="clear"></p>
            <div class="clear"></div>
			<div style="margin-top: 10px">
                            <a class="button" href="edit_profile.php"><span>edit profile</span></a>
                            <a class="button" href="user_settings.php"><span>settings</span></a>
			</div>
            
         </div>
         <div class="clear"></div>
      
	  </div>
     </div>
    </div>
                        
 <script language="javascript">
       changePageTitle('Profile');
</script>