<?php /* Smarty version 2.6.16, created on 2013-01-20 14:17:54
         compiled from add_lesson.tpl */ ?>
<?php echo '
<script language="javascript1.2" src="includejs/functions_admin.js" type="text/javascript"></script>

<script type="text/javascript">
jQuery(document).ready(function(){
		
focus_blur(\'Search_lesson_title\',\'Search by title\');
focus_blur(\'Search_from_date\',\'From\');
focus_blur(\'Search_to_date\',\'To\');
focus_blur(\'Search_from_date_in\',\'From\');
focus_blur(\'Search_to_date_in\',\'To\');
});

function GetTotalIncome(){
	if(document.getElementById(\'tot_incm\').checked==true){
			document.getElementById(\'show_tot\').style.display=\'block\';
	}
	else{
			document.getElementById(\'show_tot\').style.display=\'none\';
	}
}

function GetTotalPrice(){
	if(document.getElementById(\'tot_price\').checked==true){
			document.getElementById(\'price_tot\').style.display=\'block\';
	}
	else{
			document.getElementById(\'price_tot\').style.display=\'none\';
	}
}

function Search_Income(formID){

	var frmID=\'#\'+formID;
	
	var params ={
	
		\'module\': \'contact\'
	};

	var paramsObj = jQuery(frmID).serializeArray();

	jQuery.each(paramsObj, function(i, field){

		params[field.name] = field.value;

	});
	
	jQuery.ajax({
		type: "POST",
		url:  \'income_search.php\',
		data: params,
		dataType : \'json\',
		success: function(data){
			if(data.total_in != \'\'){
				document.getElementById(\'lable_total\').style.display="block";
				jQuery(\'#total_income\').html(data.total_in);
			}
			else{
				document.getElementById(\'lable_total\').style.display="block";
				jQuery(\'#total_income\').html(\'0\');
			}
        }
      });
}

function SearchLesson(formID){

	var frmID=\'#\'+formID;

	var params ={
	
		\'module\': \'contact\'
	};

	var paramsObj = v(frmID).serializeArray();

	jQuery.each(paramsObj, function(i, field){

		params[field.name] = field.value;

	});
	
	j.ajax({
		type: "POST",
		url:  \'add_lesson.php?action=list_search&IsProcess=Y\',
		data:  params,
		dataType : \'html\',
		success: function(data){
			//alert(data);
			jQuery(\'#show\').html(data);
        }
      });
}
</script>
'; ?>

<div id="paging"> 

<div id="show">
	
	<div class="clear"></div>
	
	
	
	<!--<div align="right"><p class="button-flex-big button150"><a href="lesson_update.php"><span>Add Lesson</span></a></p></div>-->
	<div align="right">
		<?php if ($this->_tpl_vars['is_coach'] == '0'): ?>
		<!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
		<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['is_partner'] == '0'): ?>
		<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
		<!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
		<?php endif; ?> 
	</div>
	<div class="content">
   		
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'user_tabs.tpl', 'smarty_include_vars' => array('active_tab' => 'coach')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				
        <div class="clear"></div>
        <div class="tabular-content"> 
		 <div class="total-message"><strong>Total Lessons : <?php echo $this->_tpl_vars['Numcoachgame']; ?>
</strong>&nbsp;&nbsp;</div> 
		    
			<div class="tab_sub">
            	<ul>
					<li><a href="calendar.php">Availability</a></li>
                    <li><a href="coach_game.php">My Games</a></li>
					<li><a href="javascript:;" class="active">My Lessons</a></li>
                </ul>
			</div>

            
            <div class="clear"></div>  
			<div class="dashboard_area">
	<div class="dashboard_block new-income">
	
	<h2>Income Search</h2>
		<form method="POST" name="form_search_in" id="form_search_in" action="my_lesson.php" onsubmit="return false;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="new">
				<tr>
				<td width="30%" style="color:#000000; padding-top:10px;">Check to see total income: </td>
				<td width="70%" ><input type="checkbox" name="tot_incm" id="tot_incm" onclick="GetTotalIncome();" style="float:left;" />&nbsp;<span style="color:#000000; display:none; width:280px; float:left; padding-left:3px;" id="show_tot">Total Income: &nbsp;<b>$</b><?php echo $this->_tpl_vars['tot_com']; ?>
&nbsp;&nbsp;&nbsp;&nbsp;Total Price: &nbsp;<b>$</b><?php echo $this->_tpl_vars['total_price']; ?>
</span>
				</td>
				</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="new">
				<tr>
				<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
				<td colspan="2">&nbsp;</td></tr>
				<tr>
				<td width="60%" align="left" valign="middle">
					<div style="float:left;">
					<input type="text" name="Search_from_date_in" id="Search_from_date_in" class="datebox" value="<?php echo $this->_tpl_vars['Search_from_date_in']; ?>
" style="width:80px;" readonly=""/>
					<!--<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search_in.Search_from_date_in,document.form_search_in.Search_from_date_in);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>-->
					</div>
					
					<div style="float:right;">
					<input type="text" name="Search_to_date_in" id="Search_to_date_in" class="datebox" value="<?php echo $this->_tpl_vars['Search_to_date_in']; ?>
" style="width:80px;" readonly=""/>
					<!--<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search_in.Search_from_date_in,document.form_search_in.Search_to_date_in);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>-->
					</div>
				</td>
				<td valign="middle" style="padding-left:10px;">
					<input type="hidden" name="in_search" value="DO">
					
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				</tr>
				<tr>
				
				<td><input class="go"  name="submitdosearch" type="button" value="GO" onClick="Search_Income('form_search_in');" />
					<input class="reset" name="" type="button" value="RESET" onClick="window.location.href='add_lesson.php';"/>
					<span id="lable_total" style="display:none; float:left; padding-top:5px; padding-left:10px;">Total Income :&nbsp;<b>$</b>&nbsp;</span><span id="total_income" style="float:left; padding-top:5px;"></span>
					</td>
					
				
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<!--<tr>	
					<td colspan="2" valign="middle">
					<input type="hidden" name="in_search" value="DO">
					<input class="go"  name="submitdosearch" type="button" value="GO" onClick="Search_Income('form_search_in');" />
					<input class="reset" name="" type="button" value="RESET" onClick="window.location.href='my_lesson.php';"/></td>
				</tr>-->
			</table>
		</form>	
		</div>
		
		<div class="dashboard_block new">
		<h2>General Search</h2>
	  	<form method="POST" name="form_search" id="form_search" action="my_lesson.php" onsubmit="return false;">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr>
			  <td width="30%" align="left" valign="middle"><input size="30" type="text" name="Search_lesson_title" id="Search_lesson_title" value="<?php echo $this->_tpl_vars['Search_lesson_title']; ?>
" class="textbox-search-box"/></td>
			  <td width="70%" align="left" valign="middle">
				<div style="float:left;">
					<input type="text" name="Search_from_date" id="Search_from_date" class="datebox" value="<?php echo $this->_tpl_vars['Search_from_date']; ?>
" style="width:80px;"/>
					
					<!--<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_from_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>	-->			</div>
				<div style="float:left; padding-left:10px;">
					<input type="text" name="Search_to_date" id="Search_to_date" class="datebox" value="<?php echo $this->_tpl_vars['Search_to_date']; ?>
" style="width:80px;"/>
			  <!--<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_to_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>	-->			</div>			  </td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr>
					<td width="30%" align="left" valign="middle" style="color:#000000; padding-left:5px;  ">
					<select name="Search_student_id" id="Search_student_id">
						<option value="">--Select Student--</option>
						<?php unset($this->_sections['Rows']);
$this->_sections['Rows']['name'] = 'Rows';
$this->_sections['Rows']['loop'] = is_array($_loop=$this->_tpl_vars['SelectStudentArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['Rows']['show'] = true;
$this->_sections['Rows']['max'] = $this->_sections['Rows']['loop'];
$this->_sections['Rows']['step'] = 1;
$this->_sections['Rows']['start'] = $this->_sections['Rows']['step'] > 0 ? 0 : $this->_sections['Rows']['loop']-1;
if ($this->_sections['Rows']['show']) {
    $this->_sections['Rows']['total'] = $this->_sections['Rows']['loop'];
    if ($this->_sections['Rows']['total'] == 0)
        $this->_sections['Rows']['show'] = false;
} else
    $this->_sections['Rows']['total'] = 0;
if ($this->_sections['Rows']['show']):

            for ($this->_sections['Rows']['index'] = $this->_sections['Rows']['start'], $this->_sections['Rows']['iteration'] = 1;
                 $this->_sections['Rows']['iteration'] <= $this->_sections['Rows']['total'];
                 $this->_sections['Rows']['index'] += $this->_sections['Rows']['step'], $this->_sections['Rows']['iteration']++):
$this->_sections['Rows']['rownum'] = $this->_sections['Rows']['iteration'];
$this->_sections['Rows']['index_prev'] = $this->_sections['Rows']['index'] - $this->_sections['Rows']['step'];
$this->_sections['Rows']['index_next'] = $this->_sections['Rows']['index'] + $this->_sections['Rows']['step'];
$this->_sections['Rows']['first']      = ($this->_sections['Rows']['iteration'] == 1);
$this->_sections['Rows']['last']       = ($this->_sections['Rows']['iteration'] == $this->_sections['Rows']['total']);
?>
							<option value="<?php echo $this->_tpl_vars['SelectStudentArr'][$this->_sections['Rows']['index']]['student_id']; ?>
" <?php if ($this->_tpl_vars['SelectStudentArr'][$this->_sections['Rows']['index']]['student_id'] == $this->_tpl_vars['Search_student_id']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['SelectStudentArr'][$this->_sections['Rows']['index']]['student_name']; ?>
</option>
						<?php endfor; endif; ?>
					</select>
			  </td>
					<td colspan="2" align="left" valign="middle" style="color:#000000; padding-left:5px; display:block;">
				<select name="Search_lesson_status" id="Search_lesson_status">
					<option value="">--Select Status--</option>
					<?php unset($this->_sections['Rows']);
$this->_sections['Rows']['name'] = 'Rows';
$this->_sections['Rows']['loop'] = is_array($_loop=$this->_tpl_vars['SelectStatusArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['Rows']['show'] = true;
$this->_sections['Rows']['max'] = $this->_sections['Rows']['loop'];
$this->_sections['Rows']['step'] = 1;
$this->_sections['Rows']['start'] = $this->_sections['Rows']['step'] > 0 ? 0 : $this->_sections['Rows']['loop']-1;
if ($this->_sections['Rows']['show']) {
    $this->_sections['Rows']['total'] = $this->_sections['Rows']['loop'];
    if ($this->_sections['Rows']['total'] == 0)
        $this->_sections['Rows']['show'] = false;
} else
    $this->_sections['Rows']['total'] = 0;
if ($this->_sections['Rows']['show']):

            for ($this->_sections['Rows']['index'] = $this->_sections['Rows']['start'], $this->_sections['Rows']['iteration'] = 1;
                 $this->_sections['Rows']['iteration'] <= $this->_sections['Rows']['total'];
                 $this->_sections['Rows']['index'] += $this->_sections['Rows']['step'], $this->_sections['Rows']['iteration']++):
$this->_sections['Rows']['rownum'] = $this->_sections['Rows']['iteration'];
$this->_sections['Rows']['index_prev'] = $this->_sections['Rows']['index'] - $this->_sections['Rows']['step'];
$this->_sections['Rows']['index_next'] = $this->_sections['Rows']['index'] + $this->_sections['Rows']['step'];
$this->_sections['Rows']['first']      = ($this->_sections['Rows']['iteration'] == 1);
$this->_sections['Rows']['last']       = ($this->_sections['Rows']['iteration'] == $this->_sections['Rows']['total']);
?>
						<option value="<?php echo $this->_tpl_vars['SelectStatusArr'][$this->_sections['Rows']['index']]['lesson_status_id']; ?>
" <?php if ($this->_tpl_vars['SelectStatusArr'][$this->_sections['Rows']['index']]['lesson_status_id'] == $this->_tpl_vars['Search_lesson_status']): ?> selected <?php endif; ?>><?php echo $this->_tpl_vars['SelectStatusArr'][$this->_sections['Rows']['index']]['lesson_status_title']; ?>
</option>
					<?php endfor; endif; ?>
				</select>			  </td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>	
				<td valign="middle">
					<input class="go"  name="submitdosearch" value="GO" type="button" onClick="SearchLesson('form_search')" />
					<input class="reset" name="" type="button" value="RESET" onClick="window.location.href='add_lesson.php';"/>
				</td>
			</tr>
	  </table>
</form>
	  </div>
		
		<div class="clear"></div>
		  
	</div>                   
            <div>
                 
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row tabular-data" border="1">
				<?php if ($this->_tpl_vars['Numcoachgame'] != 0): ?>
				  <tr style="border-bottom:#000000 1px solid; background:#ebebeb">
				  	<th width="10%" align="center">Title</th>
					<th width="10%" align="center">Player</th>
					<th width="5%" align="center">Points</th>
					<th width="5%" align="center">My Income</th>
					<th width="5%" align="center">Commission fee(%)</th>
					<th width="5%" align="center">Status</th>
					<th width="13%" align="center">Start date</th>
                                        <th width="12%" align="center">Date added</th>
				  </tr>
				  <?php unset($this->_sections['gameRow']);
$this->_sections['gameRow']['name'] = 'gameRow';
$this->_sections['gameRow']['loop'] = is_array($_loop=$this->_tpl_vars['CoachgameArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['gameRow']['show'] = true;
$this->_sections['gameRow']['max'] = $this->_sections['gameRow']['loop'];
$this->_sections['gameRow']['step'] = 1;
$this->_sections['gameRow']['start'] = $this->_sections['gameRow']['step'] > 0 ? 0 : $this->_sections['gameRow']['loop']-1;
if ($this->_sections['gameRow']['show']) {
    $this->_sections['gameRow']['total'] = $this->_sections['gameRow']['loop'];
    if ($this->_sections['gameRow']['total'] == 0)
        $this->_sections['gameRow']['show'] = false;
} else
    $this->_sections['gameRow']['total'] = 0;
if ($this->_sections['gameRow']['show']):

            for ($this->_sections['gameRow']['index'] = $this->_sections['gameRow']['start'], $this->_sections['gameRow']['iteration'] = 1;
                 $this->_sections['gameRow']['iteration'] <= $this->_sections['gameRow']['total'];
                 $this->_sections['gameRow']['index'] += $this->_sections['gameRow']['step'], $this->_sections['gameRow']['iteration']++):
$this->_sections['gameRow']['rownum'] = $this->_sections['gameRow']['iteration'];
$this->_sections['gameRow']['index_prev'] = $this->_sections['gameRow']['index'] - $this->_sections['gameRow']['step'];
$this->_sections['gameRow']['index_next'] = $this->_sections['gameRow']['index'] + $this->_sections['gameRow']['step'];
$this->_sections['gameRow']['first']      = ($this->_sections['gameRow']['iteration'] == 1);
$this->_sections['gameRow']['last']       = ($this->_sections['gameRow']['iteration'] == $this->_sections['gameRow']['total']);
?>	  				
				  <tr>
				  	<td width="10%" align="center"><?php echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['lesson_title']; ?>
</td>
					<td width="10%" align="center"><?php if ($this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['student_name'] != ''):  echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['student_name'];  else: ?>N/A<?php endif; ?></td>
					<td width="5%" align="center"><?php if ($this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['lesson_price']): ?>$<?php echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['lesson_price'];  else: ?>N/A<?php endif; ?></td>
					<td width="5%" align="center"><?php if ($this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['lesson_price']): ?>$<?php echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['my_commision'];  else: ?>N/A<?php endif; ?></td>
					<td width="5%" align="center"><?php if ($this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['lesson_price']):  echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['lesson_commision_admin'];  else: ?>N/A<?php endif; ?></td>
					<td width="5%" align="center"><?php if ($this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['lesson_status']):  echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['lesson_status'];  else: ?>N/A<?php endif; ?></td>
                                        <td width="13%" align="center"><?php if ($this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['start'] != '0000-00-00'):  echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['start'];  else: ?>N/A<?php endif; ?></td>
					<td width="12%" align="center"><?php if ($this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['start'] != '0000-00-00'):  echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['date_added'];  else: ?>N/A<?php endif; ?></td>
				  </tr>
	  			  <?php endfor; endif; ?> 
				<?php else: ?>
				  <tr>
                    <td colspan="6"><center><strong>No Record Exists</strong></center></td>
                  </tr>		
				<?php endif; ?>                                
                </table>
            </div>
        	<div class="clear"></div>
          
        </div>
    </div>
</div>
<?php echo ' 
<script>
	jQuery(function() {
		jQuery( "#Search_from_date_in" ).datepicker();
		jQuery( "#Search_to_date_in" ).datepicker();
		jQuery( "#Search_from_date" ).datepicker();
		jQuery( "#Search_to_date" ).datepicker();
	});
</script>
'; ?>
 
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>
</div>
<script language="javascript">
       changePageTitle('My lessons');
</script>