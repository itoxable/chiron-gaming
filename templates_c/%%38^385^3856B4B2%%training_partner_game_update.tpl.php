<?php /* Smarty version 2.6.16, created on 2013-04-20 17:50:39
         compiled from training_partner_game_update.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'training_partner_game_update.tpl', 177, false),)), $this); ?>
 <?php echo '

 <script language="javascript" src="js/contact_us.js"></script> 
 <link rel="stylesheet" href="style/autosuggest.css" type="text/css" />
 <script type="text/javascript" src="js/jquery.ausu-autosuggest.min.js"></script>

 <script type="text/javascript">
        jQuery(document).ready(function() {
         
             <!-- AUTOCOMPLETE -->
			 jQuery.fn.autosugguest({  
				   className: \'ausu-suggest\',
				  methodType: \'POST\',
					minChars: 2,
					  rtnIDs: true,
					dataFile: \'autocomplete.php?action=populateCity\'
			});
        });
    </script>
<script language="javascript" type="text/javascript">



function getChangeGame(id)
{
	jQuery.post(\'game_details.php\',{\'game_id\':id, \'action\':\'get_content\'}, function(data){
		jQuery(\'#dynamic_controls\').html(data);
	});
}
function CheckFields()
{
    if(document.getElementById(\'profile\').value ==\'Y\' && document.getElementById(\'is_coach\').value ==\'0\')
	{
	  if(document.getElementById(\'avail_L\').checked == true)
     {
		  if(document.getElementById(\'availability_city\').value==\'\')
		  {
			jQuery(\'#FormErrorMsg\').show();
			jQuery(\'#FormErrorMag\').html("Please enter availability city");
			jQuery(\'#availability_city\').attr(\'style\',\'border:1px solid #FF0000\');
			return false; 
		  }	
		 
      }
   }	  	  
 
  if(document.getElementById(\'game_id\').value==\'\' ) 
   {
    jQuery(\'#FormErrorMsg\').show();
    jQuery(\'#FormErrorMsg\').html("Please select a game");
	//jQuery(\'#FormFieldCheckErrorMsg\').fadeOut(2000);
    return false;
   }
   
  return true;
 }  
 function showhide_div(){
    if(document.getElementById(\'avail_L\').checked == true){  
        jQuery(\'#avail_city\').show();
        jQuery(\'#availability_city\').show();
    }
    if(document.getElementById(\'avail_L\').checked == false){  		
        jQuery(\'#avail_city\').hide();
        jQuery(\'#availability_city\').hide();
    }  			
} 

 function keyCheck(eventObj, obj)
{
    var keyCode
 
    // Check For Browser Type
    if (document.all){
        keyCode=eventObj.keyCode
    }
    else{
        keyCode=eventObj.which
    }
 
    var str=obj.value
 
    if(keyCode==46){
        if (str.indexOf(".")>0){
            return false
        }
    }
 
    if((keyCode<46 || keyCode >57)   &&   (keyCode >31)){ // Allow only integers and decimal points
        return false
    }
    
    return true
}
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}


</script>
'; ?>

<div align="right">
	<?php if ($this->_tpl_vars['is_coach'] == '0'): ?>
	<!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
	<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['is_partner'] == '0'): ?>
	<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
	<!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
	<?php endif; ?> 
</div>
<div class="content">
   		<!-- tab -->
    	<ul class="tabflex">
			<!--    <li><a href="#"><span>Students</span></a></li>-->
			<li><a href="schedule.php"><span>Schedule</span></a></li>
			<li><a href="messages.php"><span>Messages</span></a></li>
			<li><a href="profile.php"><span>Profile</span></a></li>
			<?php if ($this->_tpl_vars['is_coach'] == '1'): ?><li><a href="coach_game.php"><span>Coach</span></a></li><?php endif; ?>
			<?php if ($this->_tpl_vars['is_partner'] == '1'): ?><li><a href="training_partner_game.php" class="active"><span>Training Partner</span></a></li><?php endif; ?>
			<li><a href="cashier.php"><span>Cashier</a></li>
        </ul>
        <!-- tab -->
        <div class="clear"></div>
        <div class="tabular-content"> 
            <div class="total-message">&nbsp;&nbsp;</div>
            <div class="clear"></div>                     
            <div>
                <div class="register" style="margin-top: 20px; width: auto">
                    <div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:10px;"><?php if ($this->_tpl_vars['ermsg'] != ''): ?> <?php echo $this->_tpl_vars['ermsg'];  endif; ?> </div>
                    <form name="game" id="game"  method="post" action="">
                        <input type="hidden" name="user_game_id" value="<?php echo $this->_tpl_vars['user_game_id']; ?>
" />
                        <input type="hidden" name="profile" id="profile" value="<?php echo $this->_tpl_vars['profile']; ?>
" />
                        <input type="hidden" name="is_coach" id="is_coach" value="<?php echo $this->_tpl_vars['is_coach']; ?>
" />
                        <input type="hidden" name="is_partner" id="is_partner" value="<?php echo $this->_tpl_vars['is_partner']; ?>
" />
                         <?php if ($this->_tpl_vars['profile'] == 'Y' && $this->_tpl_vars['is_partner'] == '0'): ?>
                            <fieldset>
                                <label>Introduction:</label>
                                <div style="float: left">
                                    <textarea name="about-text" maxlength="300" onkeyup="countLenght(this,'about-lenght')" id="about-text"></textarea>
                                    <p class="clear"></p>
                                    <span id="about-lenght">0/300</span>
                                    <div style="margin-top: 10px;width: 588px;color: red;border: 1px solid red;padding: 3px;"><center>This Intro will be displayed on the Find Training Partner page before users view your full profile. <br>There is a 300-character limit, so write carefully!<br></center></div>
                         
                                </div>
                                <p class="clear"></p>
                            </fieldset>
                            <div style="margin: 20px; margin-top: 0; border-bottom: 1px solid #ccc">&nbsp;&nbsp;</div>    
                            <?php endif; ?>
                            <fieldset>
                            <?php if ($this->_tpl_vars['profile'] == 'Y' && $this->_tpl_vars['is_partner'] == '0'): ?>
                            <label>Availability :</label>
                            <input type="checkbox" name="availability_type1" class='check' value="O" /><label class='chklabel'>Online</label>
                            <input type="checkbox" name="availability_type2" class='check' id="avail_L" onclick="showhide_div()" value="L" /><label class='chklabel'>Local</label>
                            <p class="clear"></p>
			
                            <label id="avail_city" <?php if ($this->_tpl_vars['Formval']['availability_type'] == 'L'): ?> <?php else: ?> style='display:none' <?php endif; ?>>City (Local meet-up) :</label>
                            <div class="ausu-suggest">
                                <input name="availability_city" id="availability_city" type="text" value="<?php echo $this->_tpl_vars['Formval']['availability_city']; ?>
" class="con-req" autocomplete="off" 
                                <?php if ($this->_tpl_vars['Formval']['availability_type'] == 'L'): ?> <?php else: ?> style='display:none' <?php endif; ?>/>
                                <input name="cityid" id="cityid" type="hidden"  value="" autocomplete="off" />
                            </div>
			
                            <div class="clear"></div>
			 
                            <p class="clear"></p>
                            <?php endif; ?>
                            <div class="clear"></div>
                            
                            
                            <label>Select Game :</label>
                            <select name="game_id" id="game_id" class="con-req" onchange="getChangeGame(this.value)" >
				<option value="">Select</option>
				<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['GameArr'],'selected' => $this->_tpl_vars['Formval']['game_id']), $this);?>

                            </select>
                            <!--label>Active :</label>
                            <input name="is_active" id="is_active" type="checkbox" value="Y" <?php if ($this->_tpl_vars['Formval']['is_active'] == ''): ?> checked="checked" <?php else:  if ($this->_tpl_vars['Formval']['is_active'] == 'Y'): ?>checked<?php endif;  endif; ?>-->
                            <p class="clear"></p>
                            <label>Peak Hours:</label>
                            <input name="peak_hours" id="peak_hours" type="text" value="<?php echo $this->_tpl_vars['Formval']['peak_hours']; ?>
"/>
                            <p class="clear"></p>
			
                            <!--THE DYNAMIC CONTROL WILL PRINT HERE - PROSENJIT-->
                            <?php unset($this->_sections['categoryIndex']);
$this->_sections['categoryIndex']['name'] = 'categoryIndex';
$this->_sections['categoryIndex']['loop'] = is_array($_loop=$this->_tpl_vars['gameCategoriesArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['categoryIndex']['show'] = true;
$this->_sections['categoryIndex']['max'] = $this->_sections['categoryIndex']['loop'];
$this->_sections['categoryIndex']['step'] = 1;
$this->_sections['categoryIndex']['start'] = $this->_sections['categoryIndex']['step'] > 0 ? 0 : $this->_sections['categoryIndex']['loop']-1;
if ($this->_sections['categoryIndex']['show']) {
    $this->_sections['categoryIndex']['total'] = $this->_sections['categoryIndex']['loop'];
    if ($this->_sections['categoryIndex']['total'] == 0)
        $this->_sections['categoryIndex']['show'] = false;
} else
    $this->_sections['categoryIndex']['total'] = 0;
if ($this->_sections['categoryIndex']['show']):

            for ($this->_sections['categoryIndex']['index'] = $this->_sections['categoryIndex']['start'], $this->_sections['categoryIndex']['iteration'] = 1;
                 $this->_sections['categoryIndex']['iteration'] <= $this->_sections['categoryIndex']['total'];
                 $this->_sections['categoryIndex']['index'] += $this->_sections['categoryIndex']['step'], $this->_sections['categoryIndex']['iteration']++):
$this->_sections['categoryIndex']['rownum'] = $this->_sections['categoryIndex']['iteration'];
$this->_sections['categoryIndex']['index_prev'] = $this->_sections['categoryIndex']['index'] - $this->_sections['categoryIndex']['step'];
$this->_sections['categoryIndex']['index_next'] = $this->_sections['categoryIndex']['index'] + $this->_sections['categoryIndex']['step'];
$this->_sections['categoryIndex']['first']      = ($this->_sections['categoryIndex']['iteration'] == 1);
$this->_sections['categoryIndex']['last']       = ($this->_sections['categoryIndex']['iteration'] == $this->_sections['categoryIndex']['total']);
?>
                            <label class="game-extra"><?php echo $this->_tpl_vars['gameCategoriesArr'][$this->_sections['categoryIndex']['index']]['category_name']; ?>
:</label>
                                <div>
                                    <?php unset($this->_sections['propIndex']);
$this->_sections['propIndex']['name'] = 'propIndex';
$this->_sections['propIndex']['loop'] = is_array($_loop=$this->_tpl_vars['gameCategoriesArr'][$this->_sections['categoryIndex']['index']]['properties']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['propIndex']['show'] = true;
$this->_sections['propIndex']['max'] = $this->_sections['propIndex']['loop'];
$this->_sections['propIndex']['step'] = 1;
$this->_sections['propIndex']['start'] = $this->_sections['propIndex']['step'] > 0 ? 0 : $this->_sections['propIndex']['loop']-1;
if ($this->_sections['propIndex']['show']) {
    $this->_sections['propIndex']['total'] = $this->_sections['propIndex']['loop'];
    if ($this->_sections['propIndex']['total'] == 0)
        $this->_sections['propIndex']['show'] = false;
} else
    $this->_sections['propIndex']['total'] = 0;
if ($this->_sections['propIndex']['show']):

            for ($this->_sections['propIndex']['index'] = $this->_sections['propIndex']['start'], $this->_sections['propIndex']['iteration'] = 1;
                 $this->_sections['propIndex']['iteration'] <= $this->_sections['propIndex']['total'];
                 $this->_sections['propIndex']['index'] += $this->_sections['propIndex']['step'], $this->_sections['propIndex']['iteration']++):
$this->_sections['propIndex']['rownum'] = $this->_sections['propIndex']['iteration'];
$this->_sections['propIndex']['index_prev'] = $this->_sections['propIndex']['index'] - $this->_sections['propIndex']['step'];
$this->_sections['propIndex']['index_next'] = $this->_sections['propIndex']['index'] + $this->_sections['propIndex']['step'];
$this->_sections['propIndex']['first']      = ($this->_sections['propIndex']['iteration'] == 1);
$this->_sections['propIndex']['last']       = ($this->_sections['propIndex']['iteration'] == $this->_sections['propIndex']['total']);
?>
                                     <span>
                                        <input class="checkbox_" type="checkbox" <?php if ($this->_tpl_vars['gameCategoriesArr'][$this->_sections['categoryIndex']['index']]['properties'][$this->_sections['propIndex']['index']]['checked']): ?>checked<?php endif; ?> name="prop_id[]" value="<?php echo $this->_tpl_vars['gameCategoriesArr'][$this->_sections['categoryIndex']['index']]['category_id']; ?>
_<?php echo $this->_tpl_vars['gameCategoriesArr'][$this->_sections['categoryIndex']['index']]['properties'][$this->_sections['propIndex']['index']]['property_id']; ?>
" />&nbsp;<?php echo $this->_tpl_vars['gameCategoriesArr'][$this->_sections['categoryIndex']['index']]['properties'][$this->_sections['propIndex']['index']]['property_name']; ?>
&nbsp;&nbsp;
                                    </span>

                                    <?php endfor; endif; ?>
                                </div>
                                <br class="clear" />
                            <?php endfor; endif; ?>
                            
                            <p class="clear"></p>
                            <label>Experience :</label>
                            <textarea  name="experience" id="experience"><?php echo $this->_tpl_vars['Formval']['experience']; ?>
</textarea>
                            <p class="clear"></p>
                            <label>Need Improvement In :</label>
                            <textarea  name="need_improvement" id="need_improvement"><?php echo $this->_tpl_vars['Formval']['need_improvement']; ?>
</textarea>
                            <p class="clear"></p>
                            <?php if ($this->_tpl_vars['profile'] == 'Y' && $this->_tpl_vars['is_coach'] == '0'): ?>
                            <label>About Me :</label>
                            <textarea  name="description" id="description" onKeyDown="limitText(this.form.description,this.form.countdown,300);"onKeyUp="limitText(this.form.description,this.form.countdown,300);"></textarea>
                            <p class="clear"></p>
                            <label>Characters left </label><input readonly type="text" name="countdown" size="3" value="300" class="tiny">
                            <label></label>(Maximum characters: 300)
                            <p class="clear"></p>
                            <?php endif; ?>
                            <label>&nbsp;</label>
                            <input name="submit" class="submit" value="<?php echo $this->_tpl_vars['SubmitButton']; ?>
" type="submit" onclick='return CheckFields()' />
                            <input name="" class="cancel" value="Cancel" type="button" onclick="window.location.href='training_partner_game.php'" />
                        </fieldset>
                    </form>
                </div>
      
            </div>
        </div>
    </div>
        <script language="javascript">
            changePageTitle('Training partner - Edit game');
        </script>