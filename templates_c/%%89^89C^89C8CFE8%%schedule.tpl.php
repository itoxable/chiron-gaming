<?php /* Smarty version 2.6.16, created on 2013-02-12 17:52:49
         compiled from schedule.tpl */ ?>
<div id="show">
		
	 <div class="clear"></div>
		<div align="right">
			<?php if ($this->_tpl_vars['is_coach'] == '0'): ?>
			<!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
			<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['is_partner'] == '0'): ?>
			<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
			<!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
			<?php endif; ?> 
		</div>
		<div class="content"> 
   		
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'user_tabs.tpl', 'smarty_include_vars' => array('active_tab' => 'schedule')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				
        <div class="clear"></div>
        <div class="tabular-content" style="height: 480px;">
			<div class="total-message">&nbsp;&nbsp;</div> 
			<div class="clear"></div> 
			<div class="content-wrapper" style="position: relative;">
				<div class="schedule-links">
					<a href="javascript:;" onclick="changeSelection(this);loadPast();">Past</a>
					<span class="">|</span>
					<a href="javascript:;" class="selected" onclick="changeSelection(this);loadToday(0);">Today</a>
					<span class="">|</span>
					<a href="javascript:;" onclick="changeSelection(this);loadToday(1)">Tomorrow</a>
					<span class="">|</span>
					<a href="javascript:;" onclick="changeSelection(this);loadWeek();">This Week</a>
					<span class="">|</span>
					<a href="javascript:;" onclick="changeSelection(this);loadMonth();" >This Month</a>
					<span class="">|</span>
					<a href="javascript:;" onclick="changeSelection(this);loadAll()">All</a>
				</div>
				<div id="calendar-wrapper">
                                    <div id="calendar-wrap"></div>
                                    <div style="margin-top: 5px; display: none">
                                        <table cellspacing="1" cellpadding="2" class="calendarlegend" id="calendarlegend">
                                            <tbody>
                                                <tr align="center">
                                                    <td class="scheduleColor DONE">&nbsp;</td>
                                                    <td class="calendarLabel">Done</td>
                                                    <td class="scheduleColor CANCELED">&nbsp;</td>
                                                    <td class="calendarLabel">Canceled</td>
                                                    <td class="scheduleColor NOW">&nbsp;</td>
                                                    <td class="calendarLabel">Now or Next</td>
                                                    <td class="scheduleColor FURURE">&nbsp;</td>
                                                    <td class="calendarLabel">Future</td>

                                                </tr></tbody>
                                        </table>
                                    </div>
                                    
				</div>
                                
                            
                            
				<div id="calendar-info" style="left: 31%;">
				</div>
				<div id="info_loading" class="schedule_loading" style="margin-left: 700px;"></div>
				<div class="clear">
				</div>
				<div id="schedule_loading" class="schedule_loading" style="margin-left: 170px;"></div>
				<script language="JavaScript">
					function getUserId(){return <?php echo $this->_tpl_vars['userId']; ?>
;} 
				</script>
				<?php echo '
				
				<script type="text/javascript" src="jss/date-utils.js"></script>
				<script type="text/javascript" src="jss/schedule.js"></script>
                                <link type="text/css" href="jss/tipTip.css"  rel="stylesheet"  />
                                <script src="jss/jquery.tipTip.js" type="text/javascript"></script>
				<script language="javascript">

					loadCalendar();
					
				</script>
				'; ?>

			</div>
		</div>
	</div>
    <script language="javascript">
        changePageTitle('Schedule');
    </script>
</div>