<?php /* Smarty version 2.6.16, created on 2013-01-20 14:15:39
         compiled from calendar.tpl */ ?>
<link type="text/css" href="style/jquery.timecalendar.css" rel="stylesheet" />
<script type="text/javascript" src="jss/jquery.timecalendar.js"></script>

<div id="paging">
	<div align="right">
			<?php if ($this->_tpl_vars['is_coach'] == '0'): ?>
			<!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
			<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['is_partner'] == '0'): ?>
			<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
			<!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
			<?php endif; ?> 
		</div>
	<div class="content">	
		
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'user_tabs.tpl', 'smarty_include_vars' => array('active_tab' => 'coach')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				
		<div class="clear"></div>
		<div class="tabular-content" style="height: 600px;">    
			<div class="tab_sub">
            	<ul>
					<li><a href="javascript:;" class="active">Availability</a></li>
                    <li><a href="coach_game.php">My Games</a></li>
					<li><a href="add_lesson.php">My Lessons</a></li>
                </ul>
			</div>
			<?php echo ''; ?>

			<div id="calendarwrapper" style=""></div>
			
			<script>
				showCalendar('<?php echo $this->_tpl_vars['userId']; ?>
',"inpage");
			</script>
			
			
		</div>
   </div>
 </div>
<script language="javascript">
       changePageTitle('Availability');
</script>