<?php /* Smarty version 2.6.16, created on 2013-01-01 15:32:34
         compiled from student_leftpanel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'in_array', 'student_leftpanel.tpl', 120, false),array('function', 'html_options', 'student_leftpanel.tpl', 171, false),)), $this); ?>
<?php echo '
<script type="text/javascript" language="javascript">

jQuery(document).ready(function(){


/* right panel filter more/less links */
	var filterThreshold = 5;
	jQuery(\'.listing\').each(function(i){
	if(jQuery(this).find(\'a.selected\').length == 0){
		jQuery(this).find(\'li:gt(\'+(filterThreshold-1)+\')\').hide();
		if(jQuery(this).find(\'li\').length > filterThreshold)
			jQuery(this).after(\'<li class="more"><div class="button" style="float:right;margin: 5px;">+ more</div></li><li style="clear:both; width:100%"></li>\');
		else
			jQuery(this).after(\'\');
	}
	});

	jQuery(\'.more\').each(function(i){
		jQuery(this).toggle(function(){
		jQuery(this).prev().find(\'li:gt(\'+(filterThreshold-1)+\')\').slideDown(\'fast\');
		jQuery(this).html(\'<li class="more"><div class="button" style="float:right;margin: 5px;">- less</div></li><li style="clear:both; width:100%"></li>\');
		},function(){
		jQuery(this).prev().find(\'li:gt(\'+(filterThreshold-1)+\')\').slideUp(\'fast\');
		jQuery(this).html(\'<li class="more"><div class="button" style="float:right;margin: 5px;">+ more</div></li><li style="clear:both; width:100%"></li>\');
		});
	});
	jQuery(\'.refine\').click(function(){
	
		var val = jQuery(this).attr(\'typeId\');
		var type = jQuery(this).attr(\'stype\');
		
		if(type ==\'G\')
		{
			if(jQuery(\'#game_\'+val).attr(\'checked\'))
			jQuery(\'#game_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#game_\'+val).attr(\'checked\',\'true\');
		}			
		else if(type ==\'A\')
		{
			if(jQuery(\'#avail_\'+val).attr(\'checked\'))
			jQuery(\'#avail_\'+val).attr(\'checked\',\'\');
			else
			{
			  jQuery(\'#avail_\'+val).attr(\'checked\',\'true\');
	
		    }		 
		}
		else
		{
			if(jQuery(\'#language_\'+val).attr(\'checked\'))
			jQuery(\'#language_\'+val).attr(\'checked\',\'\');
			else
			jQuery(\'#language_\'+val).attr(\'checked\',\'true\');
		}
		
		//document.frmRefine.submit();
		frmRefine_POST();
	});
  });	
  function send_value()
  {
    var price = jQuery(\'#Slider1\').val();
	alert(price);
  }
  function frmRefine_POST()
  {
   
	var data = jQuery(document.frmRefine).serialize();
    jQuery.ajax({
	type: "GET",
	url: \'findstudent2.php\',
	data: data,			
	dataType: \'html\',
	success: function(data)
	{				
	   jQuery(\'.container-inner\').html(data+"<div class=\'clear\'></div>");
	   //alert(data);
		
	}
   });
  }
  function subForm(a){
	document.getElementById(\'listfor\').value=\'sorting\';
	document.getElementById(\'sortingby\').value=a;
	goto();
}
function avail_submit(c){
	document.getElementById(\'availability_country\').value=c;
	goto();
}
function goto(){
	frmRefine_POST();
}
</script>


<link rel="stylesheet" href="slider/stylesheets/jslider.css" type="text/css">
<link rel="stylesheet" href="slider/stylesheets/jslider.plastic.css" type="text/css">
<script type="text/javascript" src="slider/javascripts/jquery.dependClass.js"></script>
<script type="text/javascript" src="slider/javascripts/jquery.slider-min.js"></script>
'; ?>

<div class="left-panel">
   <div class="graphite demo-container">
	<ul class="accordion" id="accordion-1">
	<form action="findstudent.php" method="post" name="frmRefine">
	<input type="hidden" name="list_for" id="listfor" value="" />
	<input type="hidden" name="sorting_by" id="sortingby" value=""/>
	<input type="hidden" name="availability_country" id="availability_country" value=""/>
	<input type="hidden" name="action" value="send" />
	
	 <?php if ($this->_tpl_vars['Numgame'] != '0'): ?>
	    <li><a href="#">Game</a>
        <ul>	
		 <div class="listing">
				 <?php unset($this->_sections['game']);
$this->_sections['game']['name'] = 'game';
$this->_sections['game']['loop'] = is_array($_loop=$this->_tpl_vars['gameArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['game']['show'] = true;
$this->_sections['game']['max'] = $this->_sections['game']['loop'];
$this->_sections['game']['step'] = 1;
$this->_sections['game']['start'] = $this->_sections['game']['step'] > 0 ? 0 : $this->_sections['game']['loop']-1;
if ($this->_sections['game']['show']) {
    $this->_sections['game']['total'] = $this->_sections['game']['loop'];
    if ($this->_sections['game']['total'] == 0)
        $this->_sections['game']['show'] = false;
} else
    $this->_sections['game']['total'] = 0;
if ($this->_sections['game']['show']):

            for ($this->_sections['game']['index'] = $this->_sections['game']['start'], $this->_sections['game']['iteration'] = 1;
                 $this->_sections['game']['iteration'] <= $this->_sections['game']['total'];
                 $this->_sections['game']['index'] += $this->_sections['game']['step'], $this->_sections['game']['iteration']++):
$this->_sections['game']['rownum'] = $this->_sections['game']['iteration'];
$this->_sections['game']['index_prev'] = $this->_sections['game']['index'] - $this->_sections['game']['step'];
$this->_sections['game']['index_next'] = $this->_sections['game']['index'] + $this->_sections['game']['step'];
$this->_sections['game']['first']      = ($this->_sections['game']['iteration'] == 1);
$this->_sections['game']['last']       = ($this->_sections['game']['iteration'] == $this->_sections['game']['total']);
?>
				   	<li>
					<input type="checkbox" name="game_id[]" id='game_<?php echo $this->_tpl_vars['gameArr'][$this->_sections['game']['index']]['game_id']; ?>
' value="<?php echo $this->_tpl_vars['gameArr'][$this->_sections['game']['index']]['game_id']; ?>
" style="display:none" 
					<?php if (((is_array($_tmp=$this->_tpl_vars['gameArr'][$this->_sections['game']['index']]['game_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['game_id']) : in_array($_tmp, $this->_tpl_vars['game_id']))): ?> checked="checked" <?php endif; ?>/>
					<a href="#" class="refine <?php if (((is_array($_tmp=$this->_tpl_vars['gameArr'][$this->_sections['game']['index']]['game_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['game_id']) : in_array($_tmp, $this->_tpl_vars['game_id']))): ?> selected <?php endif; ?>" stype="G" typeId="<?php echo $this->_tpl_vars['gameArr'][$this->_sections['game']['index']]['game_id']; ?>
"> 
					<?php echo $this->_tpl_vars['gameArr'][$this->_sections['game']['index']]['game_name']; ?>
 (<?php echo $this->_tpl_vars['gameArr'][$this->_sections['game']['index']]['cnt']; ?>
) </a> </li>
                 <?php endfor; endif; ?>
				 
		 </div>		 
		 </ul>
	   </li>	 
	 <?php endif; ?>	
		<li><a href="#">Budget ($)</a>
           <ul>	
				  <li style="height:30px; padding:15px 5px 5px 5px;"> 
				   <span style="display: inline-block; width: 230px; padding: 0 5px;">
				   <input id="Slider1" type="slider" name="price" value="<?php echo $this->_tpl_vars['price']; ?>
"/></span> 
                    <?php echo '
					 <script type="text/javascript" charset="utf-8">
					  jQuery("#Slider1").slider({ from: 0, to: 100, limits: false, step: 1, dimension: \'\', skin: "plastic", callback: 
					  function( value ){ /*document.frmRefine.submit()*/ frmRefine_POST(); } });
					</script>
					'; ?>

				 </li>	
		   </ul>
		 </li>  
		 
	   <?php if ($this->_tpl_vars['Numlanguage'] != '0'): ?>	
	     <li><a href="#">Language</a>
            <ul>
			 <div class="listing">
                   
				  <?php unset($this->_sections['lang']);
$this->_sections['lang']['name'] = 'lang';
$this->_sections['lang']['loop'] = is_array($_loop=$this->_tpl_vars['languageArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['lang']['show'] = true;
$this->_sections['lang']['max'] = $this->_sections['lang']['loop'];
$this->_sections['lang']['step'] = 1;
$this->_sections['lang']['start'] = $this->_sections['lang']['step'] > 0 ? 0 : $this->_sections['lang']['loop']-1;
if ($this->_sections['lang']['show']) {
    $this->_sections['lang']['total'] = $this->_sections['lang']['loop'];
    if ($this->_sections['lang']['total'] == 0)
        $this->_sections['lang']['show'] = false;
} else
    $this->_sections['lang']['total'] = 0;
if ($this->_sections['lang']['show']):

            for ($this->_sections['lang']['index'] = $this->_sections['lang']['start'], $this->_sections['lang']['iteration'] = 1;
                 $this->_sections['lang']['iteration'] <= $this->_sections['lang']['total'];
                 $this->_sections['lang']['index'] += $this->_sections['lang']['step'], $this->_sections['lang']['iteration']++):
$this->_sections['lang']['rownum'] = $this->_sections['lang']['iteration'];
$this->_sections['lang']['index_prev'] = $this->_sections['lang']['index'] - $this->_sections['lang']['step'];
$this->_sections['lang']['index_next'] = $this->_sections['lang']['index'] + $this->_sections['lang']['step'];
$this->_sections['lang']['first']      = ($this->_sections['lang']['iteration'] == 1);
$this->_sections['lang']['last']       = ($this->_sections['lang']['iteration'] == $this->_sections['lang']['total']);
?>
				  	<li>
					<input type="checkbox" name="language_id[]" id="language_<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id']; ?>
" value="<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id']; ?>
" style="display:none;" 
					<?php if (((is_array($_tmp=$this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['language_id']) : in_array($_tmp, $this->_tpl_vars['language_id']))): ?> checked="checked" <?php endif; ?>/>				
					<a href="#" class="refine <?php if (((is_array($_tmp=$this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['language_id']) : in_array($_tmp, $this->_tpl_vars['language_id']))): ?> selected <?php endif; ?>" stype="L" typeId="<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_id']; ?>
" >
					<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['language_name']; ?>
 (<?php echo $this->_tpl_vars['languageArr'][$this->_sections['lang']['index']]['cnt']; ?>
)</a></li>
                  <?php endfor; endif; ?>
			 </div>	 	  
			</ul>
		</li>	
	  <?php endif; ?>	
	  <li><a href="#">Availability</a>
           <ul>
		   <div class="listing">
			 
			  <li> <input type="checkbox" name="availability[]" id="avail_O" value="O" style="display:none" <?php if (((is_array($_tmp='O')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> checked="checked" <?php endif; ?> />
			  <a href="#" class="refine <?php if (((is_array($_tmp='O')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> selected <?php endif; ?>" stype="A" typeId="O"> Online(<?php echo $this->_tpl_vars['avail1Num']; ?>
)</a></li>
			  <li> <input type="checkbox" name="availability[]" id="avail_L" value="L" style="display:none" <?php if (((is_array($_tmp='L')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> checked="checked" <?php endif; ?> />
			  <a href="#" class="refine <?php if (((is_array($_tmp='L')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> selected <?php endif; ?>" stype="A" typeId="L" > Local meet-up(<?php echo $this->_tpl_vars['avail2Num']; ?>
)</a></li>
			  <li id="avail_coun" <?php if (((is_array($_tmp='L')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['availability']) : in_array($_tmp, $this->_tpl_vars['availability']))): ?> style="display:block;" <?php else: ?> style="display:none;" <?php endif; ?>>
			  <select name="avail_country" id="avail_country" class="drop-down" onchange="avail_submit(this.value)">
			  <option value="">Availability Country</option>
			   <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['AvailcounArr'],'selected' => $this->_tpl_vars['availability_country']), $this);?>

			  </select>
			  </li>
			</div>  
		   </ul>
		</li>   
	  <input type="hidden" name="record_per_page" id="record_per_page" value="<?php echo $this->_tpl_vars['record_per_page']; ?>
" style="visibility:hidden">

	  <input type="submit" style="visibility:hidden">
	 </form> 	
    </ul>
   </div>   
   </div>