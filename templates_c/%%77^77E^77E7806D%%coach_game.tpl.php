<?php /* Smarty version 2.6.16, created on 2013-04-20 17:17:15
         compiled from coach_game.tpl */ ?>
<?php echo '
<script language="javascript">
function saveAbout(){
    window.location= "coach_game.php?action=ab&type=1&about="+jQuery(\'#about-text\').val();
}
</script>
'; ?>


<div id="paging">

<div align="right">
	<?php if ($this->_tpl_vars['is_coach'] == '0'): ?>
	<!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
	<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['is_partner'] == '0'): ?>
	<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
	<!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
	<?php endif; ?> 
</div>
<div class="content">	
		
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'user_tabs.tpl', 'smarty_include_vars' => array('active_tab' => 'coach')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    		
    <div class="clear"></div>
    <div class="tabular-content"> 
		
        <div class="tab_sub">
            <ul>
                <li><a href="calendar.php">Availability</a></li>
                <li><a href="javascript:;" class="active">My Games</a></li>
                <li><a href="add_lesson.php" >My Lessons</a></li>
            </ul>
        </div>
        <div class="total-message">
            <a href="coach_game_update.php" class="button">Add Game</a>&nbsp;
            <strong>Total Games : <?php echo $this->_tpl_vars['Numcoachgame']; ?>
</strong>&nbsp;&nbsp;
        </div> 
        <div class="clear"></div>                     
            <div>
                
                <div id="about" class="coach_game_details" style="margin-top: 30px;margin-left: 30px;margin-bottom: 30px;">
                    <dl style="">
                        <dt>Introduction:</dt>
                        <dd>
				<div id="about-div"> <?php if ($this->_tpl_vars['about'] != ''):  echo $this->_tpl_vars['about'];  else: ?>N/A<?php endif; ?></div> 
				<div style="margin-top: 10px;width: 588px;color: red;border: 1px solid red;padding: 3px;"><center>This Intro will be displayed on the Find Coach page before users view your full profile. <br>There is a 300-character limit, so write carefully!<br></center></div>
			</dd>
                    </dl>
                    <?php echo '
                    <a href="javascript:;" style="float: right" class="button" onclick="jQuery(\'#about\').hide(0,function(){jQuery(\'#about-edit\').show(0)})">edit</a>
                    '; ?>

                </div> 
                <div id="about-edit" id="abouttext" class="coach_game_details" style="display: none;margin-top: 30px;margin-left: 30px;margin-bottom: 30px;">
                    <dl>
                        <dt>Introduction:</dt>
                        <dd>
                            <textarea maxlength="300" onkeyup="countLenght(this,'about-lenght')" id="about-text"><?php echo $this->_tpl_vars['about']; ?>
</textarea>
                            <span id="about-lenght">0/300</span>
                            <script>
                                currentLenght('about-text', 'about-lenght');
                            </script>
                            <div style="margin-top: 10px;width: 588px;color: red;border: 1px solid red;padding: 3px;"><center>This Intro will be displayed on the Find Coach page before users view your full profile. <br>There is a 300-character limit, so write carefully!<br></center></div>
                        </dd>
                    </dl>
                    
                    <a href="javascript:;" class="button" style="float: right" onclick="saveAbout()">Save</a>
                    <a href="javascript:;" class="button" style="float: right" onclick="jQuery('#about-edit').hide(0,function(){jQuery('#about').show(0,function(){jQuery('#about-text').val(jQuery('#about-div').text());currentLenght('about-text', 'about-lenght')})})">Cancel</a>
                    
                </div>
                <p class="clear"></p>    
                <div style="margin: 20px 0; border-bottom: 1px solid #ccc">&nbsp;&nbsp;</div> 
                    
            <?php if ($this->_tpl_vars['Numcoachgame'] > 0): ?>
                <?php unset($this->_sections['gameRow']);
$this->_sections['gameRow']['name'] = 'gameRow';
$this->_sections['gameRow']['loop'] = is_array($_loop=$this->_tpl_vars['CoachgameArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['gameRow']['show'] = true;
$this->_sections['gameRow']['max'] = $this->_sections['gameRow']['loop'];
$this->_sections['gameRow']['step'] = 1;
$this->_sections['gameRow']['start'] = $this->_sections['gameRow']['step'] > 0 ? 0 : $this->_sections['gameRow']['loop']-1;
if ($this->_sections['gameRow']['show']) {
    $this->_sections['gameRow']['total'] = $this->_sections['gameRow']['loop'];
    if ($this->_sections['gameRow']['total'] == 0)
        $this->_sections['gameRow']['show'] = false;
} else
    $this->_sections['gameRow']['total'] = 0;
if ($this->_sections['gameRow']['show']):

            for ($this->_sections['gameRow']['index'] = $this->_sections['gameRow']['start'], $this->_sections['gameRow']['iteration'] = 1;
                 $this->_sections['gameRow']['iteration'] <= $this->_sections['gameRow']['total'];
                 $this->_sections['gameRow']['index'] += $this->_sections['gameRow']['step'], $this->_sections['gameRow']['iteration']++):
$this->_sections['gameRow']['rownum'] = $this->_sections['gameRow']['iteration'];
$this->_sections['gameRow']['index_prev'] = $this->_sections['gameRow']['index'] - $this->_sections['gameRow']['step'];
$this->_sections['gameRow']['index_next'] = $this->_sections['gameRow']['index'] + $this->_sections['gameRow']['step'];
$this->_sections['gameRow']['first']      = ($this->_sections['gameRow']['iteration'] == 1);
$this->_sections['gameRow']['last']       = ($this->_sections['gameRow']['iteration'] == $this->_sections['gameRow']['total']);
?>
                    <div class="profile-list" <?php if ($this->_sections['gameRow']['index']%2 != 0): ?>style="background-color:#f2f2f2;"<?php endif; ?>>
                        <div class="game-details" style="width:90%;<?php if ($this->_sections['gameRow']['index']%2 != 0): ?> background-color:#f2f2f2;<?php endif; ?>" >
                            <h2><?php echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['game']; ?>
</h2>
                                <div class="coach_game_details" style="margin-top: 10px;margin-left: 10px;">
                                    <dl>
                                            <dt>1hr:</dt>
                                            <dd><?php if ($this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['rate_A'] != ''):  echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['rate_A']; ?>
pts<?php endif; ?></dd>
                                    </dl>
                                    <dl>
                                            <dt>2hr:</dt>
                                            <dd><?php if ($this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['rate_B'] != '' && $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['rate_B'] != '0'):  echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['rate_B']; ?>
pts<?php else: ?>&nbsp;<?php endif; ?></dd>
                                    </dl>
                                    <dl>
                                            <dt>3hr:</dt>
                                            <dd><?php if ($this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['rate_C'] != '' && $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['rate_C'] != '0'):  echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['rate_C']; ?>
pts<?php else: ?>&nbsp;<?php endif; ?></dd>
                                    </dl>
                                    
                                    <?php unset($this->_sections['categoryIndex']);
$this->_sections['categoryIndex']['name'] = 'categoryIndex';
$this->_sections['categoryIndex']['loop'] = is_array($_loop=$this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['categoriesArr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['categoryIndex']['show'] = true;
$this->_sections['categoryIndex']['max'] = $this->_sections['categoryIndex']['loop'];
$this->_sections['categoryIndex']['step'] = 1;
$this->_sections['categoryIndex']['start'] = $this->_sections['categoryIndex']['step'] > 0 ? 0 : $this->_sections['categoryIndex']['loop']-1;
if ($this->_sections['categoryIndex']['show']) {
    $this->_sections['categoryIndex']['total'] = $this->_sections['categoryIndex']['loop'];
    if ($this->_sections['categoryIndex']['total'] == 0)
        $this->_sections['categoryIndex']['show'] = false;
} else
    $this->_sections['categoryIndex']['total'] = 0;
if ($this->_sections['categoryIndex']['show']):

            for ($this->_sections['categoryIndex']['index'] = $this->_sections['categoryIndex']['start'], $this->_sections['categoryIndex']['iteration'] = 1;
                 $this->_sections['categoryIndex']['iteration'] <= $this->_sections['categoryIndex']['total'];
                 $this->_sections['categoryIndex']['index'] += $this->_sections['categoryIndex']['step'], $this->_sections['categoryIndex']['iteration']++):
$this->_sections['categoryIndex']['rownum'] = $this->_sections['categoryIndex']['iteration'];
$this->_sections['categoryIndex']['index_prev'] = $this->_sections['categoryIndex']['index'] - $this->_sections['categoryIndex']['step'];
$this->_sections['categoryIndex']['index_next'] = $this->_sections['categoryIndex']['index'] + $this->_sections['categoryIndex']['step'];
$this->_sections['categoryIndex']['first']      = ($this->_sections['categoryIndex']['iteration'] == 1);
$this->_sections['categoryIndex']['last']       = ($this->_sections['categoryIndex']['iteration'] == $this->_sections['categoryIndex']['total']);
?>
                                    <dl>
                                        <dt><?php echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['categoriesArr'][$this->_sections['categoryIndex']['index']]['category_name']; ?>
:</dt>
                                        <dd><?php if ($this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['categoriesArr'][$this->_sections['categoryIndex']['index']]['properties'] != ''):  echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['categoriesArr'][$this->_sections['categoryIndex']['index']]['properties'];  else: ?>&nbsp;<?php endif; ?></dd>
                                    </dl>
                                    <?php endfor; endif; ?>
                                                                        
                                    <dl>
                                            <dt>Experience:</dt>
                                            <dd><?php if ($this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['experience'] != ''):  echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['experience'];  else: ?>N/A<?php endif; ?></dd>
                                    </dl>

                                    <dl>
                                            <dt>Lesson Plan:</dt>
                                            <dd><?php if ($this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['lesson_plan'] != ''):  echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['lesson_plan'];  else: ?>N/A<?php endif; ?></dd>
                                    </dl>
                                </div>


                            <div style="margin-top: 25px; margin-bottom: 10px; float: right;">

                                <a class="button" href="#" onclick="showYesNo('Delete your selected game:: <?php echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['game']; ?>
?', '', 'deleteGame', 'del(\'coach_game.php?action=del&delete_id=<?php echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['user_game_id']; ?>
&module=contact\')');">Delete</a>
                                <a class="button" href="coach_game_update.php?user_game_id=<?php echo $this->_tpl_vars['CoachgameArr'][$this->_sections['gameRow']['index']]['user_game_id']; ?>
">Edit</a>
                            </div>
                            <?php echo '
                            <script language="javascript">
                                function del(url){ 
                                    window.location= url;//"coach_game.php?action=del&delete_id={$CoachgameArr[gameRow].user_game_id}&module=contact&IsProcess=Y";
                                }
                            </script>  
                            '; ?>


                        </div><div class="clear"></div>
                    </div>
                <?php endfor; endif; ?>
            <?php else: ?>
            <div class="profile-list">
                    <div class="game-details"> No game found </div>
            </div>
            <?php endif; ?>
            <div class="clear"></div>
          </div>
  </div>
</div>
 </div>
<script language="javascript">
       changePageTitle('My Games');
</script>