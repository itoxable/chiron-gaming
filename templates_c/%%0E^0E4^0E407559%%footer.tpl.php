<?php /* Smarty version 2.6.16, created on 2013-01-26 17:01:40
         compiled from footer.tpl */ ?>
<div class="clear"></div>
</div>

<div class="push"></div>
</div>	
<div class="footer" <?php if ($_SESSION['user_id'] != ''): ?> style="" <?php endif; ?>>
   <div class="footer-inner">
		<div class="footer-right">
			<ul>
				<li><span>&copy; 2012 ChironGaming. All rights reserved.</span></li>
				<li>|</li>
				<li><a href="termsconditions.php">Terms & Conditions</a></li>
				<li>|</li>
				<li><a href="privacypolicy.php">Privacy Policy</a></li>
				
			</ul>
		</div>
      <div class="footer-left">
         <ul style="float: left;">
			
            <li><a href="contactus.php">Contact Us</a></li>
            <li>|</li>
            <!--li><a href="#">Forum</a></li>
            <li>|</li-->
            
            <li><a href="support.php">Support</a></li>
            <li>|</li>
			<li><a href="find_training_partner.php">Find Training Partner</a></li>
            <li>|</li>
            <li><a href="findcoach.php">Find Coach</a></li>
            <li>|</li>
            <li><a href="howitworks.php">How It Works</a></li>
            <li>|</li>
            <li><a href="index.php">Home</a></li>
         </ul>
      </div>
   </div>
</div>	


</div>

<script type="text/javascript" src="jss/bottomJs.js"></script>
</body>
</html>