<?php
    $cashierAction = isset($_POST['cashierAction']) ? $_POST['cashierAction'] : "cashierPanel";
	if ($cashierAction == 'cashierPanel') {
		include("cashier_buy_panel.php");
	} elseif ($cashierAction == 'historyPanel') {
		$numberOfTransactions = 1000;
		include("cashier_history_panel.php");
	} elseif ($cashierAction == 'withdrawPanel') {
		include("cashier_withdraw_panel.php");
	}
?>