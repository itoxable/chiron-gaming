<?php
$minSize = 7;
$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game WHERE game_id=".$_POST['game_id'];
$RsgameSql = $UserManagerObjAjax->GetRecords("Row",$SelectgameSql);
	
	if(is_array($RsgameSql) && count($RsgameSql)){
		$string = '';
		if($RsgameSql['is_ladder'] == 'Y'){
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_ladder WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY ladder_name";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="ladder_id">Select Ladder: </label><select name="ladder_id" id="ladder_id" class="con-req">'; 
				for($i=0; $i<count($RsLadderSql); $i++){
					$sel = isset($Formval) && $Formval['ladder_id'] == $RsLadderSql[$i]['ladder_id'] ? "selected" : "";
					$string .= '<option value="'.$RsLadderSql[$i]['ladder_id'].'" '.$sel.'>'.$RsLadderSql[$i]['ladder_name'].'</option>';
				}
				$string .= '</select>';
			}
			$string .= '<br class="clear" />';
		}
		if($RsgameSql['is_race'] == 'Y'){
                        $size=0;
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_race WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY race_title";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="race_id">Select Race: </label><div>'; 
				$size = count($RsLadderSql);
				for($i=0; $i<$size; $i++){
					$sel = isset($Formval) && in_array($RsLadderSql[$i]['race_id'],$raceArray) ? "checked" : "";
					$string .= '<input class="race_checkbox checkbox_" type="checkbox" name="race_id[]" value="'.$RsLadderSql[$i]['race_id'].'" '.$sel.' />&nbsp;'.$RsLadderSql[$i]['race_title'] . "&nbsp;&nbsp;";
				}				
			}
                        $string .= '<script type="text/javascript"> jQuery(".race_checkbox").shiftcheckbox();</script>';
                         if($size>$minSize)
                            $string .= '<div class="select-checkbox"><a href="javascript:;" onclick="selectAll(\'.race_checkbox\')">Select all</a><a href="javascript:;" onclick="selectNone(\'.race_checkbox\')">Deselect all</a></div>';
                         $string .= '</div><br class="clear" />';
		}
		if($RsgameSql['is_server'] == 'Y'){
                        $size=0;
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_server WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY server_name";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="server_id">Select Server: </label><div>'; 
				$size = count($RsLadderSql);
				for($i=0; $i<$size; $i++){
					$sel = isset($Formval) && in_array($RsLadderSql[$i]['server_id'],$serverArray) ? "checked" : "";
					$string .= '<input class="server_checkbox checkbox_" type="checkbox" name="server_id[]" value="'.$RsLadderSql[$i]['server_id'].'" '.$sel.' />&nbsp;'.$RsLadderSql[$i]['server_name'] . "&nbsp;&nbsp;";					
				}				
			}
                        $string .= '<script type="text/javascript"> jQuery(".server_checkbox").shiftcheckbox();</script>';
                         if($size>$minSize)
                            $string .= '<div class="select-checkbox"><a href="javascript:;" onclick="selectAll(\'.server_checkbox\')">Select all</a><a href="javascript:;" onclick="selectNone(\'.server_checkbox\')">Deselect all</a></div>';
                         $string .= '</div><br class="clear" />';
		}
		if($RsgameSql['is_region'] == 'Y'){
                        $size=0;
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_region WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY region_title";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="region_id">Select Region: </label><div>'; 
				$size = count($RsLadderSql);
				for($i=0; $i<$size; $i++){
					$sel = isset($Formval) && in_array($RsLadderSql[$i]['region_id'],$regionArray) ? "checked" : "";
					$string .= '<input class="region_checkbox checkbox_" type="checkbox" name="region_id[]" value="'.$RsLadderSql[$i]['region_id'].'" '.$sel.' />&nbsp;'.$RsLadderSql[$i]['region_title'] . "&nbsp;&nbsp;";
				}
			}
                        $string .= '<script type="text/javascript"> jQuery(".region_checkbox").shiftcheckbox();</script>';
                         if($size>$minSize)
                            $string .= '<div class="select-checkbox"><a href="javascript:;" onclick="selectAll(\'.region_checkbox\')">Select all</a><a href="javascript:;" onclick="selectNone(\'.region_checkbox\')">Deselect all</a></div>';
                         $string .= '</div><br class="clear" />';
		}
		if($RsgameSql['is_rating'] == 'Y'){
                        $size=0;
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_rating WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY rating_title";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="rating_id">Select Rating: </label><div>'; 
				$size = count($RsLadderSql);
				for($i=0; $i<$size; $i++){
					$sel = isset($Formval) && in_array($RsLadderSql[$i]['rating_id'],$ratingArray) ? "checked" : "";
					$string .= '<input class="rating_checkbox checkbox_" type="checkbox" name="rating_id[]" value="'.$RsLadderSql[$i]['rating_id'].'" '.$sel.' />&nbsp;'.$RsLadderSql[$i]['rating_title'] . "&nbsp;&nbsp;";
				}
			}
                        $string .= '<script type="text/javascript"> jQuery(".rating_checkbox").shiftcheckbox();</script>';
                         if($size>$minSize)
                            $string .= '<div class="select-checkbox"><a href="javascript:;" onclick="selectAll(\'.rating_checkbox\')">Select all</a><a href="javascript:;" onclick="selectNone(\'.rating_checkbox\')">Deselect all</a></div>';
                         $string .= '</div><br class="clear" />';
		}
                if($RsgameSql['is_class'] == 'Y'){
                        $size=0;
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_class WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY class_title";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="class_id">Select Class: </label><div>'; 
				$size = count($RsLadderSql);
				for($i=0; $i<$size; $i++){
					$sel = isset($Formval) && in_array($RsLadderSql[$i]['class_id'],$classArray) ? "checked" : "";
					$string .= '<input class="class_checkbox checkbox_" type="checkbox" name="class_id[]" value="'.$RsLadderSql[$i]['class_id'].'" '.$sel.' />&nbsp;'.$RsLadderSql[$i]['class_title'] . "&nbsp;&nbsp;";
				}
			}
                        $string .= '<script type="text/javascript"> jQuery(".class_checkbox").shiftcheckbox();</script>';
                         if($size>$minSize)
                            $string .= '<div class="select-checkbox"><a href="javascript:;" onclick="selectAll(\'.class_checkbox\')">Select all</a><a href="javascript:;" onclick="selectNone(\'.class_checkbox\')">Deselect all</a></div>';
                         $string .= '</div><br class="clear" />';
		}
                if($RsgameSql['is_mode'] == 'Y'){
                        $size=0;
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_mode WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY mode_title";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="mode_id">Select Mode: </label><div>'; 
				$size = count($RsLadderSql);
				for($i=0; $i<$size; $i++){
					$sel = isset($Formval) && in_array($RsLadderSql[$i]['mode_id'],$modeArray) ? "checked" : "";
					$string .= '<input class="mode_checkbox checkbox_" type="checkbox" name="mode_id[]" value="'.$RsLadderSql[$i]['mode_id'].'" '.$sel.' />&nbsp;'.$RsLadderSql[$i]['mode_title'] . "&nbsp;&nbsp;";
				}
			}
                        $string .= '<script type="text/javascript"> jQuery(".mode_checkbox").shiftcheckbox();</script>';
                         if($size>$minSize)
                            $string .= '<div class="select-checkbox"><a href="javascript:;" onclick="selectAll(\'.mode_checkbox\')">Select all</a><a href="javascript:;" onclick="selectNone(\'.mode_checkbox\')">Deselect all</a></div>';
                         $string .= '</div><br class="clear" />';
		}
                
                if($RsgameSql['is_versus'] == 'Y'){
                        $size=0;
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_versus WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY versus_title";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="versus_id">Select Versus: </label><div>'; 
				$size = count($RsLadderSql);
				for($i=0; $i<$size; $i++){
					$sel = isset($Formval) && in_array($RsLadderSql[$i]['versus_id'],$versusArray) ? "checked" : "";
					$string .= '<input class="versus_checkbox checkbox_" type="checkbox" name="versus_id[]" value="'.$RsLadderSql[$i]['versus_id'].'" '.$sel.' />&nbsp;'.$RsLadderSql[$i]['versus_title'] . "&nbsp;&nbsp;";
				}
			}
                        $string .= '<script type="text/javascript"> jQuery(".versus_checkbox").shiftcheckbox();</script>';
                         if($size>$minSize)
                            $string .= '<div class="select-checkbox"><a href="javascript:;" onclick="selectAll(\'.versus_checkbox\')">Select all</a><a href="javascript:;" onclick="selectNone(\'.versus_checkbox\')">Deselect all</a></div>';
                        $string .= '</div><br class="clear" />';
		}
                
                 if($RsgameSql['is_team'] == 'Y'){
                        $size=0;
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_team WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY team_title";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="team_id">Select Team: </label><div>'; 
				$size = count($RsLadderSql);
				for($i=0; $i<$size; $i++){
					$sel = isset($Formval) && in_array($RsLadderSql[$i]['team_id'],$teamArray) ? "checked" : "";
					$string .= '<input class="team_checkbox checkbox_" type="checkbox" name="team_id[]" value="'.$RsLadderSql[$i]['team_id'].'" '.$sel.' />&nbsp;'.$RsLadderSql[$i]['team_title'] . "&nbsp;&nbsp;";
				}
			}
                        $string .= '<script type="text/javascript"> jQuery(".team_checkbox").shiftcheckbox();</script>';
                         if($size>$minSize)
                            $string .= '<div class="select-checkbox"><a href="javascript:;" onclick="selectAll(\'.team_checkbox\')">Select all</a><a href="javascript:;" onclick="selectNone(\'.team_checkbox\')">Deselect all</a></div>';
                        $string .= '</div><br class="clear" />';
		}
                
                 if($RsgameSql['is_type'] == 'Y'){
                        $size=0;
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_type WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY type_title";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="type_id">Select Type: </label><div>'; 
				$size = count($RsLadderSql);
				for($i=0; $i<$size; $i++){
					$sel = isset($Formval) && in_array($RsLadderSql[$i]['type_id'],$typeArray) ? "checked" : "";
					$string .= '<input class="type_checkbox checkbox_" type="checkbox" name="type_id[]" value="'.$RsLadderSql[$i]['type_id'].'" '.$sel.' />&nbsp;'.$RsLadderSql[$i]['type_title'] . "&nbsp;&nbsp;";
				}
			}
                        $string .= '<script type="text/javascript"> jQuery(".type_checkbox").shiftcheckbox();</script>';
                         if($size>$minSize)
                            $string .= '<div class="select-checkbox"><a href="javascript:;" onclick="selectAll(\'.type_checkbox\')">Select all</a><a href="javascript:;" onclick="selectNone(\'.type_checkbox\')">Deselect all</a></div>';
                         $string .= '</div><br class="clear" />';
		}
                
                if($RsgameSql['is_champion'] == 'Y'){
                        $size=0;
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_champion WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY champion_title";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="champion_id">Select Champion: </label><div><table>'; 
				$size = count($RsLadderSql);
				for($i=0; $i<$size; $i++){
                                        if($i == 0)
                                            $string .= "<tr align='left'>";
                                        
					$sel = isset($Formval) && in_array($RsLadderSql[$i]['champion_id'],$championArray) ? "checked" : "";
					$string .= '<td><input class="champion_checkbox checkbox_" type="checkbox" name="champion_id[]" value="'.$RsLadderSql[$i]['champion_id'].'" '.$sel.' />&nbsp;'.$RsLadderSql[$i]['champion_title'] . "&nbsp;&nbsp;</td>";
				
                                        
                                        if($i == (count($RsLadderSql)-1))
                                            $string .= "</tr>";
                                        else if($i != 0 && (($i+1)%5) == 0)
                                            $string .= "</tr><tr align='left'>";
                                }
			}
			$string .= '</table>';
                        $string .= '<script type="text/javascript"> jQuery(".champion_checkbox").shiftcheckbox();</script>';
                         if($size>$minSize)
                            $string .= '<div class="select-checkbox"><a href="javascript:;" onclick="selectAll(\'.champion_checkbox\')">Select all</a><a href="javascript:;" onclick="selectNone(\'.champion_checkbox\')">Deselect all</a></div>';
                        $string .= '</div><br class="clear" />';
		}
                
                if($RsgameSql['is_map'] == 'Y'){
                        $size=0;
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_map WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY map_title";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="map_id">Select Map: </label><div>'; 
				$size = count($RsLadderSql);
				for($i=0; $i<$size; $i++){
					$sel = isset($Formval) && in_array($RsLadderSql[$i]['map_id'],$mapArray) ? "checked" : "";
					$string .= '<input class="map_checkbox checkbox_" type="checkbox" name="map_id[]" value="'.$RsLadderSql[$i]['map_id'].'" '.$sel.' />&nbsp;'.$RsLadderSql[$i]['map_title'] . "&nbsp;&nbsp;";
				}
			}
			
                        $string .= '<script type="text/javascript"> jQuery(".map_checkbox").shiftcheckbox();</script>';
                         if($size>$minSize)
                            $string .= '<div class="select-checkbox"><a href="javascript:;" onclick="selectAll(\'.map_checkbox\')">Select all</a><a href="javascript:;" onclick="selectNone(\'.map_checkbox\')">Deselect all</a></div>';
                        $string .= '</div><br class="clear" />';
		}
                
                if($RsgameSql['is_role'] == 'Y'){
                        $size=0;
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_role WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY role_title";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="role_id">Select Role: </label><div>'; 
                                $size = count($RsLadderSql);
				for($i=0; $i<$size; $i++){
					$sel = isset($Formval) && in_array($RsLadderSql[$i]['role_id'],$roleArray) ? "checked" : "";
					$string .= '<input class="role_checkbox checkbox_" type="checkbox" name="role_id[]" value="'.$RsLadderSql[$i]['role_id'].'" '.$sel.' />&nbsp;'.$RsLadderSql[$i]['role_title'] . "&nbsp;&nbsp;";
				}
			}
			
                        $string .= '<script type="text/javascript"> jQuery(".role_checkbox").shiftcheckbox();</script>';
                         if($size>$minSize)
                            $string .= '<div class="select-checkbox"><a href="javascript:;" onclick="selectAll(\'.role_checkbox\')">Select all</a><a href="javascript:;" onclick="selectNone(\'.role_checkbox\')">Deselect all</a></div>';
                        $string .= '</div><br class="clear" />';
		}
                
                if($RsgameSql['is_hero'] == 'Y'){
                        $size=0;
			$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game_hero WHERE game_id='" . $_POST['game_id'] . "' AND is_active='Y' ORDER BY hero_title";
			$RsLadderSql = $UserManagerObjAjax->GetRecords("All",$SelectgameSql);
			if(is_array($RsLadderSql) && count($RsLadderSql)){
				$string .= '<label class="game-extra" for="hero_id">Select hero: </label><div><table>'; 
                                $size = count($RsLadderSql);
				for($i=0; $i<$size; $i++){
                                        if($i == 0)
                                            $string .= "<tr align='left'>";
                                        
					$sel = isset($Formval) && in_array($RsLadderSql[$i]['hero_id'],$heroArray) ? "checked" : "";
					$string .= '<td><input class="hero_checkbox checkbox_" type="checkbox" name="hero_id[]" value="'.$RsLadderSql[$i]['hero_id'].'" '.$sel.' />&nbsp;'.$RsLadderSql[$i]['hero_title'] . "&nbsp;&nbsp;</td>";
				
                                        if($i == (count($RsLadderSql)-1))
                                            $string .= "</tr>";
                                        else if($i != 0 && (($i+1)%5) == 0)
                                            $string .= "</tr><tr align='left'>";
                                }
			}
			$string .= '</table>';
                        $string .= '<script type="text/javascript"> jQuery(".hero_checkbox").shiftcheckbox();</script>';
                         if($size>$minSize)
                            $string .= '<div class="select-checkbox"><a href="javascript:;" onclick="selectAll(\'.hero_checkbox\')">Select all</a><a href="javascript:;" onclick="selectNone(\'.hero_checkbox\')">Deselect all</a></div>';
		
                        $string .= '</div><br class="clear" />';
                }
		echo $string;
	}
	else{
		echo '<span style="color:red">Invalid game choosen!</span>';
	}
?>