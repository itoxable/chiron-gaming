<?php

$categoriesSql = "SELECT * FROM nk_game_categories WHERE game_id = '$game_id' AND status=1 ORDER BY category_name";
$categoriesArr=$UserManagerObjAjax->GetRecords("All",$categoriesSql);
$NumCategories=count($categoriesArr);
for($i=0;$i<$NumCategories;$i++){
    $categoriesPropsSql = "SELECT * FROM nk_game_categories_properties WHERE category_id = ".$categoriesArr[$i]['category_id']." AND status=1 ORDER BY property_name";
    $categoriesPropsArr=$UserManagerObjAjax->GetRecords("All",$categoriesPropsSql);
    $categoriesArr[$i]['props']=$categoriesPropsArr;
    $categoriesArr[$i]['propsSize'] = count($categoriesPropsArr);
}

// Availability //
$availSql = "SELECT count(u.user_id) FROM ".TABLEPREFIX."_user u,".TABLEPREFIX."_user_type_user_relation ur,".TABLEPREFIX."_user_game ug WHERE 
FIND_IN_SET('O',u.availability_type) AND ur.user_type_id=1 AND ur.user_id= u.user_id AND ug.user_id=u.user_id AND ug.game_id='$game_id' AND ug.user_type_id=1";
$avail1Arr = $UserManagerObjAjax->GetRecords("Row",$availSql);
$avail1Num = $avail1Arr[0];

$availSql = "SELECT count(u.user_id) FROM ".TABLEPREFIX."_user u,".TABLEPREFIX."_user_type_user_relation ur,".TABLEPREFIX."_user_game ug WHERE
FIND_IN_SET('L',u.availability_type) AND ur.user_type_id=1 AND ur.user_id = u.user_id AND ug.user_id=u.user_id AND ug.game_id='$game_id' AND ug.user_type_id=1";
$avail2Arr = $UserManagerObjAjax->GetRecords("Row",$availSql);
$avail2Num = $avail2Arr[0];

// Language //
 $languageSql = "SELECT lng.language_id AS language_id, lng.language_name AS language_name, count( lng.language_id ) AS cnt
FROM ".TABLEPREFIX."_language lng
JOIN ".TABLEPREFIX."_user st ON FIND_IN_SET( lng.language_id, st.language_ids )
AND lng.is_active = 'Y'
AND FIND_IN_SET( lng.language_id, (
SELECT u.language_ids
FROM ".TABLEPREFIX."_user u, ".TABLEPREFIX."_user_type_user_relation ur, ".TABLEPREFIX."_user_game game
WHERE u.is_active = 'Y'
AND ur.user_type_id =1 AND game.user_type_id=1
AND ur.user_id = u.user_id
AND u.user_id = st.user_id AND game.user_id = u.user_id AND game.game_id = '$game_id'
) )
GROUP BY lng.language_id, lng.language_name
ORDER BY lng.language_name ASC ";
$languageArr = $UserManagerObjAjax->GetRecords("All",$languageSql);
$Numlanguage = count($languageArr);

$availcoutSql = "SELECT country_id,country_name FROM ".TABLEPREFIX."_country ORDER BY country_name";
$AvailcounArr = $UserManagerObjAjax->HtmlOptionArrayCreate($availcoutSql);

$smarty->assign('categoriesArr',$categoriesArr);
$smarty->assign('AvailcounArr',$AvailcounArr);
 
$smarty->assign('avail1Num',$avail1Num);
$smarty->assign('avail2Num',$avail2Num);
$smarty->assign('Numlanguage',$Numlanguage);
$smarty->assign('languageArr',$languageArr);
$smarty->display('coach_leftpanel.tpl');   

?>