<?php

include_once "general_include.php";
include_once("adodb/dbconfig.php");
include('transaction_status.class.php');

function js2PhpTime($jsdate){
	if(preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches)==1){
		$ret = mktime($matches[4], $matches[5], 0, $matches[1], $matches[2], $matches[3]);
	}else if(preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches)==1){
		$ret = mktime(0, 0, 0, $matches[1], $matches[2], $matches[3]);
	}
	return $ret;
}
function php2MySqlTime($phpDate){
	return date("Y-m-d H:i:s", $phpDate);
}
if(!isset($_SESSION['user_id'])){
	echo "session not started";
	return;
}

function getPostOrGetParam($param){
	if(isset($_POST[$param]))
		return $_POST[$param];
	else{
		if(isset($_GET[$param]))
			return $_GET[$param];
		else
			return "";
	}
}

class Status{
	public static $AVAILABLE = 1;
	public static $ACTIVE = 2;
	public static $DELETED = 3;
	public static $PENDING = 4;
	public static $ACCEPTED = 5;
	public static $FINISHED = 6;
	public static $DECLINED = 7;
	public static $REQUESTED = 8;
}

class LessonStatus{
	const OPEN = 1;
	const PAID = 2;
	const COMPLETED = 3;
	const CLOSED_WITH_PAYMENT_TO_COACH = 4;
	const CANCELED = 5;
        const WAITING_CONFIRMATION = 6;
}
class LessonType{
	const REGULAR = 0;
	const INSTANT = 1;
}

class UserType{
	const COACH = 1;
        const STUDENT = 2;
	const TRAINNING_PARTNER = 3;
}


class ErrorCode{
	public static $UNKNOWN= 0;
	public static $ALREADY_USED = -2;
	public static $NO_EVENTS = -3;
	public static $ALREADY_SCHEDULED = -4;
}

class NotificationTypes{
	public static $GENERAL= 0;
	public static $SCHEDULE_ALERT = 1;
	public static $UNKNOWN = 2;
	public static $INSTANT_LESSON = 3;
        public static $INSTANT_LESSON_RESPONSE = 5;
	public static $MESSAGE = 4;
}

class Constants{
	public static $TIMES_ARRAY = array("0:00","0:30","1:00","1:30","2:00","2:30","3:00","3:30","4:00","4:30","5:00","5:30","6:00","6:30","7:00","7:30",
			"8:00","8:30","9:00","9:30","10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30","15:00",
			"15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00","19:30","20:00","20:30","21:00","21:30","22:00","22:30","23:00","23:30","23:59");

	public static $SLOTS_ARRAY = array("h0","h1","h2","h3",
			"h4","h5","h6","h7","h8","h9","h10","h11","h12","h13","h14","h15","h16","h17","h18","h19",
			"h20","h21","h22","h23","h24","h25","h26","h27","h28","h29","h30","h31","h32","h33",
			"h34","h35","h36","h37","h38","h39","h40","h41","h42","h43","h44","h45","h46","h47");
}

function login($username,$password){
	try{
		$db = new DBConnection();
		$db->getConnection();
		$sql = "select * from `nk_user` where `email`='".$username."' and `password`='".$password."'";
		$handle = mysql_query($sql);
		$linha = mysql_fetch_row($handle);

		$_SESSION['user_id'] = $linha[0];
		$_SESSION['logged_username'] = $linha[3];
		return true;
	}catch(Exception $e){
		return  $e->getMessage();
	}
}


class Notification{
	public $notification_id;
	public $user_id;
	public $title;
	public $introduction;
	public $text;
	public $date_value;
	public $link;
	public $new;
	public $date_readed;
	public $modified_date;
	public $creation_date;
	public $origin;
	public $type;
	public $extra_data;

	public function setNotification($notification_id=0,$user_id=0,$type=0,$origin="",$title="",$introduction="",$text="",$date_readed="",$date_value="",$link="",$new="", $modified_date="", $creation_date="") {
		$this->notification_id = $notification_id;
		$this->user_id = $user_id;
		$this->title = $title;
		$this->introduction = $introduction;
		$this->text = $text;
		$this->date_readed = $date_readed;
		$this->creation_date = $creation_date;
		$this->modified_date = $modified_date;
		$this->date_value = $date_value;
		$this->link = $link;
		$this->origin = $origin;
		$this->type = $type;
	}

	public function __construct($row) {

		$this->notification_id = $row->notification_id;
		$this->user_id = $row->user_id;
		$this->title = $row->title;
		$this->introduction = $row->introduction;
		$this->text = $row->text;
		$this->creation_date = $row->creation_date;
		$this->modified_date = $row->modified_date;
		$this->date_readed = $row->date_readed;
		$this->date_value = $row->date_value;
		$this->link = $row->link;
		$this->new = $row->new;
		$this->origin = $row->origin;
		$this->type = $row->nofication_type;
		$this->extra_data = $row->extra_data;
	}

	public function __toString(){
            return "";
	}
}

class Event{
	public $status = "";
	public $calendarRef = "";
	public $description = "";
	public $requestoruserId = "";
	public $availableSpot = "";
	public $isEmpty = 0;
	public $requestorTxt = "";
	public $slot = "";
	public $calendarId = "";
	public $gameId = array();
	public $userId = "";
	public $date = "";
	public $day = "";
	public $month = "";
	public $year = "";
	public $lessonId = "";
	
	function setStatus($status){
		$this->status = $status;
	}

	function setCalendarRef($calendarRef){
		$this->calendarRef = $calendarRef;
	}

	function setDescription($description){
		$this->description = $description;
	}

	function setRequestoruserId($requestoruserId){
		$this->requestoruserId = $requestoruserId;
	}

	function setAvailableSpot($availableSpot){
		$this->$availableSpot = $availableSpot;
	}

	function setIsEmpty($isEmpty){
		$this->isEmpty = $isEmpty;
	}

	function setRequestorTxt($requestorTxt){
		$this->requestorTxt = $requestorTxt;
	}
	function setGameId($gameId){
		$this->gameId = $gameId;
	}
	function setLessonIdId($lessonId){
		$this->lessonId = $lessonId;
	}

	public function setEvent($status="",$description="",$availableSpot="",$gameId="", $requestoruserId="",$calendarRef="",$requestorTxt="",$isEmpty=0,$slot="",$lessonId="") {
		$this->status = $status;
		$this->description = $description;
		$this->availableSpot = $availableSpot;
		$this->requestoruserId = $requestoruserId;
		$this->calendarRef = $calendarRef;
		$this->requestorTxt = $requestorTxt;
		$this->isEmpty = $isEmpty;
		$this->slot = $slot;
		$this->lessonId = $lessonId;
		if(is_array($gameId))
			$this->gameId = $gameId;
		else
			$this->gameId = split('#', $gameId);
	}

	public function __construct($event=null, $slot="", $row=null) {
		if(is_null($event)){
			$this->isEmpty = true;
		}else{
			list ($this->status, $this->description,$this->availableSpot, $gameId, $this->requestoruserId, $this->calendarRef, $this->requestorTxt,$this->lessonId) = split(',', $event);
			$this->gameId = split('#', $gameId);
		}
		$this->slot = $slot;
		if($row != null){
			$this->calendarId = $row->calendar_id;
			$this->userId = $row->user_id;
			$this->date = $row->date;
			$this->day = $row->day;
			$this->month = $row->month;
			$this->year = $row->year;
		}
	}

	public function __toString(){
		if($this->isEmpty){
			return "NULL";
		}
		$i=0;
		foreach ($this->gameId as $g){
			if($i==0)
				$gameId = $g;
			else
				$gameId = $gameId."#".$g;
			$i++;
		}
		return "'".$this->status.",".$this->description.",".$this->availableSpot.",".$gameId.",".$this->requestoruserId.",".$this->calendarRef.",".$this->requestorTxt.",".$this->lessonId."'";
	}
}

function getAllSlots($userid,$day){
	return getSlots($userid,$day,Constants::$SLOTS_ARRAY);
}

function getAllSlotsByCalendarId($calendarId){
	return getSlotsByCalendarId($calendarId,Constants::$SLOTS_ARRAY);
}

function getSlots($userid,$date,$slots){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();
		$sql = "select calendar_id, user_id, date, day, month, year, ";

		$i = 0;
		foreach ($slots as $slot){
			if($i == 0)
				$sql = $sql." `".$slot."` ";
			else
				$sql = $sql.", `".$slot."` ";
			$i++;
		}
		$sql = $sql."from `nk_user_calendar` where `user_id` = " .$userid;
		$sql = $sql." and `date` =".$date;
		$handle = mysql_query($sql);
		$row = mysql_fetch_object($handle);

		foreach ($slots as $slot){
			$ret[] = new Event($row->$slot, $slot, $row);

		}

	}catch(Exception $e){
		$ret['error'] = $e->getMessage();
	}
	return $ret;
}
function getCalendarById($calendarId){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();

		$sql = "select * from `nk_user_calendar` where `calendar_id` = ".$calendarId;
		$handle = mysql_query($sql);
		return mysql_fetch_object($handle);

	}catch(Exception $e){
		$ret['error'] = $e->getMessage();
	}
	return $ret;
}

function getSlotsByCalendarId($calendarId,$slots){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();
		$sql = "select calendar_id, user_id, date, day, month, year, ";
		$i = 0;
		foreach ($slots as $slot){
			if($i == 0)
				$sql = $sql." `".$slot."` ";
			else
				$sql = $sql.", `".$slot."` ";
			$i++;
		}
		$sql = $sql."from `nk_user_calendar` where `calendar_id` = ".$calendarId;
		$handle = mysql_query($sql);
		$row = mysql_fetch_object($handle);
		foreach ($slots as $slot){
			$ret[] = new Event($row->$slot, $slot, $row);
		}

	}catch(Exception $e){
		$ret['error'] = $e->getMessage();
	}
	return $ret;
}

function listCalendar($userid,$date="0",$timezoneOffset=0){

	$ret = array();
	$ret['events'] = array();
	try{
		$db = new DBConnection();
		$db->getConnection();

		$sql = "SELECT * FROM `nk_user_calendar` WHERE `user_id` = " .$userid;
		if($date != "0"){
			$sql = $sql." AND `date` = ".$date;
		}
		//logToFile($sql); 
		$handle = mysql_query($sql);
		while ($row = mysql_fetch_object($handle)) {
                    $ret['events'][] = array(
                        $row->calendar_id,
                        $row->user_id,
                        $row->date,
                        $row->day,
                        $row->month,
                        $row->year,
                        array(
                            new Event($row->h0,"h0",$row),
                            new Event($row->h1,"h1",$row),
                            new Event($row->h2,"h2",$row),
                            new Event($row->h3,"h3",$row),new Event($row->h4,"h4",$row),new Event($row->h5,"h5",$row),new Event($row->h6,"h6",$row),new Event($row->h7,"h7",$row),
                            new Event($row->h8,"h8",$row),new Event($row->h9,"h9",$row),new Event($row->h10,"h10",$row),new Event($row->h11,"h11",$row),new Event($row->h12,"h12",$row),new Event($row->h13,"h13",$row),new Event($row->h14,"h14",$row),new Event($row->h15,"h15",$row),
                            new Event($row->h16,"h16",$row),new Event($row->h17,"h17",$row),new Event($row->h18,"h18",$row),new Event($row->h19,"h19",$row),new Event($row->h20,"h20",$row),new Event($row->h21,"h21",$row),new Event($row->h22,"h22",$row),new Event($row->h23,"h23",$row),
                            new Event($row->h24,"h24",$row),new Event($row->h25,"h25",$row),new Event($row->h26,"h26",$row),new Event($row->h27,"h27",$row),new Event($row->h28,"h28",$row),new Event($row->h29,"h29",$row),new Event($row->h30,"h30",$row),new Event($row->h31,"h31",$row),
                            new Event($row->h32,"h32",$row),new Event($row->h33,"h33",$row),new Event($row->h34,"h34",$row),new Event($row->h35,"h35",$row),new Event($row->h36,"h36",$row),new Event($row->h37,"h37",$row),new Event($row->h38,"h38",$row),new Event($row->h39,"h39",$row),
                            new Event($row->h40,"h40",$row),new Event($row->h41,"h41",$row),new Event($row->h42,"h42",$row),new Event($row->h43,"h43",$row),new Event($row->h44,"h44",$row),new Event($row->h45,"h45",$row),new Event($row->h46,"h46",$row),new Event($row->h47,"h47",$row)
                        )
                    );

		}
//		$sql = "SELECT * FROM `nk_user_calendar` WHERE `user_id` = " .$userid;
//		if($date != "0"){
//                    if($timezoneOffset != 0){
//                        $sql = $sql." AND (`date` = ".(intval($date)+(24*3600))." OR `date` = ".(intval($date)-(24*3600)).") ORDER BY `date` ASC";
//                        //logToFile($sql); 
//                        $handle = mysql_query($sql);
//                        while ($row = mysql_fetch_object($handle)) {
//                            $ret['events'][($row->date < $date)?'before':'after'] = array(
//                                $row->calendar_id,
//                                $row->user_id,
//                                $row->date,
//                                $row->day,
//                                $row->month,
//                                $row->year,
//                                array(
//                                    new Event($row->h0,"h0",$row),new Event($row->h1,"h1",$row),new Event($row->h2,"h2",$row),new Event($row->h3,"h3",$row),new Event($row->h4,"h4",$row),new Event($row->h5,"h5",$row),new Event($row->h6,"h6",$row),new Event($row->h7,"h7",$row),
//                                    new Event($row->h8,"h8",$row),new Event($row->h9,"h9",$row),new Event($row->h10,"h10",$row),new Event($row->h11,"h11",$row),new Event($row->h12,"h12",$row),new Event($row->h13,"h13",$row),new Event($row->h14,"h14",$row),new Event($row->h15,"h15",$row),
//                                    new Event($row->h16,"h16",$row),new Event($row->h17,"h17",$row),new Event($row->h18,"h18",$row),new Event($row->h19,"h19",$row),new Event($row->h20,"h20",$row),new Event($row->h21,"h21",$row),new Event($row->h22,"h22",$row),new Event($row->h23,"h23",$row),
//                                    new Event($row->h24,"h24",$row),new Event($row->h25,"h25",$row),new Event($row->h26,"h26",$row),new Event($row->h27,"h27",$row),new Event($row->h28,"h28",$row),new Event($row->h29,"h29",$row),new Event($row->h30,"h30",$row),new Event($row->h31,"h31",$row),
//                                    new Event($row->h32,"h32",$row),new Event($row->h33,"h33",$row),new Event($row->h34,"h34",$row),new Event($row->h35,"h35",$row),new Event($row->h36,"h36",$row),new Event($row->h37,"h37",$row),new Event($row->h38,"h38",$row),new Event($row->h39,"h39",$row),
//                                    new Event($row->h40,"h40",$row),new Event($row->h41,"h41",$row),new Event($row->h42,"h42",$row),new Event($row->h43,"h43",$row),new Event($row->h44,"h44",$row),new Event($row->h45,"h45",$row),new Event($row->h46,"h46",$row),new Event($row->h47,"h47",$row)
//                                )
//                            );
//                        }
//                    }
//		}
		
	}catch(Exception $e){
		$ret['error'] = $e->getMessage();
	}
	return $ret;
}

function updateSlots($slots, $requestoruserid, $calendarref, $description,$status="1",$remove=false,$lessonId=0){
	$sl = array();

	foreach ($slots as $slot){
		$sl[] = $slot->slot;
	}
	return updateCalendar($slots[0]->userId, $slots[0]->date ,$sl, $status, $slots[0]->description, "0", $slots[0]->gameId, $requestoruserid, $calendarref, $description,false,$lessonId);
}

function updateCalendar($userid,$date,$slots,$status="1",$description="_",$availablespot="1", $gameId="0",$requestoruserid="0",$calendarref="0",$requestorTxt="",$remove=false,$lessonId=0){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();
		$event = new Event();
		$event->setEvent($status, $description, $availablespot, $gameId, $requestoruserid, $calendarref, $requestorTxt, $remove,"",$lessonId);
		$sql = "update `nk_user_calendar` set ";
		$i = 0;
		foreach ($slots as $slot){
			if($i == 0)
				$sql = $sql." `".$slot."` = ".$event;
			else
				$sql = $sql.", `".$slot."` = ".$event;
			$i++;
		}

		$sql = $sql." where `user_id` = '".$userid."' AND `date` = ".$date;
		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = 'add success';
			$ret['Data'] = $date;
		}

	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
	return $ret;
}

function insertCalendar($userid,$date,$slots,$status="1",$description="_",$availablespot="1", $gameId="0", $requestoruserid="0",$calendarref="0",$requestorTxt="",$lessonId=0){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();

		list ($day, $month, $year) = split ('-', gmdate('d-m-Y', $date));
		$event = new Event();
		//setEvent($status="",$description="",$availableSpot="",$gameId="", $requestoruserId="",$calendarRef="",$requestorTxt="",$isEmpty=0,$slot="",$lessonId="")
		$event->setEvent($status,$description,$availablespot,$gameId,$requestoruserid,$calendarref,$requestorTxt,0,"",$lessonId);
		$sql = "insert into `nk_user_calendar` (`user_id`, `date`, `day`, `month`,`year`";

		foreach ($slots as $slot){
			$sql = $sql.",`".$slot."`";
		}
		$sql =  $sql.") values ('".$userid."', ".$date.", '".$day."', '".$month."', '".$year."'";

		foreach ($slots as $slot){
			$sql = $sql.", ".$event."";
		}
		$sql = $sql.")";
		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = 'add success';
			$ret['Data'] = $date;
			$ret['Id'] = mysql_insert_id();
		}

	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
	return $ret;
}


function getMonthCalendar($userid){
	$events = array();
	$fullCalendar = listCalendar($userid);

	if (empty($fullCalendar))
		return "{}";
	if(!array_key_exists("events", $fullCalendar))
		return "{}";

	$ret ="{";
	$i = 0;
	foreach ($fullCalendar['events'] as $event) {
		list ($day, $month,$year) = split ('-', gmdate('d-m-Y',$event[2]));
		$key = "D".$day."_".$month."_".$year;
		$sizeof_day = sizeof($event);
		$notEmptySpots = 0;
		$availableSpots = 0;
		foreach ($event[6] as &$hour){
			if($hour->isEmpty)
				continue;
			$notEmptySpots++;
			if($hour->availableSpot == 1){
				$availableSpots++;
				continue;
			}
		}
		if($i != 0)
			$ret = $ret.',';

		if($notEmptySpots == 0){
			$ret = $ret.'"'.$key.'":"NOT_AVAILABLE"';
		}else if($availableSpots == $notEmptySpots){
			$ret = $ret.'"'.$key.'":"ALL_FREE"';
		}else if($availableSpots == 0){
			$ret = $ret.'"'.$key.'":"ALL_BOOKED"';
		}else {
			$ret = $ret.'"'.$key.'":"AVAILABLE_SPOTS"';
		}
		$i++;
	}
	$ret = $ret."}";
	return $ret;
}

function changeStatus($id, $userid, $status){
	//TODO
}

function acceptSchedule($id, $userid){
	//TODO
}

function addSlots($date, $schedules, $userid, $description,$overwrite,$games){

	$ret = array();
	$slots = getSlotsArray($schedules);
	$event = listCalendar($userid,$date);
	$sizeof_day = sizeof($event);

	if(sizeof($event['events']) == 0){
		$ret = insertCalendar($userid, $date ,$slots, 1, $description, 1,$games);
	}else{
		if($overwrite == 0){
			$times = split(',', $schedules);
			foreach ($times as $time){
                            $e = $event['events'][0][6][$time];
//				if(!$event['events'][0][6][$time]->isEmpty){
                                if(!$e->isEmpty && $e->availableSpot != 1){
					$ret['IsSuccess'] = false;
					$ret['ErrorCode'] = ErrorCode::$ALREADY_USED;
					return $ret;
				}
			}
		}
		$ret = updateCalendar($userid,$date,$slots,"1",$description,"1",$games);
	}
	return $ret;
}

function cancel($userid,$date,$schedules,$cancelClientEvents="0", $offSet=0){//TODO: Agrupar slots por
	try{
		$ret = array();
		$warnList = array();
		$lessonList = array();
		$clientEvents = array();
		$users = array();
		
		$loggedUser = $_SESSION['ch_user'];
		$slots = getSlotsArray($schedules);
		$event = getSlots($userid, $date, $slots);//gets the data from the selected slots
		if(sizeof($event) == 0){
			$ret['IsSuccess'] = false;
			$ret['ErrorCode'] = ErrorCode::$NO_EVENTS;
			return $ret;
		}else{
			if($cancelClientEvents == 0){
				foreach ($event as $e){
					if(!$e->isEmpty && $e->availableSpot != 1){
						$ret['IsSuccess'] = false;
						$ret['ErrorCode'] = ErrorCode::$ALREADY_SCHEDULED;
						return $ret;
					}
				}
			}else{
				$usersEvents = array();
				foreach ($event as $evn){
                                    //TODO: Check events with offset
					$events = getSlotsByCalendarId($evn->calendarRef,$slots);

					foreach ($events as $ev){
						$usersEvents[$ev->userId][]= $ev;
					}
				}
				foreach ($usersEvents as $userEvents){
					$res = updateStatus($userEvents, Status::$DELETED);
					if(!in_array($userEvents[0]->userId, $lessonList)){
						$lessonList[] = $userEvents[0]->lessonId;
					}
				}
			}
			$ret = updateCalendar($userid,$date,$slots,"1","","","","","","",true);
			
			foreach ($lessonList as $lessonId){
				$lesson = getLesson($lessonId);
				changeLessonStatus($lessonId, LessonStatus::CANCELED);
				$rate = $lesson->rate;
                                $returnValue = sizeof($event) * $rate;
                                
				if($lesson->coach_id == $_SESSION['user_id']){
					$user = getUserInfo($lesson->student_id);
                                           
                                        insertTransaction($user->user_id, TransactionStatus::ACTIVE, TransactionType::CREDIT_REFUND, $returnValue, $user->total_points, UserType::STUDENT, ("Refund from canceled lesson from ".$loggedUser['username']));
					insertTransaction($_SESSION['user_id'], TransactionStatus::ACTIVE, TransactionType::DEBIT_REFUND, $returnValue, $loggedUser['total_points'],UserType::COACH, ("Refund from canceled lesson to ".$user->username));
				}else if($lesson->student_id == $_SESSION['user_id']){
					$user = getUserInfo($lesson->coach_id);
                                        
                                        insertTransaction($user->user_id, TransactionStatus::ACTIVE, TransactionType::DEBIT_REFUND, $returnValue, $user->total_points,UserType::COACH, ("Refund from canceled lesson to ".$loggedUser['username']));
					insertTransaction($_SESSION['user_id'], TransactionStatus::ACTIVE, TransactionType::CREDIT_REFUND, $returnValue, $loggedUser['total_points'],UserType::STUDENT, ("Refund from canceled lesson from ".$user->username));
				}else{
					echo "\n----- BAD NEWS!! -----\n";
				}
				
				insertNotifications($user->id, NotificationTypes::$SCHEDULE_ALERT, $userid, "Schedule", "", "User ".$user->username." has canceled a schedule, please check your calendar","","/schedule.php");

			}
			
		}
	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
	return $ret;
}

function updateStatus($slots, $status){
	$sl = array();
	foreach ($slots as $slot){
		$sl[] = $slot->slot;
	}

	return updateCalendar($slots[0]->userId, $slots[0]->date ,$sl, $status, $slots[0]->description, "0", $slots[0]->gameId, $slots[0]->requestoruserId, $slots[0]->calendarref, $slots[0]->requestorTxt);
}

function getSequenceOfRequestor($index,$calendarId){
	$events = getAllSlotsByCalendarId($calendarId);
	$mainevent = $events[$index];
	$requestor = $mainevent->requestoruserId;

	$slots = array();
	$slots[]=$mainevent->slot;
	$ret = ($index<10?"0".$index:$index);
	for ($i = ($index+1); $i < 48; $i++) {
		if(!$events[$i]->isEmpty){
			if($events[$i]->requestoruserId == $requestor){
				$ret = $ret."#".($i<10?"0".$i:$i);
			}else
				break;
		}else
			break;
	}

	for ($i = ($index-1); $i >= 0; $i--) {
		if(!$events[$i]->isEmpty){
			if($events[$i]->requestoruserId == $requestor){
				$ret = $ret."#".($i<10?"0".$i:$i);
			}else
				break;
		}else
			break;
	}
	return '{"values":"'.$ret.'"}';
}

function getSequenceOfCalendarRef($index,$calendarId){
	$events = getAllSlotsByCalendarId($calendarId);
	$mainevent = $events[$index];
	$calendarRef = $mainevent->calendarRef;

	$slots = array();
	$slots[]=$mainevent->slot;
	$ret = ($index<10?"0".$index:$index);
	for ($i = ($index+1); $i < 48; $i++) {
		if(!$events[$i]->isEmpty){
			if($events[$i]->calendarRef == $calendarRef){
				$ret = $ret."#".($i<10?"0".$i:$i);
			}else
				break;
		}else
			break;
	}
	for ($i = ($index-1); $i >= 0; $i--) {
		if(!$events[$i]->isEmpty){
			if($events[$i]->calendarRef == $calendarRef){
				$ret = $ret."#".($i<10?"0".$i:$i);
			}else
				break;
		}else
			break;
	}
	return '{"values":"'.$ret.'"}';
}

function getSlotsArray($schedules,$offSet=0){
	$times = split(',', $schedules);
	foreach ($times as $time){
		$slots[] = Constants::$SLOTS_ARRAY[$time - $offSet];
	}
	return $slots;
}


function getDateFromTimeAndSlot($date, $schedules){
    $ret = array();
    $d = gmdate("Y-m-d",$date);
    $times = split(',', $schedules);

    $ret['start'] = $d." ".Constants::$TIMES_ARRAY[$times[0]];
    $ret['end'] = $d." ".Constants::$TIMES_ARRAY[$times[(sizeof($times)-1)]+1];
    return $ret;

}

function checkIfAlreadyUsed($date,$schedules,$offSet=0){
    
    $event = listCalendar($_SESSION['user_id'],$date);
    if(sizeof($event['events']) > 0){
        $times = split(',', $schedules);
        foreach ($times as $time){
                if(!$event['events'][0][6][$time - $offSet]->isEmpty){
                        $ret['IsSuccess'] = false;
                        $ret['ErrorCode'] = ErrorCode::$ALREADY_USED;
                        return $ret;
                }
        }
    }

    $ret['IsSuccess'] = true;
    return $ret;
}
function scheduleLesson($userid, $date, $schedules, $description, $gameId, $total, $studentPoints, $coachPoints,$requestorname,$coachname,$rate,$offSet=0,$sendEmail="0",$email=""){
	
    $ret = array();
    $times = split(',', $schedules);
    $s = sizeof($times);
    
    $schedulesArray = array();
    $j = 0;
    for ($i=0; $i<= ($s-1); $i++){
        $schVar = intval($times[$i]);
        $beforeSchVar = 0;
        if($i > 0){
            $beforeSchVar = intval($times[$i-1]);
        }
        if(($schVar-$beforeSchVar) > 1){
            $j++;
            $schedulesArray[$j][]=$schVar;
            continue;
        }
        $schedulesArray[$j][]=$schVar;
    }
    
    foreach($schedulesArray as $scd){
        
        $schedulesX = implode(",", $scd);
	$slots = getSlotsArray($schedulesX);
        
        $slotsStudent = getSlotsArray($schedulesX,$offSet);
        
	$calendarRef = "0";
	$event = listCalendar($_SESSION['user_id'],$date);
	$events = getSlots($userid,$date,$slots);
        
        $eventsStudent = getSlots($userid,$date,$slotsStudent);
        
	$hours = sizeof($slots);
	$startEnd = getDateFromTimeAndSlot($date, $schedulesX);
        $lessonPrice = ($rate * sizeof($scd))/2;
        $lesson = insertLesson($lessonPrice, $rate, $userid, $_SESSION['user_id'], $gameId, ($hours/2), LessonStatus::OPEN, ($offSet/2),$startEnd['start'], $startEnd['end']);
        $lessonId = $lesson['lessonId'];
        if(sizeof($event['events']) > 0){
                //If a record for the day already exists, it just updates the selected spots
		$result = updateCalendar($_SESSION['user_id'], $date ,$slotsStudent,Status::$ACTIVE,$description,"0",$events[0]->gameId,$_SESSION['user_id'],$events[0]->calendarId,"",false,$lessonId);
		$calendarRef = $event['events'][0][0];
	}else{
                $ret = insertCalendar($_SESSION['user_id'], $date ,$slotsStudent, "2", $description,"0",$gameId,$_SESSION['user_id'], $events[0]->calendarId,"",$lessonId);
		$calendarRef = $ret['Id'];
	}
        //Updating coach calendar
	$ret = updateSlots($events, $_SESSION['user_id'], $calendarRef, $description, Status::$ACTIVE,false, $lessonId);
	if($ret['IsSuccess']){
		$desc = "Lesson, coach: ".$coachname;
                $ch_user = getUserInfo($_SESSION['user_id']);
		$studentTransaction = insertTransaction($_SESSION['user_id'], TransactionStatus::BOOKED, TransactionType::DEBIT, $lessonPrice, $ch_user->total_points,0.0, UserType::STUDENT, $desc);
		if($studentTransaction['IsSuccess'])
                    $studentPoints = $studentTransaction['newpoints'];
                
                $user = getUserInfo($userid);
		$descr = "Lesson, student: ".$requestorname;
		$coachTransaction = insertTransaction($userid, TransactionStatus::BOOKED, TransactionType::CREDIT, $lessonPrice, $user->total_points, floatval($lesson['commision']) , UserType::COACH, $descr);
                if($coachTransaction['IsSuccess'])
                    $coachPoints = $coachTransaction['newpoints'];
                
                if($sendEmail == "1"){
                    sendScheduleEmail($email, $coachname, "Chiron Gaming - Schedule","scheduled",$ch_user->username, $startEnd['start']." - ".$startEnd['end']);
                }
	}
    }
    insertNotifications($userid, 1, $_SESSION['user_id'], "Schedule", "", "User ".$requestorname." has schedule a lesson, please check your calendar","","/schedule.php");
    return $ret;
}


function sendScheduleEmail($to,$toName,$subject,$action,$fromName,$date){
    
    $TemplateFile="email_templates/schedule_notification.html";
    $TemplateFileHandler=fopen($TemplateFile,"r");
    $TemplateMessage=fread($TemplateFileHandler,filesize($TemplateFile));
    fclose($TemplateFileHandler);

    $TemplateMessage=str_replace("[TO]",$toName,$TemplateMessage);
    $TemplateMessage=str_replace("[FROM]",$fromName,$TemplateMessage);
    $TemplateMessage=str_replace("[ACTION]",$action,$TemplateMessage);
    $TemplateMessage=str_replace("[DATE]",$date,$TemplateMessage);
    
    sendEmail($to,$subject,$TemplateMessage);
}

function getUserInfo($userId){
	try{
		$db = new DBConnection();
		$db->getConnection();
		$sql = "SELECT * FROM `nk_user` WHERE `user_id` = '".$userId."'";
		$handle = mysql_query($sql);
		$row = mysql_fetch_object($handle);
		
		return $row;

	}catch(Exception $e){
		return  $e->getMessage();
	}
}

function getGames(){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();
		$sql = "SELECT * FROM `nk_game`";
		$handle = mysql_query($sql);
		$rows = array();
		while ($games = mysql_fetch_object($handle)) {
			$ret[]=$games;
		}
		return $ret;
	}catch(Exception $e){
		return  $e->getMessage();
	}
}

function getGame($gameId){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();
		$sql = "SELECT * FROM `nk_game` where game_id = ".$gameId;
		$handle = mysql_query($sql);
		return mysql_fetch_object($handle);
	}catch(Exception $e){
		return  $e->getMessage();
	}
}

function getUserFullInfo($userId){//TODO: separate in 3 functions
	try{
		$db = new DBConnection();
		$db->getConnection();
		$sql = "SELECT * FROM `nk_user` WHERE `user_id` = '".$userId."'";
		$handle = mysql_query($sql);
		$row = mysql_fetch_object($handle);
		$row->onlinestatus = getUserStatusObject($row);
		$sql = "SELECT ut.user_type_id, ut.user_type_name, ut.user_type_description  FROM nk_user_type_user_relation utr, nk_user_type ut WHERE utr.user_type_id = ut.user_type_id AND utr.user_id =".$row->user_id;
		$handle = mysql_query($sql);
		$row->user_types = array();
		while ($types = mysql_fetch_object($handle)) {
			$row->user_types[]=$types;
		}

		$sql = "SELECT * FROM nk_game g, nk_user_game ug WHERE g.game_id = ug.game_id AND ug.user_type_id = 1 AND ug.user_id =".$row->user_id;

		$handle = mysql_query($sql);
		$row->games = array();
		while ($games = mysql_fetch_object($handle)) {
			$row->games[]=$games;
		}

		if($userId == $_SESSION['user_id'])
			$row->owncalendar = '1';
		else{
			$row->owncalendar = '0';
			$sql = "SELECT * FROM `nk_user` WHERE `user_id` = '".$_SESSION['user_id']."'";
			$handle = mysql_query($sql);
			$row->loggedUser = mysql_fetch_object($handle);
		}
		
		return $row;
	}catch(Exception $e){
		return  $e->getMessage();
	}
}

function updateScheduleStatus($id, $startTime, $endTime, $status){

}
function removeSlots($userid,$date,$schedules){
	$slots = getSlotsArray($schedules);
	$ret = updateCalendar($userid,$date,$slots,"1","","","","","","",true);
}

function getUsers(){
	try{
		$db = new DBConnection();
		$db->getConnection();

		$sql = "select * from `nk_user`";
		$handle = mysql_query($sql);
		echo "<br/>";
		while ($row = mysql_fetch_object($handle)) {
			echo "<a onclick=\"showCalendar('".$row->user_id."');\" id=\"calendar_".$row->user_id."\" href=\"javascript:;\">".$row->user_id." - ".$row->email."</a>";
			echo "<br/>";
		}
	}catch(Exception $e){
		$ret['error'] = $e->getMessage();
	}
}


function getNotifications(){
	$ret = array();
	$ret['notifications'] = array();
	try{
		$db = new DBConnection();
		$db->getConnection();
		//Notification
		$sql = "select * from `nk_notification` where `user_id` = " .$_SESSION['user_id'];

		$sql = $sql." order by `modified_date` DESC";
		$handle = mysql_query($sql);
		$unread = 0;
		while ($row = mysql_fetch_object($handle)) {
			if($row->new == 'Y')
				$unread++;
			$ret['notifications'][] = array(new Notification($row));
		}
		$ret['unreaded'] = $unread;
                
                $sql = "SELECT * FROM ".TABLEPREFIX."_user WHERE user_id = ".$_SESSION['user_id'];
                $handle = mysql_query($sql);
                $_SESSION['ch_user'] = mysql_fetch_array($handle);

	}catch(Exception $e){
		$ret['error'] = $e->getMessage();
	}
	return $ret;
}


function updateNotifications($userId, $title="", $introduction="", $text="", $date=""){//TODO
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();
		$sql = "update `nk_notification` set ";
		if($title != "")
			$sql = $sql.",`title` = ";
		if($introduction != "")
			$sql = $sql.",`introduction`";
		if($text != "")
			$sql = $sql.",`text`";
		if($date != "")
			$sql = $sql.",`date_value`";

		$sql = $sql.",`modified_date`,`creation_date`) VALUES (".$userId;

		if($title != "")
			$sql = $sql.",'".$title."'";
		if($introduction != "")
			$sql = $sql.",'".$introduction."'";
		if($text != "")
			$sql = $sql.",'".$text."'";
		if($date != "")
			$sql = $sql.",'".php2MySqlTime(js2PhpTime($date))."'";

		$sql = $sql.",SYSDATE(), SYSDATE());";

		// 		echo $sql;
		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = 'add success';
			$ret['Data'] = $date;
			$ret['Id'] = mysql_insert_id();
		}

	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}

	return $ret;
}

function insertNotifications($userId, $type=1, $origin="" , $title="", $introduction="", $text="", $date="",$link="",$extra_data="", $sendmail=false,$user=null){ 
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();
		$sql = "INSERT INTO `nk_notification` (`user_id`,`nofication_type`";
		if($origin != "")
			$sql = $sql.",`origin`";
		if($title != "")
			$sql = $sql.",`title`";
		if($introduction != "")
			$sql = $sql.",`introduction`";
		if($text != "")
			$sql = $sql.",`text`";
		if($date != "")
			$sql = $sql.",`date_value`";
		if($link != "")
			$sql = $sql.",`link`";
		if($extra_data != "")
			$sql = $sql.",`extra_data`";

		$sql = $sql.",`modified_date`,`creation_date`) VALUES (".$userId.",".$type;

		if($origin != "")
			$sql = $sql.",'".$origin."'";
		if($title != "")
			$sql = $sql.",'".$title."'";
		if($introduction != "")
			$sql = $sql.",'".$introduction."'";
		if($text != "")
			$sql = $sql.",'".$text."'";
		if($date != "")
			$sql = $sql.",'".php2MySqlTime(js2PhpTime($date))."'";
		if($link != "")
			$sql = $sql.",'".$link."'";
		if($extra_data != "")
			$sql = $sql.",'".$extra_data."'";
			
		$sql = $sql.",NOW(), NOW());";
		
//		logToFile($sql);		
		
		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = 'add success';
			$ret['Data'] = $date;
			$ret['Id'] = mysql_insert_id();
		}
		
	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
        if($sendmail){
            $body = "<h1></h1>
                <div>Hi ".$user->username."<br/></div>
            ";
            sendEmail($mail, $title,($link));
        }
            
	return $ret;

}

function sendEmail($to,$subject,$body, $from=""){
    
    if($from == "")
        $from = "no-reply@chirongaming.com";
    
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: Chirongamming <no-reply@chirongaming.com>';
    mail($to, $subject, $body, $headers);
}

function markAsRead($ids){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();

		$sql = "update `nk_notification` set `new` = 'N', `date_readed` = SYSDATE()  where `notification_id` in (".str_replace('#',',',$ids).")";

		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = 'add success';
		}

	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
	return $ret;
}

function deleteNotifications($ids){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();

		$sql = "DELETE FROM `nk_notification` WHERE `notification_id` IN (".str_replace('#',',',$ids).")";

		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = 'add success';
		}

	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
	return $ret;
}

function getNextEvents($date,$day){

	$date = gmdate("d#m#Y#H#i", $date);

	list ($d, $month, $year,$hour,$minute) = split ('#',$date);

	$calendar= listCalendar($_SESSION['user_id'], $day);

	if (empty($calendar))
		return "{}";
	if(!array_key_exists("events", $calendar))
		return "{}";

	$ret = array();

	$events = $calendar['events'][0][6];
	for ($i = (($hour*2)+1); $i < 48; $i++) {
		$event = $events[$i];
		if($event->isEmpty || $event->availableSpot == 0 || $event->status == 3)
			continue;
		else{
			$ret['events'][] = $event;
		}
	}

	return $ret;
}

function doPing(){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();

		$sql = "UPDATE `nk_user` SET `last_ping_time` = SYSDATE(),is_logged_in = 1 WHERE `user_id` = ".$_SESSION['user_id'];

		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = 'update success';
		}

	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
	return $ret;
}
function insertLesson($price,$rate, $coachid, $studentid, $gameId, $hours="1",$status=1, $time_zone_offset = 0, $start=0, $end=0, $lessonType=0){
	try{
		$db = new DBConnection();
		$db->getConnection();
		$user = $_SESSION['ch_user'];
		$game = getGame($gameId);
                $coach_name = "";
                $student_name = "";
		if($coachid == $_SESSION['user_id']){
                    $coach_name = $user['username'];
                    $st = getUserInfo($studentid);
                    $student_name = $st->username;
                }else{
                    $student_name = $user['username'];
                    $co = getUserInfo($coachid);
                    $coach_name = $co->username;
                }
                
                $commision = getDefaultComission();   
                
		$sql = "INSERT INTO `nk_lesson` (`coach_name`,`student_name`,`lesson_type`,`time_zone_offset`,`game_id`,`lesson_title`,`lesson_description`, `coach_id`, `student_id`, `lesson_price`,`rate`, `lesson_commision_admin`, `lesson_status_id`, `lesson_payment_date`, `lesson_payment_transaction_no`, `lesson_closed_date`, `date_added`, `date_edited`,`start`,`end`) VALUES";
		$sql .= " ('".$coach_name."','".$student_name."',".$lessonType.",".$time_zone_offset.",".$gameId.",'Lesson for game ".$game->game_name."', 'Lesson for game ".$game->game_name."',".$coachid.", ".$studentid.", ".$price.",".$rate.", ".$commision.", $status, NOW(), '0', NOW(), NOW(), NOW()";//,NOW(), NOW()+INTERVAL ".$hours." HOUR)";
		if($start == 0){
                    $sql .= ",NOW()";
                }else{
                    $sql .= ",'".$start."'";
                }
                if($end == 0){
                    $sql .= ", NOW()+INTERVAL ".$hours." HOUR";
                }else{
                    $sql .= ",'".$end."'";
                }
                    $sql .= ")";
                    
//		logToFile($sql);
                
		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['gameName'] = $game->game_name;
			$ret['text'] = "<div>User <b>".$user['username']."</b> has requested an instant lesson for <b>".$game->game_name."</b></div><div style=\'margin-top: 5px; font-size: 12px\'>If you confirm, the student\'s points will be instantly charged. You must initiate a chat and start coaching now!</div>";
			$ret['lessonId'] = mysql_insert_id();
                        $ret['commision'] = $commision;
		}
	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}

	return $ret;
}

function getLesson($lessonId){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();
		$sql = "SELECT * FROM `nk_lesson` WHERE `lesson_id` = ".$lessonId;

		$handle = mysql_query($sql);
		$row = mysql_fetch_object($handle);
		
		return $row;
	}catch(Exception $e){
		$ret['error'] = $e->getMessage();
	}
	return $ret;
}
function changeLessonStatus($lessonId, $status, $statusReason=""){
	
	try{
            $ret = array();
            $db = new DBConnection();
            $db->getConnection();
            $sql = "UPDATE `nk_lesson` SET `lesson_status_id` = ".$status.", `status_reason` = '".$statusReason."' WHERE `lesson_id` = ".$lessonId;
            if(mysql_query($sql)==false){
                    $ret['IsSuccess'] = false;
                    $ret['Msg'] = mysql_error();
            }else{
                    $ret['IsSuccess'] = true;
                    $ret['Msg'] = 'add success';
            }
		
	}catch(Exception $e){
		$ret['error'] = $e->getMessage();
	}
	return $ret;
}
function removeCanceledLesson($lessonId, $utype){
    try{
            $ret = array();
            $ret['lessonId'] = $lessonId;
            $db = new DBConnection();
            $db->getConnection();
            $sql = "UPDATE `nk_lesson` SET `".$utype."` = 1 WHERE `lesson_id` = ".$lessonId;
            if(mysql_query($sql)==false){
                    $ret['IsSuccess'] = false;
                    $ret['Msg'] = mysql_error();
                    
            }else{
                    $ret['IsSuccess'] = true;
                    $ret['Msg'] = 'add success';
            }
		
	}catch(Exception $e){
		$ret['error'] = $e->getMessage();
	}
	return $ret;
}

function acceptInstantLesson($accept, $lessonId, $notifId){
        
	$ret = array();
        try{
            markAsRead($notifId);
            $db = new DBConnection();
            $db->getConnection();
            $coach = $_SESSION['ch_user'];
            $lesson = getLesson($lessonId);
            if($accept == 0){
                $sql = "UPDATE `nk_lesson` SET `date_edited` = NOW(), `lesson_payment_date` = NOW(),`status_reason` = 'Instant Lesson not accepted', `lesson_status_id` = ".LessonStatus::CANCELED." WHERE `lesson_id` = ".$lessonId;
//                logToFile($sql);
                $text = "<b>".$coach['username']."</b> refused your Instant Lesson request. Please try again later.";
                if(mysql_query($sql)==false){
                        $ret['IsSuccess'] = false;
                        $ret['text'] = $text;
                         $ret['studentid'] = $lesson->student_id;
                        $ret['Msg'] = mysql_error();
                }else{
                        $ret['IsSuccess'] = true;
                        $ret['text'] = $text;
                        $ret['Msg'] = 'add success';
                         $ret['studentid'] = $lesson->student_id;
                }
                return $ret;
            }else{
                

                
                $student = getUserInfo($lesson->student_id);
                if(intval($student->total_points) >= intval($lesson->lesson_price)){
                        insertTransaction($lesson->student_id, TransactionStatus::BOOKED , TransactionType::DEBIT, $lesson->lesson_price,$student->total_points, 0.0,UserType::STUDENT, "Instant Lesson, coach: ".$coach->username);
                        insertTransaction($_SESSION['user_id'], TransactionStatus::BOOKED , TransactionType::CREDIT, $lesson->lesson_price, $coach['total_points'],$lesson->lesson_commision_admin,UserType::COACH, "Instant Lesson, student: ".$student->username);

                        $sql = "UPDATE `nk_lesson` SET `date_edited` = NOW(), `lesson_payment_date` = NOW(), `lesson_status_id` = ".LessonStatus::PAID." WHERE `lesson_id` = ".$lessonId;
                        if(mysql_query($sql)==false){
                            $ret['IsSuccess'] = false;
                            $ret['Msg'] = mysql_error();
                        }else{
                            $ret['coachid'] = $_SESSION['user_id'];
                            $ret['studentid'] = $lesson->student_id;
                            $ret['lessonId'] = $lessonId;
                            $ret['IsSuccess'] = true;
                            $ret['text'] = "<div>User <b>".$coach['username']."</b> confirmed your Instant Lesson request.</div><div style=\'margin-top: 5px; font-size: 12px\'><b>".$coach['username']."</b> will initiate a chat to start your lesson shortly</div>";
                            $ret['Msg'] = 'add success';
                        }
                }else{
                        $sql = "UPDATE `nk_lesson` SET `date_edited` = NOW(),`status_reason` = 'Not enough points', `lesson_payment_date` = NOW(), `lesson_status_id` = ".LessonStatus::CANCELED." WHERE `lesson_id` = ".$lessonId;
                        if(mysql_query($sql)==false){
                                $ret['IsSuccess'] = false;
                                $ret['Msg'] = mysql_error();
                        }
                        $ret['IsSuccess'] = false;
                        $ret['Msg'] = 'The student doesn\'t have enough points';
                }
            }

        }catch(Exception $e){
		$ret['error'] = $e->getMessage();
	}
	return $ret;
}

function calculateCommission($value,$comission=0.0,$userId, $userType, $txType){
    try{
        $value = floatval($value);
        if($comission == 0.0)
            $comission = getDefaultComission();
        $percentage = floatval($comission)/100;
//        if(TransactionType::CREDIT_REFUND == $txType){
//            $value = $value+($value*$percentage);
//            //remove points to chiron 
//        }else
            if(TransactionType::CREDIT == $txType || TransactionType::CREDIT_REFUND == $txType){
            $value = $value-($value*$percentage);
            //add points to chiron 
        }
        
        return $value;
    
    }catch(Exception $e){
        return $value;
    }
}

function insertTransaction($userId, $txStatus, $txType, $value, $oldPoints, $comission=0.0, $userType=1, $description=""){
	$ret = array();
	try{
	
            $value = calculateCommission($value,$comission,$userId,$userType,$txType);
            $txDate = date("d/m/Y : H:i:s", time());
            $txId = md5($userId.$txDate);
            $newpoints = 0;
            if($txType == TransactionType::CREDIT || $txType == TransactionType::DEBIT_REFUND)
                    $newpoints = $oldPoints+$value;
            else
                    $newpoints = $oldPoints-$value;

            $db = new DBConnection();
            $db->getConnection();
            $sql = "INSERT INTO nk_user_transaction(commission, nk_user_id, tx_date, rate_point_id, status, nk_tx_id,transaction_type, qty_points, description, total_before , total_after) VALUES(". $comission .",". $userId .", STR_TO_DATE('". $txDate ."', '%d/%m/%Y : %H:%i:%s'),-1, ". $txStatus .",'". $txId ."',".$txType.",".$value.",'".$description."',".$oldPoints.", ".$newpoints.")";
            //logToFile($sql);

            if(mysql_query($sql)==false){
                    $ret['IsSuccess'] = false;
                    $ret['Msg'] = mysql_error();
            }else{
                    $ret['IsSuccess'] = true;
                    $ret['Msg'] = 'add success';
                    $ret['newpoints'] = $newpoints;
                    updateUserPoints($newpoints, $userId);
            }

	}catch(Exception $e){
		$ret['error'] = $e->getMessage();
	}
	return $ret;
}
function getDefaultComission(){
    try{
        
        $db = new DBConnection();
        $db->getConnection();
        $sql = "SELECT default_comision_value FROM nk_default_comision";
        $handle = mysql_query($sql);
        $row = mysql_fetch_object($handle);
        return $row->default_comision_value;
        
    }catch(Exception $e){
        return 1;
    }
    return 1;
}

function updateUserPoints($points,$userId=""){
    $ret = array();
	try{
		if($userId == "")
			$userId = $_SESSION['user_id'];
			
		$sql = "UPDATE ".TABLEPREFIX."_user SET total_points = '".$points."' WHERE user_id = ".$userId;
		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] ="Error: ".mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = "";
                        
                        if($userId == $_SESSION['user_id']){
                            $sql = "SELECT * FROM ".TABLEPREFIX."_user WHERE user_id = ".$userId;
                            $handle = mysql_query($sql);
                            $_SESSION['ch_user'] = mysql_fetch_array($handle);
                        }
                        
                      //  $loggedUser = $_SESSION['ch_user'];
                        
		}
	
	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
	return $ret;
}

function getAllEvents(){

	$calendar= listCalendar($_SESSION['user_id']);

	if (empty($calendar))
		return "{}";
	if(!array_key_exists("events", $calendar))
		return "{}";

	$ret = array();

	$events = $calendar['events'][0][6];
	foreach($events as $event){

		if($event->isEmpty || $event->availableSpot == 0 || $event->status == 3) 
			continue;
		else{
			$ret['events'][] = $event;
		}
	}

	return $ret;
}
function addAsFriend($user){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();
                
                $sqlCheck =  "SELECT * FROM nk_user_friends_list WHERE friend_status = 2 AND ((nk_user_id_owner='".$_SESSION['user_id']."' or nk_user_id_friend='".$_SESSION['user_id']."') and (nk_user_id_owner='".$user."' or nk_user_id_friend='".$user."'))";
//                logToFile($sqlCheck);
                $handle = mysql_query($sqlCheck);
                $num_rows = mysql_num_rows($handle);
                if($num_rows>0){
                    $ret['IsSuccess'] = true;
                    $ret['Msg'] = 'add success';
                    return $ret;
                }
                
		$sql = "INSERT INTO `nk_user_friends_list` (`nk_user_id_owner`,`nk_user_id_friend`,`friend_status`, `cdate`) VALUES('".$_SESSION['user_id']."','".$user."', 2, SYSDATE());";
//		logToFile($sql);
                if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = 'add success';
			$ret['Id'] = mysql_insert_id();
		}

	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}

	return $ret;
}

function getUserType($userid){
    $ret = array();
    try{
            $db = new DBConnection();
            $db->getConnection();
            $sql = "SELECT * FROM `nk_user_type_user_relation` utr, `nk_user_type` ut WHERE ut.user_type_id = utr.user_type_id AND user_id = ".$userid." ORDER BY ut.user_type_id";

            $handle = mysql_query($sql);
            $unread = 0;
            $types = array();
            while ($row = mysql_fetch_object($handle)) {
                $types[] = $row->user_type_id;   
            }
            if (in_array("1", $types)) {
                $sql = "SELECT * FROM `nk_user_game` WHERE user_type_id = 1 AND user_id = ".$userid;
                $handle = mysql_query($sql);
                while ($row = mysql_fetch_object($handle)) {
                    $ret['link'] = "coachdetails.php?coach_id=".$userid."&game_id=".$row->game_id;
                    return $ret;
                }
                $ret['link'] = "coachdetails.php?coach_id=".$userid;
                return $ret;
            }
            if (in_array("3", $types)) {
                $sql = "SELECT * FROM `nk_user_game` WHERE user_type_id = 3 AND user_id = ".$userid;
                $handle = mysql_query($sql);
                while ($row = mysql_fetch_object($handle)) {
                    $ret['link'] = "training_partner_details.php?training_partner_id=".$userid."&game_id=".$row->game_id;
                    return $ret;
                }
                $ret['link'] = "training_partner_details.php?training_partner_id=".$userid;
                return $ret;
            }
            if (in_array("2", $types)) {
                $ret['link'] = "studentdetails.php?student_id=".$userid;
                return $ret;
            }
            

    }catch(Exception $e){
            $ret['error'] = $e->getMessage();
    }
    return $ret;
}
function removeNotificationOlderThan($days=10){
    $todaySeconds = mktime();
    $dateSeconds = $todaySeconds - (3600*24*$days);
    $d = gmdate("Y-m-d",$dateSeconds);
    $sql = "SELECT * FROM nk_notification WHERE creation_date < '".$d."' ORDER BY creation_date DESC";
    $db = new DBConnection();
    $db->getConnection();
   // logToFile($sql);
    $handle = mysql_query($sql);
    $row = mysql_fetch_array($handle);
    
    echo $sql;
}
function getAllLessons(){
     
     $ret = array();
	try{
               
		$db = new DBConnection();
		$db->getConnection();
		$sql = "SELECT * FROM nk_lesson WHERE coach_id = ".$_SESSION['user_id']." OR student_id = ".$_SESSION['user_id']." ORDER BY start ASC";

		$handle = mysql_query($sql);
		//$row = mysql_fetch_object($handle);
                while ($row = mysql_fetch_object($handle)) {
			$ret[] = $row;
		}
//		 $ret['IsSuccess'] = true;
		return $ret;
	}catch(Exception $e){
                $ret['IsSuccess'] = false;
		$ret['error'] = $e->getMessage();
	}
	return $ret;
}

function cancelLesson($lessonId, $reason, $date,$lesson_price, $coachId, $studentId,$lessThan24hr="0"){
    $ret = array();
    try{
        
        $ret = changeLessonStatus($lessonId, LessonStatus::CANCELED, $reason);
        $loggedUser = $_SESSION['ch_user'];
        list ($year, $month, $d) = split('-',$date);
        $gmt = gmmktime(0, 0, 0, $month, $d , $year);

        $coachSlots = array();
        $coachFullArray = listCalendar($coachId,$gmt);
        foreach ($coachFullArray['events'][0][6] as $coachEve){
            if($coachEve->lessonId == $lessonId){
                $coachSlots[] = $coachEve->slot;
            }
        }

        $studentSlots = array();
        $studentFullArray = listCalendar($studentId,$gmt);
        foreach ($studentFullArray['events'][0][6] as $studentEve){
            if($studentEve->lessonId == $lessonId){
                $studentSlots[] = $studentEve->slot;
            }
        }
        $user;
        $lesson = getLesson($lessonId);
        $returnValue = $lesson_price;
        if($coachId == $_SESSION['user_id']){
                $user = getUserInfo($studentId);
                $ret = updateCalendar($studentFullArray['events'][0][6][0]->userId, $studentFullArray['events'][0][6][0]->date ,$studentSlots,"1","","","","","","",true);
                $ret = updateCalendar($loggedUser['user_id'],$gmt,$coachSlots,"1","","","","","","",true);
                
                insertTransaction($user->user_id, TransactionStatus::ACTIVE, TransactionType::DEBIT_REFUND, $returnValue, $user->total_points,0.0,UserType::STUDENT, ("Refund from canceled lesson from ".$loggedUser['username']));
                insertTransaction($coachId, TransactionStatus::ACTIVE, TransactionType::CREDIT_REFUND, $returnValue, $loggedUser['total_points'],$lesson->lesson_commision_admin,UserType::COACH, ("Refund from canceled lesson to ".$user->username));
        }else if($studentId == $_SESSION['user_id']){
                $user = getUserInfo($coachId);
                $ret = updateCalendar($coachFullArray['events'][0][6][0]->userId, $coachFullArray['events'][0][6][0]->date ,$coachSlots, Status::$AVAILABLE, $coachFullArray['events'][0][6][0]->description, "1", $coachFullArray['events'][0][6][0]->gameId, "", "", "");
                $ret = updateCalendar($loggedUser['user_id'],$gmt,$studentSlots,"1","","","","","","",true);
                if($lessThan24hr == "0"){
                    insertTransaction($user->user_id, TransactionStatus::ACTIVE, TransactionType::CREDIT_REFUND, $returnValue, $user->total_points,$lesson->lesson_commision_admin,UserType::STUDENT, ("Refund from canceled lesson to ".$loggedUser['username']));
                    insertTransaction($studentId, TransactionStatus::ACTIVE, TransactionType::DEBIT_REFUND, $returnValue, $loggedUser['total_points'],0.0,UserType::COACH, ("Refund from canceled lesson from ".$user->username));
                }
                
        }else{
//                echo "\n----- BAD NEWS!! -----\n";
                $ret['IsSuccess'] = false;
        }
        $sendmail = ($user->mail_on_schedule_cancelation == '1');
        if($sendmail == "1"){
            sendScheduleEmail($user->email, $user->username, "Chiron Gaming - Schedule Canceled" ,"canceled",$loggedUser['username'], $lesson->start." - ".$lesson->end);
        }
        $ret = insertNotifications($user->user_id, NotificationTypes::$SCHEDULE_ALERT, $_SESSION['user_id'], "Schedule", "", "User ".$loggedUser['username']." has canceled a schedule, please check your calendar","","/schedule.php","", $sendmail,$user);
        
    
    }catch(Exception $e){
            $ret['error'] = $e->getMessage();
            $ret['IsSuccess'] = false;
    }
    return $ret;
}


function javascriptDateToPHP($day){
	if(getPostOrGetParam("originalDate")=='1')
		return $day;
	if($day == 0)
		return 0;
	$gmdate = gmdate("j#n#Y", ($day/1000)+(3600));
	list ($d, $month, $year) = split('#',$gmdate);
	$gmt = gmmktime(0, 0, 0, $month, $d , $year);
	return $gmt;
}
function getNewMessages($lastId){
    $ret = array();
    try{
        $db = new DBConnection();
        $db->getConnection();
        $sql = "SELECT * FROM `frei_chat` WHERE `id` > ".$lastId." AND ( `to` = '".$_SESSION['user_id']."' OR `from` = '".$_SESSION['user_id']."')";
        $handle = mysql_query($sql);
        while ($row = mysql_fetch_object($handle)) {
            $ret['messages'][] = $row;
        }
        $ret['IsSuccess'] = true;
        $ret['ownerId'] = $_SESSION['user_id'];
        return $ret;
//        $ret['events'][] = $event;
    }catch(Exception $e){
            $ret['error'] = $e->getMessage();
            $ret['IsSuccess'] = false;
    }
    return $ret;
}
if(isset($_GET['method']) || isset($_POST['method'])){
	header('Content-type:text/javascript;charset=UTF-8');

	$method = getPostOrGetParam("method");

	switch ($method) {
		case "addslots":
			$oneDay = 3600*24;
			$schedules = getPostOrGetParam("schedules");
			$description = getPostOrGetParam("description");
			$overwrite = getPostOrGetParam("overwrite");
			$games = getPostOrGetParam("games");
			$recursive = getPostOrGetParam("recursive");
			$date = javascriptDateToPHP(getPostOrGetParam("date"));
			$to = javascriptDateToPHP(getPostOrGetParam("to"));// se vier a null faço de hoje a um ano
			$from = javascriptDateToPHP(getPostOrGetParam("from"));
				
			if($recursive == 'false' || ($to == ""  && $from == "")){
				$ret = addSlots($date, $schedules, $_SESSION['user_id'], $description,$overwrite,$games);
				break;
			}
			if($from == "")
				$from = $date;
				
			if($to == "")
				$to = $date+($oneDay*365);
			$weekDays= getPostOrGetParam("weekDays");
			$diference = intval($to)-intval($from);
			$nrDays = round($diference/$oneDay);
			for ($i=0; $i <= $nrDays; $i++){
				$nextday = intval($from)+($oneDay*$i);
				list ($day, $month, $year) = split ('-', gmdate('d-m-Y', $nextday));
				$jd=cal_to_jd(CAL_GREGORIAN,$month,$day,$year);
				if($weekDays == null || $weekDays == ""){
					$ret = addSlots($nextday, $schedules, $_SESSION['user_id'], $description, $overwrite, $games);
					continue;
				}
				$weekDay = jddayofweek($jd,0);
				$pos = strpos($weekDays, ($weekDay.''));
				if ($pos === false)
					continue;

				$ret = addSlots($nextday, $schedules, $_SESSION['user_id'], $description,$overwrite,$games);
			}
			break;
		case "list":
			$userid = getPostOrGetParam("userid");
			if($userid == "")
				$userid = $_SESSION['user_id'];
			$date = javascriptDateToPHP(getPostOrGetParam("date"));
			
			$ret = listCalendar($userid, $date,intval(getPostOrGetParam("timezoneOffset")));
			break;
		case "remove":
			$ret = removeCalendar( $_POST["calendarId"]);
			break;
		case "cancel":
			$ret = cancel(getPostOrGetParam("userid"), javascriptDateToPHP(getPostOrGetParam("date")), getPostOrGetParam("schedules"), getPostOrGetParam("cancelClientEvents"));
			
                    break;
		case "updateScheduleStatus":
			$ret = updateScheduleStatus( $_POST["calendarId"]);
			break;
		case "getmonthcalendar":
			$ret=getMonthCalendar(getPostOrGetParam("userid"));
			break;
		case "getcalendarbyid":
			$ret=getCalendarById(getPostOrGetParam("calendarId"));
			break;
		case "getUserInfo":
			$ret=getUserFullInfo(getPostOrGetParam("userid"));
			break;
		case "scheduleLesson":
			$ret = scheduleLesson(getPostOrGetParam("userid"), javascriptDateToPHP(getPostOrGetParam("date")), getPostOrGetParam("schedules"), getPostOrGetParam("description"), getPostOrGetParam("gameid"),getPostOrGetParam("total"),getPostOrGetParam("studentPoints"),getPostOrGetParam("coachPoints"),getPostOrGetParam("requestorname"),getPostOrGetParam("coachname"),getPostOrGetParam("rate"),getPostOrGetParam("offSet"),getPostOrGetParam("sendEmail"),getPostOrGetParam("email"));
			break;
		case "getSequence":
			$ret = getSequenceOfRequestor(getPostOrGetParam("index"),getPostOrGetParam("calendarId"));
			break;
		case "getcalendarrefsequence":
			$ret = getSequenceOfCalendarRef(getPostOrGetParam("index"),getPostOrGetParam("calendarId"));
			break;
		case "removeSlots":
			$ret = removeSlots(getPostOrGetParam("userid"), javascriptDateToPHP(getPostOrGetParam("date")), getPostOrGetParam("schedules"));
			break;
		case "getNextEvents":
			$ret = getNextEvents(javascriptDateToPHP(getPostOrGetParam("date")),javascriptDateToPHP(getPostOrGetParam("day")));
			break;
		case "getAllEvents":
			$ret = listCalendar($_SESSION['user_id']);
			break;
		case "getGames":
                        $ret = getGames();
                        break;
		case "getNotifications":
			$ret = getNotifications();
			break;
		case "markAsRead":
			$ret = markAsRead(getPostOrGetParam("notificationIds"));
			break;
		case "insertNotifications":
			$userid = getPostOrGetParam("userid");
			if($userid == "")
                            $userid = $_SESSION['user_id'];
			$ret = insertNotifications($userid, getPostOrGetParam("notification_type"), getPostOrGetParam("origin"),getPostOrGetParam("title"), getPostOrGetParam("introduction"), getPostOrGetParam("text"), getPostOrGetParam("date"));
			break;
		case "deleteNotifications":
			$ret = markAsRead(getPostOrGetParam("notificationIds"));
			break;
		case "doInstantLessonWarning":
                        $hours = getPostOrGetParam("hours");
			$ret = insertLesson(getPostOrGetParam("value"), getPostOrGetParam("rate"), getPostOrGetParam("userid"), $userid = $_SESSION['user_id'], getPostOrGetParam("gameId"), getPostOrGetParam("hours"),  LessonStatus::WAITING_CONFIRMATION,getPostOrGetParam("offSet"), 'NOW()', "NOW()+INTERVAL ".$hours." HOUR",LessonType::INSTANT);
			
                        if($ret['IsSuccess'])
                            $ret = insertNotifications(getPostOrGetParam("userid"), NotificationTypes::$INSTANT_LESSON, $_SESSION['user_id'], "Instant Lesson", "Instant lesson request", $ret['text'], "", "/schedule.php", $ret['lessonId']);
			break;
		case "acceptInstantLesson":
			$ret = acceptInstantLesson(getPostOrGetParam("accept"), getPostOrGetParam("lessonId"), getPostOrGetParam("notifId"));
                       
                        if($ret['IsSuccess'])
                            $ret = insertNotifications($ret['studentid'], NotificationTypes::$INSTANT_LESSON_RESPONSE, $_SESSION['user_id'], "Instant Lesson", "Instant lesson request", $ret['text'], "", "/schedule.php", $ret['lessonId']);
			break;
                case "insertLesson":
			$ret = insertLesson(getPostOrGetParam("value"), getPostOrGetParam("rate"), getPostOrGetParam("userid"), $_SESSION['user_id'], getPostOrGetParam("gameId"));
			break;
		case "ping":
			$ret = doPing();
			break;	
		case "getUserStatus":
			$user = getUserInfo(getPostOrGetParam("userid"));
			$ret = getUserStatus($user);
			break;	
		case "addAsFriend":
			$ret = addAsFriend(getPostOrGetParam("userid"));
			break;	
		case "getLesson":
			$ret = getLesson(getPostOrGetParam("lessonId"));
			break;	
		case "getTime":
			$ret = array();
			$ret['DATE'] = date('l jS \of F Y h:i:s A');
			break;	
                case "checkIfAlreadyUsed":
                        $ret = checkIfAlreadyUsed(javascriptDateToPHP(getPostOrGetParam("date")), getPostOrGetParam("schedules"),getPostOrGetParam("offSet"));
                        break;	
                case "sendmail":
			$ret = array();
                        $subject = 'No reply';
                       
                        $headers  = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                        $headers .= 'From: Chirongamming - cashier <'.getPostOrGetParam("from").'>';
                        $TemplateFile="email_templates/transaction_mail.html";
                        $TemplateFileHandler=fopen($TemplateFile,"r");
                        $TemplateMessage=fread($TemplateFileHandler,filesize($TemplateFile));
                        fclose($TemplateFileHandler);

                        $TemplateMessage=str_replace("[TXID]","id",$TemplateMessage);
                        $TemplateMessage=str_replace("[DATE]","hoje",$TemplateMessage);
                        $TemplateMessage=str_replace("[QUANT_BOUGHT]","55",$TemplateMessage);
                        $TemplateMessage=str_replace("[TOTAL_ADDED]","50",$TemplateMessage);
                        $TemplateMessage=str_replace("[TOTAL_CHARGED]","5",$TemplateMessage);
                        $TemplateMessage=str_replace("[TOTAL_POINTS]","700",$TemplateMessage);

                        $ret['result'] = mail(getPostOrGetParam("to"), $subject, $TemplateMessage, $headers);
                        break;	
                    case "test":
                        echo md5(getPostOrGetParam("val"));
                        break;
                    case "removeNotificationOlderThan":
                        $ret = removeNotificationOlderThan(getPostOrGetParam("days"));
                        break;
                    case "getUserType":
                        $ret = getUserType(getPostOrGetParam("userid"));
                        break;
                    case "getAllLessons":
                        $ret = getAllLessons();
                        break;
                     case "cancelLesson":
                        $ret = cancelLesson(getPostOrGetParam("lessonId"), getPostOrGetParam("reason"), getPostOrGetParam("date"), getPostOrGetParam("lessonPrice"), getPostOrGetParam("coachId"), getPostOrGetParam("studentId"), getPostOrGetParam("lessThan24hr"));
                        break;
                    case "removeCanceledLesson":
                        $ret = removeCanceledLesson(getPostOrGetParam("lessonId"), getPostOrGetParam("utype"));
                        break;
                    case "getNewMessages":
                        $ret = getNewMessages(getPostOrGetParam("lastId"));
                        break;
	}
	echo json_encode($ret);
}
?>
