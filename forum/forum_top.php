<?php 
session_start();
if (!defined('IN_PHPBB')) 
    exit;
$_SESSION['currentpage'] = $_SERVER['SCRIPT_NAME'];
$_SESSION['currentrequest'] = urlencode($_SERVER['REQUEST_URI']);

$user = $_SESSION['PHPBB_USER'];
include('../common.php');
/*include('/general_include_forum.php');
include('/common.php');
include('includes/functions_display.php');

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup('viewforum');
$_SESSION['PHPBB_USER'] = $user;*/
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="content-type" content="text/html; charset={S_CONTENT_ENCODING}" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-language" content="{S_USER_LANG}" />
<meta http-equiv="imagetoolbar" content="no" />
<meta name="resource-type" content="document" />
<meta name="distribution" content="global" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chirongaming - Forum</title>
<?php echo (isset($this->_rootref['META'])) ? $this->_rootref['META'] : ''; ?>

<title><?php echo (isset($this->_rootref['SITENAME'])) ? $this->_rootref['SITENAME'] : ''; ?> &bull; <?php if ($this->_rootref['S_IN_MCP']) {  echo ((isset($this->_rootref['L_MCP'])) ? $this->_rootref['L_MCP'] : ((isset($user->lang['MCP'])) ? $user->lang['MCP'] : '{ MCP }')); ?> &bull; <?php } else if ($this->_rootref['S_IN_UCP']) {  echo ((isset($this->_rootref['L_UCP'])) ? $this->_rootref['L_UCP'] : ((isset($user->lang['UCP'])) ? $user->lang['UCP'] : '{ UCP }')); ?> &bull; <?php } echo (isset($this->_rootref['PAGE_TITLE'])) ? $this->_rootref['PAGE_TITLE'] : ''; ?></title>

<?php if ($this->_rootref['S_ENABLE_FEEDS']) {  if ($this->_rootref['S_ENABLE_FEEDS_OVERALL']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo (isset($this->_rootref['SITENAME'])) ? $this->_rootref['SITENAME'] : ''; ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>" /><?php } if ($this->_rootref['S_ENABLE_FEEDS_NEWS']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo ((isset($this->_rootref['L_FEED_NEWS'])) ? $this->_rootref['L_FEED_NEWS'] : ((isset($user->lang['FEED_NEWS'])) ? $user->lang['FEED_NEWS'] : '{ FEED_NEWS }')); ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>?mode=news" /><?php } if ($this->_rootref['S_ENABLE_FEEDS_FORUMS']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo ((isset($this->_rootref['L_ALL_FORUMS'])) ? $this->_rootref['L_ALL_FORUMS'] : ((isset($user->lang['ALL_FORUMS'])) ? $user->lang['ALL_FORUMS'] : '{ ALL_FORUMS }')); ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>?mode=forums" /><?php } if ($this->_rootref['S_ENABLE_FEEDS_TOPICS']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo ((isset($this->_rootref['L_FEED_TOPICS_NEW'])) ? $this->_rootref['L_FEED_TOPICS_NEW'] : ((isset($user->lang['FEED_TOPICS_NEW'])) ? $user->lang['FEED_TOPICS_NEW'] : '{ FEED_TOPICS_NEW }')); ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>?mode=topics" /><?php } if ($this->_rootref['S_ENABLE_FEEDS_TOPICS_ACTIVE']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo ((isset($this->_rootref['L_FEED_TOPICS_ACTIVE'])) ? $this->_rootref['L_FEED_TOPICS_ACTIVE'] : ((isset($user->lang['FEED_TOPICS_ACTIVE'])) ? $user->lang['FEED_TOPICS_ACTIVE'] : '{ FEED_TOPICS_ACTIVE }')); ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>?mode=topics_active" /><?php } if ($this->_rootref['S_ENABLE_FEEDS_FORUM'] && $this->_rootref['S_FORUM_ID']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo ((isset($this->_rootref['L_FORUM'])) ? $this->_rootref['L_FORUM'] : ((isset($user->lang['FORUM'])) ? $user->lang['FORUM'] : '{ FORUM }')); ?> - <?php echo (isset($this->_rootref['FORUM_NAME'])) ? $this->_rootref['FORUM_NAME'] : ''; ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>?f=<?php echo (isset($this->_rootref['S_FORUM_ID'])) ? $this->_rootref['S_FORUM_ID'] : ''; ?>" /><?php } if ($this->_rootref['S_ENABLE_FEEDS_TOPIC'] && $this->_rootref['S_TOPIC_ID']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo ((isset($this->_rootref['L_TOPIC'])) ? $this->_rootref['L_TOPIC'] : ((isset($user->lang['TOPIC'])) ? $user->lang['TOPIC'] : '{ TOPIC }')); ?> - <?php echo (isset($this->_rootref['TOPIC_TITLE'])) ? $this->_rootref['TOPIC_TITLE'] : ''; ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>?f=<?php echo (isset($this->_rootref['S_FORUM_ID'])) ? $this->_rootref['S_FORUM_ID'] : ''; ?>&amp;t=<?php echo (isset($this->_rootref['S_TOPIC_ID'])) ? $this->_rootref['S_TOPIC_ID'] : ''; ?>" /><?php } } ?>


<?php

?>
<?php if ($_SESSION['user_id'] != ''): ?>
<script type="text/javascript">
  if(window.name != "chironWrapper" ){
	window.location = "/chiron.php?dest=<?php echo $_SERVER['REQUEST_URI'] ?>";
  }
</script> 
<?php endif; ?>
<script type="text/javascript">
// <![CDATA[
	var jump_page = '<?php echo ((isset($this->_rootref['LA_JUMP_PAGE'])) ? $this->_rootref['LA_JUMP_PAGE'] : ((isset($this->_rootref['L_JUMP_PAGE'])) ? addslashes($this->_rootref['L_JUMP_PAGE']) : ((isset($user->lang['JUMP_PAGE'])) ? addslashes($user->lang['JUMP_PAGE']) : '{ JUMP_PAGE }'))); ?>:';
	var on_page = '<?php echo (isset($this->_rootref['ON_PAGE'])) ? $this->_rootref['ON_PAGE'] : ''; ?>';
	var per_page = '<?php echo (isset($this->_rootref['PER_PAGE'])) ? $this->_rootref['PER_PAGE'] : ''; ?>';
	var base_url = '<?php echo (isset($this->_rootref['A_BASE_URL'])) ? $this->_rootref['A_BASE_URL'] : ''; ?>';
	var style_cookie = 'phpBBstyle';
	var style_cookie_settings = '<?php echo (isset($this->_rootref['A_COOKIE_SETTINGS'])) ? $this->_rootref['A_COOKIE_SETTINGS'] : ''; ?>';
	var onload_functions = new Array();
	var onunload_functions = new Array();

	<?php if ($this->_rootref['S_USER_PM_POPUP'] && $this->_rootref['S_NEW_PM']) {  ?>

		var url = '<?php echo (isset($this->_rootref['UA_POPUP_PM'])) ? $this->_rootref['UA_POPUP_PM'] : ''; ?>';
		window.open(url.replace(/&amp;/g, '&'), '_phpbbprivmsg', 'height=225,resizable=yes,scrollbars=yes, width=400');
	<?php } ?>


	/**
	* Find a member
	*/
	function find_username(url)
	{
		popup(url, 760, 570, '_usersearch');
		return false;
	}

	/**
	* New function for handling multiple calls to window.onload and window.unload by pentapenguin
	*/
	window.onload = function()
	{
		for (var i = 0; i < onload_functions.length; i++)
		{
			eval(onload_functions[i]);
		}
	}

	window.onunload = function()
	{
		for (var i = 0; i < onunload_functions.length; i++)
		{
			eval(onunload_functions[i]);
		}
	}

// ]]>
</script>



<script type="text/javascript" src="<?php echo (isset($this->_rootref['T_SUPER_TEMPLATE_PATH'])) ? $this->_rootref['T_SUPER_TEMPLATE_PATH'] : ''; ?>/styleswitcher.js"></script>
<script type="text/javascript" src="<?php echo (isset($this->_rootref['T_SUPER_TEMPLATE_PATH'])) ? $this->_rootref['T_SUPER_TEMPLATE_PATH'] : ''; ?>/forum_fn.js"></script>
<link href="<?php echo (isset($this->_rootref['T_THEME_PATH'])) ? $this->_rootref['T_THEME_PATH'] : ''; ?>/print.css" rel="stylesheet" type="text/css" media="print" title="printonly" />
<link href="<?php echo (isset($this->_rootref['T_STYLESHEET_LINK'])) ? $this->_rootref['T_STYLESHEET_LINK'] : ''; ?>" rel="stylesheet" type="text/css" media="screen, projection" />
<link href="<?php echo (isset($this->_rootref['T_THEME_PATH'])) ? $this->_rootref['T_THEME_PATH'] : ''; ?>/normal.css" rel="stylesheet" type="text/css" title="A" />
<link href="<?php echo (isset($this->_rootref['T_THEME_PATH'])) ? $this->_rootref['T_THEME_PATH'] : ''; ?>/medium.css" rel="alternate stylesheet" type="text/css" title="A+" />
<link href="<?php echo (isset($this->_rootref['T_THEME_PATH'])) ? $this->_rootref['T_THEME_PATH'] : ''; ?>/large.css" rel="alternate stylesheet" type="text/css" title="A++" />

<?php if ($this->_rootref['S_CONTENT_DIRECTION'] == ('rtl')) {  ?>
	<link href="<?php echo (isset($this->_rootref['T_THEME_PATH'])) ? $this->_rootref['T_THEME_PATH'] : ''; ?>/bidi.css" rel="stylesheet" type="text/css" media="screen, projection" />
<?php } ?>




<link rel="icon" href="/images/icon_32.png" sizes="32x32">
<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
<link href="/forum/style.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="/jss/jquery-1.8.0.min.js"></script>
<link rel="stylesheet" href="/style/jquery-ui-1.8.23.custom.css" type="text/css" />
<script type="text/javascript" src="/jss/jquery-ui-1.8.23.customDEV.js"></script> 
<script language="javascript" type="text/javascript" src="/_scripts/jQuery.functions.pack.js"></script>
<script language="javascript" type="text/javascript" src="/_scripts/common.js"></script>
<link href="/style/alert.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="/includeajax/user_login.js"></script>
<script type="text/javascript" src="/jss/scripts.js"></script> 

</head>

<body id="phpbb" class="section-<?php echo (isset($this->_rootref['SCRIPT_NAME'])) ? $this->_rootref['SCRIPT_NAME'] : ''; ?> <?php echo (isset($this->_rootref['S_CONTENT_DIRECTION'])) ? $this->_rootref['S_CONTENT_DIRECTION'] : ''; ?>">
<div class="navigation">
  <div class="menu">
        <a href="/index.php" class="logo"></a>
    <ul class="menuUl">
      
            <li class="menuLi">
              <a class="menuA" href="/howitworks.php" >
                <span>How It Works</span>
              </a>
            </li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
            <li class="menuLi">
              <a class="menuA" href="/findcoach.php">
                <span>Find Coach</span>
              </a>
            </li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
            <li class="menuLi">
              <a class="menuA" href="/find_training_partner.php">
                <span>Find Training Partner</span>
        </a>
      </li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi">
        <a class="menuA" href="javascript:;" onclick="getGMTTime();">
          <span>Master Class</span>
        </a>
      </li>
            <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi"><a class="menuA active" href="javascript:;"><span>Forum</span></a></li>
    </ul>
    <ul style="float: right;" class="menuUl">
      <?php if ($_SESSION['user_id'] == ''): ?>
	  
      <li class="menuLi"><a class="menuA" href="javascript:;" onClick="openLogin()"><span>Login</span></a></li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi"><a class="menuA" href="javascript:;" onClick="openRegister()" ><span>Register</span></a></li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi"><a class="menuA" href="javascript:;" onClick="open_lostPwd()"><span>Lost password?</span></a></li>
      <?php else: ?>
	  
	  
	  
      <li class="menuLi"><a class="menuA" href="/profile.php" style="text-decoration:none;color: #FC0;" id="username"><?php echo $_SESSION['username']; ?></a></li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi"><a class="menuA" href="/schedule.php">Dashboard</a> </li>
      <li class="menuLi"><span class="verticalseparatorbar">|</span></li>
      <li class="menuLi usermenuli" style="margin-left: 0">
        <a href="javascript:;" id="arrowmenu" class="menuA downarrow" style="height: 20px"></a>  
        <div class="topsub_menu_wrapper" style="">
          <div class="topsub_menu" style="">
            <ul style="">
              <li><a href="/cashier.php">Cashier (<?php echo $_SESSION['ch_user']['total_points'];  ?>Pts)</a></li>
              <li><a href="/support.php">Support</a></li>
              <li><a href="/contactus.php">Contact</a></li>
              <li style="border: none"><a href="/logout.php">Logout</a></li>
            </ul>
          </div>
        </div>
      </li>
      <?php endif; ?>
        </ul>
    </div>

</div>
<div id="bodywrapper">
<div id="overlay"></div>
<?php if ($_SESSION['user_id'] == ''): ?>
<div class="modalpopup" id="registerwindow">
  
  <a id="closeRegisterBtn" class="modalpopup-close-btn" href="#" style=""></a>
  <div id="registeroverlay" class="overlay" style="position: absolute; z-index: 8"></div>
  <div id="registerSuccesswindow" style="">
    <div>
    You have successfully registered
    </div>
    <a class="button" href="#" onClick="closeRegister(event)" style="margin-left: 110px;margin-top: 20px;">Ok</a>
  </div>
  <div class="">
  
    <div class="title">Registration</div>
        <div class="clear"></div>
    
    <div class="register">
	  <div id="FormErrorMsg" style="color:red; text-align:center; padding-bottom:10px;"> </div>
          <div class="loading" id="regloading" style="margin-left: -30px;margin-top: -50px;display: none;"></div>
      <div class="overlay" id="regoverlay" style="z-index: 999998; position: absolute"></div>
      
            
         <form name="registerForm" id="registerForm" enctype="multipart/form-data" method='post' onsubmit="" action="">
        <fieldset>
            <input type="hidden" name="agreed" value="true" />
            <input type="hidden" name="lang" value="en" />
            <input type="hidden" name="change_lang" value="0" />
            <input type="hidden" name="creation_time" value="{$now}" />
            <input type="hidden" name="main_site" value="/" />

            
            <label>Email:</label>
            <input name="email" type="text" id="register-email" class="con-req" value="" />
            <label>Username:</label>
            <input name="username" id="username" type="text" value="" class="con-req" />
            <p class="clear"></p>
            <label>Password:</label>
            <input name="new_password" type="password" id="password1" class="con-req" value="" />
            <label>Confirm Password:</label>
            <input name="password_confirm" type="password" id="password2" class="con-req" value="" />
            <p class="clear"></p>
            <label>Timezone:</label>
            <select name="tz" id="tz" tabindex="7" class="" style="width: 300px;" >
                <?php 
                    foreach ($common['tz_zones'] as $k => $v):
                ?>
                     <option <?php if ($k == '0'): ?>selected<?php endif; ?> value="<?php echo $k; ?>"><?php echo $v; ?></option>
                <?php endforeach; ?>
            </select>
            
            <p class="clear"></p>

            <label>Captcha :</label>
            <div style="float:left; width:180px;"><!--img src="CaptchaSecurityImages.php?characters=7" /--> <img src="#" id="captcha" />
            <br />   
            
            <a href="javascript:;" onclick="document.getElementById('captcha').src='/captcha.php?'+Math.random();document.getElementById('txtCaptcha').focus();"  id="change-image">Not readable? Change text.</a>
        
        </div>
        
        <label>Security Code:</label>
        <input name="txtCaptcha" id="txtCaptcha" type="text" value="" />
       
        <p class="clear"></p>
        <div   style="margin-top:10px;">&nbsp;</div>
        
        <div style="font-size: 12px;float: right;margin-bottom: 10px;">By clicking Submit, I acknowledge that I have read and agree to our <a href="/termsconditions.php">Terms & Conditions</a> and <a href="/privacypolicy.php">Privacy Policy</a>.</div>
        <!--<label>&nbsp;</label>-->
        <div style="margin-left: 100px;">
          <a style="float: left;" class="button" href="javascript:;" onClick="return CheckFields();">Submit</a>
          <a style="float: left;" class="button" href="javascript:;" onClick="clearFields()">Reset</a>
        </div>
        <input type="hidden" name="submit" value="submit" />
        </fieldset>
      </form>
    </div>
    <div class="clear"></div>
  </div>
</div>
  <div class="modalpopup" id="loginwindow">
    <a class="modalpopup-close-btn" href="javascript:;" style=""></a>
    <div class="login">
      <div class="title">Login</div>
      <div style="width:100%">
      </div>  
      <br />
      <form name="frmLogin" id="frmLogin" method="post" action="" >
          <input type="hidden" name="login" value="Login" />
        <table>
          <tr>
            <td>
              <label for="email">E-Mail</label>
            </td>
            <td>
              <input name="email" type="text" value="Enter Your Email" id="email" onKeyPress="return submitbyenter('frmLogin',this, event)" />
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <div id='FormErrorMsg1' style="color:red;">&nbsp;</div>
            </td>
          </tr>
          <tr>
            <td>
              <label for="password">Password</label>
            </td>
            <td>
              <input name="password" type="password" value="password" id="password" onKeyPress="return submitbyenter('frmLogin',this, event)" />
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <div id='FormErrorMsg2' style="color:red;">&nbsp;</div>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <a class="button" href="javascript:;" style="margin-top: 10px" onclick="return submit_frm();">Login</a>
            </td>
          </tr>
          
        </table>
      </form>
      <div class="clear"></div>
      <p><a href="javascript:;" onclick="closeOpen('loginwindow','registerwindow',event)">Register</a> or <a href="#" onClick="closeOpen('loginwindow','lostPwd',event)">Lost password?</a></p>
    </div>
  </div>
    
  <div class="modalpopup" id="lostPwd" >
    <a class="modalpopup-close-btn" href="#" style=""></a>
    <div class="login">
      <div class="title">Lost you password?</div>
      <div style="width:100%">
        <div id='ErrorMsg' style="color:red; text-align:left; float:left; width:45%">&nbsp;</div>
      </div>  
      <br />
      <form name="frmLostpwd" id="frmLostpwd" method="post" action="" >
        <table>
          <tr>
            <td>
              <label for="email_id">E-Mail</label>
            </td>
            <td>
              <input name="email_id" type="text" value="" id="email_id" onKeyPress="return submitenter('frmLostpwd',this, event)" />
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <a class="button" href="javascript:;" style="margin-top: 20px" onclick="return Get_pwd();">Submit</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div class="clear"></div>
        <p style="float: right;"><a onClick="closeOpen('lostPwd','registerwindow',event)">Register</a> or <a href="#" onClick="closeOpen('lostPwd','loginwindow',event)">Login</a></p>
  </div> 

  
<?php endif; ?>


<script>
    var $ = jQuery.noConflict();
    $(".modalpopup").draggable();
</script>
    <div class="main ">

        

        <div class="navbar rounded">
            <div class="inner">
                

                <ul class="linklist navlinks">
                    <li class="icon-home">
                        <a href="<?php echo (isset($this->_rootref['U_INDEX'])) ? $this->_rootref['U_INDEX'] : ''; ?>" accesskey="h"><?php echo ((isset($this->_rootref['L_INDEX'])) ? $this->_rootref['L_INDEX'] : ((isset($user->lang['INDEX'])) ? $user->lang['INDEX'] : '{ INDEX }')); ?></a> <?php $_navlinks_count = (isset($this->_tpldata['navlinks'])) ? sizeof($this->_tpldata['navlinks']) : 0;if ($_navlinks_count) {for ($_navlinks_i = 0; $_navlinks_i < $_navlinks_count; ++$_navlinks_i){$_navlinks_val = &$this->_tpldata['navlinks'][$_navlinks_i]; ?> <strong>&#8249;</strong> <a href="<?php echo $_navlinks_val['U_VIEW_FORUM']; ?>"><?php echo $_navlinks_val['FORUM_NAME']; ?></a><?php }} ?>
                    </li>

                    <li class="rightside">
                        <div id="search-box">
                        <form action="<?php echo (isset($this->_rootref['U_SEARCH'])) ? $this->_rootref['U_SEARCH'] : ''; ?>" method="get" id="search">
                        <fieldset>
                            <input name="keywords" id="keywords" type="text" maxlength="128" title="<?php echo ((isset($this->_rootref['L_SEARCH_KEYWORDS'])) ? $this->_rootref['L_SEARCH_KEYWORDS'] : ((isset($user->lang['SEARCH_KEYWORDS'])) ? $user->lang['SEARCH_KEYWORDS'] : '{ SEARCH_KEYWORDS }')); ?>" class="inputbox search" value="<?php if ($this->_rootref['SEARCH_WORDS']) {  echo (isset($this->_rootref['SEARCH_WORDS'])) ? $this->_rootref['SEARCH_WORDS'] : ''; } else { echo ((isset($this->_rootref['L_SEARCH_MINI'])) ? $this->_rootref['L_SEARCH_MINI'] : ((isset($user->lang['SEARCH_MINI'])) ? $user->lang['SEARCH_MINI'] : '{ SEARCH_MINI }')); } ?>" onclick="if(this.value=='<?php echo ((isset($this->_rootref['LA_SEARCH_MINI'])) ? $this->_rootref['LA_SEARCH_MINI'] : ((isset($this->_rootref['L_SEARCH_MINI'])) ? addslashes($this->_rootref['L_SEARCH_MINI']) : ((isset($user->lang['SEARCH_MINI'])) ? addslashes($user->lang['SEARCH_MINI']) : '{ SEARCH_MINI }'))); ?>')this.value='';" onblur="if(this.value=='')this.value='<?php echo ((isset($this->_rootref['LA_SEARCH_MINI'])) ? $this->_rootref['LA_SEARCH_MINI'] : ((isset($this->_rootref['L_SEARCH_MINI'])) ? addslashes($this->_rootref['L_SEARCH_MINI']) : ((isset($user->lang['SEARCH_MINI'])) ? addslashes($user->lang['SEARCH_MINI']) : '{ SEARCH_MINI }'))); ?>';" />
                            <input class="button" value="<?php echo ((isset($this->_rootref['L_SEARCH'])) ? $this->_rootref['L_SEARCH'] : ((isset($user->lang['SEARCH'])) ? $user->lang['SEARCH'] : '{ SEARCH }')); ?>" type="submit" /><br />
                            <!--a href="<?php echo (isset($this->_rootref['U_SEARCH'])) ? $this->_rootref['U_SEARCH'] : ''; ?>" title="<?php echo ((isset($this->_rootref['L_SEARCH_ADV_EXPLAIN'])) ? $this->_rootref['L_SEARCH_ADV_EXPLAIN'] : ((isset($user->lang['SEARCH_ADV_EXPLAIN'])) ? $user->lang['SEARCH_ADV_EXPLAIN'] : '{ SEARCH_ADV_EXPLAIN }')); ?>"><?php echo ((isset($this->_rootref['L_SEARCH_ADV'])) ? $this->_rootref['L_SEARCH_ADV'] : ((isset($user->lang['SEARCH_ADV'])) ? $user->lang['SEARCH_ADV'] : '{ SEARCH_ADV }')); ?></a--> <?php echo (isset($this->_rootref['S_SEARCH_HIDDEN_FIELDS'])) ? $this->_rootref['S_SEARCH_HIDDEN_FIELDS'] : ''; ?>

                        </fieldset>
                        </form>
                        </div>
                   </li>

                    <?php if ($this->_rootref['U_EMAIL_TOPIC']) {  ?><li class="rightside"><a href="<?php echo (isset($this->_rootref['U_EMAIL_TOPIC'])) ? $this->_rootref['U_EMAIL_TOPIC'] : ''; ?>" title="<?php echo ((isset($this->_rootref['L_EMAIL_TOPIC'])) ? $this->_rootref['L_EMAIL_TOPIC'] : ((isset($user->lang['EMAIL_TOPIC'])) ? $user->lang['EMAIL_TOPIC'] : '{ EMAIL_TOPIC }')); ?>" class="sendemail"><?php echo ((isset($this->_rootref['L_EMAIL_TOPIC'])) ? $this->_rootref['L_EMAIL_TOPIC'] : ((isset($user->lang['EMAIL_TOPIC'])) ? $user->lang['EMAIL_TOPIC'] : '{ EMAIL_TOPIC }')); ?></a></li><?php } if ($this->_rootref['U_EMAIL_PM']) {  ?><li class="rightside"><a href="<?php echo (isset($this->_rootref['U_EMAIL_PM'])) ? $this->_rootref['U_EMAIL_PM'] : ''; ?>" title="<?php echo ((isset($this->_rootref['L_EMAIL_PM'])) ? $this->_rootref['L_EMAIL_PM'] : ((isset($user->lang['EMAIL_PM'])) ? $user->lang['EMAIL_PM'] : '{ EMAIL_PM }')); ?>" class="sendemail"><?php echo ((isset($this->_rootref['L_EMAIL_PM'])) ? $this->_rootref['L_EMAIL_PM'] : ((isset($user->lang['EMAIL_PM'])) ? $user->lang['EMAIL_PM'] : '{ EMAIL_PM }')); ?></a></li><?php } if ($this->_rootref['U_PRINT_TOPIC']) {  ?><li class="rightside"><a href="<?php echo (isset($this->_rootref['U_PRINT_TOPIC'])) ? $this->_rootref['U_PRINT_TOPIC'] : ''; ?>" title="<?php echo ((isset($this->_rootref['L_PRINT_TOPIC'])) ? $this->_rootref['L_PRINT_TOPIC'] : ((isset($user->lang['PRINT_TOPIC'])) ? $user->lang['PRINT_TOPIC'] : '{ PRINT_TOPIC }')); ?>" accesskey="p" class="print"><?php echo ((isset($this->_rootref['L_PRINT_TOPIC'])) ? $this->_rootref['L_PRINT_TOPIC'] : ((isset($user->lang['PRINT_TOPIC'])) ? $user->lang['PRINT_TOPIC'] : '{ PRINT_TOPIC }')); ?></a></li><?php } if ($this->_rootref['U_PRINT_PM']) {  ?><li class="rightside"><a href="<?php echo (isset($this->_rootref['U_PRINT_PM'])) ? $this->_rootref['U_PRINT_PM'] : ''; ?>" title="<?php echo ((isset($this->_rootref['L_PRINT_PM'])) ? $this->_rootref['L_PRINT_PM'] : ((isset($user->lang['PRINT_PM'])) ? $user->lang['PRINT_PM'] : '{ PRINT_PM }')); ?>" accesskey="p" class="print"><?php echo ((isset($this->_rootref['L_PRINT_PM'])) ? $this->_rootref['L_PRINT_PM'] : ((isset($user->lang['PRINT_PM'])) ? $user->lang['PRINT_PM'] : '{ PRINT_PM }')); ?></a></li><?php } ?>

                </ul>

                <?php if (! $this->_rootref['S_IS_BOT'] && $this->_rootref['S_USER_LOGGED_IN']) {  ?>

                <ul class="linklist leftside">
                    <li class="icon-ucp">
                        <a href="<?php echo (isset($this->_rootref['U_PROFILE'])) ? $this->_rootref['U_PROFILE'] : ''; ?>" title="<?php echo ((isset($this->_rootref['L_PROFILE'])) ? $this->_rootref['L_PROFILE'] : ((isset($user->lang['PROFILE'])) ? $user->lang['PROFILE'] : '{ PROFILE }')); ?>" accesskey="e"><?php echo ((isset($this->_rootref['L_PROFILE'])) ? $this->_rootref['L_PROFILE'] : ((isset($user->lang['PROFILE'])) ? $user->lang['PROFILE'] : '{ PROFILE }')); ?></a>
                                <?php if ($this->_rootref['S_DISPLAY_PM']) {  ?> (<a href="<?php echo (isset($this->_rootref['U_PRIVATEMSGS'])) ? $this->_rootref['U_PRIVATEMSGS'] : ''; ?>"><?php echo (isset($this->_rootref['PRIVATE_MESSAGE_INFO'])) ? $this->_rootref['PRIVATE_MESSAGE_INFO'] : ''; ?></a>)<?php } if ($this->_rootref['S_DISPLAY_SEARCH']) {  ?> &bull;
                        <a href="<?php echo (isset($this->_rootref['U_SEARCH_SELF'])) ? $this->_rootref['U_SEARCH_SELF'] : ''; ?>"><?php echo ((isset($this->_rootref['L_SEARCH_SELF'])) ? $this->_rootref['L_SEARCH_SELF'] : ((isset($user->lang['SEARCH_SELF'])) ? $user->lang['SEARCH_SELF'] : '{ SEARCH_SELF }')); ?></a>
                        <?php } if ($this->_rootref['U_RESTORE_PERMISSIONS']) {  ?> &bull;
                        <a href="<?php echo (isset($this->_rootref['U_RESTORE_PERMISSIONS'])) ? $this->_rootref['U_RESTORE_PERMISSIONS'] : ''; ?>"><?php echo ((isset($this->_rootref['L_RESTORE_PERMISSIONS'])) ? $this->_rootref['L_RESTORE_PERMISSIONS'] : ((isset($user->lang['RESTORE_PERMISSIONS'])) ? $user->lang['RESTORE_PERMISSIONS'] : '{ RESTORE_PERMISSIONS }')); ?></a>
                        <?php } ?>
                    </li>
                </ul>
                <?php } ?>

                <span class="corners-bottom"><span></span></span>
            </div>
        </div>
    </div>

	<a name="start_here"></a>
	<div class="main" style="margin-top: 5px;">
	<div id="page-body" class="container">
            <?php if ($this->_rootref['S_BOARD_DISABLED'] && $this->_rootref['S_USER_LOGGED_IN'] && ( $this->_rootref['U_MCP'] || $this->_rootref['U_ACP'] )) {  ?>

            <div id="information" class="rules">
                <div class="inner">
                    <span class="corners-top"><span></span></span>
                    <strong><?php echo ((isset($this->_rootref['L_INFORMATION'])) ? $this->_rootref['L_INFORMATION'] : ((isset($user->lang['INFORMATION'])) ? $user->lang['INFORMATION'] : '{ INFORMATION }')); ?>:</strong> <?php echo ((isset($this->_rootref['L_BOARD_DISABLED'])) ? $this->_rootref['L_BOARD_DISABLED'] : ((isset($user->lang['BOARD_DISABLED'])) ? $user->lang['BOARD_DISABLED'] : '{ BOARD_DISABLED }')); ?>

                    <span class="corners-bottom"><span></span></span>
                </div>
            </div>
		<?php } ?>

<script language="javascript">
       changePageTitle('Forum');
</script>