function ChangeFlow(topage)
{
	document.ShoppingCart.ToPageStatus.value=topage;	
	document.ShoppingCart.submit();

}
function ReCalculateOnCheckOut()
{
	ChangeFlow('check');
	//window.location.href='checkout.php';
}

/* Shipping Address Is Same As Billing Address Starts */
function SameAsAboveClick(sel)
{		
	if (sel.checked) {CheckShipAdd();} else {CheckShipDel();}	
}

function CheckShipAdd()
{		
	document.getElementById("ship_first_name").value=document.getElementById("bill_first_name").value;
	document.getElementById("ship_surname").value=document.getElementById("bill_surname").value;
	document.getElementById("ship_address").value=document.getElementById("bill_address").value;
	document.getElementById("ship_city").value=document.getElementById("bill_city").value;
	document.getElementById("ship_state").value=document.getElementById("bill_state").value;
	document.getElementById("ship_post_code").value=document.getElementById("bill_post_code").value;
	document.getElementById("ship_country").value=document.getElementById("bill_country").value;
	document.getElementById("ship_phone").value=document.getElementById("bill_phone").value;				
}
function CheckBillDel()
{	
	document.getElementById("bill_first_name").value="";
	document.getElementById("bill_surname").value="";
	document.getElementById("bill_address").value="";
	document.getElementById("bill_city").value="";
	document.getElementById("bill_state").value="";
	document.getElementById("bill_post_code").value="";
	document.getElementById("bill_country").value="";
	document.getElementById("bill_phone").value="";		
	
}
function CheckShipDel()
{	
	document.getElementById("ship_first_name").value="";
	document.getElementById("ship_surname").value="";
	document.getElementById("ship_address").value="";
	document.getElementById("ship_city").value="";
	document.getElementById("ship_state").value="";
	document.getElementById("ship_post_code").value="";
	document.getElementById("ship_country").value="";
	document.getElementById("ship_phone").value="";		
}
function ClearAll()
{	
	CheckBillDel();
	window.document.getElementById("is_same_as_bill").checked=false;
	CheckShipDel();
}

function Uncheck()
{
	
	window.document.getElementById("is_same_as_bill").checked=false;	
}


/* Shipping Address Is Same As Billing Address Starts */

function ShowProductImageLarge(image_path)
{	
	$('ImageLarge').src=image_path;	
}

function removetxt()
{
	if($('SearchText').value=="Search by keywords")
	{
	 	$('SearchText').value="";
	}
}

function puttxt()
{
	if($('SearchText').value=="")
	{
	 	$('SearchText').value="Search by keywords";
	}
}

function popup(mylink, windowname)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
   href=mylink;
else
   href=mylink.href;
   //window.resizeTo()
window.open(href, windowname, 'width=500,height=350,scrollbars=yes');
return false;
}

function resize(win,width,height)
{
if (parseInt(navigator.appVersion)>3)
  win.resizeTo(width,height+125);
}


/* Check numbersonly starts */
/* Note: Use In Control Attribute as onKeyPress="return numbersonly(this, event)" */
function numbersonly(myfield, e, dec)
{
	var key;
	var keychar;
	if (window.event)
	key = window.event.keyCode;
	else if (e)
	key = e.which;
	else
	return true;
	keychar = String.fromCharCode(key);	
	// control keys
	if ((key==null) || (key==0) || (key==8) || 
	(key==9) || (key==13) || (key==27))
	return true;
	// numbers
	else if ((("0123456789").indexOf(keychar) > -1))
	return true;
	// decimal point jump
	else if (dec && (keychar == "."))
	{
	myfield.form.elements[dec].focus();
	return false;
	}
	else
	return false;
}
/* Check numbersonly ends */
/* Check floatnumbersonly starts */
/* Note: Use In Control Attribute as onKeyPress="return floatnumbersonly(this, event)" */
function floatnumbersonly(myfield, e, dec)
{
	var key;
	var keychar;
	if (window.event)
	key = window.event.keyCode;
	else if (e)
	key = e.which;
	else
	return true;
	keychar = String.fromCharCode(key);
	// control keys
	if ((key==null) || (key==0) || (key==8) || 
	(key==9) || (key==13) || (key==27)|| (key==46))
	return true;
	// numbers
	else if ((("0123456789").indexOf(keychar) > -1))
	return true;
	// decimal point jump
	else if (dec && (keychar == "."))
	{
	myfield.form.elements[dec].focus();
	return false;
	}
	else
	return false;
}
/* Check floatnumbersonly ends */