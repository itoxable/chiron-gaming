// JavaScript Document
function CheckEmpty(ErrField,myID,FieldName)
{
	 
	var myVar=document.getElementById(myID).value;
	if(myVar=='Enter Your Email' || myVar=='password')
	myVar = '';
	
	if(myVar=="")
	{
		document.getElementById(ErrField).innerHTML = "Please enter <b>"+FieldName+"</b>";
		return 0;
	}else{
		document.getElementById(ErrField).innerHTML='&nbsp;';
		return 1;
	}
}
function GetLogin(formID){

	var j = jQuery.noConflict();
        
	var email = j('#email').val();
	var password = j('#password').val();
	var retval1 = CheckEmpty('FormErrorMsg1','email','Email');
	var retval2 = CheckEmpty('FormErrorMsg2','password','Password');

	
	var frmID='#'+formID;
	var params ={
            'module': 'contact',
            'action': 'sendContactData'
        };
        var paramsObj = j(frmID).serializeArray();
        j.each(paramsObj, function(i, field){
        params[field.name] = field.value;
        });
        if(retval1 && retval2){
            showLoading('#loginoverlay', '#loginloading');
            j.ajax({
                type: "POST",
                url: '/processlogin.php',
                data: params,
                dataType: 'json',
		success: function(data){
                    
                    if(!data){
                        hideLoading('#loginoverlay', '#loginloading');
                        j('#FormErrorMsg1').html('Authentication failed');
                        return;
                    }
                        
                    if (data.flag == 1){
                        
                        
                       var logindestiny = data.logindestiny;
                       
                       jQuery.post("/forum/ucp.php?mode=login", {"login":"Login","username":data.username,"password":password},function(data){
                           if(data.IsSuccess){
                               if(logindestiny && logindestiny != null && logindestiny != ""){
                                   window.location.href = logindestiny; 
                               }
                               else{
                                   location.reload();
//                                   window.location.href="/schedule.php"; 
                               }
                                   
                           }
                                
                       },"json");
                    }else{
                        hideLoading('#loginoverlay', '#loginloading');
			j('#FormErrorMsg1').html('Authentication failed');
                    }

		}

		
            });
	}
	
}

function GetPwd(formID,callback){
    
    showLoading('#forgotloading', '#forgotoverlay');
    var j = jQuery.noConflict(); 
    var email = document.getElementById('email_id').value;
    if(email=='Enter Your Email')
        email = '';
    if(email=='')
	document.getElementById('ErrorMsg').innerHTML = "Please enter <b>Email</b>";
    var frmID='#'+formID;
    var params ={
       'action': 'sendLostPwd'
     };
		 
    var paramsObj = j(frmID).serializeArray();
    j.each(paramsObj, function(i, field){

				params[field.name] = field.value;

		   });
		  if(email!='')
		  {
			  j.ajax({
					type: "POST",
					url:  '/lostpassword.php',
					data: params,
					success: function(data){
                                            hideLoading('#forgotloading', '#forgotoverlay');
						if(data == 1){
							j('#login').css('display','block');
							j('#lostPwd').css('display','none');
							j('#email_id').val();
							j('#FormErrorMsg1').html('Please check your email');
							if(callback)
								callback();
                                                        showWarning("We have sent your password to your email");
						}else if(data == "-1"){
                                                    j('#ErrorMsg').html('An error prevented us to proccess your request, please try again later'); 
                                                }
						else{
							j('#ErrorMsg').html('Wrong email address');  
						}

					}

		});
		}
	
}