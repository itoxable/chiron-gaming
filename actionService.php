<?php
include_once "general_include.php";
include_once("adodb/dbconfig.php");
//include_once "datafeed.php";

if(!isset($_SESSION['user_id'])){
	echo "session not started";
	return;
}

class WithdrawStatus{
    public static $OPEN = 'OPEN';
	public static $PENDING = 'PENDING';
	public static $COMPLETED = 'COMPLETED';
	public static $DECLINED = 'DECLINED';
	public static $REQUESTED = 'REQUESTED';
}
class NotificationTypes{
	public static $GENERAL= 0;
	public static $SCHEDULE_ALERT = 1;
	public static $UNKNOWN = 2;
	public static $INSTANT_LESSON = 3;
	public static $MESSAGE = 4;
}
function getPostOrGetParam($param){
	if(isset($_POST[$param]))
		return $_POST[$param];
	else{
		if(isset($_GET[$param]))
			return $_GET[$param];
		else
			return "";
	}
}

function insertNotifications($userId, $type=1, $origin="" , $title="", $introduction="", $text="", $date="",$link="javascript:;"){ 
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();
		$sql = "INSERT INTO `nk_notification` (`user_id`,`nofication_type`";
		if($origin != "")
			$sql = $sql.",`origin`";
		if($title != "")
			$sql = $sql.",`title`";
		if($introduction != "")
			$sql = $sql.",`introduction`";
		if($text != "")
			$sql = $sql.",`text`";
		if($date != "")
			$sql = $sql.",`date_value`";
		
		$sql = $sql.",`link`,`modified_date`,`creation_date`) VALUES (".$userId.",".$type;

		if($origin != "")
			$sql = $sql.",'".$origin."'";
		if($title != "")
			$sql = $sql.",'".$title."'";
		if($introduction != "")
			$sql = $sql.",'".$introduction."'";
		if($text != "")
			$sql = $sql.",'".$text."'";
		if($date != "")
			$sql = $sql.",'".php2MySqlTime(js2PhpTime($date))."'";
			
		$sql = $sql.",'".$link."',SYSDATE(), SYSDATE());";

		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = 'add success';
			$ret['Data'] = $date;
			$ret['Id'] = mysql_insert_id();
		}

	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}

	return $ret;
}

function addAsFriend($user){
	$ret = array();
	try{
            
                
		$db = new DBConnection();
		$db->getConnection();
                
//                $sqlCheck =  "SELECT * FROM nk_user_friends_list WHERE friend_status = 2 AND ((nk_user_id_owner='".$_SESSION['user_id']."' or nk_user_id_friend='".$_SESSION['user_id']."') and (nk_user_id_owner='".$user."' or nk_user_id_friend='".$user."'))";
//                $handle = mysql_query($sqlCheck);
                
                
		$sql = "INSERT INTO `nk_user_friends_list` (`nk_user_id_owner`,`nk_user_id_friend`,`friend_status`, `cdate`) VALUES('".$_SESSION['user_id']."','".$user."', 2, SYSDATE());";
		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = 'add success';
			$ret['Id'] = mysql_insert_id();
		}

	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}

	return $ret;
}

function removeFromFriendList($user){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();
	
		$sql = "DELETE FROM `nk_user_friends_list` WHERE (nk_user_id_owner='".$_SESSION['user_id']."' or nk_user_id_friend='".$_SESSION['user_id']."') and (nk_user_id_owner='".$user."' or nk_user_id_friend='".$user."')";
		
		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = 'add success';
		}

	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}

	return $ret;
}


function changeOnlineStatus($status){
	$ret = array();
	try{
		$sql = "UPDATE ".TABLEPREFIX."_user SET online_status = ".$status." WHERE user_id = ".$_SESSION['user_id'];
		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = 'updated';
			$ret['Data'] = $status;
		}
		
	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}

	return $ret;
}

function addPhonoSessionId($sessionId){
	$ret = array();
	try{
		$sql = "UPDATE ".TABLEPREFIX."_user SET phono_session = '".$sessionId."' WHERE user_id = ".$_SESSION['user_id'];
		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] ="Error: ".mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = $_SESSION['user_id'].'==>'.$sessionId;
		}
	
	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
	//print_r($ret);
	return $ret;
}

function updateUserPoints($points){
    $ret = array();
	try{
		$sql = "UPDATE ".TABLEPREFIX."_user SET total_points = '".$points."' WHERE user_id = ".$_SESSION['user_id'];
		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] ="Error: ".mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = "";
		}
	
                $_SESSION['ch_user'] = getUser($_SESSION['user_id']);
                
                
	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
	return $ret;
}

function getUser($userId){
	$ret = array();
	try{
		$sql = "SELECT * FROM ".TABLEPREFIX."_user WHERE user_id = ".$userId;
		$handle = mysql_query($sql);
		return mysql_fetch_array($handle);
	
	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
	return $ret;
}

function validateWithdraw($key, $quant, $txtCaptcha){
    
    $code = trim($txtCaptcha);
    if($code != $_SESSION['captcha']){
        $ret['message'] = 'Enter security code correctly';
        $ret['IsSuccess'] = false;
        return $ret;    
    }
    
    $ret = array();
    $ret['IsSuccess'] = true;
    $user = getUser($_SESSION['user_id']);
    $password = $user['password'];
    $password=md5($password);
    $totalPoints = $user['total_points'];

    if($quant > $totalPoints){
        $ret['message'] = 'You dont have enough points';
        $ret['IsSuccess'] = false;
        return $ret;
    }
    if($key != $password){
        $ret['message'] = 'Wrong password';
        $ret['IsSuccess'] = false;
        return $ret;
    }
    $ret['totalPoints'] = $totalPoints;
    return $ret;
}

function doWithdraw($quant, $paypalId, $points){

    $ret = array();
    try{
        include('transaction_status.class.php');
		$db = new DBConnection();
		$db->getConnection();
        
        $txDate = date("d/m/Y : H:i:s", time());
        $txId = md5($_SESSION['user_id'].$txDate);
	
        $sql = "INSERT INTO nk_user_transaction(nk_user_id, tx_date, rate_point_id, status, nk_tx_id,transaction_type,method,method_detail,qty_points, description) VALUES(". $_SESSION['user_id'] .", STR_TO_DATE('". $txDate ."', '%d/%m/%Y : %H:%i:%s'),-1, ". TransactionStatus::WAITING_CONF .",'". $txId ."',0,'Paypal', '".$paypalId."', ".$quant.",'Points Withdraw')";
        
		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$ret['Msg'] = 'add success';
			$ret['Id'] = mysql_insert_id();
						
            $updateResult = updateUserPoints($points-$quant);
			
			return $updateResult;
		}

	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
	return $ret;
}

 
 function getMessages(){
	$ret = array();
	$ids = "";
	$idsArray = array();
	$groupedMessages = array();
	
	try{
		$db = new DBConnection();
		$db->getConnection();
		$sql = "SELECT * FROM `frei_chat` WHERE `to` = ".$_SESSION['user_id']." OR `from` = ".$_SESSION['user_id']." ORDER BY `sent` DESC";
	
		$handle = mysql_query($sql);
		while ($message = mysql_fetch_object($handle)) {
			$to = $message->to;
			$from = $message->from;
			
			if($to != $_SESSION['user_id']){
				if(!in_array($to, $idsArray)){
					$idsArray[]=$to;
				}
				$groupedMessages[$to][] = $message;
			}else if($from != $_SESSION['user_id']){
				if(!in_array($from, $idsArray)){
					$idsArray[]=$from;
				}
				$groupedMessages[$from][] = $message;
			}	
		}
		
		$ret['groupedMessages'] = $groupedMessages;
		$idsSize = count($idsArray);
		for($i=0; $i<$idsSize; $i++){
			$id = $idsArray[$i];
			if($i == ($idsSize-1))
				$ids = $ids.$id."";
			else
				$ids = $ids.$id.",";
		}
		
		$userssql = "SELECT * FROM `nk_user` WHERE `user_id` in (".$ids.") ORDER BY FIELD(`user_id`,".$ids.")";
		//echo "\n".$userssql."\n";
		$userhandle = mysql_query($userssql);
		while ($user = mysql_fetch_object($userhandle)) {
			//echo "\n".'AA'."\n";
			$ret['users'][] = $user;
		}
		$ret['IsSuccess'] = true;
	}catch(Exception $e){
		$ret['error'] = $e->getMessage();
	}
	return $ret;
}
 
function bigintval($value) {
	$value = trim($value);
	if (ctype_digit($value)) {
		return $value;
	}
	$value = preg_replace("/[^0-9](.*)$/", '', $value);
	if (ctype_digit($value)) {
		return $value;
	}
	return 0;
}
function sendMessage($message, $toid, $fromname, $gmttime, $toname=""){

	$ret = array();
    try{
		if($toname == ""){
			$user = getUser($toid);
			$toname = $user['username'];
		}
		$gmttime = bigintval($gmttime);
		$db = new DBConnection();
		$db->getConnection();
		$time = time() . str_replace(" ", "", microtime());
		
		$sql = "INSERT INTO frei_chat (`from`, `from_name`, `to`, `to_name`, `message`, `sent`, `recd`, `time`, `GMT_time`, `message_type`, `room_id`) VALUES (".$_SESSION['user_id'].", '".$fromname."', ".$toid.", '".$toname."', '".nl2br($message)."',NOW(),1, ".$time.", ".$gmttime.", 0, -1)";
		if(mysql_query($sql)==false){
			$ret['IsSuccess'] = false;
			$ret['Msg'] = mysql_error();
		}else{
			$ret['IsSuccess'] = true;
			$sql = "SELECT * FROM `frei_chat` where `id` = ".mysql_insert_id();
			$handle = mysql_query($sql);
			$msg = mysql_fetch_object($handle);
			
			$ret['msg'] = $msg;
			//$ret['sent'] = mysql_insert_id();
			insertNotifications($toid, NotificationTypes::$MESSAGE,$_SESSION['user_id'] , 'New Message', ($fromname." sent you a new message"), ($fromname." sent you a new message"), "", "/messages.php");
		}
	
	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
	
	return $ret;
}

function pointsTransfer($id,$quant){
    $ret = array();
	$user = getUser($id);	
	$ret['status'] = getUserStatus($user);
    return $ret;
}

if(isset($_GET['method']) || isset($_POST['method'])){
	header('Content-type:text/javascript;charset=UTF-8');

	$method = getPostOrGetParam("method");

	switch ($method) {
		case "addAsFriend":
			$ret = addAsFriend(getPostOrGetParam('userid'));	
			break;
		case "changeOnlineStatus":
			$ret = changeOnlineStatus(getPostOrGetParam('status'));	
			break;
		case "addPhonoSessionId":
			$ret = addPhonoSessionId(getPostOrGetParam('sessionId'));
			break;
		case "getUser":
			$ret = getUser(getPostOrGetParam('userId'));
			break;
		case "removeFriend":
			$ret = removeFromFriendList(getPostOrGetParam('userid'));
			break;
        case "validateWithdraw":
            
    		$ret = validateWithdraw(getPostOrGetParam('key'), getPostOrGetParam('quant'), getPostOrGetParam('txtCaptcha'));
			break;
        case "doWithdraw":
        	$ret = doWithdraw(getPostOrGetParam('quant'), getPostOrGetParam('paypalId'), getPostOrGetParam('totalpoints'));
			break;  
		case "doTest":
        	$ret = doTest(getPostOrGetParam('id'));
			break;  
        case "sendMessage":
        	$ret = sendMessage(getPostOrGetParam('message'),getPostOrGetParam('toid'),getPostOrGetParam('fromname'),getPostOrGetParam('gmttime'),getPostOrGetParam('toname'));
			break;    
        case "getMessages":
        	$ret = getMessages();
			break;   
		case "pointsTransfer":
        	$ret = pointsTransfer(getPostOrGetParam('userid'),getPostOrGetParam('quant'));
			break;  
		case "getTime":
			$ret = array();
			$ret['DATE'] = date('l jS \of F Y h:i:s A');
			break;
	}
	echo json_encode($ret);
}
?>
