<?php
include("general_include.php");

$cms_id=1;
include "top.php";

function findvalue($table,$fld_name,$fld_value,$find_value){
	if($fld_value=='')
		return false;
	$SelectTypeSql = "SELECT ".$find_value." FROM ".$table." WHERE $fld_name=".$fld_value;
	$SelectType = mysql_fetch_array(mysql_query($SelectTypeSql));
	return $SelectType[0];
}	

$paypalid = "";
$paypalurl = "";
$facebook = "";
$twitter = "";
$SqlSettings = "SELECT * FROM ".TABLEPREFIX."_settings";
$settingsArr = $UserManagerObjAjax->GetRecords("All",$SqlSettings);
foreach($settingsArr as $setting){
    if($setting['id'] == PAYPAL_ID){
        $paypalid = $setting['value'];
    }else if($setting['id'] == PAYPAL_URL){
        $paypalurl = $setting['value'];
    }else if($setting['id'] == FACEBOOK_URL){
        $facebook = $setting['value'];
    }else if($setting['id'] == TWITTER_ID){
        $twitter = $setting['value'];
    }
}


$latestvdoSql = "SELECT * FROM ".TABLEPREFIX."_video WHERE is_featured='Y' ORDER BY date_added DESC LIMIT 0,6";
$latestvdoArr = $UserManagerObjAjax->GetRecords('All',$latestvdoSql);
$NumVideo = count($latestvdoArr);
for($v=0;$v<$NumVideo;$v++)
{
	$latestvdoArr[$v]['like']='N';
    $dateSql = "SELECT date_format('".$latestvdoArr[$v]['date_added']."','%M  %d , %Y') as video_date";
	$DateArr = $UserManagerObjAjax->GetRecords("Row",$dateSql);
    $latestvdoArr[$v]['video_date']=$DateArr['video_date'];
	
	$selectNumreview = "SELECT COUNT(*) FROM ".TABLEPREFIX."_video_review WHERE video_id='".$latestvdoArr[$v]['video_id']."' AND is_active='Y'";
	$totReview = $UserManagerObjAjax->GetRecords("Row",$selectNumreview);
	$latestvdoArr[$v]['video_review'] = $totReview[0];
	
	$latestvdoArr[$v]['user'] = findvalue(TABLEPREFIX."_user","user_id",$latestvdoArr[$v]['user_id'],"name");
	$totstar='';
	for($x=1;$x<=$latestvdoArr[$v]['overall_rating'];$x++)
	{
	  $totstar.="<img src='images/starb.gif' alt='' border='0' />";
	}
	$istar=5 - $latestvdoArr[$v]['overall_rating']; 
	for($y=1;$y<=$istar;$y++)
	{
	  $totstar.="<img src='images/starg.gif' alt='' border='0' />";
	}
	$latestvdoArr[$v]['star']=$totstar;
	$viewCount = "SELECT count(*) FROM ".TABLEPREFIX."_video_view_count WHERE video_id =".$latestvdoArr[$v]['video_id'];
	$viewArr = $UserManagerObjAjax->GetRecords("Row",$viewCount);
	$latestvdoArr[$v]['view_count'] = $viewArr[0];
	
	$LikeSql = "SELECT count(*) AS numlike FROM ".TABLEPREFIX."_user_like WHERE selected_for='L' AND video_id=".$latestvdoArr[$v]['video_id'] ;
	$LikeArr = $UserManagerObjAjax->GetRecords('Row',$LikeSql);
	
	$UnlikeSql = "SELECT count(*) AS numunlike FROM ".TABLEPREFIX."_user_like WHERE selected_for='D' AND video_id=".$latestvdoArr[$v]['video_id'] ;
	$UnlikeArr = $UserManagerObjAjax->GetRecords('Row',$UnlikeSql);
	
	$latestvdoArr[$v]['numlike']=$LikeArr['numlike'];
	$latestvdoArr[$v]['numunlike']=$UnlikeArr['numunlike'];
}

$homenevoSql = "SELECT * FROM ".TABLEPREFIX."_home_image WHERE is_active='Y'";
$homenevoArr = $UserManagerObjAjax->GetRecords('All',$homenevoSql);

$featuredCoachesSql = "SELECT * FROM ".TABLEPREFIX."_user_type_user_relation as rl left join ".TABLEPREFIX."_user as gt on rl.user_id = gt.user_id where user_type_id = 1 and is_featured='Y' limit 0,10";
$featuredCoachesArr = $UserManagerObjAjax->GetRecords('All',$featuredCoachesSql);
$NumFeature = count($featuredCoachesArr);

$ratingCoachesSql = "SELECT * FROM ".TABLEPREFIX."_user u, ".TABLEPREFIX."_user_type_user_relation ut WHERE ut.user_type_id =1 AND ut.overall_rating IS NOT NULL AND ut.user_id = u.user_id ORDER BY overall_rating DESC LIMIT 0,10";
$ratingCoachesArr = $UserManagerObjAjax->GetRecords('All',$ratingCoachesSql);
$NumRating = count($ratingCoachesSql);

$lessonsCoachesSql = "SELECT u . * , ut.about, COUNT( * ) AS size FROM  ".TABLEPREFIX."_lesson l,  ".TABLEPREFIX."_user u, ".TABLEPREFIX."_user_type_user_relation as ut WHERE l.coach_id = u.user_id  and ut.user_type_id = 1 and ut.user_id = u.user_id GROUP BY  `coach_id`  ORDER BY size DESC LIMIT 0,10";
$lessonsCoachesArr = $UserManagerObjAjax->GetRecords('All',$lessonsCoachesSql);
$NumLessons = count($lessonsCoachesArr);

$newsSql = "SELECT *, date_format(".date_added.",'%M  %d , %Y') as n_date FROM ".TABLEPREFIX."_news where is_active='Y' order by date_added desc";
$latestNewsArr = $UserManagerObjAjax->GetRecords('All',$newsSql);


$adSql = "SELECT * FROM ".TABLEPREFIX."_ads WHERE ads_position=1 AND is_active='Y' AND (ads_start_date <= '".date('Y-m-d')."' AND ads_end_date >= '".date('Y-m-d')."') ORDER BY date_added DESC";
$adArr = $UserManagerObjAjax->GetRecords('All',$adSql);
$NumAd = count($adArr);

$ForumSql = "SELECT topic_id, forum_id, post_text, post_subject FROM forum_posts WHERE topic_id>0 AND post_approved='1' ORDER BY topic_id DESC LIMIT 0,3";
$forumArr = $UserManagerObjAjax->GetRecords('All',$ForumSql);

$ForumRecentSql = "SELECT t.topic_id,t.forum_id, t.topic_title, t.topic_views, p.post_text, f.forum_name FROM forum_forums f, forum_topics t , forum_posts p WHERE f.forum_id=t.forum_id AND t.topic_approved='1' AND p.topic_id=t.topic_id AND p.post_subject=t.topic_title ORDER by t.topic_last_post_time DESC LIMIT 0,6";
$ForumRecentArr = $UserManagerObjAjax->GetRecords('All',$ForumRecentSql); 

$ForumPopularSql = "SELECT t.topic_id,t.forum_id, t.topic_title, t.topic_views, p.post_text, f.forum_name FROM forum_forums f, forum_topics t , forum_posts p WHERE f.forum_id=t.forum_id AND t.topic_approved='1' AND t.topic_id=p.topic_id AND p.post_subject=t.topic_title ORDER by t.topic_replies DESC LIMIT 0,6";
$ForumPopularArr = $UserManagerObjAjax->GetRecords('All',$ForumPopularSql); 

$ForumViewSql = "SELECT t.topic_id,t.forum_id, t.topic_title, t.topic_views, p.post_text FROM forum_topics t , forum_posts p WHERE t.topic_approved='1' AND p.topic_id=t.topic_id AND p.post_subject=t.topic_title ORDER by t.topic_views DESC LIMIT 0,6";
$ForumViewArr = $UserManagerObjAjax->GetRecords('All',$ForumViewSql); 

$smarty->assign('ForumRecentArr',$ForumRecentArr); 
$smarty->assign('facebook',$facebook);
$smarty->assign('twitter',$twitter); 
$smarty->assign('ForumPopularArr',$ForumPopularArr); 
$smarty->assign('ForumViewArr',$ForumViewArr);// forum topics
$smarty->assign('homenevoArr',$homenevoArr);
$smarty->assign('NumVideo',$NumVideo);
$smarty->assign('latestvdoArr',$latestvdoArr);
$smarty->assign('featurevdoArr',$featurevdoArr);
$smarty->assign('introArr',$introArr);
$smarty->assign('howitArr',$howitArr);
$smarty->assign('inForumArr',$inForumArr);
$smarty->assign('NumFeature',$NumFeature);
$smarty->assign('NumRating',$NumRating);
$smarty->assign('NumLessons',$NumLessons);
$smarty->assign('featuredCoachesArr',$featuredCoachesArr);
$smarty->assign('ratingCoachesArr',$ratingCoachesArr);
$smarty->assign('lessonsCoachesArr',$lessonsCoachesArr);
$smarty->assign('latestNewsArr',$latestNewsArr);
$smarty->assign('adArr',$adArr);
$smarty->display("index.tpl");
include("footer.php");
?>