<?php
	include("general_include.php");
	include "checklogin.php";
?>

<html>
	<head>
		<link type="text/css" rel="stylesheet" href="style/test.css">
		<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="jss/jquery.ui.datepicker.js"></script>
	</head>
	<body>
	<!--
	jQuery.ajax({
								type:	"POST",
								url:	"datafeed.php",
								datatype: "json",
								data:	{method:"list", userid:"26", day: "1344916800000"},
								success:	function(data) {
											result = jQuery(jqXHR);
										}
										
										((1345003200)-(24*3600))*1000
				});-->
	
		<script>
				var now = new Date();
				now.setHours(0,0,0,0);
				now=now.getTime();
				
				var userId = <?php echo $_SESSION['user_id']; ?>
				
				jQuery.post("datafeed.php", {method:"listEvents", userid:userId, day: now}, function(data){
					scheduleList = jQuery("#schedule_list_content");
					
					for (eachDay in data){
						for (eachEvent in data[eachDay]['allEvents']) {
							if (typeof ulScheduleList == 'undefined')  {
								ulScheduleList = jQuery("<ul></ul>");
							}
							theDate = new Date(data[eachDay]['day']*1000 - 24*3600);
							theDate = jQuery.datepicker.formatDate('M dd, yy', theDate);
							
							liScheduleList = jQuery("<li>" + 
									"Training Ses.: " + data[eachDay]['allEvents'][eachEvent]['gameName'] + "<br />" +
									theDate + " " + data[eachDay]['allEvents'][eachEvent]['startTime'] //+ "-" + data[eachDay]['allEvents'][eachEvent]['endTime']  
								+ "</li>");
							ulScheduleList.append(liScheduleList);
						}
					}
					scheduleList.append(ulScheduleList);
				}, "json");
		</script>
		<div id="user_chat_wrapper">

			<div id="timer" class="chat_left">
				<div id="timer_title" class="comunication_title">
					Timer <a>( Details... )</a>
				</div>
			</div>
			<div id="chat" class="chat_center phono">
				<div id="chat_title" class="comunication_title">
					Chat <a>( Details... )</a>
				</div>
			</div>
			<div id="schedule_list" class="chat_right">
				<div id="schedule_list_title" class="comunication_title">
					Schedule <a>( Details... )</a>
				</div>
				<div id="schedule_list_content"></div>
			</div>

		</div>
	</body>
</html>