var j=jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery(".con-req").focus(function(){
			//jQuery(this).attr('style','border:0px solid #999999');
			//if(jQuery(this).attr('tagName') == "SELECT") jQuery(this).css({'width':'180px','margin':'0 0 0 0px'});
	});
	jQuery(".con-notreq").focus(function(){
			jQuery(this).attr('style','border:1px solid #999999');
			//if(jQuery(this).attr('tagName') == "SELECT")	jQuery(this).css({'width':'180px','margin':'0 0 0 0px'});
	});
	
	jQuery(".con-req").blur(function(){
		if(jQuery(this).isEmpty())
		{	
			jQuery(this).attr('style','border:1px solid #FF0000');
			jQuery(this).addClass('denied');
		}
		else if(!jQuery(this).emailCheck() && jQuery(this).attr('id') == 'Email')
		{   
			jQuery(this).attr('style','border:1px solid #FF0000');
			jQuery(this).addClass('denied');
		}
		else if(jQuery(this).attr('id') == 'PhoneNo' && !checkPhone(jQuery('#'+jQuery(this).attr('id')).val()))
		{
			jQuery(this).attr('style','border:1px solid #FF0000');
			jQuery(this).addClass('denied');
		}
		else if(jQuery(this).attr('id') == 'txtCaptcha' && jQuery(this).attr('value') != jQuery('#txtcaptcha_correct').attr('value') )
		{   
			jQuery(this).attr('style','border:1px solid #FF0000');
			jQuery(this).addClass('denied');
		}
		else
		{
			jQuery(this).attr('style','');
			jQuery(this).removeClass('denied');
		}
	});
	
	jQuery(".con-notreq").blur(function(){
		if(!jQuery(this).isEmpty())
		{
			if(jQuery(this).attr('id') == 'PhoneNo' && !checkPhone(jQuery('#'+jQuery(this).attr('id')).val()))
			{
				jQuery(this).attr('style','border:1px solid #FF0000');
				jQuery(this).addClass('denied');
			}
			else
			{
				jQuery(this).attr('style','border:1px solid #347C17');
				jQuery(this).removeClass('denied');
			}
		}
	});

jQuery(".reset-bttn").click(function()
{
	jQuery('.con-req').css({'border':'1px solid #A0A0A0','display:':'block','color':'#747474'});
	jQuery('.con-notreq').css({'border':'1px solid #A0A0A0','display:':'block','color':'#747474'});
	jQuery('.con-req').removeClass('denied');
	jQuery('.con-notreq').removeClass('denied');
});

});
// Contact us Portion Starts
function contactus(formID)
{
	
	jQuery(".con-req").blur();
	jQuery(".con-notreq").blur();
	
	
	if(jQuery(".denied").length == 0){	
		var data=jQuery('#'+formID).serializeArray();	
		jQuery("#submit_button").after('<img src="images/indicator.gif" border="0" style="margin-left:20px;">');
		jQuery.ajax({
				type:'POST',
				url:'contact_mail_process.php',
				data: data,
				dataType:'json',
				success:function(jData){
					if(jData.flag == 1)
					{
						jQuery('#registration_123').html(jData.html);
					}
					else if(jData.flag == 2)
					{
						jQuery('#err').html(jData.html);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					jQuery(".contactus-block").html("Some server error occoured. Please report to system administrator.</div>");
				}
		});
	}
}
//Contact us Portion Ends
function checkPhone(elem)
{
//var phoneRegExp =/^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,13})(-| )?(\d{1})(( x| ext)\d{1,5}){0,1}$/; 
//var phoneRegExp = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
//if (elem.match(phoneRegExp)) 
if(elem.length<8)
 return false;
else 
 return true;	 
}
/* this is for number only check starts */
function numCheck(val)
{
	var number =/^(\d){1,}$/; 
	if (val.match(number)) 
	{
			return true;
	} 
	else 
	{
			return false;
	}
}
/* this is for number only check ends */
