

function gradient(id, level)
{
	var box = document.getElementById(id);
	box.style.opacity = level;
	box.style.MozOpacity = level;
	box.style.KhtmlOpacity = level;
	box.style.filter = "alpha(opacity=" + level * 100 + ")";
	box.style.display="block";
	return;
}


function fadein(id) 
{
	var level = 0;
	/*while(level <= 1)
	{
		setTimeout( "gradient('" + id + "'," + level + ")", (level* 1000) + 10);
		level += 0.01;
	}*/
	gradient( id ,"1.0");
	//alert(level); 
}


// Open the lightbox


function openbox(formtitle, fadin)
{
  var box = document.getElementById('box'); 
  var left = parseInt(screen.width)-518;
  left = left/2;
  document.getElementById('shadowing').style.display='block';

  var btitle = document.getElementById('boxtitle');
  document.getElementById('box').style.left = left+'px';
  btitle.innerHTML = formtitle;
  
  if(fadin)
  {
	 gradient("box", 0);
	 fadein("box");
  }
  else
  { 	
    box.style.display='block';
  }  	
}


// Close the lightbox

function closebox()
{
   document.getElementById('box').style.display='none';
   document.getElementById('shadowing').style.display='none';
}



function openreplybox(formtitle, fadin)
{
  var box = document.getElementById('replybox'); 
  var left = parseInt(screen.width)-518;
  left = left/2;
  
  document.getElementById('shadowreply').style.display='block';
  document.getElementById('replybox').style.left = left+'px';
  var btitle = document.getElementById('replytitle');
  btitle.innerHTML = formtitle;
  
  if(fadin)
  {
	 gradient("replybox", 0);
	 fadein("replybox");
  }
  else
  { 	
    replybox.style.display='block';
  }  	
}


// Close the lightbox

function closereplybox()
{
   document.getElementById('replybox').style.display='none';
   document.getElementById('shadowreply').style.display='none';
}

