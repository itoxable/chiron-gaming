var j=jQuery.noConflict();
j(document).ready(function(){
	j(".con-req").focus(function(){
			j(this).attr('style','border:1px solid #999999');
			if(j(this).attr('tagName') == "SELECT")
				j(this).css({'width':'180px','margin':'0 0 0 0px'});
	});
	j(".con-notreq").focus(function(){
			j(this).attr('style','border:1px solid #999999');
			if(j(this).attr('tagName') == "SELECT")
				j(this).css({'width':'180px','margin':'0 0 0 0px'});
	});
	
	j(".con-req").blur(function(){
		if(j(this).isEmpty())
		{	
			j(this).attr('style','border:1px solid #FF0000');
			j(this).addClass('denied');
		}
		else if(!j(this).emailCheck() && j(this).attr('id') == 'r_email')
		{   
			j(this).attr('style','border:1px solid #FF0000');
			j(this).addClass('denied');
		}
		else if(j(this).attr('id') == 'r_b_phone' && !checkPhone(j('#'+j(this).attr('id')).val()))
		{
			j(this).attr('style','border:1px solid #FF0000');
			j(this).addClass('denied');
		}
		else if(j(this).attr('id') == 'txtCaptcha' && j(this).attr('value') != j('#txtcaptcha_correct').attr('value') )
		{   
			j(this).attr('style','border:1px solid #FF0000');
			j(this).addClass('denied');
		}
		else
		{
			j(this).attr('style','border:1px solid #347C17');
			j(this).removeClass('denied');
		}
	});
	
	j(".con-notreq").blur(function(){
		if(!j(this).isEmpty())
		{
			if(j(this).attr('id') == 'r_b_phone' && !checkPhone(j('#'+j(this).attr('id')).val()))
			{
				j(this).attr('style','border:1px solid #FF0000');
				j(this).addClass('denied');
			}
			else
			{
				j(this).attr('style','border:1px solid #347C17');
				j(this).removeClass('denied');
			}
		}
	});

j(".reset-bttn").click(function()
{
	j('.con-req').css({'border':'1px solid #A0A0A0','display:':'block','color':'#747474'});
	j('.con-notreq').css({'border':'1px solid #A0A0A0','display:':'block','color':'#747474'});
	j('.con-req').removeClass('denied');
	j('.con-notreq').removeClass('denied');
});

});
// Contact us Portion Starts
function register(formID)
{
	
	j(".con-req").blur();
	j(".con-notreq").blur();
	
	
	if(j(".denied").length == 0){
		j("#registration").submit();
		
		//window.location.href="payment_gateway_process.php";
	}
}

/*	
		var data=j('#'+formID).serializeArray();	
		j("#submit_button").after('<img src="images/indicator.gif" border="0" style="margin-left:20px;">');
		j.ajax({
				type:'POST',
				url:'payment_gateway_process.php',
				data: data,
				dataType:'json',
				success:function(jData){
					if(jData.flag == 1)
					{
						j('.registration').html(jData.html);
					}
					else if(jData.flag == 2)
					{
						j('#err').html(jData.html);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					j(".contactus-block").html("Some server error occoured. Please report to system administrator.</div>");
				}
		});
	*/
//Contact us Portion Ends
function check_discount()
{
		var discount_code = j("#discount_code").val();
		j.ajax({
				type:'POST',
				url:'check_availability.php?discount_code='+discount_code,
				dataType:'json',
				success:function(jData){
					if(jData.flag == 3)
					{
						j('#message').html(jData.html);
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					j(".contactus-block").html("Some server error occoured. Please report to system administrator.</div>");
				}
		});
}



function checkPhone(elem)
{
//var phoneRegExp =/^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,13})(-| )?(\d{1})(( x| ext)\d{1,5}){0,1}$/; 
var phoneRegExp = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;

if (elem.match(phoneRegExp)) 
return true;
}
/* this is for number only check starts */
function numCheck(val)
{
	var number =/^(\d){1,}$/; 
	if (val.match(number)) 
	{
		return true;
	} 
	else 
	{
		return false;
	}
}
/* this is for number only check ends */
