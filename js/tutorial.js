$(function() {
  $('.error').hide();
 

  $(".ok").click(function() {
		// validate and process form
		// first hide any error messages
    $('.error').hide();
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var email = $("input#news_email").val();
	if (email == "") {
      $("label#email_error").show();
      $("input#news_email").focus();
      return false;
    }
	
		
		var dataString = 'email=' + email 
		//alert (dataString);return false;
		
		$.ajax({
			  
      type: "POST",
      url: "process_newsletter.php",
      data: dataString,
      success: function() {
        $('#contact_form').html("<div id='message'></div>");
        $('#message').html("<h2>Contact Form Submitted!</h2>")
        .append("<p>We will be in touch soon.</p>")
        .hide()
        .fadeIn(1500, function() {
          $('#message').append("<img id='checkmark' src='images/check.png' />");
        });
      }
     });
    return false;
	});
});
runOnLoad(function(){
  $("input#news_email").select().focus();
});
