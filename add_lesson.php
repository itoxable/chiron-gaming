<?php
$page_name=basename($_SERVER['SCRIPT_FILENAME']);
$IsPreserved	= 'Y';
$IsProcess		= $_REQUEST['IsProcess'];
include("general_include.php");
include "checklogin.php";

if($IsProcess <> 'Y')
{
	include "top.php";
	//include "left.php";
}	

$action	= $_REQUEST['action'];

if($action=="del"){	
    if(!empty($delete_id)){
	$SelectLessonDetailsSql="SELECT *,(SELECT name FROM ".TABLEPREFIX."_user WHERE user_id=".TABLEPREFIX."_lesson.coach_id) AS coach_name,(SELECT email FROM ".TABLEPREFIX."_user WHERE user_id=".TABLEPREFIX."_lesson.coach_id) AS coach_email FROM ".TABLEPREFIX."_lesson where lesson_id='$delete_id'";
        $SelectLessonDetails=$UserManagerObjAjax->GetRecords("Row",$SelectLessonDetailsSql);
        $DelSql = "DELETE FROM ".TABLEPREFIX."_lesson where lesson_id='$delete_id'";
        $UserManagerObjAjax->Execute($DelSql);

        $to       = 'admin@chirongaming.com';
        $subject  = 'Lesson Deleted';
        $message  = '<h3>Dear Administrator,</h3>';
        $message .= 'The lesson '.$SelectLessonDetails["lesson_title"].' has been deleted by tutor '.$SelectLessonDetails["coach_name"];
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: '.$SelectLessonDetails["coach_email"]. "\r\n" .
                                'Reply-To: sales@citytechsoftware.com' . "\r\n" .
                                'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);

	  
    }	  	 
}  

if($action=="list_search"){ 
    $Search_lesson_title = $Search_lesson_title=='Search by title'?'':$Search_lesson_title;
    $Search_student_id = $Search_student_id;
    $Search_lesson_status = $Search_lesson_status;
    $Search_from_date = $Search_from_date=='From'?'':$Search_from_date;
    $Search_to_date = $Search_to_date=='To'?'':$Search_to_date;

    if(!empty($Search_lesson_title))
    {
        $searchSql =  $searchSql." AND lesson_title like '%".trim(mysql_quote($Search_lesson_title,"N"))."%'"; 
    }

    if(!empty($Search_student_id))
    {
        $searchSql =  $searchSql." AND student_id=".$Search_student_id; 
    }

    if(!empty($Search_lesson_status))
    {
        $searchSql =  $searchSql." AND lesson_status_id=".$Search_lesson_status; 
    }

    if(!empty($Search_from_date))
    {
        $searchDate = $Search_from_date.' 00:00:00';
        $searchSql =  $searchSql." AND date_added >= '".mysql_quote($searchDate,"N")."'"; 
    }

    if(!empty($Search_to_date))
    {
        $toDate =  $Search_to_date.' 23:59:59';
        $searchSql =  $searchSql." AND date_added <= '".mysql_quote($toDate,"N")."'"; 
    }

    $SearchLink="dosearch=GO&Search_language=$Search_language";
}	

$SelectTotPriceSql="SELECT sum(lesson_price) AS tot_prc FROM ".TABLEPREFIX."_lesson WHERE coach_id=".$_SESSION['user_id'];
$SelectTotPrice = $UserManagerObjAjax->GetRecords("Row",$SelectTotPriceSql);

$total_price=number_format($SelectTotPrice['tot_prc'], 2, '.', '');

$SelectTotComSql="SELECT lesson_price,lesson_commision_admin FROM ".TABLEPREFIX."_lesson WHERE coach_id=".$_SESSION['user_id']." AND lesson_status_id=4";
$SelectTotCom = $UserManagerObjAjax->GetRecords("All",$SelectTotComSql);

$tot_com=0;

for($i=0;$i<count($SelectTotCom);$i++){

$tot_com=$tot_com+number_format(($SelectTotCom[$i]['lesson_price']*(1-($SelectTotCom[$i]['lesson_commision_admin']/100))), 2, '.', '');
}


$CoachgameSql = "SELECT * FROM ".TABLEPREFIX."_lesson WHERE coach_id=".$_SESSION['user_id'].$searchSql." ORDER BY date_added DESC";
$CoachgameArr = $UserManagerObjAjax->GetRecords("All",$CoachgameSql);

$Numcoachgame=count($CoachgameArr); $tot_price=0; $tot_comision=0;

for($g=0;$g<$Numcoachgame;$g++)
{
	$CoachgameArr[$g]['date_added'] = date("M j, Y",strtotime($CoachgameArr[$g]['date_added'])).' at '.date("g:i a",strtotime($CoachgameArr[$g]['date_added']));
        $CoachgameArr[$g]['start'] = date("M j, Y",strtotime($CoachgameArr[$g]['start'])).' at '.date("g:i a",strtotime($CoachgameArr[$g]['start']));
	$CoachgameArr[$g]['lesson_id'] = $CoachgameArr[$g]['lesson_id'];
	$CoachgameArr[$g]['lesson_title'] = $CoachgameArr[$g]['lesson_title'];
	$CoachgameArr[$g]['student_id'] = $CoachgameArr[$g]['student_id'];
	
	$Sql_Name="SELECT name FROM ".TABLEPREFIX."_user WHERE user_id=".$CoachgameArr[$g]['student_id'];
	$Sql_NameArr = $UserManagerObjAjax->GetRecords("Row",$Sql_Name);
	
	$CoachgameArr[$g]['student_name'] = $Sql_NameArr['name'];
	
	$CoachgameArr[$g]['lesson_price'] = number_format($CoachgameArr[$g]['lesson_price'], 2, '.', '');
	$tot_price=$tot_price+$CoachgameArr[$g]['lesson_price'];
	
	$CoachgameArr[$g]['lesson_commision_admin'] = $CoachgameArr[$g]['lesson_commision_admin'];
	
	$CoachgameArr[$g]['lesson_status_id'] = $CoachgameArr[$g]['lesson_status_id'];
	
	$Sql_Status="SELECT lesson_status_title FROM ".TABLEPREFIX."_lesson_status WHERE lesson_status_id=".$CoachgameArr[$g]['lesson_status_id'];
	$Sql_StatusArr = $UserManagerObjAjax->GetRecords("Row",$Sql_Status);
	
	$CoachgameArr[$g]['my_commision'] = number_format(($CoachgameArr[$g]['lesson_price']*(1-($CoachgameArr[$g]['lesson_commision_admin']/100))), 2, '.', '');

	if($CoachgameArr[$g]['lesson_status_id'] == 4){

		$tot_comision=$tot_comision+$CoachgameArr[$g]['my_commision'];
	}

	$CoachgameArr[$g]['lesson_status'] = $Sql_StatusArr['lesson_status_title'];
}

/* Student Record */
$StudentRecSql="SELECT us.name,les.student_id FROM ".TABLEPREFIX."_lesson AS les,".TABLEPREFIX."_user AS us WHERE les.student_id=us.user_id and les.coach_id=".$_SESSION['user_id']." GROUP BY us.name";
$StudentRecArr=$UserManagerObjAjax->GetRecords("All",$StudentRecSql);
for($i=0;$i<count($StudentRecArr);$i++)
{
$SelectStudentArr[$i]['student_id'] = $StudentRecArr[$i]['student_id'];

$StudentSql="SELECT name FROM ".TABLEPREFIX."_user WHERE user_id=".$SelectStudentArr[$i]['student_id'];
$StudentName = $UserManagerObjAjax->GetRecords("Row",$StudentSql);

$SelectStudentArr[$i]['student_name'] 	= $StudentName['name'];
}	
/* Student Record */

/* Status Record */
$StatusRecSql="SELECT lesson_status_id,lesson_status_title FROM ".TABLEPREFIX."_lesson_status ";
$StatusRecArr=$UserManagerObjAjax->GetRecords("All",$StatusRecSql);
for($i=0;$i<count($StatusRecArr);$i++)
{
$SelectStatusArr[$i]['lesson_status_id'] = $StatusRecArr[$i]['lesson_status_id'];
$SelectStatusArr[$i]['lesson_status_title']	= $StatusRecArr[$i]['lesson_status_title'];
}	
/* Status Record */
if($IsProcess == 'Y')
{
   $Sqltype = "SELECT * FROM ".TABLEPREFIX."_user_type_user_relation WHERE user_id='".$_SESSION['user_id']."'";
	$typeArr = $UserManagerObjAjax->GetRecords("All",$Sqltype);
	$Numtype = count($typeArr);
	$is_coach = 0;
	$is_partner = 0;
	for($t=0;$t<$Numtype;$t++)
	{
	   if($typeArr[$t]['user_type_id']==1)
		  $is_coach = 1;
	   if($typeArr[$t]['user_type_id']==3)
		  $is_partner = 1;
	}
}
$smarty->assign('is_coach',$is_coach);
$smarty->assign('is_partner',$is_partner);
$smarty->assign('SelectStudentArr',$SelectStudentArr);
$smarty->assign('SelectStatusArr',$SelectStatusArr);
$smarty->assign("Search_lesson_title",$Search_lesson_title);
$smarty->assign("Search_to_date",$Search_to_date);
$smarty->assign("Search_from_date",$Search_from_date);

$smarty->assign("Search_student_id",$Search_student_id);
$smarty->assign("Search_lesson_status",$Search_lesson_status);
$smarty->assign('Numcoachgame',$Numcoachgame);
$smarty->assign('CoachgameArr',$CoachgameArr);
$smarty->assign('tot_price',$tot_price);
$smarty->assign('tot_comision',$tot_comision);
$smarty->assign('total_price',$total_price);
$smarty->assign('tot_com',$tot_com);
$smarty->display('add_lesson.tpl');
if($IsProcess <> 'Y')
  include "footer.php";
?>