{literal}

<link type="text/css" rel="stylesheet" href="style/lightbox-form.css">
<script src="js/lightbox-form.js" type="text/javascript"></script>
<script language="javascript" src="js/contact_us.js"></script>
<link type="text/css" href="jss/tipTip.css"  rel="stylesheet"  />
<script src="jss/jquery.tipTip.js" type="text/javascript"></script>


<script language="javascript">
jQuery(function(){
    jQuery(".someClass").tipTip();
});

function CheckFields(formID,NumRatingCat){
    var r_comment = document.getElementById('review_comment').value;
    var NumRatingCat = NumRatingCat;	 	 	 	 
    var training_partner_id = document.getElementById('training_partner_id').value;
    var reviewed_by = document.getElementById('logged_user').value;
    if(document.getElementById('review_comment').value==''){
        jQuery('#review_comment').css('border','1px solid #FF0000');
        return false;
    }
    else 
        jQuery('#review_comment').css('border','1px solid #ccc');

    var frmID='#'+formID;
	
    var params ={
        'action': 'sendreview',
        'NumCoachRatingCat': NumRatingCat,
        'IsProcess': 'Y',
        'reviewed_by': reviewed_by
    };

    var paramsObj = jQuery(frmID).serializeArray();
    jQuery.each(paramsObj, function(i, field){
        params[field.name] = field.value;
    });
	
   if(r_comment!=''){
        jQuery.ajax({
             type: "POST",
             url: 'training_partner_details.php?training_partner_id='+training_partner_id,
             data: params,
             dataType : 'text',
             success: function(data){
                 jQuery('.signin-message').fadeIn(0);
                 jQuery('#msgbox').fadeIn(0);
                 jQuery('#msgbox').html('Review posted successfully.');
                 jQuery('.signin-message').fadeOut(5000);
                 jQuery('#paging').html(data);
             }
         });
         document.getElementById("review_comment").value='';
         document.getElementById('serialStar').value='';
         jQuery("#reviewwindow").fadeOut("fast", function(){jQuery('#overlay').fadeOut();});
     }
  }		

function closereviewbox(){
    document.getElementById("review_comment").value='';
    document.getElementById("serialStar").value='';
    jQuery('#review_comment').css('border','1px solid #ccc');
    jQuery("#reviewwindow").fadeOut("fast", function(){jQuery('#overlay').fadeOut();});
}

</script>  
{/literal}
{if $IsProcess neq 'Y'}
<div class="right-panel">
    <div class="findcoach-title" style="margin-bottom: 5px;">
        <div class="title" style="float: left;padding: 0;border: none;">
            <a style="text-decoration: none;" href="find_training_partner.php">Find Training Partner</a>
        </div>

        <div class="breadcrumb">{$tpArr.username}</div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div class="game-list-block">
	  
    <div class="game-image-block">
	{if $tpArr.photo neq ''}
        <img src="uploaded/user_images/thumbs/big_{$tpArr.photo}" alt="Training Partner" border="0" />
	{else}
	<img src="images/coach_thumb.jpg" border="0" /> 
	{/if} 
    </div> 
		    
    <div class="game-details-block">
        <div class="score_title listcoachname" data-id="{$tpArr.user_id}" >
            <h4 class="gameshead">
                <a class="{$online_status}" title="{$online_status}"></a>
                {$tpArr.username} 
                <a id="{$tpArr.user_id}_friendStar" class="{if $isfriend eq 'y'}star_friend{else}star_not_friend{/if}" title="{if $isfriend eq 'y'}in your friend list{else}not your friend list{/if}"></a>
            </h4>
            {if $smarty.session.user_id neq '' && $smarty.session.user_id neq $tpArr.user_id}
            <div class="addAsFriendWindow" id="addAsFriendWindow_{$tpArr.user_id}">
                <a class="addAsFriendLink" href="javascript:;" onclick="openChatTab(event,'{$tpArr.user_id}','{$tpArr.username}')">Message</a><br/>
                {if $isfriend eq 'n'}
                <a class="addAsFriendLink addFriend" href="javascript:;" onclick="addAsFriend(event,'{$tpArr.user_id}','{$tpArr.username}',true)">Add as friend</a>
                {else}
                <a class="addAsFriendLink removeFriend" href="javascript:;" onclick="removeFriend(event,'{$tpArr.user_id}','{$tpArr.username}',true)">Remove friend</a>
                {/if}
            </div>
            {/if}
        </div>
    <div class="score_card">
        <div id="scoreCard">
            <ul>
                {if $tpOverallArr.overall_rating neq 0}
                <li id="Overall" class="tTip">
                    <span class="someClass" title="Would you train with this partner again?">
                    {$tpOverallArr.overall_rating}
                    <span>Overall</span>
                    </span>
                </li>
                {/if}
                {section name=k loop=$CatIndividualRec}
                <li id="$CatIndividualRec[k].rating_category" class="tTip">
                   <span class="someClass" title="{$CatIndividualRec[k].rating_category_tooltip}">
                   {$CatIndividualRec[k].rating}
                   <span>{$CatIndividualRec[k].rating_category}</span>
                   </span>
                </li>   
                {/section} 
                
                
             </ul>
     </div>
		      </div>
            <br />
            <p class="clear"></p>
			<div class="games-desc partner_game_details" style="margin-top: 10px;">
				<dl>
                                    <dt>Language:</dt>
                                    <dd>{if $tpArr.language neq ''}{$tpArr.language}{else}N/A{/if}</dd>
				</dl>
				{if $tpArr.avail_local eq 'Y'}
				<dl>
                                    <dt>City (Local meet-up):</dt>
                                    <dd>{if $tpArr.availability_city neq ''}{$tpArr.availability_city}{else}N/A{/if}</dd>
				</dl>
				{/if}
				<dl>
                                    <dt>Availability:</dt>
                                    <dd>{if $tpArr.availability_type neq ''}{$tpArr.availability_type}{else}N/A{/if}</dd>
				</dl> 
				<dl>
                                    <dt>Introduction:</dt>
                                    <dd><div>{if $tpOverallArr.about neq ''}{$tpOverallArr.about}{else}N/A{/if}</div></dd>
				</dl>
			</div>
                        {section name=game loop=$tpgameArr}
			<div class="games-desc partner_game_details" style="margin-top: 15px;">
                            <h2>{$tpgameArr[game].game_name}</h2>
                            <div style="margin-left: 15px;">
                                {section name=categoryIndex loop=$tpgameArr[game].categoriesArr}
                                <dl>
                                    <dt>{$tpgameArr[game].categoriesArr[categoryIndex].category_name}:</dt>
                                    <dd>{if $tpgameArr[game].categoriesArr[categoryIndex].properties neq ''}{$tpgameArr[game].categoriesArr[categoryIndex].properties}{else}&nbsp;{/if}</dd>
                                </dl>
                                {/section}

                                <dl>
                                        <dt>Peak Hours:</dt>
                                        <dd>{if $tpgameArr[game].peak_hours neq ''} {$tpgameArr[game].peak_hours}{else}N/A{/if}</dd>
                                </dl> 

                                <dl>
                                        <dt>Rating:</dt>
                                        <dd>{if $tpgameArr[game].rating neq ''} {$tpgameArr[game].rating} {else} N/A {/if}</dd>
                                </dl> 
                                <dl>
                                        <dt>Experience:</dt>
                                        <dd><div>{if $tpgameArr[game].experience neq ''}{$tpgameArr[game].experience}{else}N/A{/if}</div></dd>
                                </dl> 
                                <dl>
                                        <dt>Need Improvement In:</dt>
                                        <dd><div>{if $tpgameArr[game].need_improvement neq ''}{$tpgameArr[game].need_improvement}{else}N/A{/if}</div></dd>
                                </dl> 
                            </div>
			</div>
			{/section}
            <div class="clear" style="padding-top:10px;"></div>
			{if $smarty.session.user_id neq '' && $smarty.session.user_id neq $tpArr.user_id}
			<div style="margin-top:10px;float: right;">
				<a style="margin-top:10px;float: right;" class="button" href="javascript:void(0);" onClick="openReview('Review and Rating for <b>{$tpArr.username}</b>')"><span>Review</span></a>
				<a style="margin-top:10px;float: right;" class="button" href="javascript:void(0);" onClick="openChatTab(event,'{$tpArr.user_id}','{$tpArr.username}','{$isfriend}')"><span>Message</span></a>
			</div>
			
			  <br /> <div class="clear"></div>
		          
			  
			  
		   {/if}
         </div>
         <div class="clear"></div>
      </div>
	  
	  {if $Numvideo > 99999}
      <div class="game-list">
         <h2 title="Video">Videos</h2>
         <div class="clear"></div>
		 {section name=video loop=$tpvideoArr}
		 
		 {assign var='i' value=$smarty.section.video.index+1}
          <div {if $i%3 eq '0'} class="games-replay-last" {else} class="games-replay" {/if}>
          
          		<div class="replay-content">
			   <a href="videodetails.php?video_id={$tpvideoArr[video].video_id}">
			   <img src="uploaded/video_images/thumbs/mid_{$tpvideoArr[video].video_image}" alt="" title="{$tpvideoArr[video].video_title}" border="0"  /></a>
			   </div>
			   <div class="replay-content">
				  <div class="replay-title"><a href="videodetails.php?video_id={$tpvideoArr[video].video_id}">{$tpvideoArr[video].video_title}</a><br />
                  <span class="date">{$tpvideoArr[video].video_date}<br />
				  {if $tpvideoArr[video].time_length neq ''}{$tpvideoArr[video].time_length} min{else} N/A {/if}
				  </span>
                                </div>
				  <div class="game-rating">
				  
                                  
                                  <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                    <tr>
                                        <td><b>Likes:</b></td>
                                        <td>{$tpvideoArr[video].numlike}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Disikes:</b></td>
                                        <td>{$tpvideoArr[video].numunlike}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Views:</b></td>
                                        <td>{$tpvideoArr[video].view_count}</td>
                                    </tr>
                                </table>
                                  
                                  
				  </div>
                  <div class="replay-title" style="padding-top:0px; width:90%"><span class="date">Game : <em>{$tpvideoArr[video].game}</em></span></div>
			   </div>
				  
          </div>
         
		  {if $i%3 eq '0'}
            <div class="clear"></div>
	      {/if}		
		 {/section}
		<div class="clear"></div> 
      </div>
	  {/if}{/if}
      <div class="clear"></div>
      
    <div class="signin-message" style="display:none;">
        <div id="msgbox" style="color:#0077BC;"></div>
    </div>
    <div id="paging"> 
    <div id="review">
    <div class="review-list">
        <h2 title="Review">Reviews</h2>
	{if $NumReview >0}
        <div class="clear"></div>
        {section name=rrow loop=$UserReview}
        <div class="review-block">
             
			 <div class="review-left">
			 <a href="{$UserReview[rrow].userlink}">
			  <img src="{$UserReview[rrow].img}" height="92" width="105"  alt="{$UserReview[rrow].review_by}" border="0" />
			 </a>
			 </div>
			 <div class="review-right">
			 <div class="review-sub-title">
             
             	<div class="review_thumb_block">
				 <h4 class="reviewshead">Reviewed By : <a href="{$UserReview[rrow].userlink}">{$UserReview[rrow].review_by}</a></h4><br/><br />
	 			 
				 <span><em>Posted on</em></span> : {$UserReview[rrow].r_date}<br /><br />
				 {if $UserReview[rrow].review_comment neq ''}
				 <p><i>Review</i>:&nbsp;{$UserReview[rrow].review_comment|nl2br}</p><br />
				 {/if}
				</div>
                
                <div class="review_card_block">
                 <!-- Score Area -->
                <div id="scoreCard">
                    <ul>
			{if $UserReview[rrow].overall_r neq 0}
                        <li id="Overall" class="tTip">
                            <span class="someClass" title="Would you train with this partner again?">
                            {$UserReview[rrow].overall_r}
                            <span>Overall</span>
                            </span>
                        </li>
                        {/if}
                        {section name=k loop=$UserReview[rrow].individual}
                         <li id="$UserReview[rrow].individual[k].rating_category" class="tTip">
                            <span class="someClass" title="{$UserReview[rrow].individual[k].rating_category_tooltip}">
                            {$UserReview[rrow].individual[k].rating}
                            <span>{$UserReview[rrow].individual[k].rating_category}</span>
                            </span>
                        </li>   
                         {/section}   
                       
                    </ul>
                </div>
                </div>
                
                <div class="clear"></div>
				<!-- Score Area END -->
                 
			 </div>
			 <!--p class="post"><span>Posted on</span> : {$UserReview[rrow].r_date}</p-->
			 </div>
			 <div class="clear"></div>
		</div>
		 
	    {/section}
      
	  {if $pagination_arr[1]}
      <div class="clear"></div>
      <div class="pagin">
         <ul>
            {$pagination_arr[1]}
         </ul>
      </div>
	  {/if}
	  {else}
	   No reviews
	{/if} 
	</div>
   </div>	
  </div>	 
  </div> 

<div class="modalpopup" id="reviewwindow">
    <a class="modalpopup-close-btn" href="javascript:;"></a>
    <div id="boxtitle" style="font-weight: normal;" class="title"></div>
    <div class="review">
    <div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:5px;">{if $ermsg neq ''} {$ermsg}{/if} </div>
    <form name="contact" id="Rating_Coach"  method='post' action="" onsubmit="return false;">
        <input type="hidden" name="training_partner_id" id="training_partner_id" value="{$training_partner_id}" />
        <input type="hidden" name="logged_user" id="logged_user" value="{$logged_user}" />
        <input type="hidden" name="user_type_id" id="user_type_id" value="3" />
        <table cellpadding="0" cellspacing="10">
            <tr>
                <td class="">
                    <span class="someClass" title="Would you train with this partner again?">Overall :</span> 
                </td>
                <td>
                    <div class="">
                        <table cellpadding="5" cellspacing="5">
                            <tr valign="middle">
                                <td>1&nbsp;<input type="radio" value="1" name="Rating_overall" id="serialStar" title="Poor"></td>
                                <td>2&nbsp;<input type="radio" value="2" name="Rating_overall" id="serialStar" title="Ok"></td>
                                <td>3&nbsp;<input type="radio" value="3" name="Rating_overall" id="serialStar" title="Good"></td>
                                <td>4&nbsp;<input type="radio" value="4" name="Rating_overall" id="serialStar" title="Better"></td>
                                <td>5&nbsp;<input type="radio" value="5" name="Rating_overall" id="serialStar" title="Awesome"></td>
                            </tr>
                        </table>
                        	 
                    </div>  
                </td>
            </tr>
            {section name=i loop=$RatingCatArr}
            <tr>
                <td class="">
                    <span class="someClass" title="{$RatingCatArr[i].rating_category_tooltip}">{$RatingCatArr[i].rating_category}:</span>
                </td>
                <td colspan="2"> 
                    <div class="">
                        <table cellpadding="5" cellspacing="5">
                            <tr>
                                <td>1&nbsp;<input type="radio" value="1:{$RatingCatArr[i].rcat_id}" name="Rating_{$smarty.section.i.index}" id="serialStar" title="Poor"></td>
                                <td>2&nbsp;<input type="radio" value="2:{$RatingCatArr[i].rcat_id}" name="Rating_{$smarty.section.i.index}" id="serialStar" title="Ok"></td>
                                <td>3&nbsp;<input type="radio" value="3:{$RatingCatArr[i].rcat_id}" name="Rating_{$smarty.section.i.index}" id="serialStar" title="Good"></td>
                                <td>4&nbsp;<input type="radio" value="4:{$RatingCatArr[i].rcat_id}" name="Rating_{$smarty.section.i.index}" id="serialStar" title="Better"></td>
                                <td>5&nbsp;<input type="radio" value="5:{$RatingCatArr[i].rcat_id}" name="Rating_{$smarty.section.i.index}" id="serialStar" title="Awesome"></td>
                            </tr>
                        </table>
                        
                    </div>
                </td>
            </tr>
            {/section}
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td width="20%" valign="top">Review :</td>
                <td width="54%"><textarea style="width: 300px;" name="review_comment" id="review_comment" cols="15" rows="5" >{$Formval.user_about}</textarea></td>
            </tr>

            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" align="center">    
                    <input name="submit" class="button" value="Submit" type="submit" onclick="return CheckFields('Rating_Coach',{$NumRatingCat})" />
                    <input name="" class="button" value="Cancel" type="button" onClick="closereviewbox()" />		
                </td>
            </tr>
        </table>
    </form>
    </div> 
</div>   
<script language="javascript">
    changePageTitle('Training partner - {$tpArr.username} ');
</script>