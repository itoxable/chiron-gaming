{literal}
<link media="screen" rel="stylesheet" href="style/colorbox.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script src="js/jquery.colorbox.js"></script>
<script>
		$(document).ready(function(){
		$(".example5").colorbox();
		$(".example6").colorbox({iframe:true, innerWidth:425, innerHeight:344});
       });
</script>
<link type="text/css" rel="stylesheet" href="style/lightbox-form.css">
<script src="js/lightbox-form.js" type="text/javascript"></script>
<script language="javascript" src="js/contact_us.js"></script>
<script language="javascript">
var $ = jQuery.noConflict();

function CheckFields()
{
   if(document.getElementById('review_comment').value=='')
   {
    $('#FormErrorMsg').show();
    $('#FormErrorMsg').html("Please enter your review");
	//$('#FormErrorMsg').fadeOut(2000);
    return false;
   }
    if(document.getElementById('rating1').checked==false && document.getElementById('rating2').checked==false &&  document.getElementById('rating3').checked==false && 
	document.  getElementById('rating4').checked==false && document.getElementById('rating5').checked==false ) 
   {
    
    $('#FormErrorMsg').show();
    $('#FormErrorMsg').html("Please select rating");
	//$('#FormFieldCheckErrorMsg').fadeOut(2000);

    return false;
   }
  return true;
}  
 
</script>   		
{/literal}
{if $IsProcess neq 'Y'}
<div class="right-panel">
      <h1 title="Video"><span>Video</span></h1>
      <div class="clear"></div>
      <div class="game-list">
         
		 <div class="game-image">
		     {if $VideoArr.is_video_url eq 'Y'}
			  <a class='example6' href="{$VideoArr.video_url}rel=0&amp;wmode=transparent" title="{$VideoArr.video_title}">
			  {else}
			   <a class='example5' href="play_video.php?video_id={$VideoArr.video_id}" title="{$VideoArr.video_title}">
			  {/if}
		     <img src="uploaded/video_images/thumbs/big_{$VideoArr.video_image}" alt="Video" border="0" height="156" width="198" />
			 </a>
		</div>
         
         <div class="game-details">
            <div class="game-details-title"><h2>{$VideoArr.video_title}</h2>
            {$VideoArr.star}
            <br />
            <br />
            <div class="clear"></div></div>
            
            <p>{$VideoArr.description}</p>
              
			<p class="clear"></p>
            <ul class="games-desc">
               <li><label class="gametype">Game :</label><label class="gamedetail">{$VideoArr.game}</label></li>
               <li><label class="gametype">Ladder :</label><label class="gamedetail">{$VideoArr.ladder}</label></li>
            </ul>
            <ul class="games-desc">
			  <li><label class="gametype">Race :</label><label class="gamedetail">{$VideoArr.race}</label></li>
               <li><label class="gametype">Time Length :</label><label class="gamedetail">{$VideoArr.time_length}</label></li>
             </ul>
			
            <div class="clear"></div>
           
         </div>
		   {if $smarty.session.user_id neq '' && $smarty.session.user_id neq $VideoArr.user_id}
              <div class="view"><a href="javascript:void(0);" onClick="openbox('Review and Rating', 1)">Review</a></div> 
		   {/if}
		   {if $smarty.session.user_type eq '2' && $is_fav eq '0'}
           
		     <div class="button-flex" id="Favourite" style="margin-right:5px;">
			 <a href="#" onclick="ManagerFavourite('{$page_name}?action=make_fav&video_id={$video_id}')" ><span>Favourite</span></a></div>
		   {/if}
         <div class="clear"></div>
         <br />
	   </div>	
	   {/if} 
      <div id="paging"> 
	  {if $NumReview >0}
     <div class="review-list">
     <h2 title="Review"><span>Review</span></h2>
       <div class="clear"></div>
         {section name=rrow loop=$VdoReview}
		 
	    <div class="review-block">
             {if $VdoReview[rrow].photo neq ''}
			 <div class="review-left">
			 <img src="uploaded/user_images/thumbs/mid_{$VdoReview[rrow].photo}" alt="" border="0" />
			 </div>
			 {/if}
			 <div>
         <div class="review-sub-title"><strong>{$VdoReview[rrow].review_by}</strong><br />
		 {$VdoReview[rrow].rating}
         <div class="clear"></div>
         </div>
         <p>{$VdoReview[rrow].review_comment|nl2br}</p>
         <p class="post"><span>Posted on</span> : {$VdoReview[rrow].r_date}</p>
         
         </div>
         <div class="clear"></div>
         </div>
		 
	    {/section}
      </div>
      {if $pagination_arr[1]}
      <div class="clear"></div>
      <div class="pagin">
         <ul>
            {$pagination_arr[1]}
         </ul>
      </div>
	  {/if}
	{/if} 
   </div>
  </div> 
<div id="shadowing"></div>
<div id="box">
  <span id="boxtitle"></span>
  <div style="font-size:14px;">Review and rating For <b>{$VideoArr.video_title}</b> </div>
   <div class="review">
   <div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:5px;">{if $ermsg neq ''} {$ermsg}{/if} </div>
   <form name="contact" id="Contact"  method='post'>
   <input type="hidden" name="video_id" value="{$video_id}" />
   <fieldset>   
    <label>Review :</label>
            <textarea name="review_comment" id="review_comment" cols="" rows="" >{$Formval.user_about}</textarea>
    <p class="clear"></p>
	<label>Rating :</label>
     
	       <input type="radio" name="rating" id="rating1" value="1" />1
		   <input type="radio" name="rating" id="rating2" value="2" />2
		   <input type="radio" name="rating" id="rating3" value="3" />3
		   <input type="radio" name="rating" id="rating4" value="4" />4
		   <input type="radio" name="rating" id="rating5" value="5" />5
	
    <p class="clear"></p>
    <label>&nbsp;</label>
            <input name="submit" class="submit" value="Submit" type="submit" onclick='return CheckFields()' />
            <input name="" class="cancel" value="Cancel" type="button" onClick="closebox()" />
    </p>
   </fieldset>	
  </form>
 </div> 
</div>