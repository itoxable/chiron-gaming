<div id="paging"> 
{literal}
<script type="text/javascript">
$(document).ready(function($){
					$('#accordion').dcAccordion({
		showCount: true,
		saveState: true
	});
					$('#accordion-1').dcAccordion({
						eventType: 'click',
						autoClose: true,
						saveState: true,
						disableLink: true,
						speed: 'slow'
					});
					
});
</script>
{/literal}
<div class="left-panel">
   <div class="graphite demo-container">
	<ul class="accordion" id="accordion-1">
	   <li><a href="#">{$game_name}</a>
        <ul>
            { if $Numrace >0}
			<li><a href="#">Race</a>
                <ul>
                    <li><a href="#" onclick="ManagerGeneral('{$page_name}?action=list_search&{$SLink}&from={$from}&from_page={$from_page}')">All <span>({$totRec})</span></a></li>
                    {section name=race loop=$RaceArr}
					{if $RaceArr[race].countrow neq '0'}
					 <li><a href="#"
onclick="ManagerGeneral('{$page_name}?action=list_search&dosearch=Go&Rsearchby=ug.race_id&Rsearchvalue={$RaceArr[race].race_id}&from={$from}&{$SLink}&from_page={$from_page}')">{$RaceArr[race].race_title} <span>({$RaceArr[race].countrow})</span></a></li>
					 {/if}
                    {/section}
                </ul>
            </li>
			{/if}
            {if $Numladder >0}
			<li><a href="#">Ladder</a>
                <ul>
                    <li><a href="#" onclick="ManagerGeneral('{$page_name}?action=list_search&{$SLink}&from={$from}&from_page={$from_page}')">All <span>({$totRec})</span></a></li>
					{section name=ladder loop=$LadderArr}
					 {if $LadderArr[ladder].countrow neq '0'}
                     <li>
					 <a href="#"
onclick="ManagerGeneral('{$page_name}?action=list_search&dosearch=Go&Lsearchby=ug.ladder_id&Lsearchvalue={$LadderArr[ladder].ladder_id}&{$SLink}&from={$from}&from_page={$from_page}')">{$LadderArr[ladder].ladder_name}<span>({$LadderArr[ladder].countrow})</span></a></li>
					 {/if}
                    {/section}
                </ul>
            </li>
			{/if}
			{if $Numserver >0}
			<li><a href="#" >Server</a>
                <ul>
                    <li><a href="#" onclick="ManagerGeneral('{$page_name}?action=list_search&{$SLink}&from={$from}&from_page={$from_page}')">All <span>({$totRec})</span></a></li>
                    {section name=server loop=$ServerArr}
					 {if $ServerArr[server].countrow neq '0'}
					 <li><a href="#"
onclick="ManagerGeneral('{$page_name}?action=list_search&dosearch=Go&Ssearchby=ug.server_id&Ssearchvalue={$ServerArr[server].server_id}&{$SLink}&from={$from}&from_page={$from_page}')">{$ServerArr[server].server_name} <span>({$ServerArr[server].countrow})</span></a></li>
					 {/if}
                    {/section}
                </ul>
            </li>
			{/if}
			{if $Numlanguage >0}
            <li><a href="#">Language</a>
            	<ul>
                    <li><a href="#" onclick="ManagerGeneral('{$page_name}?action=list_search&{$SLink}&from={$from}&from_page={$from_page}')">All <span>({$totRec})</span></a></li>
				  {section name=lang loop=$LangArr}
				    {if $LangArr[lang].countrow neq '0'}	
                    <li><a href="#"
onclick="ManagerGeneral('{$page_name}?action=list_search&dosearch=Go&Langby=u.language_ids&Langvalue={$LangArr[lang].language_id}&{$SLink}&from={$from}&from_page={$from_page}')"					
					>{$LangArr[lang].language_name} <span>({$LangArr[lang].countrow})</span></a></li>
                    {/if}
                  {/section}
				</ul>
            </li>
			{/if}
        	<!--li><a href="#">Rate</a>
                <ul>
                    <li><a href="#">All <span>(20)</span></a></li>
                    <li><a href="#">$10-$19 <span>(15)</span></a></li>
                    <li><a href="#">$20-$29 <span>(10)</span></a></li>
                </ul>
            </li-->
            <li><a href="#">Rating</a>
                <ul>
                    <li><a href="#" onclick="ManagerGeneral('{$page_name}?action=list_search&{$SLink}&from={$from}&from_page={$from_page}')">All <span>({$totRec})</span></a></li>
                    {section name=rating loop=$StarArr}
					<li><a href="#"
onclick="ManagerGeneral('{$page_name}?action=list_search&dosearch=Go&Stsearchby=ur.overall_rating&Starvalue={$StarArr[rating].star}&{$SLink}&from={$from}&from_page={$from_page}')"					
					>{$StarArr[rating].star} Star <span>({$StarArr[rating].countrow})</span></a></li>
					{/section}
                </ul>
            </li>
        
        </ul>
	   </li>
       
    </ul>
	  
   </div>   
   </div>
 <div class="right-panel">
      <div class="findcoach-title"><h1 title="Find Coach"></h1></div>
      <div class="searchpanel">
         <form action="findcoach.php" method="post">
              <fieldset>
              <input name="" class="search-bttn" value="" type="image" />
			   <select name="game_id" id="game_id">
				{html_options options=$GameArr selected=$game_id}
			   </select>
               
               </fieldset>
            </form>
           <div class="clear"></div>
      </div>
      <div class="clear"></div>
      <div class="sortby">
              <form action="" method="post">
			   <fieldset>
               <label>Sort by:</label>
               <select for="sirt" name="sort_by" onchange="ManagerGeneral('{$page_name}?action=list_order&OrderType='+this.value+'&{$SearchLink}&from={$from}&from_page={$from_page}')">
                  <option value="ASC" {if $OrderType eq 'ASC'} selected="selected" {/if}>Rating (Low)</option>
				  <option value="DESC" {if $OrderType eq 'DESC'} selected="selected" {/if}>Rating (High)</option>
               </select>
               </fieldset>
             </form>
      </div>
      <div class="clear"></div>
 

   {if $SearchTxt neq ''}<div><b>Search By</b>  {$SearchTxt}</div>{/if}
	 {if $NumCoach >0}
	  {section name=coachRow loop=$CoachArr}
      <div class="game-list">
         <h1>{$CoachArr[coachRow].name}</h1>
		  {if $CoachArr[coachRow].photo neq ''}
		    <div class="game-image">
		     <img src="uploaded/user_images/thumbs/big_{$CoachArr[coachRow].photo}" alt="Coach" border="0"  />
			</div>
			{else}
			 <div style="width:198px; padding:5px; float:left; display:block;">&nbsp;</div> 
		  {/if}  
		  
		  <div class="game-details">
            <h4>About</h4>
            <p>{$CoachArr[coachRow].about}</p>
            <p class="clear"></p>
            <ul class="games-desc">
               <li><span>Race :</span> {$CoachArr[coachRow].race}</li>
               <li><span>Language :</span> {$CoachArr[coachRow].language}</li>
               <!--li><span>Rate :</span> $10</li-->
               <li><span>Experience :</span> {$CoachArr[coachRow].experience}</li>
            </ul>
            <ul class="games-desc">
               <li><span>Ladder :</span> {$CoachArr[coachRow].ladder}</li>
               <li><span>Server :</span> {$CoachArr[coachRow].server} </li>
               <li><span>Rating :</span> {$CoachArr[coachRow].star}</li>
               
            </ul>
            <div class="clear"></div>
            <div class="view"><a href="coachdetails.php?coach_id={$CoachArr[coachRow].user_id}">view</a></div>
         </div>
         <div class="clear"></div>
      </div>
	 {/section} 
     {if $pagination_arr[1]}
	 <div class="clear"></div>
      <div class="pagin">
         <ul>
           {$pagination_arr[1]}
         </ul>
      </div>
	 {/if}
	 {else}
	    No Coach is found......
     {/if}
	</div> 
   </div>
