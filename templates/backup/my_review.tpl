{literal}
<link href="style/tab.css" rel="stylesheet" type="text/css" />
{/literal}   
   <div id="paging">
   <div class="right-panel">
      <div class="review-title"><h1 title="Review"></h1></div>
      <div class="clear"></div>
      <div class="message-list">
	     <div class="container">
           <ul class="tabs">
		   {if $smarty.session.user_type eq '1'}
				<li class="active"><a href="#">My Reviews</a></li>
				<li><a href="my_video_review.php">My Video Reviews</a></li>
		   {/if}		
				<li><a href="coach_review.php">Coach Reviews by me</a></li>
				<li><a href="video_review.php">Video Reviews by me</a></li>
		
			</ul>
			 <div class="clear"></div>
			 
			 {if $NummyReview>0}
				<div class="tab_container">
					<div id="tab1" class="tab_content" style="margin-top:20px;">
					
					 {section name=myrevrow loop=$myReviewArr}
						<div class="message-block">
						 <h3><a href="{$myReviewArr[myrevrow].link}">{$myReviewArr[myrevrow].name}</a></h3>
						 {if $myReviewArr[myrevrow].photo neq ''}
						 <div class="message-img"><img src="uploaded/user_images/thumbs/small_{$myReviewArr[myrevrow].photo}" border="0" /></div>
						 {/if}
						 <div {if $myReviewArr[myrevrow].photo neq ''}class="message-desc-right"{else}class="message-desc"{/if}>
						     <p>{$myReviewArr[myrevrow].review_comment|nl2br}</p>
							 <div class="gamerating" style="float:right; padding-right:22px;">{$myReviewArr[myrevrow].rating}</div>
						     <p class="post">
							 <span>Reviewer</span> : {$myReviewArr[myrevrow].user_type}<br  />
							 <span>Posted on</span> : {$myReviewArr[myrevrow].review_date}</p>
                         </div>
						 <div class="clear"></div>
					  </div>
					 {/section}
					 
					</div>
				</div>
				{if $pagination_arr[1]}
				 <div class="clear"></div>
				  <div class="pagin">
					 <ul>
					   {$pagination_arr[1]}
					 </ul>
				  </div>
				 {/if}
			  {else}
					  No record found
			  {/if} 
			</div>  	
         </div>
      </div>
      <div class="clear"></div>
   </div>