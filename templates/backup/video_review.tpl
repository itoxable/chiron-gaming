{literal}
<link href="style/tab.css" rel="stylesheet" type="text/css" />
{/literal}   
  <div id="paging"> 
   <div class="right-panel">
      <div class="review-title"><h1 title="Review"></h1></div>
      <div class="clear"></div>
     
      <div class="message-list">
	     <div class="container">
           <ul class="tabs">
		    {if $smarty.session.user_type eq '1'}
				<li><a href="my_review.php">My Reviews</a></li>
				<li><a href="my_video_review.php">My Video Reviews</a></li>
			{/if}	
				<li><a href="coach_review.php">Coach Reviews by me</a></li>
				<li  class="active"><a href="#">Video Reviews by me</a></li>
		
			</ul>
			 <div class="clear"></div>
			 {if $Numvdoreview > 0}
				<div class="tab_container">
					<div id="tab4" class="tab_content">
					   {section name=vdorow loop=$reviewvdoArr}
					     <div class="message-block">
							  <h3><a href="videodetails.php?video_id={$reviewvdoArr[vdorow].video_id}">{$reviewvdoArr[vdorow].video_title}</a></h3>
							  
							  <div class="message-img"><img src="uploaded/video_images/thumbs/{$reviewvdoArr[vdorow].video_image}" width="57" height="50" border="0" /></div>
							 <div class="message-desc-right">
							  <p>{$reviewvdoArr[vdorow].review_comment|nl2br}</p>
							  <div class="gamerating" style="float:right; padding-right:22px;">{$reviewvdoArr[vdorow].rating}</div>
							  <p class="post">
							   <span>Video by</span> : <a href="coachdetails.php?coach_id={$reviewvdoArr[vdorow].user_id}">{$reviewvdoArr[vdorow].name}</a><br />
							   <span>Posted on</span> : {$reviewvdoArr[vdorow].review_date}
							  </p>
							 </div>
						 <div class="reply">
						 <a href="#" 
						 onclick="ConfirmDelete('{$page_name}?action=del','{$reviewvdoArr[vdorow].video_review_id}','{$reviewvdoArr[vdorow].name}','Your review for:: ')">
						 Delete</a></div>
						<div class="clear"></div>
							
                         </div>
					   {/section}
					 </div>
				</div>
			 {if $pagination_arr[1]}
				 <div class="clear"></div>
				  <div class="pagin">
					 <ul>
					   {$pagination_arr[1]}
					 </ul>
				  </div>
				 {/if}
			 {else}
					  No record found
			{/if} 		
         </div>
      </div>
      <div class="clear"></div>
   </div>
 </div>  