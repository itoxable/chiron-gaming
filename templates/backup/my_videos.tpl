{literal}
<!--link media="screen" rel="stylesheet" href="style/alert.css" />
<link media="screen" rel="stylesheet" href="style/colorbox.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script src="js/jquery.colorbox.js"></script>
<script>
		$(document).ready(function(){
		$(".example5").colorbox();
		$(".example6").colorbox({iframe:true, innerWidth:425, innerHeight:344});
       });
</script-->		
{/literal}
<div id="paging">  
  <div class="right-panel">
      <h1 title="Games"><span>My Videos</span></h1>
      <div class="clear"></div>
	  <p class="button-flex" style='padding-right:16px'><a href="my_video_update.php"><span>Add Video</span></a></p>
	 {if $Numcoachvdo >0}
	  {section name=vdoRow loop=$CoachvdoArr}
      <div class="game-list">
	  <div class="game-image1">
	  {if $CoachvdoArr[vdoRow].is_video_url eq 'Y'}
	  <a class='example6' href="{$CoachvdoArr[vdoRow].video_url}rel=0&amp;wmode=transparent" title="{$CoachgameArr[vdoRow].video_title}">
	  {else}
	   <a class='example5' href="play_video.php?video_id={$CoachvdoArr[vdoRow].video_id}" title="{$CoachgameArr[vdoRow].video_title}">
	  {/if}
	  <img src="uploaded/video_images/thumbs/mid_{$CoachvdoArr[vdoRow].video_image }" alt="Video" border="0" />
	  </a>
	  </div>
       <div class="game-details">
	        <h4>{$CoachvdoArr[vdoRow].video_title}</h4>
			 <ul class="games-desc" style="padding-top:0px;">
               <li><span>Game :</span> {$CoachvdoArr[vdoRow].game}</li>
			   {if  $CoachvdoArr[vdoRow].ladder_id neq '' || $CoachvdoArr[vdoRow].ladder_id neq 0}
               <li><span>Ladder :</span> {$CoachvdoArr[vdoRow].ladder} </li>
			   {/if}
			    {if  $CoachvdoArr[vdoRow].team_id neq '' || $CoachvdoArr[vdoRow].team_id neq 0}
               <li><span>Team :</span> {$CoachvdoArr[vdoRow].team} </li>
			   {/if}
			    {if  $CoachvdoArr[vdoRow].map_id neq '' || $CoachvdoArr[vdoRow].map_id neq 0}
               <li><span>Map :</span> {$CoachvdoArr[vdoRow].map } </li>
			   {/if}
			    {if  $CoachvdoArr[vdoRow].class_id neq '' || $CoachvdoArr[vdoRow].class_id neq 0}
               <li><span>Class :</span> {$CoachvdoArr[vdoRow].class} </li>
			   {/if}			  
            </ul>
            <ul class="games-desc" style="padding-top:0px;">
               <!--<li><span>Race :</span> {$CoachvdoArr[vdoRow].race}</li>-->
			   <li><span>Time Length :</span> {$CoachvdoArr[vdoRow].time_length}</li>
			   {if  $CoachvdoArr[vdoRow].versus_id neq '' || $CoachvdoArr[vdoRow].versus_id neq 0}
               <li><span>Versus :</span> {$CoachvdoArr[vdoRow].versus} </li>
			   {/if}
			    {if  $CoachvdoArr[vdoRow].type_id neq '' || $CoachvdoArr[vdoRow].type_id neq 0}
               <li><span>Type :</span> {$CoachvdoArr[vdoRow].type} </li>
			   {/if}
			    {if  $CoachvdoArr[vdoRow].champion_id neq '' || $CoachvdoArr[vdoRow].champion_id neq 0}
               <li><span>Champion :</span> {$CoachvdoArr[vdoRow].champion} </li>
			   {/if}
			    {if  $CoachvdoArr[vdoRow].mode_id neq '' || $CoachvdoArr[vdoRow].mode_id neq 0}
               <li><span>Mode :</span> {$CoachvdoArr[vdoRow].mode} </li>
			   {/if}
            </ul>
			<p class="clear"></p>
            <p style="padding-top:5px;">{$CoachvdoArr[vdoRow].description}</p>
            <p class="clear"></p>
            <div style="height:10px;"></div>
            <div class="clear"></div>
            <div style="width:100%">
			<div class="view">
			<a href="#" onclick="ConfirmDelete('{$page_name}?action=del','{$CoachvdoArr[vdoRow].video_id}','{$CoachvdoArr[vdoRow].video_title}','your video:: ')">Delete</a></div>
			<div class="view" style="margin-right:5px;"><a href="my_video_update.php?video_id={$CoachvdoArr[vdoRow].video_id}">Edit</a></div></div>
         </div>
         <div class="clear"></div>
      </div>
      {/section}
	  {else}
	    <div class="game-details"> No video found </div>
     {/if}
      <div class="clear"></div>
   </div> 
 </div>    