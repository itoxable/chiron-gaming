{literal}
<link href="style/tab.css" rel="stylesheet" type="text/css" />
{/literal}   
  <div id="paging"> 
   <div class="right-panel">
      <div class="review-title"><h1 title="Review"></h1></div>
      <div class="clear"></div>
     
      <div class="message-list">
	     <div class="container">
           <ul class="tabs">
		   {if $smarty.session.user_type eq '1'}
				<li><a href="my_review.php">My Reviews</a></li>
				<li class="active"><a href="#">My Video Reviews</a></li>
		   {/if}		
				<li><a href="coach_review.php">Coach Reviews by me</a></li>
				<li><a href="video_review.php">Video Reviews by me</a></li>
			</ul>
			 <div class="clear"></div>
			 {if $Numvideo>0}
				<div class="tab_container">
					<div id="tab2" class="tab_content" style="margin-top:20px;">
					 {section name=vdorevrow loop=$videoReviewArr}
						<div class="message-block">
						 <h3><a href="{$videoReviewArr[vdorevrow].link}">{$videoReviewArr[vdorevrow].name}</a></h3>
						 {if $videoReviewArr[vdorevrow].photo neq ''}
						 <div class="message-img"><img src="uploaded/user_images/thumbs/small_{$videoReviewArr[vdorevrow].photo}"  border="0" /></div>
						 {/if}
						 <div {if $videoReviewArr[vdorevrow].photo neq ''}class="message-desc-right"{else}class="message-desc"{/if}>
						     <p>{$videoReviewArr[vdorevrow].review_comment|nl2br}</p>
							 <div class="gamerating" style="float:right; padding-right:22px;">{$videoReviewArr[vdorevrow].rating}</div>
						     <p class="post">
							 <span>Video</span> : <a href="videodetails.php?video_id={$videoReviewArr[vdorevrow].video_id}">{$videoReviewArr[vdorevrow].video_title}</a><br /><br />
							 <span>Reviewer</span> : {$videoReviewArr[vdorevrow].user_type}<br  />
							 <span>Posted on</span> : {$videoReviewArr[vdorevrow].review_date}</p>
                         </div>
						 <div class="clear"></div>
					  </div>
					 {/section}
					</div>
				</div>
				{if $pagination_arr[1]}
				 <div class="clear"></div>
				  <div class="pagin">
					 <ul>
					   {$pagination_arr[1]}
					 </ul>
				  </div>
				 {/if}
			  {else}
					  No record found
			  {/if} 
         </div>
      </div>
      <div class="clear"></div>
   </div>
 </div>  