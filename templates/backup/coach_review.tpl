{literal}
<link href="style/tab.css" rel="stylesheet" type="text/css" />

{/literal}   
   <div id="paging">  
   <div class="right-panel">
      <div class="review-title"><h1 title="Review"></h1></div>
      <div class="clear"></div>
      <div class="message-list">
	     <div class="container">
           <ul class="tabs">
		    {if $smarty.session.user_type eq '1'}
				<li><a href="my_review.php">My Reviews</a></li>
				<li><a href="my_video_review.php">My Video Reviews</a></li>
		    {/if}		
				<li class="active"><a href="#">Coach Reviews by me</a></li>
				<li><a href="video_review.php">Video Reviews by me</a></li>
		
			</ul>
			 <div class="clear"></div>
			 {if $NumotherReview>0}
				<div class="tab_container">
					<div id="tab3" class="tab_content" style="margin-top:20px;">
					 {section name=revrow loop=$reviewotherArr}
						<div class="message-block">
						 <h3><a href="{$reviewotherArr[revrow].link}">{$reviewotherArr[revrow].name}</a></h3>
						 {if $reviewotherArr[vdorevrow].photo neq ''}
						 <div class="message-img"><img src="uploaded/user_images/thumbs/small_{$reviewotherArr[revrow].photo}"  border="0" /></div>
						 {/if}
						 <div {if $reviewotherArr[vdorevrow].photo neq ''}class="message-desc-right"{else}class="message-desc"{/if}>
						  <p>{$reviewotherArr[revrow].review_comment|nl2br}</p>
						  <div class="gamerating" style="float:right; padding-right:22px;">{$reviewotherArr[revrow].rating}</div>
						   <p class="post">
							 <span>{$reviewotherArr[revrow].user_type}</span><br  />
							 <span>Posted on</span> : {$reviewotherArr[revrow].review_date}
						  </p>
                         </div>
						 <div class="clear"></div>
						 <div class="reply">
						 <a href="#" 
						 onclick="ConfirmDelete('{$page_name}?action=del','{$reviewotherArr[revrow].user_review_id}','{$reviewotherArr[revrow].name}','Your review for:: ')">
						 Delete</a></div>
						 <div class="clear"></div>
					  </div>
					 {/section}
					</div>
				</div>
			    {if $pagination_arr[1]}
				 <div class="clear"></div>
				  <div class="pagin">
					 <ul>
					   {$pagination_arr[1]}
					 </ul>
				  </div>
				 {/if}
			 {else}
					  No record found
			{/if} 	
         </div>
      </div>
      <div class="clear"></div>
   </div>
 </div>  