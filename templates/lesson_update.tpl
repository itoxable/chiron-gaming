{literal}
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jQuery.functions.pack.js"></script>

<script language="javascript" src="js/contact_us.js"></script> 

<link rel="stylesheet" href="style/autosuggest.css" type="text/css" />
<script type="text/javascript" src="js/jquery.ausu-autosuggest.min.js"></script>

    <script type="text/javascript">

        jQuery(document).ready(function() {
	       
             <!-- AUTOCOMPLETE -->
			 jQuery.fn.autosugguest({  
				   className: 'ausu-suggest',
				  methodType: 'POST',
					minChars: 2,
					  rtnIDs: true,
					dataFile: 'autocomplete.php?action=populateUser'
			}); {/literal}
			{if $Formval.lesson_id neq ''}{literal}
			   CreateProfile();
			{/literal} {/if}
			{literal}  
			 
        });
    </script>
<script language="javascript" type="text/javascript">

function getChangeGame(id)
{
	jQuery.post('game_details.php',{'game_id':id, 'action':'get_content'}, function(data){
		jQuery('#dynamic_controls').html(data);
	});
}
function CheckFields()
{
  if(document.getElementById('student_id').value=='' ) 
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please select a valid student");
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
    return false;
   }
   if(document.getElementById('lesson_price').value=='' ) 
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please enter price");
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
    return false;
   }
   if(document.getElementById('lesson_title').value=='' ) 
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please enter title");
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
    return false;
   }
  return true;
 }  
 function keyCheck(eventObj, obj)
{
    var keyCode
 
    // Check For Browser Type
    if (document.all){
        keyCode=eventObj.keyCode
    }
    else{
        keyCode=eventObj.which
    }
 
    var str=obj.value
 
    if(keyCode==46){
        if (str.indexOf(".")>0){
            return false
        }
    }
 
    if((keyCode<46 || keyCode >57)   &&   (keyCode >31)){ // Allow only integers and decimal points
        return false
    }
    
    return true
}
function CreateProfile()
{
    var studentid = jQuery('#student_id').val();
	var name = jQuery('#student').val();
	if(studentid !='')
	{
	   jQuery.ajax({
	   type: "POST",
	   url: 'autocomplete.php?action=populateInfo&studentid='+studentid+'&name='+name ,
	   dataType : 'html',
	   success:function(response)
	   {
	    if(response == 2)
		{
		   jQuery('#profile').html('<span style="padding-left:150px; color:#ff0000">Invalid User</span>');
		   document.getElementById('student_id').value='';
		}
		else{   
	      jQuery('#profile').html(response);
		}  
	   }
	  }) 	 
	}
	/*else
	{     
	   var name = jQuery('#student').val();
	   if(name !='')
		{
		   jQuery('#profile').html('<span style="padding-left:150px; color:#ff0000">Invalid User</span>');
		   document.getElementById('student_id').value='';
		 	 
		}
	} */

}

</script>
{/literal}
<div class="content">
   		<!-- tab -->
    	<ul class="tabflex">
        	<li><a href="dashboard.php"><span>Dashboard</span></a></li>
            <li><a href="inbox.php"><span>Message</span></a></li>
			<li><a href="profile.php"><span>Profile</span></a></li>
			{if $is_coach eq '1'}<li><a href="coach_game.php"><span>Coach</span></a></li>{/if}
			{if $is_partner eq '1'}<li><a href="training_partner_game.php"><span>Training Partner</span></a></li>{/if}
			{if $is_coach eq '1'}<li><a href="add_lesson.php"  class="active"><span>Lessons</span></a></li>{/if}
			{if $is_coach eq '0'}<li><a href="my_lesson.php"><span>Lessons</span></a></li>{/if}
			<li><a href="upload_videos.php"><span>Videos</span></a></li>
			{if $is_coach eq '1' or $is_partner eq '1'}<li><a href="my_review.php"><span>Reviews</span></a></li>{/if}
			{if $is_coach eq '0' && $is_partner eq '0'}<li><a href="my_video_review.php"><span>Reviews</span></a></li>{/if}
			
          <!--    <li><a href="#"><span>Students</span></a></li>-->
        </ul>
        <!-- tab -->
        <div class="clear"></div>
        <div class="tabular-content"> 
		 <div class="total-message">&nbsp;&nbsp;</div>
		  <div class="tab_sub">
            	<ul>
                	<li><a href="add_lesson.php" >Add Lesson</a></li>
                    <li><a href="my_lesson.php" class="active">My Lesson</a></li>
                </ul>
            </div>   
		 <div class="clear"></div>                     
         <div>

      <div class="register">
	   <div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:10px;">{if $ermsg neq ''} {$ermsg}{/if} </div>
      <form name="lesson" id="lesson" action="" method="post">
	  <input type="hidden" name="lesson_id" value="{$Formval.lesson_id}" />
         <fieldset>
            <label>User ID :</label><div class="ausu-suggest">
            <input name="student" id="student" type="text" value="{$Formval.student}" class="con-req" onblur="CreateProfile()" autocomplete="off"/>
			<input name="student_id" id="student_id" type="hidden"  value="{$Formval.student_id}" autocomplete="off" />
			</div>
            <!--select name="student_id" id="student_id" class="con-req">
				<option value="">Select</option>
				<!--{html_options options=$StudentArr selected=$Formval.student_id}-->
			<!--/select-->
			<p class="clear"></p>
			<div id="profile">
			</div>
			<p class="clear"></p>
			<label>Price ($):</label>
            <input name="lesson_price" id="lesson_price" type="text" value="{$Formval.lesson_price}" onkeypress="return keyCheck(event, this);" class="con-req" />
			<p class="clear"></p>
			</span>
            <p class="clear"></p>
			<label>Title:</label>
            <input name="lesson_title" id="lesson_title" type="text" value="{$Formval.lesson_title}" class="con-req" />
			<p class="clear"></p>
            <label>Description :</label>
            <textarea  name="lesson_description" id="lesson_description">{$Formval.lesson_description}</textarea>
            <p class="clear"></p>
            <label>&nbsp;</label>
            <input name="submit" class="submit" value="{$SubmitButton}" type="submit" onclick='return CheckFields()' />
            <input name="" class="cancel" value="Cancel" type="button" onclick="window.location.href='add_lesson.php'" />
         </fieldset>
      </form>
      </div>
      
     </div>
     </div>
    </div>