<div id="paging"> 
<div class="right-panel">
      <h1 title="My Coach"><span>My favourite coach</span></h1>
      <div class="searchpanel">
              <form action="" method="post">
			   <fieldset>
               <select for="sirt" name="sort_by" onchange="ManagerGeneral('{$page_name}?action=list_order&OrderType='+this.value+'&from={$from}&from_page={$from_page}')">
                  <option value="ASC" {if $OrderType eq 'ASC'} selected="selected" {/if}>Star Rating (Low)</option>
				  <option value="DESC" {if $OrderType eq 'DESC'} selected="selected" {/if}>Star Rating (High)</option>
               </select>
               </fieldset>
             </form>
      </div>
      <div class="clear"></div>
 

	 {if $NumCoach >0}
	  {section name=coachRow loop=$CoachArr}
      <div class="game-list">
        
		   <div class="game-image">
		   {if $CoachArr[coachRow].photo neq ''}
		     <img src="uploaded/user_images/thumbs/big_{$CoachArr[coachRow].photo}" alt="{$CoachArr[coachRow].name}" border="0" />
		   {else}	
		     <img src="images/coach_thumb.jpg" alt="{$CoachArr[coachRow].name}" border="0" />
		   {/if} 
		   </div>	 
		    
		  
		  <div class="game-details">
             <h2>{$CoachArr[coachRow].name}</h2>
            <p>{$CoachArr[coachRow].about}</p>
             <p class="clear"></p>
			 <ul class="games-desc">
			    <li><span>Star Rating :</span> {$CoachArr[coachRow].star}</li>
			    <li><span>Language :</span> {$CoachArr[coachRow].language}</li>
				{if $CoachArr[coachRow].avail_local eq 'Y'}
			   <li><span>City (Local meet-up) :</span> {if $CoachArr[coachRow].availability_city neq ''}{$CoachArr[coachRow].availability_city}{else}N/A{/if}</li>
			   {/if}
			 </ul>
			 <ul class="games-desc">
			   <li>&nbsp;</li>
			   <li><span>Availability :</span> {$CoachArr[coachRow].availability_type}</li>
			   {if $CoachArr[coachRow].avail_local eq 'Y'}
			   <li><span>Country (Local meet-up) :</span> {if $CoachArr[coachRow].availability_country neq ''}{$CoachArr[coachRow].availability_country}{else}N/A{/if}</li>
			   {/if}
			 </ul>
			 <p class="clear"></p>
			{section name=game_nm loop=$CoachgameArr[coachRow]} 
			<!--div style="font-size:14px; padding-top:8px;"><span>Game :</span> {$CoachgameArr[coachRow][game_nm].game}</div-->
            <ul class="games-desc">
			   <li><label class="gametype" style="width:80%; font-size:14px; padding-bottom:3px;">{$CoachgameArr[coachRow][game_nm].game}</label></li>
			   <li><span>Rate :</span> {if $CoachgameArr[coachRow][game_nm].rate neq ''}${$CoachgameArr[coachRow][game_nm].rate}{else}N/A{/if}</li>
			   {if $CoachgameArr[coachRow][game_nm].is_ladder eq 'Y'}
			   <li><span>Ladder :</span> {if $CoachgameArr[coachRow][game_nm].ladder neq ''}{$CoachgameArr[coachRow][game_nm].ladder}{else}N/A{/if}</li>
			   {/if}
			   {if $CoachgameArr[coachRow][game_nm].is_server eq 'Y'}
			   <li><span>Server :</span> {if $CoachgameArr[coachRow][game_nm].server neq ''}{$CoachgameArr[coachRow][game_nm].server}{else}N/A{/if} </li>
			   {/if}
               {if $CoachgameArr[coachRow][game_nm].is_region eq 'Y'}
			   <li><span>Region :</span> {if $CoachgameArr[coachRow][game_nm].region neq ''}{$CoachgameArr[coachRow][game_nm].region}{else}N/A{/if} </li>
			   {/if}
            </ul>
            <ul class="games-desc">
			   <li style="padding-bottom:3px;">&nbsp;</li>
			   <li style="padding-bottom:3px;">&nbsp;</li>
			   {if $CoachgameArr[coachRow][game_nm].is_race eq 'Y'}
               <li><span>Race :</span> {if $CoachgameArr[coachRow][game_nm].race neq ''}{$CoachgameArr[coachRow][game_nm].race}{else}N/A{/if}</li>
			   <li>&nbsp;</li>
			   {/if}
			   {if $CoachgameArr[coachRow][game_nm].is_rating eq 'Y'}
			   <li><span>Rating :</span> {if $CoachgameArr[coachRow][game_nm].rating neq ''}{$CoachgameArr[coachRow][game_nm].rating}{else}N/A{/if} </li>
			   {else}
			   {/if}
            </ul>
			<div class="clear" style="padding-top:0px;"></div>
			<p style="padding-top:0px; padding-left:2px;">
			<b>Experience</b> : {if $CoachgameArr[coachRow][game_nm].experience neq ''}{$CoachgameArr[coachRow][game_nm].experience}{else}N/A{/if}</p>
			{/section}
            <div class="clear"></div>
			<div style="width:100%; padding-left:20px;"><div class="button-flex button150">
			<a href="#" onclick="ConfirmDelete('{$page_name}?action=del','{$CoachArr[coachRow].fav_coach_id}','{$CoachArr[coachRow].name}','Your favourite coach:: ')">
			<span>Delete From Favourite</span></a></div>
            <div class="view" style="margin-right:5px;"><a href="coachdetails.php?coach_id={$CoachArr[coachRow].coach_id}">view</a></div></div>
         </div>
         <div class="clear"></div>
      </div>
	 {/section} 
     {if $pagination_arr[1]}
	 <div class="clear"></div>
      <div class="pagin">
         <ul>
           {$pagination_arr[1]}
         </ul>
      </div>
	 {/if}
	 {else}
	    No Coach is found......
     {/if}
	</div>
</div>	