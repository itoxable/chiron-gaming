{literal}
<script language="javascript">

function submitSettings(){
    document.getElementById("editSettingsForm").submit();
}
    
</script>
{/literal}

<div align="right">
    {if $is_coach eq '0'}
    <!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
    <a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
    {/if}
    {if $is_partner eq '0'}
    <a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
    <!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
    {/if} 
</div>
<div class="content">
   	<!-- tab -->
    <ul class="tabflex">
       	<li><a href="schedule.php"><span>Schedule</span></a></li>
        <li><a href="messages.php"><span>Messages</span></a></li>
        <li><a href="profile.php" class="active"><span>Profile</span></a></li>
        {if $is_coach eq '1'}<li><a href="coach_game.php"><span>Coach</span></a></li>{/if}
        {if $is_partner eq '1'}<li><a href="training_partner_game.php"><span>Training Partner</span></a></li>{/if}
        <li><a href="cashier.php"><span>Cashier</a></li>
         
    </ul>
    <!-- tab -->
    <div class="clear"></div>
    <div class="tabular-content"> 
        <div class="total-message">&nbsp;&nbsp;</div>
        <div class="clear"></div>                     
        <div>
            <div class="" style="margin: 40px">
                <div style="padding-bottom:10px; font-size: 16px; font-weight: bold">E-mail settings</div>
                
                <form name="editSettingsForm" id="editSettingsForm" enctype="multipart/form-data" method='post'>
                    <input type="hidden" name="submitForm" value="submitForm" />
                    
                    <table cellspacing="5" cellpadding="5" style="margin-bottom: 15px">
                            <tr>
                                <td><label>Receive Mail when somebody makes a schedule</label></td>
                                <td><input type="checkbox"{if $Formval.mail_on_schedule eq '1'}  checked="checked"{/if}  name="mail_on_schedule" value="1"></td>
                            </tr>
                            <tr>
                                <td><label>Receive Mail when somebody cancels a schedule</label></td>
                                <td><input type="checkbox" {if $Formval.mail_on_schedule_cancelation eq '1'}  checked="checked"{/if} name="mail_on_schedule_cancelation" value="1"></td>
                            </tr>
<!--                            <tr>
                                <td><label>Receive Mail half and hour before a schedule</label></td>
                                <td><input type="checkbox" {if $Formval.mail_on_hour_for_schedule eq '1'}  checked="checked"{/if} name="mail_on_hour_for_schedule" value="1"></td>
                            </tr>
                            <tr>
                                <td><label>Receive Mail when somebody sebds you a message</label></td>
                                <td><input type="checkbox" {if $Formval.mail_on_message eq '1'}  checked="checked"{/if} name="mail_on_message" value="1"></td>
                            </tr>-->
                            
                        </table>
                        
                        <a class="button" href="javascript:;" style="" onclick="return submitSettings()">Update</a>
                        <a class="button" href="profile.php" style="" onclick="">Cancel</a>
                    
		</form>
            </div>
	
      
        </div>
    </div>
</div>
<script language="javascript">
       changePageTitle('Settings');
</script>