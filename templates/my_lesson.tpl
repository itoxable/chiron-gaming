<head>
<script language="javascript1.2" src="includejs/functions_admin.js" type="text/javascript"></script>
</head>
{literal}

<script type="text/javascript">
jQuery(document).ready(function(){
		
focus_blur('Search_lesson_title','Search by title');
focus_blur('Search_from_date','From');
focus_blur('Search_to_date','To');
focus_blur('Search_from_date_in','From');
focus_blur('Search_to_date_in','To');
});

function LessonComplete(id){
	//window.location.href="check_lesson.php?action=complete&lesson_id="+id;
	var r=confirm("Are You Sure that lesson has been completed?");
	if (r==true)
	{
		window.location.href="my_lesson.php?action=complete&lesson_id="+id;
		return true;
	}
	else{
		return false;
	}

}

function ChangeStatus(id){

window.location.href="lesson_payment.php?lesson_id="+id;
}
function SearchLesson(formID){

	var frmID='#'+formID;

	var params ={
	
		'module': 'contact'
	};

	var paramsObj = jQuery(frmID).serializeArray();

	jQuery.each(paramsObj, function(i, field){

		params[field.name] = field.value;

	});
	
	jQuery.ajax({
		type: "POST",
		url:  'check_lesson.php?action=list_search&IsProcess=Y',
		data:  params,
		dataType : 'html',
		success: function(data){
			//alert(data);
			jQuery('#show').html(data);
        }
      });
}
</script>
{/literal}
<div id="show">
		
	  <div class="clear"></div>
<!--<div align="right"><p class="button-flex-big button90"><a href="my_lesson_update.php"><span>Add Lesson</span></a></p></div>-->
		<div class="content">
   		
		{* Start loading Tabs *}
		{include file='user_tabs.tpl' active_tab='lessons'}
		{* End loading Tabs *}
		
        <div class="clear"></div>
        <div class="tabular-content"> 
		 <div class="total-message"><strong>Total Lessons : {$Numcoachgame}</strong>&nbsp;&nbsp;</div>       
		      <div class="tab_sub">
            	<ul>
                	{if $is_coach eq '1'}<li><a href="add_lesson.php"  class="active" >Add Lesson</a></li>{/if}
                    <li><a href="javascript:;">My Lesson</a></li>
                </ul>
            </div>                   
            <div class="clear"></div>    
			<div class="dashboard_area">
		   <div class="dashboard_block new-income">
		   <h2>General Search</h2>
			<form method="POST" name="form_search" id="form_search" action="my_lesson.php" onsubmit="return false;" style="padding:10px;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="new">
				<tr>
				  <td width="25%" align="left" valign="middle"><input size="30" type="text" name="Search_lesson_title" id="Search_lesson_title" value="{$Search_lesson_title}" class="textbox search-box"/></td>
				
					<td width="75%" align="left" valign="middle" style="color:#000000; padding-left:5px;">
					Select Coach:&nbsp;
					<select name="Search_coach_id" id="Search_coach_id">
						<option value="">--Select--</option>
						{section name=Rows loop=$SelectCoachArr}
							<option value="{$SelectCoachArr[Rows].coach_id}" {if $SelectCoachArr[Rows].coach_id eq $Search_coach_id} selected {/if}>{$SelectCoachArr[Rows].coach_name}</option>
						{/section}
					</select>
				  </td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
				<td colspan="3" width="45%" align="left" valign="middle">
					<div style="float:left;">
						<input type="text" name="Search_from_date" id="Search_from_date" class="datebox" value="{$Search_from_date}" style="width:80px;" readonly=""/><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_from_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
					</div>
					<div style="float:left; padding-left:70px;">
						<input type="text" name="Search_to_date" id="Search_to_date" class="datebox" value="{$Search_to_date}" style="width:80px;" readonly=""/><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_to_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a></div>
				 </td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr><td colspan="3" align="left" valign="middle" style="color:#000000; padding-left:5px;">
					Select Status:&nbsp;
					<select name="Search_lesson_status" id="Search_lesson_status">
						<option value="">--Select--</option>
						{section name=Rows loop=$SelectStatusArr}
							<option value="{$SelectStatusArr[Rows].lesson_status_id}" {if $SelectStatusArr[Rows].lesson_status_id eq $Search_lesson_status} selected {/if}>{$SelectStatusArr[Rows].lesson_status_title}</option>
						{/section}
					</select></td>
				</tr>	
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>	
					<td valign="middle" colspan="3">
					<!--<input type="hidden" name="dosearch" value="GO">-->
					<input class="go"  name="submitdosearch" value="GO" type="button" onClick="SearchLesson('form_search')" />
					<input class="reset" name="" type="button" value="RESET" onClick="window.location.href='my_lesson.php';"/></td>
				</tr>
		  </table>
	</form>
	</div>
	<div class="clear"></div>
	  </div>                 
            <div>
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row tabular-data">
				{if $Numcoachgame neq 0}
				  <tr style="border-bottom:#000000 1px solid; background:#ebebeb">
				  	<th width="10%" align="left">Title</th>
					<th width="40%" align="left">Description</th>
				  	<th width="10%" align="left">Coach</th>
					<th width="10%" align="center">Price</th>
					<th width="5%" align="center">Status</th>
					<th width="5%" align="center">Date</th>
					<th width="20%" align="center">Action</th>
				  </tr>
				  <tr>
				  	<th width="10%" align="center">&nbsp;</th>
					<th width="40%" align="center">&nbsp;</th>
				  	<th width="10%" align="center">&nbsp;</th>
					<th width="10%" align="center">&nbsp;</th>
					<th width="5%" align="center">&nbsp;</th>
					<th width="5%" align="center">&nbsp;</th>
					<th width="20%" align="center">&nbsp;</th>
				  </tr>
				  {section name=gameRow loop=$CoachgameArr}	  				
				  <tr>
				  	<td width="10%" align="left">{$CoachgameArr[gameRow].lesson_title}</td>
					<td width="40%" align="left">{$CoachgameArr[gameRow].lesson_description|truncate:130:"..."}</td>
				  	<td width="10%" align="left">{if $CoachgameArr[gameRow].coach_name neq ''}{$CoachgameArr[gameRow].coach_name}{else}N/A{/if}</td>
					<td width="10%" align="center">{if $CoachgameArr[gameRow].lesson_price}${$CoachgameArr[gameRow].lesson_price}{else}N/A{/if}</td>
					<td width="5%" align="center">{if $CoachgameArr[gameRow].lesson_status}{$CoachgameArr[gameRow].lesson_status}{else}N/A{/if}</td>
					<td width="5%" align="center">{if $CoachgameArr[gameRow].date_added neq '0000-00-00'}{$CoachgameArr[gameRow].date_added|date_format:"%D"}{else}N/A{/if}</td>
					<td width="20%" align="center">
						<table>
							<tr>
								<td>
								
								{if $CoachgameArr[gameRow].lesson_status_id eq '1'}
								<div class="view"><a href="#" onclick="ChangeStatus('{$CoachgameArr[gameRow].lesson_id}')">
								Pay</a></div>
								{elseif $CoachgameArr[gameRow].lesson_status_id neq '3' && $CoachgameArr[gameRow].lesson_status_id neq '4' && $CoachgameArr[gameRow].lesson_status_id neq '5'}
								<div class="view-a"><a href="#" onclick="LessonComplete('{$CoachgameArr[gameRow].lesson_id}')">
								Lesson Completed</a></div>
								{else}
								{/if}
								</td>
								<td>
								<div class="view" style="margin-right:5px;"><a href="check_lesson_update.php?lesson_id={$CoachgameArr[gameRow].lesson_id}">View</a></div>
								</td>
							</tr>
						</table>
					</td>
				  </tr>
	  			  {/section} 
				{else}
				  <tr>
                    <td colspan="6"><center><strong>No Record Exists</strong></center></td>
                  </tr>		
				{/if}                                
                </table>
            </div>
        	<div class="clear"></div>
            
        </div>
    </div>
</div>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>
