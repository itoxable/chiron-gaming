   <div id="paging">  
	<div class="content">
   		<!-- tab -->
    	<ul class="tabflex">
        	<li><a href="#" class="active"><span>Reviews To</span></a></li>
            <li><a href="coach_review.php"><span>Sent Reviews</span></a></li>
            
        </ul>
        <!-- tab -->
        <div class="clear"></div>
        <div class="tabular-content">
            <div class="tab_sub">
            	<ul>
                	<li><a href="#" class="active">Me</a></li>
                    <li><a href="my_video_review.php">My Videos</a></li>
					<!--li><a href="#">Coach Reviews by me</a></li-->
					<!--li><a href="#">Video Reviews by me</a></li-->
                </ul>
            </div>
			
           
            <!--div class="tab_pagination">
            	<span class="page-records"><strong>1 - 20 of 60</strong></span> <span class="left-arr"><a href="#"></a></span><span class="right-arr"><a href="#"></a></span>
            </div-->   
            <div class="clear"></div>  
			{if $NummyReview>0}                   
            <div>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row">
				{section name=myrevrow loop=$myReviewArr}
                  <tr {if $smarty.section.myrevrow.index%2 neq 0}class="alter"{/if}>
                    <td width="50">
					{if $myReviewArr[myrevrow].photo neq ''}
					<img src="uploaded/user_images/thumbs/{$myReviewArr[myrevrow].photo}" border="0" />
					{else}
					<img src="images/avatar_smallest.jpg" border="0" />
					{/if}</td>
                    <td width="200" valign="top"><strong><a href="{$myReviewArr[myrevrow].link}">{$myReviewArr[myrevrow].name}</a></strong><br />
					<i>{$myReviewArr[myrevrow].user_type}</i>
					</td>
                    <td width="100" valign="top" style="padding-right:10px;"><strong>{$myReviewArr[myrevrow].review_date}</strong></td>
                    <td valign="top">{$myReviewArr[myrevrow].review_comment|nl2br}</td>
                    <td width="100" align="right" valign="top"><strong>{$myReviewArr[myrevrow].rating}</strong></td>
                    <td width="20"><!--a href="#"><img src="images/close_small.gif" alt="" border="0" /></a--></td>
                  </tr>
                 {/section}                          
                </table>
				 
            </div>
        	<div class="clear"></div>
            <br />
            <div class="tab_sub">
            	&nbsp;
            </div>
            <div class="tab_search">
            	&nbsp;
            </div>
            <div class="tab_pagination">
            	{if $pagination_arr[1]}
				 <div class="clear"></div>
				  <div class="pagin">
					 <ul>
					   {$pagination_arr[1]}
					 </ul>
				  </div>
				 {/if}
            </div>   
            <div class="clear"></div> 
			{else}
				<div align="left" style="padding:10px;">No record found</div>
			{/if}
      </div>
   </div>
</div>