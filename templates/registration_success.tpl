<div class="right-panel">
      <h1 title="Profile"><span>Registration Confirmation</span></h1>
      <div class="clear"></div>
	  
      <div>	  
	  You have successfully registered. Now you can access your account by using email address and password . 
	  Please check your email for detail information.
	  </div><br />
	
	  {if !empty($error_forum)}
	  <div style="color:#FF0000;"><h3>Forum Registration Error</h3>{$error_forum}</div>
	  {/if}
	  {if !empty($forum_username)}
	  <div style="color:#00CC33;"><h3>Forum Registration Status</h3><br />
	  Your registration is successful in our <a href="{$site_url}/forum/"><b>FORUM</b></a><br />
	Your forum username is : <strong>{$forum_username}</strong> and forum password is same as your site login password.<br />
	Please check your email for detail information.
		</div>
	  {/if}
     <div class="clear"></div>
	 <div style="text-align:center;margin-top:75px;">
	 <img src="images/loading_wait.gif" alt=""/>
	 <p>Please wait... </p>
	 <p>We are redirecting your dashboard.</p>	 
	 
	 </div>
</div>
{literal}
<script>
	showAlert();
	 	
		function showAlert()
		{
			setTimeout("document.location.href='dashboard.php'",3000)
		}
	 </script>
	 {/literal}