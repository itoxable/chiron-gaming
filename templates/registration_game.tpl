{literal}
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jQuery.functions.pack.js"></script>
<link rel="stylesheet" href="style/autosuggest.css" type="text/css" />
<script type="text/javascript" src="js/jquery.ausu-autosuggest.min.js"></script>
<script language="javascript" type="text/javascript">
jQuery(document).ready(function(){
	getPageId('{/literal}{jQueryFormval.game_id}{literal}','{/literal}{$Formval.ladder_id}{literal}','{/literal}{$Formval.race_id}{literal}','{/literal}{$Formval.server_id}{literal}');

   jQuery.fn.autosugguest({  
           className: 'ausu-suggest',
          methodType: 'POST',
            minChars: 2,
              rtnIDs: true,
            dataFile: 'autocomplete.php?action=populateCity'
    });

});

function getPageId(type,id,rid,sid)
{	

	jQuery.ajax({
			type:'POST',
			url:'get_game_info.php',
			data: 'type='+type+'&id='+id+'&rid='+rid+'&sid='+sid,
			dataType:'json',
			success:function(jData){
			
				if(jData.flag == 1)
				{
					jQuery('#ladder_id_td').html(jData.html);
					jQuery('#race_id_td').html(jData.html1);
					jQuery('#server_id_td').html(jData.html2);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				jQuery(".contactus-block").html("Some server error occoured. Please report to system administrator.</div>");
			}
		});
}
function CheckFields()
{
 
    if(document.getElementById('user_type').value==1)	
	{ 
	   if(!jQuery("#paypal_email_id").emailCheck() || document.getElementById('paypal_email_id').value=='' ) 
		   {
				jQuery('#FormErrorMsg').show();
				jQuery('#FormErrorMsg').html("Please Enter a Valid paypal email");
				jQuery("#paypal_email_id").attr('style','border:1px solid #FF0000');
				return false;
		   }
      }
      if(document.getElementById('avail_L').checked == true)
     {
		  if(document.getElementById('availability_city').value=='')
		  {
			jQuery('#FormErrorMsg').show();
			jQuery('#FormErrorMag').html("Please enter availability city");
			jQuery('#availability_city').attr('style','border:1px solid #FF0000');
			return false; 
		  }	
		 
      }	  
	  if(document.getElementById('game_id').value=='' ) 
	  {
		jQuery('#FormErrorMsg').show();
		jQuery('#FormErrorMsg').html("Please select a game");
		//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
		return false;
	 }
  return true;
 }  

 function showhide_div()
{
	
	if(document.getElementById('avail_L').checked == true)
	{  
	  jQuery('#avail_city').show();
	  jQuery('#availability_city').show();
	  
	}
	if(document.getElementById('avail_L').checked == false)
	{  		
	  jQuery('#avail_city').hide();
	  jQuery('#availability_city').hide();
	}  			
} 

function getChangeGame(id)
{
	jQuery.post('game_details.php',{'game_id':id, 'action':'get_content'}, function(data){
		jQuery('#dynamic_controls').html(data);
	});
}
  function keyCheck(eventObj, obj)
{
    var keyCode
 
    // Check For Browser Type
    if (document.all){
        keyCode=eventObj.keyCode
    }
    else{
        keyCode=eventObj.which
    }
 
    var str=obj.value
 
    if(keyCode==46){
        if (str.indexOf(".")>0){
            return false
        }
    }
 
    if((keyCode<46 || keyCode >57)   &&   (keyCode >31)){ // Allow only integers and decimal points
        return false
    }
    
    return true
}

function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
</script>
{/literal}

 <div class="right-panel">
      <div class="findstudent-title"><h1 title="Profile"><span>Registration</span></h1>
        <div class="breadcrumb">Add Game</div>
        <div class="clear"></div>
	  </div>
      <div class="register">
	   <div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:10px;">{if $ermsg neq ''} {$ermsg}{/if} </div>
      <form name="register" id="register" action="" method="post">
	  <input type="hidden" name="user_type" id="user_type" value="{$user_type}" />
         <fieldset>
		    {if $user_type eq '1'}
			<label>Paypal Email ID:</label>
            <input name="paypal_email_id" id="paypal_email_id" type="text" value="{$Formval.paypal_email_id}" class="con-req"/>
			{/if}
			<label>Availability :</label>
			 <input type="checkbox" name="availability_type1" class='check' value="O" /><label class='chklabel'>Online</label>
			 <input type="checkbox" name="availability_type2" class='check' id="avail_L" onclick="showhide_div()" value="L" /><label class='chklabel'>Local</label>
            <p class="clear"></p>
			
			<label id="avail_city"  {if $Formval.availability_type =='L'} {else} style='display:none' {/if}>City (Local meet-up) :</label>
            <div class="ausu-suggest">
		    <input name="availability_city" id="availability_city" type="text" value="{$Formval.availability_city}" class="con-req" autocomplete="off" 
			{if $Formval.availability_type =='L'} {else} style='display:none' {/if}/>
			<input name="cityid" id="cityid" type="hidden"  value="" autocomplete="off" />
			</div>
			
			<div class="clear"></div>
			<div class="clear"></div>
            <p class="clear"></p>
            <label>Select Game :</label>
            <select name="game_id" id="game_id" class="selectbox" onchange="getChangeGame(this.value)">
				<option value="">Select</option>
				{html_options options=$GameArr selected=$Formval.game_id}
			</select>
			{if $user_type eq '1'}
            <label>Rate ($):</label>
            <input name="rate" id="rate" type="text" value="{$Formval.rate}" onkeypress="return keyCheck(event, this);"/>
			{/if}
			{if $user_type eq '3'}
		    <label>Peak Hours:</label>
            <input name="peak_hours" id="peak_hours" type="text" value="{$Formval.peak_hours}"/>
			{/if}
			<p class="clear"></p>
		   
		   <p id="dynamic_controls">
			<!--THE DYNAMIC CONTROL WILL PRINT HERE - PROSENJIT-->
			{$extra_content}
		   </p>
	
			<p class="clear"></p>
            <label>Experience :</label>
            <textarea  name="experience" id="experience"></textarea>
			{if $user_type eq '1'}
            <p class="clear"></p>
			<label>Lesson Plan :</label>
            <textarea  name="lesson_plan" id="lesson_plan"></textarea>
			{/if}
			{if $user_type eq '3'}
            <p class="clear"></p>
			<label>Need Improvement In :</label>
            <textarea  name="need_improvement" id="need_improvement"></textarea>
			{/if}
			<p class="clear"></p>
			<label>About Me :</label>
            <textarea  name="description" id="description" onKeyDown="limitText(this.form.description,this.form.countdown,300);"
			onKeyUp="limitText(this.form.description,this.form.countdown,300);"></textarea>
            <p class="clear"></p>
			<label>Characters left </label><input readonly type="text" name="countdown" size="3" value="300" class="tiny">
			<label></label>(Maximum characters: 300)
			<p class="clear"></p>
            <label>&nbsp;</label>
            <input name="submit" class="submit" value="Submit" type="submit" onclick='return CheckFields()' />
            <input name="" class="cancel" value="Cancel" type="reset" />
         </fieldset>
      </form>
      </div>
      
      
      
      <div class="clear"></div>
   </div>