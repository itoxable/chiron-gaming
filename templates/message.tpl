{literal}
<script language="javascript">
jQuery(document).ready(function(){
	open_content('{/literal}{$pmsg_id}{literal}');
});

function showPostbox(message_id)
{	
	jQuery('.post-box').hide();	
	jQuery('#reply_'+message_id).slideUp();
	jQuery('#post_box_'+message_id).slideDown();	
}
function hidePostbox(message_id)
{	
	var message_text=jQuery('#message_text_'+message_id).val();
	if(message_text == ""){
		jQuery('#message_text_'+message_id).css('border','1px solid #FF0000');
		return false;
	}	
	else{
		jQuery('#message_text_'+message_id).css('border','1px solid #000');
		jQuery('.post-box').slideUp();	
		jQuery('#reply_'+message_id).slideDown();	
		var message_id=message_id;
		submit_message_text(message_id)
	}
	
		/*if ((jQuery('#reply_'+message_id).css('display') == "block")&&(jQuery('#post_box_'+message_id).css('display') == "block"))
			{  jQuery('#reply_content_'+message_id).show();}		*/
}
function cancelPostbox(message_id)
{	
		jQuery('.post-box').slideUp();	
		jQuery('#reply_'+message_id).slideDown();	
		//show_reply_btn(message_id);
	/*	if ((jQuery('#reply_'+message_id).css('display') == "block")&&(jQuery('#post_box_'+message_id).css('display') == "block"))
			{  jQuery('#reply_content_'+message_id).show();}*/
}
function submit_message_text(message_id)
{	
	var message_text=jQuery('#message_text_'+message_id).val();
	var user_id_to=jQuery('#user_id_to_'+message_id).val();	
	var reply_of_msg=jQuery('#reply_of_msg_'+message_id).val();
	jQuery('#show_uppr_rep_'+message_id).hide();	
	
	
	jQuery.ajax({
		   url:'reply.php',
		   data:'flag=1&message_text='+message_text+'&reply_of_msg='+reply_of_msg+'&user_id_to='+user_id_to,
		   type:'post',		  
		   success:function(resp){
		   //alert(resp);		   	
			    jQuery('#reply_content_'+message_id).html(resp);											
		   } 		   
	});
		jQuery('#show_post_lnk_'+message_id).html('<a href="javascript:;" onclick="open_content('+message_id+');show_reply_btn('+message_id+')">PostBox</a>');
	/*	if((jQuery('#reply_content_'+message_id).css('display') == "none") || ((jQuery('#reply_content_'+message_id).css('display') == "block")))		
		{
		//alert("#reply_content_"+message_id);
		jQuery('#show_uppr_rep_'+message_id).html('<p class="button-flex" id="reply_ini_'+message_id+'"><a href="javascript:;" onclick="reply_ini_hide('+message_id+')"><span>Reply</span></a></p>');
		}*/
	document.getElementById("message_text_"+message_id).value='';
	
}
function open_content(message_id)
{	
   
	if (jQuery('#reply_content_'+message_id).css('display') == "none")
	{
		jQuery('#reply_content_'+message_id).slideDown();
		var str = '#show_uppr_rep_'+message_id;
		
		jQuery(str).slideUp();
		jQuery('#reply_ini_'+message_id).hide();	
		
		
	}			
	else
	{
		jQuery('#post_box_'+message_id).slideUp();
		jQuery('#reply_content_'+message_id).slideUp();
		jQuery('#reply_ini_'+message_id).show();	
		jQuery('#show_uppr_rep_'+message_id).show();
		
		//show_reply_btn(message_id);
		/*if ((jQuery('#reply_'+message_id).css('display') == "block")&&(jQuery('#post_box_'+message_id).css('display') == "block"))
			{  jQuery('#reply_content_'+message_id).show();}*/
		//cancelPostbox(message_id);	
	}
}
function reply_ini_hide(message_id)
{	
	open_content(message_id);
	showPostbox(message_id);
	mark_unread(message_id);	
	jQuery('#name_'+message_id).css('color','#009933');	
	jQuery('#reply_ini_'+message_id).hide();		
}
function show_reply_btn(message_id)
{	
	if ((jQuery('#reply_content_'+message_id).css('display') == "block")||(jQuery('#reply_content_'+message_id).css('display') == "none"))
	{
		//alert("ttp");
		if ((jQuery('#reply_'+message_id).css('display') == "block")&&((jQuery('#post_box_'+message_id).css('display') == "block")))
			{  jQuery('#show_uppr_rep_'+message_id).show();  jQuery('#reply_content_'+message_id).show(); }	
		if ((jQuery('#reply_'+message_id).css('display') == "block")&&((jQuery('#post_box_'+message_id).css('display') == "none")))
			{   jQuery('#reply_ini_'+message_id).show(); jQuery('#show_uppr_rep_'+message_id).show();}	
	
	}
	
}
function mark_unread(message_id)
	{	
	
	jQuery.ajax({
		   url:'reply.php',
		   data:'flag=3&message_id='+message_id,
		   type:'post',		  
		   success:function(resp){
		   //alert(resp);	  									
		   } 		   
		});	
	}	
</script>
{/literal}
{if $IsProcess!="Y"}
<div align="right">
	{if $is_coach eq '0'}
	<!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
	<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
	{/if}
	{if $is_partner eq '0'}
	<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
	<!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
	{/if} 
</div>
	<div class="content">
		
		{* Start loading Tabs *}
		{include file='user_tabs.tpl' active_tab='message'}
		{* End loading Tabs *}
        
        <div class="clear"></div>
        <div class="tabular-content">
            <div class="tab_sub">
            	<ul>
				{if $page_name eq 'send_item.php'}
                	<li><a href="inbox.php"  class="active">Inbox</a></li>
                    <li><a href="javascript:;">Sent Items</a></li>
				{else}
					<li><a href="javascript:;" >Inbox</a></li>
                    <li><a href="send_item.php" class="active">Sent Items</a></li>
				{/if}
                </ul>
            </div>
			 
            <br />
            <div class="tab_sub">
            	&nbsp;
            </div>
            <div class="tab_search">
            	&nbsp;
            </div>
              
            <div class="clear"></div> 
		
		<div id="paging">
            
  {/if}          
     <div class="tab_pagination">
            	{if $pagination_arr[1]}
				 <div class="clear"></div>
				  <div class="pagin">
					 <ul>
					   {$pagination_arr[1]}
					 </ul>
				  </div>
				 {/if}
            </div>         
            <!-- details page block start here -->
			{section name=msg loop=$latest_msgArr}
            	<div class="comment-container">				
                	<div>
					{if $latest_msgArr[msg].photo neq ''}
                        <div class="message-thumb"><img src="uploaded/user_images/thumbs/mid_{$latest_msgArr[msg].photo}" width="57" height="50" alt="" border="0" /></div>				{else}
						<div class="message-thumb"><img src="images/avatar_small.jpg" width="57" height="50" alt="" border="0" /></div>
					{/if}
                        <div class="message-details" id="{$latest_msgArr[msg].message_id}">
                        		
							<div id="name_{$latest_msgArr[msg].message_id}" {if $latest_msgArr[msg].read eq 'N'} style="color:#CC0000" {else} style="color: #009933" {/if} >
							{$latest_msgArr[msg].name}
							</div>							
							<!--<h3>{$latest_msgArr[msg].name}</h3>-->
                            <span class="message-date">{$latest_msgArr[msg].date_add}</span>
                            <br />
                        	{$latest_msgArr[msg].message_text}
                        </div>
						<div id="show_post_lnk_{$latest_msgArr[msg].message_id}">
						{if $latest_msg_subArr[msg]|@count gt 0}
						<a href="javascript:;" onclick="open_content({$latest_msgArr[msg].message_id});show_reply_btn({$latest_msgArr[msg].message_id})">PostBox</a>
						{/if}
						</div>
                        <div class="clear"></div>
                        <!-- post -->
						<div id="show_uppr_rep_{$latest_msgArr[msg].message_id}">
						{if $latest_msg_subArr[msg]|@count gt 0}
						  <p class="button-flex button40" id="reply_ini_{$latest_msgArr[msg].message_id}"><a href="javascript:;" onclick="reply_ini_hide({$latest_msgArr[msg].message_id})"><span>Reply</span></a></p>
						{/if} 
						</div> 
						{if $latest_msg_subArr[msg]|@count gt 0}
						<div id="reply_content_{$latest_msgArr[msg].message_id}" style="display:none;">								
						{section name=msg1 loop=$latest_msg_subArr[msg]}	                        				 	
                            <div class="message-post" id="{$latest_msg_subArr[msg][msg1].message_id}">
							{if $latest_msg_subArr[msg][msg1].photo neq ''}
                            <div class="message-thumb"><img src="uploaded/user_images/thumbs/mid_{$latest_msg_subArr[msg][msg1].photo}" width="57" height="50" alt="" border="0" /></div>			{else}
							<div class="message-thumb"><img src="images/avatar_smallest.jpg" border="0" alt="" /></div>
							{/if}
							<div style="float:left; display:block; width:380px;">
                            <h3>{$latest_msg_subArr[msg][msg1].name}</h3>
                            <span class="message-date">{$latest_msg_subArr[msg][msg1].date_add}</span>
                            <br />                            
                          {$latest_msg_subArr[msg][msg1].message_text}</div>
                            <div class="clear"></div>
                            </div>
                            <div class="message-post-bottom"></div>
                            <div class="clear"></div>
                        
						{/section}
						<div class="clear"></div>                        
                        <p class="button-flex button40" id="reply_{$latest_msgArr[msg].message_id}"><a href="javascript:;" onclick="showPostbox({$latest_msgArr[msg].message_id})"><span>Reply</span></a></p>
						</div>
						{else}
						<div id="reply_content_{$latest_msgArr[msg].message_id}" >	
						
						 <div class="clear"></div>
                        
                        <p class="button-flex button40" id="reply_{$latest_msgArr[msg].message_id}"><a href="javascript:;" onclick="reply_ini_hide({$latest_msgArr[msg].message_id})"><span>Reply</span></a></p>
						</div> 
						{/if}						
                        <!-- post -->                        
                       	<div class="clear"></div>
						<br />
                        <div class="clear"></div>
                        <div class="post-box" style='display:none' id="post_box_{$latest_msgArr[msg].message_id}">						
                        	<textarea rows="5" cols="10" id="message_text_{$latest_msgArr[msg].message_id}"></textarea>
							<input name="user_id_to_{$latest_msgArr[msg].message_id}" id="user_id_to_{$latest_msgArr[msg].message_id}" type="hidden" value="{$latest_msgArr[msg].user_id_to}" />							
							<input name="reply_of_msg_{$latest_msgArr[msg].message_id}" id="reply_of_msg_{$latest_msgArr[msg].message_id}" type="hidden" value="{$latest_msgArr[msg].message_id}" /> 
                            <br />
                            <br />
                                <div class="button-flex button40" id="DelFavourite" style="margin-right:0px;">
							 <a href="javascript:;" onclick="cancelPostbox({$latest_msgArr[msg].message_id});show_reply_btn({$latest_msgArr[msg].message_id})"><span>Cancel</span></a></div>
							  <div class="button-flex button40" id="DelFavourite" style="margin-right:20px;">
							 <a href="javascript:;" onclick="hidePostbox({$latest_msgArr[msg].message_id});show_reply_btn({$latest_msgArr[msg].message_id})"><span>Post</span></a>
							 </div>
                              <div class="clear"></div>
                        </div>
                        
                         <div class="clear"></div>
                    </div>
                    
                </div>
			{/section}
            <!-- details page block start here -->           
{if $IsProcess!="Y"}
		</div>
        </div>
    </div>
{/if}
<script language="javascript">
       changePageTitle('Messages');
</script>