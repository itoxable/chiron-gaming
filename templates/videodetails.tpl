{literal}
<link type="text/css" rel="stylesheet" href="style/lightbox_form.css">
<script src="js/lightbox-form.js" type="text/javascript"></script>
<script language="javascript" src="js/contact_us.js"></script>

<script language="javascript">
jQuery(document).ready(function(){

		    jQuery(".rating").rating();
		    jQuery("#serialStar").rating();
			
		});		
		
function OpenDiv(){
document.getElementById('open_close_div').style.display="block";
}

function CloseDiv(){
document.getElementById('open_close_div').style.display="none";
}

function CheckFields(formID,NumRatingCat)
{
   var r_comment = document.getElementById('review_comment').value;
   var video_id = document.getElementById('video_id').value;
   var reviewed_by = document.getElementById('logged_user').value;
   
   if(document.getElementById('review_comment').value=='')
   {
    j('#review_comment').css('border','1px solid #FF0000');
		return false;
   }
   else 
   j('#review_comment').css('border','1px solid #ccc');
   
   
   var frmID='#'+formID;
	
	var params ={

		'action': 'review_post',
		'IsProcess': 'Y',
		//'video_id': video_id,
		'reviewed_by': reviewed_by,
		'review_comment': r_comment
	};

   
    if(r_comment!='')
   {
	  jQuery.ajax({
		type: "POST",
		url: 'videodetails.php?video_id='+video_id,
		data: params,
		dataType : 'text',
		success: function(data){
		
		//if (data.flag == '1')
		  //{
		    jQuery('#review-message').fadeIn(0);
			jQuery('#msgbox').fadeIn(0);
		    jQuery('#msgbox').html('Review posted successfully.');
		    jQuery('#review-message').fadeOut(5000);
			jQuery('#paging').html(data);
		 
		 // }
        }
      });
	  document.getElementById("review_comment").value='';
	  //document.getElementById('serialStar').value='';
	  jQuery("#reviewwindow").fadeOut("fast", function(){jQuery('#overlay').fadeOut();});
	}
  }	
/*function removeDiv()
{
clearInterval(ti);
document.getElementById('msgbox').style.display = 'none';
}	
*/
function closereviewbox()
{
	document.getElementById("review_comment").value='';
	//document.getElementById("serialStar").value='';
	jQuery('#review_comment').css('border','1px solid #ccc');
	jQuery("#reviewwindow").fadeOut("fast", function(){jQuery('#overlay').fadeOut();});
}
</script>
<script type="text/javascript">
function SortingList(sorting_url,sorting_by)
{
var params = {
	'list_for':'sorting',
	'sorting_by': sorting_by,
	'IsProcess': 'Y',
	'action': 'send',
};

jQuery.ajax({
	type: "GET",
	url: sorting_url,
	data: params,			
	dataType: 'html',
	success: function(data)
	{				
		jQuery(".container-inner").html(data+"<div class='clear'></div>");
	}
});
return false;		
}
</script>

<script type="text/javascript">
function redirect_function(id)
{
//alert(id);
  document.forms["videoform"+id].submit();
}
</script>
<script type="text/javascript">

 function LikeVideo(coach_id,video_id,like)
 { 	 
 
 	if(like == 'D')
	{ 
	 	jQuery.ajax({
		   url:'like_post.php',
		   data:'coach_id='+coach_id+'&video_id='+video_id+'&selected_for='+like,
		   type:'post',		 
		   dataType:'html', 
		   success:function(resp){	
				var arr = resp.split("-");
						  
				jQuery('#showMsgs').fadeIn(0);
				jQuery('#showMsgs').html('You have dislike this video');
				jQuery('#showMsgs').fadeOut(5000);
				
				jQuery('#like-dislike-bar').html(arr[0]);
				jQuery('#rating-like').html(arr[1]);
				jQuery('#rating-dislike').html(arr[2]);
							
			}			   
		});
	}
	else
	{
		 jQuery.ajax({
		   url:'like_post.php',
		   data:'coach_id='+coach_id+'&video_id='+video_id+'&selected_for='+like,
		   type:'post',		 
		   dataType:'html', 
		   success:function(resp){	
				var arr = resp.split("-");
				
				jQuery('#showMsgs').fadeIn(0);
				jQuery('#showMsgs').html('You have liked this video');
				jQuery('#showMsgs').fadeOut(5000);
				
				jQuery('#like-dislike-bar').html(arr[0]);
				jQuery('#rating-like').html(arr[1]);
				jQuery('#rating-dislike').html(arr[2]);

			}			   
		});	
	}
}


</script>   		
{/literal}
{if $IsProcess neq 'Y'}

<div class="video-content">
   <div class="vdetails">
     <h2>{$VideoArr.video_title}</h2>
	 <ul class="games-desc" style="padding-top:0px; width:50%;">
      <li><span>Uploaded By : </span>{$VideoArr.name}</li>
	  </ul>
	 <ul class="games-desc" style="padding-top:0px;text-align: right; width:50%;">{$VideoArr.v_dt}&nbsp;</ul>
   </div>	 
     <div class="video-placeholder">
	 {if $VideoArr.is_video_url eq 'Y'}

        <object width="490" height="376">
            <param name="movie" value="http://www.youtube.com/v/{$video_num}?fs=1"</param>
            <param name="allowFullScreen" value="true"></param>
            <param name="allowScriptAccess" value="always"></param>
            <param name="wmode" value="transparent" />
            <embed src="http://www.youtube.com/v/{$video_num}?fs=1"
            type="application/x-shockwave-flash" allowfullscreen="true" wmode="opaque" allowscriptaccess="always" width="490" height="376">
            </embed>
        </object>

            
	  {else}	
			<OBJECT ID="MediaPlayer" width="490" height="376" CLASSID="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95"
			STANDBY="Loading Windows Media Player components..." TYPE="application/x-oleobject">
			<PARAM NAME="FileName" VALUE="uploaded/videos/{$VideoArr.video_file}">
			<PARAM name="ShowControls" VALUE="true">
			<param name="ShowStatusBar" value="false">
			<PARAM name="ShowDisplay" VALUE="false">
			<PARAM name="autostart" VALUE="true">
			<param name="wmode" value="transparent" />
            <param name="windowlessVideo" value="true" >
			<EMBED TYPE="application/x-mplayer2" SRC="uploaded/videos/{$VideoArr.video_file}" NAME="MediaPlayer"
			WIDTH="490" HEIGHT="376" ShowControls="1" wmode="opaque" windowlessVideo="true" ShowStatusBar="0" ShowDisplay="0" autostart="1"> </EMBED>
            </OBJECT>
	   {/if}		
	 </div>
     <div class="video-list">
     <div class="vdetails">
           <!-- <p><span>Votes :</span><span id="votes_{$VideoArr.video_id}"> {$VideoArr.numlike}</span></p>-->
		   
           {if $smarty.session.user_id neq ''}
		   {if $VideoArr.user_id neq $smarty.session.user_id}
				  <div id="game-rating_{$VideoArr.video_id}">			 
				  <div class="like-dislike" style="float:left;">
					  <a href="javascript:;" onclick="return LikeVideo({$VideoArr.user_id},{$VideoArr.video_id},'L')">
					  <img src="images/like.png" alt="Like"/> </a>
					  <a href="javascript:;" onclick="return LikeVideo({$VideoArr.user_id},{$VideoArr.video_id},'D')">
					  <img src="images/unlike.png" alt="Unlike"/> </a>
				  </div>
		   {else}		  
				<div id="game-rating_{$VideoArr.video_id}">			 
				  <div class="like-dislike" style="float:left;">
					 
				  </div>  
		   {/if}
		   	  
		   {else}
		    <div id="game-rating_{$VideoArr.video_id}">			 
			  <div class="like-dislike" style="float:left;">
				  <a href="javascript:;" onclick="OpenDiv();">
				  <img src="images/like.png" alt="Like"/> </a>
				  <a href="javascript:;" onclick="OpenDiv();">
				  <img src="images/unlike.png" alt="Dislike"/> </a> 
			  </div>
		    {/if}	  		  
				  <div class="like-dislike-bar">
				  <span id="like-dislike-bar">
				  {if $RatingLike eq '0' && $RatingUnlike eq '0'}
					 <img src='images/null.gif' alt='' width='210px' height="4px" border='0' />
				  {else}  
					  <img src='images/green.gif' alt='' width='{$WidthGreen}px' height="4px" border='0' />
					  <img src='images/red.gif' alt='' width='{$WidthRed}px' height="4px"  border='0' />
				  {/if}
				  </span>
				  <br />
				  <span id="rating-like">{$RatingLike}</span> Likes, <span id="rating-dislike">{$RatingUnlike}</span> Dislikes
				  </div>  
				  </div>  
			
           <br>
           <div class="clear"></div>
		   <div id="showMsgs" style="color:#0077BC;"></div>
           
           <div class="clear"></div>
		   <div class="signin-message" style="display:none;" id="open_close_div">
           <a href="login.php" title="Sign In">Sign in</a> or <a href="register.php" title="Sign Up">Sign up</a> now!
           <a href="javascript:;" onclick="CloseDiv();"><img src="images/close.png" border="0" alt="Close" class="close" title="Close" /></a>
		   </div>
		   <br>
           <div class="clear"></div>
           <p>{$VideoArr.description}</p>
           <p class="clear"></p>
           <ul class="games-desc">
               <li><label class="gametype">Game :</label><label class="gamedetail">{$VideoArr.game}</label></li>
			   {if $VideoArr.ladder neq ''}
               <li><label class="gametype">Ladder :</label><label class="gamedetail">{$VideoArr.ladder}</label></li>
			   {/if}
			   {if $VideoArr.type neq ''}
			   <li><label class="gametype">Type :</label><label class="gamedetail">{$VideoArr.type}</label></li>
			   {/if}
			   {if $VideoArr.champion neq ''}
			   <li><label class="gametype">Versus :</label><label class="gamedetail">{$VideoArr.champion}</label></li>
			   {/if}
			   {if $VideoArr.mode neq ''}
			   <li><label class="gametype">Mode :</label><label class="gamedetail">{$VideoArr.mode}</label></li>
			   {/if}
            </ul>
            <ul class="games-desc">
			   <li><label class="gametype">Duration :</label><label class="gamedetail">{$VideoArr.time_length} min</label></li>
			   {if $VideoArr.versus neq ''}
			   <li><label class="gametype">Versus :</label><label class="gamedetail">{$VideoArr.versus}</label></li>
			   {/if}
			   {if $VideoArr.team neq ''}
			   <li><label class="gametype">Team :</label><label class="gamedetail">{$VideoArr.team}</label></li>
			   {/if}
			   {if $VideoArr.map neq ''}
			   <li><label class="gametype">Map :</label><label class="gamedetail">{$VideoArr.map}</label></li>
			   {/if}
			   {if $VideoArr.class neq ''}
			   <li><label class="gametype">Class :</label><label class="gamedetail">{$VideoArr.class}</label></li>
			   {/if}
             </ul>
			
            <div class="clear"></div>
          
		   <div style="padding-right:15px; padding-bottom:10px;">
		  
		   {if $smarty.session.user_id neq '' && $smarty.session.user_id neq $VideoArr.user_id}
              <div class="button-flex button70" style="margin-right:{if $smarty.session.user_type eq '2' && $is_fav eq '0'}20px;
			  {elseif $smarty.session.user_type eq '2' && $is_fav eq '1'}10px;{else}0px;{/if}"><a href="javascript:void(0);" 
			  onClick="openReview('Review and Rating for <b>{$VideoArr.video_title}</b>')"><span>Review</span></a></div> 
		   {/if}
		   <br /> 
			         <div class="clear"></div>
		             <div class="signin-message" id="review-message" style="display:none;">
                     <div id="msgbox" style="color:#0077BC;"></div>
		             </div>
		   </div>
          </div> 
       <div class="clear"></div>
	  </div>	
	   {/if} 
	   
	   <div id="paging"> 
	 
     <div class="review-list-small">
     <h2 title="Review">Reviews</h2>
	  {if $NumReview >0}
       <div class="clear"></div>
	    {section name=rrow loop=$VdoReview}
		 
	    <div class="review-block-small">
             
			 <div class="review-left">
			 <a href="{$VdoReview[rrow].userlink}">
			  <img src="{$VdoReview[rrow].img}" height="92" width="105"  alt="{$VdoReview[rrow].review_by}" border="0" />
			 </a>
			 </div>
			 <div class="review-right">
			 <div class="review-sub-title">
             
             	<div class="review_thumb_block">
				 <h4 class="reviewshead">Reviewed By : <a href="{$VdoReview[rrow].userlink}">{$VdoReview[rrow].review_by}</a></h4><br/><br />
	 			 
				 <span><em>Posted on</em></span> : {$VdoReview[rrow].r_date}<br /><br />
				 {if $VdoReview[rrow].review_comment neq ''}
				 <p><i>Review</i>:&nbsp;<span style="width:100px;">{$VdoReview[rrow].review_comment|nl2br}</span></p><br />
				 {/if}
				</div>
                
                <div class="clear"></div>
			 </div>
			 </div>
			 <div class="clear"></div>
		</div>
		 
	    {/section}
      
	  {if $pagination_arr[1]}
      <div class="clear"></div>
      <div class="pagin">
         <ul>
            {$pagination_arr[1]}
         </ul>
      </div>
	  {/if}
	  {else}
	   No reviews
	{/if} 
	</div>
  </div>
	   
   </div>

	

  
<div class="modalpopup" id="reviewwindow">
    <a class="modalpopup-close-btn" href="javascript:;"></a>
    <div id="boxtitle" style="font-weight: normal;" class="title"></div>
    <div class="review">
        <form name="Video_Review" id="Video_Review"  method='post' action="" onsubmit="return false;">
            <input type="hidden" name="video_id" id="video_id" value="{$video_id}" />
            <input type="hidden" name="logged_user" id="logged_user" value="{$logged_user}" />
            <fieldset>   
                <label>Review :</label>
                <textarea name="review_comment" id="review_comment" cols="" rows="" style="font-weight:bold;">{$Formval.user_about}</textarea>
                     <p class="clear"></p>

                <label>&nbsp;</label>
                <input name="submit" class="button" value="Submit" type="submit" onclick="return CheckFields('Video_Review','{$NumRatingCat}')" />
                <input name="" class="button" value="Cancel" type="button" onClick="closereviewbox()" />
                </p>
            </fieldset>	
        </form>
    </div> 
</div>
<!--{section name=j start=1 loop=6 step=1}
	{if $smarty.section.j.index <= $UserReview[rrow].overall_r}
		<img src='images/star-c.gif' alt='' border='0' />
	{else}
		<img src='images/star-g.gif' alt='' border='0' />
	{/if}
{/section}-->