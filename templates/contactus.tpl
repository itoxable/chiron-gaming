{literal}
<script language="javascript" src="js/cont.js"></script>


<script language="javascript">
function CheckFields(){
  if(jQuery("#name").isEmpty())
    	 jQuery("#name").attr('style','border:1px solid #FF0000');
  if(jQuery("#subject_id").isEmpty())
  		 jQuery("#subject_id").attr('style','border:1px solid #FF0000');
  if(jQuery("#msg").isEmpty())
  		 jQuery("#msg").attr('style','border:1px solid #FF0000');
  if(jQuery("#email_1").isEmpty())
    	 jQuery("#email_1").attr('style','border:1px solid #FF0000');		 	 
  if(jQuery("#contactTxtCaptcha").isEmpty())
    	 jQuery("#contactTxtCaptcha").attr('style','border:1px solid #FF0000');
  
 }
     
 function showhide_div(id)
{
	if(id == 'L')
	{  
	  jQuery('#avail_city').show();
	  jQuery('#avail_country').show();	
	  jQuery('#availability_city').show();
	  jQuery('#availability_country').show();
	}
	if(id == 'O')
	{  		
	  jQuery('#avail_city').hide();
	  jQuery('#avail_country').hide();
	  jQuery('#availability_city').hide();
	  jQuery('#availability_country').hide();
	}  			
} 
function keyCheck(eventObj, obj)
{
    var keyCode
 
    // Check For Browser Type
    if (document.all){
        keyCode=eventObj.keyCode
    }
    else{
        keyCode=eventObj.which
    }
  //alert(keyCode)
    var str=obj.value
 
    if(keyCode==46){
        if (str.indexOf(".")>0){
            return false
        }
    }
 
    if((keyCode<46 || keyCode >57)   &&   (keyCode >31)){ // Allow only integers and decimal points
        return false
    }
    
    return true
}
</script>
{/literal}
  
  
 <div class="right-panel">
    <div class="title">Contact Us</div>     
    <div class="clear"></div>
      
    <div class="contact" id="contact"> 
        <div id='err' style="color:red; text-align:center; padding-bottom:10px;"></div>
        <div class="register">
        <form action=""  name="contact_us" id="contact_us" method="post" class="contactform">
               <fieldset>
                    <label>Name :</label>
                    <input type="text" name="c_name" id="c_name" class="con-req"  />
                    <p class="clear"></p>
                    <label>Email :</label>
                    <input name="c_email" id="c_email" type="text" class="con-req" />
                    <p class="clear"></p>
                    <label>Subject :</label>
                    <select name="subject_id" id="subject_id" class="con-req">
                        <option value="">SELECT</option>
                         {html_options options=$SubjectSql selected=$SubjectArr.subject_name}
                    </select>
                    <p class="clear"></p>
                    <label>Massage :</label>
                    <textarea name="c_comment" id="c_comment" rows="5" cols="5" class="con-req"></textarea>
                    <p class="clear"></p>
                    
                    <p class="clear"></p>
                    <label>Captcha :</label>
                    <div style="float:left; width:180px;"><!--img src="CaptchaSecurityImages.php?characters=7" /--> <img src="#" id="contactCaptcha" />
                    <br />
                    <a href="javascript:;" onclick="document.getElementById('contactCaptcha').src='/captcha.php?'+Math.random();document.getElementById('contactTxtCaptcha').focus();" id="change-image">Not readable? Change text.</a>
                    </div>
                    <p class="clear"></p>
                    
                    <label></label>
                    <label>&nbsp;</label>
                    <p class="clear"></p>
                    
                    <label>Security Code :</label>
                    <input name="contactTxtCaptcha" id="contactTxtCaptcha" type="text" class="con-req" value="" />
                    <p class="clear"></p>
                    
                    <label></label>
                    <p class="clear"></p>
                    <label>&nbsp;</label>
                    <input name="" type="button" class="submit"  value="Submit" onclick="contactus('contact_us')" />	
                    <input name="" class="cancel" value="Reset" type="reset" />
             </fieldset>
          </form>
        </div>
    </div>
    <div class="clear"></div>
    <script language="javascript">
        changePageTitle('Contact Us');
        document.getElementById('contactCaptcha').src='/captcha.php?'+Math.random();
    </script>
</div>