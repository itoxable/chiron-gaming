
		<!-- tab -->
    	<ul class="tabflex">
       
			<li class="left-shadow">
				{if $active_tab eq 'schedule'}
					<a href="#" class="active">
				{else}
					<a href="schedule.php">
				{/if}
					<span>Schedule</span>
				</a>
			</li>
            <li>
				{if $active_tab eq 'message'}
					<a href="#" class="active">
				{else}
					<a href="messages.php">
				{/if}
					<span>Messages</span>
				</a>
			</li>
			<li>
				{if $active_tab eq 'profile'}
					<a href="#" class="active">
				{else}
					<a href="profile.php">
				{/if}
					<span>Profile</span>
				</a>
			</li>
			{if $is_coach eq '1'}
				<li>
					{if $active_tab eq 'coach'}
					<a href="#" class="active">
					{else}
						<a href="calendar.php">
					{/if}
						<span>Coach</span>
					</a>
					
				</li>
			{/if}		
				
			
			{if $is_partner eq '1'}
				<li>
					{if $active_tab eq 'training partner'}
						<a href="#" class="active">
					{else}
						<a href="training_partner_game.php">
					{/if}
						<span>Training Partner</span>
					</a>
				</li>
			{/if}
			
			<li>
				{if $active_tab eq 'cashier'}
					<a href="#" class="active">
				{else}
					<a href="cashier.php">
				{/if}
					<span>Cashier</span>
				</a>
			</li>
			
        </ul>