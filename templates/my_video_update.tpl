{literal}
<script language="javascript" src="js/contact_us.js"></script> 
<script language="javascript" type="text/javascript">
jQuery(document).ready(function(){
	getPageId('{/literal}{$Formval.game_id}{literal}','{/literal}{$Formval.ladder_id}{literal}','{/literal}{$Formval.race_id}{literal}');
});

function getPageId(type,id,rid,sid)
{	

	jQuery.ajax({
			type:'POST',
			url:'game_info.php',
			data: 'type='+type+'&id='+id+'&rid='+rid,
			dataType:'json',
			success:function(jData){
			
				if(jData.flag == 1)
				{
					jQuery('#ladder_id_td').html(jData.html);
					jQuery('#race_id_td').html(jData.html1);
					
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				jQuery(".contactus-block").html("Some server error occoured. Please report to system administrator.</div>");
			}
		});
}
function CheckFields()
{
  
  var vid=document.getElementById('video_id').value;
  if(document.getElementById('video_title').value=='' ) 
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please enter a title");
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
    return false;
   }
  if(document.getElementById('game_id').value=='' ) 
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please select a game");
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
    return false;
   }
/*   if(document.getElementById('race_id').value=='' ) 
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please select a race");
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
    return false;
   }
   if(document.getElementById('ladder_id').value=='' ) 
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please select a ladder");
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
    return false;
   }
*/   var vdo_type = document.getElementById('is_video_url').value;
   if(vid=='')
   {
	   if(vdo_type=='N')
	   {
		 if(document.getElementById('video_doc').value=='' ) 
		   {
			jQuery('#FormErrorMsg').show();
			jQuery('#FormErrorMsg').html("Please upload video");
			//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
			return false;
		   }
	   }
		if(vdo_type=='Y')
	   {
		 if(document.getElementById('video_url').value=='' ) 
		   {
			jQuery('#FormErrorMsg').show();
			jQuery('#FormErrorMsg').html("Please enter youtube url");
			//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
			return false;
		   }
	   }
	}   
    if(vid=='')
   {
	   if(document.getElementById('time_length').value=='' ) 
		 {
			jQuery('#FormErrorMsg').show();
			jQuery('#FormErrorMsg').html("Please enter time length");
			//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
			return false;
		 }
   		   
   if(document.getElementById('img1').value=='' ) 
   {
		jQuery('#FormErrorMsg').show();
		jQuery('#FormErrorMsg').html("Please upload image");
		//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
		return false;
   }
 }
  return true;
 }
 function getChangeGame(id)
{	
	//alert(id);
	jQuery('#ladder').hide();
	jQuery('#versus').hide();
	jQuery('#type').hide();
	jQuery('#champion').hide();
	jQuery('#team').hide();
	jQuery('#map').hide();
	jQuery('#mode').hide();
	jQuery('#class').hide();
	jQuery('#ladder_gap').hide();
	jQuery('#versus_gap').hide();
	jQuery('#type_gap').hide();
	jQuery('#champion_gap').hide();
	jQuery('#team_gap').hide();
	jQuery('#map_gap').hide();
	jQuery('#mode_gap').hide();
	jQuery('#class_gap').hide();
	jQuery('#champion_gap').hide();
	jQuery.ajax({
			url:'game_details.php?is_vedio=1',
			data: 'id='+id,
			type:'post',		
			dataType:'json',	
		   success:function(resp){
		   //alert(resp.map);
		   if(resp.ladder=='Y')
		   {
		   		jQuery('#ladder').show();
				jQuery('#ladder_gap').show();
			}			
			 if(resp.versus=='Y')
			 {
		   		/*jQuery('#versus').css('display','block');
				jQuery('#versus').html(resp.versus_str);*/
				jQuery('#versus').show();
				jQuery('#versus_gap').show();
			}	
			 if(resp.type=='Y')
		   	{
				jQuery('#type').show();				
				jQuery('#type_gap').show();
			}	
			 if(resp.champion=='Y')
		   	{
				jQuery('#champion').show();
				jQuery('#champion_gap').show();
			}	
			 if(resp.team=='Y')
		   	{
				jQuery('#team').show();
				jQuery('#team_gap').show();
			}
			 if(resp.map=='Y')
		   	{
				jQuery('#map').show();
				jQuery('#map_gap').show();
			}
			 if(resp.mode=='Y')
		   	{
				jQuery('#mode').show();
				jQuery('#mode_gap').show();
			}
			 if(resp.is_class=='Y')
		   	{
				jQuery('#class').show();
				jQuery('#class_gap').show();
			}
				}
		});
}  
 function keyCheck(eventObj, obj)
{
    var keyCode
 
    // Check For Browser Type
    if (document.all){
        keyCode=eventObj.keyCode
    }
    else{
        keyCode=eventObj.which
    }
 
    var str=obj.value
 
    if(keyCode==46){
        if (str.indexOf(".")>0){
            return false
        }
    }
 
    if((keyCode<46 || keyCode >57)   &&   (keyCode >31)){ // Allow only integers and decimal points
        return false
    }
    
    return true
}
</script>
{/literal}

 <div class="content">
   		<!-- tab -->
    	<ul class="tabflex">
        	<li><a href="dashboard.php"><span>Dashboard</span></a></li>
            <li><a href="inbox.php"><span>Message</span></a></li>
			<li><a href="profile.php"><span>Profile</span></a></li>
			{if $is_coach eq '1'}<li><a href="coach_game.php"><span>Coach</span></a></li>{/if}
			{if $is_partner eq '1'}<li><a href="training_partner_game.php"><span>Training Partner</span></a></li>{/if}
			{if $is_coach eq '1'}<li><a href="add_lesson.php"><span>Lessons</span></a></li>{/if}
			{if $is_coach eq '0'}<li><a href="my_lesson.php"><span>Lessons</span></a></li>{/if}
			<li><a href="upload_videos.php" class="active"><span>Videos</span></a></li>
			{if $is_coach eq '1'}<li><a href="my_review.php"><span>Reviews</span></a></li>{/if}
			{if $is_coach eq '0'}<li><a href="my_video_review.php"><span>Reviews</span></a></li>{/if}
			
          <!--    <li><a href="#"><span>Students</span></a></li>-->
        </ul>
        <!-- tab -->
        <div class="clear"></div>
        <div class="tabular-content"> 
		  <div class="tab_sub">
            	<ul>
                	<li><a href="upload_videos.php" >Upload Videos</a></li>
                    <li><a href="liked_videos.php" class="active">Liked Videos</a></li>
                </ul>
            </div> 
		 <div class="clear"></div>                     
         <div>
      <div class="register">
	   <div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:10px;">{if $ermsg neq ''} {$ermsg}{/if} </div>
      <form name="game" id="game" action="" method="post" enctype="multipart/form-data">
	  <input type="hidden" name="video_id" value="{$video_id}" id="video_id" />
         <fieldset>
		    <label>Title :</label>
            <input name="video_title" id="video_title" type="text" value="{$Formval.video_title}" class="con-req" />
			<p class="clear"></p>
            <label>Select Game :</label>
            <select name="game_id" id="game_id" class="con-req" onchange="getChangeGame(this.value)">
				<option value="">Select</option>
				{html_options options=$GameArr selected=$Formval.game_id}
			</select>
			<p class="clear"></p>
		
			{if $FabricArr.is_ladder eq 'Y'}
		   <div id="ladder_gap" {if $FabricArr.is_ladder neq 'Y'} style='display:none' {/if}>
			 <p class="clear"></p>
		  </div>
		  {/if}		 
		 <div id="ladder" {if $FabricArr.is_ladder neq 'Y'} style='display:none' {/if}>
			<label>Ladder:</label>			
				<select name="ladder_id" id="ladder_id">
					<option value="">Select</option>
					{html_options options=$LadderArr selected=$FabricArr.ladder_id}
				</select>			
		 </div>
		  <div id="versus_gap" {if $FabricArr.is_versus neq 'Y'} style='display:none' {/if}>
			<p class="clear"></p>
		  </div>

		
		  <div id="versus"   {if $FabricArr.is_versus neq 'Y'} style='display:none' {/if}>
			<label>Versus:</label>
            <p class="clear"></p>			
			{if $VersusArr|@count neq 0}
		 	{section name=RowVersus loop=$VersusArr}
			{if $smarty.section.RowVersus.index%3 eq 0}
			<p class="clear"></p>
			<label>&nbsp;</label>
			{/if}
			<input type="checkbox" class="check" name="submited_versus[]"  value="{$VersusArr[RowVersus].versus_id}" 
{if $VersusArr[RowVersus].versus_id|inarray:$submited_versus}checked{/if} /> 
			<label class="chkoption">{$VersusArr[RowVersus].versus_title}</label>				
			{/section}
			{/if}			
		</div>
		
		  <div id="team_gap" {if $FabricArr.is_team neq 'Y'} style='display:none' {/if}>
			<p class="clear"></p>
		  </div>
		
		  <div id="team" {if $FabricArr.is_team neq 'Y'} style='display:none' {/if}>
			<label>Team:</label> 
			<label>&nbsp;</label>           			
			{if $TeamArr|@count neq 0}
		 	{section name=RowTeam loop=$TeamArr}
			{if $smarty.section.RowTeam.index%3 eq 0}
			<p class="clear"></p>
			<label>&nbsp;</label>			
			{/if}
			<input type="checkbox" class="check" name="submited_team[]"  value="{$TeamArr[RowTeam].team_id }" 
{if $TeamArr[RowTeam].team_id|inarray:$submited_team}checked{/if} /> 
			<label class="chkoption">{$TeamArr[RowTeam].team_title}</label>	
						
			{/section}
			{/if}		
		</div>
		
		
		  <div id="type_gap" {if $FabricArr.is_type neq 'Y'} style='display:none' {/if}>
			<p class="clear"></p>
		  </div>
		 
		 <div id="type" {if $FabricArr.is_type neq 'Y'} style='display:none' {/if}>
			<label>Type:</label>
            <p class="clear"></p>			
			{if $TypeArr|@count neq 0}
		 	{section name=RowType loop=$TypeArr}
            {if $smarty.section.RowType.index%3 eq 0}
			<p class="clear"></p>
			<label>&nbsp;</label>
			{/if}
			<input type="checkbox" class="check" name="submited_type[]"  value="{$TypeArr[RowType].type_id }" 
{if $TypeArr[RowType].type_id|inarray:$submited_type}checked{/if} /> 
			<label class="chkoption">{$TypeArr[RowType].type_title}{$smarty.section.RowType.index}</label>		
			{/section}
			{/if}			
		</div>
		
		  <div id="champion_gap" {if $FabricArr.is_champion neq 'Y'} style='display:none' {/if}>
			<p class="clear"></p>
		  </div>
	
		 <div id="champion" {if $FabricArr.is_champion neq 'Y'} style='display:none' {/if}>
			<label>Champion:</label>
            <p class="clear"></p>			
			{if $ChampionArr|@count neq 0}
		 	{section name=RowChampion loop=$ChampionArr}
			{if $smarty.section.RowChampion.index%3 eq 0}
			<p class="clear"></p>
			<label>&nbsp;</label>
			{/if}
			<input type="checkbox" class="check" name="submited_champion[]"  value="{$ChampionArr[RowChampion].champion_id}" 
{if $ChampionArr[RowChampion].champion_id|inarray:$submited_champion}checked{/if} /> 
			<label class="chkoption">{$ChampionArr[RowChampion].champion_title}	</label>	
			{/section}
			{/if}			
		</div>
		
		 <div id="class_gap" {if $FabricArr.is_class neq 'Y'} style='display:none' {/if}>
			<p class="clear"></p>
		  </div>
	
		 <div id="class" {if $FabricArr.is_class neq 'Y'} style='display:none' {/if}>
			<label>Class:</label>			
			{if $ClassArr|@count neq 0}
		 	{section name=RowClass loop=$ClassArr}
			{if $smarty.section.RowClass.index%3 eq 0}
			<p class="clear"></p>
			<label>&nbsp;</label>
			{/if}
			<input type="checkbox" class="check" name="submited_class[]"  value="{$ClassArr[RowClass].class_id}" 
{if $ClassArr[RowClass].class_id|inarray:$submited_class}checked{/if} /> 
			<label class="chkoption">{$ClassArr[RowClass].class_title}</label>			
			{/section}
			{/if}	
		</div>
		
		 <div id="map_gap" {if $FabricArr.is_map neq 'Y'} style='display:none' {/if}>
			<p class="clear"></p>
		  </div>
	
		 <div id="map" {if $FabricArr.is_map neq 'Y'} style='display:none' {/if}>
		<label>Map:</label>
        <p class="clear"></p>		
			{if $MapArr|@count neq 0}
		 	{section name=RowMap loop=$MapArr}
			{if $smarty.section.RowMap.index%3 eq 0}
			<p class="clear"></p>
			<label>&nbsp;</label>
			{/if}
			<input type="checkbox" class="check" name="submited_map[]"  value="{$MapArr[RowMap].map_id}" 
{if $MapArr[RowMap].map_id|inarray:$submited_map}checked{/if} /> 
			<label class="chkoption">{$MapArr[RowMap].map_title}</label>			
			{/section}
			{/if}				
		</div>
		
		 <div id="mode_gap" {if $FabricArr.is_mode neq 'Y'} style='display:none' {/if}>
			<p class="clear"></p>
		  </div>
	
		 <div id="mode" {if $FabricArr.is_mode neq 'Y'} style='display:none' {/if}>
		<label>Mode:</label>				
			{if $ModeArr|@count neq 0}
		 	{section name=RowMode loop=$ModeArr}
			{if $smarty.section.RowMode.index%3 eq 0}
			<p class="clear"></p>
			<label>&nbsp;</label>
			{/if}
			<input type="checkbox" class="check" name="submited_mode[]"  value="{$ModeArr[RowMode].mode_id}" 
{if $ModeArr[RowMode].mode_id|inarray:$submited_mode}checked{/if} /> 
			<label class="chkoption">{$ModeArr[RowMode].mode_title}	</label>	
			{/section}
			{/if}			
		</div>
		<p class="clear"></p>	
			
        <!--<label>Select Race :</label>
            <span id="race_id_td">
			<select name="race_id" id="race_id" class="con-req">
					<option value="">Select</option>
			</select>
			</span>
            <p class="clear"></p>
            <label>Select Ladder :</label>
			<span id="ladder_id_td">
            <select name="ladder_id" id="ladder_id" class="con-req">
					<option value="">Select</option>
			</select>
			</span>-->
			 <label>Video Type :</label>
            <select name="is_video_url" id="is_video_url">
			<option value='Y' {if $Formval.is_video_url eq 'Y'}selected{/if}> URL</option>
			<option value='N' {if $Formval.is_video_url eq 'N'}selected{/if}>Upload Video</option>
			</select>
			<p class="clear"></p>
			<label>Youtube Url :</label>
            <input name="video_url" id="video_url" type="text" value="{$Formval.video_url}" />
			<label>Time length (min):</label>
            <input name="time_length" id="time_length" type="text" value="{$Formval.time_length}" class="con-req small" onkeypress="return keyCheck(event, this);" />&nbsp;
			[mm.ss]
            <p class="clear"></p>
			<div style="padding-top:0px;padding-bottom:20px; padding-left:30px; font-size:12px;">[i.e http://www.youtube.com/watch?v=Di7G4nbpyDg]</div>
			<p class="clear"></p>
			{$DisplayVideoControlStr}
			{$DisplayImageControlStr}
			
			<label>Description :</label>
            <textarea  name="description" id="description">{$Formval.description}</textarea>
            <p class="clear"></p>
           
            <label>&nbsp;</label>
            <input name="submit" class="submit" value="{$SubmitButton}" type="submit" onclick='return CheckFields()' />
            <input name="" class="cancel" value="Cancel" type="button" onclick="window.location.href='upload_videos.php'" />
         </fieldset>
      </form>
      </div>
      
     </div>
     </div>
    </div>