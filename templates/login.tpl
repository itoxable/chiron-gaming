{literal}
<script type="text/javascript">
function chk_login()
{
	var email = jQuery('#login-email').val();
	var password = jQuery('#login-password').val();
	
	if(email == ""){
		jQuery('#FormErrorMsg').html("Please enter your email");
		jQuery('#login-email').css("border", "1px solid #FF0000");
		jQuery('#login-email').focus();	
		return false;	
	}
	else{
		jQuery('#FormErrorMsg').html("");
		jQuery('#login-email').css("border", "1px solid #000");		
	}
	if(password == "")
	{
		jQuery('#FormErrorMsg').html("Please enter your password");
		jQuery('#login-password').css("border", "1px solid #FF0000");
		jQuery('#login-password').focus();		
		return false;
	}
	else{
		jQuery('#FormErrorMsg').html("");
		jQuery('#login-password').css("border", "1px solid #000");	
	}
	
	Dologin(email,password);
	
	return false;
}

function Dologin(email,password)
{
	var retval1 = 1;
	var retval2 = 1;
	
	var frmID='#loginForm';
	
			var params ={

				'module': 'contact',

				'action': 'sendContactData'

			};

			var paramsObj = jQuery(frmID).serializeArray();

			
			/*j.each(paramsObj, function(i, field){

				params[field.name] = field.value;

			});*/
			params['email'] = email;
			params['password'] = password;

     
		if(retval1 && retval2)
		{
			
			jQueryj.ajax({

				type: "POST",

				url: 'processlogin.php',

				data: params,

				dataType: 'json',
				
				success: function(data){					
					
					if (data.flag == 1)
					{
						$.post("forum/ucp.php?mode=login&main_site=true",{"username":email,"password":password,"redirect":"","login":"Log in"}, function(data){
							window.location.href=document.getElementById('url').value;
						});
					}
					else
					{
						jQuery('#FormErrorMsg').html('Authentication failed');
					}

				}

		});
		}
}
</script>
{/literal}
<div class="right-panel">
      <div class="title">Login</div>
      <div class="clear"></div>
      
      <div class="register">
	  <div id='FormErrorMsg' style="color:red; padding-bottom:10px; padding-left:140px;">{if $ermsg neq ''} {$ermsg}{/if} </div>
      <form name="loginForm" id="loginForm" enctype="multipart/form-data" method='post'>
	  <input type="hidden" name="user_type" value="{$user_type}">
         <fieldset>
            <label>Email :</label>
            <input name="login-email" type="text" id="login-email" class="con-req" value="" />
			<p class="clear"></p>			
            <label>Password :</label>
            <input name="login-password" type="password" id="login-password" class="con-req" value="" />
            <p class="clear"></p>	
            <label>&nbsp;</label>
			<input type="hidden" name="url" id="url" value="{$url}" />
            <input name="submit" class="submit" value="Submit" type="submit" onclick='return chk_login()' />
            <input name="" class="cancel" value="Reset" type="reset" />
         </fieldset>
      </form>
      </div>      
     
      <div class="clear"></div>
   </div>