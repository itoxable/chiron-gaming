 {literal}
<script language="javascript" type="text/javascript">

function CheckFields()
{
  if(document.getElementById('game_id').value=='' ) 
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please select a game");
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
    return false;
   }
  return true;
}  

</script>
{/literal}

 <div class="right-panel">
      <div class="findstudent-title"><h1 title="Profile"><span>Registration</span></h1>
        <div class="breadcrumb">Add Game</div>
        <div class="clear"></div>
	  </div>
      <div class="register">
	   <div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:10px;">{if $ermsg neq ''} {$ermsg}{/if} </div>
      <form name="register" id="register" action="" method="post">
         <fieldset>
            <label>Select Game :</label>
            <select name="game_id" id="game_id" class="selectbox" >
				<option value="">Select</option>
				{html_options options=$GameArr selected=$Formval.game_id}
			</select>
            <p class="clear"></p>
            <label>Experience :</label>
            <textarea  name="experience" id="experience"></textarea>
            <p class="clear"></p>
           
            <label>&nbsp;</label>
            <input name="submit" class="submit" value="Submit" type="submit" onclick='return CheckFields()' />
            <input name="" class="cancel" value="Cancel" type="reset" />
         </fieldset>
      </form>
      </div>
      
      
      
      <div class="clear"></div>
   </div>