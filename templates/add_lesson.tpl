{literal}
<script language="javascript1.2" src="includejs/functions_admin.js" type="text/javascript"></script>

<script type="text/javascript">
jQuery(document).ready(function(){
		
focus_blur('Search_lesson_title','Search by title');
focus_blur('Search_from_date','From');
focus_blur('Search_to_date','To');
focus_blur('Search_from_date_in','From');
focus_blur('Search_to_date_in','To');
});

function GetTotalIncome(){
	if(document.getElementById('tot_incm').checked==true){
			document.getElementById('show_tot').style.display='block';
	}
	else{
			document.getElementById('show_tot').style.display='none';
	}
}

function GetTotalPrice(){
	if(document.getElementById('tot_price').checked==true){
			document.getElementById('price_tot').style.display='block';
	}
	else{
			document.getElementById('price_tot').style.display='none';
	}
}

function Search_Income(formID){

	var frmID='#'+formID;
	
	var params ={
	
		'module': 'contact'
	};

	var paramsObj = jQuery(frmID).serializeArray();

	jQuery.each(paramsObj, function(i, field){

		params[field.name] = field.value;

	});
	
	jQuery.ajax({
		type: "POST",
		url:  'income_search.php',
		data: params,
		dataType : 'json',
		success: function(data){
			if(data.total_in != ''){
				document.getElementById('lable_total').style.display="block";
				jQuery('#total_income').html(data.total_in);
			}
			else{
				document.getElementById('lable_total').style.display="block";
				jQuery('#total_income').html('0');
			}
        }
      });
}

function SearchLesson(formID){

	var frmID='#'+formID;

	var params ={
	
		'module': 'contact'
	};

	var paramsObj = v(frmID).serializeArray();

	jQuery.each(paramsObj, function(i, field){

		params[field.name] = field.value;

	});
	
	j.ajax({
		type: "POST",
		url:  'add_lesson.php?action=list_search&IsProcess=Y',
		data:  params,
		dataType : 'html',
		success: function(data){
			//alert(data);
			jQuery('#show').html(data);
        }
      });
}
</script>
{/literal}
<div id="paging"> 

<div id="show">
	
	<div class="clear"></div>
	
	
	
	<!--<div align="right"><p class="button-flex-big button150"><a href="lesson_update.php"><span>Add Lesson</span></a></p></div>-->
	<div align="right">
		{if $is_coach eq '0'}
		<!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
		<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
		{/if}
		{if $is_partner eq '0'}
		<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
		<!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
		{/if} 
	</div>
	<div class="content">
   		
		{* Start loading Tabs *}
		{include file='user_tabs.tpl' active_tab='coach'}
		{* End loading Tabs *}
		
        <div class="clear"></div>
        <div class="tabular-content"> 
		 <div class="total-message"><strong>Total Lessons : {$Numcoachgame}</strong>&nbsp;&nbsp;</div> 
		    
			<div class="tab_sub">
            	<ul>
					<li><a href="calendar.php">Availability</a></li>
                    <li><a href="coach_game.php">My Games</a></li>
					<li><a href="javascript:;" class="active">My Lessons</a></li>
                </ul>
			</div>

            
            <div class="clear"></div>  
			<div class="dashboard_area">
	<div class="dashboard_block new-income">
	
	<h2>Income Search</h2>
		<form method="POST" name="form_search_in" id="form_search_in" action="my_lesson.php" onsubmit="return false;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="new">
				<tr>
				<td width="30%" style="color:#000000; padding-top:10px;">Check to see total income: </td>
				<td width="70%" ><input type="checkbox" name="tot_incm" id="tot_incm" onclick="GetTotalIncome();" style="float:left;" />&nbsp;<span style="color:#000000; display:none; width:280px; float:left; padding-left:3px;" id="show_tot">Total Income: &nbsp;<b>$</b>{$tot_com}&nbsp;&nbsp;&nbsp;&nbsp;Total Price: &nbsp;<b>$</b>{$total_price}</span>
				</td>
				</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="new">
				<tr>
				<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
				<td colspan="2">&nbsp;</td></tr>
				<tr>
				<td width="60%" align="left" valign="middle">
					<div style="float:left;">
					<input type="text" name="Search_from_date_in" id="Search_from_date_in" class="datebox" value="{$Search_from_date_in}" style="width:80px;" readonly=""/>
					<!--<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search_in.Search_from_date_in,document.form_search_in.Search_from_date_in);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>-->
					</div>
					
					<div style="float:right;">
					<input type="text" name="Search_to_date_in" id="Search_to_date_in" class="datebox" value="{$Search_to_date_in}" style="width:80px;" readonly=""/>
					<!--<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search_in.Search_from_date_in,document.form_search_in.Search_to_date_in);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>-->
					</div>
				</td>
				<td valign="middle" style="padding-left:10px;">
					<input type="hidden" name="in_search" value="DO">
					
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				</tr>
				<tr>
				
				<td><input class="go"  name="submitdosearch" type="button" value="GO" onClick="Search_Income('form_search_in');" />
					<input class="reset" name="" type="button" value="RESET" onClick="window.location.href='add_lesson.php';"/>
					<span id="lable_total" style="display:none; float:left; padding-top:5px; padding-left:10px;">Total Income :&nbsp;<b>$</b>&nbsp;</span><span id="total_income" style="float:left; padding-top:5px;"></span>
					</td>
					
				
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<!--<tr>	
					<td colspan="2" valign="middle">
					<input type="hidden" name="in_search" value="DO">
					<input class="go"  name="submitdosearch" type="button" value="GO" onClick="Search_Income('form_search_in');" />
					<input class="reset" name="" type="button" value="RESET" onClick="window.location.href='my_lesson.php';"/></td>
				</tr>-->
			</table>
		</form>	
		</div>
		
		<div class="dashboard_block new">
		<h2>General Search</h2>
	  	<form method="POST" name="form_search" id="form_search" action="my_lesson.php" onsubmit="return false;">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr>
			  <td width="30%" align="left" valign="middle"><input size="30" type="text" name="Search_lesson_title" id="Search_lesson_title" value="{$Search_lesson_title}" class="textbox-search-box"/></td>
			  <td width="70%" align="left" valign="middle">
				<div style="float:left;">
					<input type="text" name="Search_from_date" id="Search_from_date" class="datebox" value="{$Search_from_date}" style="width:80px;"/>
					
					<!--<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_from_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>	-->			</div>
				<div style="float:left; padding-left:10px;">
					<input type="text" name="Search_to_date" id="Search_to_date" class="datebox" value="{$Search_to_date}" style="width:80px;"/>
			  <!--<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_to_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>	-->			</div>			  </td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr>
					<td width="30%" align="left" valign="middle" style="color:#000000; padding-left:5px;  ">
					<select name="Search_student_id" id="Search_student_id">
						<option value="">--Select Student--</option>
						{section name=Rows loop=$SelectStudentArr}
							<option value="{$SelectStudentArr[Rows].student_id}" {if $SelectStudentArr[Rows].student_id eq $Search_student_id} selected {/if}>{$SelectStudentArr[Rows].student_name}</option>
						{/section}
					</select>
			  </td>
					<td colspan="2" align="left" valign="middle" style="color:#000000; padding-left:5px; display:block;">
				<select name="Search_lesson_status" id="Search_lesson_status">
					<option value="">--Select Status--</option>
					{section name=Rows loop=$SelectStatusArr}
						<option value="{$SelectStatusArr[Rows].lesson_status_id}" {if $SelectStatusArr[Rows].lesson_status_id eq $Search_lesson_status} selected {/if}>{$SelectStatusArr[Rows].lesson_status_title}</option>
					{/section}
				</select>			  </td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>	
				<td valign="middle">
					<input class="go"  name="submitdosearch" value="GO" type="button" onClick="SearchLesson('form_search')" />
					<input class="reset" name="" type="button" value="RESET" onClick="window.location.href='add_lesson.php';"/>
				</td>
			</tr>
	  </table>
</form>
	  </div>
		
		<div class="clear"></div>
		  
	</div>                   
            <div>
                 
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row tabular-data" border="1">
				{if $Numcoachgame neq 0}
				  <tr style="border-bottom:#000000 1px solid; background:#ebebeb">
				  	<th width="10%" align="center">Title</th>
					<th width="10%" align="center">Player</th>
					<th width="5%" align="center">Points</th>
					<th width="5%" align="center">My Income</th>
					<th width="5%" align="center">Commission fee(%)</th>
					<th width="5%" align="center">Status</th>
					<th width="13%" align="center">Start date</th>
                                        <th width="12%" align="center">Date added</th>
				  </tr>
				  {section name=gameRow loop=$CoachgameArr}	  				
				  <tr>
				  	<td width="10%" align="center">{$CoachgameArr[gameRow].lesson_title}</td>
					<td width="10%" align="center">{if $CoachgameArr[gameRow].student_name neq ''}{$CoachgameArr[gameRow].student_name}{else}N/A{/if}</td>
					<td width="5%" align="center">{if $CoachgameArr[gameRow].lesson_price}${$CoachgameArr[gameRow].lesson_price}{else}N/A{/if}</td>
					<td width="5%" align="center">{if $CoachgameArr[gameRow].lesson_price}${$CoachgameArr[gameRow].my_commision}{else}N/A{/if}</td>
					<td width="5%" align="center">{if $CoachgameArr[gameRow].lesson_price}{$CoachgameArr[gameRow].lesson_commision_admin}{else}N/A{/if}</td>
					<td width="5%" align="center">{if $CoachgameArr[gameRow].lesson_status}{$CoachgameArr[gameRow].lesson_status}{else}N/A{/if}</td>
                                        <td width="13%" align="center">{if $CoachgameArr[gameRow].start neq '0000-00-00'}{$CoachgameArr[gameRow].start}{else}N/A{/if}</td>
					<td width="12%" align="center">{if $CoachgameArr[gameRow].start neq '0000-00-00'}{$CoachgameArr[gameRow].date_added}{else}N/A{/if}</td>
				  </tr>
	  			  {/section} 
				{else}
				  <tr>
                    <td colspan="6"><center><strong>No Record Exists</strong></center></td>
                  </tr>		
				{/if}                                
                </table>
            </div>
        	<div class="clear"></div>
          
        </div>
    </div>
</div>
{literal} 
<script>
	jQuery(function() {
		jQuery( "#Search_from_date_in" ).datepicker();
		jQuery( "#Search_to_date_in" ).datepicker();
		jQuery( "#Search_from_date" ).datepicker();
		jQuery( "#Search_to_date" ).datepicker();
	});
</script>
{/literal} 
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="DateRange/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-300px; top:0px;"></iframe>
</div>
<script language="javascript">
       changePageTitle('My lessons');
</script>