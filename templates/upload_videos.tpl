
<div id="paging"> 
<div class="button-flex-big button150"><a href="my_video_update.php"><span>Add Video</span></a></div>	 
  <div class="content">
   		
		{* Start loading Tabs *}
		{include file='user_tabs.tpl' active_tab='videos'}
		{* End loading Tabs *}
		
        <div class="clear"></div>
        <div class="tabular-content"> 
		 <div class="total-message"><strong>Total Videos : {$Numcoachvdo}</strong>&nbsp;&nbsp;</div>
		
		 <div class="tab_sub">
            	<ul>
                	<li><a href="javascript:;" >Upload Videos</a></li>
                    <li><a href="liked_videos.php" class="active">Liked Videos</a></li>
                </ul>
            </div> 
		 <div class="clear"></div>
	  
	   {if $Numcoachvdo >0}
	  {section name=vdoRow loop=$CoachvdoArr}
      <div class="profile-list" {if $smarty.section.vdoRow.index%2 neq 0}style="background-color:#f2f2f2;"{/if}>
	  <div class="game-image1">
	  <a href="videodetails.php?video_id={$CoachvdoArr[vdoRow].video_id}">
	  <img src="uploaded/video_images/thumbs/small_{$CoachvdoArr[vdoRow].video_image }" alt="Video" border="0" height="100" width="120" />
	  </a>
	  </div>
       <div class="game-details" {if $smarty.section.vdoRow.index%2 neq 0}style="background-color:#f2f2f2;"{/if}>
	        <h4>{$CoachvdoArr[vdoRow].video_title}</h4>
			 <ul class="games-desc" style="padding-top:0px;">
               <li>Name :{$CoachvdoArr[vdoRow].game}</li>
			   {if  $CoachvdoArr[vdoRow].ladder_id neq '' || $CoachvdoArr[vdoRow].ladder_id neq 0}
               <li><span>Ladder :</span> {$CoachvdoArr[vdoRow].ladder} </li>
			   {/if}
			    {if  $CoachvdoArr[vdoRow].team_id neq '' || $CoachvdoArr[vdoRow].team_id neq 0}
               <li><span>Team :</span> {$CoachvdoArr[vdoRow].team} </li>
			   {/if}
			    {if  $CoachvdoArr[vdoRow].map_id neq '' || $CoachvdoArr[vdoRow].map_id neq 0}
               <li><span>Map :</span> {$CoachvdoArr[vdoRow].map } </li>
			   {/if}
			    {if  $CoachvdoArr[vdoRow].class_id neq '' || $CoachvdoArr[vdoRow].class_id neq 0}
               <li><span>Class :</span> {$CoachvdoArr[vdoRow].class} </li>
			   {/if}			  
            </ul>
            <ul class="games-desc" style="padding-top:0px;">
               <!--<li><span>Race :</span> {$CoachvdoArr[vdoRow].race}</li>
			   <li>Duration: {$CoachvdoArr[vdoRow].time_length}</li>
			   {if  $CoachvdoArr[vdoRow].versus_id neq '' || $CoachvdoArr[vdoRow].versus_id neq 0}
               <li><span>Versus :</span> {$CoachvdoArr[vdoRow].versus} </li>
			   {/if}-->
			    {if  $CoachvdoArr[vdoRow].type_id neq '' || $CoachvdoArr[vdoRow].type_id neq 0}
               <li><span>Type :</span> {$CoachvdoArr[vdoRow].type} </li>
			   {/if}
			    {if  $CoachvdoArr[vdoRow].champion_id neq '' || $CoachvdoArr[vdoRow].champion_id neq 0}
               <li><span>Champion :</span> {$CoachvdoArr[vdoRow].champion} </li>
			   {/if}
			    {if  $CoachvdoArr[vdoRow].mode_id neq '' || $CoachvdoArr[vdoRow].mode_id neq 0}
               <li><span>Mode :</span> {$CoachvdoArr[vdoRow].mode} </li>
			   {/if}
            </ul>
			<p class="clear"></p>
            <p style="padding-top:5px;">{$CoachvdoArr[vdoRow].description}</p>
			<ul class="games-desc" style="padding-top:5px;">
               <li><span>Like :</span> {$CoachvdoArr[vdoRow].numlike}</li>
			</ul>   
			<ul class="games-desc" style="padding-top:5px;">
               <li><span>Dislike :</span> {$CoachvdoArr[vdoRow].numunlike}</li>
			</ul>   
		    <p class="clear"></p>
            <div style="height:10px;"></div>
            <div class="clear"></div>
            <div style="width:100%">
			<div class="view">
			<a href="#" onclick="ConfirmDelete('{$page_name}?action=del','{$CoachvdoArr[vdoRow].video_id}','{$CoachvdoArr[vdoRow].video_title}','your video:: ')">Delete</a></div>
			<div class="view" style="margin-right:5px;">
			<a href="my_video_update.php?video_id={$CoachvdoArr[vdoRow].video_id}">Edit</a></div></div>
        </div>
         <div class="clear"></div>
      </div>
      {/section}
	  {else}
	    <div class="game-details"> No video found </div>
     {/if}
      <div class="clear"></div>
   </div> 
 </div>  
</div>
