<div id="paging"> 
 <div class="right-panel">
      <div class="findstudent-title"><a href="news.php"><h1 title="News"><span>News</span></h1></a>
        <div class="breadcrumb">{$NewsArr.news_title}</div>
        <div class="clear"></div>
      </div>
	  <div class="clear"></div>
      <div class="game-list-block">
		  <div class="game-image-block">
		   <a href="newsdetails.php?news_id={$NewsArr.news_id}">
			{if $NewsArr.photo neq ''}
		     <img src="uploaded/news_images/thumbs/big_{$NewsArr.photo}" alt="{$NewsArr.news_title}" border="0"  />
			{else} 
			 <img src="images/news_big.png" alt="Student" border="0" />
			{/if}
		   </a>	
		  </div>
		  <div class="game-details-block">
            <h2><a href="newsdetails.php?news_id={$NewsArr.news_id}" style="text-decoration:none;">{$NewsArr.news_title}</a></h2>
			<ul class="games-desc" style="padding-top:0px;">
               <li><span>Date : </span>{$NewsArr.n_date}</li>
            </ul>
			 <p class="clear"></p>
            <p style="padding-top:5px; padding-bottom:10px;">{$NewsArr.news_description}</p>
           
            <div class="clear"></div>
         </div>
         <div class="clear"></div>
      </div>
	</div> 
   </div>
