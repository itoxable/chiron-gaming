<div id="show">
		
	 <div class="clear"></div>
		<div align="right">
			{if $is_coach eq '0'}
			<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
			{/if}
			{if $is_partner eq '0'}
			<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
			{/if} 
		</div>
		<div class="content"> 
   		
		{* Start loading Tabs *}
		{include file='user_tabs.tpl' active_tab='message'}
		{* End loading Tabs *}
		
        <div class="clear"></div>
        <div class="tabular-content" style="height: 580px;">
			<div class="total-message">&nbsp;&nbsp;</div> 
			<div class="clear"></div> 
			
			{if $smarty.session.user_id neq ''}
			<script type="text/javascript" src="jss/quiron_messages.js"></script> 
			{/if}

			<div class="content-wrapper" style="position: relative;margin-left: 100px;">
                            {if $messagesSize >0}
				<div class="messages_user_list" style="">
				{if $usersSize > 0}
	  				{section name=userIndex loop=$usersArr}
					
						<div class="messages_user_wrap {if $smarty.section.userIndex.index ==0}messages_user_wrap_selected{/if}" style="{if $smarty.section.userIndex.index ==0}border-top: 1px solid #CCC{/if}" id="message_user_{$usersArr[userIndex].user_id}">
							<div class="messages_user_image">
								<img width="35px" height="35px" src="{if $usersArr[userIndex].photo}uploaded/user_images/thumbs/mid_{$usersArr[userIndex].photo}{else}images/featured_img.jpg{/if}" alt="" border="0"  />
							</div>
							<div class="messages_user_name">
								{$usersArr[userIndex].username}
							</div>
						</div>
					{/section}
				{/if}
				</div>
				{if $groupedMessagesSize > 0}
					{foreach from=$groupedMessages key=userid item=messagegroup name=messagegroupname }
					<div class="messages_list_wrap" style="{if $smarty.foreach.messagegroupname.first}display: block{/if}" id="messages_list_{$userid|replace:'\'':''}">
						<div class="messages_list" id="inner_messages_list_{$userid|replace:'\'':''}">
							<div style="" id="inner_messages_list_wrap_{$userid|replace:'\'':''}">
							{foreach from=$messagegroup key=chave item=message}
								<div class="messages_wrap">
									<div class="messages_sender" >
										{$message.from_name}	
										<div class="messages_date" taux="{$message.time}">
											{$message.sent}				
										</div>
									</div>
									<div class="messages_content">
										{$message.message}
									</div>
								</div>
							{/foreach}
							</div>
						</div>
						<div class="send_message">
        					<textarea rows="4" cols="50" class="send_message_text" toid="{$userid|replace:'\'':''}" fromname="{$user_name}"  id="send_message_{$userid|replace:'\'':''}"></textarea>
							<div style="text-align: right">
								<span id="checkbox_for_{$userid|replace:'\'':''}" class="chironcheckbox" onclick="changeClass(event,this)">Press Enter to send</span>
								<a href="javascript:;" class="button_message button" toid="{$userid|replace:'\'':''}" fromname="{$user_name}">Send</a>
							</div>	
						</div>	
					</div>
					
					{/foreach}
				{/if}
                                {else}
                            <div>No messages......</div>
                        {/if}
			</div>
                        
		</div>
	</div>
</div>

{literal}
<script>

	jQuery('.messages_date').each(function(index) {
		var date = jQuery(this);
		var time = date.attr('taux');
		date.text(getSystemTimeForMessages(time));
	});

	function changeClass(event,element){
		if(jQuery(element).hasClass('selected'))
			jQuery(element).removeClass('selected');
		else
			jQuery(element).addClass('selected');
	}
	jQuery('.messages_list').each(function(index) {
		var jscroll = jQuery(this).jScrollPane({stickToBottom:true,maintainPosition:true});
		var api = jscroll.data('jsp');
		api.scrollToPercentY(100);
	});
	jQuery(".messages_user_wrap").click(function(){
		var aux = this.id;
		var mid = aux.replace("message_user_","messages_list_");
                    
		var inner = '#inner_'+mid;
		mid = "#"+mid;
		if(jQuery(mid).is(':visible'))
			return;
			
		jQuery(".messages_user_wrap").removeClass('messages_user_wrap_selected');
		jQuery(this).addClass('messages_user_wrap_selected');
		
		jQuery(".messages_list_wrap").hide('fast');
		jQuery(mid).show('fast');
		
		var jscroll = jQuery(inner).jScrollPane({stickToBottom:true,maintainPosition:true});
		var api = jscroll.data('jsp');
		api.scrollToPercentY(100);
                    
	});
	
	jQuery('.send_message_text').keyup(function(e) {
		var id = jQuery(this).attr('toid');
		if(jQuery("#checkbox_for_"+id).hasClass('selected')){
			if (e.keyCode == 13) {
				var textid = "#send_message_"+id;
				sendMessage(jQuery(textid).val(),id, jQuery(this).attr('fromname')); 
			}
		}
	});
	
	jQuery('.button_message').click(function(e) {
		var id = "#send_message_"+jQuery(this).attr('toid');
		sendMessage(jQuery(id).val(),jQuery(this).attr('toid'), jQuery(this).attr('fromname')); 
	});
        
        
        
	
</script>
{/literal}
<script language="javascript">
       window.messagetimer = setInterval('getlastMessages(\'{$lastId}\')',10000);
       
       changePageTitle('Messages');
</script>