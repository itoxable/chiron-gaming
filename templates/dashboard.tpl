{literal}
<script language="javascript">
function mark_unread(message_id)
{	
jQuery.ajax({
	   url:'reply.php',
	   data:'flag=2&message_id='+message_id,
	   type:'post',		  
	   success:function(resp){
	   //alert(resp);		   	
		//	jQuery('#reply_content_'+message_id).html(resp);											
	   } 		   
});
}
</script>
{/literal}
	<div class="content">
		
		{* Start loading Tabs *}
		{include file='user_tabs.tpl' active_tab='dashboard'}
		{* End loading Tabs *}
		
		<div align="right"><!--<p class="button-flex-big button90"><a href="coach_game_update.php"><span>Add Game</span></a></p></div-->
		
			{if $is_partner eq '0'}
			<a class="button" style="float: right" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
			<!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
			{/if} 
		    {if $is_coach eq '0'}
			<!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
			<a class="button" style="float: right" href="coach_game_update.php?profile=Y">Become a Coach</a>
			{/if}
			
			<div class="clear"></div>
	</div>
		<div class="content">
        <!-- tab -->
        <div class="clear"></div>
        <div class="tabular-content"> 
		 <div class="total-message">&nbsp;&nbsp;</div>              
           <div class="clear"></div>
           <div class="dashboard_area"> 
		   	
		   <div class="clear"></div>
		   <div class="dashboard_block new">
		       <h2>Messages</h2>
			   
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row" style="padding-top:10px;">
				{if $latest_msgArrNum>0}
				  {section name=msg loop=$latest_msgArr}	  				
                  <tr onClick="javascript:window.location.href='{$latest_msgArr[msg].link}'; 
				  mark_unread({$latest_msgArr[msg].message_id});" 
				  {if $latest_msgArr[msg].is_read eq 'Y'}class="readmessage"
				  {else}
				  	{if $smarty.section.msg.index%2 neq 0}class="alter"{/if}
				  {/if}
				  >
				   <!-- uploaded/user_images/thumbs/{$latest_msgArr[msg].photo}-->
                    <td width="50">
					{if $latest_msgArr[msg].photo neq ''}
					<img src="uploaded/user_images/thumbs/{$latest_msgArr[msg].photo}" border="0" alt="" />
					{else}
					<img src="images/avatar_smallest.jpg" border="0" alt="" />
					{/if}
					</td>
                    <td width="100"><strong>{$latest_msgArr[msg].from_name}</strong></td>
                    <td width="100" style="padding-right:10px;"><strong>{$latest_msgArr[msg].date_add}</strong></td>
                    <td>{$latest_msgArr[msg].message_text}</td>
                  </tr>	
				    
	  			{/section} 
				 <tr><td colspan="4">
				  <div class="button-flex-big button150"><a href="inbox.php"><span>View More</span></a></div></td></tr>	
				{else}
				  <tr>
                    <td colspan="4"><center><strong>No Record Exists</strong></center></td>
                  </tr>		
				{/if}                                
                </table>
            </div>  
		   <div class="dashboard_block new">
		   <h2>Video Reviews</h2>
			   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row" style="padding-top:10px;">
				{if $Numvideo> 0}
				  {section name=vdoreview loop=$videoReviewArr}	  				
                  <tr {if $smarty.section.vdoreview.index%2 neq 0}class="alter"{/if}>
				    <td width="50">
					{if $videoReviewArr[vdoreview].photo neq ''}
					<img src="uploaded/user_images/thumbs/{$videoReviewArr[vdoreview].photo}" border="0" alt="" />
					{else}
					<img src="images/avatar_smallest.jpg" border="0" alt="" />
					{/if}
					</td>
                    <td width="100"><strong>{$videoReviewArr[vdoreview].name}</strong><br />
					<i>{$videoReviewArr[vdoreview].user_type}</i></td>
					
                    <td width="100" style="padding-right:10px;"><strong>{$videoReviewArr[vdoreview].vr_date}</strong></td>
					<td>{$videoReviewArr[vdoreview].video_title}</td>
                  </tr>				   
	  			{/section} 
				 <tr><td colspan="4">
				  <div class="button-flex-big button150"><a href="my_video_review.php"><span>View More</span></a></div></td></tr>	
				{else}
				  <tr>
                    <td colspan="4"><center><strong>No Record Exists</strong></center></td>
                  </tr>		
				{/if}                                
                </table>
		   </div>                 
           
        	<div class="clear"></div>
           
			 <div class="dashboard_block new">
		       <h2>Videos</h2>
			   
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row" style="padding-top:10px;">
				{if $Numcoachvdo>0}
				  {section name=vdo loop=$CoachvdoArr}	  				
                  <tr {if $smarty.section.vdo.index%2 neq 0}class="alter"{/if} >
				   <!-- uploaded/user_images/thumbs/{$latest_msgArr[msg].photo}-->
                    <td width="50">
					{if $CoachvdoArr[vdo].video_image neq ''}
					<img src="uploaded/video_images/thumbs/{$CoachvdoArr[vdo].video_image}" border="0" alt="" width="37" height="27" />
					{else}
					<img src="images/avatar_smallest.jpg" border="0" alt="" />
					{/if}
					</td>
                    <td width="120"><strong>{$CoachvdoArr[vdo].video_title}</strong></td>
                    <td width="100" style="padding-right:10px;"><strong>{$CoachvdoArr[vdo].date_add}</strong></td>
                    <td>{$CoachvdoArr[vdo].game}</td>
                  </tr>	
				   			   
	  			{/section} 
				<tr><td colspan="4">
				  <div class="button-flex-big button150"><a href="upload_videos.php"><span>View More</span></a></div></td></tr>	
				{else}
				  <tr>
                    <td colspan="4"><center><strong>No Record Exists</strong></center></td>
                  </tr>		
				{/if}                                
                </table>
            </div>  
			{if $is_coach eq '1'}
		   <div class="dashboard_block new">
		   <h2>Lessons</h2>
			   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row" style="padding-top:10px;">
				{if $Numcoachlesson> 0}
				  {section name=lesson loop=$CoachlessonArr}	  				
                  <tr {if $smarty.section.lesson.index%2 neq 0}class="alter"{/if}>
				   <!-- uploaded/user_images/thumbs/{$latest_msgArr[msg].photo}-->
				    <td>{$CoachlessonArr[lesson].lesson_title}</td>
                    <td width="100"><strong>{$CoachlessonArr[lesson].student_name}</strong>
					</td>
                    <td width="50"><strong>$ {$CoachlessonArr[lesson].lesson_price}</strong></td>
                    <td width="100" style="padding-right:10px;"><strong>{$CoachlessonArr[lesson].date_add}</strong></td>
                   
                   
                  </tr>				   
	  			{/section} 
				 <tr><td colspan="4">
				  <div class="button-flex-big button150"><a href="add_lesson.php"><span>View More</span></a></div></td></tr>	
				{else}
				  <tr>
                    <td colspan="4"><center><strong>No Record Exists</strong></center></td>
                  </tr>		
				{/if}                                
                </table>
		   </div>
		   {else}
		    <div class="dashboard_block new">
		    <h2>Lessons</h2>
			   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row" style="padding-top:10px;">
				{if $NumLesson> 0}
				  {section name=mlesson loop=$myLesson}	  				
                  <tr {if $smarty.section.mlesson.index%2 neq 0}class="alter"{/if}>
				   <!-- uploaded/user_images/thumbs/{$latest_msgArr[msg].photo}-->
				    <td>{$myLesson[mlesson].lesson_title}</td>
                    <td width="100"><strong>{$myLesson[mlesson].coach_name}</strong>
					</td>
                    <td width="50"><strong>$ {$myLesson[mlesson].lesson_price}</strong></td>
                    <td width="100" style="padding-right:10px;"><strong>{$myLesson[mlesson].date_add}</strong></td>
                   
                   
                  </tr>				   
	  			{/section} 
				 <tr><td colspan="4">
				  <div class="button-flex-big button150"><a href="my_lesson.php"><span>View More</span></a></div></td></tr>	
				{else}
				  <tr>
                    <td colspan="4"><center><strong>No Record Exists</strong></center></td>
                  </tr>		
				{/if}                                
                </table>
		   </div>
           {/if}               
		   <div class="clear"></div>
           {if $is_coach eq '1'}
			 <div class="dashboard_block new">
		       <h2>Coach Games</h2>
			   
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row" style="padding-top:10px;">
				{if $Numcoachgame>0}
				  {section name=game loop=$CoachgameArr}	  				
                  <tr {if $smarty.section.game.index%2 neq 0}class="alter"{/if} >
				   <!-- uploaded/user_images/thumbs/{$latest_msgArr[msg].photo}-->
                    <td width="100" valign="top"><strong>{$CoachgameArr[game].game}</strong>
					</td>
                    <td width="50" valign="top"><strong>${$CoachgameArr[game].rate}</strong></td>
                    <td width="80" style="padding-right:10px;" valign="top"><strong>{$CoachgameArr[game].date_add}</strong></td>
                    <td valign="top">{$CoachgameArr[game].experience}</td>
                     
                   </tr>				   
	  			 {/section} 
				  <tr><td colspan="4">
				  <div class="button-flex-big button150"><a href="coach_game.php"><span>View More</span></a></div></td></tr>	
				{else}
				  <tr>
                    <td colspan="4"><center><strong>No Record Exists</strong></center></td>
                  </tr>		
				{/if}                                
                </table>
            </div>  
		   {/if}	
		   {if $is_partner eq '1'}
		   <div class="dashboard_block new">
		   <h2>Training Partner Games</h2>
			   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row" style="padding-top:10px;">
				{if $Numtpgame>0}
				  {section name=game loop=$tpgameArr}	  				
                  <tr {if $smarty.section.game.index%2 neq 0}class="alter"{/if} >
				   <!-- uploaded/user_images/thumbs/{$latest_msgArr[msg].photo}-->
                    <td width="120" valign="top"><strong>{$tpgameArr[game].game}</strong>
					</td>
                    <td width="100" style="padding-right:10px;" valign="top"><strong>{$tpgameArr[game].date_add}</strong></td>
                    <td valign="top">{$tpgameArr[game].experience}</td>
                     
                   </tr>				   
	  			 {/section}
				  <tr><td colspan="3">
				  <div class="button-flex-big button150"><a href="training_partner_game.php"><span>View More</span></a></div></td></tr>	 
				{else}
				  <tr>
                    <td colspan="3"><center><strong>No Record Exists</strong></center></td>
                  </tr>		
				{/if}                                
                </table>
		   </div>
           {/if}               
            <div class="clear"></div>   
            </div>         
        </div>
    </div>