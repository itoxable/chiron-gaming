<link type="text/css" href="style/jquery.timecalendar.css" rel="stylesheet" />
<script type="text/javascript" src="jss/jquery.timecalendar.js"></script>

<div id="paging">
	<div align="right">
			{if $is_coach eq '0'}
			<!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
			<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
			{/if}
			{if $is_partner eq '0'}
			<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
			<!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
			{/if} 
		</div>
	<div class="content">	
		
		{* Start loading Tabs *}
		{include file='user_tabs.tpl' active_tab='coach'}
		{* End loading Tabs *}
		
		<div class="clear"></div>
		<div class="tabular-content" style="height: 600px;">    
			<div class="tab_sub">
            	<ul>
					<li><a href="javascript:;" class="active">Availability</a></li>
                    <li><a href="coach_game.php">My Games</a></li>
					<li><a href="add_lesson.php">My Lessons</a></li>
                </ul>
			</div>
			{literal}{/literal}
			<div id="calendarwrapper" style=""></div>
			
			<script>
				showCalendar('{$userId}',"inpage");
			</script>
			
			
		</div>
   </div>
 </div>
<script language="javascript">
       changePageTitle('Availability');
</script>