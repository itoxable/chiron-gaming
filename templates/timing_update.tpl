{literal}
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jQuery.functions.pack.js"></script>
<script language="javascript" src="js/contact_us.js"></script>

<script language="javascript" type="text/javascript" src="sitemanager/js/datetimepicker.js"></script>
<script src="sitemanager/js/jquery.clockpick.1.2.7.js" ></script>
<link rel="stylesheet" href="sitemanager/style/jquery.clockpick.1.2.7.css" type="text/css">
<script>
jQuery(document).ready(function(){
jQuery("#clockpick1").clockpick({ valuefield: 'time_from',starthour: 0,endhour:23,minutedivisions:1 }); 
jQuery("#clockpick2").clockpick({ valuefield: 'time_to',starthour: 0,endhour:23,minutedivisions:1 }); 
});
</script>

<script language="javascript">

function CheckFields()
{
  
 
   if(document.getElementById('time_slot_id').value=='')
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please select time slot");
	//jQuery('#FormErrorMsg').fadeOut(2000);
    return false;
   }
    if(document.getElementById('time_from').value=='') 
   {
     jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please enter from time");
   // jQuery('#FormErrorMsg').fadeOut(2000);

    return false;
   }
   if(document.getElementById('time_to').value=='')
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please enter to time");
	//jQuery('#FormErrorMsg').fadeOut(2000);
    return false;
   }     
	
  return true;
 
}
</script>
{/literal}
 <div class="right-panel">
      <div><h1 title="Profile">{$SubmitButton} Timing</h1></div>
      <div class="clear"></div>
	   <div class="register">
	  <div id='FormErrorMsg' style="color:red; text-align:left; padding-bottom:10px; padding-left:100px;">{if $ermsg neq ''} {$ermsg}{/if} </div>
      <form name="contact" id="contact" method='post'>
	 
         <fieldset>
            <label>Time Slot :</label>
            <select name="time_slot_id" id="time_slot_id" class="selectbox">
				<option value="">Select</option>
				{html_options options=$TimeslotArr selected=$Formval.time_slot_id}
			</select>
            <p class="clear"></p>
			<label>Time From :</label>
            <input type="text" name="time_from" id="time_from" readonly="" value="{$Formval.time_from}" style="width:100px;"/>
            <a href="#"><img src="images/clock.jpg" id="clockpick1" align="left" class="PopcalTrigger" /> </a>
            <label>Time To:</label>
            <input type="text" name="time_to" id="time_to" readonly="" value="{$Formval.time_to}" style="width:100px;"/>
            <a href="#"><img src="images/clock.jpg" id="clockpick2" align="left" class="PopcalTrigger" /> </a>
            <p class="clear"></p>
            
            <label>&nbsp;</label>
            <input name="submit" class="submit" value="{$SubmitButton}" type="submit" onclick='return CheckFields()' />
            <input name="" class="cancel" value="Cancel" onclick="window.location.href='timing.php'" />
         </fieldset>
      </form>
      </div>
      
     
      
      <div class="clear"></div>
   </div>