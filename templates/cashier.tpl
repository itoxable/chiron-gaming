

<div id="paging">
    <div align="right">
		{if $is_coach eq '0'}
		<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
		{/if}
		{if $is_partner eq '0'}
		<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
		{/if} 
	</div>
	<div class="content" style="position: relative; ">	
		
		{* Start loading Tabs *}
		{include file='user_tabs.tpl' active_tab='cashier'}
		{* End loading Tabs *}
		
		<div class="clear"></div>
		<div class="tabular-content"> 
			<div class="total-message"><strong>Total Points : {$totalPoints}</strong>&nbsp;&nbsp;</div> 
			<div class="tab_sub">
            	<ul>
			<li><a id="cashierPanel" href="javascript:void(0)" class="cashierButton active" >Deposit</a></li>
                	<li><a id="withdrawPanel" href="javascript:void(0)" class="cashierButton " >Withdraw</a></li>
			<li><a id="historyPanel" href="javascript:void(0)" class="cashierButton " >Transaction History</a></li>
                        <li style="display: none"><a id="transfersPanel" href="javascript:void(0)" class="cashierButton " >Transfers/Donations</a></li>
                </ul>
			</div>
			{literal}
			<script>
                            changePageTitle('Deposit');
                            function clearWithdrawFields(){
                                  jQuery('#quantity').val('');  
                                  jQuery('#paypal').val(''); 
                                  jQuery('#password').val(''); 
                                  jQuery('#txtCaptcha').val(''); 
                            }
                            jQuery('.cashierButton').click(function () {

                                jQuery('.cashierButton').removeClass('active');
                                jQuery(this).addClass('active');
                                var id = jQuery(this).attr('id');

                                jQuery('.cashier-panel').css('display' ,'none');

                                if (id == 'cashierPanel') {
                                    changePageTitle('Deposit');
                                    jQuery("#points-presentation").css('display' ,'block');
                                } else if (id == 'historyPanel') {
                                    changePageTitle('Transaction History');
                                    jQuery("#historyWrapper").css('display' ,'block');
                                    loadTransactions();
                                } else if (id == 'withdrawPanel') {
                                    clearWithdrawFields();
                                    changePageTitle('Withdraw');
                                    jQuery("#withdrawFormWrapper").css('display' ,'block');
                                    document.getElementById('captcha').src='/captcha.php?'+Math.random();
                                }else if (id == 'transfersPanel') {
                                    jQuery("#transferFormWrapper").css('display' ,'block');
                                    document.getElementById('transfercaptcha').src='/captcha.php?'+Math.random();
                                }
                    
					
                            });
					
			</script>
			{/literal}
			<div class="clear"></div>
			<div>
                            <div class="profile-list cashier-panel" id="points-presentation" style="display:block">
                                <table id="points-presentation-layer">
                                    <tr>
                                        <td>
                                                {if $txMsg neq ''}
                                                <h2 style="text-align: center;color: red;">{$txMsg}</h2>	
                                                {/if} 
                                                <p>
                                                   Select the amount of points below to deposit <b>($1 USD = 1 Pt)</b>. Please make sure to return to Chiron Gaming through the transaction success page for your deposit to be processed properly.
                                                </p>
                                                <br/>
                                                <p>
                                                   You do not need a PayPal account to make a deposit - PayPal accepts direct credit/debit card payments. 
                                                </p>
                                                <br/>
                                        </td>
                                        <td rowspan="2" id="paypal-logo" align="center">
                                                <h2>&nbsp;</h2>
                                                <div>
                                                    <img src="./images/paypal-logo.png" />
                                                    <br/>
                                                    <img src="./images/cards.png" />
                                                </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="points-panel">
                                        {literal}
                                        <script>
                                            function a_onClick(n) {
                                                jQuery("div.choice-clicked").removeClass("choice-clicked");
                                                divSelected = jQuery(n);
                                                divSelected.addClass("choice-clicked");
                                            }
                    
                                            function processTx(element, txUrl) {
                                                theButton=jQuery(element);
                                                theChoiceSelected = jQuery("div[id^=point_].choice-clicked").attr('id');
                                                id= theChoiceSelected.replace("point_","");
                                                params = {id:id};

                                                //jQuery(window.location).attr('href', txUrl + "?" + jQuery.param(params));
                                                jQuery.ajax({
                                                    type:       "POST",
                                                    url:        txUrl,
                                                    data:       params,
                                                    async:      true,
                                                    success:    function( jqXHR, status) {
                                                                    form = jQuery(jqXHR);
                                                                    parent.jQuery('body').append(form);
                                                                    //theButton.append(form);
                                                                    form.submit();
                                                                 }
                                                });
                                        }
                                    </script>
                                    {/literal}
                                <div class="points-block" style="position: relative">
                                    <span id="points-title" class="rounded-corners">
                                             Choose the Amount of Points to Deposit 
                                    </span>
                                    <div class="points-wrapper" id="points-wrapper" style="position: absolute">
                                        <div class="" >
                                            <div id="point_0" class="points-item rounded-corners highlightit blue" onClick="a_onClick(this)">
                                                    <span class="point-value" >10</span>
                                            </div>
                                            <div id="point_1" class="points-item rounded-corners highlightit blue" onClick="a_onClick(this)">
                                                    <span class="point-value" >25</span>
                                            </div>
                                            <div id="point_2" class="points-item rounded-corners highlightit blue" onClick="a_onClick(this)">
                                                    <span class="point-value" >50</span>
                                            </div>
                                            <div id="point_3" class="points-item rounded-corners highlightit blue" onClick="a_onClick(this)">
                                                    <span class="point-value" >100</span>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div>
                                            <a href="javascript:;" id="points-buy-button" class="button" onClick="processTx(this, 'paypal.php')" >Buy Now</a>
                                        </div>
                                    </div>                                                                                  
                                </div>
                                {literal}
                                <script>
                                        function setUpLayout(){
                                                var width = jQuery("#points-wrapper").width();
                                                jQuery("#points-wrapper").width(width+10);
                                                jQuery("#points-wrapper").css("left", "50%")
                                                jQuery("#points-wrapper").css("margin-left", -1*(width/2));
                                        }
                                        setUpLayout();                                  
                                </script>
                                {/literal}
                            </td>
                        </tr>
                    </table>
					
                </div>
				<div class='cashier-panel' id='withdrawFormWrapper' style="padding">
                                    <form name="withdrawForm" id="withdrawForm" enctype="multipart/form-data" method='post'>
                                        <div class='user_details'>
                                            <div id='FormErrorMsg' style="color:red; padding-bottom:10px;"></div>
                                            <input type="hidden" name="totalpoints" id="totalpoints" value="<?php echo $_SESSION['totalPoints']; ?>">
                                            <dl>
                                                <dt>Amount:</dt>
                                                <dd><input name="quantity" id="quantity" type="text" value="" class="con-req"></dd>
                                            </dl>
                                            <dl>
                                                <dt>Paypal ID:</dt>
                                                <dd><input name="paypal" id="paypal" type="text" value="" class="con-req"></dd>
                                            </dl>
                                            <dl>
                                                <dt>Confirm Password (Chiron Gaming):</dt>
                                                <dd><input name="password" id="password" type="password" value="" class="con-req"></dd>
                                            </dl>
                                            
                                            
                                            <div style="margin-left: 130px">
                                                <img src="#" id="captcha" />
                                            </div>
                                            
                                            <a style="margin-left: 130px; " href="javascript:;" onclick="document.getElementById('captcha').src='/captcha.php?'+Math.random();document.getElementById('txtCaptcha').focus();" id="change-image">Not readable? Change text.</a>
                                            
                                            <dl style="margin-top: 15px">
                                                <dt>Security Code:</dt>
                                                <dd><input name="txtCaptcha" id="txtCaptcha" type="text" value="" /></dd>
                                            </dl>
                                           
                                            <dl>
                                                <dt>&nbsp;</dt>
                                                <dd><a class="button" style="" href="#" onclick="checkWithdrawForm()">Submit</a></dd>
                                            </dl>
                                        </div>   
                                    </form>
                                    {literal}
                                    <script>
                                        clearWithdrawFields();
                                    </script>
                                    {/literal}
				</div>
				<div class='cashier-panel' id='historyWrapper' style="padding: 15px; position: relative;">
                                    <table id="historyTable" style="width: 870px">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Date</th>
                                                <th>Points</th>
                                                <th>Comm.</th>
                                                <th>Description</th>
                                                <th>Type</th>
                                                <th>Method</th>
                                                <th>Status</th>
                                                <th>Before</th>
                                                <th>After</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    
				</div>
                                <div class='cashier-panel' id='transferFormWrapper' style="display: none">
                                    <form name="transferForm" id="withdrawForm" enctype="multipart/form-data" method='post'>
                                        <div class='user_details'>
                                            <div id='FormErrorMsg' style="color:red; padding-bottom:10px;"></div>
                                            <input type="hidden" name="totalpoints" id="totalpoints" value="<?php echo $_SESSION['totalPoints']; ?>">
                                            <dl>
                                                <dt>Amount:</dt>
                                                <dd><input name="quantity" id="quantity" type="text" value="" class="con-req"></dd>
                                            </dl>
                                           
                                            <dl>
                                                <dt>Confirm Password (Chiron Gaming):</dt>
                                                <dd><input name="password" id="password" type="password" value="" class="con-req"></dd>
                                            </dl>
                                            
                                            
                                            <div style="margin-left: 130px">
                                                <img src="#" id="transfercaptcha" />
                                            </div>
                                            
                                            <a style="margin-left: 130px; " href="javascript:;" onclick="document.getElementById('transfercaptcha').src='/captcha.php?'+Math.random();document.getElementById('txtCaptcha').focus();" id="change-image">Not readable? Change text.</a>
                                            
                                            <dl style="margin-top: 15px">
                                                <dt>Security Code:</dt>
                                                <dd><input name="txtCaptcha" id="transfertxtCaptcha" type="text" value="" /></dd>
                                            </dl>
                                           
                                            <dl>
                                                <dt>&nbsp;</dt>
                                                <dd><a class="button" style="" href="#" onclick="checkWithdrawForm()">Submit</a></dd>
                                            </dl>
                                        </div>   
                                    </form>
                                    {literal}
                                    <script>
                                        clearWithdrawFields();
                                    </script>
                                    {/literal}
				</div>
                                
                                
			</div>
			<div class="clear"></div>
		</div>
   </div>
 </div>     