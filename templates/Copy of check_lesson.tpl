{literal}
<script type="text/javascript">
function ChangeStatus(id){

var params ={

		'module': 'lesson_payment',
		'lesson_id': id
	};

jQuery.ajax({
		type: "POST",
		url: 'lesson_payment.php?action=paid',
		data: params,
		dataType : 'json',
		success: function(data){
		if (data.flag == '1')
		  {
		    $('#msgbox').fadeIn(0);
		    $('#msgbox').html('Payment Successful.');
		    $('#msgbox').fadeOut(5000);
			location.reload();
		  }
        }
      });
}
</script>
{/literal}
<div id="paging">
<div class="right-panel">
      <h1 title="Games"><span>My Lesson</span></h1>
      <div class="clear"></div>
	  <!--<p class="button-flex-big button90"><a href="my_lesson_update.php"><span>Add Lesson</span></a></p>-->
	  <div id="msgbox" style="color:#00CC00;"></div>
	 {if $Numcoachgame >0}
	  {section name=gameRow loop=$CoachgameArr}
      <div class="game-list">
       <div class="game-details">
	        <h4>{$CoachgameArr[gameRow].lesson_title}</h4>
			 <ul class="games-desc" style="padding-top:0px;">
			   <li><span>Date :</span> {if $CoachgameArr[gameRow].date_added neq '0000-00-00'}{$CoachgameArr[gameRow].date_added|date_format:"%D"}{else}N/A{/if}</li> 
			   
			   {if $CoachgameArr[gameRow].coach_name neq ''}
			   <li><span>Student :</span> {if $CoachgameArr[gameRow].coach_name neq ''}{$CoachgameArr[gameRow].coach_name}{else}N/A{/if}</li>
			   {/if}
			   
			   {if $CoachgameArr[gameRow].lesson_price neq '0.00'}
               <li><span>Price :</span> {if $CoachgameArr[gameRow].lesson_price}{$CoachgameArr[gameRow].lesson_price}{else}N/A{/if} </li>
			   {/if}
			   {if $CoachgameArr[gameRow].lesson_status neq ''}
               <li><span>Status :</span> {if $CoachgameArr[gameRow].lesson_status}{$CoachgameArr[gameRow].lesson_status}{else}N/A{/if} </li>
			   {/if}
            </ul>
			<p class="clear"></p>
            <div style="height:10px;"></div>
            <div class="clear"></div>
            <div style="width:100%; padding-left:150px;">			
			{if $CoachgameArr[gameRow].lesson_status eq 'Open'}
			<div class="view">
			<a href="#" onclick="ChangeStatus('{$CoachgameArr[gameRow].lesson_id}')">
			Pay</a></div>{/if}
			<div class="view" style="margin-right:5px;">
			<a href="check_lesson_update.php?lesson_id={$CoachgameArr[gameRow].lesson_id}">View</a></div></div>
         </div>
         <div class="clear"></div>
      </div>
      {/section}
	  {else}
	    <div class="game-details"> No game found </div>
     {/if}
      <div class="clear"></div>
   </div>
 </div>     