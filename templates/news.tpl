{literal}
<script type="text/javascript">
function redirectPg(nid)
{
   window.location.href="newsdetails.php?news_id="+nid;
}
</script>
{/literal}
<div id="paging"> 
 <div class="right-panel">
      <h1 title="News"><span>News</span></h1>
      <div class="clear"></div>
 
	 {if $NumNews >0}
	  {section name=newsRow loop=$NewsArr}
      <div class="game-list-block" onclick="redirectPg({$NewsArr[newsRow].news_id})">
		  <div class="game-image-block">
		   <a href="newsdetails.php?news_id={$NewsArr[newsRow].news_id}">
			{if $NewsArr[newsRow].photo neq ''}
		     <img src="uploaded/news_images/thumbs/big_{$NewsArr[newsRow].photo}" alt="{$NewsArr[newsRow].title}" border="0"  />
			{else} 
			 <img src="images/news_big.png" alt="Student" border="0" />
			{/if}
		   </a>	
		  </div>
		  <div class="game-details-block">
            <h2><a href="newsdetails.php?news_id={$NewsArr[newsRow].news_id}" style="text-decoration:none;">{$NewsArr[newsRow].news_title}</a></h2>
			<ul class="games-desc" style="padding-top:0px;">
               <li><span>Date :</span>{$NewsArr[newsRow].n_date}</li>
            </ul>
			 <p class="clear"></p>
            <p style="padding-top:5px; padding-bottom:10px;">{$NewsArr[newsRow].news_content}</p>
           
            <div class="clear"></div>
				<!--<div class="view"><!--a href="studentdetails.php?student_id={$StudentArr[studentRow].user_id}">view</a-->
		<!--<a href="newsdetails.php?news_id={$NewsArr[newsRow].news_id}">view</a>
			</div>-->
         </div>
         <div class="clear"></div>
      </div>
	 {/section} 
     {if $pagination_arr[1]}
	 <div class="clear"></div>
      <div class="pagin">
         <ul>
           {$pagination_arr[1]}
         </ul>
      </div>
	 {/if}
	 {else}
	    News not found......
     {/if}
	</div> 
   </div>
