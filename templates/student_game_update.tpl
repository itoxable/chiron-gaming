 {literal}
 <script language="javascript" src="js/contact_us.js"></script> 
<script language="javascript" type="text/javascript">

function CheckFields()
{ 
  if(jQuery("#game_id").isEmpty())
    	 jQuery("#game_id").attr('style','border:1px solid #FF0000');
  if(jQuery("#comment").isEmpty())
    	 jQuery("#comment").attr('style','border:1px solid #FF0000');
  if(document.getElementById('game_id').value=='' ) 
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please select a game");
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
    return false;
   }
  if(document.getElementById('comment').valuue == '')
   {
    jQuery('#FormErrorMsg').show();
	jQuery('#FormErrorMsg').html("Please enter comment");
	
	return false;
   }	 
  return true;
 }  
</script>
{/literal}

 <div class="right-panel">
      <div class="findstudent-title"><a href="student_game.php"><h1 title="Profile"><span>My Games</span></h1></a>
        <div class="breadcrumb">{$SubmitButton} Game</div>
        <div class="clear"></div>
	  </div>
      
      <div class="register">
	   <div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:10px;">{if $ermsg neq ''} {$ermsg}{/if} </div>
      <form name="game" id="game" action="" method="post">
	  <input type="hidden" name="user_game_id" value="{$user_game_id}" />
         <fieldset>
            <label>Select Game :</label>
            <select name="game_id" id="game_id" class="con-req">
				<option value="">Select</option>
				{html_options options=$GameArr selected=$Formval.game_id}
			</select>
			{if $user_game_id neq ''}
			<label>Active :</label>
			<input name="is_active" id="is_active" type="checkbox" value="Y" {if $Formval.is_active =='Y'}checked{/if}>
			{/if}
			<p class="clear"></p>
			<label>Experience :</label>
			<textarea  name="experience" id="experience">{$Formval.experience}</textarea>
            <!--input name="experience" id="experience" type="text" value="{$Formval.experience}" /-->
           
			<!--p class="clear"></p>
            <label>Comment :</label>
            <textarea  name="comment" id="comment" class="con-req">{$Formval.comment}</textarea-->
            <p class="clear"></p>
           
            <label>&nbsp;</label>
            <input name="submit" class="submit" value="{$SubmitButton}" type="submit" onclick='return CheckFields()' />
            <input name="" class="cancel" value="Cancel" type="button" onclick="window.location.href='student_game.php'" />
         </fieldset>
      </form>
      </div>
      
      
      
      <div class="clear"></div>
   </div>