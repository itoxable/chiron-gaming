{literal}
<script type="text/javascript" language="javascript">

jQuery(document).ready(function(){


/* right panel filter more/less links */
	var filterThreshold = 5;
	jQuery('.listing').each(function(i){
	if(jQuery(this).find('a.selected').length == 0){
		jQuery(this).find('li:gt('+(filterThreshold-1)+')').hide();
		if(jQuery(this).find('li').length > filterThreshold)
			jQuery(this).after('<li class="more"><div class="button" style="float:right;margin: 5px;">+ more</div></li><li style="clear:both; width:100%"></li>');
		else
			jQuery(this).after('');
	}
	});

	jQuery('.more').each(function(i){
		jQuery(this).toggle(function(){
		jQuery(this).prev().find('li:gt('+(filterThreshold-1)+')').slideDown('fast');
		jQuery(this).html('<li class="more"><div class="button" style="float:right;margin: 5px;">- less</div></li><li style="clear:both; width:100%"></li>');
		},function(){
		jQuery(this).prev().find('li:gt('+(filterThreshold-1)+')').slideUp('fast');
		jQuery(this).html('<li class="more"><div class="button" style="float:right;margin: 5px;">+ more</div></li><li style="clear:both; width:100%"></li>');
		});
	});
	jQuery('.refine').click(function(){
	
		var val = jQuery(this).attr('typeId');
		var type = jQuery(this).attr('stype');
		
		if(type =='G')
		{
			if(jQuery('#game_'+val).attr('checked'))
			jQuery('#game_'+val).attr('checked','');
			else
			jQuery('#game_'+val).attr('checked','true');
		}			
		else if(type =='A')
		{
			if(jQuery('#avail_'+val).attr('checked'))
			jQuery('#avail_'+val).attr('checked','');
			else
			{
			  jQuery('#avail_'+val).attr('checked','true');
	
		    }		 
		}
		else
		{
			if(jQuery('#language_'+val).attr('checked'))
			jQuery('#language_'+val).attr('checked','');
			else
			jQuery('#language_'+val).attr('checked','true');
		}
		
		//document.frmRefine.submit();
		frmRefine_POST();
	});
  });	
  function send_value()
  {
    var price = jQuery('#Slider1').val();
	alert(price);
  }
  function frmRefine_POST()
  {
   
	var data = jQuery(document.frmRefine).serialize();
    jQuery.ajax({
	type: "GET",
	url: 'findstudent2.php',
	data: data,			
	dataType: 'html',
	success: function(data)
	{				
	   jQuery('.container-inner').html(data+"<div class='clear'></div>");
	   //alert(data);
		
	}
   });
  }
  function subForm(a){
	document.getElementById('listfor').value='sorting';
	document.getElementById('sortingby').value=a;
	goto();
}
function avail_submit(c){
	document.getElementById('availability_country').value=c;
	goto();
}
function goto(){
	frmRefine_POST();
}
</script>


<link rel="stylesheet" href="slider/stylesheets/jslider.css" type="text/css">
<link rel="stylesheet" href="slider/stylesheets/jslider.plastic.css" type="text/css">
<script type="text/javascript" src="slider/javascripts/jquery.dependClass.js"></script>
<script type="text/javascript" src="slider/javascripts/jquery.slider-min.js"></script>
{/literal}
<div class="left-panel">
   <div class="graphite demo-container">
	<ul class="accordion" id="accordion-1">
	<form action="findstudent.php" method="post" name="frmRefine">
	<input type="hidden" name="list_for" id="listfor" value="" />
	<input type="hidden" name="sorting_by" id="sortingby" value=""/>
	<input type="hidden" name="availability_country" id="availability_country" value=""/>
	<input type="hidden" name="action" value="send" />
	
	 {if $Numgame neq '0'}
	    <li><a href="#">Game</a>
        <ul>	
		 <div class="listing">
				 {section name=game loop=$gameArr}
				   	<li>
					<input type="checkbox" name="game_id[]" id='game_{$gameArr[game].game_id}' value="{$gameArr[game].game_id}" style="display:none" 
					{if $gameArr[game].game_id|in_array:$game_id} checked="checked" {/if}/>
					<a href="#" class="refine {if $gameArr[game].game_id|in_array:$game_id} selected {/if}" stype="G" typeId="{$gameArr[game].game_id}"> 
					{$gameArr[game].game_name} ({$gameArr[game].cnt}) </a> </li>
                 {/section}
				 
		 </div>		 
		 </ul>
	   </li>	 
	 {/if}	
		<li><a href="#">Budget ($)</a>
           <ul>	
				  <li style="height:30px; padding:15px 5px 5px 5px;"> 
				   <span style="display: inline-block; width: 230px; padding: 0 5px;">
				   <input id="Slider1" type="slider" name="price" value="{$price}"/></span> 
                    {literal}
					 <script type="text/javascript" charset="utf-8">
					  jQuery("#Slider1").slider({ from: 0, to: 100, limits: false, step: 1, dimension: '', skin: "plastic", callback: 
					  function( value ){ /*document.frmRefine.submit()*/ frmRefine_POST(); } });
					</script>
					{/literal}
				 </li>	
		   </ul>
		 </li>  
		 
	   {if $Numlanguage neq '0'}	
	     <li><a href="#">Language</a>
            <ul>
			 <div class="listing">
                   
				  {section name=lang loop=$languageArr}
				  	<li>
					<input type="checkbox" name="language_id[]" id="language_{$languageArr[lang].language_id}" value="{$languageArr[lang].language_id}" style="display:none;" 
					{if $languageArr[lang].language_id|in_array:$language_id} checked="checked" {/if}/>				
					<a href="#" class="refine {if $languageArr[lang].language_id|in_array:$language_id} selected {/if}" stype="L" typeId="{$languageArr[lang].language_id}" >
					{$languageArr[lang].language_name} ({$languageArr[lang].cnt})</a></li>
                  {/section}
			 </div>	 	  
			</ul>
		</li>	
	  {/if}	
	  <li><a href="#">Availability</a>
           <ul>
		   <div class="listing">
			 
			  <li> <input type="checkbox" name="availability[]" id="avail_O" value="O" style="display:none" {if 'O'|in_array:$availability} checked="checked" {/if} />
			  <a href="#" class="refine {if 'O'|in_array:$availability} selected {/if}" stype="A" typeId="O"> Online({$avail1Num})</a></li>
			  <li> <input type="checkbox" name="availability[]" id="avail_L" value="L" style="display:none" {if 'L'|in_array:$availability} checked="checked" {/if} />
			  <a href="#" class="refine {if 'L'|in_array:$availability} selected {/if}" stype="A" typeId="L" > Local meet-up({$avail2Num})</a></li>
			  <li id="avail_coun" {if 'L'|in_array:$availability} style="display:block;" {else} style="display:none;" {/if}>
			  <select name="avail_country" id="avail_country" class="drop-down" onchange="avail_submit(this.value)">
			  <option value="">Availability Country</option>
			   {html_options options=$AvailcounArr selected=$availability_country}
			  </select>
			  </li>
			</div>  
		   </ul>
		</li>   
	  <input type="hidden" name="record_per_page" id="record_per_page" value="{$record_per_page}" style="visibility:hidden">

	  <input type="submit" style="visibility:hidden">
	 </form> 	
    </ul>
   </div>   
   </div>