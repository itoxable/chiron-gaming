<div class="right-panel">
      <h1 title="Find Student"><span>Registration</span></h1>
      <div class="clear"></div>
      
      <div class="registration-student">
         <div class="register-img"><img src="uploaded/cms_images/{$StudentArr.cms_image}" alt="" border="0" /></div>
         <br />
         <div class="student-title"><h1 class="Student"></h1></div>
         <div class="clear"></div>
         <div class="register-txt"><p>
		 {$StudentArr.desc}
		 </p></div>
		 <div class="next"><a href="registration.php?user_type=2">Go</a></div>
      </div>
      <div class="registration-coach">
         <div class="register-img"><img src="uploaded/cms_images/{$CoachArr.cms_image}" alt="" border="0" /></div>
         <br />
          <div class="coach-title"><h1 class="Coach"></h1></div>
          <div class="clear"></div>
         <div class="register-txt"><p>{$CoachArr.desc}</p></div>
		  <div class="next"><a href="registration.php?user_type=1">Go</a></div>
      </div>
     
      <div class="clear"></div>
   </div>