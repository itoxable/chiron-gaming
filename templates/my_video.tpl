<div id="paging"> 
<div class="right-panel">
      <h1 title="Videos"><span>My Favourite Videos</span></h1>
      
      <div class="clear"></div>
	  {if $NumVideo >0}
	  <div class="game-list">
	   {section name=videoRow loop=$VideoArr}
	    {assign var='a' value=$smarty.section.videoRow.index+1}
         <div {if $a%3 eq '0'}class="games-replay-last"{else}class="games-replay"{/if}>
           <div class="replay-content" >
			  <!-- <a href="videodetails.php?video_id={$ReplayArr[videoRow].video_id}">-->
			   <a href="videodetails.php?video_id={$VideoArr[videoRow].video_id}" >
			   <img src="uploaded/video_images/thumbs/big_{$VideoArr[videoRow].video_image}" alt="" border="0" /></a>
			   
			   </div>
			   <div class="replay-content">
				  <div class="replay-title">
				  <a href="videodetails.php?video_id={$VideoArr[videoRow].video_id}">{$VideoArr[videoRow].video_title}</a>
				  <br /><span class="date">{$VideoArr[videoRow].video_date}<br />
				  Duration : {if $VideoArr[videoRow].time_length neq ''}{$VideoArr[videoRow].time_length} min{else}N/A{/if}
				  </span></div>
				  <div class="game-rating">{$VideoArr[videoRow].star}<br />
				  View Count : {$VideoArr[videoRow].view_count}
				  </div>
                  <div class="replay-title" style="padding-top:0px; width:90%"><span class="date">Uploaded by <em>{$VideoArr[videoRow].user}</em></span></div>
			   </div>
         </div>
		 {if $a%3 eq '0'}<div class="clear"></div>{/if}
		 {/section}
        <div class="clear"></div>
      </div>
      {if $pagination_arr[1]}
      <div class="clear"></div>
      <div class="pagin">
         <ul>
             {$pagination_arr[1]}
         </ul>
      </div>
	 {/if}
	 {else}
	    No Video is found......
     {/if} 
   </div>
</div>   