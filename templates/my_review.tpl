   <div id="paging">  
	<div class="content">
   		
		{* Start loading Tabs *}
		{include file='user_tabs.tpl' active_tab='reviews'}
		{* End loading Tabs *}
		
        <div class="clear"></div>
        <div class="tabular-content">
            <div class="tab_sub">
            	<ul>
                	<li><a href="javascript:;">My Reviews</a></li>
					<li><a href="my_video_review.php" class="active">My Video Reviews</a></li>
					<li><a href="coach_review.php" class="active">Reviews to User</a></li>
					<li><a href="video_review.php" class="active">Reviews to video</a></li>
                    <!--li><a href="my_video_review.php">My Videos</a></li-->
					<!--li><a href="#">Coach Reviews by me</a></li-->
					<!--li><a href="#">Video Reviews by me</a></li-->
                </ul>
            </div>
			
           
            <!--div class="tab_pagination">
            	<span class="page-records"><strong>1 - 20 of 60</strong></span> <span class="left-arr"><a href="#"></a></span><span class="right-arr"><a href="#"></a></span>
            </div-->   
            <div class="clear"></div>  
			{if $NummyReview>0}                   
            <div>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row">
				{section name=myrevrow loop=$myReviewArr}
                  <tr {if $smarty.section.myrevrow.index%2 neq 0}class="alter"{/if}>
                    <td width="50" valign="top">
					{if $myReviewArr[myrevrow].photo neq ''}
					<img src="uploaded/user_images/thumbs/{$myReviewArr[myrevrow].photo}" border="0" />
					{else}
					<img src="images/avatar_smallest.jpg" border="0" />
					{/if}</td>
                    <td width="100" valign="top"><strong><a href="{$myReviewArr[myrevrow].link}">{$myReviewArr[myrevrow].name}</a></strong><br />
					<i>{$myReviewArr[myrevrow].user_type}</i>
					</td>
                    <td width="100" valign="top" style="padding-right:10px;"><strong>{$myReviewArr[myrevrow].review_date}</strong></td>
					<td width="250" valign="top" style="padding-right:10px;">{$myReviewArr[myrevrow].review_comment|nl2br}</td>
                    <td valign="top">
						<div class="review_card_block">
                 <!-- Score Area -->
                <div id="scoreCard">
                    <ul>
						{if $myReviewArr[myrevrow].rating neq 0}
                        <li id="Overall" class="tTip">
						<span class="someClass" title="{$UserReview[rrow].rating_overall_tooltip}">
						<strong>{$myReviewArr[myrevrow].rating}</strong><span>Overall</span>
						</span>
						</li>
						{/if}
						{section name=k loop=$myReviewArr[myrevrow].individual}
						{if $myReviewArr[myrevrow].individual[k].rating_category eq 'Knowledge'}
                        <li id="Knowledge" class="tTip">
						<span class="someClass">
						<strong>{$myReviewArr[myrevrow].individual[k].rating}</strong>
						<span>{$myReviewArr[myrevrow].individual[k].rating_category}</span>
						</span>
						</li>
						{/if}
						{if $myReviewArr[myrevrow].individual[k].rating_category eq 'Structure'}
                       <li id="Structure" class="tTip">
					   <span class="someClass" >
					   <strong>{$myReviewArr[myrevrow].individual[k].rating}</strong><span>Structure</span>
					   </span>
					   </li>
					   {/if}
					   {if $myReviewArr[myrevrow].individual[k].rating_category eq 'Communication'}
                        <li id="Communication" class="tTip">
						<span class="someClass">
						<strong>{$myReviewArr[myrevrow].individual[k].rating}</strong><span>Communication</span>
						</span>
						</li>
						{/if}
						{if $myReviewArr[myrevrow].individual[k].rating_category eq 'Professionalism'}
                         <li id="Professionalism" class="tTip">
						 <span class="someClass" title="{$UserReview_Individual[rrow][k].rating_category_tooltip}">
						 <strong>{$myReviewArr[myrevrow].individual[k].rating}</strong><span>Professionalism</span>
						 </span>
						 </li>
						 {/if}
						{/section}
                    </ul>
                </div>
                </div>
					</td>
					<!--<td valign="top">{$myReviewArr[myrevrow].review_comment|nl2br}</td>
                    <td width="100" align="right" valign="top"><strong>{$myReviewArr[myrevrow].rating}</strong></td>-->
                  </tr>
                 {/section}                          
                </table>
				 
            </div>
        	<div class="clear"></div>
            <br />
            <div class="tab_sub">
            	&nbsp;
            </div>
            <div class="tab_search">
            	&nbsp;
            </div>
            <div class="tab_pagination">
            	{if $pagination_arr[1]}
				 <div class="clear"></div>
				  <div class="pagin">
					 <ul>
					   {$pagination_arr[1]}
					 </ul>
				  </div>
				 {/if}
            </div>   
            <div class="clear"></div> 
			{else}
				<div align="left" style="padding:10px;">No record found</div>
			{/if}
      </div>
   </div>
</div>