{literal}
<script language="javascript">
function saveAbout(){
    window.location= "training_partner_game.php?action=ab&type=3&about="+jQuery('#about-text').val();
}
</script>
{/literal}
<div id="paging">

	<div align="right">
		{if $is_coach eq '0'}
		<!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
		<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
		{/if}
		{if $is_partner eq '0'}
		<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
		<!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
		{/if} 
	</div>
	<div class="content">
			
			{* Start loading Tabs *}
			{include file='user_tabs.tpl' active_tab='training partner'}
			{* End loading Tabs *}
			
			<div class="clear"></div>
			<div class="tabular-content"> 
                            <div class="total-message">
                                    <a href="training_partner_game_update.php" class="button">Add Game</a>&nbsp;
                                    <strong>Total Games : {$Numcoachgame}</strong>&nbsp;&nbsp;
                            </div> 
                            <div class="clear"></div>                     
                            <div>
                                <div id="about" class="coach_game_details" style="margin-top: 30px;margin-left: 30px;margin-bottom: 30px;">
                                    <dl>
                                        <dt>Introduction:</dt>
                                        <dd >
						<div id="about-div"> {if $about neq ''}{$about}{else}N/A{/if}</div>
						<div style="margin-top: 10px;width: 588px;color: red;border: 1px solid red;padding: 3px;"><center>This Intro will be displayed on the Find Training Partner page before users view your full profile. <br>There is a 300-character limit, so write carefully!<br></center></div>
					</dd>
                                    </dl>
                                    {literal}
                                    <a href="javascript:;" class="button" style="float: right" onclick="jQuery('#about').hide(0,function(){jQuery('#about-edit').show(0)})">Edit</a>
                                    {/literal}
                                </div> 
                                <div id="about-edit" class="coach_game_details" style="margin-top: 30px;margin-left: 30px;margin-bottom: 30px;">
                                    <dl>
                                        <dt>Introduction:</dt>
                                        <dd>
                                            <textarea id="about-text" maxlength="300" onkeyup="countLenght(this,'about-lenght')">{$about}</textarea>
                                            <span id="about-lenght">0/300</span>
                                            <script>
                                                currentLenght('about-text', 'about-lenght');
                                            </script>
                                            <div style="margin-top: 10px;width: 588px;color: red;border: 1px solid red;padding: 3px;"><center>This Intro will be displayed on the Find Training Partner page before users view your full profile. <br>There is a 300-character limit, so write carefully!<br></center></div>
                                        </dd>
                                    </dl>
                                    
                                    <a href="javascript:;" class="button" style="float: right" onclick="saveAbout()">Save</a>
                                    <a href="javascript:;" class="button" style="float: right" onclick="jQuery('#about-edit').hide(0,function(){ldelim}jQuery('#about').show(0,function(){ldelim} jQuery('#about-text').val(jQuery('#about-div').text());currentLenght('about-text', 'about-lenght'){rdelim}){rdelim})">Cancel</a>

                                </div>
                                <p class="clear"></p>    
                                <div style="margin: 20px 0; border-bottom: 1px solid #ccc">&nbsp;&nbsp;</div>  
                                
                                    {if $Numcoachgame >0}
                                        {section name=gameRow loop=$TpgameArr}
                                        <div class="profile-list" {if $smarty.section.gameRow.index%2 neq 0}style="background-color:#f2f2f2;"{/if}>
                                            <div  class="game-details" style="width:90%;{if $smarty.section.gameRow.index%2 neq 0}background-color:#f2f2f2;{/if}" >
                                                <h2>{$TpgameArr[gameRow].game}</h2>
                                                    <div class="coach_game_details" style="margin-top: 10px;margin-left: 10px;">
                                                        {section name=categoryIndex loop=$TpgameArr[gameRow].categoriesArr}
                                                        <dl>
                                                            <dt>{$TpgameArr[gameRow].categoriesArr[categoryIndex].category_name}:</dt>
                                                            <dd>{if $TpgameArr[gameRow].categoriesArr[categoryIndex].properties neq ''}{$TpgameArr[gameRow].categoriesArr[categoryIndex].properties}{else}&nbsp;{/if}</dd>
                                                        </dl>
                                                        {/section}
                                                        
                                                        <dl>
                                                            <dt>Experience</dt>
                                                            <dd>{if $TpgameArr[gameRow].experience neq ''}{$TpgameArr[gameRow].experience}{else}N/A{/if}</dd>
                                                        </dl> 
                            
                                                        <dl>
                                                            <dt>Improve In</dt>
                                                            <dd>{if $TpgameArr[gameRow].need_improvement neq ''}{$TpgameArr[gameRow].need_improvement}{else}N/A{/if}</dd>
                                                        </dl>
                                                    </div>            
                                                 
                                                    <div style="margin-top: 25px; margin-bottom: 10px; float: right;">
                                                        <a class="button" href="#" onclick="ConfirmDelete('{$page_name}?action=del','{$TpgameArr[gameRow].user_game_id}','{$TpgameArr[gameRow].game}','Your selected game:: ')">Delete</a>
                                                        <a class="button" href="training_partner_game_update.php?user_game_id={$TpgameArr[gameRow].user_game_id}">Edit</a>
                                                    </div>
                                                    <div class="clear"></div>
                                            </div>
                                    <div class="clear"></div>
                                </div>
                                {/section}
                            {else}
                            <div class="profile-list">
                                    <div class="game-details"> No game found </div>
                            </div>
                            {/if}
                            <div class="clear"></div>
                    </div>
            </div>
	</div>
        <script language="javascript">
            changePageTitle('Training partner');
        </script>
</div>     