{literal}
<script type="text/javascript">
function redirect_function(id)
{
  document.forms["studentform"+id].submit();
}
function expand(id)
{
  jQuery('#small_'+id).css('display','none');
  jQuery('#big_'+id).css('display','block');
  
}
function collapse(id)
{
  jQuery('#big_'+id).css('display','none');
  jQuery('#small_'+id).css('display','block');
  
}
</script>
{/literal}
<div id="paging"> 

 <div class="right-panel">
      <h1 title="Find Student"><span>Find Student</span></h1>
	  <div class="searchpanel">
         <form action="" method="post">
              <fieldset>
			  <label></label>
              <select for="sirt" name="sort_by" onchange="subForm(this.value);">
			      <option value="">Sort by</option>
                  <option value="rate" {if $sort_by eq 'rate'} selected="selected" {/if}>Budget (Low)</option>
				  <option value="rate-DESC" {if $sort_by eq 'rate-DESC'} selected="selected" {/if}>Budget (High)</option>
               </select>
               </fieldset>
            </form>
           <div class="clear"></div>
      </div>
      <div class="clear"></div>
 

   {if $SearchTxt neq ''}<div><b>Search By</b>  {$SearchTxt}</div>{/if}
	 {if $NumStudent >0}
	  {section name=studentRow loop=$StudentArr}
      <div class="game-list-block">
		  
		  <form name="student{$StudentArr[studentRow].user_id}" id="studentform{$StudentArr[studentRow].user_id}" method="post" 
			action="studentdetails.php?student_id={$StudentArr[studentRow].user_id}">
			<input type="hidden" name="price" value="{$price}" />
			<input type="hidden" name="languages" value="{$languages}" />
			<input type="hidden" name="availabilitys" value="{$availabilitys}" />
			<input type="hidden" name="games" value="{$games}" />
			<div class="game-image-block">
			<a href="#" onclick="redirect_function({$StudentArr[studentRow].user_id})">
			{if $StudentArr[studentRow].photo neq ''}
		     <img src="uploaded/user_images/thumbs/big_{$StudentArr[studentRow].photo}" alt="{$StudentArr[studentRow].username}" border="0"  />
			{else} 
			 <img src="images/student_thumb.jpg" alt="Student" border="0" />
			{/if}
			</a>
		  </div>
		  <div class="game-details-block" style="height:110px;" id="small_{$StudentArr[studentRow].user_id}">
		  <h2><a href="#" onclick="redirect_function({$StudentArr[studentRow].user_id})" style="text-decoration:none;">{$StudentArr[studentRow].username}</a></h2>
          <!--p>{$StudentArr[studentRow].about}</p>
		   <p class="clear"></p-->
            <ul class="games-desc">
               <li><span>Language :</span> {if $StudentArr[studentRow].language neq ''}{$StudentArr[studentRow].language}{else}N/A{/if}</li>
			   <li><span>Availability :</span> {$StudentArr[studentRow].availability_type}</li>
			  
            </ul>
            <ul class="games-desc">
			   <li><span>Budget :</span> {if $StudentArr[studentRow].rate neq ''}${$StudentArr[studentRow].rate} {else} N/A {/if}</li>
			   <li>&nbsp;</li>
			  
		    </ul>
			
		    <div class="clear"></div>
			
            <div class="view"><!--a href="studentdetails.php?student_id={$StudentArr[studentRow].user_id}">view</a-->
			<a href="#" onclick="redirect_function({$StudentArr[studentRow].user_id})">view</a>
			</div>
			<div class="clear"></div>
			<div class="preview">
			<a href="javascript:;" style="float:right;" onclick="expand({$StudentArr[studentRow].user_id})">preview</a>
			</div>
		  </div>
		  
		  <div class="game-details" style="display:none" id="big_{$StudentArr[studentRow].user_id}">
            <h2><a href="#" onclick="redirect_function({$StudentArr[studentRow].user_id})" style="text-decoration:none;">{$StudentArr[studentRow].username}</a></h2>
            <!--p>{$StudentArr[studentRow].about}</p>
            <p class="clear"></p-->
            <ul class="games-desc">
               <li><span>Language :</span> {if $StudentArr[studentRow].language neq ''}{$StudentArr[studentRow].language}{else}N/A{/if}</li>
			   <li><span>Availability :</span> {$StudentArr[studentRow].availability_type}</li>
			   {if $StudentArr[studentRow].avail_local eq 'Y'}
			   <li><span>City (Local meet-up) :</span> {if $StudentArr[studentRow].availability_city neq ''}{$StudentArr[studentRow].availability_city}{else}N/A{/if}</li>
			   {/if}
            </ul>
            <ul class="games-desc">
			   <li><span>Budget :</span> {if $StudentArr[studentRow].rate neq ''}${$StudentArr[studentRow].rate} {else} N/A {/if}</li>
			   <li>&nbsp;</li>
			   {if $StudentArr[studentRow].avail_local eq 'Y'}
			   <li><span>Country (Local meet-up) :</span> {if $StudentArr[studentRow].availability_country neq ''}{$StudentArr[studentRow].availability_country}{else}N/A{/if}</li>
               {/if}
		    </ul>
			<p class="clear" style="padding-bottom:7px;"></p>
			{section name=game loop=$StudentgameArr[studentRow]}
				<h4 class="gameshead" style="padding-bottom:0px; padding-left:2px;"> {$StudentgameArr[studentRow][game].game} </h4>
				<p style="padding-top:2px; padding-left:2px;"><b>Experience : </b>{if $StudentgameArr[studentRow][game].experience neq ''}{$StudentgameArr[studentRow][game].experience} 
				{else} N/A {/if}</p>
				<p class="clear" style="padding-bottom:5px;"></p>
			{/section}
            <div class="clear"></div>
            <div class="view"><!--a href="studentdetails.php?student_id={$StudentArr[studentRow].user_id}">view</a-->
			<a href="#" onclick="redirect_function({$StudentArr[studentRow].user_id})">view</a>
			</div>
			<div class="clear"></div>
			<div class="preview">
			<a href="javascript:;" style="float:right;" onclick="collapse({$StudentArr[studentRow].user_id})">fold</a>
			</div>
			</div>
			<div class="clear"></div>
			</form>
      
         <div class="clear"></div>
      </div>
	 {/section} 
     {if $pagination_arr[1]}
	 <div class="clear"></div>
      <div class="pagin">
         <ul>
           {$pagination_arr[1]}
         </ul>
      </div>
	 {/if}
	 {else}
	    No Student is found......
     {/if}
	</div> 
   </div>
