<link type="text/css" rel="stylesheet" href="style/lightbox-form.css">
<script src="js/lightbox-form.js" type="text/javascript"></script>
<script language="javascript" src="js/contact_us.js"></script>
<link type="text/css" href="jss/tipTip.css"  rel="stylesheet"  />
<script src="jss/jquery.tipTip.js" type="text/javascript"></script>

{if $smarty.session.user_id neq '' && $smarty.session.user_id neq $CoachArr.user_id}
<link type="text/css" href="style/jquery.timecalendar.css" rel="stylesheet" />
<script type="text/javascript" src="jss/jquery.timecalendar.js"></script>
{/if}
{literal}
<script language="javascript">
jQuery(function(){
    jQuery(".someClass").tipTip();
});

function CheckFields(formID,NumRatingCat){
   var r_comment = document.getElementById('review_comment').value;
   var NumRatingCat = NumRatingCat;	 	 	 	 
   var coach_id = document.getElementById('coach_id').value;
   var reviewed_by = document.getElementById('logged_user').value;
   
   if(document.getElementById('review_comment').value==''){
        jQuery('#review_comment').css('border','1px solid #FF0000');
        return false;
   }
   else 
        jQuery('#review_comment').css('border','1px solid #ccc');
	var frmID='#'+formID;
	var params ={
            'action': 'sendreview',
            'NumCoachRatingCat': NumRatingCat,
            'IsProcess': 'Y',
            'reviewed_by': reviewed_by
	};
	var paramsObj = jQuery(frmID).serializeArray();
	jQuery.each(paramsObj, function(i, field){
	params[field.name] = field.value;
	});
	
   if(r_comment!=''){
	  jQuery.ajax({
		type: "POST",
		url: 'coachdetails.php?coach_id='+coach_id,
		data: params,
		dataType : 'text',
		success: function(data){
                    jQuery('.signin-message').fadeIn(0);
                    jQuery('#msgbox').fadeIn(0);
                    jQuery('#msgbox').html('Review posted successfully.');
                    jQuery('.signin-message').fadeOut(5000);
                    jQuery('#paging').html(data);
                }
      });
	  document.getElementById("review_comment").value='';
	  document.getElementById('serialStar').value='';
          jQuery("#reviewwindow").fadeOut("fast", function(){jQuery('#overlay').fadeOut();});
    }
}	

function closereviewbox(){
    document.getElementById("review_comment").value='';
    document.getElementById("serialStar").value='';
    jQuery('#review_comment').css('border','1px solid #ccc');
    jQuery("#reviewwindow").fadeOut("fast", function(){jQuery('#overlay').fadeOut();});
}
    
</script>  
{/literal}
{if $IsProcess neq 'Y'}
<div class="right-panel">
    <div class="findcoach-title" style="margin-bottom: 5px;">
        <div class="title" style="float: left;padding: 0;border: none;">
            <a style="text-decoration: none;" href="findcoach.php">Find Coach</a>
        </div>
        
        <div class="breadcrumb">{$CoachArr.username}</div>
        <div class="clear"></div>
    </div>

    <div class="clear"></div>
    <div class="game-list-block">
        <div class="game-image-block">
            {if $CoachArr.photo neq ''}
            <img src="uploaded/user_images/thumbs/big_{$CoachArr.photo}" alt="Coach" border="0" />
            {else}
             <img src="images/coach_thumb.jpg" border="0" /> 
            {/if} 
        </div> 
		    
        <div class="game-details-block">
            <div class="score_title listcoachname" data-id="{$CoachArr.user_id}">
                <h2>
                    <a class="{$online_status}" title="{$online_status}"></a><a href="javascript:;" class="" style="text-decoration:none;">{$CoachArr.username}</a> 
                    <a id="{$CoachArr.user_id}_friendStar" class="{if $isfriend eq 'y'}star_friend{else}star_not_friend{/if}" title="{if $isfriend eq 'y'}in your friend list{else}not your friend list{/if}"></a>
                </h2>
                {if $smarty.session.user_id neq '' && $smarty.session.user_id neq $CoachArr.user_id}
                <div class="addAsFriendWindow" id="addAsFriendWindow_{$CoachArr.user_id}">
                    <a class="addAsFriendLink" href="javascript:;" onclick="openChatTab(event,'{$CoachArr.user_id}','{$CoachArr.username}')">Message</a><br/>
                    {if $isfriend eq 'n'}
                    <a class="addAsFriendLink addFriend" href="javascript:;" onclick="addAsFriend(event,'{$CoachArr.user_id}','{$CoachArr.username}',true)">Add as friend</a>
                    {else}
                    <a class="addAsFriendLink removeFriend" href="javascript:;" onclick="removeFriend(event,'{$CoachArr.user_id}','{$CoachArr.username}',true)">Remove friend</a>
                    {/if}
                </div>
                {/if}
            </div>
            <div class="score_card">
                <div id="scoreCard">
                    <ul>
                        {if $CoachOverallArr.overall_rating neq 0}
                        <li id="Overall" class="tTip">
                            <span class="someClass" title="{$CoachOverallArr.rating_overall_tooltip}">
                            {$CoachOverallArr.overall_rating}<br/>Overall
                            </span>
                        </li>
                            {/if}
                            {section name=k loop=$CatIndividualRec}
                            {if $CatIndividualRec[k].rating_category eq 'Knowledge'}
                        <li id="Knowledge" class="tTip">
                        <span class="someClass" title="{$CatIndividualRec[k].rating_category_tooltip}">
                        {$CatIndividualRec[k].rating}<br/>{$CatIndividualRec[k].rating_category}
                        </span>
                        </li>
                            {/if}
                            {if $CatIndividualRec[k].rating_category eq 'Structure'}
                       <li id="Structure" class="tTip">
                            <span class="someClass" title="{$CatIndividualRec[k].rating_category_tooltip}">
                            {$CatIndividualRec[k].rating}<br/>Structure
                            </span>
                       </li>
                       {/if}
                       {if $CatIndividualRec[k].rating_category eq 'Communication'}
                            <li id="Communication" class="tTip" style="width: 85px;">
                            <span class="someClass" title="{$CatIndividualRec[k].rating_category_tooltip}">
                            {$CatIndividualRec[k].rating}<br/>Communication
                            </span>
                            </li>
                            {/if}
                            {if $CatIndividualRec[k].rating_category eq 'Professionalism'}
                             <li id="Professionalism" class="tTip" style="width: 90px;">
                             <span class="someClass" title="{$CatIndividualRec[k].rating_category_tooltip}">
                             {$CatIndividualRec[k].rating}<br/>Professionalism
                             </span>
                             </li>
                             {/if}
                            {/section}
                    </ul>
                </div>
			</div>
			<br />

			<p class="clear"></p>
			<div class="games-desc user_details" style="margin-top: 10px;">
				<dl>
					<dt>Language:</dt>
					<dd>{if $CoachArr.language_city neq ''}{$CoachArr.language}{else}&nbsp;{/if}</dd>
				</dl>
				{if $CoachArr.avail_local eq 'Y'}
				<dl>
					<dt>City (Local meet-up):</dt>
					<dd>{if $CoachArr.availability_city neq ''}{$CoachArr.availability_city}{else}N/A{/if}</dd>
				</dl>
				{/if}
				<dl>
					<dt>Availability:</dt>
					<dd>{if $CoachArr.availability_type neq ''}{$CoachArr.availability_type}{else}N/A{/if}</dd>
				</dl> 
				<dl>
					<dt>Introduction:</dt>
					<dd><div>{if $CoachOverallArr.about neq ''}{$CoachOverallArr.about}{else}N/A{/if}</div></dd>
				</dl>
				
			</div>
			
            {section name=game loop=$CoachgameArr}
            <div class="games-desc user_details" style="margin-top: 25px;">
                <h2>{$CoachgameArr[game].game_name}</h2>
                <div style="margin-left: 15px;">
                    {section name=categoryIndex loop=$CoachgameArr[game].categoriesArr}
                    <dl>
                        <dt>{$CoachgameArr[game].categoriesArr[categoryIndex].category_name}:</dt>
                        <dd>{if $CoachgameArr[game].categoriesArr[categoryIndex].properties neq ''}{$CoachgameArr[game].categoriesArr[categoryIndex].properties}{else}&nbsp;{/if}</dd>
                    </dl>
                    {/section}

                    <dl>
                        <dt>Experience:</dt>
                        <dd><div>{if $CoachgameArr[game].experience neq ''}{$CoachgameArr[game].experience}{else}N/A{/if}</div></dd>
                    </dl> 
                    <dl>
                        <dt>Lesson Plan:</dt>
                        <dd><div>{if $CoachgameArr[game].lesson_plan neq ''}{$CoachgameArr[game].lesson_plan}{else}N/A{/if}</div></dd>
                    </dl> 

                    <dl>
                        <dt>Rates:</dt>
                        <dd>
                            {if $CoachgameArr[game].rate_A neq '' or $CoachgameArr[game].rate_B neq '' or $CoachgameArr[game].rate_C neq ''}
                            <div>
                            {if $CoachgameArr[game].rate_A neq ''}<div>{$CoachgameArr[game].rate_A}pts / 1hr</div> {/if}
                            {if $CoachgameArr[game].rate_B neq '' && $CoachgameArr[game].rate_B neq '0'}<div>{$CoachgameArr[game].rate_B}pts / 2hr</div> {/if}
                            {if $CoachgameArr[game].rate_C neq '' && $CoachgameArr[game].rate_C neq '0'}<div>{$CoachgameArr[game].rate_C}pts / 3hr</div> {/if}
                            </div>
                            {else}
                            N/A
                            {/if}
                        </dd>
                    </dl>
                </div>

            </div>
            {/section}
			{if $smarty.session.user_id neq '' && $smarty.session.user_id neq $CoachArr.user_id}
                        <div style="margin-top: 20px">
                            {if $Numtoreview eq '0'}
                            <a href="javascript:void(0);" class="button" onclick="return false;" style="position: relative;float: right" ><div class="disabledLink">&nbsp;</div><span>Review</span></a>
                            {else}  
                            <a href="javascript:void(0);" onClick="openReview('Review and Rating for <b>{$CoachArr.name}</b>')"  class="button" style="float: right">Review</a>
                            {/if}
                            <a href="javascript:void(0);" class="button" style="float: right" onClick="openChatTab(event,'{$CoachArr.user_id}','{$CoachArr.name}','{$isfriend}')">Message</a>
                            <a href="javascript:void(0);" class="button" style="float: right" onClick="showCalendar('{$CoachArr.user_id}');">Schedule a lesson</a>
                            
                            {if $online_status eq 'online'}
                            <a href="javascript:void(0);" class="button" style="float: right" onClick="outsideInstant('{$CoachArr.user_id}');">Instant Lesson</a>
			    {else}
                            <a href="javascript:void(0);" class="calendarbutton button" onclick="return false;" style="position: relative;" title="The Instant Lesson feature is available when the coach is online"><div class="disabledLink">&nbsp;</div><span>Instant lesson</span></a>
                            {/if}
                            <div class="clear"></div>
                        </div>
                        <div id="outfinalstep" style="z-index: 999999999999999999999999999;position: fixed;" class="finalstep"></div>
		   {/if}
			
		</div>
        <div class="clear"></div>
	</div>
	  
	{if $Numvideo > 9999}
	<div class="game-details-block game-list" style="border: 2px solid #ccc; width: 730px; margin-left: 0;">
		<h2 title="Video">Videos</h2>	
	<div class="clear"></div>
		{section name=video loop=$CoachvideoArr}
		{assign var='i' value=$smarty.section.video.index+1}
		<div {if $i%3 eq '0'} class="games-replay-last" {else} class="games-replay" {/if} >
            <div class="replay-content"> 
                <a href="videodetails.php?video_id={$CoachvideoArr[video].video_id}">
                <img src="uploaded/video_images/thumbs/mid_{$CoachvideoArr[video].video_image}" alt="" title="{$CoachvideoArr[video].video_title}" border="0"  /></a>
            </div>
            <div class="replay-content">
                <div class="replay-title"><a href="videodetails.php?video_id={$CoachvideoArr[video].video_id}">{$CoachvideoArr[video].video_title}</a><br />
                    <span class="date">{$CoachvideoArr[video].video_date}<br />
                    {if $CoachvideoArr[video].time_length neq ''}{$CoachvideoArr[video].time_length} min{else} N/A {/if}
                    </span>
                </div>
                <div class="game-rating" >
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td><b>Likes:</b></td>
                            <td>{$CoachvideoArr[video].numlike}</td>
                        </tr>
                        <tr>
                            <td><b>Disikes:</b></td>
                            <td>{$CoachvideoArr[video].numunlike}</td>
                        </tr>
                        <tr>
                            <td><b>Views:</b></td>
                            <td>{$CoachvideoArr[video].view_count}</td>
                        </tr>
                    </table>

                </div>

                {if $i%3 eq '0'}
                <div class="clear"></div>
                {/if}		
                
                <div class="clear"></div>  
            </div>
		</div>
		{/section}
	</div> 
    {/if}{/if}
	<div class="clear"></div>
	<div class="signin-message" style="display:none;">
		<div id="msgbox" style="color:#0077BC;"></div>
	</div>
        
	<div id="paging">
		<div id="review">
                    <div class="review-list">
                        <h2 title="Review">Recent Lessons</h2>
                        {if $NumLessons >0}
                            <div class="clear"></div>

                            <div class="game-list-block" style="padding-bottom: 10px; ">
                                {section name=rrow loop=$recentLessonsArr}
						<div style="margin-top: 20px">
							<div class="review-left" style="width: 50px; height: 50px" >
								<a href="javascript:;" onclick="{$recentLessonsArr[rrow].user_id}">
									{if $recentLessonsArr[rrow].photo neq ''}
									<img src="uploaded/user_images/thumbs/big_{$recentLessonsArr[rrow].photo}" height="50" width="50"  alt="Student" border="0" />
									{else}
									<img src="images/coach_thumb.jpg" height="50" width="50" border="0" /> 
								   {/if} 
								</a>
							</div>
							<div class="review-right" style="padding: 0; padding-left: 10px">
								<div class="listcoachname">
									<div style="font-size: 16px;">
										<a class="{$recentLessonsArr[rrow].status}" title="{$recentLessonsArr[rrow].status}"></a><a class="" href="javascript:;" onclick="buildPageLink({$recentLessonsArr[rrow].user_id})" style="text-decoration:none;">{$recentLessonsArr[rrow].username}</a> 
										<a id="{$recentLessonsArr[rrow].user_id}_friendStar" class="{if $recentLessonsArr[rrow].isfriend eq 'y'}star_friend{else}star_not_friend{/if}" title="{if $recentLessonsArr[rrow].isfriend eq 'y'}in your friend list{else}not your friend list{/if}"></a>
									</div>
									{if $smarty.session.user_id neq '' && $smarty.session.user_id neq $recentLessonsArr[rrow].user_id}
									<div class="addAsFriendWindow" id="addAsFriendWindow_{$recentLessonsArr[rrow].user_id}">
											<a class="addAsFriendLink" href="javascript:;" onclick="openChatTab(event,'{$recentLessonsArr[rrow].user_id}','{$recentLessonsArr[rrow].username}')">Message</a><br/> 
											{if $recentLessonsArr[rrow].isfriend eq 'n'}
											<a class="addAsFriendLink addFriend" href="javascript:;" onclick="addAsFriend(event,'{$recentLessonsArr[rrow].user_id}','{$recentLessonsArr[rrow].username}',true)">Add as friend</a>
											{else}
											<a class="addAsFriendLink removeFriend" href="javascript:;" onclick="removeFriend(event,'{$recentLessonsArr[rrow].user_id}','{$recentLessonsArr[rrow].username}',true)">Remove friend</a>
											{/if}
									</div>
									{/if}
								</div>
								
								<table cellpadding="0" cellspacing="0" style="width: 40%;margin-left: 15px; margin-top: 5px">
								   
									<tr>
										<td><b>Game:</b></td>
										<td>{$recentLessonsArr[rrow].game_name}</td>
									</tr>
									<tr>
										<td><b>Date:</b></td>
										<td>{$recentLessonsArr[rrow].start}</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="clear"></div>
                                {/section}
						
                            </div>


                        {else}
                        No Lessons
                        {/if} 
                    </div>
		</div>
	</div>    
	<div class="clear"></div>
        
	<div id="paging"> 
		<div id="review">
			<div class="review-list">
				<h2 title="Review">Reviews</h2>
				{if $NumReview >0}
				<div class="clear"></div>
				{section name=rrow loop=$UserReview}

				<div class="review-block">

					<div class="review-left">
						<a href="{$UserReview[rrow].userlink}">
							<img src="{$UserReview[rrow].img}" height="92" width="105"  alt="{$UserReview[rrow].review_by}" border="0" />
						</a>
					</div>
					<div class="review-right">
						<div class="review-sub-title">

							<div class="review_thumb_block">
								<h4 class="reviewshead">Reviewed By : <a href="javascript:;" onclick="buildPageLink({$UserReview[rrow].reviewer_id})" >{$UserReview[rrow].review_by}</a></h4>
								<br/><br />

								<span><em>Posted on</em></span> : {$UserReview[rrow].r_date}<br /><br />
								{if $UserReview[rrow].review_comment neq ''}
								<p><i>Review</i>:&nbsp;{$UserReview[rrow].review_comment|nl2br}</p><br />
								{/if}
							</div>

							<div class="review_card_block">
								<!-- Score Area -->
								<div id="scoreCard">
									<ul>
										{if $UserReview[rrow].overall_r neq 0}
										<li id="Overall" class="tTip">
											<span class="someClass" title="{$UserReview[rrow].rating_overall_tooltip}">
											{$UserReview[rrow].overall_r}<br/>Overall
											</span>
										</li>
										{/if}
										{section name=k loop=$UserReview[rrow].individual}
										{if $UserReview[rrow].individual[k].rating_category eq 'Knowledge'}
										<li id="Knowledge" class="tTip">
											<span class="someClass" title="{$UserReview[rrow].individual[k].rating_category_tooltip}">
											{$UserReview[rrow].individual[k].rating}<br/>{$UserReview[rrow].individual[k].rating_category}
											</span>
										</li>
										{/if}
										{if $UserReview[rrow].individual[k].rating_category eq 'Structure'}
										<li id="Structure" class="tTip">
											<span class="someClass" title="{$UserReview[rrow].individual[k].rating_category_tooltip}">
											{$UserReview[rrow].individual[k].rating}<br/>Structure
											</span>
									   </li>
									   {/if}
									   {if $UserReview[rrow].individual[k].rating_category eq 'Communication'}
										<li id="Communication" class="tTip" style="width: 85px;">
											<span class="someClass" title="{$UserReview[rrow].individual[k].rating_category_tooltip}">
											{$UserReview[rrow].individual[k].rating}<br/>Communication
											</span>
										</li>
										{/if}
										{if $UserReview[rrow].individual[k].rating_category eq 'Professionalism'}
										<li id="Professionalism" class="tTip" style="width: 90px;">
											 <span class="someClass" title="{$UserReview[rrow].individual[k].rating_category_tooltip}">
											 {$UserReview[rrow].individual[k].rating}<br/>Professionalism
											 </span>
										</li>
										{/if}
									   {/section}
									</ul>
								</div>
							</div>

							<div class="clear"></div>
							<!-- Score Area END -->

						</div>
					<!--p class="post"><span>Posted on</span> : {$UserReview[rrow].r_date}</p-->
					</div>
					<div class="clear"></div>
				</div>
				{/section}
				

				{if $pagination_arr[1]}
				<div class="clear"></div>
				<div class="pagin">
					<ul>
					   {$pagination_arr[1]}
					</ul>
				</div>
				{/if}
				{else}
				No reviews
				{/if} 
			</div>
		</div>	
	</div>	 
		
		
	
	

</div> 
<div class="modalpopup" id="reviewwindow">
<a class="modalpopup-close-btn" href="javascript:;"></a>
<div id="boxtitle" style="font-weight: normal;" class="title"></div>
<div class="review">
<div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:5px;">{if $ermsg neq ''} {$ermsg}{/if} </div>
<form name="contact" id="Rating_Coach"  method='post' action="" onsubmit="return false;">
    <input type="hidden" name="game_id" value="{$game_id}" />
    <input type="hidden" name="coach_id" id="coach_id" value="{$coach_id}" />
    <input type="hidden" name="logged_user" id="logged_user" value="{$logged_user}" />
    <input type="hidden" name="user_type_id" id="user_type_id" value="1" />
    <table cellpadding="0" cellspacing="10">
        <tr>
            <td class="">
                <span class="someClass" title="{$CoachOverallArr.rating_overall_tooltip}">Overall :</span> 
            </td>
            <td>
                <div class="">
                    <table cellpadding="5" cellspacing="5">
                        <tr valign="middle">
                            <td>1&nbsp;<input type="radio" value="1" name="Rating_overall" id="serialStar" title="Poor"></td>
                            <td>2&nbsp;<input type="radio" value="2" name="Rating_overall" id="serialStar" title="Ok"></td>
                            <td>3&nbsp;<input type="radio" value="3" name="Rating_overall" id="serialStar" title="Good"></td>
                            <td>4&nbsp;<input type="radio" value="4" name="Rating_overall" id="serialStar" title="Better"></td>
                            <td>5&nbsp;<input type="radio" value="5" name="Rating_overall" id="serialStar" title="Awesome"></td>
                        </tr>
                    </table>

                </div>  
            </td>
        </tr>

        {section name=i loop=$RatingCatArr}
        <tr>
            <td class="">
                <span class="someClass" title="{$RatingCatArr[i].rating_category_tooltip}">{$RatingCatArr[i].rating_category}:</span>
            </td>
            <td colspan="2"> 
                <div class="">
                    <table cellpadding="5" cellspacing="5">
                        <tr>
                            <td>1&nbsp;<input type="radio" value="1:{$RatingCatArr[i].rcat_id}" name="Rating_{$smarty.section.i.index}" id="serialStar" title="Poor"></td>
                            <td>2&nbsp;<input type="radio" value="2:{$RatingCatArr[i].rcat_id}" name="Rating_{$smarty.section.i.index}" id="serialStar" title="Ok"></td>
                            <td>3&nbsp;<input type="radio" value="3:{$RatingCatArr[i].rcat_id}" name="Rating_{$smarty.section.i.index}" id="serialStar" title="Good"></td>
                            <td>4&nbsp;<input type="radio" value="4:{$RatingCatArr[i].rcat_id}" name="Rating_{$smarty.section.i.index}" id="serialStar" title="Better"></td>
                            <td>5&nbsp;<input type="radio" value="5:{$RatingCatArr[i].rcat_id}" name="Rating_{$smarty.section.i.index}" id="serialStar" title="Awesome"></td>
                        </tr>
                    </table>

                </div>
            </td>
        </tr>
        {/section}
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td width="20%" valign="top">Review :</td>
            <td width="54%"><textarea style="width: 300px;" name="review_comment" id="review_comment" cols="15" rows="5" >{$Formval.user_about}</textarea></td>
        </tr>

        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" align="center">    
                <input name="submit" class="button" value="Submit" type="submit" onclick="return CheckFields('Rating_Coach',{$NumRatingCat})" />
                <input name="" class="button" value="Cancel" type="button" onClick="closereviewbox()" />		
            </td>
        </tr>
    </table>
</form>
</div> 
</div>
</div>
</div> 
<script language="javascript">
    changePageTitle('Coach - {$CoachArr.username}');
</script>