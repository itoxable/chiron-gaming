<link href='https://fonts.googleapis.com/css?family=Dosis:400,700|Prosto+One|Advent+Pro:600,400|Homenaje|Quantico:400,700' rel='stylesheet' type='text/css'>
<div>
    <h1 class="howWorksTitle">What is Chiron Gaming</h1>
    <div class="howWorksText">
        <div style="margin: 15px;">
            <div id="Vivamus">
                <p><span style=" ">Chiron Gaming is a social platform for video game coaching and training. We help you connect with gaming coaches, training partners, and other players who share the same passion in video games and desire to train together.</span></p>
                <p>&nbsp;</p>
                <p><span style=" ">You are the coach, and you are also the student. Chiron Gaming allows anyone to freely sign up and teach at any rates equivalent of their gaming expertise.</span><span style=" ">&nbsp;Within this community we hope to encourage players to share, train, and grow together to reach whatever goals they've set before them. Whether it's getting out of ELO hell, or making that push to get out of Bronze league, Chiron Gaming can help you get there.</span></p>
            </div>
        </div>
    </div>
    <h1 class="howWorksTitle">How it works</h1>
    <div class="howWorksText">
        <div style="margin: 15px;">
            <h2 style="font-family: 'Quantico', sans-serif; color: rgb(75, 75, 75);margin: 50px 0 10px 10px;border-bottom: 2px solid rgb(200,200,200);">Overview</h2>
            <img src="../images/step1.png" height="235px" width="235px">
            <img src="../images/step2.png" height="235px" width="235px">
            <img src="../images/step3.png" height="235px" width="235px">
            <img src="../images/step4.png" height="235px" width="235px">
            <h2 class="howWorksTitle" style="font-family: 'Quantico', sans-serif; color: rgb(75, 75, 75);margin: 50px 0 11px 10px;border-bottom: 2px solid rgb(200, 200, 200);">How To Coach Players</h2>
            <div class="howtovideos" style="width: 640px; height: 360px;float: left;">
                <iframe width="640" height="360" src="http://www.youtube.com/embed/6lvg4okoW0o?rel=0" frameborder="0" allowfullscreen=""></iframe>
            </div>
            <div style="float: right;margin: 10px 0 10px 0;width: 310px;">
                <h3 style=" color: #013469;text-align: center;">&lt; Steps &gt;</h3>
                <div class="howitworksSTEPS" style=" margin-top: 10px;">
                    <p style="margin-bottom: 10px">1. Go to your Dashboard and click the "Become Coach" button on the top-right corner.</p>
                    <p style="margin-bottom: 10px">2. Enter game information. Don't forget to write your "Intro" which will be displayed to students on the Find Coach page!</p>
                    <p style="margin-bottom: 10px">3. Go to the "Coach"-"Availability" sub-tab and set your available time/dates for students to book lessons.</p>
                    <p style="margin-bottom: 10px">4. Spread the word that you are a GAMING COACH!</p>
                    <p style="margin-bottom: 10px">5. Frequently check the Schedule tab on your Dashboard (or email) for new lesson bookings.</p>
                    <p style="margin-bottom: 10px">6. At the time of your lessons, start the chat or voice chat with your students (Note: When students book lessons, they are automatically added to your Friend List). </p>
                    <p style="margin-bottom: 10px; color: #FF9900;">Tips: Be online as often as possible for Instant Lessons!</p>
                </div>
            </div>
            <div class="clear"></div>

            <h2 style="font-family: 'Quantico', sans-serif; color: rgb(75, 75, 75);margin: 50px 0 10px 10px;border-bottom: 2px solid rgb(200, 200, 200);">How To Find Coaches &amp; Book Lessons</h2>
            <div style="float: left; margin: 10px 0 10px 0; width: 310px;">
                <h3 style=" color: #013469;text-align: center;">&lt; Steps &gt;</h3>
                <div class="howitworksSTEPS" style="margin-top: 10px;">
                    <p style="margin-bottom: 10px">1. Go to Find Coach page.</p>
                    <p style="margin-bottom: 10px">2. Use the filters on the left to find a coach that fits your needs.</p>
                    <p style="margin-bottom: 10px">3. View the selected coach's profile and check the reviews by other students.</p>
                    <p style="margin-bottom: 10px">4. Click the "Schedule Lesson" button and select lesson dates/times.</p>
                    <p style="margin-bottom: 10px">5. Once confirmed, your coach will be automatically added to your Friend List. The coach will be online at your lesson times/dates and start the chat/voice chat with you.</p>
                    <p style="margin-bottom: 10px">6. After the lesson, don't forget to write reviews on your coach profile!</p><p style="margin-bottom: 10px; color: #FF9900;">Tips: If a coach of your choice is online, you can click "Instant Lesson" on the coach's profile to receive coaching right away!</p>

                </div>
            </div>     
            <div class="howtovideos" style="float: right;">
                <iframe width="640px" height="360" src="http://www.youtube.com/embed/UG_dBFw6dLc" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
            </div>
            <div class="clear"></div>

            <h2 class="howWorksTitle" style="font-family: 'Quantico', sans-serif; color: rgb(75, 75, 75);margin: 50px 0 11px 10px;border-bottom: 2px solid rgb(200,200,200);">How To Navigate Through Your Dashboard</h2>
            <div class="howtovideos" style="width: 640px; height: 360px;float: left;">
                <iframe width="640" height="360" src="http://www.youtube.com/embed/8GtDCh7GGqY?rel=0" frameborder="0" allowfullscreen=""></iframe>
            </div>
            <div style="float: right; margin: 10px 0 10px 0; width: 310px;">
                <h3 style=" color: #013469;text-align: center;">&lt; Dashboard Tabs &gt;</h3>
                <div class="howitworksSTEPS" style="margin-top: 10px;">
                    <p style="margin-bottom: 10px">1. Schedule: check your past, current and future trainings </p>
                    <p style="margin-bottom: 10px">2. Messages: all of your chat histories will be saved here</p>
                    <p style="margin-bottom: 10px">3. Profile: edit profile or change email notification settings</p>
                    <p style="margin-bottom: 10px">4. Coach (appear only after you activate it): set up your Coach profile  </p>
                    <p style="margin-bottom: 10px">5. Training Partner (appear only after you activate it): set up your Training Partner profile</p>
                    <p style="margin-bottom: 10px">6. Cashier: deposit and withdraw points (cash value) to book lessons with coaches </p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <h2 class="howWorksTitle">Students &amp; Training Partners</h2>
    <div class="howWorksText">
        <div style="margin: 10px;">
            <p>
                <span style="font-size: small; font-family: helvetica;">If you are a player looking for a coach or other training partners Chiron Gaming's rating system allows you to find only the best members of our community to train with. At Chiron Gaming we're committed to one thing above all else: helping you improve your game. Whether that is through our Masterclass sessions (coming soon), our streamlined system to find a coach, or just finding someone in your skill range to practice with, Chiron Gaming will help you reach whatever goals you have.</span>
            </p>
        </div>
    </div>
    <h2 class="howWorksTitle">Coaches</h2>
    <div class="howWorksText">
        <div style="margin: 10px;">
            <p>
                <span style="font-size: small; font-family: helvetica;">Chiron Gaming provides coaches with the thing they want most: Students. With a growing community, Chiron Gaming will offer unprecedented access to a variety of students from all skill levels. We also offer an easy scheduling system to help you plan and manage your lessons efficiently, allowing you to schedule sessions with your students based on your availability. With our payment system, you can expect secure, fast, and accurate transfers of points for all your lessons. Whether you're an established coach looking for more students or a player simply wishing to share gaming expertise, Chiron Gaming has all the tools to help you achieve your goal.</span>
            </p>
        </div>
    </div>
    </div>
<script language="javascript">
    changePageTitle('How it works');
</script><div class="clear"></div>
  