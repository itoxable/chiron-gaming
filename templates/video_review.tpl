<div id="paging">  
	<div class="content">
   		
		{* Start loading Tabs *}
		{include file='user_tabs.tpl' active_tab='reviews'}
		{* End loading Tabs *}
		
        <div class="clear"></div>
        <div class="tabular-content">
            <div class="tab_sub">
            	<ul>
                	{if $is_coach eq '1' or $is_partner eq '1'}<li><a href="my_review.php" class="active">My Reviews</a></li>{/if}
					<li><a href="my_video_review.php" class="active">My Video Reviews</a></li>
					<li><a href="coach_review.php" class="active">Reviews to User</a></li>
					<li><a href="javascript:;" >Reviews to video</a></li>
                </ul>
            </div>
			 <div class="clear"></div>
			 {if $Numvdoreview > 0}
				<div>
			      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row">
					   {section name=vdorow loop=$reviewvdoArr}
					   <tr {if $smarty.section.vdorow.index%2 neq 0}class="alter"{/if}>
                        <td width="50">
						<img src="uploaded/video_images/thumbs/{$reviewvdoArr[vdorow].video_image}" width="57" height="50" border="0" /></td>
						<td width="200" valign="top"><strong>
						<a href="videodetails.php?video_id={$reviewvdoArr[vdorow].video_id}">{$reviewvdoArr[vdorow].video_title}</a></strong><br />
						<i>by: {$reviewvdoArr[vdorow].name}</i>
						</td>
						<td width="100" valign="top" style="padding-right:10px;"><strong>
						{$reviewvdoArr[vdorow].review_date}</strong>
						</td>
						<td valign="top">{$reviewvdoArr[vdorow].review_comment|nl2br}</td>
						<td width="20" valign="top"> <a href="#" 
						 onclick="ConfirmDelete('{$page_name}?action=del','{$reviewvdoArr[vdorow].video_review_id}','{$reviewvdoArr[vdorow].video_title}','Your review for:: ')">
						 <img src="images/close_small.gif" alt="" border="0" /></a></td>
						</tr>
					   {/section}                          
                </table>
				</div>
        	<div class="clear"></div>
            <br />
            <div class="tab_sub">
            	&nbsp;
            </div>
            <div class="tab_search">
            	&nbsp;
            </div>
            <div class="tab_pagination">
            	{if $pagination_arr[1]}
				 <div class="clear"></div>
				  <div class="pagin">
					 <ul>
					   {$pagination_arr[1]}
					 </ul>
				  </div>
				 {/if}
            </div>   
            <div class="clear"></div> 
			{else}
				<div align="left" style="padding:10px;">No record found</div>
			{/if}
      </div>
   </div>
</div>