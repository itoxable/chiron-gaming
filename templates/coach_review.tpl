<div id="paging">  
	<div class="content">
   		
		{* Start loading Tabs *}
		{include file='user_tabs.tpl' active_tab='reviews'}
		{* End loading Tabs *}
		
        <!-- tab -->
        <div class="clear"></div>
        <div class="tabular-content">
            <div class="tab_sub">
            	<ul>
                	{if $is_coach eq '1' or $is_partner eq '1'}<li><a href="my_review.php" class="active">My Reviews</a></li>{/if}
					<li><a href="my_video_review.php" class="active">My Video Reviews</a></li>
					<li><a href="javascript:;" >Reviews to User</a></li>
					<li><a href="video_review.php" class="active">Reviews to video</a></li>
                </ul>
            </div>
			 <div class="clear"></div>
			 {if $NumotherReview>0}
				<div>
			      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row">
					 {section name=revrow loop=$reviewotherArr}
					   <tr {if $smarty.section.revrow.index%2 neq 0}class="alter"{/if}>
                        <td width="50" valign="top">
						 {if $reviewotherArr[vdorevrow].photo neq ''}
					     <img src="uploaded/user_images/thumbs/{$reviewotherArr[revrow].photo}"  border="0" />
						 {else}
					     <img src="images/avatar_smallest.jpg" border="0" />
					    {/if}</td>
						<td width="100" valign="top"><strong>
						<a href="{$reviewotherArr[revrow].link}">{$reviewotherArr[revrow].name}</a></strong><br />
						<i>{$reviewotherArr[revrow].user_type}</i>
						</td>
						<td width="100" valign="top" style="padding-right:10px;"><strong>
						{$reviewotherArr[revrow].review_date}</strong>
						</td>
						<td width="200" valign="top" style="padding-right:10px;">{$reviewotherArr[revrow].review_comment|nl2br}</td>
						
						
						<!--<td valign="top">{$reviewotherArr[revrow].review_comment|nl2br}</td>
						<td width="100" align="right" valign="top"><strong>{$reviewotherArr[revrow].rating}</strong></td>-->
						<td valign="top">
							<div class="review_card_block">
							 <!-- Score Area -->
							<div id="scoreCard">
								<ul>
									{if $reviewotherArr[revrow].rating neq 0}
									<li id="Overall" class="tTip">
									<span class="someClass" title="{$UserReview[rrow].rating_overall_tooltip}">
									<strong>{$reviewotherArr[revrow].rating}</strong><span>Overall</span>
									</span>
									</li>
									{/if}
									{section name=k loop=$reviewotherArr[revrow].individual}
									{if $reviewotherArr[revrow].individual[k].rating_category eq 'Knowledge'}
									<li id="Knowledge" class="tTip">
									<span class="someClass">
									<strong>{$reviewotherArr[revrow].individual[k].rating}</strong>
									<span>{$reviewotherArr[revrow].individual[k].rating_category}</span>
									</span>
									</li>
									{/if}
									{if $reviewotherArr[revrow].individual[k].rating_category eq 'Structure'}
								   <li id="Structure" class="tTip">
								   <span class="someClass" >
								   <strong>{$reviewotherArr[revrow].individual[k].rating}</strong><span>Structure</span>
								   </span>
								   </li>
								   {/if}
								   {if $reviewotherArr[revrow].individual[k].rating_category eq 'Communication'}
									<li id="Communication" class="tTip">
									<span class="someClass">
									<strong>{$reviewotherArr[revrow].individual[k].rating}</strong><span>Communication</span>
									</span>
									</li>
									{/if}
									{if $reviewotherArr[revrow].individual[k].rating_category eq 'Professionalism'}
									 <li id="Professionalism" class="tTip">
									 <span class="someClass" title="{$UserReview_Individual[rrow][k].rating_category_tooltip}">
									 <strong>{$reviewotherArr[revrow].individual[k].rating}</strong><span>Professionalism</span>
									 </span>
									 </li>
									 {/if}
									{/section}
								</ul>
							</div>
							</div>					
						</td>
						<td width="20" valign="top"><a href="#" 
						 onclick="ConfirmDelete('{$page_name}?action=del','{$reviewotherArr[revrow].user_review_id}','{$reviewotherArr[revrow].name}','Your review for:: ')">
						 <img src="images/close_small.gif" alt="" border="0" /></a></td>
						</tr>
                 {/section}                          
                </table>
				</div>
        	<div class="clear"></div>
            <br />
            <div class="tab_sub">
            	&nbsp;
            </div>
            <div class="tab_search">
            	&nbsp;
            </div>
            <div class="tab_pagination">
            	{if $pagination_arr[1]}
				 <div class="clear"></div>
				  <div class="pagin">
					 <ul>
					   {$pagination_arr[1]}
					 </ul>
				  </div>
				 {/if}
            </div>   
            <div class="clear"></div> 
			{else}
				<div align="left" style="padding:10px;">No record found</div>
			{/if}
      </div>
   </div>
</div>