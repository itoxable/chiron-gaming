{literal}
<link href="style/tab.css" rel="stylesheet" type="text/css" />
<link href="jsthickbox/thickbox.css" rel="stylesheet" type="text/css" />
<script src="jsthickbox/jquery-latest.js" type="text/javascript"></script>
<script src="jsthickbox/jquery-latest.pack.js" type="text/javascript"></script>
<script src="jsthickbox/thickbox.js" type="text/javascript"></script>
{/literal}
   <div class="right-panel">
	<div class="container">
		<ul class="tabs">
		<li class="active"><a href="#">Inbox</a></li>	
		<li><a href="send_item.php">Send Item</a></li>				
		</ul>
	<div class="clear"></div>
	<div class="tab_container">	
						
	  <div id="tab1" class="tab_content" style="margin-top:20px;">
		  <div class="dashboard-title"><h1 title="Registration"></h1></div>
		  <div class="clear"></div>
		  <div class="total-message">New Messages : {$latest_msgArrNum}</div>
		  <div class="clear"></div>
		  <div class="message-list">
		  {section name=msg loop=$latest_msgArr}		  
			  <div class="message-block">
				 <h3>{$latest_msgArr[msg].name}--{$latest_msgArr[msg].date_added}</h3>
				 <div class="message-desc">
				 <p>{$latest_msgArr[msg].message_text}</p>
	   </div>
				{if $smarty.session.user_id neq '' && $smarty.session.user_id neq $latest_msgArr[msg].user_id_from}
				 <div class="reply"><a href="reply.php?reply_of_msg={$latest_msgArr[msg].message_id}&user_id_to={$latest_msgArr[msg].user_id_from}&height=250&width=200&modal=true"  class="thickbox" title="Reply">reply</a></div>
				 {/if}
				 <div class="clear"></div>
			  </div>
			   {section name=msg1 loop=$latest_msg_subArr[msg]}		
			   <div class="message-block">
				 <h3>{$latest_msg_subArr[msg][msg1].name}--{$latest_msg_subArr[msg][msg1].date_added}</h3>
				 <div class="message-desc-right">
				 <p>{$latest_msg_subArr[msg][msg1].message_text}</p>
	   			</div>				
				 <div class="clear"></div>
			  </div>
			  {/section}		
		  {/section} 
			 <!-- <div class="viewall"><a href="#">View All</a></div>-->
			  <div class="clear"></div>          
		  </div>     
		  <div class="clear"></div>
   	  </div>
	  
	</div>
	</div>
	</div>