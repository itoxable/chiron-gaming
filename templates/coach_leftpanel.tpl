{literal}
<script type="text/javascript" language="javascript">

jQuery(document).ready(function(){
jQuery('#user_name').focus(function(){ if( jQuery(this).val()== 'Keyword' ) jQuery(this).val('');});
jQuery('#user_name').blur(function(){ if( jQuery(this).val()== '' ) jQuery(this).val('Keyword');});
jQuery.fn.autosugguest({  
           className: 'ausu-suggest',
          methodType: 'POST',
            minChars: 2,
              rtnIDs: true,
            dataFile: 'autopopulate.php?action=populateCity'
    });
/* right panel filter more/less links */
	var filterThreshold = 5;
	jQuery('.listing').each(function(i){
	if(jQuery(this).find('a.selected').length == 0){
		jQuery(this).find('li:gt('+(filterThreshold-1)+')').hide();
		if(jQuery(this).find('li').length > filterThreshold)
			jQuery(this).after('<li class="more"><div class="button" style="float:right;margin: 5px;">+ more</div></li><li style="clear:both; width:100%"></li>');
		else
			jQuery(this).after('');
	}
	});

	jQuery('.more').each(function(i){
		jQuery(this).toggle(function(){
		jQuery(this).prev().find('li:gt('+(filterThreshold-1)+')').slideDown('fast');
		jQuery(this).html('<li class="more"><div class="button" style="float:right;margin: 5px;"">- less</div></li><li style="clear:both; width:100%"></li>');
		},function(){
		jQuery(this).prev().find('li:gt('+(filterThreshold-1)+')').slideUp('fast');
		jQuery(this).html('<li class="more"><div class="button" style="float:right;margin: 5px;""><em><span>+ more</div></li><li style="clear:both; width:100%"></li>');
		});
	});
	jQuery('.refine').click(function(){
		var propid = jQuery(this).attr('propid');
                if(propid && propid != ""){
                    if(jQuery('#prop_'+propid).attr('checked'))
                        jQuery('#prop_'+propid).removeAttr('checked');
                    else
                        jQuery('#prop_'+propid).attr('checked','true');
                }else{
                    var val = jQuery(this).attr('typeId');
                    if(jQuery('#language_'+val).attr('checked'))
			jQuery('#language_'+val).removeAttr('checked');
                    else
			jQuery('#language_'+val).attr('checked','true');
                }

		frmRefine_POST();
	});
  });	
</script>
<link rel="stylesheet" href="slider/stylesheets/jslider.css" type="text/css">
<link rel="stylesheet" href="slider/stylesheets/jslider.plastic.css" type="text/css">
<script type="text/javascript" src="slider/javascripts/jquery.dependClass.js"></script>
<script type="text/javascript" src="slider/javascripts/jquery.slider-min.js"></script>
<script type='text/javascript'>
function frmRefine_POST(){
    var data = jQuery(document.frmRefine).serialize();
     
    jQuery('#loading').html('<p><img src="images/bar-loader.gif" alt="Loading..." /></p>');
    jQuery.ajax({
	type: "GET",
	url: 'findcoach2.php',
	data: data+'&game_id='+ jQuery('#game_id option:selected').val(),			
	dataType: 'html',
	success: function(data){				
            jQuery('.container-inner').html(data+"<div class='clear'></div>");
	   //alert(data);
            jQuery('#loading').html('');
	}
    });
}
function CreateCountry(){
    var cityid = jQuery('#cityid').val();
    var city = jQuery('#city').val();
    if(cityid !='' && city!=''){
        document.getElementById('availability_city').value=cityid;
        frmRefine_POST();
    }
    else{
        document.getElementById('availability_city').value='';
        frmRefine_POST();
    }
}
function subForm(a){
	document.getElementById('listfor').value='sorting';
	document.getElementById('sortingby').value=a;
	frmRefine_POST();
}
function avail_submit(c){
	document.getElementById('availability_country').value=c;
	frmRefine_POST();
}
function searchUser(){
    var username = jQuery('#user_name').val();
    if(username == '' || username =='Keyword')
        jQuery('#user_name').css('border','1px solid red');
    else{
        document.getElementById('unm').value=username;
	frmRefine_POST();
   }	 	
}
function resetSearch(){
    document.getElementById('unm').value='';
    frmRefine_POST();
}

function submitbyenter(event){
    var key;
    key = event.keyCode;
    if (key==13){		
        searchUser()		
    }
    return false;
}
</script>
{/literal}
<div class="left-panel">
<div class="left-search">
    <form action="findcoach.php" method="post" id="leftsearchfindcoach">
        <fieldset>
            <select name="game_id" id="game_id">
            {html_options options=$GameArr selected=$game_id}
            </select>
            <a class="button" href="#" style="float: right;min-width: 30px;padding: 0 3px; margin: 0;" onclick="document.getElementById('leftsearchfindcoach').submit();">Go</a>
       </fieldset>
     </form>
    <form action="findcoach.php" method="post" onsubmit="return false">
        <fieldset>
            <div style="background:#ccc; width:250px; float:left; display:block; top:40px; left:0px; height:37px; border:1px solid #ededed; padding:3px 0 0 5px; position:absolute;">
                <input name="unm" class="text" type="text" id="user_name" {if $searchusername neq ''}value="{$searchusername}"{else}value="Keyword"{/if} onfocus="this.value=''" onkeyup="submitbyenter(event);"/>		
                <input type="button" value="" onclick="searchUser()" />
            </div>
       </fieldset>
   </form>
</div>	
    <div class="graphite demo-container">
        <ul class="accordion" id="accordion-1">
            <form action="findcoach.php" method="post" name="frmRefine">
                <input type="hidden" name="list_for" id="listfor" value="" />
                <input type="hidden" name="sorting_by" id="sortingby" value=""/>
                <input type="hidden" name="unm" id="unm" value="{$searchusername}"/>
                <input type="hidden" name="availability_country" id="availability_country" value="{$availability_country}">
                <input type="hidden" name="availability_city" id="availability_city" value="{$availability_city}"/>
                <input type="hidden" name="action" value="send" />
                
                {section name=categoryIndex loop=$categoriesArr}
                <li>
                    <a href="#">{$categoriesArr[categoryIndex].category_name}</a>
                    {if $categoriesArr[categoryIndex].propsSize>0}
                    <ul>	
                        <div class="listing">
                            {section name=propIndex loop=$categoriesArr[categoryIndex].props}
                            <li>
                                <input type="checkbox" name="prop_id[]" id='prop_{$categoriesArr[categoryIndex].props[propIndex].property_id}' value="{$categoriesArr[categoryIndex].props[propIndex].property_id}" 
                                   style="display:none" {if $categoriesArr[categoryIndex].props[propIndex].property_id|in_array:$prop_id} checked="checked" {/if}/>
                                <a href="javascript:;" class="refine {if $categoriesArr[categoryIndex].props[propIndex].property_id|in_array:$prop_id}selected{/if}" propid="{$categoriesArr[categoryIndex].props[propIndex].property_id}"> 
                                    {$categoriesArr[categoryIndex].props[propIndex].property_name}
                                </a>
                            </li>
                            {/section} 
                        </div>
                    </ul>
                    {/if}
                </li>
                {/section}   
                
                <li>
                    <a href="#">Rate (Points)</a> 
                    <ul>	
                        <li style="height:30px; padding:15px 5px 5px 5px;"> 
                            <span style="display: inline-block; width: 230px; padding: 0 5px; font-size:11px;">
                                <input id="Slider1" type="slider" name="price" value="{$price}"/>
                            </span> 
                                {literal}
                                <script type="text/javascript" charset="utf-8">
                                    jQuery("#Slider1").slider({ from: 0, to: 100, limits: false, step: 1,smooth: true, round: 0, dimension: '', skin: "plastic", callback: 
                                    function( value ){ /* document.frmRefine.submit() */ frmRefine_POST(); } });
                                </script>
                               {/literal}
                        </li>	
                    </ul>
                </li>  
                {if $Numlanguage neq '0'}	
                <li>
                    <a href="#">Language</a>
                    <ul>
                        <div class="listing">

                            {section name=lang loop=$languageArr}
                            <li>
                                  <input type="checkbox" name="language_id[]" id="language_{$languageArr[lang].language_id}" value="{$languageArr[lang].language_id}" style="display:none;" 
                                  {if $languageArr[lang].language_id|in_array:$language_id} checked="checked" {/if} />				
                                  <a href="javascript:;" class="refine {if $languageArr[lang].language_id|in_array:$language_id} selected {/if}" stype="Z" typeId="{$languageArr[lang].language_id}" >
                                  {$languageArr[lang].language_name} ({$languageArr[lang].cnt})
                                  </a>
                            </li>
                          {/section}
                        </div> 
                    </ul>
                </li>	
                {/if}
                
                <li>
                    <a href="#">Availability</a>
                    <ul>
                        <div class="listing">
                            <li> 
                                <input type="checkbox" name="availability[]" id="avail_O" value="O" style="display:none" {if 'O'|in_array:$availability} checked="checked" {/if} />
                                <a href="javascript:;" class="refine {if 'O'|in_array:$availability} selected {/if}" stype="A" typeId="O"> Online({$avail1Num})</a>
                            </li>
                            <li> 
                                <input type="checkbox" name="availability[]" id="avail_L" value="L" style="display:none" {if 'L'|in_array:$availability} checked="checked" {/if} />
                                <a href="javascript:;" class="refine {if 'L'|in_array:$availability} selected {/if}" stype="A" typeId="L" > Local meet-up({$avail2Num})</a>
                            </li>
                            <li id="avail_coun" {if 'L'|in_array:$availability} style="display:block;" {else} style="display:none;" {/if}>
                               &nbsp;&nbsp;&nbsp;
                                <div id="coun">
                                    <select name="avail_country" id="avail_country" class="drop-down" onchange="avail_submit(this.value)">
                                        <option value="">Availability Country</option>
                                        {html_options options=$AvailcounArr selected=$availability_country}
                                    </select>
                                </div>
                                <br />
                                <div style="margin-top: 5px;">
                                    <div class="ausu-suggest" style="margin: 5px;">
                                        <input type="text" style="padding: 3px; width: 190px;" name="city" id="city" value="{$city}" autocomplete="off"/>
                                        <input name="cityid" id="cityid" value="{$availability_city}" type="hidden"  autocomplete="off" />
                                    </div>
                                    <div class='clear'></div>
                                    <div>
                                        <input style="float:right;margin: 5px;margin-right: 55px;" type="button" value="Go" onclick="CreateCountry()" class="button" />
                                    </div>
                                </div>
                                <br/>
                            </li>
                        </div>  
                    </ul>
                </li>   

                <input type="hidden" name="record_per_page" id="record_per_page" value="{$record_per_page}" style="visibility:hidden">
                <input type="submit" style="visibility:hidden">
            </form> 	
        </ul>
    </div>   
</div>