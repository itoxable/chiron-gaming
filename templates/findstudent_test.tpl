{literal}
<script type="text/javascript">
function redirect_function(id)
{
  document.forms["studentform"+id].submit();
}
function ManagerGeneral(url) 
{	
		var params ={
			'IsProcess': "Y"
			};
		//j('#pagination ul li:last').after('<img src="images/indicator.gif" alt="Loading" title="Loading"/>');	
		jQuery.ajax({
				type: "POST",
				url: url,
				data: params,				
				success: function(data){
					
					//alert(data);
						
						
						jQuery('#paging').html(data);
				}
		});
}
</script>
{/literal}
{if $IsProcess!="Y"}
<div id="paging"> 
{/if}
 <div class="right-panel">
      <h1 title="Find Student"><span>Find Student</span></h1>
      <div class="clear"></div>
 

   {if $SearchTxt neq ''}<div><b>Search By</b>  {$SearchTxt}</div>{/if}
	 {if $NumStudent >0}
	  {section name=studentRow loop=$StudentArr}
      <div class="game-list">
		  <div class="game-image">
		    <form name="student{$StudentArr[studentRow].user_id}" id="studentform{$StudentArr[studentRow].user_id}" method="post" 
			action="studentdetails.php?student_id={$StudentArr[studentRow].user_id}">
			<input type="hidden" name="price" value="{$price}" />
			<input type="hidden" name="languages" value="{$languages}" />
			<input type="hidden" name="availabilitys" value="{$availabilitys}" />
			<input type="hidden" name="games" value="{$games}" />
			<a href="#" onclick="redirect_function({$StudentArr[studentRow].user_id})">
			{if $StudentArr[studentRow].photo neq ''}
		     <img src="uploaded/user_images/thumbs/big_{$StudentArr[studentRow].photo}" alt="{$StudentArr[studentRow].name}" border="0"  />
			{else} 
			 <img src="images/student_thumb.jpg" alt="Student" border="0" />
			{/if}
			</a>
		  	
		  </div>
		  <div class="game-details">
            <h2><a href="#" onclick="redirect_function({$StudentArr[studentRow].user_id})" style="text-decoration:none;">{$StudentArr[studentRow].name}</a></h2>
            <p>{$StudentArr[studentRow].about}</p>
            <p class="clear"></p>
            <ul class="games-desc">
               <li><span>Language :</span> {if $StudentArr[studentRow].language neq ''}{$StudentArr[studentRow].language}{else}N/A{/if}</li>
			   <li><span>Availability :</span> {$StudentArr[studentRow].availability_type}</li>
            </ul>
            <ul class="games-desc">
               <li><span>Game :</span> {if $StudentArr[studentRow].game neq ''}{$StudentArr[studentRow].game} {else} N/A {/if}</li>
			   <li><span>Budget :</span> {if $StudentArr[studentRow].rate neq ''}${$StudentArr[studentRow].rate} {else} N/A {/if}</li>
            </ul>
            <div class="clear"></div>
			
            <div class="view"><!--a href="studentdetails.php?student_id={$StudentArr[studentRow].user_id}">view</a-->
			<a href="#" onclick="redirect_function({$StudentArr[studentRow].user_id})">view</a>
			</div>
			</form>
         </div>
         <div class="clear"></div>
      </div>
	 {/section} 
     {if $pagination_arr[1]}
	 <div class="clear"></div>
      <div class="pagin">
         <ul>
           {$pagination_arr[1]}
         </ul>
      </div>
	 {/if}
	 {else}
	    No Student is found......
     {/if}
{if $IsProcess!="Y"}
	</div> 
   </div>
 {/if}  
