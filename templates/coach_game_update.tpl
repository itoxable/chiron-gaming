
 {literal}
 <script language="javascript" src="js/contact_us.js"></script> 
 <!--<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
 <script type="text/javascript" src="js/jQuery.functions.pack.js"></script>

 
 <link rel="stylesheet" href="style/autosuggest.css" type="text/css" />
 <script type="text/javascript" src="js/jquery.ausu-autosuggest.min.js"></script>-->

  <script type="text/javascript">
        jQuery(document).ready(function() {
         
             <!-- AUTOCOMPLETE -->
			 jQuery.fn.autosugguest({  
				   className: 'ausu-suggest',
				  methodType: 'POST',
					minChars: 2,
					  rtnIDs: true,
					dataFile: 'autocomplete.php?action=populateCity'
			});
        });
  </script> 
 
<script language="javascript" type="text/javascript">


function getChangeGame(id)
{
	jQuery.post('game_details.php',{'game_id':id, 'action':'get_content'}, function(data){
		jQuery('#dynamic_controls').html(data);
	});
}
function CheckFields(){
  
    if(document.getElementById('profile').value =='Y' && document.getElementById('is_partner').value =='0'){
        if(document.getElementById('avail_L').checked == true){
            if(document.getElementById('availability_city').value==''){
                jQuery('#FormErrorMsg').show();
                jQuery('#FormErrorMag').html("Please enter availability city");
                jQuery('#availability_city').attr('style','border:1px solid #FF0000');
                return false; 
            }	
        }
    }  	  
 
 
    if(document.getElementById('game_id').value=='' ){
        jQuery('#FormErrorMsg').show();
        jQuery('#FormErrorMsg').html("Please select a game");
            //jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
        return false;
    }
    if(jQuery('#rateA').val() == ''){
        jQuery('#FormErrorMsg').show();
        jQuery('#rateA').attr('style','border:1px solid #FF0000');
        jQuery('#FormErrorMsg').html("You must define your rates");
        return false;    
    }
    if(jQuery('#rateB').val() == ''){
        jQuery('#FormErrorMsg').show();
        jQuery('#rateB').attr('style','border:1px solid #FF0000');
        jQuery('#FormErrorMsg').html("You must define your rates");
        return false;
    }  
    if(jQuery('#rateC').val() == ''){
        jQuery('#FormErrorMsg').show();
        jQuery('#rateC').attr('style','border:1px solid #FF0000');
        jQuery('#FormErrorMsg').html("You must define your rates");
        return false;
    } 
   
    return true;
 }
     
 function showhide_div(){
    if(document.getElementById('avail_L').checked == true){  
        jQuery('#avail_city').show();
        jQuery('#availability_city').show();
    }
    if(document.getElementById('avail_L').checked == false){  		
      jQuery('#avail_city').hide();
      jQuery('#availability_city').hide();
    }  			
} 

 function keyCheck(eventObj, obj)
{
    var keyCode
 
    // Check For Browser Type
    if (document.all){
        keyCode=eventObj.keyCode
    }
    else{
        keyCode=eventObj.which
    }
 
    var str=obj.value
 
    if(keyCode==46){
        if (str.indexOf(".")>0){
            return false
        }
    }
 
    if((keyCode<46 || keyCode >57)   &&   (keyCode >31)){ // Allow only integers and decimal points
        return false
    }
    
    return true
}
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

</script>
{/literal}
<div align="right">
	{if $is_coach eq '0'}
	<!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
	<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
	{/if}
	{if $is_partner eq '0'}
	<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
	<!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
	{/if} 
</div>
<div class="content">
    <!-- tab -->
    <ul class="tabflex">
        <li><a href="schedule.php"><span>Schedule</span></a></li>
        <li><a href="messages.php"><span>Messages</span></a></li>
        <li><a href="profile.php"><span>Profile</span></a></li>
        {if $is_coach eq '1'}<li><a href="javascript:;" class="active"><span>Coach</span></a></li>{/if}
        {if $is_partner eq '1'}<li><a href="training_partner_game.php"><span>Training Partner</span></a></li>{/if}
        <li><a href="cashier.php"><span>Cashier</span></a></li>
        <!--    <li><a href="#"><span>Students</span></a></li>-->
    </ul>
    <!-- tab -->
    <div class="clear"></div>
    <div class="tabular-content"> 
        <div class="total-message">&nbsp;&nbsp;</div>
	<div class="clear"></div>                     
        <div>
        <div class="register" style="margin-top: 20px; width: auto">
            <div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:10px;">{if $ermsg neq ''} {$ermsg}{/if} </div>
            <form name="game" id="game" action="" method="post">
                <input type="hidden" name="user_game_id" value="{$user_game_id}" />
                <input type="hidden" name="profile" id="profile" value="{$profile}" />
                <input type="hidden" name="is_coach" id="is_coach" value="{$is_coach}" />
                <input type="hidden" name="is_partner" id="is_partner" value="{$is_partner}" />
		{if $profile eq 'Y' && $is_coach eq '0'}
                <fieldset>
                    <label>Introduction:</label>
                    <div style="float: left">
                        <textarea name="about-text" maxlength="300" onkeyup="countLenght(this,'about-lenght')" id="about-text"></textarea>
                        <p class="clear"></p>
                        <span id="about-lenght">0/300</span>
                        <div style="margin-top: 10px;width: 600px;">
                            This Intro will be displayed on the Find Coach page before users view your full profile. There is a 300-character limit, so write carefully!                            
                        </div>
                    </div>
                    <p class="clear"></p>
                </fieldset>
                <div style="margin: 20px; margin-top: 0; border-bottom: 1px solid #ccc">&nbsp;&nbsp;</div>    
		
                {/if}
                    <fieldset>
                    {if $profile eq 'Y' && $$is_coach eq '0'}
                    <label>Availability :</label>
                    <input type="checkbox" name="availability_type1" class='check' value="O" /><label class='chklabel'>Online</label>
                    <input type="checkbox" name="availability_type2" class='check' id="avail_L" onclick="showhide_div()" value="L" /><label class='chklabel'>Local</label>
                    <p class="clear"></p>
                    <label id="avail_city" {if $Formval.availability_type =='L'} {else} style='display:none' {/if}>City (Local meet-up) :</label>
                    <div class="ausu-suggest">
                        <input name="availability_city" id="availability_city" type="text" value="{$Formval.availability_city}" class="con-req" autocomplete="off" 
                        {if $Formval.availability_type =='L'} {else} style='display:none' {/if} />
                        <input name="cityid" id="cityid" type="hidden"  value="" autocomplete="off" />
                    </div>
                    <div class="clear"></div>

                    <p class="clear"></p>
                    {/if}
                    <div class="clear"></div>
                    <label>Select Game :</label>
                    <select name="game_id" id="game_id" class="con-req" onchange="getChangeGame(this.value)" >
                        <option value="">Select</option>
                            {html_options options=$GameArr selected=$Formval.game_id}
                    </select>
                    <!--label>Active :</label>
                    <input name="is_active" id="is_active" type="checkbox" value="Y" {if $Formval.is_active eq ''} checked="checked" {else}{if $Formval.is_active =='Y'}checked{/if}{/if}-->
                    <p class="clear"></p>
                    <label>1h Rate:</label>
                    <input name="rateA" id="rateA" type="text" value="{$Formval.rate_A}" onkeypress="return keyCheck(event, this);" />
                    <p class="clear"></p>
                    <label>2h Rate:</label>
                    <input name="rateB" id="rateB" type="text" value="{$Formval.rate_B}" onkeypress="return keyCheck(event, this);" />
                    <p class="clear"></p>
                    <label>3h Rate:</label>
                    <input name="rateC" id="rateC" type="text" value="{$Formval.rate_C}" onkeypress="return keyCheck(event, this);" />
                    <p class="clear"></p>
                    <!--THE DYNAMIC CONTROL WILL PRINT HERE - PROSENJIT-->
                   
                    
                    {section name=categoryIndex loop=$gameCategoriesArr}
                    <label class="game-extra">{$gameCategoriesArr[categoryIndex].category_name}:</label>
                        <div>
                            {section name=propIndex loop=$gameCategoriesArr[categoryIndex].properties}
                             <span>
                                <input class="checkbox_" type="checkbox" {if $gameCategoriesArr[categoryIndex].properties[propIndex].checked}checked{/if} name="prop_id[]" value="{$gameCategoriesArr[categoryIndex].category_id}_{$gameCategoriesArr[categoryIndex].properties[propIndex].property_id}" />&nbsp;{$gameCategoriesArr[categoryIndex].properties[propIndex].property_name}&nbsp;&nbsp;
                            </span>
                            
                            {/section}
                        </div>
                        <br class="clear" />
                    {/section}
                    
                   

                    <p class="clear"></p>
                    <label>Experience :</label>
                    <textarea  name="experience" id="experience">{$Formval.experience}</textarea>
                    <p class="clear"></p>
                    <label>Lesson Plan :</label>
                    <textarea  name="lesson_plan" id="lesson_plan">{$Formval.lesson_plan}</textarea>
                    <p class="clear"></p>
                    

                    <label>&nbsp;</label>
                    <input name="submit" class="submit" value="{$SubmitButton}" type="submit" onclick='return CheckFields()' />
                    <input name="" class="cancel" value="Cancel" type="button" onclick="window.location.href='coach_game.php'" />
                </fieldset>
            </form>
        </div>
    </div>
</div>
</div>
<script language="javascript">
       changePageTitle('Edit Game');
</script>