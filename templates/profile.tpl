<div align="right">
	{if $is_coach eq '0'}
	<!--label>Become a Coach :</label> <input type="radio" name="user_type" value="1" onclick="coach_game()" /-->
	<a class="button" href="coach_game_update.php?profile=Y"><span>Become a Coach</span></a>
	{/if}
	{if $is_partner eq '0'}
	<a class="button" href="training_partner_game_update.php?profile=Y">Become Training Partner</a>
	<!--label>Become a Training Partner :</label> <input type="radio" name="user_type" value="3" onclick="tp_game()"/-->
	{/if} 
</div>
<div class="content">
   		
		{* Start loading Tabs *}
		{include file='user_tabs.tpl' active_tab='profile'}
		{* End loading Tabs *}
		 
        <div class="clear"></div>
        <div class="tabular-content"> 
			
		 <div class="total-message">&nbsp;&nbsp;</div> 
		 <div class="clear"></div>                     
          <div class="profile-list">
          <br />
		 {if $userArr.photo neq ''} 
		  <div class="game-image"><img src="uploaded/user_images/thumbs/big_{$userArr.photo}"></div>
		  {else}
		    {if $is_coach eq '1'}
			 <div class="game-image"><img src="images/coach_thumb.jpg"></div>
			{else}
			 <div class="game-image"><img src="images/student_thumb.jpg"></div>
			{/if}
		 {/if}  
		 
         <div class="game-details user_details" style="width: 80%">
		    <!--div>{if $msg neq '' } {$msg} {/if}</div-->
			<div class="clear"></div>
			<dl>
				<dt>Profile Type:</dt>
				<dd>{$userArr.type}</dd>
			</dl>
			<dl>
				<dt>Username:</dt>
				<dd>{$userArr.username}</dd>
			</dl>
			<dl>
				<dt>Email:</dt>
				<dd>{$userArr.email}</dd>
			</dl>
                        
                        <dl>
				<dt>Timezone:</dt>
				<dd>{$time_zone}</dd>
			</dl>
                        
			<dl>
				<dt>Password:</dt>
				<dd>{$userArr.pass_word}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="change_password.php" style="color:#0033FF;">Change</a></dd>
			</dl>
		
			{section name=type loop=$typeArrr}
			 {if $typeArr[type].user_type_id eq '1'}<div style="padding:3px 0 3px 0;">Paypal Email ID : {$userArr.paypal_email_id}</div>{/if}
			{/section}
			
			
			<dl>
				<dt>Language:</dt>
				<dd>{if $userArr.language neq ''}{$userArr.language}{else}N/A{/if}</dd>
			</dl>

            <p class="clear"></p>
            <div class="clear"></div>
			<div style="margin-top: 10px">
                            <a class="button" href="edit_profile.php"><span>edit profile</span></a>
                            <a class="button" href="user_settings.php"><span>settings</span></a>
			</div>
            
         </div>
         <div class="clear"></div>
      
	  </div>
     </div>
    </div>
                        
 <script language="javascript">
       changePageTitle('Profile');
</script>