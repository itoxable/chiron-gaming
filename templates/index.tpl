{literal}
<link rel="stylesheet" href="style/nivo-slider.css" type="text/css" media="screen" />
<script type="text/javascript" src="scripts/jquery.nivo.slider.js"></script>
<script type="text/javascript">
jQuery(window).load(function() {
        jQuery('#slider').nivoSlider({pauseTime:5000});
    });
</script>
<link href='https://fonts.googleapis.com/css?family=Dosis:400,200,600' rel='stylesheet' type='text/css'>
<div id="fb-root"></div>
<script>
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
{/literal}

<div class="content-right">
    <div class="face_book"><!--img src="images/facebook_snap.jpg" height="285" width="300" /-->
	<div class="fb-like-box shadowed" data-href="{$facebook}" data-width="290" data-height="285" data-show-faces="true" data-stream="false" data-header="true"></div>
    </div>

     
    <div class="advertise" style="overflow:hidden;">
		<h2 title="Advertisement"></h2>
	   <script charset="utf-8" src="https://widgets.twimg.com/j/2/widget.js"></script>
		<script>
			new TWTR.Widget({ldelim}
			  version: 2,
			  type: 'profile',
			  rpp: 4,
			  interval: 30000,
			  width: 290,
			  height: 375,
			  theme: {ldelim}
			    shell: {ldelim}
				  background: '#003366',
				  color: '#ffffff'
				{rdelim},
				tweets: {ldelim}
				  background: '#ffffff',
				  color: '#000000',
				  links: '#0084b4'
				{rdelim}
			  {rdelim},
			  features: {ldelim}
			    scrollbar: false,
			    loop: false,
			    live: false,
			    behavior: 'all'
			  {rdelim}
			{rdelim}).render().setUser('{$twitter}').start();
		</script>
		
    </div>
</div>
<div class="content-left">
    <div class="slider-wrapper theme-default">
        <div class="ribbon"></div>
        <div id="slider" class="nivoSlider">
            {section name=img loop=$homenevoArr}
            {assign var="i" value=$smarty.section.img.index+1}
            <img src="uploaded/home_images/{$homenevoArr[img].image_file}" alt="{$homenevoArr[img].image_alt}" title="#htmlcaption{$i}" />
            {/section}
        </div>
        {section name=desc loop=$homenevoArr}
        {assign var="x" value=$smarty.section.desc.index+1}
			
        <div id="htmlcaption{$x}" class="nivo-html-caption">
            <h2>{$homenevoArr[desc].title}</h2>
            <p>{$homenevoArr[desc].description}</p>
            <a href="{if $homenevoArr[desc].banner_link neq ''}{$homenevoArr[desc].banner_link}{else}news.php{/if}"><span class="more_news"></span></a>
        </div>
        {/section}      
    </div>
	
    <div class="window shadowed"> 
        <div class="windowtitle">Popular Coaches</div>
		<div class="windowcontent">
			<div class="coachresume" style="border-right: 1px solid #003366;">
				<div class="coachtitle">Most Lesson</div>
				<div class="coach_list">
					{section name=lCoach loop=$lessonsCoachesArr}
						{assign var="i" value=$smarty.section.lCoach.index+1}
							<a href="javascript:;" onclick="showCoachDetails('lessonscoachdetails_{$lessonsCoachesArr[lCoach].user_id}')"><span>#{$i} {$lessonsCoachesArr[lCoach].username}</span></a>
						
					{/section} 
				</div>
			</div>
			<div class="coachresume" style="border-right: 1px solid #003366;">
				<div class="coachtitle">Highest Ratings</div>
				<div class="coach_list">
					{section name=rCoach loop=$ratingCoachesArr}
						{assign var="i" value=$smarty.section.rCoach.index+1}
							<a href="javascript:;" onclick="showCoachDetails('ratingcoachdetails_{$ratingCoachesArr[rCoach].user_id}')"><span>#{$i} {$ratingCoachesArr[rCoach].username}</span></a>
						
					{/section} 
				</div>
			</div>
			<div class="coachresume" style="border-right: 1px solid #003366;">
				<div class="coachtitle">Featured Coaches</div>
				<div class="coach_list">
					{section name=fCoach loop=$featuredCoachesArr}
						{assign var="i" value=$smarty.section.fCoach.index+1}
							<a href="javascript:;" onclick="showCoachDetails('featuredcoachdetails_{$featuredCoachesArr[fCoach].user_id}')"><span>#{$i} {$featuredCoachesArr[fCoach].username}</span></a>
						
					{/section} 
				</div>
			</div>
			<div class="coachresume" style="width: 210px;">
				{section name=fCoach loop=$featuredCoachesArr}
				{assign var="i" value=$smarty.section.fCoach.index+1}
					<div class="coaches" id="featuredcoachdetails_{$featuredCoachesArr[fCoach].user_id}" style="{if $smarty.section.fCoach.index eq '0'} display: block; {/if}">
						<div style="padding: 5px">
							<a href="javascript:;" {if $smarty.session.user_id neq ''}onclick="buildPageLink('{$featuredCoachesArr[fCoach].user_id}')"{else}onClick="openLogin('/coachdetails.php?coach_id={$featuredCoachesArr[fCoach].user_id}')"{/if} class="coaches_photo"> 
								<img width="60px" height="60px" src="{if $featuredCoachesArr[fCoach].photo}uploaded/user_images/thumbs/mid_{$featuredCoachesArr[fCoach].photo}{else}images/featured_img.jpg{/if}" alt="" border="0"  />
							</a>
							<div style="" class="coaches_name">
								<a href="javascript:;" {if $smarty.session.user_id neq ''} onclick="buildPageLink('{$featuredCoachesArr[fCoach].user_id}')" {else}onClick="openLogin('/coachdetails.php?coach_id={$featuredCoachesArr[fCoach].user_id}')"{/if}>{$featuredCoachesArr[fCoach].username}</a>
							</div>
							<div class="clear"></div>
							
							<div class="coaches_description" id="coaches_description_{$featuredCoachesArr[fCoach].user_id}">
								<div style="overflow: hidden;height: 90px;">
									{$featuredCoachesArr[fCoach].about}
								</div>
								<div class="btn">
									{if $smarty.session.user_id neq ''}<a onclick="buildPageLink('{$featuredCoachesArr[fCoach].user_id}')" href="javascript:;" target="_self">More</a>{/if}
								</div>
							</div>
						</div>
					</div>
			  
			 {/section}  
			 
			 {section name=fCoach loop=$ratingCoachesArr}
				{assign var="i" value=$smarty.section.fCoach.index+1}
					<div class="coaches" id="ratingcoachdetails_{$ratingCoachesArr[fCoach].user_id}" style="">
						<div style="padding: 5px">
							<a href="javascript:;" {if $smarty.session.user_id neq ''}onclick="buildPageLink('{$ratingCoachesArr[fCoach].user_id}')"{else}onClick="openLogin('/coachdetails.php?coach_id={$ratingCoachesArr[fCoach].user_id}')"{/if} class="coaches_photo"> 
								<img width="60px" height="60px" src="{if $ratingCoachesArr[fCoach].photo}uploaded/user_images/thumbs/mid_{$ratingCoachesArr[fCoach].photo}{else}images/featured_img.jpg{/if}" alt="" border="0"  />
							</a>
							<div style="" class="coaches_name">
								<a href="javascript:;" {if $smarty.session.user_id neq ''}onclick="buildPageLink('{$ratingCoachesArr[fCoach].user_id}')"{else}onClick="openLogin('/coachdetails.php?coach_id={$ratingCoachesArr[fCoach].user_id}')"{/if}>{$ratingCoachesArr[fCoach].username}</a>
							</div>
							<div class="clear"></div>
							
							<div class="coaches_description" id="coaches_description_{$ratingCoachesArr[fCoach].user_id}">
								<div style="overflow: hidden;height: 90px;">
									{$ratingCoachesArr[fCoach].about}
								</div>
								<div class="btn">
									{if $smarty.session.user_id neq ''}<a onclick="buildPageLink('{$ratingCoachesArr[fCoach].user_id}')" href="javascript:;" target="_self">More</a>{/if}
								</div>
							</div>
						</div>
					</div>
			  
			 {/section} 
			 {section name=fCoach loop=$lessonsCoachesArr}
				{assign var="i" value=$smarty.section.fCoach.index+1}
					<div class="coaches" id="lessonscoachdetails_{$lessonsCoachesArr[fCoach].user_id}" style="">
						<div style="padding: 5px">
							<a href="javascript:;" {if $smarty.session.user_id neq ''}onclick="buildPageLink({$lessonsCoachesArr[fCoach].user_id})"{else}onClick="openLogin('/coachdetails.php?coach_id={$lessonsCoachesArr[fCoach].user_id}')"{/if} class="coaches_photo"> 
								<img width="60px" height="60px" src="{if $lessonsCoachesArr[fCoach].photo}uploaded/user_images/thumbs/mid_{$lessonsCoachesArr[fCoach].photo}{else}images/featured_img.jpg{/if}" alt="" border="0"  />
							</a>
							<div style="" class="coaches_name">
								<a href="javascript:;" {if $smarty.session.user_id neq ''}onclick="buildPageLink('{$lessonsCoachesArr[fCoach].user_id}')"{else}onClick="openLogin('/coachdetails.php?coach_id={$lessonsCoachesArr[fCoach].user_id}')"{/if}>{$lessonsCoachesArr[fCoach].username}</a>
							</div>
							<div class="clear"></div>
							
							<div class="coaches_description" id="coaches_description_{$lessonsCoachesArr[fCoach].user_id}">
								<div style="overflow: hidden;height: 90px;">
									{$lessonsCoachesArr[fCoach].about}
								</div>
								<div class="btn">
									{if $smarty.session.user_id neq ''}<a onclick="buildPageLink('{$lessonsCoachesArr[fCoach].user_id}')" href="javascript:;" target="_self">More</a>{/if}
								</div>
							</div>
						</div>
					</div>
			  
			 {/section} 
			
			</div>
			<div class="clear"></div>
		
		</div>
		
    </div>   
						
	<div class="window shadowed"> 
		<div class="windowtitle">In the Forums</div>
		<div class="windowcontent">
			<div class="coachresume" style="border-right: 1px solid #003366;width: 340px;">
				<div class="coachtitle">Popular Threads</div>
				<div class="coach_list">
				
					{section name=popularIndex loop=$ForumPopularArr}
					<a class="forumlink" href="/forum/viewtopic.php?f={$ForumPopularArr[popularIndex].forum_id}&t={$ForumPopularArr[popularIndex].topic_id}" onclick="">
						<div class="forumtitle" style="float: left;"><span style="color: #004b68;">[{$ForumPopularArr[popularIndex].forum_name|truncate:8:"":true}] </span>{$ForumPopularArr[popularIndex].topic_title|truncate:36:"...":true}</div>
						<div style="float: right; color: #666;">{$ForumPopularArr[popularIndex].topic_views} views</div>
						<div class="clear"></div>
					</a>
					{/section} 
				
				</div>
			</div>
			<div class="coachresume" style="width: 341px;">
				<div class="coachtitle">Latest Threads</div>
				<div class="coach_list">
                                    {section name=latestIndex loop=$ForumRecentArr}
                                    <a class="forumlink" href="/forum/viewtopic.php?f={$ForumRecentArr[latestIndex].forum_id}&t={$ForumRecentArr[latestIndex].topic_id}" onclick="">
                                        <div class="forumtitle" style="float: left;"><span style="color: #004b68;">[{$ForumRecentArr[latestIndex].forum_name|truncate:8:"":true}] </span>{$ForumRecentArr[latestIndex].topic_title|truncate:36:"...":true}</div>
                                        <div style="float: right; color: #666;">{$ForumRecentArr[latestIndex].topic_views} views</div>
                                        <div class="clear"></div>
                                    </a>
                                    {/section} 
				
				</div>
			</div>
			<div class="clear"></div>
		</div>
		
    </div>
		{literal}
			<script>
				function showCoachDetails(coach_id){

					if(jQuery("#"+coach_id).is(':visible')){
						return;
					}
					var i = 0;
					jQuery.each(jQuery('.coaches'), function() {
						if(jQuery(this).is(':visible')){
							jQuery(this).fadeOut('fast', function(){jQuery("#"+coach_id).fadeIn('fast');});
							return false;
						}
						i++;
						
				   });
				   if(i == jQuery('.coaches').length)
						jQuery("#"+coach_id).fadeIn('slow');
				 
				}
			</script>
		{/literal}
 </div>  
<script language="javascript">
       changePageTitle();
</script>
	 
 