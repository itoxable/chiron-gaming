{literal}
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jQuery.functions.pack.js"></script>
<script type="text/javascript" src="js/jquery.dateLists.min.js"></script>
<script language="javascript" src="js/contact_us.js"></script>
<link rel="stylesheet" type="text/css" href="style/ui.dropdownchecklist.css" />
<link rel="stylesheet" type="text/css" href="style/jquery.dateLists.css" />
<script type="text/javascript" src="js/ui.core.js"></script>
<script type="text/javascript" src="js/ui.dropdownchecklist.js"></script>
<link rel="stylesheet" href="style/autosuggest.css" type="text/css" />
<script type="text/javascript" src="js/jquery.ausu-autosuggest.min.js"></script>


<script language="javascript">
jQuery(document).ready(function(){
      jQuery("#s2").dropdownchecklist({ width: 150 });
      jQuery("#s4").dropdownchecklist({ maxDropHeight: 120 });
<!-- AUTOCOMPLETE -->
    jQuery.fn.autosugguest({  
           className: 'ausu-suggest',
          methodType: 'POST',
            minChars: 2,
              rtnIDs: true,
		    dataFile: 'autocomplete.php?action=populateCity'
    });
  });	
function CheckFields()
{
  
  if(jQuery("#register-email").isEmpty())
    	 jQuery("#register-email").attr('style','border:1px solid #FF0000');
  if(jQuery("#name").isEmpty())
    	 jQuery("#name").attr('style','border:1px solid #FF0000');
  if(jQuery("#password1").isEmpty())
    	 jQuery("#password1").attr('style','border:1px solid #FF0000');
  if(jQuery("#password2").isEmpty())
    	 jQuery("#password2").attr('style','border:1px solid #FF0000');
  if(jQuery("#city").isEmpty())
    	 jQuery("#city").attr('style','border:1px solid #FF0000');	
  if(jQuery("#txtCaptcha").isEmpty())	
         jQuery("#txtCaptcha").attr('style','border:1px solid #FF0000');
 

		 
  if(!jQuery("#register-email").emailCheck() || document.getElementById('register-email').value=='' ) 
   {
    
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please Enter a Valid email");
	jQuery("#register-email").attr('style','border:1px solid #FF0000');
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);

    return false;
   }
    if(document.getElementById('name').value=='')
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please enter your UserID");
	jQuery("#name").attr('style','border:1px solid #FF0000');
	//jQuery('#FormErrorMsg').fadeOut(2000);
    return false;
   }
    if(document.getElementById('password1').value=='' ) 
   {
    
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please Enter a password");
	jQuery("#password1").attr('style','border:1px solid #FF0000');
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);

    return false;
   }
   else
   {
        if(jQuery('#password1').val().length<7)
		{
		 jQuery('#FormErrorMsg').show();
		 jQuery('#FormErrorMsg').html("Password Minimum <b>7 Characters</b>");
		 jQuery("#password1").attr('style','border:1px solid #FF0000');
		 return false;
		} 
    }
    if(document.getElementById('password2').value=='' ) 
    {
    
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please Enter confirm password");
	jQuery("#password2").attr('style','border:1px solid #FF0000');
	//jQuery('#FormErrorMsg').fadeOut(2000);
    return false;
   }
   if(document.getElementById('password2').value!=document.getElementById('password1').value)
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Password doesn't match");
	jQuery("#password2").attr('style','border:1px solid #FF0000');
	//jQuery('#FormErrorMsg').fadeOut(2000);
    return false;
   }
     
   if(document.getElementById('city').value=='')
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please enter city");
	jQuery("#city").attr('style','border:1px solid #FF0000');
	//jQuery('#FormErrorMsg').fadeOut(2000);
    return false;
   }     
 
  return true;
 
}

</script>
{/literal}
  
  
 <div class="right-panel">
        <h1 title="Find Student"><span>Registration</span></h1>
        <div class="clear"></div>
    
      <div class="register">
	  <div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:10px;">{if $ermsg neq ''} {$ermsg}{/if} </div>
      <form name="contact" id="Contact" enctype="multipart/form-data" method='post'>
	     <fieldset>
            <label>Email :</label>
            <input name="email" type="text" id="register-email" class="con-req" value="{$Formval.email}" />
			<label>User ID :</label>
            <input name="name" id="name" type="text" value="{$Formval.name}" class="con-req" />
			<p class="clear"></p>
            <label>Password :</label>
            <input name="password1" type="password" id="password1" class="con-req" value="{$Formval.password1}" />
            <label>Confirm Password :</label>
            <input name="password2" type="password" id="password2" class="con-req" value="{$Formval.password2}" />
            <p class="clear"></p>
			
            <label>City :</label><div class="ausu-suggest">
            <input name="city" id="city" type="text" value="{$Formval.city}" class="con-req" autocomplete="off" />
			<input name="cityid" id="cityid" type="hidden"  value="" autocomplete="off" />
			</div>
            <p class="clear"></p>
<!-- 			<label>Skype Id :</label> -->
<!--             <input name="skype_id" id="skype_id" type="text" value="{$Formval.skype_id}" /> -->
            <label>Language :</label>
			<select id="s2" multiple="multiple" name="language[]" class="selectbox">
			{section name=lang loop=$langArr}
             <option value="{$langArr[lang].language_id}" {if $langArr[lang].language_id eq '1'} selected="selected" {/if}>{$langArr[lang].language_name}</option>
            {/section}
            </select>
			 <p class="clear"></p>
			<label>Captcha :</label>
            <div style="float:left; width:180px;"><!--img src="CaptchaSecurityImages.php?characters=7" /--> <img src="captcha.php" id="captcha" />
			<br />
			<a href="javascript:;" onclick="document.getElementById('captcha').src='/captcha.php?'+Math.random();document.getElementById('txtCaptcha').focus();" 
			id="change-image">Not readable? Change text.</a>
			</div>
			
            <label>Security Code :</label>
			<input name="txtCaptcha" id="txtCaptcha" type="text" value="" />
         
            <p class="clear"></p>
			<div   style="margin-top:10px;">&nbsp;</div>
            <label>&nbsp;</label>
            <input name="submit" class="submit" value="Submit" type="submit" onclick='return CheckFields()' />
            <input name="" class="cancel" value="Reset" type="reset" />
         </fieldset>
      </form>
      </div>
      <div class="clear"></div>
   </div>
  