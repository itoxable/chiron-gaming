<div id="paging">  
	<div class="content">
   		
		{* Start loading Tabs *}
		{include file='user_tabs.tpl' active_tab='reviews'}
		{* End loading Tabs *}
		
        <div class="clear"></div>
        <div class="tabular-content">
            <div class="tab_sub">
            	<ul>
                	{if $is_coach eq '1' or $is_partner eq '1'}<li><a href="my_review.php" class="active">My Reviews</a></li>{/if}
					<li><a href="javascript:;" >My Video Reviews</a></li>
					<li><a href="coach_review.php" class="active">Reviews to User</a></li>
					<li><a href="video_review.php" class="active">Reviews to video</a></li>
                    <!--li><a href="my_video_review.php">My Videos</a></li-->
					<!--li><a href="#">Coach Reviews by me</a></li-->
					<!--li><a href="#">Video Reviews by me</a></li-->
                </ul>
            </div>
			
			<div class="clear"></div>  
			 {if $Numvideo>0}
			  <div>
			      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabular_row">
				{section name=vdorevrow loop=$videoReviewArr}
                  <tr {if $smarty.section.vdorevrow.index%2 neq 0}class="alter"{/if}>
                    <td width="50" valign="top">
					{if $videoReviewArr[vdorevrow].photo neq ''}
					<img src="uploaded/user_images/thumbs/{$videoReviewArr[vdorevrow].photo}" border="0" />
					{else}
					<img src="images/avatar_smallest.jpg" border="0" />
					{/if}</td>
                    <td width="155" valign="top"><strong><a href="{$videoReviewArr[vdorevrow].link}">{$videoReviewArr[vdorevrow].name}</a></strong><br />
					<i>{$videoReviewArr[vdorevrow].user_type}</i>
					</td>
					<td width="120" valign="top"><a href="videodetails.php?video_id={$videoReviewArr[vdorevrow].video_id}">{$videoReviewArr[vdorevrow].video_title}</a></td>
                    <td width="100" valign="top" style="padding-right:10px;"><strong>{$videoReviewArr[vdorevrow].review_date}</strong></td>
                    <td valign="top">{$videoReviewArr[vdorevrow].review_comment|nl2br}</td>
                     <td width="20"><!--a href="#"><img src="images/close_small.gif" alt="" border="0" /></a--></td>
                  </tr>
                 {/section}                          
                </table>
				</div>
        	<div class="clear"></div>
            <br />
            <div class="tab_sub">
            	&nbsp;
            </div>
            <div class="tab_search">
            	&nbsp;
            </div>
            <div class="tab_pagination">
            	{if $pagination_arr[1]}
				 <div class="clear"></div>
				  <div class="pagin">
					 <ul>
					   {$pagination_arr[1]}
					 </ul>
				  </div>
				 {/if}
            </div>   
            <div class="clear"></div> 
			{else}
				<div align="left" style="padding:10px;">No record found</div>
			{/if}
      </div>
   </div>
</div>