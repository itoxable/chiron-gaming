{literal}
<script type="text/javascript" src="jss/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="jss/htmltooltip.js"></script>
<style type="text/css">
 
div.htmltooltip {
position: absolute; /*leave this and next 3 values alone*/
z-index: 1000;
left: -1000px;
top: -1000px;
background: #FFFDD8;
border: 2px solid #000000;
color:#000000;
padding: 3px;
width: 250px; /*width of tooltip*/
}
 
</style>


<script>
function ListSorting(sorting_url,sorting_by,ladder)
{
var params = {
	'list_for':'sorting',
	'sorting_by': sorting_by,
	'IsProcess': 'Y',
	'ladder_id': ladder
};
alert(ladder);
jQuery.ajax({
	type: "GET",
	url: sorting_url,
	data: params,			
	dataType: 'html',
	success: function(data)
	{				
		jQuery("#paging").html(data);
	}
});
return false;		
}
function expand(id)
{
  jQuery('#small_'+id).css('display','none');
  jQuery('#big_'+id).css('display','block');
  
}
function collapse(id)
{
  jQuery('#big_'+id).css('display','none');
  jQuery('#small_'+id).css('display','block');
  
}
</script>
<script type="text/javascript">
function redirect_function(id)
{
  document.forms["coachform"+id].submit();
}

</script>
{/literal}
<div id="paging"> 
 <div class="right-panel">
      <h1><span>Find Coach</span></h1>
      <div class="searchpanel">
         <form action="" method="post">
              <fieldset>
			  <label></label>
              <select for="sirt" name="sort_by" onchange="subForm(this.value);">
			      <option value="">Sort by</option>
                  <option value="rating" {if $sort_by eq 'rating'} selected="selected" {/if}>Star Rating (Low)</option>
				  <option value="rating-DESC" {if $sort_by eq 'rating-DESC'} selected="selected" {/if}>Star Rating (High)</option>
				  <option value="rate" {if $sort_by eq 'rate'} selected="selected" {/if}>Rate (Low)</option>
				  <option value="rate-DESC" {if $sort_by eq 'rate-DESC'} selected="selected" {/if}>Rate (High)</option>
               </select>
               </fieldset>
            </form>
           <div class="clear"></div>
      </div>
      <div class="clear"></div>
      <!--div class="sortby">
             <form action="" method="post">
			   <fieldset>
               <label>Sort by:</label>
               <select for="sirt" name="sort_by" onchange="ListSorting('findcoach.php',this.value)">
                  <option value="ASC" {if $sort_by eq 'ASC'} selected="selected" {/if}>Star Rating (Low)</option>
				  <option value="DESC" {if $sort_by eq 'DESC'} selected="selected" {/if}>Star Rating (High)</option>
               </select>
               </fieldset>
            </form>
      </div-->
      <div class="clear"></div>
 

   {if $SearchTxt neq ''}<div><b>Search By</b>  {$SearchTxt}</div>{/if}
	 {if $NumCoach >0}
	  {section name=coachRow loop=$CoachArr}
      <div class="game-list-block">
        <form name="coach{$CoachArr[coachRow].user_id}" id="coachform{$CoachArr[coachRow].user_id}" method="post" 
			action="coachdetails.php?coach_id={$CoachArr[coachRow].user_id}">
			<input type="hidden" name="ladders" value="{$ladders}" />
			<input type="hidden" name="price" value="{$price}" />
			<input type="hidden" name="races" value="{$races}" />
			<input type="hidden" name="ladders" value="{$ladders}" />
			<input type="hidden" name="ratings" value="{$ratings}" />
			<input type="hidden" name="servers" value="{$servers}" />
			<input type="hidden" name="languages" value="{$languages}" />
			<input type="hidden" name="regions" value="{$regions}" />
			<input type="hidden" name="availabilitys" value="{$availabilitys}" />
			<input type="hidden" name="game_id" value="{$game_id}" />
		    <div class="game-image-block">
			<a href="#" onclick="redirect_function({$CoachArr[coachRow].user_id})">
			{if $CoachArr[coachRow].photo neq ''}
		     <img src="uploaded/user_images/thumbs/big_{$CoachArr[coachRow].photo}" alt="Coach" border="0"  />
			{else}
			 <img src="images/coach_thumb.jpg">
			 <!--div style="width:198px; padding:5px; float:left; display:block;">&nbsp;</div--> 
		    {/if}
			</a> 
		    </div> 
		  <div class="game-details-block" style="height:255px;" id="small_{$CoachArr[coachRow].user_id}">
		    <!--<h2><a href="#" onclick="redirect_function({$CoachArr[coachRow].user_id})" style="text-decoration:none;">{$CoachArr[coachRow].name}</a></h2>
            <p>{$CoachArr[coachRow].about|truncate:200:"...":true}</p>
            <p class="clear"></p>
            <ul class="games-desc">
			   <li><span>Star Rating :</span> {$CoachArr[coachRow].star}</li>
			 
            </ul>
            <ul class="games-desc">
			   <li><span>Language :</span> {if $CoachArr[coachRow].language neq ''}{$CoachArr[coachRow].language}{else}N/A{/if}</li>
			  
            </ul>
            <div class="clear"></div>
			<a href="javascript:;" style="float:right;" onclick="expand({$CoachArr[coachRow].user_id})">more</a>
            <div class="view">
			a href="coachdetails.php?coach_id={$CoachArr[coachRow].user_id}&game_id={$game_id}">view</a
			<a href="#" onclick="redirect_function({$CoachArr[coachRow].user_id})">view</a>
			</div>-->
             <h2><a href="#" onclick="redirect_function({$CoachArr[coachRow].user_id})" style="text-decoration:none;">{$CoachArr[coachRow].name}</a></h2>
            <!--<p>{$CoachArr[coachRow].about}</p>-->
            <p class="clear"></p>
            <ul class="games-desc">
			   <li><span>Star Rating :</span> {$CoachArr[coachRow].star}</li>
			   <li><span>Availability :</span> {if $CoachArr[coachRow].availability_type neq ''}{$CoachArr[coachRow].availability_type}{else}N/A{/if}</li>
			   {if $CoachArr[coachRow].avail_local eq 'Y'}
			   <li><span>City (local meet-up) :</span> {if $CoachArr[coachRow].availability_city neq ''}{$CoachArr[coachRow].availability_city}{else}N/A{/if}</li>
			   {/if}
			   
			   {if $is_ladder eq 'Y'}
			     <li><span>Ladder :</span> {$CoachArr[coachRow].ladder}</li>
			   {/if}
			   {if $is_server eq 'Y'}
                 <li><span>Server :</span> {$CoachArr[coachRow].server}</li>
			   {/if}
			   {if $is_region eq 'Y'}
                 <li><span>Region :</span> {$CoachArr[coachRow].region}</li>
			   {/if}	 
               
               <!--li><span>Rate :</span> $10</li-->
               
            </ul>
            <ul class="games-desc">
			   <li><span>Language :</span> {if $CoachArr[coachRow].language neq ''}{$CoachArr[coachRow].language}{else}N/A{/if}</li>
			   <li><span>Rate :</span> {if $CoachArr[coachRow].rate neq ''}${$CoachArr[coachRow].rate}{else}N/A{/if}</li>
			   {if $CoachArr[coachRow].avail_local eq 'Y'}
			   <li><span>Country (local meet-up) :</span> {if $CoachArr[coachRow].availability_country neq ''}{$CoachArr[coachRow].availability_country}{else}N/A{/if}</li>
			   {/if}
			   {if $is_race eq 'Y'}
                 <li><span>Race :</span> {$CoachArr[coachRow].race}</li>
				 <li>&nbsp;</li>
			   {/if}
			   {if $is_rating eq 'Y'}
                 <li><span>Rating :</span> {$CoachArr[coachRow].rating}</li>
			   {/if}
            </ul>
            <div class="clear"></div>
			<p style="padding-left:2px;"><b>Experience</b> : {if $CoachArr[coachRow].experience neq ''}{$CoachArr[coachRow].experience}{else}N/A{/if}</p>
            
            <div class="clear"></div>
            
            <!-- Score Area -->
            <br />
            <div id="scoreCard">
                <ul>
				{section name=k loop=$CatIndividualRec[coachRow]}
					{if $CatIndividualRec[coachRow][k].rating_category eq 'Knowledge'}
                    <li id="Knowledge" class="tTip">
					<a href="#" rel="htmltooltip">
					<strong>{$CatIndividualRec[coachRow][k].rating}</strong>
					<span>{$CatIndividualRec[coachRow][k].rating_category}</span>
					</a>
					<div class="htmltooltip">
					<ul>Knowledge is a rating for:
						<li>Strategies</li>
						<li>Game knowledge</li>
						<li>Detailed analysis</li>
					</ul>
					</div>
					</li>
					{/if}
					{if $CatIndividualRec[coachRow][k].rating_category eq 'Structure'}
				   <li id="Structure" class="tTip">
				   <a href="#" rel="htmltooltip">
				   <strong>{$CatIndividualRec[coachRow][k].rating}</strong><span>Structure</span>
				   </a>
				   <div class="htmltooltip">
					<ul>Structure is a rating for:
						<li>Did the lesson have clear goals?</li>
						<li>Were there reasonable expectations?</li>
						<li>Was the lesson smooth?</li>
					</ul>
				   </div>
				   </li>
				   {/if}
				   {if $CatIndividualRec[coachRow][k].rating_category eq 'Communication'}
					<li id="Communication" class="tTip">
					<a href="#" rel="htmltooltip">
					<strong>{$CatIndividualRec[coachRow][k].rating}</strong><span>Communication</span>
					</a>
					<div class="htmltooltip">
					<ul>Communication is a rating for:
						<li>Clear instruction</li>
						<li>Language fluency</li>
						<li>Positive engagement</li>
					</ul>
					</div>
					</li>
					{/if}
					{if $CatIndividualRec[coachRow][k].rating_category eq 'Professionalism'}
					 <li id="Professionalism" class="tTip">
					 <a href="#" rel="htmltooltip">
					 <strong>{$CatIndividualRec[coachRow][k].rating}</strong><span>Professionalism</span>
					 </a>
					 <div class="htmltooltip">
					 <ul>Professionalism is a rating for:
						<li>Reliability</li>
						<li>Punctuality</li>
						<li>Trustworthy</li>
					 </ul>
					 </div>
					 </li>
					 {/if}
				<!--<li title="Knowledge" id="Knowledge" class="tTip"><strong>4.3</strong><span>Knowledge</span></li>
                    <li title="Structure" id="Structure" class="tTip"><strong>4.5</strong><span>Structure</span></li>
                    <li title="Communication" id="Communication" class="tTip"><strong>3.7</strong><span>Communication</span></li>-->
				{/section}
                </ul>
            </div>
            <div class="clear"></div>
            <!-- Score Area END -->
            
			<!--<a href="javascript:;" style="float:right;" onclick="collapse({$CoachArr[coachRow].user_id})">less</a>-->
            <div class="view">
			<!--a href="coachdetails.php?coach_id={$CoachArr[coachRow].user_id}&game_id={$game_id}">view</a-->
			<a href="#" onclick="redirect_function({$CoachArr[coachRow].user_id})">view</a>
			</div>
			</div>
		  <div class="game-details" style="display:none;" id="big_{$CoachArr[coachRow].user_id}">
		  
             <h2><a href="#" onclick="redirect_function({$CoachArr[coachRow].user_id})" style="text-decoration:none;">{$CoachArr[coachRow].name}</a></h2>
            <!--<p>{$CoachArr[coachRow].about}</p>-->
            <p class="clear"></p>
            <ul class="games-desc">
			   <li><span>Star Rating :</span> {$CoachArr[coachRow].star}</li>
			   <li><span>Availability :</span> {if $CoachArr[coachRow].availability_type neq ''}{$CoachArr[coachRow].availability_type}{else}N/A{/if}</li>
			   {if $CoachArr[coachRow].avail_local eq 'Y'}
			   <li><span>City (local meet-up) :</span> {if $CoachArr[coachRow].availability_city neq ''}{$CoachArr[coachRow].availability_city}{else}N/A{/if}</li>
			   {/if}
			   
			   {if $is_ladder eq 'Y'}
			     <li><span>Ladder :</span> {$CoachArr[coachRow].ladder}</li>
			   {/if}
			   {if $is_server eq 'Y'}
                 <li><span>Server :</span> {$CoachArr[coachRow].server}</li>
			   {/if}
			   {if $is_region eq 'Y'}
                 <li><span>Region :</span> {$CoachArr[coachRow].region}</li>
			   {/if}	 
               
               <!--li><span>Rate :</span> $10</li-->
               
            </ul>
            <ul class="games-desc">
			   <li><span>Language :</span> {if $CoachArr[coachRow].language neq ''}{$CoachArr[coachRow].language}{else}N/A{/if}</li>
			   <li><span>Rate :</span> {if $CoachArr[coachRow].rate neq ''}${$CoachArr[coachRow].rate}{else}N/A{/if}</li>
			   {if $CoachArr[coachRow].avail_local eq 'Y'}
			   <li><span>Country (local meet-up) :</span> {if $CoachArr[coachRow].availability_country neq ''}{$CoachArr[coachRow].availability_country}{else}N/A{/if}</li>
			   {/if}
			   {if $is_race eq 'Y'}
                 <li><span>Race :</span> {$CoachArr[coachRow].race}</li>
				 <li>&nbsp;</li>
			   {/if}
			   {if $is_rating eq 'Y'}
                 <li><span>Rating :</span> {$CoachArr[coachRow].rating}</li>
			   {/if}
            </ul>
            <div class="clear"></div>
			<p style="padding-left:2px;"><b>Experience</b> : {if $CoachArr[coachRow].experience neq ''}{$CoachArr[coachRow].experience}{else}N/A{/if}</p>
            
            <div class="clear"></div>
            
            <!-- Score Area -->
            <br />
            <div id="scoreCard">
                <ul>
                    <li title="Overall" id="Overall" class="tTip"><strong>4.4</strong><span>Overall</span></li>
                    <li title="Knowledge" id="Knowledge" class="tTip"><strong>4.3</strong><span>Knowledge</span></li>
                    <li title="Structure" id="Structure" class="tTip"><strong>4.5</strong><span>Structure</span></li>
                    <li title="Communication" id="Communication" class="tTip"><strong>3.7</strong><span>Communication</span></li>
                </ul>
            </div>
            <div class="clear"></div>
            <!-- Score Area END -->
            
			<a href="javascript:;" style="float:right;" onclick="collapse({$CoachArr[coachRow].user_id})">less</a>
            <div class="view">
			<!--a href="coachdetails.php?coach_id={$CoachArr[coachRow].user_id}&game_id={$game_id}">view</a-->
			<a href="#" onclick="redirect_function({$CoachArr[coachRow].user_id})">view</a>
            
            
			</div>
			</div>
			<div class="clear"></div>
			</form>
         <div class="clear"></div>
      </div>
	 {/section} 
     {if $pagination_arr[1]}
	 <div class="clear"></div>
      <div class="pagin">
         <ul>
           {$pagination_arr[1]}
         </ul>
      </div>
	 {/if}
	 {else}
	    No Coach is found......
     {/if}
	</div> 
   </div>
