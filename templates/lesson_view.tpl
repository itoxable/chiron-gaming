
 {literal}
 <script language="javascript" src="js/contact_us.js"></script> 
<script language="javascript" type="text/javascript">

function getChangeGame(id)
{
	jQuery.post('game_details.php',{'game_id':id, 'action':'get_content'}, function(data){
		jQuery('#dynamic_controls').html(data);
	});
}
function CheckFields()
{
  if(document.getElementById('student_id').value=='' ) 
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please select a student");
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
    return false;
   }
   if(document.getElementById('lesson_price').value=='' ) 
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please enter price");
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
    return false;
   }
   if(document.getElementById('lesson_title').value=='' ) 
   {
    jQuery('#FormErrorMsg').show();
    jQuery('#FormErrorMsg').html("Please enter title");
	//jQuery('#FormFieldCheckErrorMsg').fadeOut(2000);
    return false;
   }
  return true;
 }  
 function keyCheck(eventObj, obj)
{
    var keyCode
 
    // Check For Browser Type
    if (document.all){
        keyCode=eventObj.keyCode
    }
    else{
        keyCode=eventObj.which
    }
 
    var str=obj.value
 
    if(keyCode==46){
        if (str.indexOf(".")>0){
            return false
        }
    }
 
    if((keyCode<46 || keyCode >57)   &&   (keyCode >31)){ // Allow only integers and decimal points
        return false
    }
    
    return true
}
</script>
{/literal}

<div class="content">
   		<!-- tab -->
    	<ul class="tabflex">
        	<li><a href="dashboard.php"><span>Dashboard</span></a></li>
            <li><a href="inbox.php"><span>Message</span></a></li>
			<li><a href="profile.php"><span>Profile</span></a></li>
			{if $is_coach eq '1'}<li><a href="coach_game.php"><span>Coach</span></a></li>{/if}
			{if $is_partner eq '1'}<li><a href="training_partner_game.php"><span>Training Partner</span></a></li>{/if}
			{if $is_coach eq '1'}<li><a href="add_lesson.php"  class="active"><span>Lessons</span></a></li>{/if}
			{if $is_coach eq '0'}<li><a href="my_lesson.php"><span>Lessons</span></a></li>{/if}
			<li><a href="upload_videos.php"><span>Videos</span></a></li>
			{if $is_coach eq '1' or $is_partner eq '1'}<li><a href="my_review.php" ><span>Reviews</span></a></li>{/if}
			{if $is_coach eq '0' && $is_partner eq '0'}<li><a href="my_video_review.php"><span>Reviews</span></a></li>{/if}
			
          <!--    <li><a href="#"><span>Students</span></a></li>-->
        </ul>
        <!-- tab -->
        <div class="clear"></div>
        <div class="tabular-content"> 
		 <div class="total-message">&nbsp;&nbsp;</div>
		  <div class="tab_sub">
            	<ul>
                	<li><a href="add_lesson.php" >Add Lesson</a></li>
                    <li><a href="my_lesson.php" class="active">My Lesson</a></li>
                </ul>
            </div>   
		 <div class="clear"></div>                     
         <div>

      <div class="register">
	   <div id='FormErrorMsg' style="color:red; text-align:center; padding-bottom:10px;">{if $ermsg neq ''} {$ermsg}{/if} </div>
      <form name="lesson" id="lesson" action="" method="post">
	  <input type="hidden" name="lesson_id" value="{$Formval.lesson_id}" />
         <fieldset>
            <label>Student :</label>
			<label style="padding-left:10px; text-align:left;">{$Formval.student_name}</label>
			<p class="clear"></p>
			<label>Price ($):</label>
			<label style="padding-left:10px; text-align:left;">{$Formval.lesson_price}</label>
			<p class="clear"></p>
			</span>
            <p class="clear"></p>
			<label>Title:</label>
            <label style="padding-left:10px; text-align:left;">{$Formval.lesson_title}</label>
			<p class="clear"></p>
            <label>Description :</label>
            <label style="padding-left:10px; text-align:left; width:400px;">{$Formval.lesson_description}</label>
            <p class="clear"></p>
			{if $Formval.lesson_status_id eq '2'}
			<label>Payment Gateway:</label>
            <label style="padding-left:10px; text-align:left;">PayPal</label>
			<p class="clear"></p>
			<label>Payment Date:</label>
            <label style="padding-left:10px; text-align:left;">{$Formval.lesson_payment_date}</label>
			<p class="clear"></p>
			<label>Transaction No.:</label>
            <label style="padding-left:10px; text-align:left;">{$Formval.lesson_payment_transaction_no}</label>
			<p class="clear"></p>
			{/if}
            <label>&nbsp;</label>
            <input name="" class="cancel" value="Back" type="button" onclick="window.location.href='add_lesson.php'" />
         </fieldset>
      </form>
      </div>
      
     </div>
     </div>
    </div>