{literal}
<link type="text/css" rel="stylesheet" href="style/lightbox-form.css">
<script src="js/lightbox-form.js" type="text/javascript"></script>
<script language="javascript" src="js/contact_us.js"></script>
<script language="javascript">
function submit_message_text()
{	
	var message_text=jQuery('#message_text').val();
	var user_id_to=jQuery('#user_id_to').val();
	var user_id_from=jQuery('#user_id_from').val();
	var reply_of_msg=jQuery('#reply_of_msg').val();
	
	if(message_text == ""){
		jQuery('#message_text').css('border','1px solid #FF0000');
		return false;
	}	
	else{
		jQuery('#message_text').css('border','1px solid #000');
	
		jQuery.ajax({
			   url:'reply.php',
			   data:'flag=1&message_text='+message_text+'&reply_of_msg='+reply_of_msg+'&user_id_to='+user_id_to+'&user_id_from='+user_id_from,
			   type:'post',		  
			   success:function(resp){
					jQuery('#msgbox').fadeIn(0);
					jQuery('#msgbox').html('Messege sent successfully.An e-mail notification has been successfully sent with this message!');
					jQuery('#msgbox').fadeOut(5000);
			   } 		   
		});
		
		document.getElementById("message_text").value='';
		closereplybox();
		//ti = setInterval('removeDiv()', 5000);
	}
	
}
/*function removeDiv()
{
clearInterval(ti);
document.getElementById('msgbox').style.display = 'none';
}	
*/
function closereply()
{
	document.getElementById("message_text").value='';
	jQuery('#message_text').css('border','1px solid #ccc');
	closereplybox();
}
</script>
{/literal}

<div class="right-panel">
      <div class="findstudent-title" style="margin-bottom: 5px;">
        <div class="title" style="float: left;padding: 0;border: none;"><a style="text-decoration: none;" href="findstudent.php">Find Student</a></div>
        <div class="breadcrumb">{$StudentArr.username} </div>
        <div class="clear"></div>
      </div>
	  <div class="clear"></div>    
      <div class="game-list-block">
         <!--h1>{$StudentArr.username}</h1-->
		    <div class="game-image-block">
			{if $StudentArr.photo neq ''}
                        <img src="uploaded/user_images/thumbs/big_{$StudentArr.photo}" alt="{$StudentArr.username}" border="0"/>
			{else}
			 <img src="images/student_thumb.jpg" border="0" alt="{$StudentArr.username}" />
			{/if} 
                    </div> 
		    
         <div class="game-details-block">
                     <div class="score_title coachname" >
				<!--<h4 class="gameshead">{$StudentArr.username}</h4>-->
				<h2>
					<a class="{$online_status}" title="{$online_status}"></a><a href="javascript:;" class="" onclick="redirect_function({$StudentArr.user_id})" style="text-decoration:none;">{$StudentArr.username}</a> 
					<a id="{$StudentArr.user_id}_friendStar" class="{if $isfriend eq 'y'}star_friend{else}star_not_friend{/if}" title="{if $isfriend eq 'y'}in your friend list{else}not your friend list{/if}"></a>
				</h2>
				{if $smarty.session.user_id neq '' && $smarty.session.user_id neq $StudentArr.user_id}
				<div class="addAsFriendWindow">
					<a class="addAsFriendLink" href="javascript:;" onclick="openChatTab(event,'{$StudentArr.user_id}','{$StudentArr.username}')">Message</a><br/>
					{if $isfriend eq 'n'}
					<a class="addAsFriendLink addFriend" href="javascript:;" onclick="addAsFriend(event,'{$StudentArr.user_id}','{$StudentArr.username}',true)">Add as friend</a>
					{else}
					<a class="addAsFriendLink removeFriend" href="javascript:;" onclick="removeFriend(event,'{$StudentArr.user_id}','{$StudentArr.username}',true)">Remove friend</a>
					{/if}
				</div>
				{/if}
				
			</div>
                                
                      <div class="clear"></div>          
                    <div>
			<ul class="games-desc">
			   <li><span>Language : </span>{if $StudentArr.language neq ''}{$StudentArr.language}{else}N/A{/if}</li>
			  <li><span>Budget : </span>{if $StudentArr.rate neq ''}${$StudentArr.rate}{else}N/A {/if}</li>
			  {if $StudentArr.avail_local eq 'Y'}
			  <li><span>City (Local meet-up) : </span>{if $StudentArr.availability_city neq ''}{$StudentArr.availability_city}{else}N/A{/if}</li>
			   {/if}
			</ul> 
			<ul class="games-desc">
			   <li><span>Country : </span>{$StudentArr.country}</li>
			   <li><span>Availability : </span>{$StudentArr.availability_type}</li>
			   {if $StudentArr.avail_local eq 'Y'}
			    <li><span>Country (Local meet-up) : </span>{if $StudentArr.availability_country neq ''}{$StudentArr.availability_country}{else}N/A{/if}</li>
			   {/if}
			</ul>   
			<p class="clear" style="padding-bottom:5px;"></p><br  />
			
			{section name=game loop=$StudentGameArr}
			 {assign var="i" value=$smarty.section.game.index+1}
				<p style="padding-left:2px;"><b>{$StudentGameArr[game].game}</b></p>
				<p style="padding-left:2px;">
				   <b>Experience : </b>{if $StudentGameArr[game].experience neq ''}{$StudentGameArr[game].experience}{else}N/A{/if}</li>
				</p><br />
			{/section}
          </div>
            <div class="clear"></div>
			{if $smarty.session.user_id neq '' && $smarty.session.user_id neq $StudentArr.user_id}
              <div class="button-flex button70" style="margin-right:{if $smarty.session.user_type eq '2'}{if $is_fav eq '0'}10{else}10{/if}{else}10{/if}px;">
				<a href="javascript:void(0);" class="button" style="float: right" onClick="openChatTab(event,'{$StudentArr.user_id}','{$StudentArr.username}')">Message</a>
			</div>
			  <br /> 
			  <div id="msgbox" style="color:#0077BC;"></div>
		    {/if}
         </div>
         <div class="clear"></div>
      </div>
	  
      <div class="clear"></div>
  </div> 
<div id="shadowreply"></div>
<div id="replybox">
  <h2><span id="replytitle"></span></h2>
  
   <div class="review">   
   <form name="contact" id="Contact"  method='post' action="" onsubmit="return false;">
<!--   <input type="hidden" name="coach_id" id="coach_id" value="{$coach_id}" />
   <fieldset>   
    <label>Review :</label>
            <textarea name="review_comment" id="review_comment" cols="" rows="" >{$Formval.user_about}</textarea>
   
    <p class="clear"></p>
    <label>&nbsp;</label>
            <input name="submit" class="submit" value="Submit" type="submit" onclick='return CheckFields()' />
            <input name="" class="cancel" value="Cancel" type="button" onClick="closereplybox()" />
			
    </p>
   </fieldset>	-->
   <fieldset> 
   <label>Message :</label>
      <textarea rows="10" cols="40"  name="message_text" id="message_text" class=""></textarea>	  
    <input name="flag" type="hidden" value="1" />
	<input name="user_id_to" id="user_id_to" type="hidden" value="{$StudentArr.user_id}" />
    <input name="user_id_from" id="user_id_from" type="hidden" value="{$smarty.session.user_id}" />
    <input name="reply_of_msg" id="reply_of_msg" type="hidden" value="0" /> 
	<p class="clear"></p>
	 <label>&nbsp;</label>            
			<input name="snd_message_text" type="submit" class="submit" value="Submit" onclick="return submit_message_text()"/> 
            <input name="" class="cancel" value="Cancel" type="button" onClick="closereply()" />
			
   </fieldset> 
  </form> 
 </div> 
</div>

