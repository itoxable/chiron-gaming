{literal}
<script type="text/javascript" language="javascript">

jQuery(document).ready(function(){
jQuery('#vnm').focus(function(){ if( jQuery(this).val()== 'Keyword' ) jQuery(this).val('');});
jQuery('#vnm').blur(function(){ if( jQuery(this).val()== '' ) jQuery(this).val('Keyword');});


/* right panel filter more/less links */
	var filterThreshold = 5;
	jQuery('.listing').each(function(i){
	if(jQuery(this).find('a.selected').length == 0){
		jQuery(this).find('li:gt('+(filterThreshold-1)+')').hide();
		if(jQuery(this).find('li').length > filterThreshold)
			jQuery(this).after('<li class="more"><div class="button" style="float:right;margin: 5px;">+ more</div></li><li style="clear:both; width:100%"></li>');
		else
			jQuery(this).after('');
	}
	});

	jQuery('.more').each(function(i){
		jQuery(this).toggle(function(){
		jQuery(this).prev().find('li:gt('+(filterThreshold-1)+')').slideDown('fast');
		jQuery(this).html('<li class="more"><div class="button" style="float:right;margin: 5px;">- less</div></li><li style="clear:both; width:100%"></li>');
		},function(){
		jQuery(this).prev().find('li:gt('+(filterThreshold-1)+')').slideUp('fast');
		jQuery(this).html('<li class="more"><div class="button" style="float:right;margin: 5px;">+ more</div></li><li style="clear:both; width:100%"></li>');
		});
	});
	jQuery('.refine').click(function(){
	
		var val = jQuery(this).attr('typeId');
		var type = jQuery(this).attr('stype');
		
		if(type =='L')
		{
			
			if(jQuery('#ladder_'+val).attr('checked'))
			jQuery('#ladder_'+val).attr('checked','');
			else
			jQuery('#ladder_'+val).attr('checked','true');
		}
		else if(type =='CH')
		{
			if(jQuery('#champion_'+val).attr('checked'))
			jQuery('#champion_'+val).attr('checked','');
			else
			jQuery('#champion_'+val).attr('checked','true');
		}	
		else if(type =='CL')
		{
			if(jQuery('#class_'+val).attr('checked'))
			jQuery('#class_'+val).attr('checked','');
			else
			jQuery('#class_'+val).attr('checked','true');
		}	
		else if(type =='MP')
		{
			if(jQuery('#map_'+val).attr('checked'))
			jQuery('#map_'+val).attr('checked','');
			else
			jQuery('#map_'+val).attr('checked','true');
		}	
		else if(type =='MD')
		{
			if(jQuery('#mode_'+val).attr('checked'))
			jQuery('#mode_'+val).attr('checked','');
			else
			jQuery('#mode_'+val).attr('checked','true');
		}	
		else if(type =='TM')
		{
			if(jQuery('#team_'+val).attr('checked'))
			jQuery('#team_'+val).attr('checked','');
			else
			jQuery('#team_'+val).attr('checked','true');
		}	
		else if(type =='TP')
		{
			if(jQuery('#type_'+val).attr('checked'))
			jQuery('#type_'+val).attr('checked','');
			else
			jQuery('#type_'+val).attr('checked','true');
		}	
		else if(type =='V')
		{
			if(jQuery('#versus_'+val).attr('checked'))
			jQuery('#versus_'+val).attr('checked','');
			else
			jQuery('#versus_'+val).attr('checked','true');
		}	
		else if(type =='R')
		{
			if(jQuery('#race_'+val).attr('checked'))
			jQuery('#race_'+val).attr('checked','');
			else
			jQuery('#race_'+val).attr('checked','true');
		}	
		else if(type =='S')
		{
			if(jQuery('#server_'+val).attr('checked'))
			jQuery('#server_'+val).attr('checked','');
			else
			jQuery('#server_'+val).attr('checked','true');
		}
		else if(type =='X')
		{
			if(jQuery('#region_'+val).attr('checked'))
			jQuery('#region_'+val).attr('checked','');
			else
			jQuery('#region_'+val).attr('checked','true');
		}
		else if(type =='Y')
		{
			if(jQuery('#rating_'+val).attr('checked'))
			jQuery('#rating_'+val).attr('checked','');
			else
			jQuery('#rating_'+val).attr('checked','true');
		}		
		else if(type =='A')
		{
			if(jQuery('#avail_'+val).attr('checked'))
			jQuery('#avail_'+val).attr('checked','');
			else
			jQuery('#avail_'+val).attr('checked','true');
		}
		else
		{
			if(jQuery('#language_'+val).attr('checked'))
			jQuery('#language_'+val).attr('checked','');
			else
			jQuery('#language_'+val).attr('checked','true');
		}
		
		//document.frmRefine.submit();
		frmRefine_POST();
	});
  });	
</script>
<link rel="stylesheet" href="slider/stylesheets/jslider.css" type="text/css">
<link rel="stylesheet" href="slider/stylesheets/jslider.plastic.css" type="text/css">
<script type="text/javascript" src="slider/javascripts/jquery.dependClass.js"></script>
<script type="text/javascript" src="slider/javascripts/jquery.slider-min.js"></script>
<script type='text/javascript'>
function frmRefine_POST()
{
  var data = jQuery(document.frmRefine).serialize();
  jQuery('#loading').html('<p><img src="images/bar-loader.gif" alt="Loading..." /></p>');
  //alert(data);
  jQuery.ajax({
	type: "GET",
	url: 'replays2.php',
	data: data,			
	dataType: 'html',
	success: function(data)
	{				
	   
	   jQuery('.container-inner').html(data + "<div class='clear'></div>");
	   //alert(data);
	   jQuery('#loading').html();
	}
});
}
 
function subForm(a){
//alert(a);
	document.getElementById('listfor').value='sorting';
	document.getElementById('sortingby').value=a;
	frmRefine_POST();
}
function searchVdo()
{
   var vnm = jQuery('#vnm').val();
   if(vnm == '' || vnm =='Keyword')
    jQuery('#vnm').css('border','1px solid red');
   else
   {
     document.getElementById('vtitle').value=vnm;
	 frmRefine_POST();
   }	 	
}
function resetSearch()
{
     document.getElementById('vtitle').value='';
	 frmRefine_POST();
   	 	
}

</script>
{/literal}
{if $FromPage eq 'videodetails'}
<div class="container-inner">
{/if}
<div class="left-panel">
<div class="left-search">
       <form action="replays.php" method="post" name="frmRefine" id="leftsearchfindvideo">
       <fieldset>
       <select name="game_id" id="game_id">
	  {html_options options=$GameArr selected=$game_id}
	</select>

        <a class="button" href="#" style="float: right;min-width: 30px;padding: 0 3px; margin: 0;" onclick="document.getElementById('leftsearchfindvideo').submit();">Go</a>
       <div style="background:#ccc; width:250px; float:left; display:block; top:40px; left:0px; height:37px; border:1px solid #ededed; padding:3px 0 0 5px; position:absolute;">
           <input name="vnm" class="text" type="text" id="vnm" {if $vdotitle neq ''}value="{$vdotitle}"{else}value="Keyword"{/if} />
           <input type="button" value="" onclick="searchVdo()" />
        </div>
       </fieldset>
       </form>
 </div>
   <div class="graphite demo-container">
	<ul class="accordion" id="accordion-1">
	<form action="replays.php" method="post" name="frmRefine">
	<input type="hidden" name="vtitle" id="vtitle" value="{$vdotitle}"/>

	<input type="hidden" name="list_for" id="listfor" value="" />
	   <input type="hidden" name="sorting_by" id="sortingby" value=""/>
	<input type="hidden" name="action" value="send" />
	{if $is_ladder eq 'Y'}
	 {if $Numladder neq '0'}
	    <li><a href="#">Ladder</a>
		<ul>	
		 <div class="listing">
		 {section name=ladder loop=$LadderArr}
			<li>
			<input type="checkbox" name="ladder_id[]" id='ladder_{$LadderArr[ladder].ladder_id}' value="{$LadderArr[ladder].ladder_id}" style="display:none;"
			{if $ladder_id|@sizeof neq 0 and $LadderArr[ladder].ladder_id|in_array:$ladder_id} checked="checked" {/if}/>
			<a href="#" class="refine {if $ladder_id|@sizeof neq 0 and $LadderArr[ladder].ladder_id|in_array:$ladder_id} selected {/if}" stype="L" typeId="{$LadderArr[ladder].ladder_id}"> 
			{$LadderArr[ladder].ladder_name} ({$LadderArr[ladder].countrow}) </a>
			</li>
                 {/section}
				 
		 </div>		 
		 </ul>
	   </li>	 
	 {/if}	
	{/if} 
	{if $is_race eq 'Y'}
	 {if $Numrace neq '0'}
	    <li><a href="#">Race</a>
		<ul>	
		 <div class="listing">
			 {section name=race loop=$RaceArr}
				   	<li>
					<input type="checkbox" name="race_id[]" id='race_{$RaceArr[race].race_id}' value="{$RaceArr[race].race_id}" style="display:none" 
					{if $race_id|@sizeof neq '' and  $RaceArr[race].race_id|in_array:$race_id} checked="checked" {/if}/>
					<a href="#" class="refine {if $race_id|@sizeof neq '' and $RaceArr[race].race_id|in_array:$race_id} selected {/if}" stype="R" typeId="{$RaceArr[race].race_id}"> 
					{$RaceArr[race].race_title} ({$RaceArr[race].countrow}) </a> </li>
			{/section}
				 
		 </div>		 
	      </ul>
	   </li>	 
	 {/if}	
	{/if}
	<!-- Chandan Added this Starts-->
	{if $is_champion eq 'Y'}
	 {if $NumChampion neq '0'}
	    <li><a href="#">Champion</a>
		<ul>	
		 <div class="listing">
				 {section name=champion loop=$ChampionArr}
				   	<li>
					<input type="checkbox" name="champion_id[]" id='champion_{$ChampionArr[champion].champion_id}' value="{$ChampionArr[champion].champion_id}" style="display:none" 
					{if $champion_id|@sizeof neq '' and $ChampionArr[champion].champion_id|in_array:$champion_id} checked="checked" {/if}/>
					<a href="#" class="refine {if $champion_id|@sizeof neq '' and $ChampionArr[champion].champion_id|in_array:$champion_id} selected {/if}" stype="CH" typeId="{$ChampionArr[champion].champion_id}"> 
					{$ChampionArr[champion].champion_title} ({$ChampionArr[champion].countrow}) </a> </li>
                 {/section}
				 
		 </div>		 
	      </ul>
	   </li>	 
	 {/if}	
	{/if}
        <!-- Chandan Added This Ends-->
	
	<!-- Chandan Added this Starts-->
	{if $is_class eq 'Y'}
	 {if $NumClass neq '0'}
	    <li><a href="#">Class</a>
		<ul>	
		 <div class="listing">
				 {section name=class loop=$ClassArr}
				   	<li>
					<input type="checkbox" name="class_id[]" id='class_{$ClassArr[class].class_id}' value="{$ClassArr[class].class_id}" style="display:none" 
					{if $class_id|@sizeof neq '' and $ClassArr[class].class_id|in_array:$class_id} checked="checked" {/if}/>
					<a href="#" class="refine {if $class_id|@sizeof neq '' and $ClassArr[class].class_id|in_array:$class_id} selected {/if}" stype="CL" typeId="{$ClassArr[class].class_id}"> 
					{$ClassArr[class].class_title} ({$ClassArr[class].countrow}) </a> </li>
                 {/section}
				 
		 </div>		 
	      </ul>
	   </li>	 
	 {/if}	
	{/if}
        <!-- Chandan Added This Ends-->
	<!-- Chandan Added this Starts-->
	{if $is_map eq 'Y'}
	 {if $NumMap neq '0'}
	    <li><a href="#">Map</a>
		<ul>	
		 <div class="listing">
				 {section name=map loop=$MapArr}
				   	<li>
					<input type="checkbox" name="map_id[]" id='map_{$MapArr[map].map_id}' value="{$MapArr[map].map_id}" style="display:none" 
					{if $map_id|@sizeof neq '' and $MapArr[map].map_id|in_array:$map_id} checked="checked" {/if}/>
					<a href="#" class="refine {if $map_id|@sizeof neq '' and $MapArr[map].map_id|in_array:$map_id} selected {/if}" stype="MP" typeId="{$MapArr[map].map_id}"> 
					{$MapArr[map].map_title} ({$MapArr[map].countrow}) </a> </li>
                 {/section}
				 
		 </div>		 
	      </ul>
	   </li>	 
	 {/if}	
	{/if}
        <!-- Chandan Added This Ends-->
	<!-- Chandan Added this Starts-->
	{if $is_mode eq 'Y'}
	 {if $NumMode neq '0'}
	    <li><a href="#">Mode</a>
		<ul>	
		 <div class="listing">
				 {section name=mode loop=$ModeArr}
				   	<li>
					<input type="checkbox" name="mode_id[]" id='mode_{$ModeArr[mode].mode_id}' value="{$ModeArr[mode].mode_id}" style="display:none" 
					{if $mode_id|@sizeof neq '' and $ModeArr[mode].mode_id|in_array:$mode_id} checked="checked" {/if}/>
					<a href="#" class="refine {if $mode_id|@sizeof neq '' and $ModeArr[mode].mode_id|in_array:$mode_id} selected {/if}" stype="MD" typeId="{$ModeArr[mode].mode_id}"> 
					{$ModeArr[mode].mode_title} ({$ModeArr[mode].countrow}) </a> </li>
				{/section}
				 
		 </div>		 
	      </ul>
	   </li>	 
	 {/if}	
	{/if}
        <!-- Chandan Added This Ends-->
	<!-- Chandan Added this Starts-->
	{if $is_team eq 'Y'}
	 {if $NumTeam neq '0'}
	    <li><a href="#">Team</a>
		<ul>	
		 <div class="listing">
				 {section name=team loop=$TeamArr}
				   	<li>
					<input type="checkbox" name="team_id[]" id='team_{$TeamArr[team].team_id}' value="{$TeamArr[team].team_id}" style="display:none" 
					{if $team_id|@sizeof neq '' and $TeamArr[team].team_id|in_array:$team_id} checked="checked" {/if}/>
					<a href="#" class="refine {if $team_id|@sizeof neq '' and $TeamArr[team].team_id|in_array:$team_id} selected {/if}" stype="TM" typeId="{$TeamArr[team].team_id}"> 
					{$TeamArr[team].team_title} ({$TeamArr[team].countrow}) </a> </li>
				 {/section}
				 
		 </div>		 
	      </ul>
	   </li>	 
	 {/if}	
	{/if}
        <!-- Chandan Added This Ends-->
	<!-- Chandan Added this Starts-->
	{if $is_type eq 'Y'}
	 {if $NumType neq '0'}
	    <li><a href="#">Type</a>
		<ul>	
		 <div class="listing">
				 {section name=type loop=$TypeArr}
				   	<li>
					<input type="checkbox" name="type_id[]" id='type_{$TypeArr[type].type_id}' value="{$TypeArr[type].type_id}" style="display:none" 
					{if $type_id|@sizeof neq '' and $TypeArr[type].type_id|in_array:$type_id} checked="checked" {/if}/>
					<a href="#" class="refine {if $type_id|@sizeof neq '' and $TypeArr[type].type_id|in_array:$type_id} selected {/if}" stype="TP" 
					typeId="{$TypeArr[type].type_id}" title="{$TypeArr[type].type_full_title}"> 
					{$TypeArr[type].type_title} ({$TypeArr[type].countrow}) </a> </li>
				 {/section}
				 
		 </div>		 
	      </ul>
	   </li>	 
	 {/if}	
	{/if}
        <!-- Chandan Added This Ends-->
	<!-- Chandan Added this Starts-->
	{if $is_versus eq 'Y'}
	 {if $NumVersus neq '0'}
	    <li><a href="#">Versus</a>
		<ul>	
		 <div class="listing">
				 {section name=versus loop=$VersusArr}
				   	<li>
					<input type="checkbox" name="versus_id[]" id='versus_{$VersusArr[versus].versus_id}' value="{$VersusArr[versus].versus_id}" style="display:none" 
					{if $versus_id|@sizeof neq '' and $VersusArr[versus].versus_id|in_array:$versus_id} checked="checked" {/if}/>
					<a href="#" class="refine {if $versus_id|@sizeof neq '' and $VersusArr[versus].versus_id|in_array:$versus_id} selected {/if}" stype="V" typeId="{$VersusArr[versus].versus_id}"> 
					{$VersusArr[versus].versus_title} ({$VersusArr[versus].countrow}) </a> </li>
				 {/section}
				 
		 </div>		 
	      </ul>
	   </li>	 
	 {/if}	
	{/if}
	<li><a href="#">Duration (min)</a> 
           <ul>	
				  <li style="height:30px; padding:15px 5px 5px 5px;"> 
				   <span style="display: inline-block; width: 230px; padding: 0 5px;">
				   <input id="Slider2" type="slider" name="duration" value="{$duration}"/></span> 
                    {literal}
					 <script type="text/javascript" charset="utf-8">
					  jQuery("#Slider2").slider({ from: 0, to: 60, limits: false, step: 1,smooth: true, round: 0, dimension: '', skin: "plastic", callback: 
					  function( value ){ /* document.frmRefine.submit() */ frmRefine_POST(); } });
					</script>
					{/literal}
				 </li>	
		   </ul>
	</li>
        <!-- Chandan Added This Ends-->

	<!-- {if $is_server eq 'Y'}
	 {if $Numserver neq '0'}
	    <li><a href="#">Server</a>
        <ul>	
		 <div class="listing">
				 {section name=server loop=$serverArr}
				   	<li>
					<input type="checkbox" name="server_id[]" id='server_{$serverArr[server].server_id}' value="{$serverArr[server].server_id}" style="display:none" 
					{if $serverArr[server].server_id|in_array:$server_id} checked="checked" {/if}/>
					<a href="#" class="refine {if $serverArr[server].server_id|in_array:$server_id} selected {/if}" stype="S" typeId="{$serverArr[server].server_id}"> 
					{$serverArr[server].server_name} ({$serverArr[server].countrow}) </a> </li>
                 {/section}
				 
		 </div>		 
		 </ul>
	   </li>	 
	 {/if}	
	{/if} 
	{if $is_region eq 'Y'}
	 {if $Numregion neq '0'}
	    <li><a href="#">Region</a>
        <ul>	
		 <div class="listing">
				 {section name=region loop=$regionArr}
				   	<li>
					<input type="checkbox" name="region_id[]" id='region_{$regionArr[region].region_id}' value="{$regionArr[region].region_id}" style="display:none;"
					{if $regionArr[region].region_id|in_array:$region_id} checked="checked" {/if}/>
					<a href="#" class="refine {if $regionArr[region].region_id|in_array:$region_id} selected {/if}" stype="X" typeId="{$regionArr[region].region_id}"> 
					{$regionArr[region].region_title} ({$regionArr[region].countrow}) </a> </li>
                 {/section}
				 
		 </div>		 
		 </ul>
	   </li>	 
	 {/if}	
	{/if} 
	{if $is_rating eq 'Y'}
	 {if $Numrating neq '0'}
	    <li><a href="#">Rating</a>
        <ul>	
		 <div class="listing">
				 {section name=rating loop=$ratingArr}
				   	<li>
					<input type="checkbox" name="rating_id[]" id='rating_{$ratingArr[rating].rating_id}' value="{$ratingArr[rating].rating_id}" style="display:none" 
					{if $ratingArr[rating].rating_id|in_array:$rating_id} checked="checked" {/if}/>
					<a href="#" class="refine {if $ratingArr[rating].rating_id|in_array:$rating_id} selected {/if}" stype="Y" typeId="{$ratingArr[rating].rating_id}"> 
					{$ratingArr[rating].rating_title} ({$ratingArr[rating].countrow}) </a> </li>
                 {/section}
				 
		 </div>		 
		 </ul>
	   </li>	 
	 {/if}	
	{/if} 
	<li><a href="#">Rate ($)</a> 
           <ul>	
		  <li style="height:30px; padding:15px 5px 5px 5px;"> 
		   <span style="display: inline-block; width: 230px; padding: 0 5px;">
		   <input id="Slider1" type="slider" name="price" value="{$price}"/></span> 
			{literal}
			 <script type="text/javascript" charset="utf-8">
			  jQuery("#Slider1").slider({ from: 0, to: 100, limits: false, step: 1, dimension: '', skin: "plastic", callback: 
			  function( value ){ /* document.frmRefine.submit() */ frmRefine_POST(); } });
			</script>
			{/literal}
		 </li>	
	  </ul>
	</li>  
	{if $Numlanguage neq '0'}	
	     <li><a href="#">Language</a>
            <ul>
			 <div class="listing">
                   
				  {section name=lang loop=$languageArr}
				  	<li>
					<input type="checkbox" name="language_id[]" id="language_{$languageArr[lang].language_id}" value="{$languageArr[lang].language_id}" style="display:none;" 
					{if $languageArr[lang].language_id|in_array:$language_id} checked="checked" {/if}/>				
					<a class="refine {if $languageArr[lang].language_id|in_array:$language_id} selected {/if}" stype="Z" typeId="{$languageArr[lang].language_id}" >
					{$languageArr[lang].language_name} ({$languageArr[lang].cnt})</a></a></li>
                  {/section}
			  </div> 
			</ul>
		</li>	
	  {/if}	
	<li><a href="#">Availability</a>
           <ul>
		   <div class="listing">
			 
			  <li> <input type="checkbox" name="availability[]" id="avail_O" value="O" style="display:none" {if 'O'|in_array:$availability} checked="checked" {/if} />
			  <a href="#" class="refine {if 'O'|in_array:$availability} selected {/if}" stype="A" typeId="O"> Online({$avail1Num})</a></li>
			  <li> <input type="checkbox" name="availability[]" id="avail_L" value="L" style="display:none" {if 'L'|in_array:$availability} checked="checked" {/if} />
			  <a href="#" class="refine {if 'L'|in_array:$availability} selected {/if}" stype="A" typeId="L" > Local meet-up({$avail2Num})</a></li>
		   </div>  
	   </ul>
	</li> -->   
	   
	  <input type="hidden" name="record_per_page" id="record_per_page" value="{$record_per_page}" style="visibility:hidden">

	  <input type="submit" style="visibility:hidden">
	 </form> 	
    </ul>
   </div>   
   </div>