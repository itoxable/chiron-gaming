<div id="paging"> 
{literal}
<!--<script type="text/javascript">
$(document).ready(function($){
	$('#accordion').dcAccordion({
		showCount: true,
		saveState: true
	});
	jQuery('#accordion-1').dcAccordion({
		eventType: 'click',
		autoClose: true,
		saveState: true,
		disableLink: true,
		speed: 'slow'
	});
					
});
</script>
-->
<script type="text/javascript">
function SortingList(sorting_url,sorting_by)
{
var params = {
	'list_for':'sorting',
	'sorting_by': sorting_by,
	'IsProcess': 'Y',
	'action': 'send',
};

jQuery.ajax({
	type: "GET",
	url: sorting_url,
	data: params,			
	dataType: 'html',
	success: function(data)
	{				
		jQuery(".container-inner").html(data+"<div class='clear'></div>");
	}
});
return false;		
}
</script>
<script type="text/javascript">
function redirect_function(id)
{
//alert(id);
  document.forms["videoform"+id].submit();
}
</script>
<script type="text/javascript">
  function LikeVideo(coach_id,video_id,like,numlike)
 { 	 
 
	var str1=  '</div></a>';
	if(numlike<1 ) numlike=parseInt(numlike);	
	
 	if(like == 'N')
	{ 
		numlike=parseInt(numlike+1);
		var str=  '<a href="javascript:;" onclick=\"return LikeVideo('+coach_id+','+video_id+',\'Y\','+numlike+')\"><div class="game-rating">';		 
	 jQuery.ajax({
		   url:'like_post.php',
		   data:'action=sendlike&coach_id='+coach_id+'&video_id='+video_id,
		   type:'post',		 
		   dataType:'html', 
		   success:function(resp){			  
			  	 jQuery('#game-rating_'+video_id).html(str+resp+str1);
				  jQuery('#votes_'+video_id).html(numlike);								
			}			   
	});
	}
	else
	{
		numlike=parseInt(numlike-1);
		var str2=  '<a href="javascript:;" onclick=\"return LikeVideo('+coach_id+','+video_id+',\'N\','+numlike+')\"><div class="game-rating">';
		 jQuery.ajax({
		   url:'like_post.php',
		   data:'action=sendunlike&coach_id='+coach_id+'&video_id='+video_id,
		   type:'post',		 
		   dataType:'html', 
		   success:function(resp){		  		  
			  	 jQuery('#game-rating_'+video_id).html(str2+resp+str1);	
				 jQuery('#votes_'+video_id).html(numlike);		
				 						
			}			   
	});	
	}
}

</script>
{/literal}
 <!-- <div class="left-panel">
   <div class="graphite demo-container">
	<ul class="accordion" id="accordion-1">
	   <li><a href="#">{$game_name}</a>
        <ul>
            { if $Numrace >0}
			<li><a href="#">Race</a>
                <ul>
                    <li><a href="#" onclick="ManagerGeneral('{$page_name}?action=list_search&from={$from}&from_page={$from_page}')">All <span>({$totRec})</span></a></li>
                    {section name=race loop=$RaceArr}
					{if $RaceArr[race].countrow neq '0'}
					 <li><a href="#"
onclick="ManagerGeneral('{$page_name}?action=list_search&dosearch=GO&race_id={$RaceArr[race].race_id}&from={$from}&{$SLink}&from_page={$from_page}')">{$RaceArr[race].race_title} <span>({$RaceArr[race].countrow})</span></a></li>
					 {/if}
                    {/section}
                </ul>
            </li>
			{/if}
            {if $Numladder >0}
			<li><a href="#">Ladder</a>
                <ul>
                    <li><a href="#" onclick="ManagerGeneral('{$page_name}?action=list_search&from={$from}&from_page={$from_page}')">All <span>({$totRec})</span></a></li>
					{section name=ladder loop=$LadderArr}
					 {if $LadderArr[ladder].countrow neq '0'}
                     <li>
					 <a href="#"
onclick="ManagerGeneral('{$page_name}?action=list_search&dosearch=GO&ladder_id={$LadderArr[ladder].ladder_id}&{$SLink}&from={$from}&from_page={$from_page}')">{$LadderArr[ladder].ladder_name}<span>({$LadderArr[ladder].countrow})</span></a></li>
					 {/if}
                    {/section}
                </ul>
            </li>
			{/if}
			
        	<li><a href="#">Rating</a>
                <ul>
                    <li><a href="#" onclick="ManagerGeneral('{$page_name}?action=list_search&from={$from}&from_page={$from_page}')">All <span>({$totRec})</span></a></li>
                    {section name=rating loop=$StarArr}
					<li><a href="#"
onclick="ManagerGeneral('{$page_name}?action=list_search&dosearch=GO&overall_rating={$StarArr[rating].star}&{$SLink}&from={$from}&from_page={$from_page}')">
                    {$StarArr[rating].star} Star <span>({$StarArr[rating].countrow})</span></a></li>
					{/section}
                </ul>
            </li>
        
        </ul>
	   </li>
       
    </ul>
   </div>   
   </div> -->
   

<div class="right-panel">
<!--this is the loading screen div-->
<div id="loading" class="loading-visible">
</div>
<!--this is the loading screen JavaScript-->
      <div class="title">Replays</div>
	  <div class="clear"></div>
      <div class="searchpanel" style=''>
           <form action="{$page_name}" method="post" name="form_search" id="form_search" >
              <fieldset>
			  <select name="order_by" id="order_by" onchange="subForm(this.value);">
				<option value="">Sort By</option>
				<option value="time_length" {if $sorting_by eq 'time_length'} selected="selected" {/if} >Duration(Low)</option>
				<option value="time_length-DESC" {if $sorting_by eq 'time_length-DESC'} selected="selected" {/if} >Duration(High)</option>
				<option value="date_added" {if $sorting_by eq 'date_added'} selected="selected" {/if} >Upload Date(Ascending)</option>
				<option value="date_added-DESC" {if $sorting_by eq 'date_added-DESC'} selected="selected" {/if} >Upload Date(Discending)</option>
				<option value="review" {if $sorting_by eq 'review'} selected="selected" {/if} >View Count(Ascending)</option>
				<option value="review-DESC" {if $sorting_by eq 'review-DESC'} selected="selected" {/if} >View Count(Discending)</option>
			   </select>
			  
			  
			  
			  
			 <!-- <input name="vnm" class="text" type="text" id="vnm" {if $vdotitle neq ''}value="{$vdotitle}"{else}value="Keyword"{/if} />
			  <input type="button" value="" onclick="searchVdo()" />-->
			  <!--input type="button" value="clear" onclick="resetSearch()" /-->
                
               </fieldset>
            </form>
           <div class="clear"></div>
      </div>
      <div class="clear"></div>
	  {if $NumReplay >0}
      <div class="game-list">
	  {section name=videoRow loop=$ReplayArr}
	   	 <form name="videoform{$ReplayArr[videoRow].video_id}" id="videoform{$ReplayArr[videoRow].video_id}" method="post" 
			action="videodetails.php?video_id={$ReplayArr[videoRow].video_id}">
			    <input type="hidden" name="ladders" value="{$ladder_id}" />
				<input type="hidden" name="races" value="{$race_id}" />
				<input type="hidden" name="champion_id" value="{$champion_id}" >
				<input type="hidden" name="class_id" value="{$class_id}" >
				<input type="hidden" name="map_id" value="{$map_id}" >
				<input type="hidden" name="mode_id" value="{$mode_id}" >
				<input type="hidden" name="team_id" value="{$team_id}" >
				<input type="hidden" name="type_id" value="{$type_id}" >
				<input type="hidden" name="versus_id" value="{$versus_id}"  />
				<input type="hidden" name="game_id" value="{$game_id}"  />
				<input type="hidden" name="Numladder" value="{$Numladder}" />
				<input type="hidden" name="NumReplay" value="{$NumReplay}" />
				<input type="hidden" name="NumChampion" value="{$NumChampion}" />
				<input type="hidden" name="NumClass" value="{$NumClass}" />
				<input type="hidden" name="NumMap" value="{$NumMap}" />
				<input type="hidden" name="NumMode" value="{$NumMode}" />
				<input type="hidden" name="NumTeam" value="{$NumTeam}" />
				<input type="hidden" name="NumType" value="{$NumType}" />
				<input type="hidden" name="NumVersus" value="{$NumVersus}" />
		  {assign var='v' value=$smarty.section.videoRow.index+1}
			 <div {if $v%3 eq '0'}class="games-replay-last"{else}class="games-replay"{/if} >
			   <div class="replay-content" >
			  <!-- <a href="videodetails.php?video_id={$ReplayArr[videoRow].video_id}">-->
			   <a href="#" onclick="redirect_function({$ReplayArr[videoRow].video_id})">
			   <img src="uploaded/video_images/thumbs/mid_{$ReplayArr[videoRow].video_image}" alt="" border="0" /></a>
			   
			   </div>
			   <div class="replay-content">
				  <div class="replay-title">
				  <a href="#" onclick="redirect_function({$ReplayArr[videoRow].video_id})">{$ReplayArr[videoRow].video_title}</a>
				  <br /><span class="date">{$ReplayArr[videoRow].video_date}<br />
				  {if $ReplayArr[videoRow].time_length neq ''}{$ReplayArr[videoRow].time_length} min{else}N/A{/if}
				  </span></div>
				  <!--{if $ReplayArr[videoRow].user_id neq $smarty.session.user_id && $smarty.session.user_id neq ''}
				  <div id="game-rating_{$ReplayArr[videoRow].video_id}">			 
				  <a href="javascript:;" onclick="return LikeVideo({$ReplayArr[videoRow].user_id},{$ReplayArr[videoRow].video_id},'{$ReplayArr[videoRow].like}',{$ReplayArr[videoRow].numlike})">
				  <div class="game-rating">{if $ReplayArr[videoRow].like eq 'Y'} <img alt="Unlike" src="images/unlike.png"> {else} <img alt="Like" src="images/like.png"> {/if}</div>  
				  </a>      
				  </div>  
				  {/if} -->
				    <div class="game-rating">
					
                                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tr>
                                                <td><b>Likes:</b></td>
                                                <td>{$ReplayArr[videoRow].numlike}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Disikes:</b></td>
                                                <td>{$ReplayArr[videoRow].numunlike}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Views:</b></td>
                                                <td>{$ReplayArr[videoRow].view_count}</td>
                                            </tr>
                                        </table> 
                                        
				</div> 
					<br  />
				         					
                  <div class="replay-title" style="padding-top:0px; width:90%"><span class="date">by <em>{$ReplayArr[videoRow].user}</em></span></div>
                 
			   </div>
			 </div>
		  {if $v%3 eq '0'}<div class="clear"></div>{/if}
		  </form>	 
        {/section}
       <div class="clear"></div>
      </div>
      {if $pagination_arr[1]}
      <div class="clear"></div>
      <div class="pagin">
         <ul>
            {$pagination_arr[1]}
         </ul>
      </div>
	  {/if}
	 {else}
	    No Replays is found......
     {/if}
   </div>
</div>
{literal}
<style type="text/css">  
  div.loading-visible{
    /*make visible*/
    display:block;
    /*position it 200px down the screen*/
    position:absolute;
    top:200px;
    left:145px;
    width:100%;
    text-align:center;
    /*in supporting browsers, make it
      a little transparent*/    
	filter: alpha(opacity=75); /* internet explorer */
	-khtml-opacity: 0.75;      /* khtml, old safari */
	-moz-opacity: 0.75;       /* mozilla, netscape */
	opacity: 0.75;           /* fx, safari, opera */
   
  }
</style>
{/literal}