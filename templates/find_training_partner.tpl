{literal}
<link type="text/css" href="jss/tipTip.css"  rel="stylesheet" />
<script src="jss/jquery.tipTip.js" type="text/javascript"></script>

<script>
jQuery(function(){
    jQuery(".someClass").tipTip(); 
});
</script>

<script>
function ListSorting(sorting_url,sorting_by,ladder){
    var params = {
        'list_for':'sorting',
            'sorting_by': sorting_by,
            'IsProcess': 'Y',
            'ladder_id': ladder
    };
    alert(ladder);
    jQuery.ajax({
            type: "GET",
            url: sorting_url,
            data: params,			
            dataType: 'html',
            success: function(data)
            {				
                    jQuery("#paging").html(data);
            }
    });
    return false;		
}
function expand(id,e)
{
  jQuery('#small_'+id).css('display','none');
  jQuery('#big_'+id).css('display','block');
  if (!e) var e = window.event;
  e.cancelBubble = true;
  if (e.stopPropagation) e.stopPropagation();
  
}
function collapse(id,e)
{
  jQuery('#big_'+id).css('display','none');
  jQuery('#small_'+id).css('display','block');
  if (!e) var e = window.event;
  e.cancelBubble = true;
  if (e.stopPropagation) e.stopPropagation();
  
}
function reviewList(id,e)
{
  if (!e) var e = window.event;
  e.cancelBubble = true;
  if (e.stopPropagation) e.stopPropagation();
  window.location.href="training_partner_details.php?training_partner_id="+id+"#review";
}

</script>

{/literal}
<script type="text/javascript">
function redirect_function(id){ldelim}
	{if $smarty.session.user_id eq ''}
	openLogin('/training_partner_details.php?training_partner_id='+id);
	return false;
	{/if}
	jQuery('#coachform'+id).submit();
{rdelim}
    
</script>
<div id="paging"> 
 <div class="right-panel">
 <!--this is the loading screen div-->
<div id="loading" class="loading-visible">
  
</div>
<!--this is the loading screen JavaScript-->
	  <div class="title">Find Training Partner</div>
       <div class="searchpanel">
         <form action="" method="post">
              <fieldset>
			  {if $NumCoach >0}
			   <div style="float:left; display:block; width:170px;">
			   {$pagination_arr[2]} </div>
			  {/if}  
			    <div style="float:right; display:block; border:0px solid #FF0000;">
			  <select for="sirt" name="sort_by" onchange="subForm(this.value);">
			      <option value="">Sort by</option>
                  <option value="rating" {if $sort_by eq 'rating'} selected="selected" {/if}>Overall Rating (Low)</option>
				  <option value="rating-DESC" {if $sort_by eq 'rating-DESC'} selected="selected" {/if}>Overall Rating (High)</option>
				 
               </select>
			   </div>
               </fieldset>
            </form>
           <div class="clear"></div>
      </div>
      <div class="clear"></div>

      <div class="clear"></div>
 

   {if $SearchTxt neq ''}<div><b>Search By</b>  {$SearchTxt}</div>{/if}
	 {if $NumCoach >0}
	  {section name=coachRow loop=$CoachArr}
      <div class="game-list-block" onclick="redirect_function({$CoachArr[coachRow].user_id})">
        <form name="coach{$CoachArr[coachRow].user_id}" id="coachform{$CoachArr[coachRow].user_id}" method="post" 
			action="training_partner_details.php?training_partner_id={$CoachArr[coachRow].user_id}">
			<input type="hidden" name="prop_ids" value="{$prop_ids}" />
			<input type="hidden" name="ratings" value="{$ratings}" />
			<input type="hidden" name="availabilitys" value="{$availabilitys}" />
			<input type="hidden" name="game_id" value="{$game_id}" />
			<input type="hidden" name="availability_country" value="{$availability_country}" />
			<input type="hidden" name="availability_city" value="{$availability_city}" />
		    <div class="game-image-block">
			<a href="#" onclick="redirect_function({$CoachArr[coachRow].user_id})">
			{if $CoachArr[coachRow].photo neq ''}
		     <img src="uploaded/user_images/thumbs/big_{$CoachArr[coachRow].photo}" alt="Coach" border="0"  />
			{else}
			 <img src="images/coach_thumb.jpg">
		    {/if}
			</a> 
		    </div> 
		  <div class="game-details-block" style="height:100px;" id="small_{$CoachArr[coachRow].user_id}">
                    <div class="score_title listcoachname" data-id="{$CoachArr[coachRow].user_id}">
                        <h2>
                            <a class="{$CoachArr[coachRow].status}" title="{$CoachArr[coachRow].status}"><a href="#" onclick="redirect_function({$CoachArr[coachRow].user_id})" style="text-decoration:none;">{$CoachArr[coachRow].username}</a> 
                            <a id="{$CoachArr[coachRow].user_id}_friendStar" class="{if $CoachArr[coachRow].isfriend eq 'y'}star_friend{else}star_not_friend{/if}" title="{if $CoachArr[coachRow].isfriend eq 'y'}in your friend list{else}not your friend list{/if}"></a>
                        </h2>
                        {if $smarty.session.user_id neq '' && $smarty.session.user_id neq $CoachArr[coachRow].user_id}
                        <div class="addAsFriendWindow" id="addAsFriendWindow_{$CoachArr[coachRow].user_id}">
                            <a class="addAsFriendLink" href="javascript:;" onclick="openChatTab(event,'{$CoachArr[coachRow].user_id}','{$CoachArr[coachRow].username}')">Message</a><br/>
                            {if $CoachArr[coachRow].isfriend eq 'n'}
                            <a class="addAsFriendLink removeFriend" onclick="addAsFriend(event,'{$CoachArr[coachRow].user_id}','{$CoachArr[coachRow].username}',true)" href="javascript:;">Add as friend</a>
                            {else}
                            <a class="addAsFriendLink addFriend" onclick="removeFriend(event,'{$CoachArr[coachRow].user_id}','{$CoachArr[coachRow].username}',true)" href="javascript:;">Remove friend</a>
                            {/if}
                        </div>
                        {/if}
                    </div>
            
		    <div class="score_card">
			<div class="overall_card"><div style="width: 47px; height: 28px;">{$CoachArr[coachRow].overall_rating}<br/>Overall</div></div>
            
            </div>
			 
            <p class="clear"></p>
            <div class="games-desc">
				<div style="margin-top: 10px; height: 60px">{$CoachArr[coachRow].about}</div>
            </div>

            <div class="clear"></div>
			
            <div class="clear"></div>
			<div class="preview">
			<a href="javascript:;" style="float:right;" onclick="expand({$CoachArr[coachRow].user_id},event)">Click to Preview</a>
			</div>
			</div>
		  <div class="game-details-block" style="display:none;" id="big_{$CoachArr[coachRow].user_id}">
		  	<div class="score_title">
             <h2><a class="{$CoachArr[coachRow].status}" title="{$CoachArr[coachRow].status}"><a href="#" onclick="redirect_function({$CoachArr[coachRow].user_id})" style="text-decoration:none;">{$CoachArr[coachRow].username}</a> 
             </div>
              <div class="score_card">
            <div id="scoreCard">
                <ul>
                    {if $CoachArr[coachRow].overall_rating neq 0}
                    <li id="Overall" class="tTip">
                        <span class="someClass" title="Would you train with this partner again?" >
                        {$CoachArr[coachRow].overall_rating}<br/>Overall
                        </span>
                    </li>
                    {/if}
                
                
                    {section name=k loop=$CatIndividualRec[coachRow]}
                    <li id="$CatIndividualRec[k].rating_category" class="tTip">
                       <span class="someClass" title="{$CatIndividualRec[coachRow][k].rating_category_tooltip}">
                       {$CatIndividualRec[coachRow][k].rating}
                       <span>{$CatIndividualRec[coachRow][k].rating_category}</span>
                       </span>
                    </li>   
                    {/section} 
                    
                </ul>
            </div>
            </div>
            <p class="clear"></p>
			 <div class="games-desc user_details" style="margin-top: 10px;">
                                {section name=categoryIndex loop=$CoachArr[coachRow].categoriesArr}
                                <dl>
                                    <dt>{$CoachArr[coachRow].categoriesArr[categoryIndex].category_name}:</dt>
                                    <dd>{if $CoachArr[coachRow].categoriesArr[categoryIndex].properties neq ''}{$CoachArr[coachRow].categoriesArr[categoryIndex].properties}{else}&nbsp;{/if}</dd>
                                </dl>
                                {/section}
				<dl>
                                    <dt>Availability:</dt>
                                    <dd>{if $CoachArr[coachRow].availability_type neq ''}{$CoachArr[coachRow].availability_type}{else}N/A{/if}</dd>
				</dl>
				{if $CoachArr[coachRow].avail_local eq 'Y'}
				<dl>
                                    <dt>City (local meet-up):</dt>
                                    <dd>{if $CoachArr[coachRow].availability_city neq ''}{$CoachArr[coachRow].availability_city}{else}N/A{/if}</dd>
				</dl>
				{/if}
				
				<dl>
                                    <dt>Peak Hours:</dt>
                                    <dd>{if $CoachArr[coachRow].peak_hours neq ''}{$CoachArr[coachRow].peak_hours}{else}&nbsp;{/if}</dd>
				</dl>
				<dl>
                                    <dt>Language:</dt>
                                    <dd>{if $CoachArr[coachRow].language neq ''}{$CoachArr[coachRow].language}{else}N/A{/if}</dd>
				</dl>
				
                                <dl>
                                    <dt>Introduction:</dt>
                                    <dd><div style="width: 438px;float: left;">{if $CoachArr[coachRow].about neq ''}{$CoachArr[coachRow].about}{else}N/A{/if}</div></dd>
				</dl>
                                <dl>
					<dt>Experience:</dt>
					<dd>{if $CoachArr[coachRow].experience neq ''}{$CoachArr[coachRow].experience}{else}N/A{/if}</dd>
				</dl>
                                <dl>
                                    <dt>Need improvement</dt>
                                    <dd>{if $CoachArr[coachRow].need_improvement neq ''}{$CoachArr[coachRow].need_improvement}{else}N/A{/if}</dd>
                                </dl>
				
            </div>
			
            
            <div class="clear"></div>
			
			<div class="preview">
			<a href="javascript:;" style="float:right;" onclick="collapse({$CoachArr[coachRow].user_id},event)">fold</a>
			</div>
			</div>
			<div class="clear"></div>
			</form>
         <div class="clear"></div>
      </div>
	 {/section} 
     {if $pagination_arr[1]}
	 <div class="clear"></div>
      <div class="pagin">
         <ul>
           {$pagination_arr[1]}
         </ul>
      </div>
	 {/if}
	 {else}
	    No Training Partner is found......
     {/if}
	</div> 
   </div>
{literal}
<style type="text/css">  
  div.loading-visible{
    /*make visible*/
    display:block;
    /*position it 200px down the screen*/
    position:absolute;
    top:200px;
    left:145px;
    width:100%;
    text-align:center;
    /*in supporting browsers, make it
      a little transparent*/    
	filter: alpha(opacity=75); /* internet explorer */
	-khtml-opacity: 0.75;      /* khtml, old safari */
	-moz-opacity: 0.75;       /* mozilla, netscape */
	opacity: 0.75;           /* fx, safari, opera */
   
  }
</style>
{/literal}
<script language="javascript">
    changePageTitle('Find training partner');
</script>