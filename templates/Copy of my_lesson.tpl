{literal}
<script type="text/javascript">

function Search_Income(formID){

	var frmID='#'+formID;
	
	var params ={
	
		'module': 'contact'
	};

	var paramsObj = j(frmID).serializeArray();

	jQuery.each(paramsObj, function(i, field){

		params[field.name] = field.value;

	});
	
	jQuery.ajax({
		type: "POST",
		url:  'income_search.php',
		data: params,
		dataType : 'json',
		success: function(data){
		//alert(data.IncomeSql);
			if(data.total_in != ''){
				//alert(data.total_in);
				j('#total_income').html(data.total_in);
			}
        }
      });

}

function Search_Lesson(formID){

	var frmID='#'+formID;

	var params ={
	
		'module': 'contact'
	};

	var paramsObj = v(frmID).serializeArray();

	jQuery.each(paramsObj, function(i, field){

		params[field.name] = field.value;

	});
	
	jQuery.ajax({
		type: "POST",
		url:  'my_lesson.php?action=list_search',
		data: params,
		dataType : 'data',
		success: function(data){
			if(data.total_in != ''){
				//alert(data.total_in);
				jQuery('#total_income').html(data.total_in);
			}
        }
      });

}
</script>
{/literal}
<div id="paging">
<div class="right-panel">
      <h1 title="Games"><span>My Lesson</span></h1>
      <div class="clear"></div>
	  <p class="button-flex-big button90"><a href="my_lesson_update.php"><span>Add Lesson</span></a></p>
	  <div>
	  <!--<form method="POST" name="form_search" id="form_search" action="my_lesson.php" onsubmit="return false;">
		<table width="80%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="40%" align="left" valign="middle">	
				<input size="30" type="text" name="Search_lesson_title" id="Search_lesson_title" value="{$Search_lesson_title}" class="textbox search-box"/>	
				</td>
				<td width="40%" align="right" valign="middle" style="color:#000000;">
				Select Student:&nbsp;<select name="Search_student_id" id="Search_student_id">
					<option value="">--Select--</option>
					{section name=Rows loop=$SelectStudentArr}
						<option value="{$SelectStudentArr[Rows].student_id}" {if $SelectStudentArr[Rows].student_id eq $Search_student_id} selected {/if}>{$SelectStudentArr[Rows].student_name}</option>
					{/section}
				</select>
				</td>
			</tr>
			<tr>
			<td width="40%" align="left" valign="middle">
				<div style="float:left;">
					<input type="text" name="Search_from_date" id="Search_from_date" class="datebox" value="{$Search_from_date}" style="width:80px;"/>
					<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_from_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				</div>
				<div style="float:right;">
					<input type="text" name="Search_to_date" id="Search_to_date" class="datebox" value="{$Search_to_date}" style="width:80px;"/>
					<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search.Search_from_date,document.form_search.Search_to_date);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
				</div>
					</td>
					<td width="40%" align="right" valign="middle" style="color:#000000;">
				Select Status:&nbsp;<select name="Search_lesson_status" id="Search_lesson_status">
					<option value="">--Select--</option>
					{section name=Rows loop=$SelectStatusArr}
						<option value="{$SelectStatusArr[Rows].lesson_status_id}" {if $SelectStatusArr[Rows].lesson_status_id eq $Search_lesson_status} selected {/if}>{$SelectStatusArr[Rows].lesson_status_title}</option>
					{/section}
				</select>
				</td>
			</tr>
			<tr>	
				<td valign="middle">
				<input type="hidden" name="dosearch" value="GO">
				<input  name="submitdosearch" value="GO" type="button" onClick="Search_Lesson('form_search');" />
				<input name="" type="button" value="RESET" onClick="window.location.href='my_lesson.php';"/></td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>
		</table>
		</form>-->
	  </div>
	  <div>
		<form method="POST" name="form_search_in" id="form_search_in" action="my_lesson.php" onsubmit="return false;">
			<table width="80%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td width="40%" align="left" valign="middle">
					<div style="float:left;">
					<input type="text" name="Search_from_date_in" id="Search_from_date_in" class="datebox" value="Income From" style="width:80px;"/>
					<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search_in.Search_from_date_in,document.form_search_in.Search_from_date_in);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
					</div>
					<div style="float:right;">
					<input type="text" name="Search_to_date_in" id="Search_to_date_in" class="datebox" value="Income To" style="width:80px;"/>
					<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.form_search_in.Search_from_date_in,document.form_search_in.Search_to_date_in);return false;" HIDEFOCUS><img class="PopcalTrigger" align="top" src="images/icon_calender.gif" border="0" alt="Click to set Date" height="17"></a>
					</div>
				</td>
				<td style="color:#000000; padding-left:15px;" width="40%">
				Total Income:&nbsp;<span id="total_income"></span>
				</td>
				</tr>
				<tr>	
					<td valign="middle">
					<input type="hidden" name="in_search" value="DO">
					<input  name="submitdosearch" type="button" value="GO" onClick="Search_Income('form_search_in');" />
					<input name="" type="button" value="RESET" onClick="window.location.href='my_lesson.php';"/></td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>
			</table>
		</form>	  
	  </div>
	  
	 {if $Numcoachgame >0}
	  {section name=gameRow loop=$CoachgameArr}
      <div class="game-list">
       <div class="game-details">
	        <h4>{$CoachgameArr[gameRow].lesson_title}</h4>
			 <ul class="games-desc" style="padding-top:0px;">
			   <li><span>Date :</span> {if $CoachgameArr[gameRow].date_added neq '0000-00-00'}{$CoachgameArr[gameRow].date_added|date_format:"%D"}{else}N/A{/if}</li> 
			   
			   {if $CoachgameArr[gameRow].student_name neq ''}
			   <li><span>Student :</span> {if $CoachgameArr[gameRow].student_name neq ''}{$CoachgameArr[gameRow].student_name}{else}N/A{/if}</li>
			   {/if}
			   
			   {if $CoachgameArr[gameRow].lesson_price neq '0.00'}
               <li><span>Price :</span> {if $CoachgameArr[gameRow].lesson_price}{$CoachgameArr[gameRow].lesson_price}{else}N/A{/if} </li>
			   {/if}
			   {if $CoachgameArr[gameRow].lesson_status neq ''}
               <li><span>Status :</span> {if $CoachgameArr[gameRow].lesson_status}{$CoachgameArr[gameRow].lesson_status}{else}N/A{/if} </li>
			   {/if}
            </ul>
			<p class="clear"></p>
            <div style="height:10px;"></div>
            <div class="clear"></div>
			{if $CoachgameArr[gameRow].lesson_status eq 'Open'}
            <div style="width:100%; padding-left:150px;"><div class="view">
			<a href="#" onclick="ConfirmDelete('{$page_name}?action=del','{$CoachgameArr[gameRow].lesson_id}','{$CoachgameArr[gameRow].lesson_title}','Your selected lesson:: ')">
			Delete</a></div>
			<div class="view" style="margin-right:5px;">
			<a href="my_lesson_update.php?lesson_id={$CoachgameArr[gameRow].lesson_id}">Edit</a></div></div>
			{/if}
         </div>
         <div class="clear"></div>
      </div>
      {/section}
	  {else}
	    <div class="game-details"> No records found </div>
     {/if}
      <div class="clear"></div>
   </div>
 </div>     