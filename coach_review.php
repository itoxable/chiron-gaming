<?php
include "general_include.php";
include "checklogin.php";

$page_name=basename($_SERVER['SCRIPT_FILENAME']);
$IsPreserved	= 'Y';
$IsProcess		= $_REQUEST['IsProcess'];
if($IsProcess <> 'Y')
{
	include "top.php";
	//include "left.php";
}

$item_per_page=$_SESSION['item_per_page'] = $_REQUEST['item_per_page'];
	if(empty($item_per_page) || !isset($item_per_page))
	{
	$item_per_page=10;
	} 

$action	= $_REQUEST['action'];

if($action=="del")
{	
	if(!empty($delete_id))
	{
	
	  $Reviewdtls = "SELECT * FROM ".TABLEPREFIX."_user_review WHERE user_review_id='$delete_id'";
	  $dtlsArr = $UserManagerObjAjax->GetRecords("Row",$Reviewdtls);
	  $rating = $dtlsArr['rating'];
	  $review_to_id = $dtlsArr['user_id'];
	  $is_active = $dtlsArr['is_active'];
	  $totreview = "SELECT count(*),sum(rating) as totrate FROM ".TABLEPREFIX."_user_review WHERE user_id='".$review_to_id."' AND is_active='Y'";
	  $totArr = $UserManagerObjAjax->GetRecords("Row",$totreview);
	  $tot_count = $totArr[0];
	  $tot_rate = $totArr['totrate'];
	  if($is_active == 'Y')
	  {
		$rate = $tot_rate-$rating;
		$totreviewer = $tot_count-1;
	  }	
		$avg = $rate/$totreviewer;
		$UpdateSql = "UPDATE ".TABLEPREFIX."_user_type_user_relation SET overall_rating='$avg' WHERE user_id='$review_to_id'";
		$UserManagerObjAjax->Execute($UpdateSql);
		/* Delete review Starts */
		$SqlDelete="DELETE FROM ".TABLEPREFIX."_user_review WHERE user_review_id='$delete_id'";
		$UserManagerObjAjax->Execute($SqlDelete);		
		/* Delete review Ends */
	}	
		
}

//Start Coach Review By Me
$reviewother = "SELECT ur.*,u.name,u.photo FROM ".TABLEPREFIX."_user u ,".TABLEPREFIX."_user_review ur WHERE ur.reviewed_by='".$_SESSION['user_id']."' AND u.user_id=ur.user_id ORDER BY ur.date_added DESC";
 $PaginationObjAjaxLatest=new PaginationClassAjax($item_per_page,"prev",'',"next","active",$adodbcon);
 $pagination_arr = $PaginationObjAjaxLatest->PaginationAjax($reviewother,$page_name."?action=".$action."&item_per_page=".$item_per_page,"Managergeneral");	
 $reviewotherArr = $UserManagerObjAjax->GetRecords("All",$pagination_arr[0]);
 $NumotherReview = count($reviewotherArr);
 for($x=0;$x<$NumotherReview;$x++)
 {
	$dateSql = "SELECT date_format('".$reviewotherArr[$x]['date_added']."','%M  %d , %Y at %h %p') as r_date";
	$DateArr = $UserManagerObjAjax->GetRecords("Row",$dateSql);
	$reviewotherArr[$x]['review_date']=$DateArr['r_date'];
	
	$Usertype = "SELECT user_type_id FROM ".TABLEPREFIX."_user_type_user_relation WHERE user_id='".$reviewotherArr[$x]['user_id']."'";
	$typeArr = $UserManagerObjAjax->GetRecords("All",$Usertype);
	$link = 'coachdetails.php?coach_id='.$reviewotherArr[$x]['user_id'];
 
    $reviewotherArr[$x]['link']=$link;
    $Sql_Individual="select rat.comment,rat.rating,cat.rating_category from ".TABLEPREFIX."_user_rating as rat,gt_rating_category as cat where rat.rcat_id=cat.rcat_id and rat.user_review_id=".$reviewotherArr[$x]['user_review_id'];
  
	$Individual =  $UserManagerObjAjax->GetRecords("All",$Sql_Individual);
	
	for($k=0;$k<count($Individual);$k++){
	
		$Individual[$k]['comment']=$Individual[$k]['comment'];
		$Individual[$k]['rating']=$Individual[$k]['rating'];
		//$Individual[$x][$k]['rating_category']=$Individual[$x][$k]['rating_category'];
	
	}
   $reviewotherArr[$x]['individual']=$Individual;
 }
 /*echo '<pre>';
 print_r($Individual);*/
// End Coach Review by Me
if($IsProcess == 'Y') {
	$Sqltype = "SELECT * FROM ".TABLEPREFIX."_user_type_user_relation WHERE user_id='".$_SESSION['user_id']."'";
	$typeArr = $UserManagerObjAjax->GetRecords("All",$Sqltype);
	$Numtype = count($typeArr);
	$is_coach = 0;
	$is_partner = 0;
	for($t=0;$t<$Numtype;$t++)
	{
	   if($typeArr[$t]['user_type_id']==1)
		  $is_coach = 1;
	   if($typeArr[$t]['user_type_id']==3)
		  $is_partner = 1;
	}
}	

$smarty->assign('is_coach',$is_coach);
$smarty->assign('is_partner',$is_partner);
$smarty->assign('NumotherReview',$NumotherReview);
$smarty->assign('reviewotherArr',$reviewotherArr);
$smarty->assign('page_name',$page_name);
$smarty->assign('Individual',$Individual);
$smarty->display('coach_review.tpl');
if($IsProcess <> 'Y')
 include "footer.php";
?>
