<?php
$page_name=basename($_SERVER['SCRIPT_FILENAME']);
$IsPreserved	= 'Y';
$IsProcess		= $_REQUEST['IsProcess'];
include "general_include.php";
include "checklogin.php";
if($IsProcess <> 'Y')
{
	include "top.php";
	//include "left.php";
}

$item_per_page=$_SESSION['item_per_page'] = $_REQUEST['item_per_page'];
	if(empty($item_per_page) || !isset($item_per_page))
	{
	$item_per_page=10;
	} 

$action	= $_REQUEST['action'];


//Start my Video Review
 $videoReview = "SELECT vr.*,v.video_title,u.name,u.photo FROM ".TABLEPREFIX."_video_review vr,".TABLEPREFIX."_video v,".TABLEPREFIX."_user u WHERE vr.video_id=v.video_id AND 
 v.user_id='".$_SESSION['user_id']."' AND vr.is_active='Y' AND u.user_id=vr.reviewed_by ORDER BY vr.date_added DESC";
 $PaginationObjAjaxLatest=new PaginationClassAjax($item_per_page,"prev",'',"next","active",$adodbcon);
 $pagination_arr = $PaginationObjAjaxLatest->PaginationAjax($videoReview,$page_name."?action=".$action."&item_per_page=".$item_per_page,"Managergeneral");	
 $videoReviewArr = $UserManagerObjAjax->GetRecords("All",$pagination_arr[0]);
 $Numvideo = count($videoReviewArr);
 for($vr=0;$vr<$Numvideo;$vr++)
 {
     $dateSql = "SELECT date_format('".$videoReviewArr[$vr]['date_added']."','%M  %d , %Y at %h %p') as vr_date";
	 $DateArr = $UserManagerObjAjax->GetRecords("Row",$dateSql);
	 $videoReviewArr[$vr]['review_date']=$DateArr['vr_date'];
	 
	$Usertype = "SELECT user_type_id FROM ".TABLEPREFIX."_user_type_user_relation WHERE user_id='".$videoReviewArr[$vr]['reviewed_by']."'";
	$typeArr = $UserManagerObjAjax->GetRecords("All",$Usertype);
	for($t=0;$t<count($typeArr);$t++)
	{
	  if($typeArr[$t][0] == 1)
	     $coach = 1;
	  if($typeArr[$t][0] == 3)
	     $partner = 1;	 
	}
	if($coach == 1)
	{ 
	   $videoReviewArr[$vr]['user_type']='Coach';
	   $videoReviewArr[$vr]['link']='coachdetails.php?coach_id='.$myReviewArr[$r]['reviewed_by'];
	}
	else if($partner == 1)
	{
	   $videoReviewArr[$vr]['user_type']='Training Partner';
	   $videoReviewArr[$vr]['link']='training_partner_details.php?training_partner_id='.$myReviewArr[$r]['reviewed_by'];
	}  
	else 
	{ 
	   $videoReviewArr[$vr]['user_type']='Student';
	   $videoReviewArr[$vr]['link']='#';
	}  
	
 
 }
//End My Video Review 

$smarty->assign('is_coach',$is_coach);
$smarty->assign('is_partner',$is_partner);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('Numvideo',$Numvideo);
$smarty->assign('videoReviewArr',$videoReviewArr);
$smarty->assign('page_name',$page_name);
$smarty->display('my_video_review.tpl');
if($IsProcess<>'Y')
  include "footer.php";
?>
