<?php
include "general_include.php";
include "checklogin.php";
include "top.php";
include "left.php";

$timing_id = $_REQUEST['timing_id'];

if(isset($_POST['submit']))
{
    $Formval = $_POST;
    $time_slot_id = $_POST['time_slot_id'];
	$time_from = $_POST['time_from'];
	$time_to = $_POST['time_to'];
	if(empty($timing_id))
		{
			$table_name = TABLEPREFIX."_timings ";
			$fields_values = array( 
									'time_slot_id'						=> $time_slot_id,
									'user_id'                           => $_SESSION['user_id'],
									'time_from'					        => $time_from,
									'time_to'                           => $time_to                          
								 );		
			
			$UserManagerObjAjax->InsertRecords($table_name,$fields_values);
		}
		else if(!empty($timing_id))
		{
			$table_name = TABLEPREFIX."_timings";
			$fields_values = array( 
									'time_slot_id'						=> $time_slot_id,
									'time_from'					        => $time_from,
									'time_to'                           => $time_to           
									);

			$where="timing_id='$timing_id'";										
			$UserManagerObjAjax->UpdateRecords($table_name,$fields_values,$where);
		}		
		echo "<script>window.location.href='timing.php'</script>";
		exit; 
    
}


if(!empty($timing_id))
{
	/* Get Record For Display Starts */	

	$SelecttimeSql="SELECT * FROM ".TABLEPREFIX."_timings WHERE timing_id='".$timing_id."' AND user_id='".$_SESSION['user_id']."'";
	$Formval = $UserManagerObjAjax->GetRecords("Row",$SelecttimeSql);
	
	/* Get Record For Display Ends */

	$SubmitButton="Edit";	
	
}
else
{
	$SubmitButton="Add";
}

$timeslotSql = "SELECT time_slot_id,time_slot FROM ".TABLEPREFIX."_time_slot ORDER BY time_slot_id";
$TimeslotArr = $UserManagerObjAjax->HtmlOptionArrayCreate($timeslotSql);

$smarty->assign('Formval',$Formval);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('TimeslotArr',$TimeslotArr);
$smarty->display('timing_update.tpl');
include "footer.php";
?>
