<table id="points-presentation-layer">
	<tr>
		<td>
			<p>
				Select an points amount below to deposit. Please ensure you will return to the Chirongaming success page for your deposit to be processed properly.
			</p>
			<br/>
			<p>
			Chirongaming legal name will display as XXXXXX once you get on PayPal. You Do not need a PayPal account to make a deposit. PayPal accepts direct credit/debit card payments. 
			</p>
			<br/>
		</td>
		<td rowspan="2" id="paypal-logo">
			<h2>100% Secure</h2>
			<div>
				<img src="./images/paypal-logo.png" />
				<br/>
				<img src="./images/cards.png" />
			</div>
		</td>
	</tr>
	<tr>
		<td id="points-panel">
			
            <script>
                function a_onClick(n) {
            	    $("div.choice-clicked").removeClass("choice-clicked");
            		divSelected = $(n);
            		divSelected.addClass("choice-clicked");
            	}

            	function processTx(element, txUrl) {
            		theButton=jQuery(element);
            		theChoiceSelected = jQuery("div[id^=point_].choice-clicked").attr('id');
            		id= theChoiceSelected.replace("point_","");
            		params = {id:id};
            		
            		//jQuery(window.location).attr('href', txUrl + "?" + jQuery.param(params));
            		jQuery.ajax({
            			type:		"POST",
            			url:		txUrl,
            			data:		params,
            			async:		true,
            			success:	function( jqXHR, status) {
            							form = jQuery(jqXHR);
            							theButton.append(form);
            							form.submit();
            						}
            		});
            		
            	}
            </script>
            <div class="points-block" style="position: relative">
            	<span id="points-title" class="rounded-corners">
            		Choose the amount of points to add
            	</span>
            	<div class="points-wrapper" id="points-wrapper" style="position: absolute">
            		<div class="" >
            			<div id="point_0" class="points-item rounded-corners highlightit blue" onClick="a_onClick(this)">
            				<span class="point-value" >10</span>
            			</div>
            			<div id="point_1" class="points-item rounded-corners highlightit blue" onClick="a_onClick(this)">
            				<span class="point-value" >25</span>
            			</div>
            			<div id="point_2" class="points-item rounded-corners highlightit blue" onClick="a_onClick(this)">
            				<span class="point-value" >50</span>
            			</div>
            			<div id="point_3" class="points-item rounded-corners highlightit blue" onClick="a_onClick(this)">
            				<span class="point-value" >100</span>
            			</div>
            			<div class="clear"></div>
            		</div>
            		<div>
            			<a href="javascript:;" id="points-buy-button" class="button" onClick="processTx(this, './paypal.php')" >Buy Now</a>
            		</div>
            	</div>											
            </div>
            
            <script>
            	function setUpLayout(){
            		var width = $("#points-wrapper").width();
            		$("#points-wrapper").width(width+10);
            		$("#points-wrapper").css("left", "50%")
            		$("#points-wrapper").css("margin-left", -1*(width/2));
            	}
            	setUpLayout();					
            </script>
            
		</td>
	</tr>
</table>	

<!--https://www.paypal.com/cgi-bin/webscr-->
<!-- "https://www.sandbox.paypal.com/cgi-bin/webscr" -->
