<?php 
session_start();
error_reporting(1);

date_default_timezone_set('America/New_York');
include_once("common.php");
include('include/registerglobal2general.php');
include_once "include/functions.php";
include('include/user_smarty.php');
include("include/config.php");
include("include/general_functions.php");
include('class/class.phpmailer.php');
include('class/validation_class.php');
include('class/json.php');
include('class/usermanager_class.php');
include("class/user_sorting_class.php");
include("class/preserve_variable_class.php");
include("class/router.Class.php");
include('class/pagination_class_front_ajax.php');
include("class/pagination_class_ajax_front.php");
//echo VALIDATE_ACTIVE;
//echo $_SESSION['VALIDATE_ACTIVE'];
if(VALIDATE_ACTIVE==1){
include('checkvalidate.php');
}
$routerObj = new RouterCl($adodbcon);
$smarty->assign_by_ref('routerObj',$routerObj);

/* Status User Manager Object Creation Starts */
$UserManagerObjAjax=new UserManagerClass($adodbcon);
/* Status User Manager Object Creation Ends */

/* Preserve Variable Object Creation Starts */	
$preserve_variable_object=new PreserveVariableClass(TABLEPREFIX."_preserve_variable",$adodbcon);
/* Preserve Variable Object Creation Ends */

/* Sorting Object Creation Starts */	
$SortingObjAjax=new UserSortingClass;
/* Sorting Object Creation Ends */

//print_r($UserManagerObjAjax);

define('PAYPAL_ID', 1);
define('PAYPAL_URL', 2);
define('FACEBOOK_URL', 3);
define('TWITTER_ID', 4);
?>