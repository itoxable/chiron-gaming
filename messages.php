<?php
include("general_include.php");
include "checklogin.php";
$page_name = "messages.php";
$IsProcess = $_REQUEST['IsProcess'];
$user_id = $_SESSION['user_id'];

if($IsProcess!="Y")
    require_once('top.php');
else
	require_once("general_include.php");
	
$allmessagessql = "SELECT * FROM `frei_chat` WHERE `to` = ".$user_id." OR `from` = ".$user_id." ORDER BY `sent` DESC";
logToFile($allmessagessql);
$allmessages = $UserManagerObjAjax->GetRecords("All",$allmessagessql);
$messagesSize = count($allmessages);
$ids = "";
$idsArray = array();
$groupedMessages = array();
$groupedMessagesAux = array();
$lastMessage = $allmessages[0];
for($i=0; $i<$messagesSize; $i++){
    $message = $allmessages[$i];
	$to = $message['to'];
    $from = $message['from'];
    
    if($to != $user_id){
        if(!in_array($to, $idsArray)){
                $idsArray[]=$to;
        }
        $groupedMessagesAux["'".$to."'"][] = $message;
    }else if($from != $user_id){
        if(!in_array($from, $idsArray)){
                $idsArray[]=$from;
        }
        $groupedMessagesAux["'".$from."'"][] = $message;
    }	
}

$idsSize = count($idsArray);
for($i=0; $i<$idsSize; $i++){
    $id = $idsArray[$i];
    if($i == ($idsSize-1))
    	$ids = $ids.$id."";
	else
		$ids = $ids.$id.",";
		
	$groupedMessages["'".$id."'"] = array_reverse($groupedMessagesAux["'".$id."'"]);
	//$groupedMessages["'".$id."'"]['name'] = array_reverse($groupedMessagesAux["'".$id."'"]);
}
$groupedMessagesSize = count($groupedMessages);

$userssql = "SELECT * FROM `nk_user` WHERE `user_id` in (".$ids.") ORDER BY FIELD(`user_id`,".$ids.")";
$usersArr = $UserManagerObjAjax->GetRecords("All",$userssql);
$usersSize = count($usersArr);


$smarty->assign('lastId',$lastMessage['id']);
$smarty->assign('messagesSize',$messagesSize);
$smarty->assign('usersArr',$usersArr);
$smarty->assign('usersSize',$usersSize);
$smarty->assign('groupedMessages',$groupedMessages);
$smarty->assign('groupedMessagesSize',$groupedMessagesSize);
$smarty->assign('is_coach',$is_coach);
$smarty->assign('is_partner',$is_partner);

$smarty->display('messages.tpl');
if($IsProcess!="Y")
	include("footer.php");
?>