<?php
$page_name=basename($_SERVER['SCRIPT_FILENAME']);
$IsPreserved	= 'Y';
$IsProcess		= $_REQUEST['IsProcess'];
include "general_include.php";
include "checklogin.php";
if($_SESSION['user_type']=='2')
  header("location:coach_review.php");
if($IsProcess <> 'Y')
{
	include "top.php";
	//include "left.php";
}

$item_per_page=$_SESSION['item_per_page'] = $_REQUEST['item_per_page'];
	if(empty($item_per_page) || !isset($item_per_page))
	{
	$item_per_page=10;
	} 

$action	= $_REQUEST['action'];


// Start My Reviews 
  $myReview = "SELECT ur.*,u.* FROM ".TABLEPREFIX."_user_review ur,".TABLEPREFIX."_user u WHERE ur.user_id='".$_SESSION['user_id']."' AND ur.reviewed_by=u.user_id AND ur.is_active='Y' ORDER BY ur.date_added DESC";
 $PaginationObjAjaxLatest=new PaginationClassAjax($item_per_page,"prev",'',"next","active",$adodbcon);
 $pagination_arr = $PaginationObjAjaxLatest->PaginationAjax($myReview,$page_name."?action=".$action."&item_per_page=".$item_per_page,"Managergeneral");	
 $myReviewArr = $UserManagerObjAjax->GetRecords("All",$pagination_arr[0]);
$NummyReview = count($myReviewArr);
for($r=0;$r<$NummyReview;$r++)
{
    $dateSql = "SELECT date_format('".$myReviewArr[$r]['date_added']."','%M  %d , %Y at %h %p') as review_date";
	$DateArr = $UserManagerObjAjax->GetRecords("Row",$dateSql);
    $myReviewArr[$r]['review_date']=$DateArr['review_date'];
	
	$Usertype = "SELECT user_type_id FROM ".TABLEPREFIX."_user_type_user_relation WHERE user_id='".$myReviewArr[$r]['reviewed_by']."'";
	$typeArr = $UserManagerObjAjax->GetRecords("All",$Usertype);
	for($t=0;$t<count($typeArr);$t++)
	{
	  if($typeArr[$t][0] == 1)
	     $coach = 1;
	  if($typeArr[$t][0] == 3)
	     $partner = 1;	 
	}
	if($coach == 1)
	{ 
	   $myReviewArr[$r]['user_type']='Coach';
	   $myReviewArr[$r]['link']='coachdetails.php?coach_id='.$myReviewArr[$r]['reviewed_by'];
	}
	else if($partner == 1)
	{
	   $myReviewArr[$r]['user_type']='Training Partner';
	   $myReviewArr[$r]['link']='training_partner_details.php?training_partner_id='.$myReviewArr[$r]['reviewed_by'];
	}  
	else 
	{ 
	   $myReviewArr[$r]['user_type']='Student';
	   $myReviewArr[$r]['link']='#';
	}  
	
	
	$Sql_Individual="SELECT rat.rating, cat.rating_category FROM ".TABLEPREFIX."_user_rating AS rat, ".TABLEPREFIX."_user_review AS rev, ".TABLEPREFIX."_rating_category AS cat WHERE rat.rcat_id = cat.rcat_id AND rat.user_review_id = rev.user_review_id AND rat.user_review_id =".$myReviewArr[$r]['user_review_id'];
	$Individual =  $UserManagerObjAjax->GetRecords("All",$Sql_Individual);
	
	
	//$id=$UserReview[$r]['user_review_id'];
	for($k=0;$k<count($Individual);$k++){
	
	$Individual[$k]['rating']=$Individual[$k]['rating'];
	$Individual[$k]['rating_category']=$Individual[$k]['rating_category'];
	//$Individual[$r][$k]['rating_category_tooltip']=html_entity_decode($Individual[$k]['rating_category_tooltip']);
	
	
	
	}
	
	$myReviewArr[$r]['individual']=$Individual;
	
/*echo '<pre>';
print_r($UserReview_Individual);*/

}
/*echo '<pre>';
print_r($myReviewArr);
*/// End My Review


$smarty->assign('is_coach',$is_coach);
$smarty->assign('is_partner',$is_partner);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('NummyReview',$NummyReview);
$smarty->assign('myReviewArr',$myReviewArr);
$smarty->assign('page_name',$page_name);
$smarty->assign('Individual',$Individual);

$smarty->display('my_review.tpl');
if($IsProcess<>'Y')
  include "footer.php";
?>
