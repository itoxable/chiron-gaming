<?	

	class DocumentClass

	{				

		var $ERROR = "";

		

		function clear_error()

		{

			$this->ERROR = "";

		}

		

		

		function UploadDocument($DocRadio,$dochidden1,$http_doc_name,$http_doc_temp)

		{

			/* Document upload starts */			

			

			if($DocRadio == "Keep_Doc")

			{

				$docc1=$dochidden1;

			}

			if($DocRadio == "Remove_Doc")

			{

				$docc1="";

				if(!empty($dochidden1))

				{

					$filep=$dochidden1;					

					@unlink("../uploaded/docs/".$filep);					

					

				}

			}

			if(($DocRadio == "Change_Doc" || $DocRadio == ""))

			{

					$docc1 = $http_doc_name;

					#####################################

					

					$temp = $http_doc_temp;

					$Doccs = $http_doc_name;

					$Doccs = ereg_replace(" ", "_", $Doccs);

					$Doccs = ereg_replace("%20", "_", $Doccs);

					$Doccs = preg_replace("/[^-\._a-zA-Z0-9]/", "", $Doccs);

					$uniq = uniqid("");

					$docc1 = $uniq."_".$Doccs;

					$upload  = "../uploaded/docs/".$docc1;

					copy($temp, $upload);

					

					if(!empty($dochidden1))

					{

						$filep=$dochidden1;

						@unlink("../uploaded/docs/".$filep);

					}					

											

			}

			

			return $docc1;

			/* Document upload ends */



			

		}

				

		

		

		/* Checking of Document Starts */

		function CheckDocument($DocRadio,$http_doc_name,$http_doc_temp,$http_doc_type,$http_doc_size,$checktype=1,$Doclebel="",$mandatory="N",$bytesize=3)

		{

				if(!empty($Doclebel))

					$Doclebel =" for <span class=red>".$Doclebel."</span>";

				if($mandatory=="Y")

				{

					if($DocRadio == "Remove_Doc")

					{

						$err_msgs .= "<li>Document should not be removed $Doclebel!</li>";

					}

				}	

				if ($DocRadio == "Change_Doc" || $DocRadio == "")

				{

						$docc1 = $http_doc_name;

																								

						if(!empty($docc1)) ## CHECKING IMAGE

						{

							$doc_size=$http_doc_size;

							$doc_type=$http_doc_type;

							$temp = $http_doc_temp;

							

							if(empty($bytesize))

								$bytesize=3;

							$size_limit=$bytesize*1048576;  /* set size limit in Bytes  */								

							

							if($checktype==3)

								$allowed_types = array("application/msword","application/pdf","application/force-download","application/rtf","application/x-rtf","text/rtf","text/richtext","application/doc","application/x-soffice","application/vnd.ms-excel");

							else if($checktype==2)

								$allowed_types = array("application/msword","application/pdf","application/doc","application/vnd.ms-excel ");

							else if($checktype==4)

								$allowed_types = array("application/pdf");

							else

								$allowed_types = array("application/msword","application/pdf","application/doc");							

																		

							if($doc_size!="0")

							{

								if ($doc_size < $size_limit)

								{									

									if (!in_array($doc_type,$allowed_types))

									{

										$err_msgs .= "<li>Files of type <b>$doc_type</b> are not allowed to be uploaded!</li>";

									}									

								}

								else

								{

									$err_msgs .= "<li>The file <span class=red>$docc1</span> you selected to upload is too large to be uploaded $Doclebel!</li>";

								}

							 }

							 else if($doc_size=="0")

							{

								$err_msgs .= "<li>The file <span class=red>$docc1</span> was not found $Doclebel!</li>";

							}

						} ## END OF CHECKING IMAGE

						

						if($mandatory=="Y")

						{

							

							if( ($DocRadio == "Change_Doc" || $DocRadio == "" ) && empty($docc1))

							{

								$err_msgs .= "<li>No Document uploaded $Doclebel!</li>";

							}

							

						}

						elseif($mandatory=="N")

						{

							if ($DocRadio == "Change_Doc" && empty($docc1))

							{

								$err_msgs .= "<li>No Document uploaded $Doclebel!</li>";

							}

						}

				} 

				

				return $err_msgs;

		}

		/* Checking of Document Ends */

		

		/* Echo Document Instruction Note starts */

		function EchoInstructionDoc($instuctiontype="",$docsize=10)

		{

			$docsize=10;

			if(empty($instuctiontype)||$instuctiontype==1)

			{

				return "<b>Upload doc,pdf files. Upload file should not exceed ".$docsize."Mb.</b>";				

			}	

			elseif($instuctiontype==2)

			{

				return "<b>Upload doc,pdf files. Upload file should not exceed ".$docsize."Mb.</b>";				

			}

			elseif($instuctiontype==3)

			{

				return "<b>Upload only pdf file. Upload file should not exceed ".$docsize."Mb.</b>";				

			}		

		}

		/* Echo Document Instruction Note Ends */

		

		/* Display of Upload Control starts */

		function DisplayDocumentControl($doc_lebel_name,$doc_control_name,$doc_control_val,$doc_hidden_name,$doc_radio_name,$mandatory="N",$display_doc="Y",$instuctiontype=1,$docsize=3)

		{

			$DisplayDocumentControlStr="";

			//$DisplayDocumentControlStr.="";

			

			$DisplayDocumentControlStr.="<tr>

				<td class='plaintxt' valign='top'>$doc_lebel_name";

				if($mandatory=="Y") {$DisplayDocumentControlStr.="<span class='red'>*</span>";}

			$DisplayDocumentControlStr.=":</td>

							<td valign='top' class='plaintxt'>";		

						

			if (strlen($doc_control_val)!=0 && file_exists("../uploaded/docs/".$doc_control_val))

				{																									

					if($display_doc=="Y")

					{

					$DocPath="../uploaded/docs/".$doc_control_val;

					$DisplayDocumentControlStr.="

					<a href='$DocPath' class='links' target='_blank'>".substr($doc_control_val,14)."</a><BR>";

					}

					$DisplayDocumentControlStr.="

					<input type='radio' name='$doc_radio_name' value='Keep_Doc' checked >

					<span class='maintext'>Keep Document</span>"; 

					if($mandatory!="Y")

					{

					$DisplayDocumentControlStr.="

					<input type='radio' name='$doc_radio_name' value='Remove_Doc' >

					<span class='maintext'>Remove Document</span>";

					}

					$DisplayDocumentControlStr.="

					<input type='radio' name='$doc_radio_name' value='Change_Doc'>

					<span class='maintext'>Change Document</span><br>

					<INPUT type='hidden' name='$doc_hidden_name' value='$doc_control_val'>";

				}			

			$DisplayDocumentControlStr.="

				<INPUT type='file' name='$doc_control_name' class='form' size='30'>

				</td></tr><tr>

											<td class='plaintxt'>&nbsp;</td>	

											<td valign='top' class='maintext' align='left'>				

											".$this->EchoInstructionDoc($instuctiontype,$docsize=3)."							

											</td>		

										</tr>

				";													

			return $DisplayDocumentControlStr;			

				

		}

		/* Display of Upload Control Ends */

		

		/* Display of document at front Starts */

		

		function DisplayDocumentFront($DocumentFile,$DocumentPath,$DownloadType="normal",$DocumentTitle="Document")

		{

			if(substr($DocumentFile,-3)=="doc" || substr($DocumentFile,-3)=="DOC")

				$display_image_path="images/word_icon.gif";

			elseif(substr($DocumentFile,-3)=="pdf" || substr($DocumentFile,-3)=="PDF")

				$display_image_path="images/pdficon.gif";

			elseif(substr($DocumentFile,-3)=="xls" || substr($DocumentFile,-3)=="XLS")

				$display_image_path="images/xl_icon.gif";

			else

				$display_image_path="images/download_icon.gif";

			

			if($DownloadType=="typical")

				$hreftext="file_download.php?filename=".$DocumentPath."/".$DocumentFile;	

			else

				$hreftext=$DocumentPath."/".$DocumentFile;

					

		

		$return_show="

					<table border='0' cellspacing='0' cellpadding='5' width='100%'>

						<tr>

							<td width='8%' align='left'>

							<a href='$hreftext' class='small' title='Download $DocumentTitle' style='cursor:hand'>	

							<img src='$display_image_path' align='left' border='0' title='Download $DocumentTitle'>

							</a>

							</td>

							<td width='92%' nowrap>

							<a href='$hreftext' class='maintext' title='Download $DocumentTitle' style='cursor:hand'>".substr($DocumentFile,14)."</a>

							</td>				

						</tr>

					</table>";		

			return $return_show;									

		}

		/* Display of document at front Ends */

	

	}

?>