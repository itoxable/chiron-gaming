<?	
	class VideoClass
	{							
		var $ERROR = "";
		
		function clear_error()
		{
			$this->ERROR = "";
		}	
					
		function SetVideoPath($video_path)
		{			
			$this->video_path=$video_path;													
		}
		
		function UploadVideo($VideoRadio,$videohidden1,$http_video_name,$http_video_temp)
		{
			/* Video upload starts */															
			
			if($VideoRadio == "Keep_Media")
			{
				$videoc1=$videohidden1;
			}
			if($VideoRadio == "Remove_Media")
			{
				$videoc1="";
				if(!empty($videohidden1))
				{
					$filep=$videohidden1;					
					@unlink("../".$this->video_path."/".$filep);					
					
				}
			}
			if(($VideoRadio == "Change_Media" || $VideoRadio == ""))
			{
					$videoc1 = $http_video_name;
					#####################################
					
					$temp = $http_video_temp;
					$Videocs = $http_video_name;
					$Videocs = ereg_replace(" ", "_", $Videocs);
					$Videocs = ereg_replace("%20", "_", $Videocs);
					$Videocs = preg_replace("/[^-\._a-zA-Z0-9]/", "", $Videocs);
					$uniq = uniqid("");
					$videoc1 = $uniq."_".$Videocs;
					$upload  = "../".$this->video_path."/".$videoc1;
					@copy($temp, $upload);
					
					if(!empty($videohidden1))
					{
						$filep=$videohidden1;
						@unlink("../".$this->video_path."/".$filep);
					}											
			}
			
			return $videoc1;
			/* Video upload ends */
		}
				
		/* Checking of Video Starts */
		function CheckVideo($VideoRadio,$http_video_name,$http_video_temp,$http_video_type,$http_video_size,$Videolebel="",$mandatory="N",$bytesize=10)
		{
				if(!empty($Videolebel))
					$Videolebel =" for <span class=red>".$Videolebel."</span>";
				if($mandatory=="Y")
				{
					if($VideoRadio == "Remove_Media")
					{
						$err_msgs .= "<li>Media should not be removed $Videolebel!</li>";
					}
				}	
				if ($VideoRadio == "Change_Media" || $VideoRadio == "")
				{
						$videoc1 = $http_video_name;
																								
						if(!empty($videoc1)) ## CHECKING IMAGE
						{
							$video_size=$http_video_size;
							$video_type=$http_video_type;
							$temp = $http_video_temp;
							$video_extn_arr = explode(".",$videoc1);
							$video_extn = $video_extn_arr[count($video_extn_arr)-1];
							$video_type = $video_extn;
							
							if(empty($bytesize))
								$bytesize=10;
							$size_limit=$bytesize*1048576;  /* set size limit in Bytes  */								
														
							//$allowed_types = array("video/avi","audio/x-avi","video/mpeg","video/x-mpeg","mpg","video/x-wav");
							$allowed_types = array("avi","AVI","mpeg","MPEG","mpg","MPG","wav","WAV");							
																		
							if($video_size!="0")
							{
								if ($video_size < $size_limit)
								{									
									if (!in_array($video_type,$allowed_types))
									{
										$err_msgs .= "<li>Files of type <b>$video_type</b> are not allowed to be uploaded $Videolebel!</li>";
									}									
								}
								else
								{
									$err_msgs .= "<li>The file <span class=red>$videoc1</span> you selected to upload is too large to be uploaded $Videolebel!</li>";
								}
							 }
							 else if($video_size=="0")
							{
								$err_msgs .= "<li>The file <span class=red>$videoc1</span> was not found $Videolebel!</li>";
							}
						} ## END OF CHECKING IMAGE
						
						if($mandatory=="Y")
						{
							
							if( ($VideoRadio == "Change_Media" || $VideoRadio == "" ) && empty($videoc1))
							{
								$err_msgs .= "<li>No Video uploaded $Videolebel!</li>";
							}
							
						}
						elseif($mandatory=="N")
						{
							if ($VideoRadio == "Change_Media" && empty($videoc1))
							{
								$err_msgs .= "<li>No Media uploaded $Videolebel!</li>";
							}
						}
				} 
				
				return $err_msgs;
		}
		/* Checking of Video Ends */
		
		/* Echo Video Instruction Note starts */
		function EchoInstructionVideo($videosize=10)
		{			
			return "<b>Upload only .avi, .mpeg, .wmv files. Maximum file size - $videosize MB.</b>";									
		}
		/* Echo Video Instruction Note Ends */
		
		/* Display of Upload Control starts */
		
		function DisplayVideoControl($video_lebel_name,$video_control_name,$video_control_val,$video_hidden_name,$video_radio_name,$mandatory="N",$display_video="Y",$videosize=10)
		{
			$DisplayVideoControlStr="";
			$DisplayVideoControlStr .=
			"<tr>
				<td valign='top' class='maintext' colspan='2' align='right'>
				".$this->EchoInstructionVideo($videosize)."
				</td>		
			</tr>
			<tr>
				<td class='maintext' valign='top'>$video_lebel_name<span class='red' id='check_doc'></span>:";
				 if($mandatory=='Y') {$DisplayVideoControlStr .="<span class='red' id='doc_up'>*</span>"; }
			$DisplayVideoControlStr .=
				"</td>
			<td valign='top' class='plaintxt'>";
			
				if (strlen($video_control_val)!=0 && file_exists('../'.$this->video_path.'/'.$video_control_val))
				{																									
					
					$DisplayVideoControlStr .= substr($video_control_val,14).'<br>';
					$DisplayVideoControlStr .="					
					<input type='radio' name='$video_radio_name' value='Keep_Media' checked >
					<span class='maintext'>Keep Media</span>"; 
					if($mandatory!='Y')
					{
						$DisplayVideoControlStr .="
						<input type='radio' name='$video_radio_name' value='Remove_Media' >
						<span class='maintext'>Remove Media</span>
						";
					}
					$DisplayVideoControlStr .="
					<input type='radio' name='$video_radio_name' value='Change_Media'>
					<span class='maintext'>Change Media</span><br>					
					<INPUT type='hidden' name='$video_hidden_name' value='$video_control_val'>
					";
				}
			$DisplayVideoControlStr .="
				<INPUT type='file' name='$video_control_name' class='form' size='30'>				
				</td>
			</tr>
			";
			return  $DisplayVideoControlStr;
		}		
		/* Display of Upload Control Ends */
				
	
	}
?>