<?php
#include_once('class/TABLE_SQL.php');
Class RouterCl
{
	var $adodbcon;
	var $urlType;
	var $istext;
	var $sportsTable;
	var $options=array('route','cms');
		 /**
     * Constructor of the Betting_Router class
     *
     * @param  string Connection data or DB object
     * @return object Returns an error object if something went wrong
     */

	function RouterCl($adodbcon)
	{
		
		$this->adodbcon = $adodbcon;
		$this->conMode = $mode;
	}
		/* 
	 * Fetching url type
     *

     *
	 * @access public
     * @return  mixed  Error object or string 
	*/

	function getURLType()
	{
		return $this->getRecords("One","SELECT setting_value FROM ".TABLEPREFIX."_settings WHERE setting_id='1'");
	}
	function getIsText()
	{
		return $this->getRecords("One","SELECT setting_order FROM ".TABLEPREFIX."_settings WHERE setting_id='1'");
	}
		/* 
	 * Rewrite the url
     *
     * This function uses the given opt to construct
     * the url.
     *
	 * @access public
	 * @param   string  opt 
	 * @param   Integer  id 
	 * @param   Integer  sId 
     * @return  mixed  Error object or string 
	*/

	function router($opt,$id,$pId=0,$ref=0)
	{
		$this->urlType=$this->getURLType();
		$this->istext=$this->getIsText();
		if($opt == $this->options[0])
		{
		 if($this->istext=="Y")
		 {
		  return str_replace("_","-",$id).'.php';
		 }
		 else
		 {
			switch($this->urlType){
				case 'Y':
					return  str_replace("_","-",$id).'.html';
				break;
				case 'N':
					return $id.'.php';
				break;
			}
		  }
		}
		
	if($opt == $this->options[1])
		{
			$aboutTitle = $this->getCmsTitle($id,"Y");
			
			switch($this->urlType){
				case 'Y':
					return $aboutTitle.'.html';
				break;
				case 'N':
					return 'content.php?seo_link='.$aboutTitle;
				break;
				}
		}

	
	}
	
	function getCmsTitle($Id,$formatText='Y')
	{
		$seoTitle = $this->getRecords("One","SELECT seo_link FROM ".TABLEPREFIX."_cms WHERE cms_id=".$Id);
		if($formatText=='Y')
			return $this->seoFormat(strtolower(stripslashes($seoTitle)));
		else
			return $seoTitle;
	}

	
	function seoFormat($title)
	{
		$remove_arr = array("_","-",".","|","~","!","@","#","$","%","^","&","*","-","+","(",")","=","{","}","[","]",":",";","'","\"","<",">",",","?","/","\\","`");
		$text = ucwords($title);
		$text = trim($text);
		$text=str_replace(" ","",$text);
		foreach($remove_arr as $val)
		{
			$text=str_replace($val,"-",$text);
		}		
		$text=trim($text);
		return $text;
	}
	function getRecords($GetType,$SelectSql)
	{				
		
		if($GetType=="One")				
			$RsRecords=$this->adodbcon->GetOne($SelectSql);		
		elseif($GetType=="Row")				
			$RsRecords=$this->adodbcon->GetRow($SelectSql);	
		elseif($GetType=="Col")				
			$RsRecords=$this->adodbcon->GetCol($SelectSql);	
		elseif($GetType=="All")				
			$RsRecords=$this->adodbcon->GetAll($SelectSql);					
			
		return $RsRecords;														
	}	
}
?>