<?	

	class ChangeStatusClass

	{				

		var $ERROR = "";

		var $StatusTableName;		

				

		function clear_error()

		{

			$this->ERROR = "";

		}

		function ChangeStatusClass($StatusTableName,$adodbcon)

		{			

			$this->StatusTableName=$StatusTableName;

			$this->adodbcon=$adodbcon;															

		}				

		function ChangeStatus($record_id,$change_status_id)

		{		

			$ChangeStatusType=$this->adodbcon->GetOne("SELECT change_status_type FROM ".$this->StatusTableName." WHERE change_status_id='$change_status_id'");

			$TableName=$this->adodbcon->GetOne("SELECT table_name FROM ".$this->StatusTableName." WHERE change_status_id='$change_status_id'");

			$TableName=TABLEPREFIX.$TableName;

			$Pkey=$this->adodbcon->GetOne("SELECT pkey FROM ".$this->StatusTableName." WHERE change_status_id='$change_status_id'");

			$ChangeStatusName=$this->adodbcon->GetOne("SELECT change_status_name FROM ".$this->StatusTableName." WHERE change_status_id='$change_status_id'");	

			$ChangeOtherStatusName=$this->adodbcon->GetOne("SELECT other_status_name FROM ".$this->StatusTableName." WHERE change_status_id='$change_status_id'");	

			$IsOtherStatus=$this->adodbcon->GetOne("SELECT is_other_status FROM ".$this->StatusTableName." WHERE change_status_id='$change_status_id'");	

			$IsOtherStatusMode=$this->adodbcon->GetOne("SELECT is_other_status_mode FROM ".$this->StatusTableName." WHERE change_status_id='$change_status_id'");	

			$ChangeOtherStatusDependency=$this->adodbcon->GetOne("SELECT other_status_dependency FROM ".$this->StatusTableName." WHERE change_status_id='$change_status_id'");				

			$ChangeOtherStatusType=$this->adodbcon->GetOne("SELECT other_status_type FROM ".$this->StatusTableName." WHERE change_status_id='$change_status_id'");	

			

			

			/* Default Setting Starts */

			if($ChangeStatusType=="Default")

			{	

				$this->adodbcon->StartTrans();				

				$upt_default_all = " UPDATE ".$TableName." SET $ChangeStatusName='N'";	

				$this->adodbcon->Execute($upt_default_all);

				if($IsOtherStatus=="Y")

				{

					if($ChangeOtherStatusDependency=="TO")

					{

						$SelectOtherStatus=$this->adodbcon->GetOne("SELECT $ChangeOtherStatusName from ".$TableName."  WHERE ".$Pkey."='$record_id' ");

						if($ChangeOtherStatusType=="OPPOSITE")						

							$ChangeOtherStatusValue=($SelectOtherStatus=="Y")?"N":"Y";						

						else						

							$ChangeOtherStatusValue=($SelectOtherStatus=="Y")?"Y":"N";	

					}

					else

					{

						$ChangeOtherStatusValue=($ChangeOtherStatusType=="OPPOSITE")?"N":"Y";													

					}

					$upt_default_one = " UPDATE ".$TableName." SET $ChangeStatusName='Y',$ChangeOtherStatusName='$ChangeOtherStatusValue'  WHERE ".$Pkey."='$record_id'";	

				}

				else

				$upt_default_one = " UPDATE ".$TableName." SET $ChangeStatusName='Y'  WHERE ".$Pkey."='$record_id'";	

				$this->adodbcon->Execute($upt_default_one);	

				$this->adodbcon->CompleteTrans();

			}		

			/* Default Setting Ends */

			

			/* Activate Setting Starts */

			if($ChangeStatusType=="Activate")

			{

				$SelectStatus=$this->adodbcon->GetOne("SELECT $ChangeStatusName from ".$TableName."  WHERE ".$Pkey."='$record_id' ");				

				$ChangeToStatus=$SelectStatus=='Y'?'N':'Y';

				if($IsOtherStatus=="Y")

				{					

					if($ChangeOtherStatusDependency=="TO")

					{						

						$SelectOtherStatus=$this->adodbcon->GetOne("SELECT $ChangeOtherStatusName from ".$TableName."  WHERE ".$Pkey."='$record_id' ");

						if($ChangeOtherStatusType=="OPPOSITE")						

							$ChangeOtherStatusValue=($SelectOtherStatus=="Y")?"N":"Y";						

						else						

							$ChangeOtherStatusValue=($SelectOtherStatus=="Y")?"Y":"N";	

						if($IsOtherStatusMode==$ChangeOtherStatusValue || $IsOtherStatusMode=="B")	

						$UpdateStatus="UPDATE ".$TableName."  SET $ChangeStatusName='$ChangeOtherStatusValue' WHERE ".$Pkey."='$record_id' ";

						else

						$UpdateStatus="UPDATE ".$TableName."  SET $ChangeStatusName='$ChangeToStatus' WHERE ".$Pkey."='$record_id' ";

						

					}

					else

					{

						if($ChangeOtherStatusType=="OPPOSITE")						

							$ChangeOtherStatusValue=($SelectStatus=="Y")?"N":"Y";						

						else						

							$ChangeOtherStatusValue=($SelectStatus=="Y")?"Y":"N";	

						$UpdateStatus="UPDATE ".$TableName."  SET $ChangeStatusName='$ChangeToStatus',$ChangeOtherStatusName='$ChangeOtherStatusValue' WHERE ".$Pkey."='$record_id' ";	

																		

					}

					

				}

				else

				$UpdateStatus="UPDATE ".$TableName."  SET $ChangeStatusName='$ChangeToStatus' WHERE ".$Pkey."='$record_id' ";

				$RsUpdateStatus=$this->adodbcon->Execute($UpdateStatus); 	

									

			}		

			if($ChangeStatusType=="Featured")

			{

				$SelectStatus=$this->adodbcon->GetOne("SELECT $ChangeStatusName from ".$TableName."  WHERE ".$Pkey."='$record_id' ");				

				$ChangeToStatus=$SelectStatus=='Y'?'N':'Y';

				if($IsOtherStatus=="Y")

				{					

					if($ChangeOtherStatusDependency=="TO")

					{						

						$SelectOtherStatus=$this->adodbcon->GetOne("SELECT $ChangeOtherStatusName from ".$TableName."  WHERE ".$Pkey."='$record_id' ");

						if($ChangeOtherStatusType=="OPPOSITE")						

							$ChangeOtherStatusValue=($SelectOtherStatus=="Y")?"N":"Y";						

						else						

							$ChangeOtherStatusValue=($SelectOtherStatus=="Y")?"Y":"N";	

						if($IsOtherStatusMode==$ChangeOtherStatusValue || $IsOtherStatusMode=="B")	

						$UpdateStatus="UPDATE ".$TableName."  SET $ChangeStatusName='$ChangeOtherStatusValue' WHERE ".$Pkey."='$record_id' ";

						else

						$UpdateStatus="UPDATE ".$TableName."  SET $ChangeStatusName='$ChangeToStatus' WHERE ".$Pkey."='$record_id' ";

						

					}

					else

					{

						if($ChangeOtherStatusType=="OPPOSITE")						

							$ChangeOtherStatusValue=($SelectStatus=="Y")?"N":"Y";						

						else						

							$ChangeOtherStatusValue=($SelectStatus=="Y")?"Y":"N";	

						$UpdateStatus="UPDATE ".$TableName."  SET $ChangeStatusName='$ChangeToStatus',$ChangeOtherStatusName='$ChangeOtherStatusValue' 
						WHERE ".$Pkey."='$record_id' ";	

																		

					}

					

				}

				else

				$UpdateStatus="UPDATE ".$TableName."  SET $ChangeStatusName='$ChangeToStatus' WHERE ".$Pkey."='$record_id' ";

				$RsUpdateStatus=$this->adodbcon->Execute($UpdateStatus); 	

									

			}

			/* Activate Setting Ends */

		}		

				

	}	

?>