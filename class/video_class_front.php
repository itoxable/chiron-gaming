<?	
	class VideoClass
	{							
		var $ERROR = "";
		
		function clear_error()
		{
			$this->ERROR = "";
		}	
					
		function SetVideoPath($video_path)
		{			
			$this->video_path=$video_path;													
		}
		
		function UploadVideo($VideoRadio,$videohidden1,$http_video_name,$http_video_temp)
		{
			/* Video upload starts */															
			
			if($VideoRadio == "Keep_Media")
			{
				$videoc1=$videohidden1;
			}
			if($VideoRadio == "Remove_Media")
			{
				$videoc1="";
				if(!empty($videohidden1))
				{
					$filep=$videohidden1;					
					@unlink("".$this->video_path."/".$filep);					
					
				}
			}
			if(($VideoRadio == "Change_Media" || $VideoRadio == ""))
			{
					$videoc1 = $http_video_name;
					#####################################
					
					$temp = $http_video_temp;
					$Videocs = $http_video_name;
					$Videocs = ereg_replace(" ", "_", $Videocs);
					$Videocs = ereg_replace("%20", "_", $Videocs);
					$Videocs = preg_replace("/[^-\._a-zA-Z0-9]/", "", $Videocs);
					$uniq = uniqid("");
					$videoc1 = $uniq."_".$Videocs;
					$upload  = "".$this->video_path."/".$videoc1;
					@copy($temp, $upload);
					
					if(!empty($videohidden1))
					{
						$filep=$videohidden1;
						@unlink("".$this->video_path."/".$filep);
					}											
			}
			
			return $videoc1;
			/* Video upload ends */
		}
				
		/* Checking of Video Starts */
		function CheckVideo($VideoRadio,$http_video_name,$http_video_temp,$http_video_type,$http_video_size,$Videolebel="",$mandatory="N",$bytesize=10)
		{
				if(!empty($Videolebel))
					$Videolebel =" for <span class=red>".$Videolebel."</span>";
				if($mandatory=="Y")
				{
					if($VideoRadio == "Remove_Media")
					{
						$ermsg = "Media should not be removed $Videolebel!";
					}
				}	
				if ($VideoRadio == "Change_Media" || $VideoRadio == "")
				{
						$videoc1 = $http_video_name;
																								
						if(!empty($videoc1)) ## CHECKING IMAGE
						{
							$video_size=$http_video_size;
							$video_type=$http_video_type;
							$temp = $http_video_temp;
							$video_extn_arr = explode(".",$videoc1);
							$video_extn = $video_extn_arr[count($video_extn_arr)-1];
							$video_type = $video_extn;
							
							if(empty($bytesize))
								$bytesize=10;
							$size_limit=$bytesize*1048576;  /* set size limit in Bytes  */								
														
							//$allowed_types = array("video/avi","audio/x-avi","video/mpeg","video/x-mpeg","mpg","video/x-wav");
							$allowed_types = array("avi","AVI","mpeg","MPEG","mpg","MPG","wav","WAV");							
																		
							if($video_size!="0")
							{
								if ($video_size < $size_limit)
								{									
									if (!in_array($video_type,$allowed_types))
									{
										$ermsg = "Files of type <b>$video_type</b> are not allowed to be uploaded $Videolebel!";
									}									
								}
								else
								{
									$ermsg = "The file <span class=red>$videoc1</span> you selected to upload is too large to be uploaded $Videolebel!";
								}
							 }
							 else if($video_size=="0")
							{
								$ermsg = "The file <span class=red>$videoc1</span> was not found $Videolebel!";
							}
						} ## END OF CHECKING IMAGE
						
						if($mandatory=="Y")
						{
							
							if( ($VideoRadio == "Change_Media" || $VideoRadio == "" ) && empty($videoc1))
							{
								$ermsg .= "No Video uploaded $Videolebel!";
							}
							
						}
						elseif($mandatory=="N")
						{
							if ($VideoRadio == "Change_Media" && empty($videoc1))
							{
								$ermsg = "No Media uploaded $Videolebel!";
							}
						}
				} 
				
				return $err_msgs;
		}
		/* Checking of Video Ends */
		
		/* Echo Video Instruction Note starts */
		function EchoInstructionVideo($videosize=10)
		{			
			return "<em>(.avi, .mpeg, .wmv files. Max size - $videosize MB)</em>";									
		}
		/* Echo Video Instruction Note Ends */
		
		/* Display of Upload Control starts */
		
		function DisplayVideoControl($video_lebel_name,$video_control_name,$video_control_val,$video_hidden_name,$video_radio_name,$mandatory="N",$display_video="Y",$videosize=10)
		{
			$DisplayVideoControlStr="";
			
			
				if (strlen($video_control_val)!=0 && file_exists(''.$this->video_path.'/'.$video_control_val))
				{																									
					
					$DisplayVideoControlStr .= "<label>&nbsp;</label>".substr($video_control_val,14);
					$DisplayVideoControlStr .="<p class='clear'></p>
					<label>&nbsp</label>
					<input type='radio' name='$video_radio_name' class='check' value='Keep_Media' checked >
					<label class='chkoption'>Keep Media</label>"; 
					if($mandatory!='Y')
					{
						$DisplayVideoControlStr .="
						<input type='radio' name='$video_radio_name' class='check' value='Remove_Media' >
						<label class='chkoption'>Remove Media</label>";
					}
					$DisplayVideoControlStr .="
					<input type='radio' name='$video_radio_name' class='check' value='Change_Media'>
					<label class='chkoption'>Change Media</label>
					<p class='clear'></p>				
					<INPUT type='hidden' name='$video_hidden_name' value='$video_control_val'>";
				}
				$DisplayVideoControlStr .=
			"<label>$video_lebel_name :</label>
			<span class='browse'><input type='file' name='$video_control_name' id='$video_control_name'";
			if($mandatory=='Y') {$DisplayVideoControlStr .="class='con-req'";}
			$DisplayVideoControlStr .="></span>";
			$DisplayVideoControlStr .="<label class='instruction'>".$this->EchoInstructionVideo($videosize)."</label>
			<p class='clear'></p>";	
/*			$DisplayVideoControlStr .="<p class='clear'></p>
			<div>".$this->EchoInstructionVideo($videosize)."</div>
			<p class='clear'></p>";
*/			return  $DisplayVideoControlStr;
		}		
		/* Display of Upload Control Ends */
				
	
	}
?>
