<?	

	class RankingClass

	{				

		var $ERROR = "";

		var $RankingTableName;	

		var $adodbcon;	

		var $ImagePath="";

		var $ImageUp="";

		var $ImageDown="";

		var $page_id=0;

		var $page_name="";		

		var $MaxOrderNo=0;			

		var $MinOrderNo=0;		

		function clear_error()

		{

			$this->ERROR = "";

		}

		function RankingClass($RankingTableName,$ImagePath,$ImageUp,$ImageDown,$adodbcon)

		{			

			$this->RankingTableName=$RankingTableName;

			$this->ImagePath=$ImagePath;

			$this->ImageUp=$ImageUp;

			$this->ImageDown=$ImageDown;

			$this->adodbcon=$adodbcon;																		

		}	

		

		/* Set Page ID Starts */

		function SetPageDetails($page_name,$page_id)

		{

			$this->page_name=$page_name;

			$this->page_id=$page_id;

		}

		/* Set Page ID Ends */

					

		function Ranking($to,$id,$is_cat_id="N",$cat_id="",$is_other_id="N",$other_param="")

		{				

		

			$TableName=$this->adodbcon->GetOne("SELECT table_name FROM ".$this->RankingTableName." WHERE page_id='".$this->page_id."'");

			$TableName=TABLEPREFIX.$TableName;

			$Pkey=$this->adodbcon->GetOne("SELECT pkey FROM ".$this->RankingTableName." WHERE page_id='".$this->page_id."'");

			$PageName=$this->adodbcon->GetOne("SELECT page_name FROM ".$this->RankingTableName." WHERE page_id='".$this->page_id."'");

			$OrderName=$this->adodbcon->GetOne("SELECT order_name FROM ".$this->RankingTableName." WHERE page_id='".$this->page_id."'");

			$CategoryIDName=$this->adodbcon->GetOne("SELECT category_id_name FROM ".$this->RankingTableName." WHERE page_id='".$this->page_id."'");			

			/* Ranking Up Starts */

			if($to=='dn')

			{

				$query_pageorder=" SELECT ".$OrderName." FROM ".$TableName." WHERE ".$Pkey."='$id'";

				if($is_cat_id=="Y")

					$query_pageorder.=" AND $CategoryIDName='".$cat_id."'";	
													
				if($is_other_id=="Y")

					$query_pageorder.=" AND $other_param";
					
				$pageOrder=$this->adodbcon->GetOne($query_pageorder);				

			

				$query_pageid=" SELECT $Pkey FROM ".$TableName." WHERE $OrderName=($pageOrder+1)";				

				if($is_cat_id=="Y")

					$query_pageid.=" AND $CategoryIDName='".$cat_id."'";			
				
				if($is_other_id=="Y")

					$query_pageid.=" AND $other_param";
					
				$pageid=$this->adodbcon->GetOne($query_pageid);				

			

				$upt_pageorder = " UPDATE ".$TableName." SET $OrderName=($pageOrder+1) where ".$Pkey."='$id'";

				if($is_cat_id=="Y")

					$upt_pageorder.=" AND $CategoryIDName='".$cat_id."'";
				
				if($is_other_id=="Y")

					$upt_pageorder.=" AND $other_param";				
				
				$this->adodbcon->Execute($upt_pageorder);

			

				$upt_pageorder1 = " UPDATE ".$TableName." SET $OrderName=$pageOrder where ".$Pkey."=$pageid";

				if($is_cat_id=="Y")

					$upt_pageorder1.=" AND $CategoryIDName='".$cat_id."'";				

				$this->adodbcon->Execute($upt_pageorder1);

			

			}

			/* Ranking Up Ends */

			

			/* Ranking Down Starts */

			if($to == 'up')

			{

				$query_pageorder="SELECT ".$OrderName." from ".$TableName." where ".$Pkey."='$id'"; 

				if($is_cat_id=="Y")

					$query_pageorder.=" AND $CategoryIDName='".$cat_id."'";			
				
				if($is_other_id=="Y")

					$query_pageorder.=" AND $other_param";
					
				$pageOrder=$this->adodbcon->GetOne($query_pageorder);				

			

				$query_pageid="SELECT $Pkey from ".$TableName." where $OrderName=($pageOrder-1)"; 

				if($is_cat_id=="Y")

					$query_pageid.=" AND $CategoryIDName='".$cat_id."'";					
				
				if($is_other_id=="Y")

					$query_pageid.=" AND $other_param";
					
				$pageid=$this->adodbcon->GetOne($query_pageid);					

			

				$upt_pageorder = "UPDATE ".$TableName." SET $OrderName=($pageOrder-1) where ".$Pkey."='$id'";

				if($is_cat_id=="Y")

					$upt_pageorder.=" AND $CategoryIDName='".$cat_id."'";				
				
				if($is_other_id=="Y")

					$upt_pageorder.=" AND $other_param";
					
				$this->adodbcon->Execute($upt_pageorder);

			

				$upt_pageorder1 = "UPDATE ".$TableName." SET $OrderName=$pageOrder where ".$Pkey."=$pageid";

				if($is_cat_id=="Y")

					$upt_pageorder1.=" AND $CategoryIDName='".$cat_id."'";				
								
				$this->adodbcon->Execute($upt_pageorder1);
			
			}

			/* Ranking Down Ends */

			

		}		

		

		

		/* Set Ranking MaxOrderNo Starts */

		function SetRankingMaxOrderNo($MaxOrderSql)

		{

			$this->MaxOrderNo=$this->adodbcon->GetOne($MaxOrderSql);

		}

		function SetRankingMinOrderNo($MinOrderSql)

		{

			$this->MinOrderNo=$this->adodbcon->GetOne($MinOrderSql);

		}

		/* Set Ranking MaxOrderNo Ends */

		

		/* Ranking Row Create Starts */

		function RankingRowCreate($OrderValue,$record_id,$OtherParameter)

		{

			global $from;

			$ImgRank="";

			if($OrderValue==1)

			{

				if($OrderValue==$this->MaxOrderNo)

					$ImgRank="--";

				else

				$ImgRank="<table border=0 cellspacing=0 cellpadding=0><tr><td width=15px><a href='#' onClick=\"ManagerGeneral('".$this->page_name."?action=ranking&id=".$record_id."&to=dn&page_id=".$this->page_id."&from=$from&$OtherParameter')\"><img src=".$this->ImagePath."/".$this->ImageDown." border=0 alt='Down' title='Down'></a></td><td width=15px>&nbsp;</td></tr></table>";

			}

			elseif($OrderValue < $this->MaxOrderNo)

				$ImgRank="<table border=0 cellspacing=0 cellpadding=0><tr><td width=15px><a href='#' onClick=\"ManagerGeneral('".$this->page_name."?action=ranking&id=".$record_id."&to=up&page_id=".$this->page_id."&from=$from&$OtherParameter')\"><img src=".$this->ImagePath."/".$this->ImageUp." border=0 alt='Up' title='Up'></a>&nbsp;</td><td width=15px><a href='#' onClick=\"ManagerGeneral('".$this->page_name."?action=ranking&id=".$record_id."&to=dn&page_id=".$this->page_id."&from=$from&$OtherParameter')\"><img src=".$this->ImagePath."/".$this->ImageDown." border=0 alt='Down' title='Down'></a></td></tr></table>";

			elseif($OrderValue == $this->MaxOrderNo)

				$ImgRank="<table border=0 cellspacing=0 cellpadding=0><tr><td width=15px>&nbsp;</td><td align=right width=15px><a href='#' onClick=\"ManagerGeneral('".$this->page_name."?action=ranking&id=".$record_id."&to=up&page_id=".$this->page_id."&from=$from&$OtherParameter')\"><img src=".$this->ImagePath."/".$this->ImageUp." border=0 alt='Up' title='Up'></a></td></tr></table>";

				

			return 	$ImgRank;

				

		}

		

		function RankingRowCreate1($OrderValue,$record_id,$OtherParameter="")

		{

			//echo $OrderValue."--".$this->MaxOrderNo."--".$this->MinOrderNo.",";

			global $from;

			$ImgRank="";

			if($OrderValue==$this->MinOrderNo)

			{

				//if($OrderValue==$this->MaxOrderNo)

					//$ImgRank="--";

				//else

				$ImgRank="<table border=0 cellspacing=0 cellpadding=0><tr><td width=15px><a href='#' onClick=\"ManagerGeneral('".$this->page_name."?action=ranking&id=".$record_id."&to=dn&page_id=".$this->page_id."&from=$from&$OtherParameter')\"><img src=".$this->ImagePath."/".$this->ImageDown." border=0 alt='Down' title='Down'></a></td><td width=15px>&nbsp;</td></tr></table>";

			}

			elseif($OrderValue < $this->MaxOrderNo)

				$ImgRank="<table border=0 cellspacing=0 cellpadding=0><tr><td width=15px><a href='#' onClick=\"ManagerGeneral('".$this->page_name."?action=ranking&id=".$record_id."&to=up&page_id=".$this->page_id."&from=$from&$OtherParameter')\"><img src=".$this->ImagePath."/".$this->ImageUp." border=0 alt='Up' title='Up'></a>&nbsp;</td><td width=15px><a href='#' onClick=\"ManagerGeneral('".$this->page_name."?action=ranking&id=".$record_id."&to=dn&page_id=".$this->page_id."&from=$from&$OtherParameter')\"><img src=".$this->ImagePath."/".$this->ImageDown." border=0 alt='Down' title='Down'></a></td></tr></table>";

			elseif($OrderValue == $this->MaxOrderNo)

				$ImgRank="<table border=0 cellspacing=0 cellpadding=0><tr><td width=15px>&nbsp;</td><td align=right width=15px><a href='#' onClick=\"ManagerGeneral('".$this->page_name."?action=ranking&id=".$record_id."&to=up&page_id=".$this->page_id."&from=$from&$OtherParameter')\"><img src=".$this->ImagePath."/".$this->ImageUp." border=0 alt='Up' title='Up'></a></td></tr></table>";

				

			return 	$ImgRank;

				

		}

		/* Ranking Row Create Ends */

		

		/* Ranking Delete Starts */

		function RankingDelete($id,$is_cat="N",$category_id=0)

		{

			$TableName=$this->adodbcon->GetOne("SELECT table_name FROM ".$this->RankingTableName." WHERE page_id='".$this->page_id."'");

			$TableName=TABLEPREFIX.$TableName;

			$Pkey=$this->adodbcon->GetOne("SELECT pkey FROM ".$this->RankingTableName." WHERE page_id='".$this->page_id."'");

			$PageName=$this->adodbcon->GetOne("SELECT page_name FROM ".$this->RankingTableName." WHERE page_id='".$this->page_id."'");

			$OrderName=$this->adodbcon->GetOne("SELECT order_name FROM ".$this->RankingTableName." WHERE page_id='".$this->page_id."'");

			$CategoryIDName=$this->adodbcon->GetOne("SELECT category_id_name FROM ".$this->RankingTableName." WHERE page_id='".$this->page_id."'");			

			

			if($is_cat=="Y")			

				$CatStr=" AND ".$CategoryIDName." = '$category_id'";

			else

				$CatStr="";

				

			$SelectedDeleteOrder=$this->adodbcon->GetOne("SELECT ".$OrderName." FROM ".$TableName." WHERE ".$Pkey."='$id' $CatStr "); 						

			

			$SqlOrderRearrange = "SELECT ".$Pkey.",".$OrderName." FROM ".$TableName."  WHERE ".$OrderName." > '$SelectedDeleteOrder' $CatStr  ORDER BY ".$OrderName." ASC";

			$OrderRearrangeArr=$this->adodbcon->GetAll($SqlOrderRearrange);			

			for($i=0;$i<count($OrderRearrangeArr);$i++)

			{

			  $OldCategoryID = $OrderRearrangeArr[$i][$Pkey];

			  $OldCategoryOrder = $OrderRearrangeArr[$i][$OrderName];

			  $NewCategoryOrder = $OldCategoryOrder - 1;

			  $SqlOrderUpdate = "UPDATE ".$TableName." SET ".$OrderName." = '$NewCategoryOrder' WHERE ".$Pkey." = '$OldCategoryID' $CatStr";			 

			  $this->adodbcon->Execute($SqlOrderUpdate);

			}			

				

		}

		/* Ranking Delete Ends */

				

	}	

?>

