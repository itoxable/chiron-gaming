<?	

	class SortingClass

	{				

		var $ERROR = "";		

		function clear_error()

		{

			$this->ERROR = "";

		}

			

		function Sorting($DefaultOrderName,$SortingArr,$do_order="",$OrderByID=0,$OrderType="ASC")

		{	

			if($do_order=="GO"&&!empty($OrderByID))

			{						

				$OrderType=$OrderType=="ASC"?"DESC":"ASC";								 

				$OrderByName=$SortingArr[$OrderByID];						

				$OrderBySql =" ORDER BY $OrderByName $OrderType ";

				

				$OrderTypePreserve=$OrderType=="ASC"?"DESC":"ASC";

				$OrderLink="do_order=GO&OrderByID=$OrderByID&OrderType=$OrderTypePreserve";

			}

			else

			{

				$OrderBySql =" ORDER BY $DefaultOrderName ";

				$OrderLink="";

				$OrderType="";				

			}							

			

			$DisplaySortingImage=array();

			foreach($SortingArr as $key =>$val)

			{

				$SortingImage="";				

				if($do_order=="GO" && $OrderByID==$key)

				{											

					if($OrderType=="ASC") 

						$SortingImage="<img src='images/order_down.gif' border='0' align='absmiddle'>";

					else if($OrderType=="DESC")

						$SortingImage="<img src='images/order_up.gif' border='0' align='absmiddle'>";

				}									

				$DisplaySortingImage[$key]=$SortingImage;				

			}	

			

			$return_arr['OrderBySql']=$OrderBySql;

			$return_arr['OrderLink']=$OrderLink;

			$return_arr['OrderType']=$OrderType;

			$return_arr['DisplaySortingImage']=$DisplaySortingImage;		

			

			return $return_arr;							

		}				

				

	}	

?>