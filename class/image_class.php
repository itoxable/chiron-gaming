<?php
	class ImageClass
	{
		var $create_type;
		var $dimension;
		var $bigdimension;
		var $smalldimension;
		var $quality;
		var $resize_type;
		var $image_path="";
		
		var $ERROR = "";
		
		function clear_error()
		{
			$this->ERROR = "";
		}
		function ImageClass($create_type,$quality,$resize_type)
		{
			
			$this->create_type=$create_type;						
			$this->quality=$quality;
			$this->resize_type=$resize_type;
				
		}
		
		function SetImagePath($image_path)
		{			
			$this->image_path=$image_path;	
																
		}
		
		
		function UploadImage($PhotoT1,$imghidden1,$http_image_name,$http_image_temp,$dimension1=0,$resize_type=1,$rt4_dimension1=0,$bigthumb="N",$dimension2=0,$resize_type2=1,$rt4_dimension2=0,$smallthumb="N",$dimension3=0,$resize_type3=1,$rt4_dimension3=0)
		{			
			/* Image upload starts */
			//$src_path="/srv/www/test1/htdocs/cnc/".$this->image_path."/thumbs/";
			$src_path="/home2/grouptou/public_html/".$this->image_path."/thumbs/";
			
			$this->resize_type=$resize_type;
			$this->dimension=$dimension1;
			$this->bigdimension=$dimension2;
			$this->smalldimension=$dimension3;
			
			if($PhotoT1 == "Keep_Photo")
			{
				$Picc1=$imghidden1;
			}
			if($PhotoT1 == "Remove_Photo")
			{
				$Picc1="";
				if(!empty($imghidden1))
				{
					$filep=$imghidden1;
					@unlink("".$this->image_path."/".$filep);
					@unlink("".$this->image_path."/thumbs/".$filep);
					if($bigthumb=="Y")
					{
						@unlink("".$this->image_path."/thumbs/big_".$filep);
					}
					if($smallthumb=="Y")
					{
						@unlink("".$this->image_path."/thumbs/small_".$filep);
					}
					
				}
			}
			if(($PhotoT1 == "Change_Photo" || $PhotoT1 == ""))
			{
				$Picc1 = $http_image_name;
				#####################################
				if($this->create_type=="Normal")
				{
					$temp = $http_image_temp;
					$Piccs = $http_image_name;
					$Piccs = ereg_replace(" ", "_", $Piccs);
					$Piccs = ereg_replace("%20", "_", $Piccs);
					$Piccs = preg_replace("/[^-\._a-zA-Z0-9]/", "", $Piccs);
					$uniq = uniqid("");
					$Picc1 = $uniq."_".$Piccs;
					$upload  = "".$this->image_path."/".$Picc1;
					copy($temp, $upload);
					
					if(!empty($imghidden1))
					{
						$filep=$imghidden1;
						@unlink("".$this->image_path."/".$filep);
					}
				}
				elseif($this->create_type=="GDThumbnail"||$this->create_type=="NETPBMThumbnail")
				{
					

					//thumbnailstart
					$Piccs = $http_image_name;
					$Piccs = ereg_replace(" ", "_", $Piccs);
					$Piccs = ereg_replace("%20", "_", $Piccs);
					$Piccs = preg_replace("/[^-\._a-zA-Z0-9]/", "", $Piccs);
					$extension=substr($Piccs,strrpos($Piccs,".")+1);
					$img_folder="".$this->image_path."/thumbs/";
					$picname = $Piccs;
					$tmp_pict_name=$img_folder.$picname;
	
					$pict_name=$picname;
					if(strlen($Piccs)!=0)
					{							
							if(copy($http_image_temp,$tmp_pict_name))
							{
		
								$Piccs=uniqid("")."_".$pict_name;
								$Picc1 = $Piccs;
								$imagename = $src_path.$pict_name;
								$thumbnail = $src_path.$Picc1;
								copy($http_image_temp,"".$this->image_path."/".$Picc1);
								//$image_info = getimagesize($imagename);
								
								if($this->create_type=="NETPBMThumbnail")
								{
								$this->create_netpbmthumbnail($imagename, $thumbnail, $this->quality, $this->dimension, $this->resize_type,$rt4_dimension1);
								}
								elseif($this->create_type=="GDThumbnail")
								{
								$this->create_gdthumbnail($tmp_pict_name,$Picc1,$this->dimension,$this->resize_type,$rt4_dimension1);
								}
								
								$thumbcreated = 1;
								@unlink($tmp_pict_name);
								if($bigthumb=="Y")
								{																		
									copy($http_image_temp,$tmp_pict_name);
									$Picc2="big_".$Piccs;
									$imagename = $src_path.$pict_name;
									$thumbnail = $src_path.$Picc2;
									if($this->create_type=="NETPBMThumbnail")
									{
									$this->create_netpbmthumbnail($imagename, $thumbnail, $this->quality, $this->bigdimension,$resize_type2,$rt4_dimension2);
									}
									elseif($this->create_type=="GDThumbnail")
									{
									$this->create_gdthumbnail($tmp_pict_name,$Picc2,$this->bigdimension,$resize_type2,$rt4_dimension2);
									}
								
									@unlink($tmp_pict_name);
									$bigthumbcreated = 1;
								}
								if($smallthumb=="Y")
								{																		
									copy($http_image_temp,$tmp_pict_name);
									$Picc3="small_".$Piccs;
									$imagename = $src_path.$pict_name;
									$thumbnail = $src_path.$Picc3;
									if($this->create_type=="NETPBMThumbnail")
									{
									$this->create_netpbmthumbnail($imagename, $thumbnail, $this->quality, $this->smalldimension,$resize_type3,$rt4_dimension3);
									}
									elseif($this->create_type=="GDThumbnail")
									{
									$this->create_gdthumbnail($tmp_pict_name,$Picc3,$this->smalldimension,$resize_type3,$rt4_dimension3);
									}
								
									@unlink($tmp_pict_name);
									$smallthumbcreated = 1;
								}
																
							}
							
							if(!empty($imghidden1))
							{
								$filep=$imghidden1;
								@unlink("".$this->image_path."/".$filep);
								@unlink("".$this->image_path."/thumbs/".$filep);
								if($bigthumb=="Y")
								{
									@unlink("".$this->image_path."/thumbs/big_".$filep);
								}
								if($smallthumb=="Y")
								{
									@unlink("".$this->image_path."/thumbs/small_".$filep);
								}
							}
							
							
					}
				}
				
			}
			
			return $Picc1;
			/* Image upload ends */


			
		}
				
		
		/*  GD LIBRARY Functions Starts   */		
		
		function create_gdthumbnail($src,$destimagename,$dimension,$resize_type,$rt4_dimension=0)
		{
			$src_path=explode("/",$src);
			$name=$src_path[count($src_path)-1];
			array_pop($src_path);
			$oldpath=implode("/",$src_path); 
			$newpath=$oldpath;
			$newpath=$newpath."/".$destimagename;
			if(copy($src,$newpath))
			{				
				$name_extension=substr($name,strrpos($name,".")+1);
				if (preg_match("/gif/i",$name_extension)){ $src_img=imagecreatefromgif($newpath);}
				if (preg_match("/jpg|jpeg/i",$name_extension)){ $src_img=imagecreatefromjpeg($newpath);}
				if (preg_match("/png/i",$name_extension)){$src_img=imagecreatefrompng($newpath);}
				$old_x=imageSX($src_img);
				$old_y=imageSY($src_img);
				
				if($resize_type == 1) 
				{
					$thumb_w = $dimension;
					$thumb_h = $rt4_dimension;
				}
				elseif($resize_type == 2) 
				{
					$thumb_w = $dimension;
					$thumb_h = floor(($old_y/$old_x) * $dimension);
				}
				elseif($resize_type == 3) 
				{
					$thumb_w = floor(($old_x/$old_y) * $dimension);
					$thumb_h = $dimension;
				}
				elseif($resize_type == 4) 
				{
					$ratio = $old_x / $old_y;
					if ($ratio > 1) 
					{
					$thumb_w = $dimension;
					$thumb_h = floor(($old_y/$old_x) * $dimension);
					}
					else 
					{
					$thumb_w = floor(($old_x/$old_y) * $rt4_dimension);
					$thumb_h = $rt4_dimension;
					}
				}
				elseif($resize_type == 5) 
				{
					$ratio = $old_x / $old_y;
					if ($ratio > 1) 
					{
						$thumb_w = $dimension;
						$thumb_h = floor(($old_y/$old_x) * $dimension);
					
						if($thumb_h>$rt4_dimension)
						{
							$thumb_w = floor(($old_x/$old_y) * $rt4_dimension);
							$thumb_h = $rt4_dimension;
						}
						
					}
					else 
					{
						$thumb_w = floor(($old_x/$old_y) * $rt4_dimension);
						$thumb_h = $rt4_dimension;
					
						if($thumb_w>$dimension)
						{
							$thumb_w = $dimension;
							$thumb_h = floor(($old_y/$old_x) * $dimension);
						}
					
					}
				}else 
				{
					$ratio = $old_x / $old_y;
					if ($ratio > 1) 
					{
					$thumb_w = $dimension;
					$thumb_h = floor(($old_y/$old_x) * $dimension);
					}
					else 
					{
					$thumb_w = floor(($old_x/$old_y) * $dimension);
					$thumb_h = $dimension;
					}
				}
				$dst_img=@ImageCreateTrueColor($thumb_w,$thumb_h);
				@imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 
			
				if(preg_match("/png/i",$name_extension))
				{
					@imagepng($dst_img,$newpath); 
				}
				if(preg_match("/jpg|jpeg/i",$name_extension))
				{
					@imagejpeg($dst_img,$newpath); 
				}
				if(preg_match("/gif/i",$name_extension))
				{
					@imagegif($dst_img,$newpath); 
				}
				@imagedestroy($dst_img); 
				@imagedestroy($src_img); 
			}
		}
		
		/*  GD LIBRARY Functions Ends   */
		

		
		/*  NETPBM Functions Starts */
		
		function create_netpbmthumbnail($src, $dest, $quality, $dimension, $resize_type,$rt4_dimension=0) 
		{
			  global $convert_options;
				define('IN_CP', 1);
				$image_info = (defined("IN_CP")) ? getimagesize($src) : @getimagesize($src);
			  if (!$image_info) 
			  {
				return false;
			  }
			  $width_height = $this->get_width_height($dimension, $image_info[0], $image_info[1], $resize_type,$rt4_dimension=0);
			  $resize_handle = "resize_image_netpbm";
			  if ($this->$resize_handle($src, $dest, $quality, $width_height['width'], $width_height['height'], $image_info)) 
			  {
				@chmod($dest, CHMOD_FILES);
				$thumbcreated1 = 1;
				return true;
			  }
			  else 
			  {
				return false;
			  }
		}

		
		function get_width_height($dimension, $width, $height, $resize_type = 1,$rt4_dimension=0) 
		{
			  if($resize_type == 2) 
			  {
				$new_width = $dimension;
				$new_height = floor(($height/$width) * $dimension);
			  }
			  elseif($resize_type == 3) 
			  {
				$new_width = floor(($width/$height) * $dimension);
				$new_height = $dimension;
			  }
			  elseif($resize_type == 4) 
			  {
					$ratio = $width / $height;
					if ($ratio > 1) 
					{
					  $new_width = $dimension;
					  $new_height = floor(($height/$width) * $dimension);
					}
					else 
					{
					  $new_width = floor(($width/$height) * $rt4_dimension);
					  $new_height = $rt4_dimension;
					}
			  }
			  else 
			  {
					$ratio = $width / $height;
					if ($ratio > 1) 
					{
					  $new_width = $dimension;
					  $new_height = floor(($height/$width) * $dimension);
					}
					else 
					{
					  $new_width = floor(($width/$height) * $dimension);
					  $new_height = $dimension;
					}
			  }
			  return array("width" => $new_width, "height" => $new_height);
		}


		function resize_image_netpbm($src, $dest, $quality, $width, $height, $image_info) 
		{
		global $convert_options;
			$convert_path = "/home2/grouptou/public_html/netpbm";
		  $types = array(1 => "gif", 2 => "jpeg", 3 => "png");
		  $target = ($width > $height) ? $width : $height;
		  
		  $command = $convert_path."/".$this->check_executable($types[$image_info[2]]."topnm")." ".$src." | ".$convert_path."/".$this->check_executable("pnmscale")." --quiet -xysize ".$target." ".$target." | ";
		  if ($image_info[2] == 1) {
			$command .= $convert_path."/".$this->check_executable("ppmquant")." 256 | " . $convert_path."/".$this->check_executable("ppmtogif")." > ".$dest;
		  }
		  elseif ($image_info[2] == 3) {
			$command .= $convert_path."/".$this->check_executable("pnmtopng")." > ".$dest;
		  }
		  else {
			$jpeg_exec = (file_exists($convert_path."/".$this->check_executable("pnmtojpeg"))) ? $this->check_executable("pnmtojpeg") : $this->check_executable("ppmtojpeg");
			$command .= $convert_path."/".$jpeg_exec." --quality=".$quality." > ".$dest;
		  }
		  
		  system($command);
		  return (file_exists($dest)) ? 1 : 0;
		}
		
		function check_executable($file_name) 
		{
		  if (substr(PHP_OS, 0, 3) == "WIN" && !eregi("\.exe$", $file_name)) {
			$file_name .= ".exe";
		  }
		  elseif (substr(PHP_OS, 0, 3) != "WIN") {
			$file_name = eregi_replace("\.exe$", "", $file_name);
		  }
		  return $file_name;
		}


		/*  NETPBM Functions Ends */
		
		/* Checking of Image Starts */
		function CheckImage($PhotoT1,$http_image_name,$http_image_temp,$http_image_type,$http_image_size,$checktype=1,$Photolebel="",$mandatory="N",$bytesize=3,$ispixelsize="N",$pixelequal="N",$Pixelwidth=0,$Pixelheight=0)
		{
			//echo $mandatory.','.$bytesize.','.$ispixelsize.','.$pixelequal.','.$Pixelwidth.','.$Pixelheight;exit;
				if(!empty($Photolebel))
					$Photolebel =" for <span class=red>".$Photolebel."</span>";
				if($mandatory=="Y")
				{
					if($PhotoT1 == "Remove_Photo")
					{
						$err_msgs .= "<li>Image should not be removed $Photolebel!</li>";
					}
				}	
				if ($PhotoT1 == "Change_Photo" || $PhotoT1 == "")
				{
						$Picc1 = $http_image_name;
																								
						if(!empty($Picc1)) ## CHECKING IMAGE
						{
							$img_size=$http_image_size;
							$img_type=$http_image_type;
							$temp = $http_image_temp;
							
							switch($bytesize)
							{
								/* set size limit in MB */
								
								case 1:$size_limit = "1048576";
										break;
								case 2:$size_limit = "2097152";
										break;
								case 3:$size_limit = "3145728";
										break;
								case 4:$size_limit = "4194304";
										break;
								default:$size_limit = "3145728";
										break;
							}
							
							if($checktype==2)
								$allowed_types = array("image/gif","image/jpeg", "image/pjpeg", "image/jpg","image/png","image/x-png");
							else
								$allowed_types = array("image/gif","image/jpeg", "image/pjpeg", "image/jpg","image/png");
																		
							if($img_size!="0")
							{
								if ($img_size < $size_limit)
								{
									
									if (!in_array($img_type,$allowed_types))
									{
										$err_msgs .= "<li>The file <span class=red>$Picc1</span> is not an image $Photolebel. <br>&nbsp;&nbsp;&nbsp;Files of type <b>$img_type</b> are not allowed to be uploaded!</li>";
									}
									else
									{
										if($ispixelsize=="Y")
										{
										
											$pixelsize_arr=GetImageSize($temp);
											if($pixelequal=="N")
											{
												if($pixelsize_arr[0]>$Pixelwidth||$pixelsize_arr[1]>$Pixelheight)
												{
													$err_msgs .= "<li>The file <span class=red>$Picc1</span> you selected is <b>$pixelsize_arr[0] X $pixelsize_arr[1]</b> pixels and is too large to be uploaded!</li>";
												}
											}
											else
											{
												 if($pixelsize_arr[0] != $Pixelwidth || $pixelsize_arr[1] != $Pixelheight)
												  {
													
													$err_msgs .= "<li>The file <span class=red>$Picc1</span> you selected should be <b>$Pixelwidth X $Pixelheight</b> pixels exactly!</li>";
												  }
												 /* else
												  {
												   $givenpixelratio=$Pixelwidth/$Pixelheight;
												   $uploadedpixelratio=$pixelsize_arr[0]/$pixelsize_arr[1];
												   if($givenpixelratio!=$uploadedpixelratio)
												   {
													 $err_msgs .= "The width/height pixels ratio of the file <span class=red>$Picc1</span> that you selected <b>$pixelsize_arr[0] X $pixelsize_arr[1]</b> should be $givenpixelratio($uploadedpixelratio)!<br>";
												   }
												   
												  }*/
											
											}
										}
									}
								}
								else
								{
									$err_msgs .= "<li>The file <span class=red>$Picc1</span> you selected to upload is too large to be uploaded $Photolebel!</li>";
								}
							 }
							 else if($img_size=="0")
							 {
								$err_msgs .= "<li>The file <span class=red>$Picc1</span> was not found $Photolebel!</li>";
							}
						} ## END OF CHECKING IMAGE
						
						if($mandatory=="Y")
						{
							
							if( ($PhotoT1 == "Change_Photo" || $PhotoT1 == "" ) && empty($Picc1))
							{
								$err_msgs .= "<li>No image uploaded $Photolebel!</li>";
							}
							
						}
						elseif($mandatory=="N")
						{
							if ($PhotoT1 == "Change_Photo" && empty($Picc1))
							{
								$err_msgs .= "<li>No image uploaded $Photolebel!</li>";
							}
						}
				} 
				
				return $err_msgs;
		}
		/* Checking of Image Ends */
		
		/* Echo Image Instruction Note starts */
		function EchoInstructionImage($instuctiontype="",$is_size="N",$pixelequel="N",$pixelwidth=0,$pixelheight=0)
		{
			$InstructDisplayStr="";
			if(empty($instuctiontype)||$instuctiontype==1)
			{
				$InstructDisplayStr .="<em>(.gif, .jpeg, .jpg, .png, max-3MB)</em>";				
			}	
			elseif($instuctiontype==2)
			{
				$InstructDisplayStr .="<em>(.gif, .jpeg, .jpg, .png, max-3MB)</em>";
				
			}
			if($is_size=="Y")
			{
				if($pixelequel=="N")
				{
					$InstructDisplayStr .="<br>";
					if(!empty($pixelwidth))
						$InstructDisplayStr .="<b>Display width $pixelwidth </b>";
					if(!empty($pixelheight))
						$InstructDisplayStr .="<b>Display  height $pixelheight </b>";
					$InstructDisplayStr .="<b>in pixel.</b>";		
						
				}	
				elseif($pixelequel=="Y")
				{
					$InstructDisplayStr .="<br><b> width $pixelwidth and height $pixelheight in pixel exactly.</b>";
				}		
			}					
				
			return $InstructDisplayStr;		
		}
		/* Echo Image Instruction Note Ends */
		
		/* Display of Upload Control starts */
	function DisplayImageControl($image_lebel_name,$image_control_name,$image_control_val,$image_hidden_name,$image_radio_name,$instuctiontype=1,$mandatory='N',$display_by='Thumbs',$normaldisplaydimension=0,$is_size='N',$pixelequel='N',$pixelwidth=0,$pixelheight=0)
	{
		$DisplayImageControlStr="";
		/*$DisplayImageControlStr .= 
		"<tr>
			<td height='27' align='right' class='plaintxt' colspan='2'>			
				".$this->EchoInstructionImage($instuctiontype=1,$is_size,$pixelequel,$pixelwidth,$pixelheight) ."
			</td>
		</tr>";*/
		
		$DisplayImageControlStr .= 
		"<tr >
			<td width='40%' height='27' align='left' class='plaintxt'>$image_lebel_name".$this->EchoInstructionImage($instuctiontype=1,$is_size,$pixelequel,$pixelwidth,$pixelheight)."<span class='red' id='check_img'></span>";
			if($mandatory=='Y') {$DisplayImageControlStr .="<span class='red' id='img_up'>*</span>:"; }
			else{$DisplayImageControlStr .=":"; }
			$DisplayImageControlStr .= 
			"</td>
			<td height='27' align='left' class='simpleText'>";			
			if(strlen($image_control_val)!=0 && file_exists(''.$this->image_path.'/'.$image_control_val))
			{															
				if($this->create_type=='Normal'||$display_by=='Normal')
				{						
					$size_arr=$this->GetImageWidthHeight(''.$this->image_path,$image_control_val,$normaldisplaydimension = 90);						
					$ImageSize='width='.$size_arr[0].' height='.$size_arr[1];
					$ImagePath=''.$this->image_path.'/'.$image_control_val;
				}
				else
					$ImagePath=''.$this->image_path.'/thumbs/'.$image_control_val;
				
				$DisplayImageControlStr .=	
				"
				<img src='$ImagePath' $ImageSize><br />
				<input type='radio' name='$image_radio_name' value='Keep_Photo' checked class='form' />
				<span class='body'>Keep Image</span>
				";
				if($mandatory!='Y')
				{
				$DisplayImageControlStr .=
				"<input type='radio' name='$image_radio_name' value='Remove_Photo' class='form' />
				<span class='body'>Remove Image</span>
				";
				}
				$DisplayImageControlStr .=
				"<input type='radio' name='$image_radio_name' value='Change_Photo' class='form' />
				<span class='body'>Change Image</span><br />
				";
			}
			$DisplayImageControlStr .=
			"<input type='file' name='$image_control_name' class='form' size='35' />
			<input type='hidden' name='$image_hidden_name' value='$image_control_val' />";
			
			$DisplayImageControlStr .= "</td>
		</tr>";
		
		return $DisplayImageControlStr;
			
	}
/* Display of Upload Control Ends */
		
		/* GetImageHeightWidth starts */
		function GetImageWidthHeight($path,$pic,$newdimension = 75)
		{
			$vpath  = $path."/".$pic;
			$size   = GetImageSize($vpath);
		
			$old_width = $size[0];
			$old_height = $size[1];
			
			$ratio=$old_width/$old_height;
			if($ratio>1)
			{
				$new_width = $newdimension;
				$new_height = floor($newdimension*($old_height/$old_width));
			}
			else
			{
				$new_width = floor($newdimension*($old_width/$old_height));
				$new_height =$newdimension ;
			}
			$size_arr[0]=$new_width;
			$size_arr[1]=$new_height;
			
			return $size_arr;			
		}
		/* GetImageHeightWidth Ends */
	
	
	}
?>