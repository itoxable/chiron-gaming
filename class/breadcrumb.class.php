<?php

/*

  $Id: breadcrumb.php 1739 2007-12-20 00:52:16Z hpdl $

  Copyright (c) 2003 Indiwo

  Released under the GNU General Public License

*/



  class breadcrumb {

    var $_trail;

	var $adodbcon;

	var $table_name;
	
	var $cat;

    function breadcrumb($table_name,$adodbcon,$cat) {

      $this->table_name = $table_name;

	  $this->adodbcon = $adodbcon;
	  $this->cat = $cat;

	  $this->reset();

    }



    function reset() {

      $this->_trail = array();

    }



    function add($name, $link = '') {

      $this->_trail[] = array('name' => $name, 'link' => $link);

    }

	

	  function tep_not_null($value) {

		if (is_array($value)) {

		  if (sizeof($value) > 0) {

			return true;

		  } else {

			return false;

		  }

		} else {

		  if (($value != '') && (strtolower($value) != 'null') && (strlen(trim($value)) > 0)) {

			return true;

		  } else {

			return false;

		  }

		}

	  }

	

    function trail($separator = ' - ',$categoryID,$link) 

	{

		$SqlParent = "SELECT ".$this->cat."_name,".$this->cat."_parent_id,".$this->cat."_id FROM ".$this->table_name." WHERE ".$this->cat."_id = $categoryID";

		$RsParentArr = $this->adodbcon->GetRow($SqlParent);

		$this->add($RsParentArr[$this->cat.'_name'], '');

		$this->build_parent($RsParentArr[$this->cat.'_parent_id']);

		

     	$trail_string = '';

		$this->_trail = array_reverse($this->_trail);

      for ($i=0, $n=sizeof($this->_trail); $i<$n; $i++) {

        if (isset($this->_trail[$i]['link']) && $this->tep_not_null($this->_trail[$i]['link'])) {

          $trail_string .= '<a href="' .$link.$this->_trail[$i]['link'] . '" class="headerNavigation">' . $this->_trail[$i]['name'] . '</a>';

        } else {

          $trail_string .= $this->_trail[$i]['name'];

        }



        if (($i+1) < $n ) $trail_string .= $separator;

      }

		// print_r($this->_trail);

     return $trail_string;

		

    }

	

	function build_parent($categoryID)

	{

		

		if($categoryID!=0)

		{

			$SqlParent = "SELECT ".$this->cat."_name,".$this->cat."_parent_id,".$this->cat."_id FROM ".$this->table_name." WHERE ".$this->cat."_id = $categoryID";

			$RsParentArr = $this->adodbcon->GetRow($SqlParent);

			if($RsParentArr[$this->cat.'_parent_id'] ==0)

			{

				 $this->add($RsParentArr[$this->cat.'_name'], $RsParentArr[$this->cat.'_id']);

				 

			}

			elseif($RsParentArr[$this->cat.'_parent_id'] !=0)

			{

				$this->add($RsParentArr[$this->cat.'_name'], $RsParentArr[$this->cat.'_id']);

				$this->build_parent($RsParentArr[$this->cat.'_parent_id']);

				

			}

		}

		return;

	}

	

	function top_parent($categoryID)

	{

		global $pId;

		//print "categoryID = $this->categoryID";

		if($categoryID!=0)

		{

			 $SqlParent = "SELECT ".$this->cat."_name,".$this->cat."_parent_id,".$this->cat."_id FROM ".$this->table_name." WHERE ".$this->cat."_id = $categoryID";

			 $RsParentArr = $this->adodbcon->GetRow($SqlParent);

			

			if($RsParentArr[$this->cat.'_parent_id'] !=0)

				$this->top_parent($RsParentArr[$this->cat.'_parent_id']);

			else

				$pId = $RsParentArr[$this->cat.'_id'];

			

		}

		return;

	}

  }

?>

