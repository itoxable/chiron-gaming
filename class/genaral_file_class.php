<?	
	class GeneralFileClass
	{				
		var $ERROR = "";
		var $upload_path = "uploaded";
		
		function clear_error()
		{
			$this->ERROR = "";
		}
		
		
		function UploadGeneralFile($GeneralFileRadio,$general_file_hidden1,$http_general_file_name,$http_general_file_temp)
		{
			/* File upload starts */			
			
			if($GeneralFileRadio == "Keep_General_File")
			{
				$filec1=$general_file_hidden1;
			}
			if($GeneralFileRadio == "Remove_General_File")
			{
				$filec1="";
				if(!empty($general_file_hidden1))
				{
					$filep=$general_file_hidden1;					
					@unlink($this->upload_path."/".$filep);					
					
				}
			}
			if(($GeneralFileRadio == "Change_General_File" || $GeneralFileRadio == ""))
			{
					$filec1 = $http_general_file_name;
					#####################################
					
					$temp = $http_general_file_temp;
					$Filecs = $http_general_file_name;
					$Filecs = ereg_replace(" ", "_", $Filecs);
					$Filecs = ereg_replace("%20", "_", $Filecs);
					$Filecs = preg_replace("/[^-\._a-zA-Z0-9]/", "", $Filecs);
					$uniq = uniqid("");
					$filec1 = $uniq."_".$Filecs;
					$upload  = $this->upload_path."/".$filec1;
					copy($temp, $upload);
					
					if(!empty($general_file_hidden1))
					{
						$filep=$general_file_hidden1;
						@unlink($this->upload_path."/".$filep);
					}					
											
			}
			
			return $filec1;
			/* File upload ends */

			
		}
				
		
		
		/* Checking of Document Starts */
		function CheckGeneralFile($GeneralFileRadio,$http_general_file_name,$http_general_file_temp,$http_general_file_type,$http_general_file_size,$allowed_types,$allowed_types_lebel,$Filelebel="",$mandatory="N",$bytesize=3)
		{
				if(!empty($Filelebel))
					$Filelebel =" for <span class=red>".$Filelebel."</span>";
				if($mandatory=="Y")
				{
					if($GeneralFileRadio == "Remove_General_File")
					{
						$err_msgs .= "<li>File should not be removed $Filelebel!</li>";
					}
				}	
				if ($GeneralFileRadio == "Change_General_File" || $GeneralFileRadio == "")
				{
						$filec1 = $http_general_file_name;
																								
						if(!empty($filec1)) ## CHECKING IMAGE
						{
							$general_file_size=$http_general_file_size;
							$general_file_type=$http_general_file_type;
							$temp = $http_general_file_temp;
							
							if(empty($bytesize))
								$bytesize=3;
							$size_limit=$bytesize*1048576;  /* set size limit in Bytes  */
							
							$http_general_file_name_arr=explode(".",$http_general_file_name);
							$general_file_type_dot=$http_general_file_name_arr[count($http_general_file_name_arr)-1];																													
							$general_file_type_dot=".".$general_file_type_dot;											
							if($general_file_size!="0")
							{
								if ($general_file_size < $size_limit)
								{									
									if (!in_array($general_file_type,$allowed_types))
									{
										$err_msgs .= "<li>Files of type <b>$general_file_type</b> are not allowed to be uploaded!</li>";
									}
									elseif (!in_array($general_file_type_dot,$allowed_types_lebel))
									{
										$err_msgs .= "<li>Files of type <b>$general_file_type</b> are not allowed to be uploaded!</li>";
									}									
								}
								else
								{
									$err_msgs .= "<li>The file <span class=red>$filec1</span> you selected to upload is too large to be uploaded $Filelebel!</li>";
								}
							 }
							 else if($general_file_size=="0")
							{
								$err_msgs .= "<li>The file <span class=red>$filec1</span> was not found $Filelebel!</li>";
							}
						} ## END OF CHECKING IMAGE
						
						if($mandatory=="Y")
						{
							
							if( ($GeneralFileRadio == "Change_General_File" || $GeneralFileRadio == "" ) && empty($filec1))
							{
								$err_msgs .= "<li>No File uploaded $Filelebel!</li>";
							}
							
						}
						elseif($mandatory=="N")
						{
							if ($GeneralFileRadio == "Change_General_File" && empty($filec1))
							{
								$err_msgs .= "<li>No File uploaded $Filelebel!</li>";
							}
						}
				} 
				
				return $err_msgs;
		}
		/* Checking of Document Ends */		
		
		/* Display of Upload Control starts */
		function DisplayGeneralFileControl($general_file_lebel_name,$general_file_control_name,$general_file_control_val,$general_file_hidden_name,$general_file_radio_name,$mandatory="Y",$display_general_file="Y",$allowed_types,$generalfilesize=3)
		{
			?>
			<tr bgcolor="#FFFFFF">
				<td valign="top" class="body" colspan="2" align="left">				
				<b>Upload 
				<? 
					$allowed_types_display_str="";
					foreach($allowed_types as $val)
					{
						$allowed_types_display_str.=$val.",";
					}
					echo substr($allowed_types_display_str,0,-1);
				?> file
				. Upload file should not exceed <?=$generalfilesize?>Mb.</b>								
				</td>		
			</tr>
			<tr bgcolor="#FFFFFF">
				<td class="body" valign="top"><?=$general_file_lebel_name?>: <? if($mandatory=="Y") {?><span class="red">*</span><? }?></td>
				<td valign="top">
				<? 
				if (strlen($general_file_control_val)!=0 && file_exists($this->upload_path."/".$general_file_control_val))
				{																									
					if($display_general_file=="Y")
					{
					$DocPath=$this->upload_path."/".$general_file_control_val;
					?>
					<a href="<? echo $DocPath;?>" class="links"><? echo substr($general_file_control_val,14);?></a><BR>
					<?
					}
					?>
					<input type="radio" name="<?=$general_file_radio_name?>" value="Keep_General_File" checked >
					<span class="body">Keep File</span>
					<? 
					if($mandatory!="Y")
					{
					?>
					<input type="radio" name="<?=$general_file_radio_name?>" value="Remove_General_File" >
					<span class="body">Remove File</span>
					<?
					}
					?>
					<input type="radio" name="<?=$general_file_radio_name?>" value="Change_General_File">
					<span class="body">Change File</span><br>
					
					<INPUT type="hidden" name="<?=$general_file_hidden_name?>" value="<? echo $general_file_control_val; ?>">
					<?
				}
				?>
				<INPUT type="file" name="<?=$general_file_control_name?>" class="form" size="30">				
				</td>
			</tr>
			<?	
		}
		/* Display of Upload Control Ends */
				
	
	}
?>