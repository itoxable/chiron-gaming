<?php
/*
  $Id: breadcrumb.php 1739 2007-12-20 00:52:16Z hpdl $
  Copyright (c) 2003 Indiwo
  Released under the GNU General Public License
*/

  class breadcrumbFront {
    var $_trail;

    function breadcrumbFront() {
      $this->reset();
    }

    function reset() {
      $this->_trail = array();
    }

    function add($title, $link = '') {
      $this->_trail[] = array('title' => $title, 'link' => $link);
    }
	
	  function tep_not_null($value) {
		if (is_array($value)) {
		  if (sizeof($value) > 0) {
			return true;
		  } else {
			return false;
		  }
		} else {
		  if (($value != '') && (strtolower($value) != 'null') && (strlen(trim($value)) > 0)) {
			return true;
		  } else {
			return false;
		  }
		}
	  }
	
    function trail($separator = ' - ') {
      $trail_string = '';

      for ($i=0, $n=sizeof($this->_trail); $i<$n; $i++) {
        if (isset($this->_trail[$i]['link']) && $this->tep_not_null($this->_trail[$i]['link'])) {
          $trail_string .= '<a href="' . $this->_trail[$i]['link'] . '" class="genl-link">' . $this->_trail[$i]['title'] . '</a>';
        } else {
          $trail_string .= $this->_trail[$i]['title'];
        }

        if (($i+1) < $n) $trail_string .= $separator;
      }

      return $trail_string;
    }
  }
?>
