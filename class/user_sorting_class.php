<?php	

	class UserSortingClass

	{				

		var $ERROR = "";		

		function clear_error()

		{

			$this->ERROR = "";

		}

			

		function Sorting($DefaultOrderName,$OrderByName,$OrderType)

		{	

			if(!empty($OrderType))

			{						

				$OrderType=$OrderType=="ASC"?"ASC":"DESC";								 

									

				$OrderBySql =" ORDER BY $OrderByName $OrderType ";

				$OrderTypePreserve=$OrderType=="ASC"?"ASC":"DESC";

				$OrderLink="OrderType=$OrderTypePreserve";

			}

			else

			{

				$OrderBySql =" ORDER BY $DefaultOrderName ";

				$OrderLink="";

				$OrderType="";				

			}							

			
 
			$return_arr['OrderBySql']=$OrderBySql;

			$return_arr['OrderLink']=$OrderLink;

			$return_arr['OrderType']=$OrderType;
	

			

			return $return_arr;							

		}				

				

	}	

?>