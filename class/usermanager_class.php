<?php 	

	class UserManagerClass extends Validator

	{				

		var $ERROR = "";			

				

		function clear_error()

		{

			$this->ERROR = "";

		}

		function UserManagerClass($adodbcon)

		{						
		
			$this->adodbcon=$adodbcon;															

		}

		

		function Execute($Sql,$InputArr=false)

		{				

			$RsRecords = $this->adodbcon->Execute($Sql,$InputArr=false);					

			return $RsRecords;														

		}

			

		function GetRecords($GetType,$SelectSql)

		{				

			if($GetType=="One")				

				$RsRecords=$this->adodbcon->GetOne($SelectSql);		

			elseif($GetType=="Row")				

				$RsRecords=$this->adodbcon->GetRow($SelectSql);	

			elseif($GetType=="Col")				

				$RsRecords=$this->adodbcon->GetCol($SelectSql);	

			elseif($GetType=="All")				

				$RsRecords=$this->adodbcon->GetAll($SelectSql);					

				

			return $RsRecords;														

		}	

		

		function InsertRecords($table_name,$table_fields)

		{																				

			$RsInsert = $this->adodbcon->AutoExecute($table_name, $table_fields,'INSERT');						

			if($this->adodbcon->ErrorNo()) 

			{

				//die($this->adodbcon->ErrorMsg());				

				return 1;

			}	

			else

				return 2;										

		}	

		

		function LastInsertID()

		{																			

			$InsertedOrderDetailsID=$this->adodbcon->Insert_ID();	

			if(!$InsertedOrderDetailsID)

				$InsertedOrderDetailsID=0;									

			return $InsertedOrderDetailsID;										

		}	

		

		function UpdateRecords($table_name,$table_fields,$where)

		{

			if($table_name !='' && count($table_fields)>0  && $where!='')

			{								

				$RsUpdate = $this->adodbcon->AutoExecute($table_name, $table_fields,'UPDATE', $where);								
				
				if($this->adodbcon->ErrorNo()) 

				{

					//die($this->adodbcon->ErrorMsg());		

					return 3;

				}	

				else

					return 4;	

			}

		}	

		

		function trim_val($val)

		{

			return trim($val);	

		}

		function trim_array($table_name)

		{

			if(is_array($table_name))

			{

				foreach($table_name as $key=>$val)

				{

					$val=trim($val);

					$table_name['$key']=$val;

				}

				return $table_name;	

			}

			else

				return false;

		}

		

		

		function DuplicateCheck($LebelName,$DuplicateCheckSql,$PKeyName,$PKeyValue="",$CategoryIDName="",$CategoryID="")

		{

				if(empty($PKeyValue))

				{						

					if(!empty($CategoryID))

						$DuplicateCheckSql.=" AND $CategoryIDName=".$this->sql_value($CategoryID);								 																							

				}

				elseif(!empty($PKeyValue))

				{

						

					 $DuplicateCheckSql.=" AND $PKeyName!=".$PKeyValue; 

					 if(!empty($CategoryID))

						$DuplicateCheckSql.=" AND $CategoryIDName=".$this->sql_value($CategoryID);							 						

						

				}			

				$RsDuplicateCheckSql = $this->GetRecords("All",$DuplicateCheckSql);		

				//echo count($RsDuplicateCheckSql);			

				if(count($RsDuplicateCheckSql)>0)

				{				  

				   $err_msgs= "<li><span class=red>Duplicate $LebelName found.</span></li>";

				}	

				

				return $err_msgs;

		}	

		

		function sql_value($value) 

		{

			   $value = trim($value);

			   

			   if($value == null || $value == "") 

			   {

				  return "NULL";

			   }

			   $magic_on=get_magic_quotes_gpc();

			   //news_name

			

			   //return "'$value'";

			   return "'".str_replace("'", "\'", str_replace("\n", "\\n", $value))."'";

		}				

		

		function HtmlOptionArrayCreate($HtmlOptionSql,$AddOnsArr=array())

		{

			$ReturnArray=array();

			$HtmlOptionArr = $this->GetRecords("All",$HtmlOptionSql);			

			if(is_array($HtmlOptionArr) && count($HtmlOptionArr)>0)

			{				

				foreach($HtmlOptionArr as $val)

				{					

					$selectoutput=array_pop($val); 

					array_pop($val);

					$selectvalues=array_pop($val); 

					if(is_array($AddOnsArr) && count($AddOnsArr)>0)

					{

						foreach($AddOnsArr as $rowval)

						{

							if($rowval[0]==$selectvalues)	

								$selectoutput=$selectoutput.$rowval[1];

						}	

					}				

					$ReturnArray[$selectvalues]=$selectoutput;

				}

			}

			return $ReturnArray;

		}	

		

		function GetLanguageAbbr($LanguageID)

		{			

			$SqlAbbr="SELECT language_abbr_name FROM ".TABLEPREFIX."_language l,".TABLEPREFIX."_language_abbr la

					 WHERE l.language_abbr_id=la.language_abbr_id 

					 AND l.language_id=".$LanguageID." 

					 AND l.is_active='Y'

					 AND la.is_active='Y'";

			$LanguageAbbr = $this->GetRecords("One",$SqlAbbr);			

			if(empty($LanguageAbbr))

				$LanguageAbbr='en';

			return $LanguageAbbr;

		}	

		

		function GetLanguageDir($LanguageID)

		{			

			$SqlDir="SELECT is_rtl FROM ".TABLEPREFIX."_language 

					 WHERE language_id=".$LanguageID;

			$LanguageDir = $this->GetRecords("One",$SqlDir);			

			$LanguageDir=($LanguageDir=="Y")?'rtl':'ltr';							

			return $LanguageDir;

		}

		

		/*  Random Number Generator Starts */

		function OrderNumberGenerator($tablename,$field,$type="")

		{

				$OrderNumber="";

				function make_seed()

				{

				   list($usec, $sec) = explode(' ', microtime());

				   return (float) $sec + ((float) $usec * 100000);

				}

				

				while(!$OrderNumber)

				{

						mt_srand(make_seed());

						$randval = mt_rand();

						$randvalw=rand();

						$randw = ($randvalw%22);

						switch($randw)

						{

							case 0 : $randwr='A';break;					

							case 1 : $randwr='B';break;					

							case 2 : $randwr='C';break;					

							case 3 : $randwr='D';break;

							case 4 : $randwr='E';break;

							case 5 : $randwr='F';break;

							case 6 : $randwr='G';break;

							case 7 : $randwr='H';break;

							case 8 : $randwr='J';break;

							case 9 : $randwr='K';break;

							case 10 : $randwr='L';break;

							case 11 : $randwr='M';break;

							case 12 : $randwr='N';break;

							case 13 : $randwr='P';break;

							case 14 : $randwr='Q';break;

							case 15 : $randwr='R';break;

							case 16 : $randwr='S';break;

							case 17 : $randwr='T';break;

							case 18 : $randwr='U';break;

							case 19 : $randwr='V';break;

							case 20 : $randwr='W';break;

							case 21 : $randwr='Y';break;

							case 22 : $randwr='Z';break;

						}

						$OrderNumber=$randwr."-".$randval;

						$OrderNumber=$type.$OrderNumber;

						$SelectNumber="SELECT * FROM $tablename WHERE $field = '$OrderNumber' ";						

						$SelectNumberArr=$this->GetRecords("All",$SelectNumber);

						$NumSelectNumberArr=count($SelectNumberArr);

						if($NumSelectNumberArr>0)

							$OrderNumber="";

				}

				

				return  $OrderNumber;

		

		}



		/*  Random Number Generator Ends */				

					

	}	

?>