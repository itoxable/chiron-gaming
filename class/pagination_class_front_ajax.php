<?	
 /*
 *	 Sportsbetting Version 1    
 *	 PaginationClassFrontAjaxSports Class
 *	 @desc class to pagination in ajax operation
 */

	class PaginationClassFrontAjax
	{
		var $item_per_page;		# Variable Integer item_per_page.
		var $shift_page_no;		# Variable Integer shift_page_no.
		var $display_page_no;	# Variable Integer display_page_no.
		var $next_text;			# Variable String next_text.
		var $previous_text;		# Variable String previous_text.
		var $ERROR = "";		# Variable String ERROR.
	/* 
			Clear error message.
	*/

		function clear_error()
		{
			$this->ERROR = "";
		}
		/* 
	 * Construct the PaginationClassFrontAjaxSports object.
     *
     *
	 * @access public
	 * @param   Integer  item_per_page 
	 * @param   Integer  display_page_no 
	 * @param   Integer  shift_page_no 
	 * @param   String   next_text 
	 * @param   String  previous_text 
	 * @return  mixed  Error object or string 
	*/

		function PaginationClassFrontAjax($item_per_page,$display_page_no,$shift_page_no,$next_text="",$previous_text="")
		{			
			$this->item_per_page=$item_per_page;
			$this->shift_page_no=$shift_page_no;
			$this->display_page_no=$display_page_no;	
			$this->next_text=$next_text;	
			$this->previous_text=$previous_text;										
		}
		/* 
	 * Function the PaginationFrontAjax.
     *
     *
	 * @access public
	 * @param  String  function_name 
	 * @param  String  sql 
	 * @param  String  page_name 
	 * @param  Integer CategoryID 
	 * @param  String  CategoryName 
	 * @return array
	*/
	
		function PaginationFrontAjax($function_name,$sql,$page_name,$CategoryID="",$CategoryName="")
		{		
			global $from;
			global $from_page;
			if(empty($from))
			{
				$from=0;
			}
			if(empty($from_page))
			{
				$from_page=1;
			}
			if(!empty($CategoryID))
				$category_link="&$CategoryName=".$CategoryID;			
			
				
			$totrec=mysql_num_rows(mysql_query($sql));
			if($totrec>0)
			{
				if($totrec>$this->item_per_page)
				{
					$prev=$from-$this->item_per_page;
					$next=$from+$this->item_per_page;					
					if($totrec-($this->item_per_page*$this->display_page_no) && $from_page>1)
					{
						$navigation.= "<li class='previous'><a href='#'  onClick=\"$function_name('$page_name&from=".$from."&from_page=".($from_page-$this->shift_page_no).$category_link."');return false;\" >".$this->previous_text."</a></li>";

					}
					else
					{
						$navigation.= "";
					}					
					for($i=1; $i<=ceil($totrec/$this->item_per_page);$i++)
					{
						
						if($i>=$from_page && $i<=($from_page+$this->display_page_no-1))
						{
							$num_len=strlen($i);	
									
							if((($from/$this->item_per_page)+1)==$i)
							{
								$navigation.= "<li><a href='#' onClick=\"$function_name('$page_name&from=".$startval."&from_page=".$from_page.$category_link."'); return false;\" class='select'>".$i."</a></li>";
							}
							else
							{
								$startval=($i-1)*$this->item_per_page;	
								$navigation.= "<li><a href='#' onClick=\"$function_name('$page_name&from=".$startval."&from_page=".$from_page.$category_link."');return false;\" >".$i."</a></li>";
							}
						}
					}									
					
					if($totrec-($this->item_per_page*$this->display_page_no) && ($totrec>(($from_page+$this->display_page_no-1)*$this->item_per_page)))
					{
						$navigation.= "<li class='next'><a href='#' onClick=\"$function_name('$page_name&from=".$from."&from_page=".($from_page+$this->shift_page_no).$category_link."');return false;\" >".$this->next_text."</a></li>";
					}
					else
					{
						$navigation.= "&nbsp;";
					}
				}
			}
			$limit_sql=" LIMIT $from , ".$this->item_per_page;
			$sql=$sql.$limit_sql;
			
			$num=mysql_num_rows(mysql_query($sql));
			
			$totalrecord="<span style='font:normal 10px/13px Verdana, Arial, Helvetica, sans-serif;'>Showing ".($from+1)." To ".($from+$num)."</span>";
			$return_arr=array($sql,$navigation,$totalrecord,$totrec);
			
			
			return $return_arr;							
		}
		
			function PaginationFrontAjaxALL($function_name,$sql,$page_name,$CategoryID="",$CategoryName="",$link="",$totalText='')
		{		
			global $from;
			global $from_page;
			$navigation1 = array();
			if(empty($from))
			{
				$from=0;
			}
			if(empty($from_page))
			{
				$from_page=1;
			}
			if(!empty($CategoryID))
				$category_link="&$CategoryName=".$CategoryID;			
			if(!empty($link))
				$link = ",'$link'";
				
			$totrec=mysql_num_rows(mysql_query($sql));
			
			if($this->display_page_no%2==0)
			{
				 $diff=ceil(ceil($this->display_page_no/2)/2);
			}
			else
			{
				 $diff=ceil(ceil($this->display_page_no/2)/2);
			}
			if($totrec>0)
			{
				if($totrec>$this->item_per_page)
				{
					$prev=$from-$this->item_per_page;
					$next=$from+$this->item_per_page;					
					if($totrec-($this->item_per_page*$this->display_page_no)  && $from_page>1)
					{
						$navigation.= "<li><a href='#' onclick=\"$function_name('$page_name&action=list_paginate&from=".$from."&amp;from_page=".($from_page-$this->shift_page_no).$category_link."')\" >Prev</a></li>";



					}

					else

					{

						$navigation.= "";

					}					

					for($i=1; $i<=ceil($totrec/$this->item_per_page);$i++)

					{

						if($i>=$from_page && $i<=($from_page+$this->display_page_no-1))

						{

							$num_len=strlen($i);							

							if((($from/$this->item_per_page)+1)==$i)

							{

								$navigation.= "<li><a href='#' class='pageselect'>".$i."</a></li> ";

							}

							else

							{

								$startval=($i-1)*$this->item_per_page;

								$navigation.= "<li><a href='#' onclick=\"$function_name('$page_name&action=list_paginate&amp;from=".$startval."&amp;from_page=".$from_page.$category_link."'); return false;\" >".$i."</a></li> ";

							}

						}

					}									

					if($totrec-($this->item_per_page*$this->display_page_no) && ($totrec>(($from_page+$this->display_page_no-1)*$this->item_per_page)))

					{

						$navigation.= "<li><a href='#' onClick=\"$function_name('$page_name&action=list_paginate&from=".$from."&amp;from_page=".($from_page+$this->shift_page_no).$category_link."')\" >Next</a></li>";

					}

					else

					{

						$navigation.= " ";

					}

				}

			}

			$limit_sql=" LIMIT $from , ".$this->item_per_page;

			$sql=$sql.$limit_sql;

			

			$num=mysql_num_rows(mysql_query($sql));

			

			//$totalrecord="<b>Total Records:</b> ".$totrec." <b><br>(Showing</b> ".($from+1)." <b>To</b> ".($from+$num)."<b>)</b> ";
            $totalrecord = "Displaying <b>".($from+1)."</b> - <b>".($from+$num)."</b> of <b>".$totrec."</b> Results";
			$return_arr=array($sql,$navigation,$totalrecord,$totrec);

			return $return_arr;						
		}
		
		function PaginationFormFrontAjax($select_box_id)
		{	
			$ItemPerPageArr=array(2,5,10,15,20,25,50);
			$PaginationFormStr="<select name='$select_box_id' id='$select_box_id'>";	
			for($i=0;$i<count($ItemPerPageArr);$i++)
			{
				if($_SESSION['item_per_page_session']==$ItemPerPageArr[$i])				
					$OptionSelected="Selected";			
				else
					$OptionSelected="";
										
				$PaginationFormStr .="<option value='".$ItemPerPageArr[$i]."' $OptionSelected >".$ItemPerPageArr[$i]."</option>";
			}
			$PaginationFormStr .="</select>";
			
			return $PaginationFormStr;
		}
		
	}	
?>