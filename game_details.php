<?php
include_once('general_include.php');
include('class/json.php');

if(isset($_POST['action']) && $_POST['action'] == "get_content"){
	include_once("game_extra_content.php");
}
else{	   
	$game_id = $_REQUEST['id'];
	
	$SelectgameSql="SELECT * FROM ".TABLEPREFIX."_game WHERE game_id=".$game_id;
	$RsgameSql = $UserManagerObjAjax->GetRecords("Row",$SelectgameSql);
	
	
	$is_ladder					= $RsgameSql["is_ladder"];
	$is_race					= $RsgameSql["is_race"];
	$is_server					= $RsgameSql["is_server"];
	$is_region					= $RsgameSql["is_region"];
	$is_rating					= $RsgameSql["is_rating"];
   
   $is_versus					= $RsgameSql["is_versus"];
	$is_team					= $RsgameSql["is_team"];
	$is_type					= $RsgameSql["is_type"];
	$is_champion				= $RsgameSql["is_champion"];
	$is_map						= $RsgameSql["is_map"];
	$is_mode					= $RsgameSql["is_mode"];
	$is_class					= $RsgameSql["is_class"];
	
	if($is_vedio=='1')
	{
	
		
		echo json_encode(
			array(
				'ladder' 	=> $is_ladder,
				'versus'    => $is_versus,
				'team'		=> $is_team,
				'type' 		=> $is_type,
				'champion'  => $is_champion,
				'map'		=> $is_map,
				'mode' 		=> $is_mode,
				'is_class' 	=> $is_class
			)
		);
	
	}
	else
	{
   
	
	if($is_ladder=='Y')
	{
		$LadderSql = "SELECT *  FROM ".TABLEPREFIX."_game_ladder WHERE game_id=".$game_id." ORDER BY date_added DESC ";   
		$LadderArr = $UserManagerObjAjax->GetRecords("All",$LadderSql);
	}
	
	if($is_race=='Y')
	{
	$RaceSql = "SELECT *  FROM ".TABLEPREFIX."_game_race WHERE game_id=".$game_id." ORDER BY date_added DESC ";   
	$RaceArr = $UserManagerObjAjax->GetRecords("All",$RaceSql);
	
	/*$race_str .='<td align="left" valign="top" width="30%" class="plaintxt">Race<span class="red">*</span>:</td>	
			<td width="70%" align="left" valign="middle" class="plaintxt">';
			 for($i=0;$i<count($RaceArr);$i++)
			 { 
			  $race_str .='<input type="checkbox" name="submited_race[]"  value="'.$RaceArr[$i]['race_id'].'" />'.$RaceArr[$i]['race_title']; 
			 }	
	$race_str .='<br /></td>';*/
	}
	

	
	if($is_rating=='Y')
	{
	$RatingSql = "SELECT *  FROM ".TABLEPREFIX."_game_rating  WHERE game_id=".$game_id." ORDER BY date_added DESC ";   
	$RatingArr = $UserManagerObjAjax->GetRecords("All",$RatingSql);
	}
	if($is_region=='Y')
	{
		$RegionSql = "SELECT *  FROM ".TABLEPREFIX."_game_region WHERE game_id=".$game_id." ORDER BY date_added DESC ";   
		$RegionArr = $UserManagerObjAjax->GetRecords("All",$RegionSql);
	}
	
	if($is_server=='Y')
	{
		$ServerSql = "SELECT *  FROM ".TABLEPREFIX."_game_server WHERE game_id=".$game_id." ORDER BY date_added DESC ";   
		$ServerArr = $UserManagerObjAjax->GetRecords("All",$ServerSql);
	}

  
	echo json_encode(
		array(
			'ladder' 	=> $is_ladder,
			'race'      => $is_race,
			'server'	=> $is_server,
			'region' 	=> $is_region,
			'rating' 	=> $is_rating
		)
	);
}
}

?>