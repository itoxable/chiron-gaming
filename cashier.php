<?php
	$page_name=basename($_SERVER['SCRIPT_FILENAME']);
	$IsPreserved	= 'Y';
	$IsProcess		= $_REQUEST['IsProcess'];
	include("general_include.php");
	include "checklogin.php";
 
	if($IsProcess <> 'Y')
	{
		include "top.php";
	}

	$SqlUserPoints = "SELECT total_points FROM ".TABLEPREFIX."_user where user_id='".$_SESSION['user_id']."'";;
	$userArr = $UserManagerObjAjax->GetRecords("Row",$SqlUserPoints);
	$totalPoints = $userArr['total_points'];
	
	$action = isset($_POST['cashier_action']) ? $_POST['cashier_action'] : "cashier";

	if($IsProcess == 'Y'){
		$Sqltype = "SELECT * FROM ".TABLEPREFIX."_user_type_user_relation WHERE user_id='".$_SESSION['user_id']."'";
		$typeArr = $UserManagerObjAjax->GetRecords("All",$Sqltype);
		$Numtype = count($typeArr);
		$is_coach = 0;
		$is_partner = 0;
		for($t=0;$t<$Numtype;$t++)
		{
		   if($typeArr[$t]['user_type_id']==1)
			  $is_coach = 1;
		   if($typeArr[$t]['user_type_id']==3)
			  $is_partner = 1;
		}
		
	}
	
	$sqlQuery = "SELECT * FROM ".TABLEPREFIX."_user_transaction WHERE nk_user_id='".$_SESSION['user_id']."' ORDER BY tx_date DESC, nk_transaction_id";
	$transactionList = $UserManagerObjAjax->GetRecords("All",$sqlQuery);
	
	$numberOfTransactions=count($transactionList);
	
	if (isset($_GET['txStatus']) && isset($_GET['txMsg'])) {
		$smarty->assign('tx_status',$_GET['txStatus']);
		$smarty->assign('tx_msg',$_GET['txMsg']);
	}
	
	$cashierAction = isset($_POST['cashierAction']) ? $_POST['cashierAction'] : "cashierPanel";
	
	
	$txMsg = $_GET['txMsg'];
	
	$smarty->assign('is_coach',$is_coach);
	$smarty->assign('is_partner',$is_partner);
	
	$smarty->assign('totalPoints',$totalPoints);
	$smarty->assign('transactionList', $transactionList);
	$smarty->assign('numberOfTransactions', $numberOfTransactions);
	$smarty->assign('cashierAction', $cashierAction);
	$smarty->assign('txMsg', $txMsg);
	
	$smarty->display('cashier.tpl');
	if($IsProcess <> 'Y')
	{
		include "footer.php";
	}
?>