// code for blank checking
/*
function IsBlank(str)
{
 var nCheck =str.value;
 var ln_length =nCheck.length;
 var i =0;
 var all_blank =true;
 if(ln_length <1)return true;
 while (i <ln_length &&all_blank)
 {
  if ((nCheck.charAt(i)!=" "))all_blank =false;i++;
 }
 return all_blank;
}
*/
// end of code for blank checking



// Validation.js


// VARIABLE DECLARATIONS

var digits = "0123456789";

var lowercaseLetters = "abcdefghijklmnopqrstuvwxyz"

var uppercaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


// whitespace characters
var whitespace = " \t\n\r";


// decimal point character differs by language and culture
var decimalPointDelimiter = "."


// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()-+ ";


// characters which are allowed in US phone numbers
var validUSPhoneChars = digits + phoneNumberDelimiters;


// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = digits + phoneNumberDelimiters + "+";


// U.S. phone numbers have 10 digits.
// They are formatted as 123 456 7890 or (123) 456-7890.
var digitsInUSPhoneNumber = 10;



// CONSTANT STRING DECLARATIONS
// (grouped for ease of translation and localization)

// m is an abbreviation for "missing"

var mPrefix = "You did not enter a value into the "
var mSuffix = " field. This is a required field. Please enter it now."

// s is an abbreviation for "string"

var sPhone = "Phone Number"
var sFax = "Fax Number"


// i is an abbreviation for "invalid"

var iUSPhone = "This field must be a 10 digit phone number (like 415 555 1212). Please reenter it now."
var iWorldPhone = "This field must be a valid international phone number. Please reenter it now."

var defaultEmptyOK = false

// Check whether string s is empty.

function isEmpty(s)
{   return ((s == null) || (s.length == 0))
}



// Returns true if string s is empty or
// whitespace characters only.

function isWhitespace (s)

{   var i;

    // Is s empty?
    if (isEmpty(s)) return true;


    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);

        if (whitespace.indexOf(c) == -1) return false;
    }

    // All characters are whitespace.
    return true;
}



// Removes all characters which appear in string bag from string s.

function stripCharsInBag (s, bag)

{   var i;
    var returnString = "";

    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.

    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }

    return returnString;
}

function stripCharsNotInBag (s, bag)

{   var i;
    var returnString = "";
    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) != -1) returnString += c;
    }

    return returnString;
}

function stripWhitespace (s)

{   return stripCharsInBag (s, whitespace)
}

function charInString (c, s)
{   for (i = 0; i < s.length; i++)
    {   if (s.charAt(i) == c) return true;
    }
    return false
}

function stripInitialWhitespace (s)

{   var i = 0;

    while ((i < s.length) && charInString (s.charAt(i), whitespace))
       i++;

    return s.substring (i, s.length);
}

function isLetter (c)
{   return ( ((c >= "a") && (c <= "z")) || ((c >= "A") && (c <= "Z")) )
}



// Returns true if character c is a digit
// (0 .. 9).

function isDigit (c)
{   return ((c >= "0") && (c <= "9"))
}



// Returns true if character c is a letter or digit.

function isLetterOrDigit (c)
{   return (isLetter(c) || isDigit(c))
}



function isInteger (s)

{   var i;

    if (isEmpty(s))
       if (isInteger.arguments.length == 1) return false;
       else return (isInteger.arguments[1] == true);
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);

        if (!isDigit(c)) return false;
    }

    // All characters are numbers.
    return true;
}


function isSignedInteger (s)

{   if (isEmpty(s))
       if (isSignedInteger.arguments.length == 1) return false;
       else return (isSignedInteger.arguments[1] == true);

    else {
        var startPos = 0;
        var secondArg = false;

        if (isSignedInteger.arguments.length > 1)
            secondArg = isSignedInteger.arguments[1];

        // skip leading + or -
        if ( (s.charAt(0) == "-") || (s.charAt(0) == "+") )
           startPos = 1;
        return (isInteger(s.substring(startPos, s.length), secondArg))
    }
}

function isPositiveInteger (s)
{   var secondArg = defaultEmptyOK;

    if (isPositiveInteger.arguments.length > 1)
        secondArg = isPositiveInteger.arguments[1];
    return (isSignedInteger(s, secondArg)
         && ( (isEmpty(s) && secondArg)  || (parseInt (s) > 0) ) );
}

function reformat (s)

{   var arg;
    var sPos = 0;
    var resultString = "";

    for (var i = 1; i < reformat.arguments.length; i++) {
       arg = reformat.arguments[i];
       if (i % 2 == 1) resultString += arg;
       else {
           resultString += s.substring(sPos, sPos + arg);
           sPos += arg;
       }
    }
    return resultString;
}

function isUSPhoneNumber (s)
{   if (isEmpty(s))
       if (isUSPhoneNumber.arguments.length == 1) return defaultEmptyOK;
       else return (isUSPhoneNumber.arguments[1] == true);
    return (isInteger(s) && s.length == digitsInUSPhoneNumber)
}

function isInternationalPhoneNumber (s)
{   if (isEmpty(s))
       if (isInternationalPhoneNumber.arguments.length == 1) return defaultEmptyOK;
       else return (isInternationalPhoneNumber.arguments[1] == true);
    return (isPositiveInteger(s))
}


function isIntegerInRange (s, a, b)
{   if (isEmpty(s))
       if (isIntegerInRange.arguments.length == 1) return defaultEmptyOK;
       else return (isIntegerInRange.arguments[1] == true);

    // Catch non-integer strings to avoid creating a NaN below,
    // which isn't available on JavaScript 1.0 for Windows.
    if (!isInteger(s, false)) return false;

    // Now, explicitly change the type to integer via parseInt
    // so that the comparison code below will work both on
    // JavaScript 1.2 (which typechecks in equality comparisons)
    // and JavaScript 1.1 and before (which doesn't).
    var num = parseInt (s);
    return ((num >= a) && (num <= b));
}


function warnEmpty (theField, s)
{
    alert(mPrefix + s + mSuffix)
    theField.focus()
    return false
}

function warnInvalid (theField, s)
{
    theField.select()
    alert(s)
    theField.focus()
    return false
}

function checkString (theField, s, emptyOK)
{   // Next line is needed on NN3 to avoid "undefined is not a number" error
    // in equality comparison below.
    if (checkString.arguments.length == 2) emptyOK = defaultEmptyOK;
    if ((emptyOK == true) && (isEmpty(theField.value))) return true;
    if (isWhitespace(theField.value))
       return warnEmpty (theField, s);

    else return true;
}

// takes USPhone, a string of 10 digits
// and reformats as (123) 456-789

function reformatUSPhone (USPhone)
{   return (reformat (USPhone, "(", 3, ") ", 3, "-", 4))
}

function checkUSPhone (theField, emptyOK)
{   if (checkUSPhone.arguments.length == 1) emptyOK = defaultEmptyOK;
    if ((emptyOK == true) && (isEmpty(theField.value))) return true;
    else
    {  var normalizedPhone = stripCharsInBag(theField.value, phoneNumberDelimiters)
       if (!isUSPhoneNumber(normalizedPhone, false))
          return warnInvalid (theField, iUSPhone);
       else
       {  // if you don't want to reformat as (123) 456-789, comment next line out
          theField.value = reformatUSPhone(normalizedPhone)
          return true;
       }
    }
    return false;
}

function checkInternationalPhone (theField, emptyOK)
{   if (checkInternationalPhone.arguments.length == 1) emptyOK = defaultEmptyOK;
    if ((emptyOK == true) && (isEmpty(theField.value))) return true;
    else
        var thisPhone = (stripCharsInBag(theField.value,phoneNumberDelimiters));
    {  if (!isInternationalPhoneNumber(thisPhone, false))
          return warnInvalid (theField, iWorldPhone);
       else return true;
    }
    return false;
}



//********************
        function ChkChar(str)
                {
                var check=0;
                var len = str.length
                 for (var i=0;i<len;++i)
                {
                  if(str.charCodeAt(i)<65 || str.charCodeAt(i)>122)
                   {
                        check=1;
                        if (i>0)
                        {
                                if(str.charCodeAt(i)==32)
                                {
                                        check=0;
                                }
                        }
                  }
                  else
                  {
                        check=0;
                  }
                  if (check==1)
                  {
                        return true;
                  }
                }
                return false;
        }
//***********Char validation end*****************
 function ChkPhone(no){
        var find=/[a-zA-Z\*\`\~\!\#\^\|\.\,\"\'\;\/\\\=\_\>\<\:\@\&\%\?\$]/;
        if (no.search(find)!= -1)
                {
                        return false;
                }
       //  var normalizedPhone = stripCharsInBag(no, phoneNumberDelimiters)
         // if you don't want to reformat as (123) 456-789, comment next line out
       //  no = reformatUSPhone(normalizedPhone)

    }
//****Phone validation end****************
 function ChkSpecial(str)
                {
                //var test=/[?\>\<\*\.\:\;\@]/;
                var test=/[@]/;
                if (str.search(test)!= -1)
                {
                        return false;
                }
                }
/****************************************************************/

function ChkNum(str)
                {
                //var test=/[?\>\<\*\.\:\;\@]/;
                var test=/[-/+/=/.]/;
                if (str.search(test)!= -1)
                {
                        return false;
                }
                }
/****************************************************************/

// Returns true if the string passed in is a valid money
//  (no alpha characters except a decimal place),
//   else it displays an error message

function ForceMoney(str)
{
    var strField = new String(str);

    var i = 0;

    for (i = 0; i < strField.length; i++)
    {
        if ((strField.charAt(i) < '0' || strField.charAt(i) > '9') && (strField.charAt(i) != '.')) {
            return false;
        }
    }
}


/****************************************************************/
function frmValidate(frmName,fldName,display,IsBlank,CharNumAdvPhDc){
                var FormName;
                var FldName;
                var Display;
                var Blank;
                var Special;
                FormName=frmName;
                FldName=fldName;
                Display=display;
                Blank=IsBlank;
                Special=CharNumAdvPhDc;
                var val;

val=eval("document."+FormName+"."+FldName+".value");
                if (Blank=='YES'){
                        if (val==""){
                                alert(""+ Display +" cannot be blank.");

eval("document."+FormName+"."+FldName+".focus()");
                                return false;
                        }
                        var check;
                        var len = val.length
                         for (var i=0;i<len;++i)
                        {
                        if (val.charCodeAt(i)!=32)
                                {
                                        check=1;
                                }
                        }
                        if (check!='1'){
                                alert(""+ Display +" cannot be blank.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
                                return false;
                        }

                }

                if (Special=='Adv'){
                        if (ChkSpecial(val)==false){
                                alert(""+ Display +" should be filled up properly.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
                                return false;
                        }
                }

                if (Special=='Char'){
                        if (ChkChar(val)){
                                alert(""+ Display +" can contain characters only.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
                                return false;
                        }
                }

                                if (Special=='Ph'){
                        if (ChkPhone(val)==false){
                                alert(""+ Display +" should be filled up properly.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
                                return false;
                        }

                }

                if (Special=='Num'){
                        if (isNaN(val)==true){
                                alert(""+ Display +" can contain numeric only.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
                                return false;
                        }
                        if (ChkNum(val)==false){
                                alert(""+ Display +" can contain numeric only without any '+' or '-' sign.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
                                return false;
                        }
                }


                if (Special=='Dc'){
                        if (ForceMoney(val)==false){
                                alert(""+ Display +" can contain numeric characters with decimal only.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
                                return false;
                        }
                }
        }
//*************for date
function checkDate(value){
   var dateregex=/^[]*[0]?(\d{1,2})\/(\d{1,2})\/(\d{2,})[ ]*$/;
   var match=value.match(dateregex);
   if (match) {
        var tmpdate=new
Date('20'+match[3],parseInt(match[1])-1,match[2]);
        if (tmpdate.getDate()==parseInt(match[2]) && tmpdate.getFullYear()==parseInt('20'+match[3]) && (tmpdate.getMonth()+1)==parseInt(match[1])){
        return true;
        }
   }
   return false;
}
//**************************************

  //email validation starts
  function ChkEmail(frmName,strEmail){
                var FormName;
                var FldName;
                FormName=frmName;
                FldName=strEmail;
          var
str=eval("document."+FormName+"."+FldName+".value");
if (str=="")
{
        alert("Please enter Email Address.");

eval("document."+FormName+"."+FldName+".focus()");
        return false;
}
if (str!="")
{
if (str.indexOf("@",1) == -1)
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
if (str.indexOf("@",1)== 0)
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
if (str.indexOf(".")== 0)
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
if (str.indexOf(".",1) == -1)
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }

// extra validation
var posat=str.indexOf("@");
var posdot=str.indexOf(".");
var rposdot=str.lastIndexOf(".");
if(rposdot==posdot)
if((posdot < posat) || (posdot-posat < 3))
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
if(str.charAt(str.length-1)==".")
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
if(str.charAt(str.length-1)=="@")
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
var j=0;
for( var i=0;i<str.length;i++)
{
if(str.charAt(i) == "@")
j++;
}
if(j > 1)
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
}
}

//email validation starts
  function ChkEmails(frmName,strEmail){
                var FormName;
                var FldName;
                FormName=frmName;
                FldName=strEmail;
          var str=eval("document."+FormName+"."+FldName+".value");

if (str!="")
{
if (str.indexOf("@",1) == -1)
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
if (str.indexOf("@",1)== 0)
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
if (str.indexOf(".")== 0)
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
if (str.indexOf(".",1) == -1)
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }

// extra validation
var posat=str.indexOf("@");
var posdot=str.indexOf(".");
var rposdot=str.lastIndexOf(".");
if(rposdot==posdot)
if((posdot < posat) || (posdot-posat < 3))
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
if(str.charAt(str.length-1)==".")
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
if(str.charAt(str.length-1)=="@")
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
var j=0;
for( var i=0;i<str.length;i++)
{
if(str.charAt(i) == "@")
j++;
}
if(j > 1)
        {
        alert("That is not a valid Email address. Please enter again.");

eval("document."+FormName+"."+FldName+".focus()");

eval("document."+FormName+"."+FldName+".select()");
        return false;
        }
}
}


function ChkSelect(frmName,fldName,display){
                var FormName;
                var FldName;
                var Display;
                FormName=frmName;
                FldName=fldName;
                Display=display;
        var val;

val=eval("document."+FormName+"."+FldName+".options[document."+FormName+"."+FldName+".selectedIndex].value");
        if (val==""){
                alert("Please, select the "+ Display +"");

eval("document."+FormName+"."+FldName+".focus()");
                return false;
                }
}





/*
<!--
var s;
var digitsInUSPhoneNumber = 10;
var phoneNumberDelimiters = "()-+ ";


// characters which are allowed in US phone numbers
var validUSPhoneChars = digits + phoneNumberDelimiters;


// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = digits + phoneNumberDelimiters + "+";
var iUSPhone = "This field must be a 10 digit U.S. phone number (like (415)-555-1212). Please reenter it now."
var iWorldPhone = "This field must be a valid international phone number. Please reenter it now."
function isEmpty(s)
{   return ((s == null) || (s.length == 0))
}

function isWhitespace (s)

{   var i;

    // Is s empty?
    if (isEmpty(s)) return true;

    // Search through string's characters one by one
    // until we find a non-whitespace character.
    // When we do, return false; if we don't, return true.

    for (i = 0; i < s.length; i++)
    {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);

        if (whitespace.indexOf(c) == -1) return false;
    }

    // All characters are whitespace.
    return true;
}

var n;
var p;
var p1;
var mPrefix = "You did not enter a value into the "
var mSuffix = " field. This is a required field. Please enter it now."
var whitespace = " \t\n\r";
var defaultEmptyOK = false
var digits = "0123456789";
function ValidatePhone(){


p=p1.value

if(p.length==3){
        //d10=p.indexOf('(')

        pp=p;
        d4=p.indexOf('(')
        d5=p.indexOf(')')

        if(d4==-1){
                pp="("+pp;

        }
        if(d5==-1){
                pp=pp+")";

        }
        //pp="("+pp+")";
        //document.frmPhone.txtphone.value="";
        //document.frmPhone.txtphone.value=pp;

        p1.value="";
        p1.value=pp;

}


if(p.length>3){

        d1=p.indexOf('(')
        d2=p.indexOf(')')


        if (d2==-1){

                l30=p.length;


                p30=p.substring(0,4);
                //alert(p30);
                p30=p30+")"
                p31=p.substring(4,l30);
                pp=p30+p31;
                //alert(p31);
                //document.frmPhone.txtphone.value="";
                //document.frmPhone.txtphone.value=pp;

                        p1.value="";
                        p1.value=pp;


        }
        }

if(p.length>5){


        p11=p.substring(d1+1,d2);
        if(p11.length>3){


        p12=p11;
        l12=p12.length;
        l15=p.length
        //l12=l12-3
        p13=p11.substring(0,3);
        p14=p11.substring(3,l12);
        p15=p.substring(d2+1,l15);



                        p1.value="";

        //document.frmPhone.txtphone.value="";
        pp="("+p13+")"+p14+p15;


        p1.value=pp;
        //document.frmPhone.txtphone.value=pp;
        //obj1.value="";
        //obj1.value=pp;


        }
        l16=p.length;
        p16=p.substring(d2+1,l16);

        l17=p16.length;
        if(l17>3&&p16.indexOf('-')==-1){
                p17=p.substring(d2+1,d2+4);

                p18=p.substring(d2+4,l16);

                p19=p.substring(0,d2+1);

        pp=p19+p17+"-"+p18;
        //document.frmPhone.txtphone.value="";
        //document.frmPhone.txtphone.value=pp;

        p1.value="";
        p1.value=pp;

        //obj1.value="";
        //obj1.value=pp;
        }

}

//}
setTimeout(ValidatePhone,100)
}
function isDigit (c)
{   return ((c >= "0") && (c <= "9"))
}

function isInteger (s)

{   var i;

    if (isEmpty(s))
       if (isInteger.arguments.length == 1) return defaultEmptyOK;
       else return (isInteger.arguments[1] == true);

    // Search through string's characters one by one
    // until we find a non-numeric character.
    // When we do, return false; if we don't, return true.

    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);

        if (!isDigit(c)) return false;
    }

    // All characters are numbers.
    return true;
}


function isUSPhoneNumber (s)
{   if (isEmpty(s))
       if (isUSPhoneNumber.arguments.length == 1) return defaultEmptyOK;
       else return (isUSPhoneNumber.arguments[1] == true);
    return (isInteger(s) && s.length == digitsInUSPhoneNumber)
}

function warnEmpty (theField, s)
{
    alert(mPrefix + s + mSuffix)
    theField.focus()
    return false
}
function getIt(m, fdName){
n=m.name;
//p1=document.forms[0].elements[n]
if (isWhitespace(m.value))
{
 return warnEmpty (m, fdName);
}
else
{

p1=m
isUSPhoneNumber()
//ValidatePhone()
}

}

function testphone(obj1){

p=obj1.value
//alert(p)
p=p.replace("(","")
p=p.replace(")","")
p=p.replace("-","")
p=p.replace("-","")
//alert(isNaN(p))
if (isNaN(p)==true){
alert("Check phone");
return false;

}

}


//-->
*/
function echeck(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   alert("Invalid E-mail ID")
		   return false
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert("Invalid E-mail ID")
		   return false
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    alert("Invalid E-mail ID")
		    return false
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    alert("Invalid E-mail ID")
		    return false
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Invalid E-mail ID")
		    return false
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    alert("Invalid E-mail ID")
		    return false
		 }
		
		 if (str.indexOf(" ")!=-1){
		    alert("Invalid E-mail ID")
		    return false
		 }

 		 return true					
	}
	

// Removes leading whitespaces
		function LTrim( value ) {
			
			var re = /\s*((\S+\s*)*)/;
			return value.replace(re, "$1");
			
		}
		
		// Removes ending whitespaces
		function RTrim( value ) {
			
			var re = /((\s*\S+)*)\s*/;
			return value.replace(re, "$1");
			
		}
		
		// Removes leading and ending whitespaces
		function trim( value ) {
			
			return LTrim(RTrim(value));
			
		}
		
