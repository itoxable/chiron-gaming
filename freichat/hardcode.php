<?php
/* Data base details */
require_once $_SERVER['DOCUMENT_ROOT'].'/adodb/DATABASE.php';


$con = DATABASE::$connection;
$username = DATABASE::$username;
$password = DATABASE::$password;
$client_db_name = DATABASE::$databasename;
$host = DATABASE::$host;
$driver='Custom';
$db_prefix='';
$uid='508b040f66acf';

$PATH = 'freichat/'; // Use this only if you have placed the freichat folder somewhere else
$installed=true;
$admin_pswd='123456';

$debug = true;

/* email plugin */
$smtp_username = '';
$smtp_password = '';



/* Custom driver */
$usertable='nk_user'; //specifies the name of the table in which your user information is stored.
$row_username='username'; //specifies the name of the field in which the user's name/display name is stored.
$row_userid='user_id'; //specifies the name of the field in which the user's id is stored (usually id or userid)
$avatar_field_name = 'photo';