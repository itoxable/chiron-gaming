<?php
//session_start();
include "general_include.php";

/* Deatils of Coach */
$page_name=basename($_SERVER['SCRIPT_FILENAME']);
$IsPreserved = 'Y';
$IsProcess = $_REQUEST['IsProcess'];
$action	= $_REQUEST['action'];
$training_partner_id = $_REQUEST['training_partner_id'];
//$game_id = $_REQUEST['game_id']; 
$logged_user=$_SESSION['user_id'];
if(isset($_REQUEST['game_id'])){ 
    $_SESSION['game_id'] = $_REQUEST['game_id'];
    $game_id = $_SESSION['game_id'];
}else{
    $game_id = $_SESSION['game_id'];
}


if($_REQUEST['action']=='sendreview'){
    $num_coach_rating_type=$_REQUEST['NumCoachRatingCat'];
    //$coach_id = $_POST['coach_id'];
    $user_type_id=$_POST['user_type_id'];
    $review_comment = $_POST['review_comment'];
    $reviewed_by=$_POST['reviewed_by'];
    $total_rating=0;

    for($i=0;$i<$num_coach_rating_type;$i++){
        $get_rating[$i]=$_REQUEST['Rating_'.$i.''];
        $get_comment[$i]=$_REQUEST['Comment_'.$i.''];
        $explode_str=explode(":",$get_rating[$i]);
        $rating[$i]=$explode_str[0];
        $rating_cat_id[$i]=$explode_str[1];
        $store_id[$i]=$_REQUEST['comment_rating_'.$i.''];
    }
	
    $overall_comment=$_REQUEST['Comment_overall'];
    $overall_rating=$_REQUEST['Rating_overall'];

    $CoachReviewSql="INSERT INTO ".TABLEPREFIX."_user_review (user_id,user_type_id,reviewed_by,review_comment,rating,is_active,date_added) VALUES ('".$training_partner_id."','".$user_type_id."','".$reviewed_by."','".$review_comment."','".$overall_rating."','Y','".date('Y-m-d')."')";
    $CoachReview=$UserManagerObjAjax->Execute($CoachReviewSql);
    $user_review_id=mysql_insert_id();
    if($num_coach_rating_type>0){
        for($i=0;$i<$num_coach_rating_type;$i++){
            $total_rating=$total_rating+$rating[$i];
            $CoachRatingSql="INSERT INTO ".TABLEPREFIX."_user_rating (user_review_id,rcat_id,rating) VALUES ('".$user_review_id."', '".$rating_cat_id[$i]."','".$rating[$i]."')";
            $CoachRating=$UserManagerObjAjax->Execute($CoachRatingSql);
        }

        if($CoachRating){
            $totreview = "SELECT count(*),sum(rating) as totrate FROM ".TABLEPREFIX."_user_review WHERE user_id='".$training_partner_id."' AND is_active='Y'";
            $totArr = $UserManagerObjAjax->GetRecords("Row",$totreview);
            $tot_count = $totArr[0];
            $tot_rate = $totArr['totrate'];
            $avg = $tot_rate/$tot_count;
            $UpdateSql = "UPDATE ".TABLEPREFIX."_user_type_user_relation SET overall_rating='".$avg."' WHERE user_id='".$training_partner_id."' and user_type_id='".$user_type_id."'";
            $UserManagerObjAjax->Execute($UpdateSql);
        }
    }
}


if($IsProcess <> 'Y'){
    include "top.php";
}
$RatingCatSql="SELECT rating_category,rcat_id, rating_category_tooltip FROM ".TABLEPREFIX."_rating_category WHERE rating_type='TP' ORDER BY rating_order";
$RatingCatDetails=$UserManagerObjAjax->GetRecords("All",$RatingCatSql);

$NumRatingCat=count($RatingCatDetails);

for($i=0;$i<$NumRatingCat;$i++){
    $RatingCatArr[$i]['rcat_id']=$RatingCatDetails[$i]['rcat_id'];
    $RatingCatArr[$i]['rating_category']=$RatingCatDetails[$i]['rating_category'];
    $RatingCatArr[$i]['rating_category_tooltip']=html_entity_decode($RatingCatDetails[$i]['rating_category_tooltip']);
}

$Sql_Individual="select avg(rat.rating) as tot,rat.rcat_id,cat.rating_category,cat.rating_category_tooltip from ".TABLEPREFIX."_user_rating as rat,".TABLEPREFIX."_user_review as rev,".TABLEPREFIX."_rating_category as cat where rat.rcat_id=cat.rcat_id and rat.user_review_id=rev.user_review_id and rev.user_id=".$training_partner_id." and rev.user_type_id=3 group by rat.rcat_id";
$Individual=$UserManagerObjAjax->GetRecords("All",$Sql_Individual);

for($j=0;$j<count($Individual);$j++){
    $CatIndividualRec[$j]['rating']=number_format($Individual[$j]['tot'], 1, '.', '');
    $CatIndividualRec[$j]['rcat_id']=$Individual[$j]['rcat_id'];
    $CatIndividualRec[$j]['rating_category']=$Individual[$j]['rating_category'];
    $CatIndividualRec[$j]['rating_category_tooltip']=html_entity_decode($Individual[$j]['rating_category_tooltip']);
}

$item_per_page=$_SESSION['item_per_page'] = $_REQUEST['item_per_page'];

if(empty($item_per_page) || !isset($item_per_page)){
    $item_per_page=10;
} 


$GameSql = "SELECT * FROM ".TABLEPREFIX."_game WHERE game_id='$game_id'";  
$gamerow = $UserManagerObjAjax->GetRecords("Row",$GameSql);
$game_id = $gamerow['game_id'];
$game_name = $gamerow['game_name'];

$prop_ids = $_REQUEST['prop_ids'];
$smarty->assign('prop_ids',$prop_ids);
$prop_id = explode(",",$_REQUEST['prop_ids']);
if($prop_id == '')
    $prop_id = array();
      
  // --------------------------------------------------------------------------//

 // Function for finding value 
function findvalue($table,$fld_name,$fld_value,$find_value){
    if($fld_value=='')
        return false;

    $SelectTypeSql = "SELECT ".$find_value." FROM ".$table." WHERE $fld_name=".$fld_value;
    $SelectType = mysql_fetch_array(mysql_query($SelectTypeSql));
    return trim($SelectType[0]);
}	

$tpSql="SELECT * FROM ".TABLEPREFIX."_user where user_id='$training_partner_id'";
$tpArr=$UserManagerObjAjax->GetRecords("Row",$tpSql);

$tpArr['name']=show_to_control($tpArr['name']);


$Clanguage = explode(",",$tpArr['language_ids']);
$language='';
for($l=0;$l<count($Clanguage);$l++){
    if($Clanguage[$l]<>'')
        $language .=findvalue(TABLEPREFIX."_language","language_id",$Clanguage[$l],"language_name").', ';
}
$tpArr['language']=substr($language,0,-2);   
$avail_type = explode(",",$tpArr['availability_type']);
if($avail_type[1] == ''){
    if($avail_type[0]=='O')
        $tpArr['availability_type'] = 'Online';
    if($avail_type[0]=='L'){ 
        $tpArr['availability_type'] = 'Local meet-up';
        $tpArr['avail_local'] = 'Y';
    } 
}
else{
    $tpArr['availability_type'] = 'Online , Local meet-up';  
    $tpArr['avail_local'] = 'Y';
}  

$tpArr['availability_country'] = findvalue(TABLEPREFIX."_country","country_id",$tpArr['availability_country'],"country_name");
$tpArr['availability_city'] = findvalue(TABLEPREFIX."_cities","city_id",$tpArr['availability_city'],"city_key");

$tpOverall="SELECT * FROM ".TABLEPREFIX."_user_type_user_relation WHERE user_id=".$training_partner_id." and user_type_id=3";
$tpOverallArr = $UserManagerObjAjax->GetRecords("Row",$tpOverall);
//$tpArr['about'] = $tpOverallArr['about'];

$tpOverallArr['overall_rating']=number_format($tpOverallArr['overall_rating'], 1, '.', '');

$Sql_overall_tooptip="select rating_overall_tooltip from ".TABLEPREFIX."_default_comision";
$overall_tooptip=$UserManagerObjAjax->GetRecords("Row",$Sql_overall_tooptip);
$tpOverallArr['rating_overall_tooltip']=html_entity_decode($overall_tooptip['rating_overall_tooltip']);


$tpgameSql = "SELECT * FROM ".TABLEPREFIX."_user_game where user_id='$training_partner_id' and user_type_id=3 and is_active='Y'";
$tpgameArr = $UserManagerObjAjax->GetRecords("All",$tpgameSql);
$Numtpgame=count($tpgameArr);
for($g=0;$g<$Numtpgame;$g++){
//    $gameSql = "SELECT * FROM ".TABLEPREFIX."_game WHERE game_id ='".$tpgameArr[$g]['game_id']."'";
//    $gameDetailArr =  $UserManagerObjAjax->GetRecords("Row",$gameSql);
    $gameSql = "SELECT * FROM ".TABLEPREFIX."_game g, nk_game_categories gc WHERE  g.game_id ='".$tpgameArr[$g]['game_id']."' AND gc.game_id = g.game_id ";
    $gameDetailArr =  $UserManagerObjAjax->GetRecords("All",$gameSql);
    $gameRow = $gameDetailArr[0];
    $numGameDetail=count($gameDetailArr);

    for($x=0; $x<$numGameDetail; $x++){
        $userCategoriesPropSql = "SELECT * FROM ".TABLEPREFIX."_user_game_property p, nk_game_categories_properties c WHERE p.user_id='".$training_partner_id."' AND p.user_type_id = 3 
            AND p.category_id = ".$gameDetailArr[$x]['category_id']." AND c.property_id = p.property_id";
        $userCategoriesPropArr = $UserManagerObjAjax->GetRecords("All",$userCategoriesPropSql);
        $NumuserCategoriesProp=count($userCategoriesPropArr);
        $prop ='';
        for($y=0; $y<$NumuserCategoriesProp; $y++){
            $prop .= (($y > 0)?', ':'').$userCategoriesPropArr[$y]['property_name'];
        }
        $gameDetailArr[$x]['properties'] = $prop;
    }
    $tpgameArr[$g]['categoriesArr'] = $gameDetailArr;  
    $tpgameArr[$g]['game_name']= $gameRow['game_name'];
        
//    $tpgameArr[$g]['game']= findvalue("".TABLEPREFIX."_game","game_id",$tpgameArr[$g]['game_id'],"game_name");
	
}

$tpvideoSql = "SELECT * FROM ".TABLEPREFIX."_video WHERE user_id='$training_partner_id' ORDER BY date_added DESC";
$tpvideoArr =  $UserManagerObjAjax->GetRecords("All",$tpvideoSql);
$Numvideo = count($tpvideoArr);
for($v=0;$v<$Numvideo;$v++)
{
    $tpvideoArr[$v]['game'] = findvalue(TABLEPREFIX."_game","game_id",$tpvideoArr[$v]['game_id'],"game_name");
	$dateSql = "SELECT date_format('".$tpvideoArr[$v]['date_added']."','%M  %d , %Y') as video_date";
	$DateArr = $UserManagerObjAjax->GetRecords("Row",$dateSql);
    $tpvideoArr[$v]['video_date']=$DateArr['video_date'];
	
	$LikeNumSql = "SELECT count(*) AS numlike FROM ".TABLEPREFIX."_user_like WHERE selected_for='L' AND video_id=".$tpvideoArr[$v]['video_id'] ;
	$LikeArrNum = $UserManagerObjAjax->GetRecords('Row',$LikeNumSql);
	
	$tpvideoArr[$v]['numlike']=$LikeArrNum['numlike'];
	
	$UnlikeNumSql = "SELECT count(*) AS numunlike FROM ".TABLEPREFIX."_user_like WHERE selected_for='D' AND  video_id=".$tpvideoArr[$v]['video_id'] ;
	$UnlikeArrNum = $UserManagerObjAjax->GetRecords('Row',$UnlikeNumSql);
	
	$tpvideoArr[$v]['numunlike']=$UnlikeArrNum['numunlike'];

    $viewCount = "SELECT count(*) FROM ".TABLEPREFIX."_video_view_count WHERE video_id =".$tpvideoArr[$v]['video_id'];
    $viewArr = $UserManagerObjAjax->GetRecords("Row",$viewCount);
	$tpvideoArr[$v]['view_count'] = $viewArr[0];
		
}


 $Reviews = "SELECT * FROM ".TABLEPREFIX."_user_review WHERE user_id='$training_partner_id' and user_type_id=3 and is_active='Y' ORDER BY date_added DESC";
 
 $PaginationObjAjaxLatest=new PaginationClassAjax($item_per_page,"prev",'',"next","active",$adodbcon);
 $pagination_arr = $PaginationObjAjaxLatest->PaginationAjax($Reviews,$page_name."?training_partner_id=".$training_partner_id."&action=".$action."&item_per_page=".$item_per_page,"Managergeneral");
 $UserReview = $UserManagerObjAjax->GetRecords("All",$pagination_arr[0]);

 $NumReview = count($UserReview);
 
 for($r=0;$r<$NumReview;$r++)
 {
    $UserReview[$r]['review_by'] = findvalue(TABLEPREFIX."_user","user_id",$UserReview[$r]['reviewed_by'],"name");
	
    $RdateSql = "SELECT date_format('".$UserReview[$r]['date_added']."','%M  %d , %Y') as review_date";
	$RdateArr = $UserManagerObjAjax->GetRecords("Row",$RdateSql);
	
	$UserReview[$r]['r_date']    = $RdateArr['review_date'];
	$UserReview[$r]['overall_c'] = $UserReview[$r]['overall_comment'];
	$UserReview[$r]['overall_r'] = $UserReview[$r]['rating'];
	
	$Sql_overall_tooptip="select rating_overall_tooltip from ".TABLEPREFIX."_default_comision";
	$overall_tooptip=$UserManagerObjAjax->GetRecords("Row",$Sql_overall_tooptip);
	$UserReview[$r]['rating_overall_tooltip']=html_entity_decode($overall_tooptip['rating_overall_tooltip']);

	//echo $Sql_Individual_User="select rat.rating,cat.rating_category from gt_user_rating as rat,gt_user_review as rev,gt_rating_category as cat where rat.rcat_id=cat.rcat_id and rat.user_review_id=rev.user_review_id and rev.user_review_id=".$UserReview[$r]['user_review_id']." and rev.reviewed_by=".$UserReview[$r]['reviewed_by']." group by cat.rating_category";
	$Sql_Individual_User="SELECT rat.rating, cat.rating_category, cat.rating_category_tooltip FROM ".TABLEPREFIX."_user_rating AS rat, ".TABLEPREFIX."_user_review AS rev, ".TABLEPREFIX."_rating_category AS cat WHERE rat.rcat_id = cat.rcat_id AND rat.user_review_id = rev.user_review_id AND rat.user_review_id =".$UserReview[$r]['user_review_id']." GROUP BY cat.rcat_id";
	$Individual_User =  $UserManagerObjAjax->GetRecords("All",$Sql_Individual_User);
	
	//$id=$UserReview[$r]['user_review_id'];
	for($k=0;$k<count($Individual_User);$k++){
            $UserReview_Individual[$k]['rating']=$Individual_User[$k]['rating'];
            $UserReview_Individual[$k]['rating_category']=$Individual_User[$k]['rating_category'];
            $UserReview_Individual[$k]['rating_category_tooltip']=html_entity_decode($Individual_User[$k]['rating_category_tooltip']);
	}
	$UserReview[$r]['individual'] = $UserReview_Individual; 
	
	$userImg = "SELECT photo FROM ".TABLEPREFIX."_user WHERE user_id='".$UserReview[$r]['reviewed_by']."'";
	$ImgArr = $UserManagerObjAjax->GetRecords("Row",$userImg);
	$UserReview[$r]['photo'] = $ImgArr[0];	
	
	$userType = "SELECT user_type_id FROM ".TABLEPREFIX."_user_type_user_relation WHERE user_id='".$UserReview[$r]['reviewed_by']."'";
	$TypeArr = $UserManagerObjAjax->GetRecords("Row",$userType);
	for($t=0;$t<count($TypeArr);$t++){
	  if($TypeArr[$t][0] == 1)
	     $coach = 1;
	  if($TypeArr[$t][0] == 3)
	     $partner = 1;	 
	}
	if($coach == '1')
	{
	  $UserReview[$r]['userlink'] = "coachdetails.php?coach_id=".$UserReview[$r]['reviewed_by'];
	  if($UserReview[$r]['photo'] == '')
	    $img = 'images/coach_thumb.jpg';
	  else
	  	$img = 'uploaded/user_images/thumbs/mid_'.$UserReview[$r]['photo'];
	} 
	else if($partner == 1)
	{
	   
	  $UserReview[$r]['userlink'] = "training_partner_details.php?training_partner_id=".$UserReview[$r]['reviewed_by'];
	  if($UserReview[$r]['photo'] == '')
	    $img = 'images/student_thumb.jpg';
	  else
	  	$img = 'uploaded/user_images/thumbs/mid_'.$UserReview[$r]['photo'];
	   
	  
	} 
	else
	{
	  $UserReview[$r]['userlink'] = "#"; 
	  if($UserReview[$r]['photo'] == '')
	    $img = 'images/student_thumb.jpg';
	  else
	  	$img = 'uploaded/user_images/thumbs/mid_'.$UserReview[$r]['photo'];
	}  
	  $UserReview[$r]['img'] = $img;
 }

$city = findvalue(TABLEPREFIX."_cities","city_id",$availability_city,"city_key");

$gameSql = "SELECT game_id,game_name FROM ".TABLEPREFIX."_game where status=1 ORDER BY game_name";
$GameArr = $UserManagerObjAjax->HtmlOptionArrayCreate($gameSql);

$online_status= getUserStatus($tpArr);
$friendsSql = "SELECT * FROM ".TABLEPREFIX."_user_friends_list WHERE (nk_user_id_owner='".$_SESSION['user_id']."' or nk_user_id_friend='".$_SESSION['user_id']."') and (nk_user_id_owner='".$training_partner_id."' or nk_user_id_friend='".$training_partner_id."')";
$friendsArr = $UserManagerObjAjax->GetRecords("All",$friendsSql);
$isfriend = "n";
for($t=0;$t<count($friendsArr);$t++){
	if($friendsArr[$t]['nk_user_id_friend'] == $training_partner_id || $friendsArr[$t]['nk_user_id_owner'] == $training_partner_id){
		$isfriend = "y";
		break;
	}
}

$smarty->assign('isfriend',$isfriend);
$smarty->assign('online_status',$online_status);

$smarty->assign('prop_id',$prop_id);
$smarty->assign('language_id',$language_id);
$smarty->assign('availability',$availability);
$smarty->assign('city',$city);
$smarty->assign('cityid',$availability_city);
$smarty->assign('availability_country',$availability_country);
$smarty->assign('availability_city',$availability_city);
$smarty->assign('game_id',$game_id);
$smarty->assign("GameArr",$GameArr);
$smarty->assign('NumReview',$NumReview);
$smarty->assign('UserReview',$UserReview);
$smarty->assign('Numtpgame',$Numtpgame);
$smarty->assign('RatingCatArr',$RatingCatArr);
$smarty->assign('Numvideo',$Numvideo);
$smarty->assign('tpvideoArr',$tpvideoArr);
$smarty->assign('tpgameArr',$tpgameArr);
$smarty->assign('tpOverallArr',$tpOverallArr);
$smarty->assign('tpArr',$tpArr);
$smarty->assign('training_partner_id',$training_partner_id);
$smarty->assign('IsProcess',$IsProcess);
$smarty->assign('pagination_arr',$pagination_arr);
$smarty->assign('page_name',$page_name);
$smarty->assign('NumRatingCat',$NumRatingCat);
$smarty->assign('logged_user',$logged_user);
$smarty->assign('CatIndividualRec',$CatIndividualRec);
$smarty->assign('UserReview_Individual',$UserReview_Individual);
$smarty->assign('CoachOverallArr',$CoachOverallArr);
$smarty->assign('ratingArr',$ratingArr);
if($IsProcess <> 'Y')
  include "training_partner_leftpanel.php";
$smarty->display('training_partner_details.tpl');
if($IsProcess <> 'Y')
   include "footer.php";
?>