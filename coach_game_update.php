<?php
include("general_include.php");
include "checklogin.php";
include "top.php";
//include "left.php";
$profile = $_REQUEST['profile'];
$user_ladder_id = $_REQUEST['user_ladder_id'];
$extra_content = "";

if(isset($_POST['submit'])){
   $Formval = $_POST;
   $game_id = $_POST['game_id'];
   $about = $_POST['about-text'];
   $experience = $_POST['experience'];
   $description = $_POST['description'];
   $rateAvalue = $_POST['rateA'];
   $rateBvalue = $_POST['rateB'];
   $rateCvalue = $_POST['rateC'];
   $lesson_plan = $_POST['lesson_plan'];
   $user_game_id = $_POST['user_game_id'];
   $is_active = $_POST['is_active'] =="Y" ? "Y" : "N";
   $profile = $_POST['profile'];
   $is_partner = $_POST['is_partner'];
   $paypal_email_id = $_POST['paypal_email_id'];
   $availability_type1 = trim($_POST['availability_type1']);
   $availability_type2 = trim($_POST['availability_type2']);
   $availability_city = trim($_POST['availability_city']);
   $cityid = $_POST['cityid'];
   $prop_ids = $_REQUEST['prop_id'];
   $action	= $_REQUEST['action'];
   $valid=1;
   
   ob_start();

   $allPropssql = "SELECT * FROM `nk_user_game_property` WHERE game_id=".$game_id." AND user_id=".$_SESSION['user_id']." AND user_type_id=1";
   $allPropsArr = $UserManagerObjAjax->GetRecords("All",$allPropssql);
   $NumAllPropsArr=count($allPropsArr);
   
   $gameCategoriesSql = "SELECT * FROM ".TABLEPREFIX."_game_categories WHERE game_id=".$game_id;
   $gameCategoriesArr = $UserManagerObjAjax->GetRecords("All",$gameCategoriesSql);
   $NumGameCategories=count($gameCategoriesArr);
   for($x=0; $x<$NumGameCategories; $x++){
        $userCategoriesPropSql = "SELECT * FROM ".TABLEPREFIX."_game_categories_properties WHERE category_id = ".$gameCategoriesArr[$x]['category_id'];
        $categoriesPropArr = $UserManagerObjAjax->GetRecords("All",$userCategoriesPropSql);
        $gameCategoriesArr[$x]['properties'] = $categoriesPropArr;
    }
   
   ob_end_clean();
   $proptxt_id = isset($_POST['prop_id']) && is_array($_POST['prop_id']) ? implode(",",$_POST['prop_id']) : '';
	
    if(empty($_POST['user_game_id'])){
        $dupchk = "SELECT * FROM ".TABLEPREFIX."_user_game where game_id='$game_id' and user_type_id='1' and user_id='".$_SESSION['user_id']."'";
        $chkArr = $UserManagerObjAjax->GetRecords('Row',$dupchk);
        $Numrow = count($chkArr);
        if($Numrow > 0){
            $ermsg ='This game is already added';
            $valid = 0;  
        }
        if($valid ==1){
            if($is_coach == 0 && $profile == 'Y'){	
               // Availability Type
                if($availability_type1!='' && $availability_type2!='')
                     $availability_type = $availability_type1.','.$availability_type2;
                if($availability_type1!='' && $availability_type2=='')   
                    $availability_type = $availability_type1;
                if($availability_type2!='' && $availability_type1=='')   
                    $availability_type = $availability_type2; 
                if($cityid!= '' && $availability_type2!=''){
                    $sqlCity = "SELECT * FROM ".TABLEPREFIX."_cities WHERE city_id='$cityid'";
                    $rowCity = $UserManagerObjAjax->GetRecords("Row",$sqlCity);
                    $availability_country = $rowCity['country_id'];
                } 
                $updateSQl = "UPDATE ".TABLEPREFIX."_user set availability_type='$availability_type',availability_city='$cityid',availability_country='$availability_country'
                ,description='".addslashes($description)."' WHERE user_id='".$_SESSION['user_id']."'";
                $UserManagerObjAjax->Execute($updateSQl);	 
             }
             if($profile == 'Y'){
                $updateSQl = "UPDATE ".TABLEPREFIX."_user set  paypal_email_id='$paypal_email_id' WHERE user_id='".$_SESSION['user_id']."'";
                $UserManagerObjAjax->Execute($updateSQl);

                $Updatetype = "INSERT INTO ".TABLEPREFIX."_user_type_user_relation set user_type_id='1',user_id='".$_SESSION['user_id']."',date_added='".date('Y-m-d')."' ,about= '".$about."'";
                $UserManagerObjAjax->Execute($Updatetype);
             }		  
             $removesql = "DELETE FROM `nk_user_game_property` WHERE game_id=".$game_id." AND user_id=".$_SESSION['user_id']." AND user_type_id=1";
             $UserManagerObjAjax->Execute($removesql);
             foreach ($prop_ids as $prop_details){
                 list ($category_id, $prop_id) = split ('_', $prop_details);
                 $sql = "INSERT INTO nk_user_game_property SET user_id=".$_SESSION['user_id'].", user_type_id=1, property_id=".$prop_id.", category_id=".$category_id;
                 logToFile($sql);
                 $UserManagerObjAjax->Execute($sql);
             }

        } 	   
   }
   else if(!empty($_POST['user_game_id'])){
        $dupchk = "SELECT * FROM ".TABLEPREFIX."_user_game where game_id='$game_id' and user_type_id='1' and user_id='".$_SESSION['user_id']."' and user_game_id!='$user_game_id'";
        $chkArr = $UserManagerObjAjax->GetRecords('Row',$dupchk);
        $Numrow = count($chkArr);
        if($Numrow > 0){
             $ermsg ='This game is already added';
             $valid = 0;  
        }
        if($valid ==1){ 
             $removesql = "DELETE FROM `nk_user_game_property` WHERE game_id=".$game_id." AND user_id=".$_SESSION['user_id']." AND user_type_id=1";
             $UserManagerObjAjax->Execute($removesql);   
             foreach ($prop_ids as $prop_details){
                 list ($category_id, $prop_id) = split ('_', $prop_details);
                 $sql = "INSERT INTO nk_user_game_property SET user_id=".$_SESSION['user_id'].", user_type_id=1, property_id=".$prop_id.", category_id=".$category_id.", game_id=".$game_id;
                 logToFile($sql);
                 $UserManagerObjAjax->Execute($sql);
             }
             if(!mysql_error()){
                 echo "<script>window.location.href='coach_game.php'</script>";
             }
        }	   
   }	   
   
}

if(!empty($_GET['user_game_id'])){
    /* Get Record For Display Starts */	
    //$Formval.rate_A
    $rate_A = $Formval['rate_A'];
    $rate_B = $Formval['rate_B'];
    $rate_C = $Formval['rate_C'];

    $SelectgameSql="SELECT * FROM ".TABLEPREFIX."_user_game WHERE user_game_id='".$_GET['user_game_id']."'";
    $Formval = $UserManagerObjAjax->GetRecords("Row",$SelectgameSql);
    $Formval['experience'] = stripslashes($Formval['experience']);
    $Formval['lesson_plan'] = stripslashes($Formval['lesson_plan']);
    $SubmitButton="Save";	

    ////
    $_POST['game_id'] = $Formval['game_id'];

    ob_start();

   $allPropssql = "SELECT * FROM `nk_user_game_property` WHERE game_id=".$_POST['game_id']." AND user_id=".$_SESSION['user_id']." AND user_type_id=1";
   $allPropsArr = $UserManagerObjAjax->GetRecords("All",$allPropssql);
   $NumAllPropsArr=count($allPropsArr);
    
   $gameCategoriesSql = "SELECT * FROM ".TABLEPREFIX."_game_categories WHERE game_id=".$_POST['game_id'];
//   logToFile($gameCategoriesSql);
   $gameCategoriesArr = $UserManagerObjAjax->GetRecords("All",$gameCategoriesSql);
   $NumGameCategories=count($gameCategoriesArr);
   for($x=0; $x<$NumGameCategories; $x++){
        $userCategoriesPropSql = "SELECT * FROM ".TABLEPREFIX."_game_categories_properties WHERE category_id = ".$gameCategoriesArr[$x]['category_id'];
        $categoriesPropArr = $UserManagerObjAjax->GetRecords("All",$userCategoriesPropSql);
//        $gameCategoriesArr[$x]['properties'] = $categoriesPropArr;
        $NumGameCategoriesProps=count($categoriesPropArr);
        $props = array();
        for($y=0; $y<$NumGameCategoriesProps; $y++){
            $prop = $categoriesPropArr[$y];
            $prop['checked'] = false;
            for($z=0; $z<$NumAllPropsArr; $z++){
               if($allPropsArr[$z]['property_id'] == $prop['property_id']){
                   $prop['checked'] = true;
                   break;
               }
            }
            
            $props[] = $prop;
        }
        
        $gameCategoriesArr[$x]['properties'] = $props;
        
    }
  
   ob_end_clean();
	
}
else{
    $SubmitButton="Add";
}
$countrySql = "SELECT country_id,country_name FROM ".TABLEPREFIX."_country ORDER BY country_name";
$CountryArr = $UserManagerObjAjax->HtmlOptionArrayCreate($countrySql);


$gameSql = "SELECT game_id, game_name FROM ".TABLEPREFIX."_game where status=1 ORDER BY game_name";
$GameArr = $UserManagerObjAjax->HtmlOptionArrayCreate($gameSql);

$smarty->assign('gameCategoriesArr',$gameCategoriesArr);
$smarty->assign('extra_content',$extra_content);
$smarty->assign('Formval',$Formval);
$smarty->assign('is_coach',$is_coach);
$smarty->assign('is_partner',$is_partner);
$smarty->assign('profile',$profile);
$smarty->assign('ermsg',$ermsg);
$smarty->assign('CountryArr',$CountryArr);
$smarty->assign('Formval',$Formval);
$smarty->assign('SubmitButton',$SubmitButton);
$smarty->assign('GameArr',$GameArr);
$smarty->assign('user_game_id',$user_game_id);
$smarty->display("coach_game_update.tpl");
include("footer.php");
?>