<?php

session_start();

require_once $_SERVER['DOCUMENT_ROOT'].'/adodb/DATABASE.php';
include_once($_SERVER['DOCUMENT_ROOT'].'/adodb/adodb.inc.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/adodb/tohtml.inc.php'); 

$DBConnection="remote";

define('TABLEPREFIX','nk');
define('VALIDATE_ACTIVE',0); // 0 for inactive, 1 for active

$user = DATABASE::$username;
$pass = DATABASE::$password;
$host = DATABASE::$host;
$db_name  =DATABASE::$databasename;	
$adodbcon = &ADONewConnection(DATABASE::$connection); 
$adodbcon->debug=false;
$adodbcon->Connect($host,$user,$pass,$db_name);  
        
if($DBConnection=="local"){	
	/*---------Local URL Info Starts----------*/
	$domainName  = "http://localhost/";
	$site_url  = "http://localhost/";
	$site_url_ssl  = "http://localhost/";
	$site_name = "http://localhost/";
	/*---------Local URL Info Ends------------*/
        define('SITE_URL','http://localhost/');

}

elseif($DBConnection=="remote"){	
	/*---------Local URL Info Starts----------*/

	$domainName  = "http://www.chirongaming.com";
	$site_url  = "http://www.chirongaming.com/";
	$site_url_ssl  = "http://www.chirongaming.com/";
	$site_name = "http://www.chirongaming.com/";
	define('SITE_URL','http://www.chirongaming.com/');

	/*---------Local URL Info Ends------------*/
}
/* Payment Details Starts */
$PaypalID="ito@netcabo.pt";
$PaymentMode="Test";
if($PaymentMode=='Test')	
	$PaypalUrl='https://www.sandbox.paypal.com/cgi-bin/webscr';	
elseif($PaymentMode=='Live')	
	$PaypalUrl='https://www.paypal.com/cgi-bin/webscr';	
/* Payment Details Ends */

//$admin_email=$adodbcon->GetOne("SELECT admin_email FROM ".TABLEPREFIX."_admin WHERE admin_id='1'");
$admin_email='administrator@chirongaming.com';

$items_per_page   = 30;
