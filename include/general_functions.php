<?php
/*************************************************************************************/
#Function needed to create a black and white version of the image automatically
#sample call :  convertToBW('images','100_1498.jpg');
function convertToBW($folder_path,$filename)
{
	if(empty($filename))
		return false;
		
	#remove any trailing slashes
	$folder_path=rtrim($folder_path,"/");	
	
	$filepath=$folder_path."/".$filename;
	if(file_exists($filepath))
	{
		#explode filename to find the file type
		$file_arr=explode(".",$filename);
		$file_ext=	$file_arr[count($file_arr)-1];
		
		#set black+white_image name
		$filename_bw='BW_'.$filename;
		$bw_filepath=$folder_path."/".$filename_bw;
	
		if(empty($file_ext) || !in_array($file_ext,array('jpeg','jpg','gif','png')))
			return false;
			
		list($width, $height) = getimagesize($filepath);
		
		switch($file_ext)
		{
			case 'jpeg':
			case 'jpg':
					$source = imagecreatefromjpeg($filepath);
					$bwimage= imagecreate($width, $height);
					for ($c=0;$c<256;$c++)
					{
						$palette[$c] = imagecolorallocate($bwimage,$c,$c,$c);
					}
					for ($y=0;$y<$height;$y++)
					{
						for ($x=0;$x<$width;$x++)
						{
							$rgb = imagecolorat($source,$x,$y);
							$r = ($rgb >> 16) & 0xFF;//error is here I am getting a value of zerofor $r and $g
							
							$g = ($rgb >> 8) & 0xFF; //error getting the value zero everytime
							$b = $rgb & 0xFF;//Here I am getting value
							
							$gs = yiq($r,$g,$b);
							imagesetpixel($bwimage,$x,$y,$palette[$gs]);
						}
					}
					imagejpeg($bwimage,$bw_filepath);
					break;
				case 'gif':
					$source = imagecreatefromgif($filepath);
					$bwimage= imagecreate($width, $height);
					for ($c=0;$c<256;$c++)
					{
						$palette[$c] = imagecolorallocate($bwimage,$c,$c,$c);
					}
					for ($y=0;$y<$height;$y++)
					{
						for ($x=0;$x<$width;$x++)
						{
							$rgb = imagecolorat($source,$x,$y);
							$r = ($rgb >> 16) & 0xFF;//error is here I am getting a value of zerofor $r and $g
							
							$g = ($rgb >> 8) & 0xFF; //error getting the value zero everytime
							$b = $rgb & 0xFF;//Here I am getting value
							
							$gs = yiq($r,$g,$b);
							imagesetpixel($bwimage,$x,$y,$palette[$gs]);
						}
					}
					imagegif($bwimage,$bw_filepath);
					break;
				case 'png':
					$source = imagecreatefrompng($filepath);
					$bwimage= imagecreate($width, $height);
					for ($c=0;$c<256;$c++)
					{
						$palette[$c] = imagecolorallocate($bwimage,$c,$c,$c);
					}
					for ($y=0;$y<$height;$y++)
					{
						for ($x=0;$x<$width;$x++)
						{
							$rgb = imagecolorat($source,$x,$y);
							$r = ($rgb >> 16) & 0xFF;//error is here I am getting a value of zerofor $r and $g
							
							$g = ($rgb >> 8) & 0xFF; //error getting the value zero everytime
							$b = $rgb & 0xFF;//Here I am getting value
							
							$gs = yiq($r,$g,$b);
							imagesetpixel($bwimage,$x,$y,$palette[$gs]);
						}
					}
					imagepng($bwimage,$bw_filepath);
					break;	
		}	
	}
	return true;
}

function yiq($r,$g,$b)
{
	return (($r*0.299)+($g*0.587)+($b*0.114));
}

/*************************************************************************************/


function setPages($pages)
{
	$page_arr=array();
	for($i=$pages;$i>=1;$i--)
	{
		$page_arr[]=$i;
	}
	return $page_arr;
}


function sql_value($value)

{

	$value = trim($value);

  

	if($value == null || $value == "")

	{

		return "NULL";

	}

	$magic_on=get_magic_quotes_gpc();

	return "'".str_replace("'", "\'", str_replace("\n", "\\n", $value))."'";

}



function strip_quote($value) 

{

	$value = trim($value);

	

	if($value == null || $value == "") 

	{

	return "NULL";

	}  

	return str_replace("'", "", str_replace("\"", "", $value));

}



function mysql_quote($value,$is_quote="Y")

{

	// Stripslashes

	if (get_magic_quotes_gpc()) {

		$value = stripslashes($value);

	}

	// Quote if not a number or a numeric string

	if($is_quote=="Y")

		$quote_str="'";

	else

		$quote_str="";

		

	if (!is_numeric($value)) {

		$value =  mysql_real_escape_string($value);

	}

	return $quote_str.$value.$quote_str;

}



function show_to_control($value)

{	

	return htmlspecialchars(stripslashes($value));;

}



function date_format_insert($Date)

{

	$Date =explode("/",$Date);

	$new_date= $Date[2]."-".$Date[1]."-".$Date[0];

	return $new_date ; 

}

function date_format_display($Date)

{
    if($Date=="0000-00-00"||empty($Date)|| $Date=="00/00/0000")

	{

		return "N/A";

	}
	else
	{
    
		$Date =explode("-",$Date);
	
		$new_date= $Date[2]."/".$Date[1]."/".$Date[0];
	
		return $new_date ;
	}	 

}



function date_format_admin($date)

{

	if($date=="0000-00-00"||empty($date)|| $date=="00/00/0000")

	{

		return "N/A";

	}	

	else

	{

		$datetimeArr = explode(" ",$date);
		
		$date_arr=explode("-",$datetimeArr[0]);

		$date_str=$date_arr[2]."/".$date_arr[1]."/".$date_arr[0];
         
		//$time_arr=explode(":",$datetimeArr[0]); 
        
		//$timestr=
		return $date_str.' '.$datetimeArr[1];

	}	

}



function getPageTrail($id="")

{

	switch($id)

	{

		case 1: $PageTrail = "Company"; break;

		case 2: $PageTrail = "Services"; break;

		case 3: $PageTrail = "Solutions"; break;

		case 4: $PageTrail = "Process"; break;

		case 5: $PageTrail = "Partner with us"; break;

		case 6: $PageTrail = "Customers"; break;

		case 7: $PageTrail = "Careers"; break;

	}

	return $PageTrail;

}



# miscellaneous functions...



	# function to get parent-Id...

	function getParentID($txt='')

	{

		global $adodbcon;

		

		$SQL  = "SELECT `categoryID` FROM ". TABLEPREFIX ."_category

				 WHERE LOWER(`categoryName`) = '". $txt ."' ";

		$ID = $adodbcon->GetOne($SQL);

		

		return $ID;

	}

	

	

	# function to get Random Latest Project Info...

	function getLatestProjectInfo()

	{

		global $adodbcon;

		

		$projectID = getParentID('projects');

		

		$Query = "SELECT * FROM ". TABLEPREFIX ."_category

				  WHERE	`parentID` = ". $projectID ."

				  ORDER BY RAND()";

		$Link  = $adodbcon->Execute($Query);

		

		$latestProject = $Link->FetchRow();

		

		return $latestProject;

	}





					  
function is_selected($id,$cat_id)
{
	
	#print("Select FIND_IN_SET('$cat_id',".tbl_product.".category_id) as total from ".tbl_product." where product_id='". $id ."'");
	if(!empty($id)&&!empty($cat_id))
	{
		$total=mysql_result(mysql_query("Select FIND_IN_SET('$cat_id',".tbl_product.".category_id) as total from ".tbl_product." where product_id='". $id ."'"),0,'total');
		if($total>0)
		{
			return 'Selected';
		}
		else
		{
			return;
		}
	}
}


//Upload file function starts
function uploadFile($fileLocation,$fileName,$fileType,$targetName,$folderLoc)
{

if(isset($_FILES["$fileName"]))
{
if ((
 ($_FILES["$fileName"]["type"] == "$fileType")
)
&& ($_FILES["$fileName"]["size"] < 20000000)
)
  {
  if ($_FILES["$fileName"]["error"] > 0)
    {
   // echo "Return Code: " . $_FILES["$fileName"]["error"] . "<br />";
   return false;
    }
  else
    {
      
      move_uploaded_file($_FILES["$fileName"]["tmp_name"],
      "$fileLocation" . "$targetName");
  $final_url=SITE_URL.$folderLoc.$targetName;
  return $final_url;
    }
  }
else
  {
    return false;
 // echo "Invalid file";
  }
}
}//FUNCTION UPLOAD PDF


//Auto generating password

function generate_pass()
{
//set the random id length 
$random_id_length = 10; 

$rnd_id = crypt(uniqid(rand(),1)); 

$rnd_id = strip_tags(stripslashes($rnd_id)); 

$rnd_id = str_replace(".","",$rnd_id); 

$rnd_id = strrev(str_replace("/","",$rnd_id)); 

$rnd_id = substr($rnd_id,0,$random_id_length); 
return $rnd_id;
}
//end:autogenerating password

//sending mail to tenant
function sent_tenant_mail($tenant_email,$tenant_pass,$tenant_phone_no,$site,$site_url,$land_lord_id=0,$agent_id=0,$update=0)
{
global $AdminManagerObjAjax;

		$LandlordName = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_name FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$land_lord_id);
		if($site=="admin")
		{
			$loc="../";
			if($update == 0)
			$comment = "We are pleased to confirm that Rental Relations Team has registered your rented property on to RentalRelations.com under $LandlordName. You can now view your tenant information and contact your Landlord online by using the login details below:";
			if($update != 0)
			$comment = "We are pleased to inform that Rental Relations Team has updated your rented property on to RentalRelations.com under $LandlordName. To view your tenant information and contact your Landlord online by using the login details below:";
		}
		if($site=="front")
		{
			$loc="";
			if($agent_id != 0)
			{
				$AgentName = $AdminManagerObjAjax->GetRecords("One","SELECT agent_name FROM ".TABLEPREFIX."_agent WHERE agent_id=".$agent_id);
				if($update == 0)
				$comment = "We are pleased to confirm that $AgentName has registered your rented property on to RentalRelations.com under $LandlordName. You can now view your tenant information and contact your Landlord online by using the login details below:";
				if($update != 0)
				$comment = "We are pleased to inform that $AgentName has updated your rented property on to RentalRelations.com under $LandlordName. To view your tenant information and contact your Landlord online by using the login details below:";
			}
			else
			{
				if($update == 0)
				$comment = "We are pleased to confirm that $LandlordName has registered your rented property on to RentalRelations.com. You can now view your tenant information and contact your Landlord online by using the login details below:";
				if($update != 0)
				$comment = "We are pleased to inform that $LandlordName has updated your rented property on to RentalRelations.com. To view your tenant information and contact your Landlord online by using the login details below:";
			}
			
		}
		$tpl_file="".$loc."email_template/tenant_account_mail.html";
		$tpl_handler=fopen($tpl_file,"r");
		$tpl_message=fread($tpl_handler,filesize($tpl_file));
		fclose($tpl_handler);
	
	
	
		/***************Select Admin Email****************************/
		$SqlAdmin="SELECT admin_email FROM ".TABLEPREFIX."_admin WHERE admin_id=1";
		$admin_email= $AdminManagerObjAjax->GetRecords("One",$SqlAdmin);
		$SqlTnt="SELECT tenant_name FROM ".TABLEPREFIX."_property WHERE tenant_email='".$tenant_email."'";
		$tntName= $AdminManagerObjAjax->GetRecords("One",$SqlTnt);
		/***************Select Admin Email****************************/	
		
		$fromName = "Rental Relation:Tenant Account";
		$fromEmail = $admin_email;
		$to = $tenant_email;
		$subject = "Rental Relation:Tenant Account";
		$url = $site_url;
		
		$tpl_message=str_replace("[COMMENT]",$comment,$tpl_message);
		$tpl_message=str_replace("[SITEURL]",$url,$tpl_message);
		$tpl_message=str_replace("[HEADING]",$tntName,$tpl_message);
		$tpl_message=str_replace("[USERNAME]",$tenant_email,$tpl_message);
		$tpl_message=str_replace("[PASSWORD]",$tenant_pass,$tpl_message);
		$tpl_message=str_replace("[PHONE]",$tenant_phone_no,$tpl_message);
		$tpl_message=str_replace("[SUBJECT]","Rental Relation:Tenant Account",$tpl_message);
		
		$mail = new PHPMailer;
		$mail->FromName = $fromName;
		$mail->From    =  $fromEmail;
		$mail->Subject =  $subject;
		$mail->Body    = stripslashes($tpl_message);
		//$mail->AltBody = $text_content;
		$mail->IsHTML(true);
		$mail->AddAddress($to,$to);

				
		 if($_SERVER['HTTP_HOST'] !='192.168.0.120')
			  $mail->Send();
			  
}
//end of:sending mail to tenant


//sending mail to landlord
function sent_landlord_mail($land_lord_id,$site,$site_url,$agent_id=0,$update=0)
{
global $AdminManagerObjAjax;

		$AgentName = $AdminManagerObjAjax->GetRecords("One","SELECT agent_name FROM ".TABLEPREFIX."_agent WHERE agent_id=".$agent_id);
		
		if($site=="admin")
		{
			$loc="../";
			if($update == 0)
			$comment = "We are pleased to confirm that Rental Relations Team has registered your property on to RentalRelations.com. You can now view all your property information and contact your Agent online by using the login details below:";
			if($update != 0)
			$comment = "We are pleased to inform that Rental Relations Team has updated your property on to RentalRelations.com. To view your property information and contact your Agent online by using the login details below:";
		}
		if($site=="front")
		{
			$loc="";
			
			if($update == 0)
			$comment = "We are pleased to confirm that $AgentName has registered your property on to RentalRelations.com. You can now view all your property information and contact your Agent online by using the login details below:";
			if($update != 0)
			$comment = "We are pleased to inform that $AgentName has updated your property on to RentalRelations.com. To view your property information and contact your Agent online by using the login details below:";
		}
		
		$tpl_file="".$loc."email_template/landlord_account_mail.html";
		$tpl_handler=fopen($tpl_file,"r");
		$tpl_message=fread($tpl_handler,filesize($tpl_file));
		fclose($tpl_handler);
	
		/***************Select Admin Email****************************/
		$SqlAdmin="SELECT admin_email FROM ".TABLEPREFIX."_admin WHERE admin_id=1";
		$admin_email= $AdminManagerObjAjax->GetRecords("One",$SqlAdmin);
		$SqlLandlord="SELECT * FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id='".$land_lord_id."'";
		$LandlordArr = $AdminManagerObjAjax->GetRecords("Row",$SqlLandlord);
		/***************Select Admin Email****************************/	
		
		$fromName = "Rental Relation:Landlord Account";
		$fromEmail = $admin_email;
		$to = $LandlordArr['land_lord_email'];
		$subject = "Rental Relation:Landlord Account";
		$url = $site_url;
		
		$tpl_message=str_replace("[COMMENT]",$comment,$tpl_message);
		$tpl_message=str_replace("[SITEURL]",$url,$tpl_message);
		$tpl_message=str_replace("[HEADING]",$LandlordArr['land_lord_name'],$tpl_message);
		$tpl_message=str_replace("[USERNAME]",$LandlordArr['land_lord_username'],$tpl_message);
		$tpl_message=str_replace("[PASSWORD]",$LandlordArr['land_lord_password'],$tpl_message);
		$tpl_message=str_replace("[SUBJECT]","Rental Relation:Landlord Account",$tpl_message);
		
		$mail = new PHPMailer;
		$mail->FromName = $fromName;
		$mail->From    =  $fromEmail;
		$mail->Subject =  $subject;
		$mail->Body    = stripslashes($tpl_message);
		//$mail->AltBody = $text_content;
		$mail->IsHTML(true);
		$mail->AddAddress($to,$to);

				
		 if($_SERVER['HTTP_HOST'] !='192.168.0.120')
			  $mail->Send();
			  
}
//end of:sending mail to landlord

//sending mail for comment
function send_comment_mail($comment_id,$site_url)
{
global $AdminManagerObjAjax;

		$SqlComment = "SELECT * FROM ".TABLEPREFIX."_comment WHERE comment_id='".$comment_id."'";
		$CommentArr = $AdminManagerObjAjax->GetRecords("Row",$SqlComment);
		
		if($CommentArr['comment_from_to_type'] == "AL")
		{
			$from = $AdminManagerObjAjax->GetRecords("One","SELECT agent_name FROM ".TABLEPREFIX."_agent WHERE agent_id=".$CommentArr['comment_from_id']);
			$fromemail = $AdminManagerObjAjax->GetRecords("One","SELECT agent_email FROM ".TABLEPREFIX."_agent WHERE agent_id=".$CommentArr['comment_from_id']);
			
			$postedto = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_name FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_to_id']);
			$to = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_email FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_to_id']);
		}
		if($CommentArr['comment_from_to_type'] == "LA")
		{
			$postedto = $AdminManagerObjAjax->GetRecords("One","SELECT agent_name FROM ".TABLEPREFIX."_agent WHERE agent_id=".$CommentArr['comment_to_id']);
			$from = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_name FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_from_id']);
			
			$fromemail = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_email FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_from_id']);
			$to = $AdminManagerObjAjax->GetRecords("One","SELECT agent_email FROM ".TABLEPREFIX."_agent WHERE agent_id=".$CommentArr['comment_to_id']);
		}
		if($CommentArr['comment_from_to_type'] == "LT")
		{
			$from = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_name FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_from_id']);
			$postedto = $AdminManagerObjAjax->GetRecords("One","SELECT tenant_name FROM ".TABLEPREFIX."_property WHERE property_id=".$CommentArr['comment_to_id']);
			
			$fromemail = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_email FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_from_id']);
			$to = $AdminManagerObjAjax->GetRecords("One","SELECT tenant_email FROM ".TABLEPREFIX."_property WHERE property_id=".$CommentArr['comment_to_id']);
		}
		if($CommentArr['comment_from_to_type'] == "TL")
		{
			$from = $AdminManagerObjAjax->GetRecords("One","SELECT tenant_name FROM ".TABLEPREFIX."_property WHERE property_id=".$CommentArr['comment_from_id']);
			$fromemail = $AdminManagerObjAjax->GetRecords("One","SELECT tenant_email FROM ".TABLEPREFIX."_property WHERE property_id=".$CommentArr['comment_from_id']);
			
			$postedto = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_name FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_to_id']);
			$to = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_email FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_to_id']);
		}
		
		$tpl_file="email_template/comment_mail.html";
		$tpl_handler=fopen($tpl_file,"r");
		$tpl_message=fread($tpl_handler,filesize($tpl_file));
		fclose($tpl_handler);
		
		$fromName = "Rental Relation:Comment";
		$fromEmail = $fromemail;
		$to = $to;
		$subject = "Rental Relation:Comment";
		$url = $site_url;
		
		$tpl_message=str_replace("[SITEURL]",$url,$tpl_message);
		$tpl_message=str_replace("[HEADING]",$postedto,$tpl_message);
		$tpl_message=str_replace("[FROM]",$from,$tpl_message);
		$tpl_message=str_replace("[SUBJECT]",$CommentArr['comment_subject'],$tpl_message);
		$tpl_message=str_replace("[COMMENT]",$CommentArr['comment_text'],$tpl_message);
		
		$mail = new PHPMailer;
		$mail->FromName = $fromName;
		$mail->From    =  $fromEmail;
		$mail->Subject =  $subject;
		$mail->Body    = stripslashes($tpl_message);
		//$mail->AltBody = $text_content;
		$mail->IsHTML(true);
		$mail->AddAddress($to,$to);

				
		 if($_SERVER['HTTP_HOST'] !='192.168.0.120')
			  $mail->Send();
			  
}
//end of:sending mail for comment

//sending mail for reply
function send_reply_mail($comment_id,$site_url)
{
global $AdminManagerObjAjax;

		$SqlComment = "SELECT * FROM ".TABLEPREFIX."_comment WHERE comment_id='".$comment_id."'";
		$CommentArr = $AdminManagerObjAjax->GetRecords("Row",$SqlComment);
		
		if($CommentArr['comment_from_to_type'] == "AL")
		{
			$from = $AdminManagerObjAjax->GetRecords("One","SELECT agent_name FROM ".TABLEPREFIX."_agent WHERE agent_id=".$CommentArr['comment_from_id']);
			$fromemail = $AdminManagerObjAjax->GetRecords("One","SELECT agent_email FROM ".TABLEPREFIX."_agent WHERE agent_id=".$CommentArr['comment_from_id']);
			
			$postedto = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_name FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_to_id']);
			$to = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_email FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_to_id']);
		}
		if($CommentArr['comment_from_to_type'] == "LA")
		{
			$postedto = $AdminManagerObjAjax->GetRecords("One","SELECT agent_name FROM ".TABLEPREFIX."_agent WHERE agent_id=".$CommentArr['comment_to_id']);
			$from = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_name FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_from_id']);
			
			$fromemail = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_email FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_from_id']);
			$to = $AdminManagerObjAjax->GetRecords("One","SELECT agent_email FROM ".TABLEPREFIX."_agent WHERE agent_id=".$CommentArr['comment_to_id']);
		}
		if($CommentArr['comment_from_to_type'] == "LT")
		{
			$from = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_name FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_from_id']);
			$postedto = $AdminManagerObjAjax->GetRecords("One","SELECT tenant_name FROM ".TABLEPREFIX."_property WHERE property_id=".$CommentArr['comment_to_id']);
			
			$fromemail = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_email FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_from_id']);
			$to = $AdminManagerObjAjax->GetRecords("One","SELECT tenant_email FROM ".TABLEPREFIX."_property WHERE property_id=".$CommentArr['comment_to_id']);
		}
		if($CommentArr['comment_from_to_type'] == "TL")
		{
			$from = $AdminManagerObjAjax->GetRecords("One","SELECT tenant_name FROM ".TABLEPREFIX."_property WHERE property_id=".$CommentArr['comment_from_id']);
			$fromemail = $AdminManagerObjAjax->GetRecords("One","SELECT tenant_email FROM ".TABLEPREFIX."_property WHERE property_id=".$CommentArr['comment_from_id']);
			
			$postedto = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_name FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_to_id']);
			$to = $AdminManagerObjAjax->GetRecords("One","SELECT land_lord_email FROM ".TABLEPREFIX."_land_lord WHERE land_lord_id=".$CommentArr['comment_to_id']);
		}
		
		$comment_subject = $AdminManagerObjAjax->GetRecords("One","SELECT comment_subject FROM ".TABLEPREFIX."_comment WHERE comment_id=".$CommentArr['reply_to_comment_id']);
		
		$tpl_file="email_template/reply_mail.html";
		$tpl_handler=fopen($tpl_file,"r");
		$tpl_message=fread($tpl_handler,filesize($tpl_file));
		fclose($tpl_handler);
		
		$fromName = "Rental Relation:Comment";
		$fromEmail = $fromemail;
		$to = $to;
		$subject = "Rental Relation:Comment";
		$url = $site_url;
		
		$tpl_message=str_replace("[SITEURL]",$url,$tpl_message);
		$tpl_message=str_replace("[HEADING]",$postedto,$tpl_message);
		$tpl_message=str_replace("[FROM]",$from,$tpl_message);
		$tpl_message=str_replace("[SUBJECT]","RE:".$comment_subject,$tpl_message);
		$tpl_message=str_replace("[COMMENT]",$CommentArr['comment_text'],$tpl_message);
		
		$mail = new PHPMailer;
		$mail->FromName = $fromName;
		$mail->From    =  $fromEmail;
		$mail->Subject =  $subject;
		$mail->Body    = stripslashes($tpl_message);
		//$mail->AltBody = $text_content;
		$mail->IsHTML(true);
		$mail->AddAddress($to,$to);

				
		 if($_SERVER['HTTP_HOST'] !='192.168.0.120')
			  $mail->Send();
			  
}
//end of:sending mail for reply

function send_common_mail($fromarray,$toarray,$subject,$message,$isHTML = true)
{
/*	$mail = new PHPMailer;
	
	$mail->FromName = $fromarray['name'];
	$mail->From    =  $fromarray['email'];
	$mail->Subject =  $subject;
	$mail->Body    = stripslashes($message);
	//$mail->AltBody = $text_content;
	$mail->IsHTML($isHTML);
	$mail->AddAddress($toarray['email'],$toarray['name']);
*/
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From:'.$fromarray['email'] . "\r\n" .
								'X-Mailer: PHP/' . phpversion();
					
					mail($toarray['email'], $subject, $message, $headers);
    
	
	// if($_SERVER['HTTP_HOST'] !='10.10.0.100')
		//  $mail->Send();
}
//getting next autoincremented value
function predictAutoInc($tablename){
global $db_name;
$sql = "SELECT AUTO_INCREMENT as ai FROM information_schema.tables WHERE table_name = '$tablename' AND table_schema='$db_name'";
$res = mysql_query($sql);
$data = mysql_fetch_array($res);
return $data['ai'];
}

?>